import csv, time, os, sys
import scipy as sp
import numpy as np
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack
from scipy import signal

####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

######### Define importing, analysis functions #########

c=299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005
lWire = 3.551
lDUT = 2.45
resCopper = 17.24*10.0**-9
resOuter = 1/(3.3*10.0**7)
ZChar = 60*sp.log(dAper/rWire)

def overlapFreqMKI(lenOverlap, harmonic, permitivitty,delta):
    return harmonic*C/(2*permitivitty**0.5*(lenOverlap+delta))

def importDatSimMeas(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
    datStore = []
    for entry in tempDat[1:]:
        temp = map(float, entry.split(','))
        datStore.append(temp)

    return datStore

def analSingReSimMeas(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    reStore = []
    temp = []
##    Z0 = 60*sp.log(1.27*apPipe/r_wire)
    Z0=50.0
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(-2*Z0/lenTot*sp.log(10**(data[i,j]/20))) 
        temp.append(tempLin)
    return temp

def fileImport(tarFile):
    readPart=open(tarFile, 'r+')
    datUnPro = readPart.readlines()
    readPart.close()
    freqDat=[]
    qLoaded=[]
    s21DB=[]
    for row in datUnPro[20:]:
        freqDat.append(float(row.split(",")[0]))
        qLoaded.append(float(row.split(",")[1]))
        s21DB.append(float(row.split(",")[2]))

    return sp.array(freqDat), sp.array(qLoaded), sp.array(s21DB)

def skinDepth(freq, cond):
    return (1/(sp.pi*freq*mu0*cond))**0.5

def resImpGet(tarArr1,tarArr2,tarArr3,attenCableLen):

    freqList, qTotal, transCoeff = tarArr1, tarArr2, tarArr3

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(C*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCable = attenCableLen*freqList*10.0**-9
    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor

    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper-attenCable)/(attenCopper)

    return freqList, zMeas/lDUT

def resImpGetFromFile(targetFile):

    freqList, qTotal, transCoeff = fileImport(targetFile)

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(c*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor


    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper)/attenCopper

    return freqList, zMeas/lDUT

def resImpGetFromFileTrans(targetFile):

    freqList, qTotal, transCoeff = fileImport(targetFile)

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(c*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor


    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper)/attenCopper

    return list(freqList), list(zMeas/lDUT)

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def extract_dat2013(file_name):
    data = open(file_name, "r+")
    tar = data.readlines()
    temp=[]
    for row in tar[2:]:
        row = map(float, row.split())
        temp.append(row)
    data.close()

    return temp[:-1]

def sParaRead(fileTar, lenDUT, col):
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        phase = row[col+1]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360    

        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        import_dat = [row[0], row[col], sp.radians(phase), -s21Pec]
        data.append(import_dat)
        last = phase

    return data

def readS21FromVNA(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[6:]:
        temp=line.split("\t")
        outputData.append(map(float,temp))
    return outputData

def readS21FromCSV(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[3:]:
        temp=line.split(",")
        outputData.append(map(float,temp))
    return outputData

def impAnalysisSingle(fileTar, lenDUT, r_wire, r_pipe, Zc):
    ###### Analyses a S2P file for measurement of longitudinal impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        phase = row[4]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360


        import_dat = [row[0], -2*Zc*sp.log(linDat), -2*Zc*(sp.radians(phase)+s21Pec)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDUT), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDUT))]
        data.append(import_dat)
        last = phase

    return data

def impAnalysisTwo(fileTar, lenDut, r_wire, r_pipe, d_wire, Zc):
    ###### Analyses a S2P file for measurement of dipole impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter = 0.0
    last = 1e8

    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDut/C
        phase = row[4]-counter
        if phase>last and (phase-last)>180:
            count+=1
            counter+=360
            pgase-=360
        import_dat = [row[0], -2*Zc*sp.log(linDat)*C/(2*sp.pi*row[0]*d_wire**2), -2*Zc*(sp.radians(phase)+s21Pec)*C/(2*sp.pi*row[0]*d_wire**2)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDut)*C/(row[0]*d_wire**2), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDut))*C/(row[0]*d_wire**2)]
        data.append(import_dat)
    return data

def impTransPara(dispWire, measImp, wireErr):
##### Takes an array of longitudinal measurements and returns the transverse impedance
    quadFunc = lambda p, x: p[0] + x * p[1] + x**2 * p[2]
    errFunc = lambda p, x, Z: (Z-quadFunc(p,x))

    totTrans = []
    longComp = []

    for i in range(0,len(measImp[0,:,1])):
        pinit = [1.0, -1.0, 1.0]
        ZerrRe = measImp[:,i,1]*wireErr
        pReal, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,1]), full_output = 0)
        pinit = [1.0, -1.0, 1.0]
        ZerrIm = measImp[:,i,2]*wireErr
        pImag, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,2]), full_output = 0)
        totTrans.append([pReal[2]*C/(2*sp.pi*measImp[0,i,0]), pImag[2]*C/(2*sp.pi*measImp[0,i,0])])
        longComp.append([pReal[0], pImag[0]])

    return longComp, totTrans

def impTransParaRes(dispWire, measImp, wireErr):
##### Takes an array of longitudinal measurements and returns the transverse impedance
    quadFunc = lambda p, x: p[0] + x * p[1] + x**2 * p[2]
    errFunc = lambda p, x, Z: (Z-quadFunc(p,x))

    totTrans = []
    longComp = []
    for i in range(0,len(measImp)):
        pinit = [1.0, -1.0, 1.0]
        ZerrRe = measImp[i,1:]*wireErr
        pReal, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[i,1:]), full_output = 0)
        totTrans.append(pReal[2]*C/(2*sp.pi*measImp[i,0]))
        longComp.append(pReal[0])

    return longComp, totTrans

def quadImp(totalTrans, dipTrans, axis):
######## Returns the quadrupolar impedance from total transverse and dipolar data
    quadImp = []
    for i in range(0,len(totalTrans)):
        if axis == "horz":
            quadImp.append([totalTrans[i,1]+dipTrans[i,1],totalTrans[i,2]+dipTrans[i,2]])
        elif axis == "vert":
            quadImp.append([totalTrans[i,1]-dipTrans[i,1],totalTrans[i,2]-dipTrans[i,2]])
    return quadImp


def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def peakFitSimulations(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 50
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 50 and y_val[current_max_x]>2:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [0.2, 10.0, 10.0]
        lower_bound = res[0]-50
        upper_bound = res[0]+50
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def peakFitMeasurements(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 0
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 2 and y_val[current_max_x]>3:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [res[1], 4.0, data[res[0],1]]
        lower_bound = res[0]-4
        upper_bound = res[0]+3
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2)

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2
                                                    
def logToLin(data):
    return 10**(data/20)

def linToLog(data):
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

def overLapFreq(overlap, fringeLen, harmonic):
    return harmonic*C/(2*10**0.5*(overlap+fringeLen))

def heatLossBunByBunGauss(imp, bunLen, nBun, nParticles, freqRev):
    prodImpSpec = imp[1,:]*gaussProf(imp[0,:]*2*sp.pi,bunLen)**2
    curImpTime=prodImpSpec
    enLossPerBunch=2*(1.6*10**-19*nParticles*freqRev)**2*(curImpTime.sum())
    pLossBeam = enLossPerBunch*nBun**2
    pLossBeamFreq = 2*(1.6*10**-19*nParticles*freqRev)**2*nBun**2*curImpTime
    print pLossBeam, pLossBeamFreq
    return pLossBeam, pLossBeamFreq, gaussProf(imp[0,:],bunLen)**2

def extract_dat2013_MultipleSweeps(file_name):
    data = open(file_name, "r+")
    tar = data.readlines()
    temp=[]
    bigTemp = []
    count=2
    while count<len(tar):
        try:
            row = map(float, tar[count].split())
            temp.append(row)
            count+=1
        except:
            bigTemp.append(temp)
            temp=[]
            count+=3
    data.close()
    bigTemp.append(temp)
    return bigTemp

def freqRLC(L, C, harmonic):
    return harmonic/(L*C)**0.5/(2*sp.pi)

def heatingValGauss(impArr, beamCur, bunSpac, bunLen):
    ### Expects frequency in GHz ###
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac/10**9,impArr[-1,0],impArr[-1,0]*10**9*bunSpac)
    splineFitHeatingImp = SplineFitImp(freqListHeating)
    heatingTotalPart = []
    for i in range(0,len(splineFitHeatingImp)):
        heatingTotalPart.append([freqListHeating[i], abs(2*splineFitHeatingImp[i]*(beamCur**2)*gaussProf(freqListHeating[i]*10.0**9, bunLen))])
    heatingTotalPart=sp.array(heatingTotalPart)
    cumHeating = []
    for i in range(0,len(heatingTotalPart)):
        cumHeating.append([heatingTotalPart[i,0],sum(heatingTotalPart[:i,1])])
    cumHeating=sp.array(cumHeating)
    return heatingTotalPart[:,1].sum(), heatingTotalPart, cumHeating

def heatingValGaussS2P(impArr, beamCur, bunSpac, bunLen):
    ### Expects frequency in Hz ###
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac,impArr[-1,0],impArr[-1,0]*bunSpac)
    splineFitHeatingImp = SplineFitImp(freqListHeating)
    heatingTotalPart = []
    for i in range(0,len(splineFitHeatingImp)):
        heatingTotalPart.append([freqListHeating[i], abs(2*splineFitHeatingImp[i]*(beamCur**2)*gaussProf(freqListHeating[i], bunLen))])
    heatingTotalPart=sp.array(heatingTotalPart)
    cumHeating = []
    for i in range(0,len(heatingTotalPart)):
        cumHeating.append([heatingTotalPart[i,0],sum(heatingTotalPart[:i,1])])
    cumHeating=sp.array(cumHeating)
    return heatingTotalPart[:,1].sum(), heatingTotalPart, cumHeating

def heatingValGaussRes(impArr, beamCur, bunSpac, bunLen):
    ### Expects frequency in Hz ###
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac,impArr[-1,0],impArr[-1,0]*bunSpac)
    splineFitHeatingImp = SplineFitImp(freqListHeating)
    heatingTotalPart = []
    for i in range(0,len(splineFitHeatingImp)):
        heatingTotalPart.append([freqListHeating[i], abs(2*splineFitHeatingImp[i]*(beamCur**2)*gaussProf(freqListHeating[i], bunLen))])
    heatingTotalPart=sp.array(heatingTotalPart)
    cumHeating = []
    for i in range(0,len(heatingTotalPart)):
        cumHeating.append([heatingTotalPart[i,0],sum(heatingTotalPart[:i,1])])
    cumHeating=sp.array(cumHeating)
    return heatingTotalPart[:,1].sum(), heatingTotalPart, cumHeating

def imgImpFromSPara(inputData, lenDUT, Zc):
    data=[]
    for row in inputData:
        tmpDat = []
        count = 0
        counter=0.0
        last = 1e8
        freq_last = 0.0
        for line in row:
            s21Pec = 2*sp.pi*line[0]*lenDUT/C
            phase = line[4]-counter
            if phase > last and (phase-last)>180 and abs(freq_last-line[0])>0.3e8:
                count+=1
                counter+=360
                phase-=360
                freq_last = line[0]
            elif phase>last and (phase-last)>180 and abs(freq_last-line[0])<0.3e8:
                phase+=360
        
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDUT), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDUT))]
            tmpDat.append(-2*Zc*(sp.radians(phase)+s21Pec))
            last = phase
        data.append(tmpDat)
    return sp.array(data)


def fitQuadraticSquaresImp(sParaData, dispWire):
    impLongDat=[]
    impTransDat=[]
    errors=[]
    quadFunc = lambda p, x: p[0] + x*p[1] + x**2*p[2]
    errFunc = lambda p, x, z: (z-quadFunc(p,x))
    impDatImag = imgImpFromSPara(sParaData, 3.45, 305)
    try:
        os.mkdir("E:/PhD/1st_Year_09-10/Data/CLICStripline/singleWireDisplace/plots")
    except:
        pass
    for i in range(0,len(sParaData[0])):
        fitDataReal = sp.array(logImpFormula(logToLin(sParaData[:,i,3]),1,305))
        initVal = [1.0,-0.6,0.7]
        out = op.leastsq(errFunc, initVal, args=(dispWire, fitDataReal), full_output=1)
        pReal, pFunc = out[0], out[1]
        out = op.leastsq(errFunc, initVal, args=(dispWire, impDatImag[:,i]), full_output = 1)
        pImag, pother = out[0], out[1]
        impLongDat.append([sParaData[0,i,0], pReal[0], pImag[0]])
##        print C/(2*sp.pi*sParaData[0,i,0])
        totTrans = [(pReal[2]*C)/(2*sp.pi*sParaData[0,i,0]), (pImag[2]*C)/(2*sp.pi*sParaData[0,i,0])]
##        print totTrans
        impTransDat.append([sParaData[0,i,0], totTrans[0], totTrans[1]])
        errors.append([pFunc, pother])
##        dispPlot = sp.linspace(dispWire[0]-0.005,dispWire[-1]+0.005,100)
##        pl.plot(dispWire,fitDataReal)
##        pl.plot(dispPlot, quadFunc(pReal,dispPlot))
##        pl.savefig("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/27-10-15/mkiHorzTrans/plotImp"+str(i)+".png")
        pl.clf()
        
    return sp.array(impLongDat), sp.array(impTransDat), sp.array(errors)

def errorPlottables(errJacobian, impData):
    """Takes a list of error Jacobians and impedance data as input. Outputs scaled values for statistical output
    """
##    print errJacobian
    errReal, errImag = errJacobian[:,0,2,2], errJacobian[:,1,2,2]
    errReal = sp.sqrt(errReal)*impData[:,1]/10.0**-6*(C/(2*sp.pi*impData[:,0]))
    errImag = sp.sqrt(errImag)*impData[:,2]/10.0**-6*(C/(2*sp.pi*impData[:,0]))
    return errReal, errImag

def errorfill(x, y, yerr, color=None, alpha_fill=0.2, ax=None, label=None):
    ax = ax if ax is not None else pl.gca()
    if color is None:
        color = ax._get_lines.color_cycle.next()
    if np.isscalar(yerr) or len(yerr) == len(y):
        ymin = y - yerr
        ymax = y + yerr
    elif len(yerr) == 2:
        ymin, ymax = yerr
    ax.plot(x, y, color=color, label=label)
    ax.fill_between(x, ymax, ymin, color=color, alpha=alpha_fill)

def concatArrRes(freqArr, impArr):
    temp = [[freqArr[i], impArr[i]] for i in range(0,len(freqArr))]
    return sp.array(temp)

length = 2.88
f_rev = 43400
nBunches = 288
qPart = 1.6*10.0**-19
nPart=1.1*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=4*0.2/C
testImp = [250*10**6, 10.0, 10000]
bunchCur=nPart*qPart*f_rev


topDir = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/"
postls130m117mm100mDat = sp.array(extract_dat2013(topDir+"POSTLS1Overlap117mm100mWake/longitudinal-impedance-real.txt"))
postls230m70mm100mDat = sp.array(extract_dat2013(topDir+"POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/70mmOverlapWith1mmAirGap/longitudinal-impedance-real.txt"))
HLLHC80mm100mDat = sp.array(extract_dat2013(topDir+"HLLHCProposal/80mmHigherMeshDens/longitudinal-impedance-real.txt"))

topDirMeasHorz = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/21-10-15/mkiTransverseHorzRes/"
topDirMeasVert = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/21-10-15/mkiTransverseVertRes/"

filesHorz = [topDirMeasHorz+"res"+str(i)+"mm" for i in range(-15,12,3)]
filesVert = [topDirMeasVert+str(i)+"mmres" for i in range(-9,15,3)]

topDirMeasHorzNewDesign = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/18-11-15/mkiResHorz/"
topDirMeasVertNewDesign = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/18-11-15/mkiResVert/"

filesHorzNewDesign = [topDirMeasHorzNewDesign+"x"+str(i)+"mm" for i in range(-9,15,3)]
filesVertNewDesign = [topDirMeasVertNewDesign+"y"+str(i)+"mm" for i in range(-12,12,3)]


topDirMeasVertMatched = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/27-10-15/mkiVertTrans/"
filesVertMatched = [topDirMeasVertMatched+"Y"+str(i)+"MM.S2P" for i in range(-15,12,3)]

topDirMeasVertMatchedLowFreq = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/27-10-15/mkiVertTransLowFreq/"
filesVertMatchedLowFreq = [topDirMeasVertMatchedLowFreq+"Y"+str(i)+"MM.S2P" for i in range(-15,12,3)]

topDirMeasHorzMatched = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/27-10-15/mkiHorzTrans/"
filesHorzMatched = [topDirMeasHorzMatched+"X"+str(i)+"MM.S2P" for i in range(-9,18,3)]

topDirMeasHorzMatchedLowFreq = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/27-10-15/mkiHorzTransLowFreq/"
filesHorzMatchedLowFreq = [topDirMeasHorzMatchedLowFreq+"X"+str(i)+"MM.S2P" for i in range(-9,15,3)]

topDirMeasVertMatchedNewDesign = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/24-11-15/vertNoAtten/"
filesVertMatchedNewDesign = [topDirMeasVertMatchedNewDesign+"Y"+str(i)+"MM.S2P" for i in range(-12,12,3)]

topDirMeasVertMatchedLowFreqNewDesign = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/24-11-15/vertNoAttenLowFreq/"
filesVertMatchedLowFreqNewDesign = [topDirMeasVertMatchedLowFreqNewDesign+"Y"+str(i)+"MM.S2P" for i in range(-12,12,3)]

##topDirMeasVertMatchedNewDesign = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/24-11-15/vert/"
##filesVertMatchedNewDesign = [topDirMeasVertMatchedNewDesign+"Y"+str(i)+"MM.S2P" for i in range(-12,12,3)]
##
##topDirMeasVertMatchedLowFreqNewDesign = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/24-11-15/vertLowFreq/"
##filesVertMatchedLowFreqNewDesign = [topDirMeasVertMatchedLowFreqNewDesign+"Y"+str(i)+"MM.S2P" for i in range(-12,12,3)]

topDirMeasHorzMatchedNewDesign = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/24-11-15/horzNoAtten/"
filesHorzMatchedNewDesign = [topDirMeasHorzMatchedNewDesign+"X"+str(i)+"MM.S2P" for i in range(-12,12,3)]

topDirMeasHorzMatchedLowFreqNewDesign = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/24-11-15/horzNoAttenLowFreq/"
filesHorzMatchedLowFreqNewDesign = [topDirMeasHorzMatchedLowFreqNewDesign+"X"+str(i)+"MM.S2P" for i in range(-12,12,3)]


datHorz = []
for entry in filesHorz:
    freq, imp = resImpGetFromFileTrans(entry)
    tmp = []
    for i in range(0,len(freq)):
        tmp.append([freq[i],imp[i]])
    datHorz.append(tmp)

tempHorz = []

for i in range(0,len(datHorz[0])-1):
    tempSlice = [datHorz[0][i][0]]
    for j in range(0,len(datHorz)):
        tempSlice.append(datHorz[j][i][1])
    tempHorz.append(tempSlice)

impHorz = sp.array(tempHorz)

datVert = []
for entry in filesVert:
    freq, imp = resImpGetFromFileTrans(entry)
    tmp = []
    for i in range(0,len(freq)):
        tmp.append([freq[i],imp[i]])
    datVert.append(tmp)

tempVert = []

for i in range(0,len(datVert[0])-1):
    tempSlice = [datVert[0][i][0]]
    for j in range(0,len(datVert)):
        tempSlice.append(datVert[j][i][1])
    tempVert.append(tempSlice)

impVert = sp.array(tempVert)

xDisplacements = sp.linspace(-15,9,9)
yDisplacements = sp.linspace(-9,12,8)

longHorz, totHorz = impTransParaRes(xDisplacements, impHorz, 0)
longVert, totVert = impTransParaRes(yDisplacements, impVert, 0)

datMatchedVertTemp = []
for fileEn in filesVertMatched:
    datMatchedVertTemp.append(readS21FromVNA(fileEn))
datMatchedVertTemp=sp.array(datMatchedVertTemp)
yDisplacements = sp.linspace(-15,9,9)
longitudinalVert, totTransVert, errorVert = fitQuadraticSquaresImp(datMatchedVertTemp, yDisplacements)

errVertReal, errVertImag = errorPlottables(errorVert, totTransVert)

datMatchedVertTempLowFreq = []
for fileEn in filesVertMatchedLowFreq:
    datMatchedVertTempLowFreq.append(readS21FromVNA(fileEn))
datMatchedVertTempLowFreq=sp.array(datMatchedVertTempLowFreq)
yDisplacements = sp.linspace(-15,9,9)
longitudinalVertLowFreq, totTransVertLowFreq, errorVertLowFreq = fitQuadraticSquaresImp(datMatchedVertTempLowFreq, xDisplacements)

errVertLowFreqReal, errVertLowFreqImag = errorPlottables(errorVertLowFreq, totTransVertLowFreq)

datMatchedHorzTemp = []
for fileEn in filesHorzMatched:
    datMatchedHorzTemp.append(readS21FromVNA(fileEn))
datMatchedHorzTemp=sp.array(datMatchedHorzTemp)
xDisplacements = sp.linspace(9,15,9)
longitudinalHorz, totTransHorz, errorHorz = fitQuadraticSquaresImp(datMatchedHorzTemp, xDisplacements)

errHorzReal, errHorzImag = errorPlottables(errorHorz, totTransHorz)

datMatchedHorzTempLowFreq = []
for fileEn in filesHorzMatchedLowFreq:
    datMatchedHorzTempLowFreq.append(readS21FromVNA(fileEn))
datMatchedHorzTempLowFreq=sp.array(datMatchedHorzTempLowFreq)
xDisplacements = sp.linspace(9,12,8)
longitudinalHorzLowFreq, totTransHorzLowFreq, errorHorzLowFreq = fitQuadraticSquaresImp(datMatchedHorzTempLowFreq, xDisplacements)

errHorzLowFreqReal, errHorzLowFreqImag = errorPlottables(errorHorzLowFreq, totTransHorzLowFreq)

datMatchedVertTempNewDesign = []
for fileEn in filesVertMatchedNewDesign:
    datMatchedVertTempNewDesign.append(readS21FromVNA(fileEn))
datMatchedVertTempNewDesign=sp.array(datMatchedVertTempNewDesign)
yDisplacements = sp.linspace(-12,9,8)
longitudinalVertNewDesign, totTransVertNewDesign, errorVertNewDesign = fitQuadraticSquaresImp(datMatchedVertTempNewDesign, yDisplacements)

errVertRealNewDesign, errVertImagNewDesign = errorPlottables(errorVertNewDesign, totTransVertNewDesign)

datMatchedVertLowFreqTempNewDesign = []
for fileEn in filesVertMatchedLowFreqNewDesign:
    datMatchedVertLowFreqTempNewDesign.append(readS21FromVNA(fileEn))
datMatchedVertLowFreqTempNewDesign=sp.array(datMatchedVertLowFreqTempNewDesign)
yDisplacements = sp.linspace(-12,9,8)
longitudinalVertLowFreqNewDesign, totTransVertLowFreqNewDesign, errorVertLowFreqNewDesign = fitQuadraticSquaresImp(datMatchedVertLowFreqTempNewDesign, yDisplacements)

errVertRealLowFreqNewDesign, errVertImagLowFreqNewDesign = errorPlottables(errorVertLowFreqNewDesign, totTransVertLowFreqNewDesign)


datMatchedHorzTempNewDesign = []
for fileEn in filesHorzMatchedNewDesign:
    datMatchedHorzTempNewDesign.append(readS21FromVNA(fileEn))
datMatchedHorzTempNewDesign=sp.array(datMatchedHorzTempNewDesign)
xDisplacements = sp.linspace(-12,9,8)
longitudinalHorzNewDesign, totTransHorzNewDesign, errorHorzNewDesign = fitQuadraticSquaresImp(datMatchedHorzTempNewDesign, xDisplacements)

errHorzRealNewDesign, errHorzImagNewDesign = errorPlottables(errorHorzNewDesign, totTransHorzNewDesign)

datMatchedHorzLowFreqTempNewDesign = []
for fileEn in filesHorzMatchedLowFreqNewDesign:
    datMatchedHorzLowFreqTempNewDesign.append(readS21FromVNA(fileEn))
datMatchedHorzLowFreqTempNewDesign=sp.array(datMatchedHorzLowFreqTempNewDesign)
xDisplacements = sp.linspace(-12,9,8)
longitudinalHorzLowFreqNewDesign, totTransHorzLowFreqNewDesign, errorHorzLowFreqNewDesign = fitQuadraticSquaresImp(datMatchedHorzLowFreqTempNewDesign, xDisplacements)

errHorzRealLowFreqNewDesign, errHorzImagLowFreqNewDesign = errorPlottables(errorHorzLowFreqNewDesign, totTransHorzLowFreqNewDesign)


datHorzNewDesign = []
for entry in filesHorzNewDesign:
    freq, imp = resImpGetFromFileTrans(entry)
    tmp = []
    for i in range(0,len(freq)):
        tmp.append([freq[i],imp[i]])
    datHorzNewDesign.append(tmp)

tempHorzNewDesign = []

for i in range(0,len(datHorzNewDesign[0])-1):
    tempSlice = [datHorzNewDesign[0][i][0]]
    for j in range(0,len(datHorzNewDesign)):
        tempSlice.append(datHorzNewDesign[j][i][1])
    tempHorzNewDesign.append(tempSlice)

impHorzNewDesign = sp.array(tempHorzNewDesign)

datVertNewDesign = []
for entry in filesVertNewDesign:
    freq, imp = resImpGetFromFileTrans(entry)
    tmp = []
    for i in range(0,len(freq)):
        tmp.append([freq[i],imp[i]])
    datVertNewDesign.append(tmp)

tempVertNewDesign = []

for i in range(0,len(datVertNewDesign[0])-1):
    tempSlice = [datVertNewDesign[0][i][0]]
    for j in range(0,len(datVertNewDesign)):
        tempSlice.append(datVertNewDesign[j][i][1])
    tempVertNewDesign.append(tempSlice)

impVertNewDesign = sp.array(tempVertNewDesign)

xDisplacements = sp.linspace(-9,15,8)
yDisplacements = sp.linspace(-12,12,8)

longHorzNewDesign, totHorzNewDesign = impTransParaRes(xDisplacements, impHorzNewDesign, 0)
longVertNewDesign, totVertNewDesign = impTransParaRes(yDisplacements, impVertNewDesign, 0)




##heatingTotalMKPCen = 0.0
##heatingFreqMKPCen = []
##heatingCumFreqMKPCen = []
####
##for i in range(0,len(splineFitHeatingMKPCen)):
##    heatingTotalMKPCen+=2*splineFitHeatingMKPCen[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
##    heatingFreqMKPCen.append(2*splineFitHeatingMKPCen[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
##    heatingCumFreqMKPCen.append(sp.sum(heatingFreqMKPCen))
    
##print heatingTotalMKPCen
##print bCur
##pl.plot(freqListHeating, heatingFreqMKPCen)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("Power Loss (W)", fontsize=16.0)
##pl.show()
pl.clf()


##
##print longHorz
##pl.semilogy()
##pl.semilogx()
##pl.plot(impHorz[:,0]/10**9, (longHorz), "kx",markersize=16.0, label="Z$_{\parallel, x}$ Resonant, Old Design")
##pl.plot(impHorz[:,0]/10**9, (longHorz), "k-")
##pl.plot(impHorz[:,0]/10**9, (longHorz), "k-")
##pl.plot(impVert[:,0]/10**9, longVert, "gx",label="Z$_{\parallel, y}$ Resonant")
##pl.plot(impVert[:,0]/10**9, longVert, "g-")
##pl.plot(impHorzNewDesign[:,0]/10**9, (longHorzNewDesign), "bx",label="Z$_{\parallel, x}$ Resonant, New Design")
##pl.plot(impHorzNewDesign[:,0]/10**9, (longHorzNewDesign), "b-")
##pl.plot(impHorzNewDesign[:,0]/10**9, (longHorzNewDesign), "r-")
pl.plot(impVertNewDesign[:,0]/10**9, longVertNewDesign, "bx",label="Z$_{\parallel, y}$ Resonant, new design")
pl.plot(impVertNewDesign[:,0]/10**9, longVertNewDesign, "b-")
##pl.plot(longitudinalVert[:,0]/10**9, signal.detrend(longitudinalVert[:,1]-longitudinalVert[0,1])/lDUT, "k-", label="$\Re{}$e(Z$_{\parallel, y}$), Transmission")
##pl.plot(longitudinalVert[:,0]/10**9, longitudinalVert[:,2]-longitudinalVert[0,2], "r-", label="$\Im{}$m(Z$_{\parallel, y}$)")
##pl.plot(longitudinalVertLowFreq[:,0]/10**9, longitudinalVertLowFreq[:,1]-longitudinalVertLowFreq[-1,1], "k--", label="$\Re{}$e(Z$_{\parallel, y}$), Transmission Low Freq")
##pl.plot(longitudinalVertLowFreq[:,0]/10**9, longitudinalVertLowFreq[:,2]-longitudinalVertLowFreq[0,2], "r--", label="$\Im{}$m(Z$_{\parallel, y}$)")
##pl.plot(longitudinalHorz[:,0]/10**9, (longitudinalHorz[:,1]-longitudinalHorz[0,1])/lDUT, "k-", label="$\Re{}$e(Z$_{\parallel, x}$), Old Design")
##pl.plot(longitudinalHorz[:,0]/10**9, (longitudinalHorz[:,2]-longitudinalHorz[0,2]), "r-", label="$\Im{}$m(Z$_{\parallel, x}$)")
##pl.plot(longitudinalHorzLowFreq[:,0]/10**9, (longitudinalHorzLowFreq[:,1]-500), "k--", label="$\Re{}$e(Z$_{\parallel, x}$)")
##pl.plot(longitudinalHorzLowFreq[:,0]/10**9, (longitudinalHorzLowFreq[:,2]-500), "r--", label="$\Im{}$m(Z$_{\parallel, x}$)")
##pl.plot(impHorz[:,0]/10**9, impHorz[:,5]*100.0, "bo",label="Z$_{\parallel, x}$")
##for entry in datMatchedHorzTemp:
##    pl.plot(entry[:,0]/10**9, logImpFormula(logToLin(entry[:,3]),1,305)-1100)
pl.plot(longitudinalVertNewDesign[:,0]/10**9, signal.detrend(longitudinalVertNewDesign[:,1]-longitudinalVertNewDesign[0,1])/lDUT, "b-", label="$\Re{}$e(Z$_{\parallel, y}$), Transmission, New Design")
##pl.plot(longitudinalVertNewDesign[:,0]/10**9, longitudinalVertNewDesign[:,2]-longitudinalVertNewDesign[0,2], "r-", label="$\Im{}$m(Z$_{\parallel, y}$)")
##pl.plot(longitudinalVertLowFreqNewDesign[:,0]/10**9, longitudinalVertLowFreqNewDesign[:,1]-longitudinalVertLowFreqNewDesign[-1,1], "b--", label="$\Re{}$e(Z$_{\parallel, y}$), Transmission Low Freq")
##pl.plot(longitudinalVertLowFreqNewDesign[:,0]/10**9, longitudinalVertLowFreqNewDesign[:,2]-longitudinalVertLowFreqNewDesign[0,2], "r--", label="$\Im{}$m(Z$_{\parallel, y}$)")


pl.plot(postls130m117mm100mDat[:,0], postls130m117mm100mDat[:,1]/lDUT, 'k--', label="Post-LS1 MKI Simulation")
##pl.plot(HLLHC80mm100mDat[:,0],HLLHC80mm100mDat[:,1]/lDUT, "b--", label="HL-LHC Proposal Simulation")
pl.plot(postls230m70mm100mDat[:,0], postls230m70mm100mDat[:,1]/lDUT, 'b--', label="Post-LS2 MKI Simulation")
pl.xlabel("Frequency (GHz)", fontsize=24.0)
pl.ylabel("$(Z_{\parallel})$ ($\Omega$/m)", fontsize=24.0)
pl.legend(loc="upper left")
##pl.xlim(10**-4,2.5)
pl.ylim(-10,70)
pl.show()
pl.clf()

meanBeta = 70

betaWeighting2DVert = meanBeta
betaWeighting2CVert = meanBeta
betaWeighting2BVert = meanBeta
betaWeighting2AVert = meanBeta

betaWeighting2DHorz = meanBeta
betaWeighting2CHorz = meanBeta
betaWeighting2BHorz = meanBeta
betaWeighting2AHorz = meanBeta

##pl.loglog()
##pl.semilogy()
pl.semilogx()
##pl.plot(impHorz[:,0]/10**9, sp.array(totHorz)/10.0**-6/10**6, "b-",label="Z$_{\perp,x}$$^{Tot}$")
##pl.plot(impVert[:,0]/10**9, sp.array(totVert)/10.0**-6/10**6, "g-", label="Z$_{\perp,y}$$^{Tot}$")
##pl.plot(impHorzNewDesign[:,0]/10**9, sp.array(totHorzNewDesign)/10.0**-6/10**6, "r-",label="Z$_{\perp,x}$$^{Tot}$, new design")
##pl.plot(impVertNewDesign[:,0]/10**9, sp.array(totVertNewDesign)/10.0**-6/10**6, "k-", label="Z$_{\perp,y}$$^{Tot}$, new design")
##pl.plot(totTransVert[:,0]/10**9, totTransVert[:,1]/10.0**-6/10**6, "k-", label="$\Re{}$e(Z$_{\perp,y}$$^{Tot}$)")
##pl.plot(totTransVert[:,0]/10**9, totTransVert[:,2]/10.0**-6/10**6, "r-", label="$\Im{}$m(Z$_{\perp,y}$$^{Tot}$)")
##pl.errorbar(totTransVert[:,0]/10**9, totTransVert[:,1]/10.0**-6/10**6, yerr = errVertReal/10**6,fmt="k-", ecolor="k",label="$\Re{}$e(Z$_{\perp,y}$$^{Tot}$)")
##pl.errorbar(totTransVert[:,0]/10**9, totTransVert[:,2]/10.0**-6/10**6, yerr = errVertImag/10**6,fmt="r-", ecolor="r", label="$\Im{}$m(Z$_{\perp,y}$$^{Tot}$)")
##pl.errorbar(totTransVertLowFreq[:,0]/10**9, totTransVertLowFreq[:,1]/10.0**-6/10**6, yerr = errVertLowFreqReal/10**6,fmt="k--", ecolor="k", label="$\Re{}$e(Z$_{\perp,y}$$^{Tot}$)")
##pl.errorbar(totTransVertLowFreq[:,0]/10**9, totTransVertLowFreq[:,2]/10.0**-6/10**6, yerr = errVertLowFreqImag/10**6,fmt="r--", ecolor="r", label="$\Im{}$m(Z$_{\perp,y}$$^{Tot}$)")

errorfill(totTransVert[:,0]/10**9, totTransVert[:,1]/10.0**-6/10**6, yerr = errVertReal/10**6, color="k", label="$\Re{}$e(Z$_{\perp,y}$$^{Tot}$)")
errorfill(totTransVert[:,0]/10**9, totTransVert[:,2]/10.0**-6/10**6, yerr = errVertImag/10**6, color="r", label="$\Im{}$m(Z$_{\perp,y}$$^{Tot}$)")
errorfill(totTransVertLowFreq[:,0]/10**9, totTransVertLowFreq[:,1]/10.0**-6/10**6, yerr = errVertLowFreqReal/10**6, color="k", label="$\Re{}$e(Z$_{\perp,y}$$^{Tot}$), Low Freq")
errorfill(totTransVertLowFreq[:,0]/10**9, totTransVertLowFreq[:,2]/10.0**-6/10**6, yerr = errVertLowFreqImag/10**6, color="r", label="$\Im{}$m(Z$_{\perp,y}$$^{Tot}$), Low Freq")

errorfill(totTransVertNewDesign[:,0]/10**9, totTransVertNewDesign[:,1]/10.0**-6/10**6, yerr = errVertRealNewDesign/10**6, color="b", label="$\Re{}$e(Z$_{\perp,y}$$^{Tot}$)")
errorfill(totTransVertNewDesign[:,0]/10**9, totTransVertNewDesign[:,2]/10.0**-6/10**6, yerr = errVertImagNewDesign/10**6, color="g", label="$\Im{}$m(Z$_{\perp,y}$$^{Tot}$)")
errorfill(totTransVertLowFreqNewDesign[:,0]/10**9, totTransVertLowFreqNewDesign[:,1]/10.0**-6/10**6, yerr = errVertRealLowFreqNewDesign/10**6, color="b", label="$\Re{}$e(Z$_{\perp,y}$$^{Tot}$), Low Freq")
errorfill(totTransVertLowFreqNewDesign[:,0]/10**9, totTransVertLowFreqNewDesign[:,2]/10.0**-6/10**6, yerr = errVertImagLowFreqNewDesign/10**6, color="g", label="$\Im{}$m(Z$_{\perp,y}$$^{Tot}$), Low Freq")

##pl.plot(totTransVertLowFreq[:,0]/10**9, abs(totTransVertLowFreq[:,1]/10.0**-6)/10**6, "k--", label="$\Re{}$e(Z$_{\perp,y}$$^{Tot}$)")
##pl.plot(totTransVertLowFreq[:,0]/10**9, abs(totTransVertLowFreq[:,2]/10.0**-6)/10**6, "r--", label="$\Im{}$m(Z$_{\perp,y}$$^{Tot}$)")
##pl.plot(totTransHorz[:,0]/10**9, (totTransHorz[:,1]/10.0**-6)/(10**6), "k-", label="$\Re{}$e(Z$_{\perp,x}$$^{Tot}$), Old Design")
##pl.plot(totTransHorz[:,0]/10**9, (totTransHorz[:,2]/10.0**-6)/10**6, "r-", label="$\Im{}$m(Z$_{\perp,x}$$^{Tot}$), Old Design")
##pl.plot(totTransHorzLowFreq[:,0]/10**9, (totTransHorzLowFreq[:,1]/10.0**-6)/(10**6), "k-")
##pl.plot(totTransHorzLowFreq[:,0]/10**9, (totTransHorzLowFreq[:,2]/10.0**-6)/10**6, "r-")

##pl.plot(totTransHorzNewDesign[:,0]/10**9, (totTransHorzNewDesign[:,1]/10.0**-6)/10**6, "b-", label="$\Re{}$e(Z$_{\perp,x}$$^{Tot}$), New Design")
##pl.plot(totTransHorzNewDesign[:,0]/10**9, (totTransHorzNewDesign[:,2]/10.0**-6)/10**6, "g-", label="$\Im{}$m(Z$_{\perp,x}$$^{Tot}$), New Design")
##pl.plot(totTransHorzLowFreqNewDesign[:,0]/10**9, (totTransHorzLowFreqNewDesign[:,1]/10.0**-6)/10**6, "b-")
##pl.plot(totTransHorzLowFreqNewDesign[:,0]/10**9, (totTransHorzLowFreqNewDesign[:,2]/10.0**-6)/10**6, "g-")

##pl.errorbar(totTransHorzLowFreq[:,0]/10**9, totTransHorzLowFreq[:,1]/10.0**-6/10**6, yerr = errHorzLowFreqReal/10**6,fmt="k--", ecolor="k", label="$\Re{}$e(Z$_{\perp,x}$$^{Tot}$)")
##pl.errorbar(totTransHorzLowFreq[:,0]/10**9, totTransHorzLowFreq[:,2]/10.0**-6/10**6, yerr = errHorzLowFreqImag/10**6,fmt="r--", ecolor="r", label="$\Im{}$m(Z$_{\perp,x}$$^{Tot}$)")

##errorfill(totTransHorz[:,0]/10**9, totTransHorz[:,1]/10.0**-6/10**6, yerr = errHorzReal/10**6, color="k", label="$\Re{}$e(Z$_{\perp,x}$$^{Tot}$), Old Design")
##errorfill(totTransHorz[:,0]/10**9, totTransHorz[:,2]/10.0**-6/10**6, yerr = errHorzImag/10**6, color="r", label="$\Im{}$m(Z$_{\perp,x}$$^{Tot}$), Old Design")
##errorfill(totTransHorzLowFreq[:,0]/10**9, totTransHorzLowFreq[:,1]/10.0**-6/10**6, yerr = errHorzLowFreqReal/10**6, color="k")
##errorfill(totTransHorzLowFreq[:,0]/10**9, totTransHorzLowFreq[:,2]/10.0**-6/10**6, yerr = errHorzLowFreqImag/10**6, color="r")

##errorfill(totTransHorzNewDesign[:,0]/10**9, totTransHorzNewDesign[:,1]/10.0**-6/10**6, yerr = errHorzReal/10**6, color="b", label="$\Re{}$e(Z$_{\perp,x}$$^{Tot}$), New Design")
##errorfill(totTransHorzNewDesign[:,0]/10**9, totTransHorzNewDesign[:,2]/10.0**-6/10**6, yerr = errHorzImag/10**6, color="g", label="$\Im{}$m(Z$_{\perp,x}$$^{Tot}$), New Design")
##errorfill(totTransHorzLowFreqNewDesign[:,0]/10**9, totTransHorzLowFreqNewDesign[:,1]/10.0**-6/10**6, yerr = errHorzLowFreqReal/10**6, color="b")
##errorfill(totTransHorzLowFreqNewDesign[:,0]/10**9, totTransHorzLowFreqNewDesign[:,2]/10.0**-6/10**6, yerr = errHorzLowFreqImag/10**6, color="g")


pl.legend(loc="upper right")
##pl.ylim(-50,200)
pl.xlim(10**-3,2)
pl.ylim(-3,5)
##pl.xlim(10**-4,2)
pl.xlabel("Frequency (GHz)", fontsize=24.0)
pl.ylabel("$(Z_{\perp})$ (M$\Omega$/m)", fontsize=24.0)
##pl.show()
pl.clf()

##

##### Section on Warming Up ##### 

resImpForPowOld=concatArrRes(impHorz[:,0],longHorz)
resImpForPowNew=concatArrRes(impHorzNewDesign[:,0],longHorzNewDesign)
powLossOldRes, powLossFreqPartOldRes, cumPowOldRes = heatingValGaussRes(resImpForPowOld, 0.5, 25*10.0**-9, 1*10.0**-9)
powLossNewRes, powLossFreqPartNewRes, cumPowNewRes = heatingValGaussRes(resImpForPowNew, 0.5, 25*10.0**-9, 1*10.0**-9)
powLossOld, powLossFreqPartOld, cumPowOld = heatingValGauss(postls130m117mm100mDat, 0.5, 25*10.0**-9, 1*10.0**-9)
powLossNew, powLossFreqPartNew, cumPowNew = heatingValGauss(postls230m70mm100mDat, 0.5, 25*10.0**-9, 1*10.0**-9)
print powLossOld, powLossNew
print powLossOldRes, powLossNewRes

pl.plot(cumPowOldRes[:,0]/10**9, cumPowOldRes[:,1])
pl.plot(cumPowNewRes[:,0]/10**9, cumPowNewRes[:,1])
pl.plot(cumPowOld[:,0], cumPowOld[:,1])
pl.plot(cumPowNew[:,0], cumPowNew[:,1])
##pl.show()
pl.clf()
