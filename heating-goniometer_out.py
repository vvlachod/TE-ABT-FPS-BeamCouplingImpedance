import scipy as sp
import pylab as pl
import time, csv, sys, os
import matplotlib.pyplot as plt
import scipy.integrate as inte

########################## Imported Profile ##################################

start = time.time()

#######   SPS parameters for LHC type beam  #######

###### Define constants ####
charge = 18.4*10**-9
sigma_z = 0.075
C = 299792458.0
eps0 = 8.854 * 10 **-12
mu0 = 4*sp.pi*10**-7
Z0 = 120*sp.pi
circ=6911.0
t_bunch = 2.5*10**-9
n_bunch = 144
Nb = 3.3*10**11
q_part = 1.6*10**-19


def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def gauss_prof(dist, sigma, order):
    return sp.e**(-order*(dist/(2*sigma))**2)

def para_prof(dist, bunch_length):
    return 1-(2*dist/bunch_length)**2

def cos_prof(dist, bunch_length):
    return sp.cos(sp.pi/bunch_length*dist)**2

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

######## Import data for time domain ##########

top_directory = "E:/PhD/1st_Year_09-10/Data/UA9_goniometer/time_domain/"
measure_stash_path = "E:/PhD/1st_Year_09-10/Data/UA9_goniometer/time_domain/"
##top_directory = "C:/Users/hugo/PhD/Data/UA9-Goniometer/impedances/"
##measure_stash_path = "C:/Users/hugo/PhD/Data/UA9-Goniometer/impedances/"

out_data = top_directory+"parked-out/bellows-ferrite/longitudinal-impedance.csv"

data_out_ua9_long_ferr = []
data_out_ua9_long_norm_ferr = []

input_file = open(out_data, 'r+')
tar = csv.reader(input_file, delimiter=",")

i=0
for row in tar:
    if i!=0:
        data_out_ua9_long_ferr.append(map(float, row))
    i+=1
input_file.close()

input_file = open(out_data, 'r+')
tar = csv.reader(input_file, delimiter=",")
i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        row_copy = row
        row_copy[1] = row_copy[1]/(row[0]*10**9/(C/circ))
        row_copy[3] = row_copy[3]/(row[0]*10**9/(C/circ))
        data_out_ua9_long_norm_ferr.append(row_copy)
    i+=1

input_file.close()
data_out_ua9_long_ferr = sp.array(data_out_ua9_long_ferr)
data_out_ua9_long_norm_ferr = sp.array(data_out_ua9_long_norm_ferr)


out_data = top_directory+"parked-out/bellows-no-ferrite/longitudinal-impedance.csv"

data_out_ua9_long_no_ferr = []
data_out_ua9_long_norm_no_ferr = []

input_file = open(out_data, 'r+')
tar = csv.reader(input_file, delimiter=",")

i=0
for row in tar:
    if i!=0:
        data_out_ua9_long_no_ferr.append(map(float, row))
    i+=1
input_file.close()

input_file = open(out_data, 'r+')
tar = csv.reader(input_file, delimiter=",")
i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        row_copy = row
        row_copy[1] = row_copy[1]/(row[0]*10**9/(C/circ))
        row_copy[3] = row_copy[3]/(row[0]*10**9/(C/circ))
        data_out_ua9_long_norm_no_ferr.append(row_copy)
    i+=1

input_file.close()
data_out_ua9_long_no_ferr = sp.array(data_out_ua9_long_no_ferr)
data_out_ua9_long_norm_no_ferr = sp.array(data_out_ua9_long_norm_no_ferr)


##pl.plot(data_out_ua9_long_norm[:,0], data_out_ua9_long_norm[:, 1], 'k-', label= "Parked out Position $\Re{}e(Z)$")
##pl.plot(data_out_ua9_long_norm[:,0], data_out_ua9_long_norm[:, 3], 'r-', label= "Parked out Position $\Im{}m(Z)$")


pl.grid(linestyle="--", which = "major")
pl.xlabel("Frequency (GHz)",fontsize = 16)                  #Label axes
pl.ylabel("Longitudinal Impedance (Z/n)$(\Omega)$",fontsize = 16)
##pl.title("", fontsize = 16)
pl.legend(loc="upper right")
##pl.axis([0,2000,-1000,3500])
##pl.savefig(directory+"test"+".pdf")                  # Save fig for film
##pl.show()
pl.clf() 


########### Import and produce data for frequency domain ###########

power_loss_tot = 0.0
top_directory_in = "E:/PhD/1st_Year_09-10/Data/UA9-Goniometer/eigenmode-results/parked_in/"
top_directory_out = "E:/PhD/1st_Year_09-10/Data/UA9_goniometer/eigenmode-results/parked_out_alt/parked_out/with-bellows-ferrite/"

eigenmode_store_ferr = [] ###### first column - freq, second - Q, Third R, Fourth - Power Loss
directory_list = [top_directory_out+i+"/" for i in os.listdir(top_directory_out)]
count = 0
try:
    os.mkdir(top_directory_in+"tex_output/")
    os.mkdir(top_directory_out+"tex_output/")

except:
    pass


for work_directory in directory_list:

    if work_directory == top_directory_out+"tex_output/":
        pass
    else:
##        print work_directory
        file_input = open(work_directory+"/e_field_on_axis.fld", 'r+')
        data = file_input.readlines()
        Ez = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ez.append([temp[0],complex(temp[-6],temp[-5])])

        temp = []
        for i in range(0,len(Ez)):
            temp.append([Ez[i][0],complex((Ez[i][1]).real,(Ez[i][1]).imag)])
        Ez = sp.array(temp)

        potential = 0.0
        potential_alt = 0.0
        gap = (Ez[1][0]-Ez[0][0])
        for i in range(0, len(Ez)-1):
            potential_alt += 0.5*abs(Ez[i+1,1]+Ez[i,1])*gap
        potential = inte.simps(abs(Ez[:,1]), Ez[:,0], gap)
        if count == 0:
            pl.plot(Ez[:,0], Ez[:,1])
##            pl.show()
            pl.clf()
            count+=1

        file_input = open(work_directory+"q_1.csv", 'r+')
        data = file_input.readlines()
        Q = []
        for i in data[1:]:
            Q.append(map(float, i.rsplit(',')))

        Q = sp.array(Q)
        ##print Q

        file_input = open(work_directory+"stored_energy_in.csv", 'r+')
        data = file_input.readlines()
        stored_energy = []
        for i in data[1:]:
            stored_energy.append(map(float, i.rsplit(',')))

        ##print stored_energy

        file_input = open(work_directory+"freq.csv", 'r+')
        data = file_input.readlines()
        freq = []
        for i in data[1:]:
            temp = map(float, i.rsplit(','))
            freq.append([temp[0],complex(temp[2],temp[1])])

        freq = sp.array(freq)

        gap = Ez[1,0]-Ez[0,0]
        confluence = []
        for i in range(0,len(Ez)):
            confluence.append(Ez[i,1]*complex(sp.cos(freq[0,1]*2*sp.pi*Ez[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ez[i,0]/C)))

        long_r_over_q = (potential**2/(2*sp.pi*freq[0,1]*stored_energy[0][1]))

        long_r_over_q_alt = abs(inte.simps(confluence, Ez[:,0], gap))**2/(2*2*sp.pi*freq[0,1]*stored_energy[0][1])
        print freq[0,1], Q[0,1], long_r_over_q_alt, long_r_over_q_alt*Q[0,1], (Nb*q_part*n_bunch*C/circ)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2)
        eigenmode_store_ferr.append([freq[0,1], Q[0,1], long_r_over_q_alt*Q[0,1], (Nb*q_part*n_bunch*C/circ)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2)])
        power_loss_tot += (Nb*q_part*n_bunch*C/circ)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2)

power_loss_tot = 0.0
top_directory_in = "E:/PhD/1st_Year_09-10/Data/UA9-Goniometer/eigenmode-results/parked_in/"
top_directory_out = "E:/PhD/1st_Year_09-10/Data/UA9_goniometer/eigenmode-results/parked_out_alt/parked_out/with-bellows-no-ferrite/"

eigenmode_store_no_ferr = [] ###### first column - freq, second - Q, Third R, Fourth - Power Loss
directory_list = [top_directory_out+i+"/" for i in os.listdir(top_directory_out)]
count = 0
try:
    os.mkdir(top_directory_in+"tex_output/")
    os.mkdir(top_directory_out+"tex_output/")

except:
    pass


for work_directory in directory_list:

    if work_directory == top_directory_out+"tex_output/":
        pass
    else:
##        print work_directory
        file_input = open(work_directory+"/e_field_on_axis.fld", 'r+')
        data = file_input.readlines()
        Ez = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ez.append([temp[0],complex(temp[-6],temp[-5])])

        temp = []
        for i in range(0,len(Ez)):
            temp.append([Ez[i][0],complex((Ez[i][1]).real,(Ez[i][1]).imag)])
        Ez = sp.array(temp)

        potential = 0.0
        potential_alt = 0.0
        gap = (Ez[1][0]-Ez[0][0])
        for i in range(0, len(Ez)-1):
            potential_alt += 0.5*abs(Ez[i+1,1]+Ez[i,1])*gap
        potential = inte.simps(abs(Ez[:,1]), Ez[:,0], gap)
        if count == 0:
            pl.plot(Ez[:,0], Ez[:,1])
##            pl.show()
            pl.clf()
            count+=1

        file_input = open(work_directory+"q_1.csv", 'r+')
        data = file_input.readlines()
        Q = []
        for i in data[1:]:
            Q.append(map(float, i.rsplit(',')))

        Q = sp.array(Q)
        ##print Q

        file_input = open(work_directory+"stored_energy_in.csv", 'r+')
        data = file_input.readlines()
        stored_energy = []
        for i in data[1:]:
            stored_energy.append(map(float, i.rsplit(',')))

        ##print stored_energy

        file_input = open(work_directory+"freq.csv", 'r+')
        data = file_input.readlines()
        freq = []
        for i in data[1:]:
            temp = map(float, i.rsplit(','))
            freq.append([temp[0],complex(temp[2],temp[1])])

        freq = sp.array(freq)

        gap = Ez[1,0]-Ez[0,0]
        confluence = []
        for i in range(0,len(Ez)):
            confluence.append(Ez[i,1]*complex(sp.cos(freq[0,1]*2*sp.pi*Ez[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ez[i,0]/C)))

        long_r_over_q = (potential**2/(2*sp.pi*freq[0,1]*stored_energy[0][1]))

        long_r_over_q_alt = abs(inte.simps(confluence, Ez[:,0], gap))**2/(2*2*sp.pi*freq[0,1]*stored_energy[0][1])
        print freq[0,1], Q[0,1], long_r_over_q_alt, long_r_over_q_alt*Q[0,1], (Nb*q_part*n_bunch*C/circ)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2)
        eigenmode_store_no_ferr.append([freq[0,1], Q[0,1], long_r_over_q_alt*Q[0,1], (Nb*q_part*n_bunch*C/circ)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2)])
        power_loss_tot += (Nb*q_part*n_bunch*C/circ)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2)



######### Plotting comparison time and frequency domain ##########

##for i in eigenmode_store:
##    print i

freq_list = []
for i in range(0,20000,1):
        freq_list.append(float(i/10.0*10.0**6))

freq_list = list(set(freq_list))
freq_list.sort()

impedance_profile_ferr = []
for i in freq_list:
    total = 0.0
    for entry in eigenmode_store_ferr:
        total+=Z_bb(i, entry)
    impedance_profile_ferr.append([total.real, total.imag])
impedance_profile_ferr=sp.array(impedance_profile_ferr)
impedance_profile_no_ferr = []
for i in freq_list:
    total = 0.0
    for entry in eigenmode_store_no_ferr:
        total+=Z_bb(i, entry)
    impedance_profile_no_ferr.append([total.real, total.imag])
impedance_profile_no_ferr=sp.array(impedance_profile_no_ferr)
freq_list = sp.array(freq_list)

pl.semilogy()
pl.plot(freq_list/10**9, impedance_profile_ferr[:,0], 'k-', label="$\Re{}e(Z)$ Freq Domain, Out, Bellows, Ferrite")
##pl.plot(freq_list/10**9, impedance_profile_ferr[:,1], 'r-', label="$\Im{}m(Z)$ Freq Domain, Out, Bellows, Ferrite")
pl.plot(freq_list/10**9, impedance_profile_no_ferr[:,0], 'k--', label="$\Re{}e(Z)$ Freq Domain, Out, Bellows, No Ferrite")
##pl.plot(freq_list/10**9, impedance_profile_no_ferr[:,1], 'r-', label="$\Im{}m(Z)$ Freq Domain, Out, Bellows, No Ferrite")
##pl.plot(data_out_ua9_long_ferr[:,0], data_out_ua9_long_ferr[:, 1], 'r-', label= "$\Re{}e(Z)$ Time Domain, Out, Bellows, Ferrite")
##pl.plot(data_out_ua9_long_ferr[:,0], data_out_ua9_long_ferr[:, 3], 'r--', label= "$\Im{}m(Z)$ Time Domain, Out, Bellows, Ferrite")
##pl.plot(data_out_ua9_long_no_ferr[:,0], data_out_ua9_long_no_ferr[:, 1], 'r--', label= "$\Re{}e(Z)$ Time Domain, Out, Bellows, No Ferrite")
##pl.plot(data_out_ua9_long_no_ferr[:,0], data_out_ua9_long_no_ferr[:, 3], 'r--', label= "$\Im{}m(Z)$ Time Domain, Out, Bellows, No Ferrite")
pl.xlabel("Frequency (GHz)", fontsize=16)
pl.ylabel("Impedance ($\Omega$)", fontsize=16)
pl.legend(loc="lower right")
pl.axis([0, 1.2, -1200, 2000])
pl.savefig(top_directory_out+"tex_output/long_imp.pdf")
pl.savefig(top_directory_out+"tex_output/long_imp.eps")
##pl.show()
pl.clf()

freq_list = []
for i in range(0,20000,1):
        freq_list.append(float(i/10.0*10.0**6))

freq_list = list(set(freq_list))
freq_list.sort()

impedance_profile_norm_ferr = []
for i in freq_list:
    total = 0.0
    for entry in eigenmode_store_ferr:
        total+=Z_bb(i, entry)
    total = total/(i/(C/circ))
    impedance_profile_norm_ferr.append([total.real, total.imag])
impedance_profile_norm_ferr=sp.array(impedance_profile_norm_ferr)
impedance_profile_norm_no_ferr = []
for i in freq_list:
    total = 0.0
    for entry in eigenmode_store_no_ferr:
        total+=Z_bb(i, entry)
    total = total/(i/(C/circ))
    impedance_profile_norm_no_ferr.append([total.real, total.imag])
impedance_profile_norm_no_ferr=sp.array(impedance_profile_norm_no_ferr)
freq_list = sp.array(freq_list)

pl.plot(freq_list/10**9, impedance_profile_norm_ferr[:,0], 'k-', label="$\Re{}e(Z)$ Freq Domain, Out, Bellows, Ferrite")
pl.plot(freq_list/10**9, impedance_profile_norm_ferr[:,1], 'r-', label="$\Im{}m(Z)$ Freq Domain, Out, Bellows, Ferrite")
pl.plot(freq_list/10**9, impedance_profile_norm_no_ferr[:,0], 'k-', label="$\Re{}e(Z)$ Freq Domain, Out, Bellows, No Ferrite")
pl.plot(freq_list/10**9, impedance_profile_norm_no_ferr[:,1], 'r-', label="$\Im{}m(Z)$ Freq Domain, Out, Bellows, No Ferrite")
pl.plot(data_out_ua9_long_norm_ferr[:,0], data_out_ua9_long_norm_ferr[:, 1], 'k--', label= "$\Re{}e(Z)$ Time Domain, Out, Bellows, Ferrite")
pl.plot(data_out_ua9_long_norm_ferr[:,0], data_out_ua9_long_norm_ferr[:, 3], 'r--', label= "$\Im{}m(Z)$ Time Domain, Out, Bellows, Ferrite")
pl.plot(data_out_ua9_long_norm_no_ferr[:,0], data_out_ua9_long_norm_no_ferr[:, 1], 'k--', label= "$\Re{}e(Z)$ Time Domain, Out, Bellows, No Ferrite")
pl.plot(data_out_ua9_long_norm_no_ferr[:,0], data_out_ua9_long_norm_no_ferr[:, 3], 'r--', label= "$\Im{}m(Z)$ Time Domain, Out, Bellows, No Ferrite")
pl.xlabel("Frequency (GHz)", fontsize=16)
pl.ylabel("Impedance Z/n ($\Omega$)", fontsize=16)
pl.legend(loc="upper left")
pl.axis([0, 1.2, -0.1, 0.1])
##pl.show()
pl.savefig(top_directory_out+"tex_output/long_imp_norm.pdf")
pl.savefig(top_directory_out+"tex_output/long_imp_norm.eps")
pl.clf()

#################### Heating Section #########################

######## Define beam parameters for parked out position ########

######### For 50ns bunch spacing  ###########

print """Heating estiates for parked out, full machine (144 bunches) with 50ns spacing
        (parabolic, cos^2, gaussian)"""

n_bunches_out = 144
possible_bunches_out = 144
p_bunch_out = 1.5*10**11
Q_part_out = 1.6*10**-19
f_rev_out = 2.0*10**7       ###### Only thing to be changed between 25ns/50ns beam
I_b_out = p_bunch_out*Q_part_out*C/circ*n_bunches_out
power_tot_out = I_b_out**2
print I_b_out, power_tot_out

mark_length = 1.1*10**-9
time_domain_parabolic_50 = []
time_domain_cos_50 = []
time_domain_gauss_50 = []
data_points = 10000.0
sample_rate = 0.1/(mark_length/data_points)
bunch_length = 4.0*10**-9

## For 50ns spacing 22.726
if f_rev_out == 2*10**7:
    t_50=pl.r_[-22.726*mark_length:22.726*mark_length:1/sample_rate]
    n_bunches = 144

for i in t_50:
    if abs(i)<=bunch_length/2:
        time_domain_parabolic_50.append(para_prof(i,bunch_length))           ## Parabolic bunch profile
        time_domain_cos_50.append(cos_prof(i,bunch_length))  ## cos^2 profile
    else:
        time_domain_parabolic_50.append(0)
        time_domain_cos_50.append(0)
    time_domain_gauss_50.append(gauss_prof(i,bunch_length,30))           ## gaussian profile

        
time_domain_parabolic_50 = sp.array(time_domain_parabolic_50)
time_domain_cos_50 = sp.array(time_domain_cos_50)
time_domain_gauss_50 = sp.array(time_domain_gauss_50)
N=len(t_50)
freq_domain_parabolic_50=sp.fft(time_domain_parabolic_50)
freq_domain_cos_50=sp.fft(time_domain_cos_50)
freq_domain_gauss_50=sp.fft(time_domain_gauss_50)
f_50 = sample_rate*sp.r_[0:(N/2)]/N
n=len(f_50)

freq_domain_parabolic_50 = freq_domain_parabolic_50[0:n]/freq_domain_parabolic_50[0]
freq_domain_cos_50 = freq_domain_cos_50[0:n]/freq_domain_cos_50[0]
freq_domain_gauss_50 = freq_domain_gauss_50[0:n]/freq_domain_gauss_50[0]

#### To dB scale    ########

freq_domain_parabolic_50_db = sp.real(20*sp.log(freq_domain_parabolic_50))
freq_domain_cos_50_db = sp.real(20*sp.log(freq_domain_cos_50))
freq_domain_gauss_50_db = sp.real(20*sp.log(freq_domain_gauss_50))

fig = plt.figure()
ax1 = fig.add_subplot(111)
pl.plot(f_50[:100]/10**9, freq_domain_parabolic_50_db[:100], 'b-', label="Parabolic")
pl.plot(f_50[:100]/10**9, freq_domain_cos_50_db[:100], 'r-', label="Cos$^{2}$")
pl.plot(f_50[:100]/10**9, freq_domain_gauss_50_db[:100], 'g-', label="Gaussian")
ax1.set_xlabel("Frequency (GHz)", fontsize = "16")
ax1.set_ylabel("S (dB)", fontsize = "16")
ax1.legend(loc = "upper right")
ax1.axis([0,2,-80,0])

ax2 = ax1.twinx()
pl.plot(freq_list/10**9, impedance_profile[:,0], 'm-', label = "Parked Out")
ax2.set_ylabel("Real Longitudinal Impedance ($\Omega$)", fontsize = "16")
ax2.legend(loc="center left")
pl.savefig(top_directory_out+"tex_output/spectra_imp"+str(bunch_length)+"_50ns.pdf")
pl.savefig(top_directory_out+"tex_output/spectra_imp"+str(bunch_length)+"_50ns.eps")
##pl.show()
pl.clf()

heating_out_para_50 = 0.0
heating_out_cos_50 = 0.0
heating_out_gauss_50 = 0.0
heating_cumulative_out_cos_50 = []


for i in range(0,len(f_50)):
    j = 0
    if f_50[i] < 2.1*10**9:
        while j<len(data_out_ua9_long) :
            if f_50[i] == 0.0:
                pass
            elif abs(f_50[i]/10**9-data_out_ua9_long[j,0])<0.0005:
                heating_out_para_50+=2*power_tot_out*abs(freq_domain_parabolic_50[i])**2*abs(data_out_ua9_long[j,1])
                heating_out_cos_50+=2*power_tot_out*abs(freq_domain_cos_50[i])**2*abs(data_out_ua9_long[j,1])
                heating_cumulative_out_cos_50.append(2*power_tot_out*abs(freq_domain_cos_50[i])**2*abs(data_out_ua9_long[j,1]))
                heating_out_gauss_50+=2*power_tot_out*abs(freq_domain_gauss_50[i])**2*abs(data_out_ua9_long[j,1])
                pass
            j+=1
            

print heating_out_para_50
print heating_out_cos_50
print heating_out_gauss_50                

heating_out_para_50_freq = 0.0
heating_out_cos_50_freq = 0.0
heating_out_gauss_50_freq = 0.0
heating_cumulative_out_cos_50_freq = []

for i in range(0,len(f_50)):
    if f_50[i] == 0.0:
        pass
    elif f_50[i] < 2.1*10**9:
        impedance_tot = 0.0
        for entry in eigenmode_store:
            impedance_tot+=Z_bb(f_50[i], entry)
        heating_out_para_50_freq+=2*power_tot_out*abs(freq_domain_parabolic_50[i])**2*impedance_tot.real
        heating_out_cos_50_freq+=2*power_tot_out*abs(freq_domain_cos_50[i])**2*impedance_tot.real
        heating_cumulative_out_cos_50_freq.append(2*power_tot_out*abs(freq_domain_cos_50[i])**2*impedance_tot.real)
        heating_out_gauss_50_freq+=2*power_tot_out*abs(freq_domain_gauss_50[i])**2*impedance_tot.real

print heating_out_para_50_freq
print heating_out_cos_50_freq
print heating_out_gauss_50_freq          
     
pl.plot(f_50[:len(heating_cumulative_out_cos_50)], heating_cumulative_out_cos_50, "k-", label="Heating Parked Out Time")
pl.plot(f_50[:len(heating_cumulative_out_cos_50_freq)], heating_cumulative_out_cos_50_freq, "r-", label="Heating Parked Out Freq")
pl.xlabel("Frequency (Hz)", fontsize = "16")
pl.ylabel("Power Loss (W)", fontsize = "16")
pl.legend(loc="upper right")
##pl.show()
pl.clf()


########## Heating for 25ns bunch spacing ###########

print """Heating estiates for parked out, full machine (288 bunches) with 25ns spacing
        (parabolic, cos^2, gaussian)"""


n_bunches_out = 288
possible_bunches_out = 288
p_bunch_out = 1.15*10**11
Q_part_out = 1.6*10**-19
f_rev_out = 4.0*10**7       ###### Only thing to be changed between 25ns/50ns beam
I_b_out = p_bunch_out*Q_part_out*C/circ*n_bunches_out
power_tot_out = I_b_out**2

print I_b_out, power_tot_out

mark_length = 1.1*10**-9
time_domain_parabolic_25 = []
time_domain_cos_25 = []
time_domain_gauss_25 = []
data_points = 10000.0
sample_rate = 0.1/(mark_length/data_points)
bunch_length = 4.0*10**-9


## For 25ns spacing
if f_rev_out == 4*10**7:
    t_25=pl.r_[-11.357*mark_length:11.357*mark_length:1/sample_rate]
    n_bunch = 144
    ##print len(t)
omega = sample_rate

for i in t_25:
    if abs(i)<=bunch_length/2:
        time_domain_parabolic_25.append(para_prof(i,bunch_length))           ## Parabolic bunch profile
        time_domain_cos_25.append(cos_prof(i,bunch_length))  ## cos^2 profile
    else:
        time_domain_parabolic_25.append(0)
        time_domain_cos_25.append(0)
    time_domain_gauss_25.append(gauss_prof(i,bunch_length,30))           ## gaussian profile

        
time_domain_parabolic_25 = sp.array(time_domain_parabolic_25)
time_domain_cos_25 = sp.array(time_domain_cos_25)
time_domain_gauss_25 = sp.array(time_domain_gauss_25)
N=len(t_25)
freq_domain_parabolic_25=sp.fft(time_domain_parabolic_25)
freq_domain_cos_25=sp.fft(time_domain_cos_25)
freq_domain_gauss_25=sp.fft(time_domain_gauss_25)
f_25 = sample_rate*sp.r_[0:(N/2)]/N
n=len(f_25)

freq_domain_parabolic_25 = freq_domain_parabolic_25[0:n]/freq_domain_parabolic_25[0]
freq_domain_cos_25 = freq_domain_cos_25[0:n]/freq_domain_cos_25[0]
freq_domain_gauss_25 = freq_domain_gauss_25[0:n]/freq_domain_gauss_25[0]

#### To dB scale    ########

freq_domain_parabolic_25_db = sp.real(20*sp.log(freq_domain_parabolic_25))
freq_domain_cos_25_db = sp.real(20*sp.log(freq_domain_cos_25))
freq_domain_gauss_25_db = sp.real(20*sp.log(freq_domain_gauss_25))

fig = plt.figure()
ax1 = fig.add_subplot(111)
pl.plot(f_25[:100]/10**9, freq_domain_parabolic_25_db[:100], 'b-', label="Parabolic")
pl.plot(f_25[:100]/10**9, freq_domain_cos_25_db[:100], 'r-', label="Cos$^{2}$")
pl.plot(f_25[:100]/10**9, freq_domain_gauss_25_db[:100], 'g-', label="Gaussian")
ax1.set_xlabel("Frequency (GHz)", fontsize = "16")
ax1.set_ylabel("S (dB)", fontsize = "16")
ax1.legend(loc = "upper right")
ax1.axis([0,2,-80,0])

ax2 = ax1.twinx()
pl.plot(freq_list/10**9, impedance_profile[:,0], 'm-', label = "Parked Out")
ax2.set_ylabel("Real Longitudinal Impedance ($\Omega$)", fontsize = "16")
ax2.legend(loc="center left")
pl.savefig(top_directory_out+"tex_output/spectra_imp"+str(bunch_length)+"_25ns.pdf")
pl.savefig(top_directory_out+"tex_output/spectra_imp"+str(bunch_length)+"_25ns.eps")

##pl.show()
pl.clf()

heating_out_para_25 = 0.0
heating_out_cos_25 = 0.0
heating_out_gauss_25 = 0.0
heating_cumulative_out_cos_25 = []

for i in range(0,len(f_25)):
    j = 0
    if f_25[i] < 2.1*10**9:
        while j<len(data_out_ua9_long) :
            if f_25[i] == 0.0:
                pass
            elif abs(f_25[i]/10**9-data_out_ua9_long[j,0])<0.0005:
                heating_out_para_25+=2*power_tot_out*abs(freq_domain_parabolic_25[i])**2*abs(data_out_ua9_long[j,1])
                heating_out_cos_25+=2*power_tot_out*abs(freq_domain_cos_25[i])**2*abs(data_out_ua9_long[j,1])
                heating_cumulative_out_cos_25.append(2*power_tot_out*abs(freq_domain_cos_25[i])**2*abs(data_out_ua9_long[j,1]))
                heating_out_gauss_25+=2*power_tot_out*abs(freq_domain_gauss_25[i])**2*abs(data_out_ua9_long[j,1])
                pass
            j+=1
            

print heating_out_para_25
print heating_out_cos_25
print heating_out_gauss_25                

heating_out_para_25_freq = 0.0
heating_out_cos_25_freq = 0.0
heating_out_gauss_25_freq = 0.0
heating_cumulative_out_cos_25_freq = []

for i in range(0,len(f_25)):
    if f_25[i] == 0.0:
        pass
    elif f_25[i] < 2.1*10**9:
        impedance_tot = 0.0
        for entry in eigenmode_store:
            impedance_tot+=Z_bb(f_25[i], entry)
        heating_out_para_25_freq+=2*power_tot_out*abs(freq_domain_parabolic_25[i])**2*impedance_tot.real
        heating_out_cos_25_freq+=2*power_tot_out*abs(freq_domain_cos_25[i])**2*impedance_tot.real
        heating_cumulative_out_cos_25_freq.append(2*power_tot_out*abs(freq_domain_cos_25[i])**2*impedance_tot.real)
        heating_out_gauss_25_freq+=2*power_tot_out*abs(freq_domain_gauss_25[i])**2*impedance_tot.real

print heating_out_para_25_freq
print heating_out_cos_25_freq
print heating_out_gauss_25_freq          

print power_loss_tot

pl.plot(f_25[:len(heating_cumulative_out_cos_25)], heating_cumulative_out_cos_25, "k-", label="Heating Parked Out Time")
pl.plot(f_25[:len(heating_cumulative_out_cos_25_freq)], heating_cumulative_out_cos_25_freq, "r-", label="Heating Parked Out Freq")
pl.xlabel("Frequency (Hz)", fontsize = "16")
pl.ylabel("Power Loss (W)", fontsize = "16")
pl.legend(loc="upper right")
##pl.show()
pl.clf()

