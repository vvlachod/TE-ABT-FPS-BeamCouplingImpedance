import sympy.mpmath as mpmath
import scipy, numpy, csv
from matplotlib import pyplot
import numbers

Z0 = 377.0
a = 0.001
b = 0.02
d = 0.080
e0 = 8.85*10**-12
eprime = 12.0
rho = 10**6
muprime = 460.0
tau = 1.0/(20.0*10**6)
er=12.0

def k(freq):
    return 2*scipy.pi*freq/3*10**8

def mur(freq):
    return (1 + (muprime/(1+1j*tau*freq)))

def kxn(n):
    return ((2*n + 1)*scipy.pi)/(2*a)

def kyn(n,freq):
    return ((er*mur(freq) - 1)*k(freq)**2 - kxn(n)**2)**0.5

def sh(n):
    return mpmath.sinh(kxn(n)*b)

def ch(n):
    return mpmath.cosh(kxn(n)*b)

def tn(n,freq):
    return mpmath.tan(kyn(n,freq)*(b-d))

def ct(n,freq):
    return mpmath.cot(kyn(n,freq)*(b-d))

def FX(n,freq):
    return (kxn(n)/k(freq))*((1+mur(freq)*er)*sh(n)*ch(n))/(mur(freq)*er-1)

def FY(n,freq):
    return (kyn(n,freq)/k(freq))*(mur(freq)*sh(n)**2*tn(n,freq) - er*ch(n)**2*ct(n,freq))/(er*mur(freq) - 1)

def ferrite_long(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (FX(n,freq) + FY(n,freq) - (k(freq)*sh(n)*ch(n)/kxn(n)))**-1, [0, mpmath.inf])

a_real=[]
a_imag=[]

mu_real_file = csv.writer(open("mu_real.tab", "w+"), delimiter = '\t',
                     lineterminator='\n')
mu_imag_file = csv.writer(open("mu_imag.tab", "w+"), delimiter = '\t',
                     lineterminator='\n')

for i in range(1,10**9,10**6):
    a_real.append(mur(i).real)
    mu_real_file.writerow([i, mur(i).real/muprime])
    a_imag.append(mur(i).imag)
    mu_imag_file.writerow([i, mur(i).imag/muprime])
pyplot.plot(a_real, a_imag)
pyplot.show()




