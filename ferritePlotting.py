import sympy.mpmath as mpmath
import numpy, csv, time
from matplotlib import pyplot
import numbers
import pylab as pl
import scipy as sp


Z0 = 377.0
a = 0.025
b = 0.005
d = b+0.005
length = 1
disp = 1
e0 = 8.85*10**-12
eprime = 12.0
rho = 10**6
muprime = 460.0
tau = 1.0/(20.0*10**6)
er=12.0
mu0 = 4*sp.pi*10**-7
fileNam = "ferrGeoJul2012"
c = 3.0*10.0**8

def k(freq):
    return 2*sp.pi*freq/(3*10**8)

def mur(freq):
    return (1 + (muprime/(1+1j*tau*freq)))

def penDepth(data, epsRe):
    penDepthList = []
    for entry in data:
        penDepthList.append(c/(2*sp.pi*entry[0]*10.0**6)*1/((1-epsRe*complex(entry[1],entry[2]))**0.5).real)
    return sp.array(penDepthList)

start = time.time()

importTT2111R = []
datFileTT2 = open("E:/PhD/1st_Year_09-10/Data/Material Properties/TT2-111R/reading_1.csv", 'r+')

datTemp = datFileTT2.readlines()
datFileTT2.close()
for entry in datTemp[1:]:
    importTT2111R.append(map(float, entry.split(',')))

importTT2111R=sp.array(importTT2111R)

import4S60 = []
datFile4S60real = open("E:/PhD/1st_Year_09-10/Data/Material Properties/4S60/4S60_mu_prime.tab", 'r+')
datFile4S60imag = open("E:/PhD/1st_Year_09-10/Data/Material Properties/4S60/4S60_mag_loss_tangent.tab", 'r+')


datTempReal = datFile4S60real.readlines()
datTempImag = datFile4S60imag.readlines()
datFile4S60real.close()
datFile4S60imag.close()
for i in range(0,len(datTempReal[1:])):
    import4S60.append([float(datTempReal[i].split('\t')[0])/(10.0**6), float(datTempReal[i].split('\t')[1]), float(datTempImag[i].split('\t')[-1])*float(datTempReal[i].split('\t')[-1])])

import4S60=sp.array(import4S60)


freq_list = []
for i in range(3,10,1):
    for j in range(1,100,1):
        freq_list.append(float((j/10.0)*10.0**i))

freq_list = list(set(freq_list))
freq_list.sort()

mu4A4=[]

for entry in freq_list:
    mu4A4.append([entry/(10.0**6), mur(entry).real, -mur(entry).imag])

mu4A4=sp.array(mu4A4)

import4E2Real = []
import4E2Imag = []
datFile4E2real = open("E:/PhD/1st_Year_09-10/Data/Material Properties/4E2/muPrime4E2.csv", 'r+')
datFile4E2imag = open("E:/PhD/1st_Year_09-10/Data/Material Properties/4E2/muDoublePrime4E2.csv", 'r+')


datTempReal = datFile4E2real.readlines()
datTempImag = datFile4E2imag.readlines()
datFile4E2real.close()
datFile4E2imag.close()
for i in range(0,len(datTempReal)):
    import4E2Real.append([float(datTempReal[i].split(',')[0]), float(datTempReal[i].split(',')[1])])

for i in range(0,len(datTempImag)):
    import4E2Imag.append([float(datTempImag[i].split(',')[0]), float(datTempImag[i].split(',')[1])])

import4E2Real=sp.array(import4E2Real)
import4E2Imag=sp.array(import4E2Imag)

importPenDep4E2 = []
datFile4E2real = open("E:/PhD/1st_Year_09-10/Data/Material Properties/4E2/muPrime4E2PenDep.csv", 'r+')
datFile4E2imag = open("E:/PhD/1st_Year_09-10/Data/Material Properties/4E2/muDoublePrime4E2PenDepth.csv", 'r+')


datTempReal = datFile4E2real.readlines()
datTempImag = datFile4E2imag.readlines()
datFile4E2real.close()
datFile4E2imag.close()
for i in range(0,len(datTempReal[1:])):
    importPenDep4E2.append([float(datTempReal[i].split(',')[0]), float(datTempReal[i].split(',')[1]), float(datTempImag[i].split(',')[-1])])

importPenDep4E2=sp.array(importPenDep4E2)

pl.semilogx()
##pl.loglog()
##pl.plot(importTT2111R[:,0], importTT2111R[:,1], "k-", label="$\mu^{'}$ TT2-111R")
##pl.plot(importTT2111R[:,0], importTT2111R[:,2], "r--", label="$\mu^{''}$ TT2-111R")
##pl.plot(import4S60[:,0], import4S60[:,1], "k-", label="$\mu^{'}$ 4S60")
##pl.plot(import4S60[:,0], import4S60[:,2], "r--", label="$\mu^{''}$ 4S60")
##pl.plot(mu4A4[:,0], mu4A4[:,1], "k-", label="$\mu^{'}$ 4A4")
##pl.plot(mu4A4[:,0], mu4A4[:,2], "r--", label="$\mu^{''}$ 4A4")
pl.plot(import4E2Real[:,0], import4E2Real[:,1], "k-", label="$\mu^{'}$ 4E2")
pl.plot(import4E2Imag[:,0], import4E2Imag[:,1], "r--", label="$\mu^{''}$ 4E2")
pl.xlabel("Frequency (MHz)", fontsize=16.0)
pl.ylabel("$\mu_{r}$", fontsize=16.0)
pl.axis([3.0, 1000, 10.0**0, 100])
pl.legend()
##pl.show()
pl.clf()

penDepth4A4 = penDepth(mu4A4, 10.0)
penDepthTT2 = penDepth(importTT2111R, 10.0)
penDepth4S60 = penDepth(import4S60, 10.0)
penDepth4E2 = penDepth(importPenDep4E2, 10.0)

pl.loglog()
##pl.semilogx()
##pl.plot(importTT2111R[:,0], penDepthTT2*10.0**3, "k-", label="Penetration Depth TT2-111R")
##pl.plot(import4S60[:,0], penDepth4S60*10.0**3, "b-", label="Penetration Depth 4S60")
##pl.plot(mu4A4[:,0], penDepth4A4*10.0**3, "r-", label="Penetration Depth 4A4")
pl.plot(importPenDep4E2[:,0], penDepth4E2*10.0**3, "b-", label="Penetration Depth 4E2")
pl.xlabel("Frequency (MHz)", fontsize=16.0)
pl.ylabel("Penetration Depth (mm)", fontsize=16.0)
pl.axis([10.0, 10.0**3, 10.0**-0, 10.0**5])
pl.legend()
pl.show()
pl.clf()


##print time.time() - start
