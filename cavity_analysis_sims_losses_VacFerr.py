import scipy as sp
import pylab as pl
import time, os, sys, csv
import scipy.integrate as inte

start = time.time()

###### Define Impedance from resonator ######

def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def extract(fileTar):
    tmpFile = open(fileTar, 'r+')
    dataIn = tmpFile.readlines()
    tmpFile.close()
    count = 0
    store = []
    for row in dataIn:
        if count > 3:
            store.append(map(float, row.rsplit()))
        count+=1

    return sp.array(store)

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp


###### Define constants ####

sigma_z = 0.075
C = 299792458.0
circ = 26679.0
eps0 = 8.854 * 10 **-12
mu0 = 4*sp.pi*10**-7
Z0 = 120*sp.pi
start=time.time()
bound_typ = "PerfE"
x01 = 0.001
x02 = 0.0011
dy = 0.001
disp_y = 0.0005
power_loss_tot_vac = 0.0
power_loss_tot = 0.0
n_bunches = 1380
Nb = 1.7*10**11
qPart = 1.6*10**-19

Ib = n_bunches*qPart*Nb*C/circ
powerTot = Ib**2

top_directory = "E:/PhD/1st_Year_09-10/Data/TCTP/eigenmode_solutions/longitudinal_eigenmodes_vacuum/"

eigenmode_store_vac = [] ###### first column - freq, second - Q, Third R, Fourth - Power Loss
eigenmode_store_transverse_Ez_vac = []
eigenmode_store_transverse_EyBx_vac = []
directory_list = [top_directory+i+"/" for i in os.listdir(top_directory)]

count = 0
for work_directory in directory_list:
    print work_directory
    if work_directory == top_directory+"tex_output/":
        pass
    elif work_directory == top_directory+"f193/":
        pass
    else:
        for pos in os.listdir(work_directory):
            if "PerfE" in pos:
                bound_typ = "PerfE"
            elif "perfE" in pos:
                bound_typ = "perfE"
            elif "PerfH" in pos:
                bound_typ = "PerfH"
            elif "perfH" in pos:
                bound_typ = "perfH"
            else:
                pass
##        print work_directory
        file_input = open(work_directory+"E_field_"+bound_typ+".fld", 'r+')
        data = file_input.readlines()
        Ez = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ez.append([temp[2],complex(temp[-2],temp[-1])])

        ##Ez = sp.array(Ez)

        ##print Ez
        if bound_typ == "PerfE" or bound_typ == "perfE":
            temp = []
            for i in range(1,len(Ez)):
                temp.append([-Ez[-i][0], complex((Ez[-i][1]).real,(Ez[-i][1]).imag)])
            for i in range(0,len(Ez)):
                temp.append([Ez[i][0],complex((Ez[i][1]).real,(Ez[i][1]).imag)])
            Ez = sp.array(temp)
        elif bound_typ == "PerfH" or "perfH":
            temp = []
            for i in range(1,len(Ez)):
                temp.append([-Ez[-i][0], complex(-(Ez[-i][1]).real,-(Ez[-i][1]).imag)])
            for i in range(0,len(Ez)):
                temp.append([Ez[i][0], complex((Ez[i][1]).real,(Ez[i][1]).imag)])
            Ez = sp.array(temp)

        ##pl.plot(Ez[:,0], Ez[:,1].real)
        ##pl.plot(Ez[:,0], Ez[:,1].imag)
        ##pl.show()
        ##pl.clf()

        gap = (Ez[1][0]-Ez[0][0])

        file_input = open(work_directory+"E_field_"+bound_typ+".fld", 'r+')
        data = file_input.readlines()
        Ey = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ey.append([temp[2],complex(temp[-4],temp[-3])])

        ##Ey = sp.array(Ey)
        ##print Ey

        if bound_typ == "PerfE" or bound_typ == "perfE":
            temp = []
            for i in range(1,len(Ey)):
                temp.append([-Ey[-i][0], complex(-(Ey[-i][1]).real,-(Ey[-i][1]).imag)])
            for i in range(0,len(Ey)):
                temp.append([Ey[i][0],complex((Ey[i][1]).real,(Ey[i][1]).imag)])
            Ey = sp.array(temp)
        elif bound_typ == "PerfH" or bound_typ == "perfH":
            temp = []
            for i in range(1,len(Ey)):
                temp.append([-Ey[-i][0], complex((Ey[-i][1]).real,(Ey[-i][1]).imag)])
            for i in range(0,len(Ey)):
                temp.append([Ey[i][0], complex((Ey[i][1]).real,(Ey[i][1]).imag)])
            Ey = sp.array(temp)

        file_input = open(work_directory+"q_1.csv", 'r+')
        data = file_input.readlines()
        Q = []
        for i in data[1:]:
            Q.append(map(float, i.rsplit(',')))

        Q = sp.array(Q)
        ##print Q

        file_input = open(work_directory+"stored_energy.csv", 'r+')
        data = file_input.readlines()
        stored_energy = []
        for i in data[1:]:
            stored_energy.append(map(float, i.rsplit(',')))

        ##print stored_energy

        file_input = open(work_directory+"freq.csv", 'r+')
        data = file_input.readlines()
        freq = []
        for i in data[1:]:
##            print i
            temp = map(float, i.rsplit(','))
            freq.append([temp[0],complex(temp[2],temp[1])])

        freq = sp.array(freq)

        if count == 68:
            pl.plot(Ez[:,0],Ez[:,1])
##            pl.show()
            pl.clf()
        count+=1

        gap = Ez[1,0]-Ez[0,0]
        confluence = []
        for i in range(0,len(Ez)):
            confluence.append(Ez[i,1]*complex(sp.cos(freq[0,1]*2*sp.pi*Ez[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ez[i,0]/C)))


        long_r_over_q_alt = abs(inte.simps(confluence, Ez[:,0], gap))*(Ez[-1,0]-Ez[0,0])/C


##        print trans_r_over_q_from_Ey_Bx/trans_r_over_q_from_dEzdy1, long_r_over_q_alt
        
    ##    print "Frequency of mode is %f" % freq[0,1]
    ##    print "Q-factor of mode is %f" % Q[0,1]
    ##    print "Transverse kicker factor k_y from dEz/dy1 (V/nC/mm) = %f" % (k_y_from_dEzdy1/10**12)
    ##    print "Transverse R/Q from dEz/dy1 (Ohms/mm) = %f" % (trans_r_over_q_from_dEzdy1/10**3)
    ##    print "Transverse Rs1 = %f (Ohms/mm)" % (trans_r_over_q_from_dEzdy1*Q[0,1]/10**3)
    ##    ##print "Transverse kicker factor k_y from  dEz/dy2 (V/nC/mm) = %f" % (k_y_from_dEzdy2)
    ##    ##print "Transverse R/Q from dEz/dy2 (Ohms/mm) = %f" % (trans_r_over_q_from_dEzdy2)
    ##    ##print "Transverse  Rs2 = %f (Ohms/mm)" % (trans_r_over_q_from_dEzdy2*Q[0,1])
    ##    print "Transverse kicker factor k_y from Ey+cBx (V/nC/mm) = %f" % (k_y_from_Ey_Bx/10**12)
    ##    print "Transverse R/Q from Ey+cBx (Ohms/mm) = %f" % (trans_r_over_q_from_Ey_Bx/10**3)
    ##    print "Transverse Rs = %f (Ohms/mm)" % (trans_r_over_q_from_Ey_Bx*Q[0,1]/10**3)
    ##    print "Longitudinal R/Q = %f" % long_r_over_q
    ##    print "Longitudinal Rs = %f (Ohms)" % (long_r_over_q*Q[0,1])
    ##    print "Longitudinal R/Q alt= %f" % long_r_over_q_alt
    ##    print "Longitudinal Rs alt= %f (Ohms)" % (long_r_over_q_alt*Q[0,1])
    ##    print "Power loss is: %f (Watts)" % ((charge/t_bunch)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2))
##        print freq[0,1], Q[0,1], long_r_over_q_alt, count
        eigenmode_store_vac.append([freq[0,1], Q[0,1], long_r_over_q_alt*Q[0,1], powerTot**long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2)])
        power_loss_tot_vac += powerTot*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2).real

top_directory = "E:/PhD/1st_Year_09-10/Data/TCTP/eigenmode_solutions/longitudinal_eigenmodes/"

eigenmode_store = [] ###### first column - freq, second - Q, Third R, Fourth - Power Loss
eigenmode_store_transverse_Ez = []
eigenmode_store_transverse_EyBx = []
directory_list = [top_directory+i+"/" for i in os.listdir(top_directory)]

for work_directory in directory_list:

    if work_directory == top_directory+"tex_output/":
        pass
    else:
        for pos in os.listdir(work_directory):
            if "PerfE" in pos:
                bound_typ = "PerfE"
            elif "perfE" in pos:
                bound_typ = "perfE"
            elif "PerfH" in pos:
                bound_typ = "PerfH"
            elif "perfH" in pos:
                bound_typ = "perfH"
            else:
                pass
##        print work_directory
        file_input = open(work_directory+"E_field_"+bound_typ+".fld", 'r+')
        data = file_input.readlines()
        Ez = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ez.append([temp[2],complex(temp[-2],temp[-1])])

        ##Ez = sp.array(Ez)

        ##print Ez
        if bound_typ == "PerfE" or bound_typ == "perfE":
            temp = []
            for i in range(1,len(Ez)):
                temp.append([-Ez[-i][0], complex((Ez[-i][1]).real,(Ez[-i][1]).imag)])
            for i in range(0,len(Ez)):
                temp.append([Ez[i][0],complex((Ez[i][1]).real,(Ez[i][1]).imag)])
            Ez = sp.array(temp)
        elif bound_typ == "PerfH" or "perfH":
            temp = []
            for i in range(1,len(Ez)):
                temp.append([-Ez[-i][0], complex(-(Ez[-i][1]).real,-(Ez[-i][1]).imag)])
            for i in range(0,len(Ez)):
                temp.append([Ez[i][0], complex((Ez[i][1]).real,(Ez[i][1]).imag)])
            Ez = sp.array(temp)

        ##pl.plot(Ez[:,0], Ez[:,1].real)
        ##pl.plot(Ez[:,0], Ez[:,1].imag)
        ##pl.show()
        ##pl.clf()

        potential = 0.0
        gap = (Ez[1][0]-Ez[0][0])

        potential = inte.simps(abs(Ez[:,1]), Ez[:,0], gap)

        file_input = open(work_directory+"E_field_"+bound_typ+".fld", 'r+')
        data = file_input.readlines()
        Ey = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ey.append([temp[2],complex(temp[-4],temp[-3])])

        ##Ey = sp.array(Ey)
        ##print Ey

        if bound_typ == "PerfE" or bound_typ == "perfE":
            temp = []
            for i in range(1,len(Ey)):
                temp.append([-Ey[-i][0], complex(-(Ey[-i][1]).real,-(Ey[-i][1]).imag)])
            for i in range(0,len(Ey)):
                temp.append([Ey[i][0],complex((Ey[i][1]).real,(Ey[i][1]).imag)])
            Ey = sp.array(temp)
        elif bound_typ == "PerfH" or bound_typ == "perfH":
            temp = []
            for i in range(1,len(Ey)):
                temp.append([-Ey[-i][0], complex((Ey[-i][1]).real,(Ey[-i][1]).imag)])
            for i in range(0,len(Ey)):
                temp.append([Ey[i][0], complex((Ey[i][1]).real,(Ey[i][1]).imag)])
            Ey = sp.array(temp)


        file_input = open(work_directory+"q_1.csv", 'r+')
        data = file_input.readlines()
        Q = []
        for i in data[1:]:
            Q.append(map(float, i.rsplit(',')))

        Q = sp.array(Q)
        ##print Q

        file_input = open(work_directory+"stored_energy.csv", 'r+')
        data = file_input.readlines()
        stored_energy = []
        for i in data[1:]:
            stored_energy.append(map(float, i.rsplit(',')))

        ##print stored_energy

        file_input = open(work_directory+"freq.csv", 'r+')
        data = file_input.readlines()
        freq = []
        for i in data[1:]:
##            print i
            temp = map(float, i.rsplit(','))
            freq.append([temp[0],complex(temp[2],temp[1])])

        freq = sp.array(freq)

        gap = Ez[1,0]-Ez[0,0]
        confluence = []
        for i in range(0,len(Ez)):
            confluence.append(Ez[i,1]*complex(sp.cos(freq[0,1]*2*sp.pi*Ez[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ez[i,0]/C)))


        ##print freq
        long_r_over_q = (potential**2/(2*sp.pi*freq[0,1]*stored_energy[0][1]))

        long_r_over_q_alt = abs(inte.simps(confluence, Ez[:,0], gap))**2/(2*2*sp.pi*freq[0,1]*stored_energy[0][1])


##        print trans_r_over_q_from_Ey_Bx/trans_r_over_q_from_dEzdy1
##        print long_r_over_q_alt/long_r_over_q
        
    ##    print "Frequency of mode is %f" % freq[0,1]
    ##    print "Q-factor of mode is %f" % Q[0,1]
    ##    print "Transverse kicker factor k_y from dEz/dy1 (V/nC/mm) = %f" % (k_y_from_dEzdy1/10**12)
    ##    print "Transverse R/Q from dEz/dy1 (Ohms/mm) = %f" % (trans_r_over_q_from_dEzdy1/10**3)
    ##    print "Transverse Rs1 = %f (Ohms/mm)" % (trans_r_over_q_from_dEzdy1*Q[0,1]/10**3)
    ##    ##print "Transverse kicker factor k_y from  dEz/dy2 (V/nC/mm) = %f" % (k_y_from_dEzdy2)
    ##    ##print "Transverse R/Q from dEz/dy2 (Ohms/mm) = %f" % (trans_r_over_q_from_dEzdy2)
    ##    ##print "Transverse  Rs2 = %f (Ohms/mm)" % (trans_r_over_q_from_dEzdy2*Q[0,1])
    ##    print "Transverse kicker factor k_y from Ey+cBx (V/nC/mm) = %f" % (k_y_from_Ey_Bx/10**12)
    ##    print "Transverse R/Q from Ey+cBx (Ohms/mm) = %f" % (trans_r_over_q_from_Ey_Bx/10**3)
    ##    print "Transverse Rs = %f (Ohms/mm)" % (trans_r_over_q_from_Ey_Bx*Q[0,1]/10**3)
    ##    print "Longitudinal R/Q = %f" % long_r_over_q
    ##    print "Longitudinal Rs = %f (Ohms)" % (long_r_over_q*Q[0,1])
    ##    print "Longitudinal R/Q alt= %f" % long_r_over_q_alt
    ##    print "Longitudinal Rs alt= %f (Ohms)" % (long_r_over_q_alt*Q[0,1])
    ##    print "Power loss is: %f (Watts)" % ((charge/t_bunch)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2))

        eigenmode_store.append([freq[0,1], Q[0,1], long_r_over_q_alt*Q[0,1], powerTot*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2)])
        power_loss_tot += powerTot*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2).real

print power_loss_tot, power_loss_tot_vac

freq_list = []
for i in range(0,20000,1):
        freq_list.append(float(i/10.0*10.0**6))

freq_list = list(set(freq_list))
freq_list.sort()

impProfFerr = []
impProfVac = []

for i in freq_list:
    totalFerr = 0.0
    totalVac = 0.0
    for entry in eigenmode_store:
        totalFerr+= Z_bb(i, entry)
    for entry in eigenmode_store_vac:
        totalVac+= Z_bb(i, entry)
    impProfFerr.append([totalFerr.real, totalFerr.imag])
    impProfVac.append([totalVac.real, totalVac.imag])    

impProfFerr = sp.array(impProfFerr)
impProfVac = sp.array(impProfVac)
freq_list = sp.array(freq_list)

dataDir = "E:/PhD/1st_Year_09-10/Data/TCTP/forUse/"
dat8c11 = sp.array(extract_dat(dataDir+"TCTP8C11.csv"))
dat4s60 = sp.array(extract_dat(dataDir+"TCTP4S60.csv"))
datclosed = sp.array(extract_dat(dataDir+"TCTPClosed.csv"))
datopen = sp.array(extract_dat(dataDir+"TCTPOpen.csv"))



##pl.semilogy()
##pl.plot(data8c11[:,0], data8c11[:,1],'r-', label = "8C11 Time")
##pl.plot(dataClosed[:,0], abs(dataClosed[:,1]),'b-', label="\Re{}e (Z_{\parallel}) Closed Time Dom")
##pl.plot(dataFerrite[:,0], abs(dataFerrite[:,1]),'g-', label="\Re{}e (Z_{\parallel}) Ferrite Time Dom")
pl.plot(dat8c11[:,0], dat8c11[:,1], label="Time Domain, Ferrite, 8C11")
pl.plot(datopen[:,0], datopen[:,1], label="Time Domain, Gap, Open")
pl.plot(freq_list/10**9, impProfFerr[:,0], 'k-', label="$\Re{}e (Z_{\parallel})$ Ferrite")
pl.plot(freq_list/10**9, impProfVac[:,0]*10**11, 'r-', label="$\Re{}e (Z_{\parallel})$ Vacuum")
pl.xlabel("Frequency (GHz)")
pl.ylabel("Impedance ($\Omega$)")
pl.legend(loc = "upper right")
pl.grid(linestyle="--", which = "major")
pl.axis([0,2,-10,300])
pl.show()
pl.clf()
