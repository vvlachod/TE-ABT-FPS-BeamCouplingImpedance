import csv, os, sys, math
import scipy as sp
import pylab as pl
from scipy import optimize as op

fitfunc = lambda p, x: p[2] + p[1]*x + p[0]*x**2
##fitfunc = lambda p, x: p[2] + p[1]*x + p[0]
errfunc = lambda p, x, y, err: (y-fitfunc(p, x))/err

lenMKI = 2.88
lenWire = 3.5356
lenElec = 3.53
C = 299792458

def importDatDip(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
##    print tempDat
    datStore = []
    for i in range(0,len(tempDat)/2):
        if i%2==0:
            temp = [float(tempDat[i].rstrip("\n"))*10.0**6, float(tempDat[i+len(tempDat)/2].rstrip("\n"))]
            datStore.append(temp)
        else:
            pass
    return datStore

def importDatSing(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
##    print tempDat
    datStore = []
    for i in range(0,len(tempDat)/2):
        temp = [float(tempDat[i].rstrip("\n"))*10.0**6, float(tempDat[i+len(tempDat)/2].rstrip("\n"))]
        datStore.append(temp)
    return datStore


def analDipRe(data, lenDUT, r_sep):
    temp = []
    for i in range(0,len(data)):
        temp.append(C/(2*sp.pi*data[i,0]*r_sep**2)*data[i,1]*lenDUT) 
    return temp

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

###########  Simulation Analysis  ############

dirFiles = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/newStepOutAll/"

longImp = sp.array(extract_dat(dirFiles+"0mm_x_0mm_y/longitudinal-impedance.csv"))

horzDipList = [dirFiles+str(i)+"mm_x_0mm_y/horizontal-dipolar-impedance.csv" for i in range(-2,3,1)]
vertDipList = [dirFiles+"0mm_x_"+str(i)+"mm_y/vertical-dipolar-impedance.csv" for i in range(-2,2,1)]
horzQuadList = [dirFiles+str(i)+"mm_x_int_0mm_y_int/horizontal-dipolar-impedance.csv" for i in range(-1,3,1)]
vertQuadList = [dirFiles+"0mm_x_int_"+str(i)+"mm_y_int/vertical-dipolar-impedance.csv" for i in range(2,3,1)]




horzDipDat = []
vertDipDat = []
horzQuadDat = []
vertQuadDat = []

for datFile in horzDipList:
    horzDipDat.append(extract_dat(datFile))    
for datFile in vertDipList:
    vertDipDat.append(extract_dat(datFile))    
for datFile in horzQuadList:
    horzQuadDat.append(extract_dat(datFile))    
for datFile in vertQuadList:
    vertQuadDat.append(extract_dat(datFile))    

horzDipDat=sp.array(horzDipDat)
vertDipDat=sp.array(vertDipDat)
horzQuadDat=sp.array(horzQuadDat)
vertQuadDat=sp.array(vertQuadDat)

xDipDisp = sp.linspace(-0.002,0.002,5)
yDipDisp = sp.linspace(-0.002,0.001,4)
xQuadDisp = sp.array([-0.001,0,0.001,0.002])
yQuadDisp = sp.array([0.002])

horzDipDip = []
horzDipConst = []
vertDipDip = []
vertDipConst = []
horzQuadQuad = []
horzQuadConst = []
vertQuadQuad = []
vertQuadConst = []

linFitFunc = lambda p, x: p[1]*x + p[0]
errFunc = lambda p, x, y, err: (y-linFitFunc(p, x))/err


for i in range(0, len(horzDipDat[1])):
    realPart = horzDipDat[:,i,1]
    imagPart = horzDipDat[:,i,3]
    pinit = [0.0, 1.0, 1.0]
    out = op.leastsq(errFunc, pinit, args=(xDipDisp,realPart,realPart), full_output=1)
    pfinal_real = out[0]
    pinit = [0.0, 1.0, 1.0]
    out = op.leastsq(errFunc, pinit, args=(xDipDisp,imagPart,imagPart), full_output=1)
    pfinal_imag = out[0]
    horzDipDip.append([horzDipDat[1,i,0], pfinal_real[1], pfinal_imag[1]])        
    horzDipConst.append([horzDipDat[1,i,0], pfinal_real[0], pfinal_imag[0]])

    realPart = vertDipDat[:,i,1]
    imagPart = vertDipDat[:,i,3]
    pinit = [0.0, 1.0, 1.0]
    out = op.leastsq(errFunc, pinit, args=(yDipDisp,realPart,realPart), full_output=1)
    pfinal_real = out[0]
    pinit = [0.0, 1.0, 1.0]
    out = op.leastsq(errFunc, pinit, args=(yDipDisp,imagPart,imagPart), full_output=1)
    pfinal_imag = out[0]
    vertDipDip.append([vertDipDat[1,i,0], pfinal_real[1], pfinal_imag[1]])        
    vertDipConst.append([vertDipDat[1,i,0], pfinal_real[0], pfinal_imag[0]])

    realPart = horzQuadDat[:,i,1]
    imagPart = horzQuadDat[:,i,3]
    pinit = [0.0, 1.0, 1.0]
    out = op.leastsq(errFunc, pinit, args=(xQuadDisp,realPart,realPart), full_output=1)
    pfinal_real = out[0]
    pinit = [0.0, 1.0, 1.0]
    out = op.leastsq(errFunc, pinit, args=(xQuadDisp,imagPart,imagPart), full_output=1)
    pfinal_imag = out[0]
    horzQuadQuad.append([horzQuadDat[1,i,0], pfinal_real[1], pfinal_imag[1]])        
    horzQuadConst.append([horzQuadDat[1,i,0], pfinal_real[0], pfinal_imag[0]])

##    realPart = vertQuadDat[:,i,1]
##    imagPart = vertQuadDat[:,i,3]
##    pinit = [0.0, 1.0, 1.0]
##    out = op.leastsq(errFunc, pinit, args=(yQuadDisp,realPart,realPart), full_output=1)
##    pfinal_real = out[0]
##    pinit = [0.0, 1.0, 1.0]
##    out = op.leastsq(errFunc, pinit, args=(yQuadDisp,imagPart,imagPart), full_output=1)
##    pfinal_imag = out[0]
##    vertQuadQuad.append([vertQuadDat[1,i,0], pfinal_real[1], pfinal_imag[1]])        
##    vertQuadConst.append([vertQuadDat[1,i,0], pfinal_real[0], pfinal_imag[0]])

horzDipDip = sp.array(horzDipDip)
horzDipConst = sp.array(horzDipConst)
vertDipDip = sp.array(vertDipDip)
vertDipConst = sp.array(vertDipConst)
horzQuadQuad = sp.array(horzQuadQuad)
horzQuadConst = sp.array(horzQuadConst)
##vertQuadQuad = sp.array(vertQuadQuad)
##vertQuadConst = sp.array(vertQuadConst)

    

pl.plot(longImp[:,0], longImp[:,1], 'k-', label="$\Re{}e(Z_{\parallel})$ Simulations")
pl.plot(longImp[:,0], longImp[:,3], 'r-', label="$\Im{}m(Z_{\parallel})$ Simulations")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\parallel}$ $(\Omega)$", fontsize=16.0)
pl.legend(loc = "upper left")
##pl.axis([0,2,-200,100])
pl.grid(True)
##pl.show()
pl.clf()


pl.plot(horzDipDip[:,0], -horzDipDip[:,1], 'k-', label="$\Re{}e(Z_{\perp, x}^{dipolar})$ Simulated")
pl.plot(horzDipDip[:,0], horzDipDip[:,2], 'r-', label="$\Im{}m(Z_{\perp, x}^{dipolar})$ Simulated")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\perp}$ $(\Omega/m)$", fontsize=16.0)
pl.legend(loc = "upper left")
pl.grid(True)
##pl.axis([0,2,-200,100])
##pl.show()
pl.clf()


pl.plot(vertDipDip[:,0], -vertDipDip[:,1], 'k-', label="$\Re{}e(Z_{\perp, y}^{dipolar})$ Simulated")
pl.plot(vertDipDip[:,0], -vertDipDip[:,2], 'r-', label="$\Im{}m(Z_{\perp, y}^{dipolar})$ Simulated")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\perp}$ $(\Omega/m)$", fontsize=16.0)
pl.legend(loc = "upper left")
pl.grid(True)
##pl.axis([0,2,0,250*10.0**3])
##pl.show()
pl.clf()



pl.plot(horzQuadQuad[:,0], horzQuadQuad[:,1], 'k-', label="$\Re{}e(Z_{\perp, x}^{quadrupolar})$ Simulated")
pl.plot(horzQuadQuad[:,0], horzQuadQuad[:,2], 'r-', label="$\Im{}m(Z_{\perp, x}^{quadrupolar})$ Simulated")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\perp}$ $(\Omega/m)$", fontsize=16.0)
pl.legend(loc = "upper left")
pl.grid(True)
##pl.axis([0,2,-200,100])
##pl.show()
pl.clf()


##pl.plot(vertQuadQuad[:,0], -vertQuadQuad[:,1], 'k-', label="$\Re{}e(Z_{\perp, y}^{quadrupolar})$ Simulated")
##pl.plot(vertQuadQuad[:,0], vertQuadQuad[:,2], 'r-', label="$\Im{}m(Z_{\perp, y}^{quadrupolar})$ Simulated")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\perp}$ $(\Omega/m)$", fontsize=16.0)
pl.legend(loc = "upper left")
pl.grid(True)
##pl.axis([0,2,-200,100])
##pl.show()
pl.clf()

pl.plot(horzDipConst[:,0], horzDipConst[:,1], 'k-', label="$\Re{}e(Z_{\perp, x}^{Const, Dip})$ Simulated")
pl.plot(horzDipConst[:,0], horzDipConst[:,2], 'r-', label="$\Im{}m(Z_{\perp, x}^{Const, Dip})$ Simulated")
##pl.plot(horzQuadConst[:,0], horzQuadConst[:,1], 'k--', label="$\Re{}e(Z_{\perp, x}^{Const, Quad})$ Simulated")
##pl.plot(horzQuadConst[:,0], horzQuadConst[:,2], 'r--', label="$\Im{}m(Z_{\perp, x}^{Const, Quad})$ Simulated")

pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\perp}$ $(\Omega)$", fontsize=16.0)
pl.legend(loc = "upper left")
pl.grid(True)
##pl.axis([0,2,-200,100])
pl.show()
pl.clf()
##
##pl.plot(vertDipConst[:,0], vertDipConst[:,1], 'k-', label="$\Re{}e(Z_{\perp}^{Const, Dip})$ Simulated")
##pl.plot(vertDipConst[:,0], vertDipConst[:,2], 'r-', label="$\Im{}m(Z_{\perp, y}^{Const, Dip})$ Simulated")
##pl.plot(vertQuadConst[:,0], vertQuadConst[:,1], 'k-.', label="$\Re{}e(Z_{\perp}^{Const, Quad})$ Simulated")
##pl.plot(vertQuadConst[:,0], vertQuadConst[:,2], 'r-', label="$\Im{}m(Z_{\perp, y}^{Const, Quad})$ Simulated")

pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\perp}$ $(\Omega)$", fontsize=16.0)
pl.legend(loc = "upper left")
pl.grid(True)
##pl.axis([0,2,-200,100])
##pl.show()
pl.clf()

