import sympy.mpmath as mpmath
import scipy, numpy, csv, time
from matplotlib import pyplot
import numbers
import pylab as pl

Z0 = 377.0
a = 0.02725
b = 0.027
d = b+0.0415
length = 1
disp = 1
e0 = 8.85*10**-12
eprime = 12.0
rho = 10**6
muprime = 460.0
tau = 1.0/(20.0*10**6)
er=12.0
mu0 = 4*scipy.pi*10**-7

def k(freq):
    return 2*scipy.pi*freq/(3*10**8)

def mur(freq):
    return (1 + (muprime/(1+1j*tau*freq)))

def kxn(n):
    return ((2*n + 1)*scipy.pi)/(2*a)

def kyn(n,freq):
    return ((er*mur(freq) - 1)*k(freq)**2 - kxn(n)**2)**0.5

def sh(n):
    return mpmath.sinh(kxn(n)*b)

def ch(n):
    return mpmath.cosh(kxn(n)*b)

def tn(n,freq):
    return mpmath.tan(kyn(n,freq)*(b-d))

def ct(n,freq):
    return mpmath.cot(kyn(n,freq)*(b-d))

def FX(n,freq):
    return (kxn(n)/k(freq))*((1+(mur(freq)*er))*sh(n)*ch(n))

def FY(n,freq):
    return (kyn(n,freq)/k(freq))*(mur(freq)*(sh(n)**2)*tn(n,freq) - er*(ch(n)**2)*ct(n,freq))

def FY_2(n,freq):
    return (kyn(n,freq)/k(freq))*(mur(freq)*(ch(n)**2)*tn(n,freq) - er*(sh(n)**2)*ct(n,freq))

def ferrite_long(freq):
    return (1j*Z0)/(2*a) * (mpmath.nsum(lambda n: (((FX(n,freq) +FY(n,freq))/(er*mur(freq)-1)) - ((k(freq)*sh(n)*ch(n))/kxn(n)))**(-1), [0,10]))

def ferrite_dip_horz(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY(n,freq))/(er*mur(freq)-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def ferrite_dip_vert(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY_2(n,freq))/(er*mur(freq)-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def ferrite_quad_horz(freq):
    return (-1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY(n,freq))/(er*mur(freq)-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def ferrite_quad_vert(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY(n,freq))/(er*mur(freq)-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])


start = time.time()


temp_real = []
temp_imag = []

freq_list = []
for i in range(0,10,1):
    for j in range(1,100,1):
        freq_list.append(float((j/10.0)*10.0**i))

freq_list = list(set(freq_list))
freq_list.sort()
freq_list=scipy.array(freq_list)
for i in freq_list:
    temp_real.append(mur(i).real)
    temp_imag.append(mur(i).imag)

temp_real = scipy.array(temp_real)
temp_imag = scipy.array(temp_imag)

pl.semilogx()
pl.plot(freq_list/10**9, temp_real,"k-",label="$\mu_{r}^{'}$")
pl.plot(freq_list/10**9, -temp_imag,"r--",label="$\mu_{r}^{''}$")
pl.xlabel("Frequency (GHz)", fontsize=18.0)
pl.ylabel("$\mu_{r}$", fontsize=18.0)
pl.axis([10**-3,10,0,500])
pl.legend(loc="upper right", prop={'size':18})
pl.show()
pl.clf()
##
##
##output = open('mki-long.csv', 'w+')
##for i in range(0,len(temp_real)):
##              output.write(str(freq_list[i])+","+str(temp_real[i])+','+str(temp_imag[i])+'\n')
##
##output.close()
##pl.clf()
####pl.loglog()
##
##pl.plot(freq_list,temp_real, color='black')
##pl.plot(freq_list,temp_imag, color='red')
##pl.xlabel("Frequency (MHz)")                  #Label axes
##pl.ylabel("Impedance ($\Omega/m$)")
##pl.savefig('mki-long.png')
##
##x= []
##temp_real = []
##temp_imag = []
##
##for i in freq_list:
##    temp_real.append(ferrite_dip_vert(i).real*length*disp)
##    temp_imag.append(ferrite_dip_vert(i).imag*length*disp)
##
##
##output = open('mki_vert_dip_data.csv', 'w+')
##for i in range(0,len(temp_real)):
##              output.write(str(freq_list[i])+","+str(temp_real[i])+','+str(temp_imag[i])+'\n')
##output.close()
##
##
##pl.clf()
##pl.plot(freq_list,temp_real, color='black')
##pl.plot(freq_list,temp_imag, color='red')
##pl.xlabel("Frequency (MHz)")                  #Label axes
##pl.ylabel("Impedance (Ohms/m/m)")
##pl.savefig('mki_dip_vert_theo.png')
##
##x= []
##temp_real = []
##temp_imag = []
##
##for i in freq_list:
##    temp_real.append(ferrite_dip_horz(i).real*length*disp)
##    temp_imag.append(ferrite_dip_horz(i).imag*length*disp)
##
##
##output = open('mki_horz_dip_data.csv', 'w+')
##for i in range(0,len(temp_real)):
##              output.write(str(freq_list[i])+","+str(temp_real[i])+','+str(temp_imag[i])+'\n')
##output.close()
##
##pl.clf()
##pl.plot(freq_list,temp_real, color='black')
##pl.plot(freq_list,temp_imag, color='red')
##pl.xlabel("Frequency (MHz)")                  #Label axes
##pl.ylabel("Impedance (Ohms/m/m)")
##pl.savefig('mki_dip_horz_theo.png')
##
##x= []
##temp_real = []
##temp_imag = []
##
##for i in freq_list:
##    temp_real.append(ferrite_quad_horz(i).real*length*disp)
##    temp_imag.append(ferrite_quad_horz(i).imag*length*disp)
##
##
##output = open('mki_horz_quad_data.csv', 'w+')
##for i in range(0,len(temp_real)):
##              output.write(str(freq_list[i])+","+str(temp_real[i])+','+str(temp_imag[i])+'\n')
##output.close()
##
##pl.clf()
##pl.plot(freq_list,temp_real, color='black')
##pl.plot(freq_list,temp_imag, color='red')
##pl.xlabel("Frequency (MHz)")                  #Label axes
##pl.ylabel("Impedance (Ohms/m/m)")
##pl.savefig('mki_quad_horz_theo.png')
##
##x= []
##temp_real = []
##temp_imag = []
##
##for i in freq_list:
##    temp_real.append(ferrite_quad_vert(i).real*length*disp)
##    temp_imag.append(ferrite_quad_vert(i).imag*length*disp)
##
##
##output = open('mki_vert_quad_data.csv', 'w+')
##for i in range(0,len(temp_real)):
##              output.write(str(freq_list[i])+","+str(temp_real[i])+','+str(temp_imag[i])+'\n')
##output.close()
##
##
##pl.clf()
##pl.plot(freq_list,temp_real, color='black')
##pl.plot(freq_list,temp_imag, color='red')
##pl.xlabel("Frequency (MHz)")                  #Label axes
##pl.ylabel("Impedance (Ohms/m/m)")
##pl.savefig('mki_quad_vert_theo.png')
##
##x= []
##temp_real = []
##temp_imag = []
##
##for i in freq_list:
##    temp_real.append((ferrite_dip_vert(i)+ferrite_quad_vert(i)).real*length*disp)
##    temp_imag.append((ferrite_dip_vert(i)+ferrite_quad_vert(i)).imag*length*disp)
##
##output = open('mki_vert_total_data.csv', 'w+')
##for i in range(0,len(temp_real)):
##              output.write(str(freq_list[i])+","+str(temp_real[i])+','+str(temp_imag[i])+'\n')
##
##output.close()
##
##
##
##pl.clf()
##pl.plot(freq_list,temp_real, color='black')
##pl.plot(freq_list,temp_imag, color='red')
##pl.xlabel("Frequency (MHz)")                  #Label axes
##pl.ylabel("Impedance (Ohms/m)")
##pl.savefig('mki_total_vert.png')
##
##x= []
##temp_real = []
##temp_imag = []
##
##for i in freq_list:
##    temp_real.append((ferrite_dip_horz(i)+ferrite_quad_horz(i)).real*length*disp)
##    temp_imag.append((ferrite_dip_horz(i)+ferrite_quad_horz(i)).imag*length*disp)
##
##
##output = open('mki_horz_total_data.csv', 'w+')
##for i in range(0,len(temp_real)):
##              output.write(str(freq_list[i])+","+str(temp_real[i])+','+str(temp_imag[i])+'\n')
##
##output.close()
##
##pl.clf()
##pl.plot(freq_list,temp_real, color='black')
##pl.plot(freq_list,temp_imag, color='red')
##pl.xlabel("Frequency (MHz)")                  #Label axes
##pl.ylabel("Impedance (Ohms/m)")
##pl.savefig('mki_total_horz.png')

print time.time() - start
