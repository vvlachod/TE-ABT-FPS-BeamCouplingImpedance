import csv, time
import scipy as sp
import pylab as pl

start = time.time()

gamma = 27.7286
v_rf = 6.0*10**5
h = 4620
circ = 6911.0
C = 3.0*10**8
f_rev = C/circ
tune= 0.00324
omega = tune*2*sp.pi*f_rev
e = 1.602176487*10**-19
m_pro = 1.672621637 *10**-27
p_0 = gamma*m_pro*C
k_h = 2*sp.pi*h/circ
T_0 = circ/C
alpha_bt = 0.00068
alpha_at = 0.00192
eta_bt = 1/(gamma**2) - alpha_bt
eta_at = 1/(gamma**2) - alpha_at
cos_phi_bt = sp.cos(0)        #### Non-accelerating bucket with maximum momentum acceptance below transition
cos_phi_at = sp.cos(sp.pi)       #### Non-accelerating bucket with maximum momentum acceptance above transition


directory = "E:/PhD/1st_Year_09-10/Software/headtail/data_dump/hdtl-data/" #Directory of data
directory_other = "E:/PhD/1st_Year_09-10/Software/headtail/data_dump/test/" 
list_bt_no_sc = [directory+"hdtl-bt-no-s.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_bt_no_bb =  [directory+"hdtl-bt-no-BB.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_bt_no_bb_1 =  [directory+"hdtl-bt-no-BB-1.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_bt_no_bb_1000 =  [directory+"hdtl-bt-no-BB-1000.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_bt_sc = [directory+"hdtl-bt-s.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_bt_sc_200 = [directory+"hdtl-bt-sc-200.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_bt_sc_5 = [directory+"hdtl-bt-sc-5.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_bt_sc_1000 = [directory+"hdtl-bt-sc-1000.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_at_no_sc = [directory+"hdtl-at-no-sc.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_no_bb =  [directory+"hdtl-at-no-BB.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_at_no_bb_1 =  [directory+"hdtl-at-no-BB-1.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_at_no_bb_1000 =  [directory+"hdtl-at-no-BB-1000.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_at_sc = [directory+"hdtl-at-s.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_at_sc_1000 = [directory+"hdtl-at-sc-1000.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_at_sc_5 = [directory+"hdtl-at-sc-5.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_at_sc_10 = [directory+"hdtl-at-sc-10.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_no_bb_matched =  [directory+"hdtl-at-no-BB-matched.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_bt_no_bb_matched =  [directory+"hdtl-bt-no-BB-matched.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]


list_bt_no_sc_matched_lin = [directory+"hdtl-bt-no-sc-matched-lin.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_bt_no_bb_matched_lin =  [directory+"hdtl-bt-no-BB-matched-lin.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_bt_sc_matched_lin = [directory+"hdtl-bt-sc-matched-lin.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_no_sc_matched_lin = [directory+"hdtl-at-no-sc-matched-lin.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_no_bb_matched_lin =  [directory+"hdtl-at-no-BB-matched-lin.NumPar."+str(i)+"_prt.dat" for i in range(1,61)]
list_at_sc_matched_lin = [directory+"hdtl-at-sc-matched-lin.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]


metral_at_bb = directory+"e_hdtl.txt"

value_desired = 9
##
##data_list_1 = []
##for data_raw in list_bt_no_sc:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_no_sc=sp.array(data_list_1)
##
######
######
##data_list_1 = []
##for data_raw in list_bt_sc:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_sc=sp.array(data_list_1)
########

##data_list_1 = []
##for data_raw in list_bt_sc_200:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_sc_200=sp.array(data_list_1) 
##
####
##data_list_1 = []
##for data_raw in list_bt_sc_5:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_sc_5=sp.array(data_list_1)
##
####
##data_list_1 = []
##for data_raw in list_bt_sc_1000:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_sc_1000=sp.array(data_list_1) 

##
data_list_1 = []
for data_raw in list_bt_no_bb:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    if value_desired == 9 or 10 or 13 or 16:
        data_list_1.append(append_tar[value_desired])
    elif value_desired == 20:
        data_list_1.append(append_tar[9]*append_tar[10]) 


bt_no_bb=sp.array(data_list_1)

data_list_1 = []
for data_raw in list_bt_no_bb_1:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    if value_desired == 9 or 10 or 13 or 16:
        data_list_1.append(append_tar[value_desired])
    elif value_desired == 20:
        data_list_1.append(append_tar[9]*append_tar[10]) 


bt_no_bb_1=sp.array(data_list_1)

##data_list_1 = []
##for data_raw in list_bt_no_bb_1000:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##
##
##bt_no_bb_1000=sp.array(data_list_1)

##data_list_1 = []
##for data_raw in list_bt_no_bb:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    append_tar = []
##    for row in linelist:
##        temp = map(float, row.split())
##        append_tar.append(temp[value_desired])
##    data_list_1.append(sp.average(append_tar))
####    print sp.average(append_tar)
##
##
##bt_no_bb_half=sp.array(data_list_1)

##
data_list_1 = []
for data_raw in list_at_no_bb:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    if value_desired == 9 or 10 or 13 or 16:
        data_list_1.append(append_tar[value_desired])
    elif value_desired == 20:
        data_list_1.append(append_tar[9]*append_tar[10]) 
    
at_no_bb=sp.array(data_list_1)
##
data_list_1 = []
for data_raw in list_at_no_bb_1:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    if value_desired == 9 or 10 or 13 or 16:
        data_list_1.append(append_tar[value_desired])
    elif value_desired == 20:
        data_list_1.append(append_tar[9]*append_tar[10]) 


at_no_bb_1=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_at_no_bb_1000:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##
##
##at_no_bb_1000=sp.array(data_list_1)


##
##data_list_1 = []
##for data_raw in list_at_no_sc:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##at_no_sc=sp.array(data_list_1)

##
######
data_list_1 = []
for data_raw in list_at_sc:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    if value_desired == 9 or 10 or 13 or 16:
        data_list_1.append(append_tar[value_desired])
    elif value_desired == 20:
        data_list_1.append(append_tar[9]*append_tar[10]) 
    
at_sc=sp.array(data_list_1)
##
##
##data_list_1 = []
##for data_raw in list_at_sc_1000:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##at_sc_1000=sp.array(data_list_1)
##

##data_list_1 = []
##for data_raw in list_at_sc_5:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##at_sc_5=sp.array(data_list_1)
####
##
##data_list_1 = []
##for data_raw in list_at_sc_10:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##at_sc_10=sp.array(data_list_1)




##
####
######data_list_1 = []
######input_file = open(metral_at_bb, 'r+')
######linelist = input_file.readlines()
######input_file.close()
######temp=[]
######for line in linelist:
######    line = line.rstrip('\n')
######    line = map(float, line.split(' '))
######    line[0] = float(line[0])*10
######    temp.append(line)
####
####
######
######metral_at=sp.array(temp)
####
x_values = sp.array(range(1,301))/10.0
x_val_new = range(1,61)
for i in range(0,len(x_val_new)):
    x_val_new[i] = float(x_val_new[i])/2

x_val_new = sp.array(x_val_new)

name_str = "_AT_BB_MATCHEE_NONMATCHED"
####
######
##pl.plot(x_values, bt_no_sc[:], 'k-', label = "Below Transition - NoSC/BB")                    # plot fitted curve
##pl.plot(x_val_new, bt_sc[:],'m-', label = "Below Transition - SC/BB")
##pl.plot(x_val_new, bt_sc_200[:],'g-', label = "Below Transition 200 - SC/BB")
##pl.plot(x_val_new, bt_sc_5[:],'r--', label = "Below Transition 5 - SC/BB")
##pl.plot(x_val_new, bt_sc_1000[:],'k--', label = "Below Transition 1000 - SC/BB")
pl.plot(x_val_new, bt_no_bb[:],'k.', label = "Below Transition - SC/noBB")
pl.plot(x_val_new, bt_no_bb_1[:],'k--', label = "Below Transition 1 - SC/noBB")
##pl.plot(x_val_new, bt_no_bb_1000[:],'k-', label = "Below Transition 1000 - SC/noBB")
##pl.plot(x_val_new, bt_no_bb_matched[:],'k.', label = "Below Transition - SC/noBB")
##pl.plot(x_values, at_no_sc[:], 'r-', label = "Above Transition - NoSC/BB")                    # plot fitted curve
##pl.plot(x_val_new, at_sc[:],'b-', label = "Above Transition - SC/BB")
##pl.plot(x_val_new, at_sc_1000[:],'r--', label = "Above Transition 1000 - SC/BB")
##pl.plot(x_val_new, at_sc_5[:],'r--', label = "Above Transition 5 - SC/BB")
##pl.plot(x_values, at_sc_10[:],'g--', label = "Above Transition 10 - SC/BB")
pl.plot(x_val_new, at_no_bb[:],'r.', label = "Above Transition 1.37 - SC/noBB")
pl.plot(x_val_new, at_no_bb_1[:],'r--', label = "Above Transition 1 - SC/noBB")
##pl.plot(x_val_new, at_no_bb_1000[:],'r-', label = "Above Transition 1000 - SC/noBB")
##pl.plot(x_values, bt_no_sc_matched_lin[:], 'k-', label = "Below Transition Matched Lin - NoSC/BB")                    # plot fitted curve
##pl.plot(x_values, bt_sc_matched_lin[:],'r-', label = "Below Transition Matched Lin - SC/BB")
##pl.plot(x_val_new, bt_no_bb_matched_lin[:],'k.', label = "Below Transition Matched Lin - SC/noBB")
##pl.plot(x_val_new, bt_no_bb_matched[:],'k.', label = "Below Transition Matched Lin - SC/noBB")
##pl.plot(x_val_new, at_no_bb_matched[:],'k.', label = "Below Transition Matched Lin - SC/noBB")
##pl.plot(x_values, at_no_sc_matched_lin[:], 'm-', label = "Above Transition Matched Lin - NoSC/BB")                    # plot fitted curve
##pl.plot(x_values, at_sc_matched_lin[:],'k-', label = "Above Transition Matched Lin - SC/BB")
##pl.plot(x_val_new, at_no_bb_matched_lin[:],'r.', label = "Above Transition Matched Lin - SC/noBB")
####
##
##
##
pl.grid(linestyle="--", which = "major")
pl.xlabel("Bunch Intensity ($10^{10}$)",fontsize = 16)                  #Label axes
if value_desired == 9:
    pl.ylabel("RMS Bunch Lenth ($\sigma_{z}$) (m)",fontsize = 16)
elif value_desired == 10:
    pl.ylabel("Momentum Spread $\delta{}p/p$",fontsize = 16)
elif value_desired == 20 or 13 or 16:
    pl.ylabel("RMS Longitudinal Emittance $\epsilon_{l}$ (eVs)",fontsize = 16)
pl.title("", fontsize = 16)
pl.legend(loc = "upper left")
##pl.axis([0,15,0.2,0.8])
if value_desired == 9:
    pl.savefig(directory+"rms_bunch_length"+name_str+".pdf")
    pl.savefig(directory+"rms_bunch_length"+name_str+".eps")
    pl.savefig(directory+"rms_bunch_length"+name_str+".png")
elif value_desired == 10:
    pl.savefig(directory+"rms_momentum_spred"+name_str+".pdf")
    pl.savefig(directory+"rms_momentum_spred"+name_str+".eps")
    pl.savefig(directory+"rms_momentum_spred"+name_str+".png")
elif value_desired == 20 or 13 or 16:
    pl.savefig(directory+"rms_longitudinal_emittance"+name_str+".pdf")
    pl.savefig(directory+"rms_longitudinal_emittance"+name_str+".eps")
    pl.savefig(directory+"rms_longitudinal_emittance"+name_str+".png")
    
pl.show()
pl.clf()   


print time.time() - start


