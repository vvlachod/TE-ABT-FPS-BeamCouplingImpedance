import csv, os, sys
import scipy as sp
import pylab as pl

n_bunches = 2808
p_bunch = 1.1*10**11
Q_part = 1.6*10**-19
f_rev = 4*10**7

I_b = p_bunch*Q_part*f_rev
print I_b, I_b**2
power_tot = I_b**2

##I_b = 1
##power_tot = I_b**2
##
##print I_b, power_tot

t_0 = 0.6*10**-9
data_points = 1000.0
sample_rate = 0.1/(t_0/data_points)

t=pl.r_[-21*t_0:21*t_0:1/sample_rate]
##print len(t)
omega = sample_rate
s = []
for i in t:
    if abs(i)<=t_0:
        s.append(sp.cos(2*sp.pi*0.25/t_0*i)**2)
    else:
        s.append(0)
s = sp.array(s)

N=len(t)
S=sp.fft(s)
f = sample_rate*sp.r_[0:(N/2)]/N
n=len(f)

##pl.plot(t,s)
##pl.axis([-2.5*t_0,2.5*t_0,0,1])
##pl.xlabel("Time (s)")
##pl.ylabel("Magnitude")
##pl.show()
##pl.clf()

f=f/10**9
curr = abs(S[0:n])/abs(S[0])
S=(abs(S[0:n]))**2/(abs(S[0]))**2
##
##pl.plot(t,s)
##pl.show()
##pl.clf()
##
##pl.plot(f/10**9,(abs(S[0:n]))**2/(abs(S[0]))**2)
##pl.plot(f,S[0:n],label='Cos$^{2}$ fit $t _{b} = 1.2ns$ - Power')
##pl.plot(f,curr,label='Cos$^{2}$ fit $t _{b} = 1.2ns$ - Current')
##########pl.plot(x_test_prof, lin_prof_fit, label='Linear Measured')
##pl.axis([0,4,0,1])
##pl.xlabel('Frequency (GHz)')
##pl.ylabel('Magnitude')
##pl.legend(loc="upper right")
##pl.show()
##pl.clf()

total=0.0
convolution_cos = []
running_total_cos = []
total_curr = 0.0


print (f[1]-f[0])
for i in range(0,len(f)-1):
##    j = (float(f[i])+((float(f[1])-float(f[0]))/2))/10**9
    j = float(f[i])

    if j<20.0: #and (j*1000)%40 == 0.0:
##        print j
##        convolution_cos.append([j,power_tot*1*S[i]*(f[1]-f[0])/10**3])
        convolution_cos.append([j,power_tot*2*1*S[i]])
        total+=power_tot*2*1*S[i]
        total_curr+=curr[i]*I_b*(f[1]-f[0])*25/(t_0*10**9)
        running_total_cos.append(total)
##        print total
##for i in range(0,len(convolution_cos)):
##    total+=convolution_cos[i][1]


convolution_cos=sp.array(convolution_cos)
##pl.plot(convolution_cos[:,0],running_total_cos[:])
##pl.xlabel('Frequency (GHz)')
##pl.ylabel("Integrated Power (W/m)")
##pl.axis([0,3,0,50])
##pl.show()
##pl.clf()


##print running_total, running_total_cos
print total
##print total_curr

