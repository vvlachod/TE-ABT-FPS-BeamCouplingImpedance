import csv, os, sys
import scipy as sp
import pylab as pl


def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp


disp = 0.001
length = 2.88
f_rev = 3*10**8/27000.0
directory = "E:/PhD/1st_Year_09-10/Data/Thesis_data/beam_impedance_reduction_techniques/tapers/data/" #Directory of data
dat15DegTaper = sp.array(extract_dat(directory+"15degree_taper_longitudinal.csv"))
dat45DegTaper = sp.array(extract_dat(directory+"45degree_taper_longitudinal.csv"))
dat90DegTaper = sp.array(extract_dat(directory+"pillbox_longitudinal.csv"))

##pl.plot(dat15DegTaper[:,0], dat15DegTaper[:,1], "b-", label="15$^{\circ}$ Taper $\Re{}e(Z_{\parallel})$")
##pl.plot(dat15DegTaper[:,0], dat15DegTaper[:,3], "b--", label="15$^{\circ}$ Taper $\Im{}m(Z_{\parallel})$")
pl.plot(dat15DegTaper[:,0], dat15DegTaper[:,3]/(10**9*dat15DegTaper[:,0]), "b--", label="15$^{\circ}$ Taper $\Im{}m(Z_{\parallel})$")
##pl.plot(dat45DegTaper[:,0], dat45DegTaper[:,1], "r-", label="45$^{\circ}$ Taper $\Re{}e(Z_{\parallel})$")
##pl.plot(dat45DegTaper[:,0], dat45DegTaper[:,3], "r--", label="45$^{\circ}$ Taper $\Im{}m(Z_{\parallel})$")
pl.plot(dat45DegTaper[:,0], dat45DegTaper[:,3]/(10**9*dat45DegTaper[:,0]), "r--", label="45$^{\circ}$ Taper $\Im{}m(Z_{\parallel})$")
##pl.plot(dat90DegTaper[:,0], dat90DegTaper[:,1], "k-", label="90$^{\circ}$ Taper $\Re{}e(Z_{\parallel})$")
##pl.plot(dat90DegTaper[:,0], dat90DegTaper[:,3], "k--", label="90$^{\circ}$ Taper $\Im{}m(Z_{\parallel})$")
pl.plot(dat90DegTaper[:,0], dat90DegTaper[:,3]/(10**9*dat90DegTaper[:,0]), "k--", label="90$^{\circ}$ Taper $\Im{}m(Z_{\parallel})$")

pl.grid(linestyle="--", which = "major")
pl.xlabel("Frequency (GHz)", fontsize = 16)                  #Label axes
##pl.ylabel("$(Z_{\parallel})$ $(\Omega)$", fontsize = 16)
pl.ylabel("$(Z_{\parallel}/n)$ $(\Omega)$", fontsize = 16)
##pl.ylabel("$\Im{}m (Z_{\parallel})$ $(\Omega)$",fontsize = 16)
##pl.title("", fontsize = 16)
pl.legend(loc="upper left")
##pl.axis([0,2.0,0,200])
pl.axis([0,0.5,0,10**-7])
##pl.savefig(directory+"test"+".pdf")                  # Save fig for film
pl.show()
pl.clf()   
