import scipy as sp
import pylab as pl
import os, sys

class ClosestDict(dict):
    def get(self, key):
        key=min(self.iterkeys(), key=lambda x:abs(x-key))
        return dict.get(self,key)
    
mu0 = 4*sp.pi*10.0**-7
muCopper = 1.0
muDoubleCopper = 1.0
condCopper = 1.0*10.0**7
rIn = 0.0005
rOut = 0.010
lenCable=0.0

directXToT = ClosestDict({0.0: 1.0, 0.5: 0.9998, 0.6: 0.9997, 0.7:0.9994, 0.8:0.9989,
              0.9:0.9983, 1.0:0.9974, 1.1:0.9962, 1.2:0.9946, 1.3:0.9927,
              1.4:0.9902, 1.5:0.9871, 1.6:0.9834, 1.7:0.9790, 1.8:0.9739,
              1.9:0.9680, 2.0:0.9611, 2.2:0.9448, 2.4:0.9248, 2.6:0.9013,
              2.8:0.8745, 3.0:0.8452, 3.2:0.8140, 3.4:0.7818, 3.6:0.7493,
              3.8:0.7173, 4.0:0.6863, 4.2:0.6568, 4.4:0.6289, 4.6:0.6028,
              4.8:0.5785, 5.0:0.556, 5.2:0.5351, 5.4:0.5157, 5.6:0.4976,
              5.8:0.4809, 6.0:0.4652, 6.2:0.4506, 6.4:0.4368, 6.6:0.4239,
              6.8:0.4117, 7.0:0.4002, 7.2:0.3893, 7.4:0.3790, 7.6:0.3692,
              7.8:0.3599, 8.0:0.3511, 8.2:0.3426, 8.4:0.3346,8.6:0.3269,
              8.8:0.3196, 9.0:0.3126, 9.2:0.3058, 9.4:0.2994, 9.6:0.2932,
              9.8:0.2873, 10.0:0.2816, 10.5:0.2682, 11.0:0.2562, 11.5:0.2452,
              12.0:0.2350, 12.5:0.2257, 13.0:0.2170, 13.5:0.2090, 14.0:0.2016,
              14.5:0.1947, 15.0:0.1882, 16.0:0.1765, 17.0:0.1661, 18.0:0.1589,
              19.0:0.1487, 20.0:0.1413, 21.0:0.1346, 22.0:0.1285, 23.0:0.1229,
              24.0:0.1178, 25.0:0.1131, 26.0:0.1087, 28.0:0.1010, 30.0:0.942,
              32.0:0.0884, 34.0:0.0832, 36.0:0.0785, 38.0:0.0744, 40.0:0.0707,
              42.0:0.0673, 44.0:0.0643, 46.0:0.0615, 48.0:0.0589, 50.0:0.0566,
              60.0:0.0471, 70.0:0.0404, 80.0:0.0354, 90.0:0.0314, 100.0:0.0283
              })


def skinDepth(freq, conductivity, mur, epsr):
    return (1/(conductivity*sp.pi*freq*mur*mu0))**0.5

def inductCoax(freq, rInner, rOuter, cond):
    return 2*10**(-4)*sp.log((2*rOuter+ skinDepth(freq, cond, 1, 1))/(2*rInner+ skinDepth(freq, cond, 1, 1)))

def inductInnerCond(freq, rInner, lenCoax, cond, muPrime, muDouble):
    return 2*lenCoax*(sp.log(2*lenCoax/rInner)-1+(muDouble/4)*directXToT.get(2*sp.pi*rInner*(2*muPrime*mu0*freq/cond)**0.5))

def inductOuterCond(rOuter, lenCoax):
    return 2*lenCoax*(sp.log(2*lenCoax/rOuter)-1)

def inductCoaxLine(freq, rInner, rOuter, cond, muPrime):
##    print sp.log(rOuter/rInner), directXToT.get(2*sp.pi*rInner*(2*muPrime*freq*cond)**0.5)
    return 2*(sp.log(rOuter/rInner) + 0.25*directXToT.get(2*sp.pi*rInner*(2*mu0*muPrime*freq*cond)**0.5))

def fileInductRead(targetFile):
    fileRead=open(targetFile, 'r+')

    listData = fileRead.readlines()
    fileRead.close()
    listCurrents = []
    listStoredEnergyElectric=[]
    listStoredEnergyMagnetic=[]
    for entry in listData:
        if entry.startswith("Total current (integral J ds)  "):
            tidbit=entry.replace("Total current (integral J ds)                    = ","")
            listCurrents.append(float(tidbit.replace(" [A]", "")))
        elif entry.startswith("Stored energy/unit length (integral A.J/2 ds)"):
            tidbit=entry.replace("Stored energy/unit length (integral A.J/2 ds)    = ", "")
            listStoredEnergyElectric.append(float(tidbit.replace(" [J/mm]", ""))*10.0**3)
        elif entry.startswith("Stored energy/unit length (integral B.H/2 ds) "):
            tidbit=entry.replace("Stored energy/unit length (integral B.H/2 ds)    = ", "")
            listStoredEnergyMagnetic.append(float(tidbit.replace(" [J/mm]", ""))*10.0**3)
        else:
            pass

    tempCurrents = []
    tempStoredEnergyElectric=[]
    tempStoredEnergyMagnetic=[]

    for i in range(0,len(listStoredEnergyMagnetic)-1,2):
        tempCurrents.append(listCurrents[i]+listCurrents[i+1])
        tempStoredEnergyElectric.append((listStoredEnergyElectric[i]**2+listStoredEnergyElectric[i+1]**2)**0.5)
        tempStoredEnergyMagnetic.append((listStoredEnergyMagnetic[i]**2+listStoredEnergyMagnetic[i+1]**2)**0.5)


    arrCurrents=sp.array(tempCurrents)
    arrStoredEnergyElectric=sp.array(tempStoredEnergyElectric)
    arrStoredEnergyMagnetic=sp.array(tempStoredEnergyMagnetic)
    listInductances = []
##    print arrStoredEnergyMagnetic

    #Calculate inductances using U=0.5 I^2 L U and I multiplied by 4 for quarter geometry

    geoFact=2.0
    for i in range(0,len(arrCurrents)):
        listInductances.append([arrStoredEnergyElectric[i]*geoFact*2/(geoFact*arrCurrents[i])**2,arrStoredEnergyMagnetic[i]*4*2/(4*arrCurrents[i])**2])

    arrInductances=sp.array(listInductances)

    return arrInductances

def inductOutput(array, freq, fileName):
    output=open(fileName, 'w+')
    for i in range(0,len(array)):
        output.write(str(freq[i])+','+str(array[i,1])+'\n')
    output.close()

def lowFreqCircuitParam(array, freq, lower, upper):
    induct=(2*sp.pi*freq[lower])*(2*sp.pi*freq[upper])*array[lower,1]*array[upper,1]*((2*sp.pi*freq[lower])**2-(2*sp.pi*freq[upper])**2)/(2*sp.pi*freq[lower]*array[lower,1]*(2*sp.pi*freq[lower])**2*(2*sp.pi*freq[upper])-2*sp.pi*freq[upper]*array[upper,1]*(2*sp.pi*freq[upper])**2*(2*sp.pi*freq[lower]))
    res=((2*sp.pi*array[upper,1]*(2*sp.pi*induct)**2)/abs((2*sp.pi*induct)-(2*sp.pi*array[upper,1])))**0.5
    print freq[upper], freq[lower]
    return res, induct-array[-1,1]

def highFreqCircuitParam(array, freq, lower, upper):
    res=array[lower,0]*array[upper,0]*(freq[upper]**2-freq[lower]**2)/(array[lower,0]*freq[upper]**2-array[upper,0]*freq[lower]**2)
    induct = 1/(2*sp.pi*freq[upper])*(array[upper,0]*res**2/abs(res-array[upper,0]))**0.5
    return res, induct

####### crossSection24ConductorsNothingMore ########

homeDir = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/ferriteTorroidHeating/crossSection24ConductorsAndThatsAll/"

centralCond = fileInductRead(homeDir+"logCentralConductorOnly.lp")
individualScreenConductorsFile = [homeDir+"logScreenCondReg"+str(i)+".lp" for i in range(3,15)]
datIndividualScreenCond = []
for fileTar in individualScreenConductorsFile:
    datIndividualScreenCond.append(fileInductRead(fileTar))

cenCondCond14CenEn = fileInductRead(homeDir+"centralCondCond14Cond1En.lp")
cenCondCond14Cond14En = fileInductRead(homeDir+"centralCondCond14Cond14En.lp")
cenCondCond14BothEn = fileInductRead(homeDir+"centralCondCond14Both.lp")
cenCondCond13CenEn = fileInductRead(homeDir+"centralCondCond13CenCond.lp")
cenCondCond13Cond13En = fileInductRead(homeDir+"centralCondCond13Cond13.lp")
cenCondCond13BothEn = fileInductRead(homeDir+"centralCondCond14Both.lp")



cenCondCond14CenEn = fileInductRead(homeDir+"cenCondAllCondEqualCenOnly.lp")
cenCondCond14AllEn = fileInductRead(homeDir+"cenCondAllCondEqualAll.lp")
cenCondCond14Cond14En = fileInductRead(homeDir+"cenCondAllCondEqualCond14.lp")
cond14Cond14En = fileInductRead(homeDir+"cond14EnCond14.lp")
cond13Cond13En = fileInductRead(homeDir+"cond13EnCond13.lp")

freqList=[]
for i in range(1,10):
    for j in range(1,9):
        freqList.append(i*10.0**j/10)

freqList=list(set(freqList))
freqList.sort()
freqList=sp.array(freqList)


pl.loglog()
##pl.semilogx()
##pl.plot(freqList, centralCond[:,1]*10**9)
pl.plot(freqList, centralCond[:,0]*10**9, label="cenCondOnly")
pl.plot(freqList, datIndividualScreenCond[-1][:,1]*10**9, label="Cond14Only Total Induct")
pl.plot(freqList, datIndividualScreenCond[-2][:,1]*10**9, label="Cond13Only Total Induct")
pl.plot(freqList, cenCondCond14CenEn[:,0]*10**9, label="cenCondCond14 Cen")
pl.plot(freqList, cenCondCond14Cond14En[:,0]*10**9, label="cenCondCond14 Cond14")
pl.plot(freqList, cenCondCond13CenEn[:,0]*10**9, label="cenCondCond13 Cen")
pl.plot(freqList, cenCondCond13Cond13En[:,0]*10**9, label="cenCondCond13 Cond13")
pl.plot(freqList, cenCondCond14AllEn[:,0]*10**9, label="cenCondCond14 All")


##pl.plot(freqList, cond14Cond14En[:,0]*10**9, label="Cond14 Cond14 Self Induct")
##pl.plot(freqList, datIndividualScreenCond[-1][:,0]*10**9-cond14Cond14En[:,0]*10**9, label="Cond14 Cond14 Mutual Induct")
##pl.plot(freqList, cond13Cond13En[:,0]*10**9, label="Cond13 Cond13 Self Induct")
##pl.plot(freqList, datIndividualScreenCond[-2][:,0]*10**9-cond13Cond13En[:,0]*10**9, label="Cond13 Cond13 Mutual Induct")
##pl.plot(freqList, centralCond[:,0]*10**9-cenCondCond14CenEn[:,0]*10**9, label="cenCondCond14 Cen")
##pl.plot(freqList, cenCondCond14AllEn[:,0]*10**9, label="cenCondCond14 All")
##pl.plot(freqList, cenCondCond14BothEn[:,0]*10**9, label="cenCondCond14 All")
##for entry in datIndividualScreenCond:
##    pl.plot(freqList, entry[:,1]*10**9)

pl.legend(loc="lower left")
pl.xlabel("Frequency (Hz)")
pl.ylabel("Inductance (nH)")
##pl.axis([10**0,10**9,0,500])
##pl.show()
pl.clf()

freqList=[]
for i in range(1,10):
    for j in range(1,7):
        freqList.append(i*10.0**j/10)
freqList=list(set(freqList))
freqList.sort()
freqList=sp.array(freqList)

inductCoaxTest = inductCoaxLine(freqList, 0.5*10.0**-3, 5*10.0**-3, 10**6, 1)
pl.loglog()
pl.plot(freqList, inductCoaxTest*10**9)
pl.show()
pl.clf()


