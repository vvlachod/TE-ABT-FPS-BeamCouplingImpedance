import csv, os, sys
import scipy as sp
import pylab as pl

##t_0 = 1.0*10**-9
t_0 = sp.linspace(0.5*10**-9, 1.2*10**-9,8)
print t_0
data_points = 5000
data_s = []
data_S = []
data_t = []
list_f = []
list_n = []
for j in t_0:
    sample_rate = 0.1/(j/data_points)
    print sample_rate
    t=pl.r_[-10*j:10*j:1/sample_rate]
    print len(t)
    data_t.append(t)
    omega = sample_rate
    s = []
    for i in t:
        if abs(i)<=j:
            s.append(sp.cos(2*sp.pi*0.25/j*i)**2)
        else:
            s.append(0)
    s = sp.array(s)
    N=len(t)
    S=sp.fft(s)
    f = sample_rate*sp.r_[0:(N/2)]/N
    n = len(f)
    list_n.append(n)
    list_f.append(f)
    data_s.append(s)
    data_S.append(list(S))


##pl.plot(data_t[:][0],data_s[:][0],label="t = 0.5ns")
##pl.plot(data_t[:][1],data_s[:][1],label="t = 0.6ns")
##pl.plot(data_t[:][2],data_s[:][2],label="t = 0.7ns")
##pl.plot(data_t[:][3],data_s[:][3],label="t = 0.8ns")
##pl.plot(data_t[:][4],data_s[:][4],label="t = 0.9ns")
##pl.plot(data_t[:][5],data_s[:][5],label="t = 1.0ns")
##pl.plot(data_t[:][6],data_s[:][6],label="t = 1.1ns")
##pl.plot(data_t[:][7],data_s[:][7],'k-.',label="t = 1.2ns")
##pl.legend()
##pl.axis([-1.5*10**-9,1.5*10**-9,0,1.5])
##pl.xlabel('Time (s)')
##pl.ylabel('Magnitude')
##pl.show()
##pl.clf()

pl.plot(list_f[0]/10**9,abs(sp.array(data_S[0][:list_n[0]]))/(abs(data_S[0][0])),label="t = 0.5ns")
pl.plot(list_f[1]/10**9,abs(sp.array(data_S[1][:list_n[1]]))/(abs(data_S[1][0])),label="t = 0.6ns")
pl.plot(list_f[2]/10**9,abs(sp.array(data_S[2][:list_n[2]]))/(abs(data_S[2][0])),label="t = 0.7ns")
pl.plot(list_f[3]/10**9,abs(sp.array(data_S[3][:list_n[3]]))/(abs(data_S[3][0])),label="t = 0.8ns")
pl.plot(list_f[4]/10**9,abs(sp.array(data_S[4][:list_n[4]]))/(abs(data_S[4][0])),label="t = 0.9ns")
pl.plot(list_f[5]/10**9,abs(sp.array(data_S[5][:list_n[5]]))/(abs(data_S[5][0])),label="t = 1.0ns")
pl.plot(list_f[6]/10**9,abs(sp.array(data_S[6][:list_n[6]]))/(abs(data_S[6][0])),label="t = 1.1ns")
pl.plot(list_f[7]/10**9,abs(sp.array(data_S[7][:list_n[7]]))/(abs(data_S[7][0])),'k-.',label="t = 1.2ns")
pl.legend()
pl.axis([0,3,0,1])
pl.xlabel('Frequency (GHz)')
pl.ylabel('Magnitude')
pl.show()
pl.clf()

##pl.plot(list_f[0]/10**9,abs(sp.array(data_S[0][:list_n[0]]))**2/(abs(data_S[0][0]))**2,label="t = 0.8ns")
##pl.plot(list_f[1]/10**9,abs(sp.array(data_S[1][:list_n[1]]))**2/(abs(data_S[1][0]))**2,label="t = 0.9ns")
##pl.plot(list_f[2]/10**9,abs(sp.array(data_S[2][:list_n[2]]))**2/(abs(data_S[2][0]))**2,label="t = 1.0ns")
##pl.plot(list_f[3]/10**9,abs(sp.array(data_S[3][:list_n[3]]))**2/(abs(data_S[3][0]))**2,label="t = 1.1ns")
##pl.plot(list_f[4]/10**9,abs(sp.array(data_S[4][:list_n[4]]))**2/(abs(data_S[4][0]))**2,label="t = 1.2ns")
##pl.legend()
##pl.axis([0,2,0,1])
##pl.xlabel('Frequency (GHz)')
##pl.ylabel('Magnitude')
##pl.show()
##pl.clf()

total=0.0
convolution_cos = []

##for i in range(0,len(f)):
##    j = float(f[i])/10**9
##    if j<1.8:
##        convolution_cos.append([j,power_tot*sp.polyval(poly_coeffs_imp,j)*S[i].real/10**9])
##        total+=power_tot*sp.polyval(poly_coeffs_imp,j)*S[i]**2*(f[1]-f[0])/10**9

##for i in range(0,len(convolution_cos)):
##    total+=convolution_cos[i][1]

print total

####    Imported Spectra #####

directory_prof = "E:/PhD/1st_Year_09-10/Data/mki-heating/longitudinal-profile/"
directory_imp = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/new-plots/"

impedance_values = []

data=open(directory_imp+"longitudinal.csv", 'r+')
linelist = data.readlines()
data.close()
for row in linelist[:-3]:
    row=map(float, row.rsplit(','))
    impedance_values.append([row[0]/1000, row[1]])

impedance_values=sp.array(impedance_values)

poly_coeffs_imp = sp.polyfit(impedance_values[:,0],impedance_values[:,1],25)
x_test = sp.linspace(0,2,1000)
imp_fit = sp.polyval(poly_coeffs_imp,x_test)

##pl.plot(impedance_values[:,0],impedance_values[:,1],'r+')
##pl.plot(x_test,imp_fit,'b-')
##pl.axis([0,2,0,50])
##pl.show()
##pl.clf()

prof_values = []
data=open(directory_prof+"long_bunch_power_spectrum",'r+')
linelist=data.readlines()
data.close()
for row in linelist[2:]:
    row=map(float,row.rsplit(','))
    prof_values.append(row)

prof_values=sp.array(prof_values)

poly_coeffs_prof = sp.polyfit(prof_values[:,0],prof_values[:,1],70)
x_test_prof = sp.linspace(0,3,3000)
prof_fit = sp.polyval(poly_coeffs_prof, x_test_prof)
lin_prof = 10**(prof_fit/20)/(10**(prof_fit[10]/20))

##pl.plot(list_f[0]/10**9,abs(sp.array(data_S[0][:list_n[0]]))**2/(abs(data_S[0][0]))**2,label="t = 0.5ns")
##pl.plot(list_f[1]/10**9,abs(sp.array(data_S[1][:list_n[1]]))**2/(abs(data_S[1][0]))**2,label="t = 0.6ns")
##pl.plot(list_f[2]/10**9,abs(sp.array(data_S[2][:list_n[2]]))**2/(abs(data_S[2][0]))**2,label="t = 0.7ns")
##pl.plot(list_f[3]/10**9,abs(sp.array(data_S[3][:list_n[3]]))**2/(abs(data_S[3][0]))**2,label="t = 0.8ns")
##pl.plot(list_f[4]/10**9,abs(sp.array(data_S[4][:list_n[4]]))**2/(abs(data_S[4][0]))**2,label="t = 0.9ns")
##pl.plot(list_f[5]/10**9,abs(sp.array(data_S[5][:list_n[5]]))**2/(abs(data_S[5][0]))**2,label="t = 1.0ns")
##pl.plot(list_f[6]/10**9,abs(sp.array(data_S[6][:list_n[6]]))**2/(abs(data_S[6][0]))**2,label="t = 1.1ns")
##pl.plot(list_f[7]/10**9,abs(sp.array(data_S[7][:list_n[7]]))**2/(abs(data_S[7][0]))**2,'k-.',label="t = 1.2ns")
##pl.plot(x_test_prof, lin_prof, label = 'Meas R. Steinhagen')
##pl.legend()
##pl.axis([0,2.5,0,1.2])
##pl.xlabel('Frequency (GHz)')
##pl.ylabel('Magnitude')
##pl.show()
##pl.clf()
