import csv, os, sys, time
import scipy as sp
import pylab as pl
import matplotlib.pyplot as plt

def gauss_prof(dist, sigma, order):
    return sp.e**(-order*(dist/sigma)**2)

def para_prof(dist, bunch_length):
    return 1-(2*dist/bunch_length)**2

def cos_prof(dist, bunch_length):
    return sp.cos(sp.pi/bunch_length*dist)**2

def impedance(res, w_meas):
    return res[1]/(complex(1,res[0]*(res[2]/w_meas - w_meas/res[2])))

start = time.time()

C = 299792458.0
circ=26659.0
p_bunch = 1.15*10**11
Q_part = 1.6*10**-19
f_rev = 2*10**7


resArr = [[1,2.3*10**5, 1.0*10**9]]

freq_list = []
for i in range(6,10,1):
    for j in range(1,10000,1):
        freq_list.append(float((j/1000.0)*10.0**i))

freq_list = list(set(freq_list))
freq_list.sort()

res_imp = []
for i in freq_list:
    res_temp = 0.0
    for res in resArr:
        res_temp+=impedance(res, i)
    res_imp.append([res_temp.real, res_temp.imag])

res_imp = sp.array(res_imp)

##pl.loglog()
print resArr[0][2]

for i in range(0,2*resArr[0][2], f_rev):
    pl.axvline(i/10.0**9)
pl.plot(sp.array(freq_list)/10**9, res_imp[:,0], 'k-', label="$\Re{}e (Z_{\parallel})$")
pl.plot(sp.array(freq_list)/10**9, res_imp[:,1], 'r-', label="$\Im{}m (Z_{\parallel})$")
pl.axis([0*10**9,1.8,-0.25*10**6,0.25*10**6])
pl.xlabel("Frequency (GHz)", fontsize="16")
pl.ylabel("$Z_{\parallel}$ ($\Omega$)", fontsize="16")
pl.legend(loc="upper left")
pl.show()
pl.clf()

I_b = p_bunch*Q_part*f_rev
power_tot = I_b**2#*n_bunches

print I_b, power_tot


mark_length = 1.1*10**-9
time_domain_parabolic = []
time_domain_cos = []
time_domain_gauss = []
time_domain_gauss_trunc = []
data_points = 10000.0
sample_rate = 0.1/(mark_length/data_points)
bunch_length = 1.5*10**-9

## For 50ns spacing 22.726
if f_rev == 2*10**7:
    t=pl.r_[-22.726*mark_length:22.726*mark_length:1/sample_rate]
    n_bunches = 1768

## For 25ns spacing
elif f_rev == 4*10**7:
    t=pl.r_[-11.357*mark_length:11.357*mark_length:1/sample_rate]
    n_bunch = 3578
    ##print len(t)
omega = sample_rate

for i in t:
    if abs(i)<=bunch_length/2:
        time_domain_parabolic.append(para_prof(i,bunch_length))           ## Parabolic bunch profile
        time_domain_cos.append(cos_prof(i,bunch_length))  ## cos^2 profile
        time_domain_gauss_trunc.append(gauss_prof(i,bunch_length,20))           ## gaussian profile
    else:
        time_domain_parabolic.append(0)
        time_domain_cos.append(0)
        time_domain_gauss_trunc.append(0)
    time_domain_gauss.append(gauss_prof(i,bunch_length,30))           ## gaussian profile

        
time_domain_parabolic = sp.array(time_domain_parabolic)
time_domain_cos = sp.array(time_domain_cos)
time_domain_gauss = sp.array(time_domain_gauss)
time_domain_gauss_trunc = sp.array(time_domain_gauss_trunc)
N=len(t)
freq_domain_parabolic=sp.fft(time_domain_parabolic)
freq_domain_cos=sp.fft(time_domain_cos)
freq_domain_gauss=sp.fft(time_domain_gauss)
freq_domain_gauss_trunc=sp.fft(time_domain_gauss_trunc)
f = sample_rate*sp.r_[0:(N/2)]/N
n=len(f)

freq_domain_parabolic = freq_domain_parabolic[0:n]/freq_domain_parabolic[0]
freq_domain_cos = freq_domain_cos[0:n]/freq_domain_cos[0]
freq_domain_gauss = freq_domain_gauss[0:n]/freq_domain_gauss[0]
freq_domain_gauss_trunc = freq_domain_gauss_trunc[0:n]/freq_domain_gauss_trunc[0]

freq_domain_parabolic = sp.real(20*sp.log(freq_domain_parabolic))
freq_domain_cos = sp.real(20*sp.log(freq_domain_cos))
freq_domain_gauss = sp.real(20*sp.log(freq_domain_gauss))
freq_domain_gauss_trunc = sp.real(20*sp.log(freq_domain_gauss_trunc))

pl.plot(t, time_domain_parabolic, 'k-', label="Parabolic")
pl.plot(t, time_domain_cos, 'r-', label="cos$^{2}$")
pl.plot(t, time_domain_gauss, 'b-', label="Gaussian")
pl.plot(t, time_domain_gauss_trunc, 'g-', label="Truncated Gaussian")
pl.legend(loc="upper left")
pl.axis([-2*bunch_length, 2*bunch_length, 0, 1])
##pl.show()
pl.clf()

##pl.semilogy()
pl.plot(f, freq_domain_parabolic, 'k-',label="Parabolic")
pl.plot(f, freq_domain_cos, 'r-', label="cos$^{2}$")
pl.plot(f, freq_domain_gauss, 'b-', label="Gaussian")
pl.plot(f, freq_domain_gauss_trunc, 'g-', label="Truncated Gaussian")
pl.legend(loc="upper right")
pl.axis([0, 5*10**9, -100, 0])
##pl.show()
pl.clf()

power_loss_total_para = 0.0
power_loss_freq_para = []
power_loss_total_cos = 0.0
power_loss_freq_cos = []
power_loss_total_gauss = 0.0
power_loss_freq_gauss = []
power_loss_total_gauss_trunc = 0.0
power_loss_freq_gauss_trunc = []

for i in range(0,len(f)):
    power_loss=0.0
    for res in resArr:
        power_loss = 2*power_tot*10**(freq_domain_parabolic[i]/40)*impedance(res, f[i]).real
    power_loss_total_para+=power_loss
    power_loss_freq_para.append(power_loss)

    power_loss=0.0
    for res in resArr: 
        power_loss = 2*power_tot*10**(freq_domain_cos[i]/40)*impedance(res, f[i]).real
    power_loss_total_cos+=power_loss
    power_loss_freq_cos.append(power_loss)

    power_loss=0.0
    for res in resArr:
        power_loss = 2*power_tot*10**(freq_domain_gauss[i]/40)*impedance(res, f[i]).real
    power_loss_total_gauss+=power_loss
    power_loss_freq_gauss.append(power_loss)

    power_loss=0.0
    for res in resArr:
        power_loss = 2*power_tot*10**(freq_domain_gauss_trunc[i]/40)*impedance(res, f[i]).real
    power_loss_total_gauss_trunc+=power_loss
    power_loss_freq_gauss_trunc.append(power_loss)

##pl.semilogy()
pl.plot(f, power_loss_freq_para, 'k-',label="Parabolic")
pl.plot(f, power_loss_freq_cos, 'r-', label="cos$^{2}$")
pl.plot(f, power_loss_freq_gauss, 'b-', label="Gaussian")
pl.plot(f, power_loss_freq_gauss_trunc, 'g-', label="Truncated Gaussian")
pl.legend(loc="upper right")
pl.axis([0, 5*10**9, 0, 10000])
##pl.show()
pl.clf()

fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.plot(freq_list, res_imp[:,0], 'm-', label = "$\Re{}e \left( Z_{parallel} \right)$")
##ax1.plot(freq_list, res_imp[:,1])
##plt.legend(loc="lower left")
ax1.axis([0*10**9,2.0*10**9,0,0.3*10**6])
ax1.set_xlabel("Hz")
ax1.set_ylabel("$Z_{\parallel}$ ($\Omega$)")
##pl.show()

ax2 = ax1.twinx()
ax2.plot(f, freq_domain_parabolic, 'k-',label="Parabolic")
ax2.plot(f, freq_domain_cos, 'r-', label="cos$^{2}$")
ax2.plot(f, freq_domain_gauss, 'b-', label="Gaussian")
ax2.plot(f, freq_domain_gauss_trunc, 'g-', label="Truncated Gaussian")
plt.legend(loc="upper right")
ax2.set_xlabel("Hz")
ax2.set_ylabel("Current Amplitude (dB)")
ax2.axis([0, 2*10**9, -100, 0])
##plt.show()
plt.clf()


print power_loss_total_para
print power_loss_total_cos
print power_loss_total_gauss
print power_loss_total_gauss_trunc

print "Time Taken for code to run = %f (s)" % (time.time()-start)
