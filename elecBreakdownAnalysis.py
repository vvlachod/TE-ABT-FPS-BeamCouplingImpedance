import csv, os, sys, time, datetime
import scipy as sp
import pylab as pl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.patches as mpatches
import scipy.optimize as op

def peak_finder(xValues, yValues):
    listPeaks=[]
    pastHighestY = 0.0
    pastHighestX = 0.0
    i=0
    while i <len(yValues):
        if yValues[i]>pastHighestY:
            pastHighestY=yValues[i]
            pastHighestX=i
            i+=1
        elif yValues[i]<pastHighestY:
            listPeaks.append([pastHighestX, xValues[pastHighestX], yValues[pastHighestX]])
            pastHighestY=0.0
            while yValues[i+1]<yValues[i]:
                i+=1
            pastHighestX=i
        else:
            i+=1

    return listPeaks

###### Defining coversion factors for PFN voltage in SIMI tank to MKI PFN voltage ########

SIMIVoltToPFNTunnelVolt = 0.7*2
## Metallized with 24 staggered conductors and 56mm tube
FieldMetal24Cond56MMtUBE = SIMIVoltToPFNTunnelVolt/60.0 * 17.5
FieldMetal24Cond56MMtUBERadial = SIMIVoltToPFNTunnelVolt/60.0 * 5.3
## Metallized with 24 staggered conductors and 53mm tube
FieldMetal24Cond53MMtUBE = SIMIVoltToPFNTunnelVolt/60.0 * 18.5
FieldMetal24Cond53MMtUBERadial = SIMIVoltToPFNTunnelVolt/60.0 * 4.7
## Offset with 24 staggered conductors and 56mm tube
FieldOffset24CondStaggered = SIMIVoltToPFNTunnelVolt/60.0 * 11.6
FieldOffset24CondStaggeredRadial = SIMIVoltToPFNTunnelVolt/60.0 * 3.7
## Offset with 24 tapered conductors, 56mm tube, and the metal cylinder rotated 180 degrees
FieldOffsetRotatedTapering24Cond = SIMIVoltToPFNTunnelVolt * 9.1/6.9
FieldOffsetRotatedTapering24CondRadial = SIMIVoltToPFNTunnelVolt/60.0 * 2.7



targetFile56mmOffset = "E:/PhD/1st_Year_09-10/Data/elecBreakdownData/MKI simi - 56mm OD/OffsetTube_56mm_PositivePulse"
targetFileMetallizedEndMarch = "E:/PhD/1st_Year_09-10/Data/elecBreakdownData/Offset Cylinder/Metalized Conditioning 25.03.2013 to 29.03.2013" 
targetFileOffsetFeb = "E:/PhD/1st_Year_09-10/Data/elecBreakdownData/Offset Cylinder/OffsetCylinderFeb.csv" 
##targetFile56mmNewDesignInvertedBackPressure = "E:/PhD/1st_Year_09-10/Data/elecBreakdownData/SIMITankPressureTests/24-10-13_To_30-10-13_BackgrndPressure/SimiTankTestsNormalVac15kVTo40kVPFN_24-10-13To30-10-13Test.csv"
targetFile56mmNewDesignInvertedBackPressure = "E:/PhD/1st_Year_09-10/Data/elecBreakdownData/SIMITankPressureTests/24-10-13_To_30-10-13_BackgrndPressure/Test1.csv"
targetFile56mmNewDesignInverted1e9Pressure = "E:/PhD/1st_Year_09-10/Data/elecBreakdownData/SIMITankPressureTests/VacHVData1e-9.csv"
targetFile56mmNewDesignInverted1e8Pressure = "E:/PhD/1st_Year_09-10/Data/elecBreakdownData/SIMITankPressureTests/VacHVData1e-8.csv"
targetFile56mmNewDesignInverted1e7Pressure = "E:/PhD/1st_Year_09-10/Data/elecBreakdownData/SIMITankPressureTests/Attempt31e-007mbar.csv"

dates56mmOffset, dataPressure56mmOffset, dataVolt56mmOffset, dataPulse56mmOffset = sp.loadtxt(
    targetFile56mmOffset, delimiter=';', converters={
    0: mdates.strpdate2num("%m/%d/%Y %I:%M:%S %p")}, skiprows=1, usecols=(0,1,3,9),
    unpack=True)

datesMetallizedEndMarch, dataPressureMetallizedEndMarch, dataVoltMetallizedEndMarch = sp.loadtxt(
    targetFileMetallizedEndMarch, delimiter=';', converters={
    0: mdates.strpdate2num("%m/%d/%Y %I:%M:%S %p")}, skiprows=1, usecols=(0,1,3),
    unpack=True)

datesOffsetFeb, dataPressureOffsetFeb, dataVoltOffsetFeb = sp.loadtxt(
    targetFileOffsetFeb, delimiter=',', converters={
    0: mdates.strpdate2num("%d/%m/%Y %H:%M:%S")}, skiprows=1, usecols=(0,1,3),
    unpack=True)

datesNewDesignInvertedBackPressure, dataPressureNewDesignInvertedBackPressure, dataVoltNewDesignInvertedBackPressure = sp.loadtxt(
    targetFile56mmNewDesignInvertedBackPressure, delimiter=',', converters={
    0: mdates.strpdate2num("%m/%d/%Y %I:%M:%S %p")}, skiprows=1, usecols=(0,1,3),
    unpack=True)

datesNewDesignInverted1e9Pressure, dataPressureNewDesignInverted1e9Pressure, dataVoltNewDesignInverted1e9Pressure = sp.loadtxt(
    targetFile56mmNewDesignInverted1e9Pressure, delimiter=',', converters={
    0: mdates.strpdate2num("%m/%d/%Y %I:%M:%S %p")}, skiprows=1, usecols=(0,1,3),
    unpack=True)

datesNewDesignInverted1e8Pressure, dataPressureNewDesignInverted1e8Pressure, dataVoltNewDesignInverted1e8Pressure = sp.loadtxt(
    targetFile56mmNewDesignInverted1e8Pressure, delimiter=',', converters={
    0: mdates.strpdate2num("%m/%d/%Y %I:%M:%S %p")}, skiprows=1, usecols=(0,1,3),
    unpack=True)

datesNewDesignInverted1e7Pressure, dataPressureNewDesignInverted1e7Pressure, dataVoltNewDesignInverted1e7Pressure = sp.loadtxt(
    targetFile56mmNewDesignInverted1e7Pressure, delimiter=',', converters={
    0: mdates.strpdate2num("%m/%d/%Y %I:%M:%S %p")}, skiprows=1, usecols=(0,1,3),
    unpack=True)

##inputData = extract_dat(targetFile)
##
##i=1
##
##for entry in inputData:
####    print entry[0].rstrip("PAM ")
##    if entry[0] == "4/29/2013 12:00:00":
##        print i
##        i+=1
##    else:
##        i+=1
##        pass
##


listPeaksVolt56mmOffset = sp.array(peak_finder(dates56mmOffset, dataVolt56mmOffset))
listPeaksVoltMetallizedEndMarch = sp.array(peak_finder(datesMetallizedEndMarch, dataVoltMetallizedEndMarch))
listPeaksVoltOffsetFeb = sp.array(peak_finder(datesOffsetFeb, dataVoltOffsetFeb))
listPeaksVoltNewDesignInvertedBackPressure = sp.array(peak_finder(datesNewDesignInvertedBackPressure, dataVoltNewDesignInvertedBackPressure))
listPeaksVoltNewDesignInverted1e9Pressure = sp.array(peak_finder(datesNewDesignInverted1e9Pressure, dataVoltNewDesignInverted1e9Pressure))
listPeaksVoltNewDesignInverted1e8Pressure = sp.array(peak_finder(datesNewDesignInverted1e8Pressure, dataVoltNewDesignInverted1e8Pressure))
listPeaksVoltNewDesignInverted1e7Pressure = sp.array(peak_finder(datesNewDesignInverted1e7Pressure, dataVoltNewDesignInverted1e7Pressure))


listPeaksPres56mmOffset = []
listPeaksDates56mmOffset = []
listPeaksPulse56mmOffset = []
for entry in listPeaksVolt56mmOffset:
   listPeaksPres56mmOffset.append(dataPressure56mmOffset[entry[0]]) 
   listPeaksDates56mmOffset.append(dates56mmOffset[entry[0]])
   listPeaksPulse56mmOffset.append(dataPulse56mmOffset[entry[0]])

listPeaksPres56mmOffset=sp.array(listPeaksPres56mmOffset)
listPeaksDates56mmOffset=sp.array(listPeaksDates56mmOffset)
listPeaksPulse56mmOffset = sp.array(listPeaksPulse56mmOffset)

listPeaksPresMetallizedEndMarch = []
listPeaksDatesMetallizedEndMarch = []
for entry in listPeaksVoltMetallizedEndMarch:
   listPeaksPresMetallizedEndMarch.append(dataPressureMetallizedEndMarch[entry[0]]) 
   listPeaksDatesMetallizedEndMarch.append(datesMetallizedEndMarch[entry[0]]) 

listPeaksPresMetallizedEndMarch=sp.array(listPeaksPresMetallizedEndMarch)
listPeaksDatesMetallizedEndMarch=sp.array(listPeaksDatesMetallizedEndMarch)

listPeaksPresOffsetFeb = []
listPeaksDatesOffsetFeb = []
for entry in listPeaksVoltOffsetFeb:
   listPeaksPresOffsetFeb.append(dataPressureOffsetFeb[entry[0]]) 
   listPeaksDatesOffsetFeb.append(datesOffsetFeb[entry[0]]) 

listPeaksPresOffsetFeb=sp.array(listPeaksPresOffsetFeb)
listPeaksDatesOffsetFeb=sp.array(listPeaksDatesOffsetFeb)

listPeaksPresNewDesignInvertedBackPressure = []
listPeaksDatesNewDesignInvertedBackPressure = []
for entry in listPeaksVoltNewDesignInvertedBackPressure:
   listPeaksPresNewDesignInvertedBackPressure.append(dataPressureNewDesignInvertedBackPressure[entry[0]]) 
   listPeaksDatesNewDesignInvertedBackPressure.append(datesNewDesignInvertedBackPressure[entry[0]]) 

listPeaksPresNewDesignInvertedBackPressure=sp.array(listPeaksPresNewDesignInvertedBackPressure)
listPeaksDatesNewDesignInvertedBackPressure=sp.array(listPeaksDatesNewDesignInvertedBackPressure)

listPeaksPresNewDesignInverted1e9Pressure = []
listPeaksDatesNewDesignInverted1e9Pressure = []
for entry in listPeaksVoltNewDesignInverted1e9Pressure:
   listPeaksPresNewDesignInverted1e9Pressure.append(dataPressureNewDesignInverted1e9Pressure[entry[0]]) 
   listPeaksDatesNewDesignInverted1e9Pressure.append(datesNewDesignInverted1e9Pressure[entry[0]]) 

listPeaksPresNewDesignInverted1e9Pressure=sp.array(listPeaksPresNewDesignInverted1e9Pressure)
listPeaksDatesNewDesignInverted1e9Pressure=sp.array(listPeaksDatesNewDesignInverted1e9Pressure)

listPeaksPresNewDesignInverted1e8Pressure = []
listPeaksDatesNewDesignInverted1e8Pressure = []
for entry in listPeaksVoltNewDesignInverted1e8Pressure:
   listPeaksPresNewDesignInverted1e8Pressure.append(dataPressureNewDesignInverted1e8Pressure[entry[0]]) 
   listPeaksDatesNewDesignInverted1e8Pressure.append(datesNewDesignInverted1e8Pressure[entry[0]]) 

listPeaksPresNewDesignInverted1e8Pressure=sp.array(listPeaksPresNewDesignInverted1e8Pressure)
listPeaksDatesNewDesignInverted1e8Pressure=sp.array(listPeaksDatesNewDesignInverted1e8Pressure)

listPeaksPresNewDesignInverted1e7Pressure = []
listPeaksDatesNewDesignInverted1e7Pressure = []
for entry in listPeaksVoltNewDesignInverted1e7Pressure:
   listPeaksPresNewDesignInverted1e7Pressure.append(dataPressureNewDesignInverted1e7Pressure[entry[0]]) 
   listPeaksDatesNewDesignInverted1e7Pressure.append(datesNewDesignInverted1e7Pressure[entry[0]]) 

listPeaksPresNewDesignInverted1e7Pressure=sp.array(listPeaksPresNewDesignInverted1e7Pressure)
listPeaksDatesNewDesignInverted1e7Pressure=sp.array(listPeaksDatesNewDesignInverted1e7Pressure)

######## Plotting vs. Date ###############


ax1=pl.subplot(111)

pl.semilogy()
##pl.plot(dates56mmOffset-dates56mmOffset[0], dataPressure56mmOffset, 'k-', label="Pressure")
##pl.plot(listPeaksDates56mmOffset, listPeaksPres56mmOffset, 'go')
##pl.plot(datesMetallizedEndMarch-datesMetallizedEndMarch[0], dataPressureMetallizedEndMarch, 'k--', label="Pressure")
##pl.plot(listPeaksDatesMetallizedEndMarch, listPeaksPresMetallizedEndMarch, 'go')
##pl.plot(datesOffsetFeb-datesOffsetFeb[0], dataPressureOffsetFeb, 'k-.', label="Pressure")
##pl.plot(listPeaksDatesOffsetFeb, listPeaksPresOffsetFeb, 'go')
pl.plot(datesNewDesignInvertedBackPressure-datesNewDesignInvertedBackPressure[0], dataPressureNewDesignInvertedBackPressure, 'k-', label="Pressure")
##pl.plot(datesNewDesignInverted1e9Pressure-datesNewDesignInverted1e9Pressure[0], dataPressureNewDesignInverted1e9Pressure, 'k-', label="Pressure")
##pl.plot(datesNewDesignInverted1e8Pressure-datesNewDesignInverted1e8Pressure[0], dataPressureNewDesignInverted1e8Pressure, 'k-', label="Pressure")
##pl.plot(datesNewDesignInverted1e7Pressure-datesNewDesignInverted1e7Pressure[0], dataPressureNewDesignInverted1e7Pressure, 'k-', label="Pressure")
pl.ylabel("Vacuum Pressure (mbar)", fontsize=16.0)
pl.xlabel("Days", fontsize=16.0)
pl.legend(loc="upper right")
pl.ylim(3*10**-10, 10**-7)
ax2=pl.twinx()
##pl.plot(dates56mmOffset-dates56mmOffset[0], SIMIVoltToPFNTunnelVolt*dataVolt56mmOffset, 'r-', label="PFN Voltage")
##pl.plot(listPeaksDates56mmOffset,listPeaksVolt56mmOffset[:,2], 'bo')
##pl.plot(datesMetallizedEndMarch-datesMetallizedEndMarch[0], SIMIVoltToPFNTunnelVolt*dataVoltMetallizedEndMarch, 'r--', label="PFN Voltage")
##pl.plot(listPeaksDatesMetallizedEndMarch,listPeaksVoltMetallizedEndMarch[:,2], 'bo')
##pl.plot(datesOffsetFeb-datesOffsetFeb[0], SIMIVoltToPFNTunnelVolt*dataVoltOffsetFeb, 'r-.', label="PFN Voltage")
pl.plot(datesNewDesignInvertedBackPressure-datesNewDesignInvertedBackPressure[0], SIMIVoltToPFNTunnelVolt*(9.1/6.9)*dataVoltNewDesignInvertedBackPressure, 'r-', label="PFN Voltage")
##pl.plot(datesNewDesignInverted1e9Pressure-datesNewDesignInverted1e9Pressure[0], SIMIVoltToPFNTunnelVolt*(9.1/6.9)*dataVoltNewDesignInverted1e9Pressure, 'r-', label="PFN Voltage")
##pl.plot(datesNewDesignInverted1e8Pressure-datesNewDesignInverted1e8Pressure[0], SIMIVoltToPFNTunnelVolt*(9.1/6.9)*dataVoltNewDesignInverted1e8Pressure, 'r-', label="PFN Voltage")
##pl.plot(datesNewDesignInverted1e7Pressure-datesNewDesignInverted1e7Pressure[0], SIMIVoltToPFNTunnelVolt*(9.1/6.9)*dataVoltNewDesignInverted1e7Pressure, 'r-', label="PFN Voltage")
##pl.plot(listPeaksDatesOffsetFeb,listPeaksVoltOffsetFeb[:,2], 'bo')
pl.ylabel("PFN Voltage (kV)", fontsize=16.0)
pl.legend(loc="upper left")
##pl.show()
pl.clf()


######## Plotting vs. Pulse number ############




ax1=pl.subplot(111)
##
pl.semilogy()
##pl.plot(dataPressure56mmOffset, 'k-', label="Pressure")
##pl.plot(listPeaksDates56mmOffset, listPeaksPres56mmOffset, 'go')
##pl.plot(dataPressureMetallizedEndMarch, 'k--', label="Pressure")
##pl.plot(listPeaksDatesMetallizedEndMarch, listPeaksPresMetallizedEndMarch, 'go')
##pl.plot(dataPressureOffsetFeb, 'k-.', label="Pressure")
##pl.plot(dataPressureNewDesignInvertedBackPressure, 'k-', label="Pressure")
##pl.plot(dataPressureNewDesignInverted1e9Pressure, 'k--', label="Pressure")
##pl.plot(dataPressureNewDesignInverted1e8Pressure, 'k-.', label="Pressure")
##pl.plot(dataPressureNewDesignInverted1e7Pressure, 'k-.', label="Pressure")
##pl.plot(listPeaksDatesOffsetFeb, listPeaksPresOffsetFeb, 'go')
pl.ylabel("Vacuum Pressure (mbar)", fontsize=16.0)
pl.legend(loc="upper right")
pl.axis([0, 10**6, 5*10**-10, 10**-7])
##
ax2=pl.twinx()
##pl.plot(SIMIVoltToPFNTunnelVolt*dataVolt56mmOffset, 'r-', label="PFN Voltage")
##pl.plot(listPeaksDates56mmOffset,listPeaksVolt56mmOffset[:,2], 'bo')
##pl.plot(SIMIVoltToPFNTunnelVolt*dataVoltMetallizedEndMarch, 'r--', label="PFN Voltage")
##pl.plot(listPeaksDatesMetallizedEndMarch,listPeaksVoltMetallizedEndMarch[:,2], 'bo')
##pl.plot(SIMIVoltToPFNTunnelVolt*dataVoltOffsetFeb, 'r-.', label="PFN Voltage")
##pl.plot(SIMIVoltToPFNTunnelVolt*dataVoltNewDesignInvertedBackPressure, 'r-', label="PFN Voltage")
##pl.plot(dataVoltNewDesignInvertedBackPressure, 'r-', label="PFN Voltage")
pl.plot(dataVoltNewDesignInverted1e9Pressure, 'r--', label="PFN Voltage")
##pl.plot(dataVoltNewDesignInverted1e8Pressure, 'r-.', label="PFN Voltage")
##pl.plot(dataVoltNewDesignInverted1e7Pressure, 'r-.', label="PFN Voltage")
##pl.plot(listPeaksDatesOffsetFeb,listPeaksVoltOffsetFeb[:,2], 'bo')
pl.ylabel("PFN Voltage (kV)", fontsize=16.0)
pl.legend(loc="upper left")
##pl.show()
pl.clf()


###### Determining a relationship between pressure and breakdown voltage #########

listPresVolt56mmOffset = []
listPresVoltMetallizedEndMarch = []
listPresVoltOffsetFeb = []
listPresVoltNewDesignInvertedBackPressure = []
listPresVoltNewDesignInverted1e9Pressure = []
listPresVoltNewDesignInverted1e8Pressure = []
listPresVoltNewDesignInverted1e7Pressure = []

for i in range(0,len(listPeaksVolt56mmOffset)):
    if listPeaksPres56mmOffset[i]>1.0*10**-6:
        pass
    else:
        listPresVolt56mmOffset.append([listPeaksVolt56mmOffset[i,0], listPeaksPres56mmOffset[i], listPeaksVolt56mmOffset[i,2], listPeaksPulse56mmOffset[i]])

for i in range(0,len(listPeaksVoltMetallizedEndMarch)):
    if listPeaksPresMetallizedEndMarch[i]>1.0*10**-6:
        pass
    else:
        listPresVoltMetallizedEndMarch.append([listPeaksVoltMetallizedEndMarch[i,0], listPeaksPresMetallizedEndMarch[i], listPeaksVoltMetallizedEndMarch[i,2]])

for i in range(0,len(listPeaksVoltOffsetFeb)):
    if listPeaksPresOffsetFeb[i]>1.0*10**-6:
        pass
    else:
        listPresVoltOffsetFeb.append([listPeaksVoltOffsetFeb[i,0], listPeaksPresOffsetFeb[i], listPeaksVoltOffsetFeb[i,2]])

for i in range(0,len(listPeaksVoltNewDesignInvertedBackPressure)):
    if listPeaksPresNewDesignInvertedBackPressure[i]>1.0*10**-6:
        pass
    else:
        listPresVoltNewDesignInvertedBackPressure.append([listPeaksVoltNewDesignInvertedBackPressure[i,0], listPeaksPresNewDesignInvertedBackPressure[i], listPeaksVoltNewDesignInvertedBackPressure[i,2]])

for i in range(0,len(listPeaksVoltNewDesignInverted1e9Pressure)):
    if listPeaksPresNewDesignInverted1e9Pressure[i]>1.0*10**-6:
        pass
    else:
        listPresVoltNewDesignInverted1e9Pressure.append([listPeaksVoltNewDesignInverted1e9Pressure[i,0], listPeaksPresNewDesignInverted1e9Pressure[i], listPeaksVoltNewDesignInverted1e9Pressure[i,2]])

for i in range(0,len(listPeaksVoltNewDesignInverted1e8Pressure)):
    if listPeaksPresNewDesignInverted1e8Pressure[i]>1.0*10**-6:
        pass
    else:
        listPresVoltNewDesignInverted1e8Pressure.append([listPeaksVoltNewDesignInverted1e8Pressure[i,0], listPeaksPresNewDesignInverted1e8Pressure[i], listPeaksVoltNewDesignInverted1e8Pressure[i,2]])

for i in range(0,len(listPeaksVoltNewDesignInverted1e7Pressure)):
    if listPeaksPresNewDesignInverted1e7Pressure[i]>1.0*10**-6:
        pass
    else:
        listPresVoltNewDesignInverted1e7Pressure.append([listPeaksVoltNewDesignInverted1e7Pressure[i,0], listPeaksPresNewDesignInverted1e7Pressure[i], listPeaksVoltNewDesignInverted1e7Pressure[i,2]])

listPresVolt56mmOffset=sp.array(listPresVolt56mmOffset)
listPresVoltMetallizedEndMarch=sp.array(listPresVoltMetallizedEndMarch)
listPresVoltOffsetFeb=sp.array(listPresVoltOffsetFeb)
listPresVoltNewDesignInvertedBackPressure=sp.array(listPresVoltNewDesignInvertedBackPressure)
listPresVoltNewDesignInverted1e9Pressure=sp.array(listPresVoltNewDesignInverted1e9Pressure)
listPresVoltNewDesignInverted1e8Pressure=sp.array(listPresVoltNewDesignInverted1e8Pressure)
listPresVoltNewDesignInverted1e7Pressure=sp.array(listPresVoltNewDesignInverted1e7Pressure)

print listPresVolt56mmOffset
print listPresVoltNewDesignInvertedBackPressure
print listPresVoltNewDesignInverted1e9Pressure
print listPresVoltNewDesignInverted1e8Pressure
print listPresVoltNewDesignInverted1e7Pressure
##toFitForm = lambda p, pres: pres**p[1]/p[0]
##toFitForm = lambda p, pres: p[1]**pres*p[0] + p[2]
##fitErrFunc = lambda p, pres, volt: (volt-toFitForm(p,pres))
##
##pinit=[20,0.5,0.01]
##
##pfinal, pcov = op.leastsq(fitErrFunc, pinit, args=(listPresVolt[:,1], listPresVolt[:,2]))
##print pfinal
##
##listValues = sp.linspace(10**-8,10**-7,1000)
##
##listFitValues=sp.array(listValues**pfinal[1]/pfinal[0]+pfinal[2])
##
##ax1=pl.subplot(111)

print FieldOffset24CondStaggered*listPresVoltOffsetFeb[:,2], FieldOffset24CondStaggeredRadial*listPresVoltOffsetFeb[:,2]
print FieldMetal24Cond56MMtUBE*listPresVoltMetallizedEndMarch[:,2], FieldMetal24Cond56MMtUBERadial*listPresVoltMetallizedEndMarch[:,2]


fig=pl.gcf()
##pl.plot(sp.log10(listPresVolt56mmOffset[:,1]),FieldOffsetRotatedTapering24Cond*listPresVolt56mmOffset[:,2], "bx', label="Breakdowns Offset Tube")
pl.plot(sp.log10(listPresVoltMetallizedEndMarch[:,1]), FieldMetal24Cond56MMtUBE*listPresVoltMetallizedEndMarch[:,2], 'rx', markersize=16.0, label="Breakdowns Metallized Tube")
pl.plot(sp.log10(listPresVoltOffsetFeb[:,1]), FieldOffset24CondStaggered*listPresVoltOffsetFeb[:,2], 'kx', markersize=16.0, label="Breakdowns Offset Tube")
##pl.plot(sp.log10(listPresVoltNewDesignInvertedBackPressure[:,1]), FieldOffset24CondStaggered*listPresVoltNewDesignInvertedBackPressure[:,2], 'gx', label="Breakdowns 53mm Offset End of February")
##pl.plot(sp.log10(listValues), listFitValues, 'r-')
##pl.ylabel("PFN Voltage (kV)", fontsize=16.0)
pl.ylabel("Axial Electric Field (kV/mm)", fontsize=16.0)
pl.xlabel("Vacuum Pressure log$_{10}$(mbar)", fontsize=16.0)

##count=-1
##for thing in listPresVolt56mmOffset:
##    circle=mpatches.Ellipse((sp.log10(thing[1]),FieldOffsetRotatedTapering24Cond*thing[2]), width=0.03, height=1.2, color="k", fill=False)
##    fig.gca().add_artist(circle)
##    count+=1
##    if count%2==1:
##        pl.annotate('Pulse no. %s' % str(thing[-1]), xy=(sp.log10(thing[1]),FieldOffsetRotatedTapering24Cond*thing[2]), xytext=(sp.log10(thing[1])+0.05,FieldOffsetRotatedTapering24Cond*thing[2]+0.5), arrowprops=dict(facecolor="black", shrink=0.05),)
##    else:
##        pass
##
##for thing in listPresVolt56mmOffset:
##    circle=mpatches.Ellipse((sp.log10(thing[1]),FieldOffsetRotatedTapering24Cond*thing[2]), width=0.03, height=1.2, color="k", fill=False)
##    fig.gca().add_artist(circle)
##    count+=1
##    if count%2==1:
##        pl.annotate('Pulse no. %s' % str(thing[-1]), xy=(sp.log10(thing[1]),FieldOffsetRotatedTapering24Cond*thing[2]), xytext=(sp.log10(thing[1])+0.05,FieldOffsetRotatedTapering24Cond*thing[2]+0.5), arrowprops=dict(facecolor="black", shrink=0.05),)
##    else:
##        pass
##
##for thing in listPresVolt56mmOffset:
##    circle=mpatches.Ellipse((sp.log10(thing[1]),FieldOffsetRotatedTapering24Cond*thing[2]), width=0.03, height=1.2, color="k", fill=False)
##    fig.gca().add_artist(circle)
##    count+=1
##    if count%2==1:
##        pl.annotate('Pulse no. %s' % str(thing[-1]), xy=(sp.log10(thing[1]),FieldOffsetRotatedTapering24Cond*thing[2]), xytext=(sp.log10(thing[1])+0.05,FieldOffsetRotatedTapering24Cond*thing[2]+0.5), arrowprops=dict(facecolor="black", shrink=0.05),)
##    else:
##        pass

    
pl.legend(loc="upper right")
##pl.axis([-7.6,-5.5,5,25])
##pl.show()
pl.clf()

pl.plot(sp.log10(listPresVolt56mmOffset[:,1]),FieldOffsetRotatedTapering24CondRadial*listPresVolt56mmOffset[:,2], 'kx', label="Breakdowns 56mm Offset")
pl.plot(sp.log10(listPresVoltMetallizedEndMarch[:,1]), FieldMetal24Cond56MMtUBERadial*listPresVoltMetallizedEndMarch[:,2], 'rx', label="Breakdowns 53mm Metallized End of March")
##pl.plot(sp.log10(listPresVoltOffsetFeb[:,1]), FieldOffset24CondStaggeredRadial*listPresVoltOffsetFeb[:,2], 'bx', label="Breakdowns 53mm Offset End of February")
##pl.plot(sp.log10(listPresVoltNewDesignInvertedBackPressure[:,1]), FieldOffset24CondStaggeredRadial*listPresVoltNewDesignInvertedBackPressure[:,2], 'gx', label="Breakdowns 53mm Offset End of February")
##pl.plot(sp.log10(listValues), listFitValues, 'r-')
##pl.ylabel("PFN Voltage (kV)", fontsize=16.0)
pl.ylabel("Radial Electric Field (kV/mm)", fontsize=16.0)
pl.xlabel("Vacuum Pressure log$_{10}$(mbar)", fontsize=16.0)
##pl.axis([-7.6,-6.5,15,50])
##fig=pl.gcf()
##for thing in listPresVolt:
##    circle=mpatches.Ellipse((sp.log10(thing[1]),thing[2]), width=0.03, height=1.2, color="k", fill=False)
##    fig.gca().add_artist(circle)
##    pl.annotate('Pulse no. %s' % str(thing[0]), xy=(sp.log10(thing[1]),thing[2]), xytext=(sp.log10(thing[1])+0.2,thing[2]+1), arrowprops=dict(facecolor="black", shrink=0.05),)

pl.legend(loc="lower left")
##pl.show()
pl.clf()
