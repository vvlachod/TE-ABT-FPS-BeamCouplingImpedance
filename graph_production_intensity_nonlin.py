import csv
import scipy as sp
import pylab as pl

directory = "C:/Users/hday/Documents/PhD/1st_Year_09-10/Software/headtail/data_dump/comp_wake_non_lin/" #Directory of data
list_bt_sc_wake = [directory+"hdtl-bt-sc-short-wake.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_bt_sc_nonlin = [directory+"hdtl-bt-sc-short-nonlin.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_at_sc_wake = [directory+"hdtl-at-sc-short-wake.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_at_sc_nonlin = [directory+"hdtl-at-sc-short-nonlin.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]


value_desired = 9
big_bugger =[]
data_list_1 = []
for data_raw in list_bt_sc_wake:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])
    
bt_sc_wake = sp.array(data_list_1)



data_list_1 = []
for data_raw in list_bt_sc_nonlin:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])
    
bt_sc_nonlin = sp.array(data_list_1)


data_list_1 = []
for data_raw in list_at_sc_wake:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])
    
at_sc_wake = sp.array(data_list_1)



data_list_1 = []
for data_raw in list_at_sc_nonlin:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    print data_raw
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])
    
at_sc_nonlin = sp.array(data_list_1)



x_values = sp.array(range(1,31))

pl.plot(x_values, bt_sc_wake[0,:], 'r-', label = "Below Transition - Wake")                    # plot fitted curve
pl.plot(x_values, bt_sc_nonlin[0,:],'r:', label = "Below Transition - nonlin")
pl.plot(x_values, at_sc_wake[0,:], 'k-', label = "Above Transition - Wake")                    # plot fitted curve
pl.plot(x_values, at_sc_nonlin[0,:],'k:', label = "Above Transition - nonlin")




pl.grid(linestyle="--", which = "major")
pl.xlabel("Bunch Intensity ($10^{10}$)",fontsize = 16)                  #Label axes
pl.ylabel("RMS Bunch Length $\sigma_{z}$ $(m)$",fontsize = 16)
pl.title("", fontsize = 16)
pl.legend(loc = "upper left")
pl.axis([0,5,0,5])
pl.savefig(directory+"bt-at-rms-bunch-length"+".pdf")                  # Save fig for film
pl.show()
pl.clf()   

##polycoeffs = sp.polyfit(x_values,big_bugger[2,:],1)
##print polycoeffs
##yfit=sp.polyval(polycoeffs,x_values)
##pl.plot(x_values, big_bugger[2,:])
##pl.plot(x_values, yfit)
##pl.show()
##pl.savefig(directory+"moo.pdf")
##pl.clf()
##        

