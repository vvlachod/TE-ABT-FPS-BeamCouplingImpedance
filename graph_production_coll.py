import csv
import scipy as sp
import pylab as pl


length = 1.1
displacement = 0.003

f_rev = 3*10**8/27000.0
directory = "E:/PhD/1st_Year_09-10/Data/TCTP/" #Directory of data
graph_2mm = "longitudinal-impedance.csv"
graph_60mm = "horz-dip.csv"

data_graph_2mm = []

input_file = open(directory+graph_2mm, 'r+')
tar=input_file.readlines()


##i=0
for row in tar[1:]:
    row = map(float, row.rsplit(','))
    row[1] = row[1]/length/(row[0]*10**9/f_rev)
    row[3] = row[3]/length/(row[0]*10**9/f_rev)
    data_graph_2mm.append(row)


input_file.close()
data_graph_2mm = sp.array(data_graph_2mm)

data_graph_60mm = []

input_file = open(directory+graph_60mm, 'r+')
tar=input_file.readlines()


##i=0
for row in tar:
    row = map(float, row.rsplit(','))
    row[1] = row[1]/displacement
    row[3] = row[3]/displacement
    data_graph_60mm.append(row)


input_file.close()
data_graph_60mm = sp.array(data_graph_60mm)

print data_graph_2mm

pl.semilogy()
##pl.plot(data_graph_2mm[:,0], data_graph_2mm[:,1], 'k-', markersize=8.0,label = "CERN Graphite - 2mm spacing")                    # plot fitted curve
##pl.plot(data_graph_2mm[:,0], data_graph_2mm[:,3], 'r-', markersize=8.0,label = "CERN Graphite - 2mm spacing")                    # plot fitted curve
pl.plot(data_graph_60mm[:,0], abs(data_graph_60mm[:,1]), 'k-', markersize=8.0,label = "CERN Graphite - 60mm spacing")                    # plot fitted curve
pl.plot(data_graph_60mm[:,0], abs(data_graph_60mm[:,3]), 'r-', markersize=8.0,label = "CERN Graphite - 60mm spacing")                    # plot fitted curve


pl.grid(linestyle="--", which = "major")
pl.xlabel("Frequency (GHz)",fontsize = 20)                  #Label axes
pl.ylabel("Impedance $(\Omega/m)$",fontsize = 20)
##pl.title("", fontsize = 16)
pl.legend(loc="lower left")
##pl.axis([10**-1,2,10**2,10**7])
##pl.savefig(directory+"cross-section-hor-dip"+".pdf")                  # Save fig for film
pl.show()
pl.clf()   
