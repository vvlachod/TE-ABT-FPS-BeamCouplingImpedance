import sympy.mpmath as mpmath
import scipy, numpy, csv, time
from matplotlib import pyplot
import numbers
import pylab as pl
from PIL import Image
import images2swf

start = time.time()
directory = "C:/Users/hday/Documents/PhD/1st_Year_09-10/Software/materials/" #Directory of data
data_file = "with_plane_eps.csv"      # File containing data


input_file = open(directory+data_file, "r+")

data_raw = csv.reader(input_file)       #Assign and open input file

y_data= []    # Create blank for data
wire_t = 5*10**-4                  # Radius of wire
seperation = 1.6*10**-2            # Seperation of plates

for row in data_raw:
    for i in range(0,len(row)):
        row[i] = float(row[i])
    y_data.append(row)   # Read in data

input_file.close()                              # close input file

y_array = scipy.array(y_data)

##x_data = sp.linspace(-9,9,7)                    # Create x data
x_data_fit = scipy.linspace(0,2000,2000)
coefficients = []          # Blank for coeffs


poly_coeffs_epsprime = scipy.polyfit(y_array[:,0], y_array[:,1], 40)  # coeffs for one data set
poly_coeffs_epsdoubleprime = scipy.polyfit(y_array[:,0], y_array[:,2], 40)  # coeffs for one data set
print poly_coeffs_epsprime, poly_coeffs_epsdoubleprime                               # Verify its not garbage
y_fit_epsprime = scipy.polyval(poly_coeffs_epsprime, x_data_fit)     # fitted line for plot
y_fit_epsdoubleprime = scipy.polyval(poly_coeffs_epsdoubleprime, x_data_fit)
##pl.plot(y_array[:,0], y_array[:,1], 'k.')                # Plot raw_data
##pl.plot(x_data_fit, y_fit_epsprime, 'r-',markersize=25)                    # plot fitted curve
##pl.xlabel("Displacement (mm)",fontsize = 16)                  #Label axes
##pl.ylabel("Impedance (Ohms/m)",fontsize = 16)
##pl.show()
##pl.clf()                                        # Clear figure
##pl.plot(y_array[:,0], y_array[:,2], 'k.')                # Plot raw_data
##pl.plot(x_data_fit, y_fit_epsdoubleprime, 'r-',markersize=25)                    # plot fitted curve
##pl.xlabel("Displacement (mm)",fontsize = 16)                  #Label axes
##pl.ylabel("Impedance (Ohms/m)",fontsize = 16)
##pl.show()
##pl.clf()                                        # Clear figure


Z0 = 377.0
a = 0.04
b = 0.00046288
d = 0.02246288
e0 = 8.85*10**-12
eprime = 1.0
rho = 1.76*10**7
muprime = 1.0
tau = 1.0/(20.0*10**6)
eprime= 1.0
mu0 = 4*scipy.pi*10**-7
mur = 1.0

def k(freq):
    return 2*scipy.pi*freq/(3*10**8)

def er(freq):
    return complex(scipy.polyval(poly_coeffs_epsprime, freq),scipy.polyval(poly_coeffs_epsdoubleprime,freq))

##def mur(freq):
##    return (1 + (muprime/(1+1j*tau*freq)))

def kxn(n):
    return ((2*n + 1)*scipy.pi)/(2*a)

def kyn(n,freq):
    return ((er(freq)*mur - 1)*k(freq)**2 - kxn(n)**2)**0.5

def sh(n):
    return mpmath.sinh(kxn(n)*b)

def ch(n):
    return mpmath.cosh(kxn(n)*b)

def tn(n,freq):
    return mpmath.tan(kyn(n,freq)*(b-d))

def ct(n,freq):
    return mpmath.cot(kyn(n,freq)*(b-d))

def FX(n,freq):
    return (kxn(n)/k(freq))*((1+(mur*er(freq)))*sh(n)*ch(n))

def FY(n,freq):
    return (kyn(n,freq)/k(freq))*(mur*(sh(n)**2)*tn(n,freq) - er(freq)*(ch(n)**2)*ct(n,freq))

def FY_2(n,freq):
    return (kyn(n,freq)/k(freq))*(mur*(ch(n)**2)*tn(n,freq) - er(freq)*(sh(n)**2)*ct(n,freq))

def ferrite_long(freq):
    return (1j*Z0)/(2*a) * (mpmath.nsum(lambda n: (((FX(n,freq) +FY(n,freq))/(er(freq)*mur-1)) - ((k(freq)*sh(n)*ch(n))/kxn(n)))**(-1), [0,10]))

def ferrite_dip_horz(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY(n,freq))/(er(freq)*mur-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def ferrite_dip_vert(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY_2(n,freq))/(er(freq)*mur-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def ferrite_quad_horz(freq):
    return (-1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY(n,freq))/(er(freq)*mur-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def ferrite_quad_vert(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY(n,freq))/(er(freq)*mur-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])


start = time.time()

x= []
temp_real = []
temp_imag = []

freq_list = []
for i in range(0,9,1):
    for j in range(1,100,1):
        freq_list.append(float((j/10.0)*10.0**i))

freq_list = list(set(freq_list))
freq_list.sort()

for i in freq_list:
    temp_real.append(ferrite_long(i).real)
    temp_imag.append(ferrite_long(i).imag)
##    x.append(float(i)/10.0**9)

output = open('phase-II-coll-long-sic.csv', 'w+')
for i in range(0,len(temp_real)):
              output.write(str(freq_list[i])+','+str(temp_real[i])+','+str(temp_imag[i])+'\n')

output.close()

pl.loglog(freq_list, temp_real, 'k-', label = "Real Longitudinal")
pl.loglog(freq_list, temp_imag, 'r-', label = "Imaginary Longitudinal")


pl.xlabel("Frequency (MHz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m$)", fontsize=16)
pl.legend()
pl.savefig('phase-II-coll-long-sic.pdf')
pl.clf()


temp_real = []
temp_imag = []

for i in freq_list:
    temp_real.append(ferrite_dip_vert(i).real)
    temp_imag.append(ferrite_dip_vert(i).imag)


output = open('phase-II-coll-vert-dip-sic.csv', 'w+')
for i in range(0,len(temp_real)):
              output.write(str(freq_list[i])+','+str(temp_real[i])+','+str(temp_imag[i])+'\n')
output.close()

pl.loglog(freq_list, temp_real, 'k-', label = "Real Longitudinal")
pl.loglog(freq_list, temp_imag, 'r-', label = "Imaginary Longitudinal")

pl.xlabel("Frequency (MHz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m^{2}$)", fontsize=16)
pl.legend()
pl.savefig('phase-II-coll-vert-dip-sic.pdf')
pl.clf()


temp_real = []
temp_imag = []

for i in freq_list:
    temp_real.append(ferrite_dip_horz(i).real)
    temp_imag.append(ferrite_dip_horz(i).imag)


output = open('phase-II-coll-horz-dip-sic.csv', 'w+')
for i in range(0,len(temp_real)):
              output.write(str(freq_list[i])+','+str(temp_real[i])+','+str(temp_imag[i])+'\n')
output.close()

pl.loglog(freq_list, temp_real, 'k-', label = "Real Longitudinal")
pl.loglog(freq_list, temp_imag, 'r-', label = "Imaginary Longitudinal")


pl.xlabel("Frequency (MHz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m^{2}$)", fontsize=16)
pl.legend()
pl.savefig('phase-II-coll-horz-dip-sic.pdf')
pl.clf()


temp_real = []
temp_imag = []

for i in freq_list:
    temp_real.append(abs(ferrite_quad_horz(i).real))
    temp_imag.append(abs(ferrite_quad_horz(i).imag))


output = open('phase-II-coll-horz-quad-sic.csv', 'w+')
for i in range(0,len(temp_real)):
              output.write(str(freq_list[i])+','+str(temp_real[i])+','+str(temp_imag[i])+'\n')
output.close()

pl.loglog(freq_list, temp_real, 'k-', label = "Real Longitudinal")
pl.loglog(freq_list, temp_imag, 'r-', label = "Imaginary Longitudinal")


pl.xlabel("Frequency (MHz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m^{2}$)", fontsize=16)
pl.legend()
pl.savefig('phase-II-coll-horz-quad-sic.pdf')
pl.clf()

temp_real = []
temp_imag = []

for i in freq_list:
    temp_real.append(ferrite_quad_vert(i).real)
    temp_imag.append(ferrite_quad_vert(i).imag)


output = open('phase-II-coll-vert-quad-sic.csv', 'w+')
for i in range(0,len(temp_real)):
              output.write(str(freq_list[i])+','+str(temp_real[i])+','+str(temp_imag[i])+'\n')
output.close()
pl.loglog(freq_list, temp_real, 'k-', label = "Real Longitudinal")
pl.loglog(freq_list, temp_imag, 'r-', label = "Imaginary Longitudinal")


pl.xlabel("Frequency (MHz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m^{2}$)", fontsize=16)
pl.legend()
pl.savefig('phase-II-coll-vert-quad-sic.pdf')
pl.clf()


temp_real = []
temp_imag = []

for i in freq_list:
    temp_real.append((ferrite_dip_vert(i)+ferrite_quad_vert(i)).real)
    temp_imag.append((ferrite_dip_vert(i)+ferrite_quad_vert(i)).imag)

output = open('phase-II-coll-vert-total-sic.csv', 'w+')
for i in range(0,len(temp_real)):
              output.write(str(freq_list[i])+','+str(temp_real[i])+','+str(temp_imag[i])+'\n')

output.close()

pl.loglog(freq_list, temp_real, 'k-', label = "Real Longitudinal")
pl.loglog(freq_list, temp_imag, 'r-', label = "Imaginary Longitudinal")

pl.xlabel("Frequency (MHz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m^{2}$)", fontsize=16)
pl.legend()
pl.savefig('phase-II-coll-vert-total-sic.pdf')
pl.clf()


temp_real = []
temp_imag = []

for i in freq_list:
    temp_real.append(abs((ferrite_dip_horz(i).real+ferrite_quad_horz(i)).real))
    temp_imag.append(abs((ferrite_dip_horz(i).imag+ferrite_quad_horz(i)).imag))


output = open('phase-II-coll-horz-total-sic.csv', 'w+')
for i in range(0,len(temp_real)):
              output.write(str(freq_list[i])+','+str(temp_real[i])+','+str(temp_imag[i])+'\n')

output.close()

pl.loglog(freq_list, temp_real, 'k-', label = "Real Longitudinal")
pl.loglog(freq_list, temp_imag, 'r-', label = "Imaginary Longitudinal")


pl.xlabel("Frequency (MHz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m^{2}$)", fontsize=16)
pl.legend()
pl.savefig('phase-II-coll-horz-total-sic.pdf')
pl.clf()

print time.time() - start
