import csv, time, os, sys
import scipy as sp
import numpy as np
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack
from scipy import signal
sys.path.append("../")
import impedance.impedance as imp
import impedance.powlossprof as lossprof

"""
This example introduces the functionality of the coaxial wire measurement methods
and function in impedance.impedance library. This will cover both classical coaxial wire
methods and resonant coaxial wire methods.
Details of the physics can be found in:
"Longitudinal and Transverse Wire Measurements for the Evaluation of Impedance Reduction Measures on the MKE Extraction Kickers", T. Kroyer et al
http://cds.cern.ch/record/1035461?ln=en
or "Simulations and Measurements of Impedance Reduction Techniques" PhD Thesis, H. Day
"""
####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
c=299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
lDUT = 2.45

### Define the directories of resonant impedance measurements
topDirMeasHorz = "./transMeasExamples/ResonantMethod/mkiTransverseHorzRes/"
topDirMeasVert = "./transMeasExamples/ResonantMethod/mkiTransverseVertRes/"

### Define file names. For single measurements no list is required
filesHorz = [topDirMeasHorz+"res"+str(i)+"mm" for i in range(-15,12,3)]
filesVert = [topDirMeasVert+str(i)+"mmres" for i in range(-9,15,3)]

### Directories and file names for coaxial wire measurements
topDirMeasVertMatched = "./transMeasExamples/CoaxialWireMethod/mkiVertTrans/"
filesVertMatched = [topDirMeasVertMatched+"Y"+str(i)+"MM.S2P" for i in range(-15,12,3)]

topDirMeasVertMatchedLowFreq = "./transMeasExamples/CoaxialWireMethod/mkiVertTransLowFreq/"
filesVertMatchedLowFreq = [topDirMeasVertMatchedLowFreq+"Y"+str(i)+"MM.S2P" for i in range(-15,12,3)]

topDirMeasHorzMatched = "./transMeasExamples/CoaxialWireMethod/mkiHorzTrans/"
filesHorzMatched = [topDirMeasHorzMatched+"X"+str(i)+"MM.S2P" for i in range(-9,18,3)]

topDirMeasHorzMatchedLowFreq = "./transMeasExamples/CoaxialWireMethod/mkiHorzTransLowFreq/"
filesHorzMatchedLowFreq = [topDirMeasHorzMatchedLowFreq+"X"+str(i)+"MM.S2P" for i in range(-9,15,3)]

### Calculate impedance from data filesHorz
datHorz = []
for entry in filesHorz:
    freq, impe = imp.resImpGetFromFileTrans(entry)
    tmp = []
    for i in range(0,len(freq)):
        tmp.append([freq[i],impe[i]])
    datHorz.append(tmp)

tempHorz = []

for i in range(0,len(datHorz[0])-1):
    tempSlice = [datHorz[0][i][0]]
    for j in range(0,len(datHorz)):
        tempSlice.append(datHorz[j][i][1])
    tempHorz.append(tempSlice)

impHorz = sp.array(tempHorz)

datVert = []
for entry in filesVert:
    freq, impe = imp.resImpGetFromFileTrans(entry)
    tmp = []
    for i in range(0,len(freq)):
        tmp.append([freq[i],impe[i]])
    datVert.append(tmp)

tempVert = []

for i in range(0,len(datVert[0])-1):
    tempSlice = [datVert[0][i][0]]
    for j in range(0,len(datVert)):
        tempSlice.append(datVert[j][i][1])
    tempVert.append(tempSlice)

impVert = sp.array(tempVert)

### Define displacements of wire
xDisplacements = sp.linspace(-15,9,9)
yDisplacements = sp.linspace(-9,12,8)

### function impTransParaRes calculates longitudinal and total transverse impedance
### from a series of displaced wire measurements. Input displacement should be given
### as a scipy array of displacements in millimetres.
longHorz, totHorz = imp.impTransParaRes(xDisplacements, impHorz, 0)
longVert, totVert = imp.impTransParaRes(yDisplacements, impVert, 0)

### Importing coaxial measurements data
datMatchedVertTemp = []
for fileEn in filesVertMatched:
    datMatchedVertTemp.append(imp.readS21FromVNA(fileEn))
datMatchedVertTemp=sp.array(datMatchedVertTemp)
yDisplacements = sp.linspace(-15,9,9)
### function fitQuadraticSquaresImp takes a series of impedance values and displacements
### and returns the longitudinal and total transverse impedance and an error jacobian
### that can be used to generate error bars for the fit. Note real and imaginary impedances calculated
longitudinalVert, totTransVert, errorVert = imp.fitQuadraticSquaresImp(datMatchedVertTemp, yDisplacements)

### function errorPlottables returns an array compatible with errorfill to plot
### error bars
errVertReal, errVertImag = imp.errorPlottables(errorVert, totTransVert)

datMatchedVertTempLowFreq = []
for fileEn in filesVertMatchedLowFreq:
    datMatchedVertTempLowFreq.append(imp.readS21FromVNA(fileEn))
datMatchedVertTempLowFreq=sp.array(datMatchedVertTempLowFreq)
yDisplacements = sp.linspace(-15,9,9)
longitudinalVertLowFreq, totTransVertLowFreq, errorVertLowFreq = imp.fitQuadraticSquaresImp(datMatchedVertTempLowFreq, xDisplacements)

errVertLowFreqReal, errVertLowFreqImag = imp.errorPlottables(errorVertLowFreq, totTransVertLowFreq)

datMatchedHorzTemp = []
for fileEn in filesHorzMatched:
    datMatchedHorzTemp.append(imp.readS21FromVNA(fileEn))
datMatchedHorzTemp=sp.array(datMatchedHorzTemp)
xDisplacements = sp.linspace(9,15,9)
longitudinalHorz, totTransHorz, errorHorz = imp.fitQuadraticSquaresImp(datMatchedHorzTemp, xDisplacements)

errHorzReal, errHorzImag = imp.errorPlottables(errorHorz, totTransHorz)

datMatchedHorzTempLowFreq = []
for fileEn in filesHorzMatchedLowFreq:
    datMatchedHorzTempLowFreq.append(imp.readS21FromVNA(fileEn))
datMatchedHorzTempLowFreq=sp.array(datMatchedHorzTempLowFreq)
xDisplacements = sp.linspace(9,12,8)
longitudinalHorzLowFreq, totTransHorzLowFreq, errorHorzLowFreq = imp.fitQuadraticSquaresImp(datMatchedHorzTempLowFreq, xDisplacements)

errHorzLowFreqReal, errHorzLowFreqImag = imp.errorPlottables(errorHorzLowFreq, totTransHorzLowFreq)

### Plot impedances
pl.semilogx()
pl.plot(impHorz[:,0]/10**9, (longHorz), "kx",markersize=16.0, label="Z$_{\parallel, x}$ Resonant, Old Design")
pl.plot(impHorz[:,0]/10**9, (longHorz), "k-")
##pl.plot(impVert[:,0]/10**9, longVert, "gx",label="Z$_{\parallel, y}$ Resonant")
##pl.plot(impVert[:,0]/10**9, longVert, "g-")
##pl.plot(longitudinalVert[:,0]/10**9, signal.detrend(longitudinalVert[:,1]-longitudinalVert[0,1])/lDUT, "k-", label="$\Re{}$e(Z$_{\parallel, y}$), Transmission")
##pl.plot(longitudinalVert[:,0]/10**9, longitudinalVert[:,2]-longitudinalVert[0,2], "r-", label="$\Im{}$m(Z$_{\parallel, y}$)")
##pl.plot(longitudinalVertLowFreq[:,0]/10**9, longitudinalVertLowFreq[:,1]-longitudinalVertLowFreq[-1,1], "k--", label="$\Re{}$e(Z$_{\parallel, y}$), Transmission Low Freq")
##pl.plot(longitudinalVertLowFreq[:,0]/10**9, longitudinalVertLowFreq[:,2]-longitudinalVertLowFreq[0,2], "r--", label="$\Im{}$m(Z$_{\parallel, y}$)")
pl.plot(longitudinalHorz[:,0]/10**9, (longitudinalHorz[:,1]-longitudinalHorz[0,1])/lDUT, "k-", label="$\Re{}$e(Z$_{\parallel, x}$), Old Design")
pl.plot(longitudinalHorz[:,0]/10**9, (longitudinalHorz[:,2]-longitudinalHorz[0,2]), "r-", label="$\Im{}$m(Z$_{\parallel, x}$)")
pl.plot(longitudinalHorzLowFreq[:,0]/10**9, (longitudinalHorzLowFreq[:,1]-500), "k--", label="$\Re{}$e(Z$_{\parallel, x}$)")
pl.plot(longitudinalHorzLowFreq[:,0]/10**9, (longitudinalHorzLowFreq[:,2]-500), "r--", label="$\Im{}$m(Z$_{\parallel, x}$)")

pl.xlabel("Frequency (GHz)", fontsize=24.0)
pl.ylabel("$(Z_{\parallel})$ ($\Omega$/m)", fontsize=24.0)
pl.legend(loc="upper left")
##pl.xlim(10**-4,2.5)
pl.ylim(-10,70)
pl.show()
pl.clf()

### Plot Transverse impedances
##pl.loglog()
##pl.semilogy()
pl.semilogx()
##pl.plot(impHorz[:,0]/10**9, sp.array(totHorz)/10.0**-6/10**6, "b-",label="Z$_{\perp,x}$$^{Tot}$")
##pl.plot(impVert[:,0]/10**9, sp.array(totVert)/10.0**-6/10**6, "g-", label="Z$_{\perp,y}$$^{Tot}$")
##pl.plot(impHorzNewDesign[:,0]/10**9, sp.array(totHorzNewDesign)/10.0**-6/10**6, "r-",label="Z$_{\perp,x}$$^{Tot}$, new design")
##pl.plot(impVertNewDesign[:,0]/10**9, sp.array(totVertNewDesign)/10.0**-6/10**6, "k-", label="Z$_{\perp,y}$$^{Tot}$, new design")
##pl.plot(totTransVert[:,0]/10**9, totTransVert[:,1]/10.0**-6/10**6, "k-", label="$\Re{}$e(Z$_{\perp,y}$$^{Tot}$)")
##pl.plot(totTransVert[:,0]/10**9, totTransVert[:,2]/10.0**-6/10**6, "r-", label="$\Im{}$m(Z$_{\perp,y}$$^{Tot}$)")

### errorfill plots impedances with error bars from fitting.
imp.errorfill(totTransVert[:,0]/10**9, totTransVert[:,1]/10.0**-6/10**6, yerr = errVertReal/10**6, color="k", label="$\Re{}$e(Z$_{\perp,y}$$^{Tot}$)")
imp.errorfill(totTransVert[:,0]/10**9, totTransVert[:,2]/10.0**-6/10**6, yerr = errVertImag/10**6, color="r", label="$\Im{}$m(Z$_{\perp,y}$$^{Tot}$)")
imp.errorfill(totTransVertLowFreq[:,0]/10**9, totTransVertLowFreq[:,1]/10.0**-6/10**6, yerr = errVertLowFreqReal/10**6, color="k", label="$\Re{}$e(Z$_{\perp,y}$$^{Tot}$), Low Freq")
imp.errorfill(totTransVertLowFreq[:,0]/10**9, totTransVertLowFreq[:,2]/10.0**-6/10**6, yerr = errVertLowFreqImag/10**6, color="r", label="$\Im{}$m(Z$_{\perp,y}$$^{Tot}$), Low Freq")


##pl.plot(totTransVertLowFreq[:,0]/10**9, abs(totTransVertLowFreq[:,1]/10.0**-6)/10**6, "k--", label="$\Re{}$e(Z$_{\perp,y}$$^{Tot}$)")
##pl.plot(totTransVertLowFreq[:,0]/10**9, abs(totTransVertLowFreq[:,2]/10.0**-6)/10**6, "r--", label="$\Im{}$m(Z$_{\perp,y}$$^{Tot}$)")
##pl.plot(totTransHorz[:,0]/10**9, (totTransHorz[:,1]/10.0**-6)/(10**6), "k-", label="$\Re{}$e(Z$_{\perp,x}$$^{Tot}$), Old Design")
##pl.plot(totTransHorz[:,0]/10**9, (totTransHorz[:,2]/10.0**-6)/10**6, "r-", label="$\Im{}$m(Z$_{\perp,x}$$^{Tot}$), Old Design")
##pl.plot(totTransHorzLowFreq[:,0]/10**9, (totTransHorzLowFreq[:,1]/10.0**-6)/(10**6), "k-")
##pl.plot(totTransHorzLowFreq[:,0]/10**9, (totTransHorzLowFreq[:,2]/10.0**-6)/10**6, "r-")


##imp.errorfill(totTransHorz[:,0]/10**9, totTransHorz[:,1]/10.0**-6/10**6, yerr = errHorzReal/10**6, color="k", label="$\Re{}$e(Z$_{\perp,x}$$^{Tot}$), Old Design")
##imp.errorfill(totTransHorz[:,0]/10**9, totTransHorz[:,2]/10.0**-6/10**6, yerr = errHorzImag/10**6, color="r", label="$\Im{}$m(Z$_{\perp,x}$$^{Tot}$), Old Design")
##imp.errorfill(totTransHorzLowFreq[:,0]/10**9, totTransHorzLowFreq[:,1]/10.0**-6/10**6, yerr = errHorzLowFreqReal/10**6, color="k")
##imp.errorfill(totTransHorzLowFreq[:,0]/10**9, totTransHorzLowFreq[:,2]/10.0**-6/10**6, yerr = errHorzLowFreqImag/10**6, color="r")

pl.legend(loc="upper right")
##pl.ylim(-50,200)
pl.xlim(10**-3,2)
pl.ylim(-3,5)
##pl.xlim(10**-4,2)
pl.xlabel("Frequency (GHz)", fontsize=24.0)
pl.ylabel("$(Z_{\perp})$ (M$\Omega$/m)", fontsize=24.0)
pl.show()
pl.clf()

##### Section on Warming Up #####

### function heatingValGaussRes calculates power loss from a resonant impedance measurement.
### An interpolation is used to acquire data points so is subject to measurement frequency
### resolution issues.
resImpForPowOld=imp.concatArrRes(impHorz[:,0],longHorz)
powLossOldRes, powLossFreqPartOldRes, cumPowOldRes = imp.heatingValGaussRes(resImpForPowOld, 0.5, 25*10.0**-9, 1*10.0**-9)

print powLossOldRes

pl.plot(cumPowOldRes[:,0]/10**9, cumPowOldRes[:,1])
##pl.show()
pl.clf()
