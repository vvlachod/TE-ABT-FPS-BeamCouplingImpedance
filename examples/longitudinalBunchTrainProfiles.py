import csv, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
sys.path.append("../")
import impedance.impedance as imp
import impedance.powlossprof as prof

"""An example of longitudinal bunch profiles depending on bunch spacing,
bunch length, bunch train length, number of bunches and bunches per train
"""

C = 299792458.0 # Setting speed of light

### Setting some LHC bunch parameters ###

f_rev = 3*10**8/27000.0
nBunches = 2808
qPart = 1.6*10.0**-19
nPart=1.15*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=4*0.2/(3.0*10.0**8)
##bLength=0.67*10.0**-9
testImp = [250*10**6, 10.0, 10000]
timeRes = 0.08*10.0**-9
bunRange = 50

print bLength

##### A variety of beam parameters for SPS, LHC, and FCC type beams.
##### These can be used as a basis for further exploration with different
##### beam parameters and to explore how different components of power spectra
##### as sensitive to which parameters

###### Single bunch
##bunSpacing = 24.97*10.0**-9
##trainSpacing = 8*bunSpacing
##bunInTrain = 1
##trainsInMachine = 1
##blankBunSpaces = 562


####### 25ns Spec SPS
##
bunSpacing = 24.97*10.0**-9
trainSpacing = 8*bunSpacing
bunInTrain = 72
trainsInMachine = 4
blankBunSpaces = 562
print 43450
##### 50ns Spec SPS

##bunSpacing = 2*24.97*10.0**-9
##trainSpacing = 224.6*10.0**-9
##bunInTrain = 36
##trainsInMachine = 4
##blankBunSpaces = 287
##print 43450
##### 25ns Spec LHC
##
##bunSpacing = 24.97*10.0**-9
##trainSpacing = 8*bunSpacing
##bunInTrain = 288
##trainsInMachine = 10
##blankBunSpaces = 290
##print 11245
##### 50ns Spec LHC

##bunSpacing = 2*24.97*10.0**-9
##trainSpacing = 2*224.6*10.0**-9
##bunInTrain = 144
##trainsInMachine = 10
##blankBunSpaces = 165
##print 11245
##### 25ns Spec FCC

##bunSpacing = 24.97*10.0**-9
##trainSpacing = 12*bunSpacing
##bunInTrain = 144
##trainsInMachine = 90
##blankBunSpaces = 610
print 100000/C

##### ESS Spec

##bunSpacing = 2.841*10.0**-9
##trainSpacing = 8*bunSpacing
##bunInTrain = 72
##trainsInMachine = 4
##blankBunSpaces = 562
##beamCur = 62.5*10.0**-3
print 43450

print bunInTrain*trainsInMachine

print "Bunch Harmonic:"+str(1/bunSpacing)+"Hz"
print "Train Harmonic:"+str(1/trainSpacing)+"Hz"
print "Buch Harmonic:"+str(1/bunSpacing/trainsInMachine/bunInTrain)+"Hz"

#### Definition is time domain profile

#### length of array set taking into account bunch spacing, no. bunches per trains
#### no. trains and spacing between trains
spaceCount = ((trainsInMachine*bunInTrain*bunSpacing)+(trainSpacing*(trainsInMachine+1))+(bunInTrain*trainsInMachine*bLength)+bunSpacing*blankBunSpaces)/timeRes
#### amplitude of zero set
ampListTime = [0 for i in range(0,int(spaceCount))]
print spaceCount
timing = []
for i in range(0,len(ampListTime)):
    timing.append(i*timeRes)

bunLimit = int((bLength)/timeRes)

#### Create time profile, using train structure and separation

for i in range(0,trainsInMachine):
    for j in range(0,bunInTrain):
        midPoint = int(((j*bunSpacing)+(i*trainsInMachine*trainSpacing)+(i*bunInTrain*bunSpacing)+bLength)/timeRes)
##        print midPoint, bunLimit, i
        for k in range(midPoint-bunLimit, midPoint+bunLimit):
            try:
                ampListTime[k] = prof.gaussProfTime(abs(k-midPoint)*timeRes, bLength, 31)
            except:
##                print i, j, j
                pass
##            ampListTime[k] = prof.cosProfTime(abs(k-midPoint)*timeRes, bLength)
##            ampListTime[k] = prof.paraProfTime(abs(k-midPoint)*timeRes, bLength)


#### Plotting time profile

pl.plot(timing, ampListTime)
pl.axis([0,timing[-1], -0.5,1.5])
pl.xlabel("Time (s)", fontsize=16.0)
pl.ylabel("Amplitude", fontsize=16.0)
pl.show()
pl.clf()

#### FFT to time domain

N = len(timing)
f = 1/timeRes*sp.r_[0:(N/2)]/N
print sp.r_[0:(N/2)]
n= len(f)
ampListFreq = sp.fft(ampListTime)
ampListFreq = ampListFreq[0:n]/sp.amax(ampListFreq)
ampListFreqLog = imp.linToLog(ampListFreq)
temp = abs(ampListFreq)

freqListHeating = sp.linspace(40,2000,2000/40)
##interpFreqLin = interp.InterpolatedUnivariateSpline(freqListHeating/10**6, temp)
##plotHeating = interpFreqLin(freqListHeating)

print f[1]-f[0]
##print len(f), len(ampListFreq)

freqList = sp.linspace(10**3,2*10**9,10**6)
gausProfSPS = prof.gaussProfFreq(freqList, 0.8/(3*10**8))

#### Plotting in frequency domain

pl.plot(f/10**9, ampListFreqLog)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("S (dB)", fontsize=16.0)
##pl.axis([0.03,0.405,-60,0])
pl.xlim(0.0,2.0)
pl.ylim(-60,0)
pl.show()
pl.clf()

pl.plot(f/10**9, abs(ampListFreq))
##pl.plot(freqListHeating/10**3, plotHeating, "bx")
pl.plot(freqList/10**9, gausProfSPS)
for i in range(1,2):
    pl.axvline(1/bunSpacing/10**9)
    for j in range(-10,10):
        pl.axvline((1/bunSpacing+j/trainSpacing/trainsInMachine)/10**9)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("S (au)", fontsize=16.0)
##pl.axis([0.395,0.405,-60,0])
pl.xlim(0.03,0.05)
pl.ylim(0,1.0)
##pl.show()
pl.clf()

freqList = []
for j in range(1,30001):
    freqList.append(j*(10.0**5))

freqList = list(set(freqList))
freqList.sort()


