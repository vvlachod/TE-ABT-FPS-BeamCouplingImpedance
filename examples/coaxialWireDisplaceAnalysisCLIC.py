"""
This example introduces coaxial wire measurements, both longitudinal, displaced single wire and two wire
measurements, using the CLIC stripline kicker prototypes as the device under test (DUT)
"""

import scipy as sp
import scipy.optimize as op
import pylab as pl
import time, csv, os, sys
sys.path.append("../")
import impedance.impedance as imp

#### Declare some system variables

C = 299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005

#### Declare a list of files representing data for displaced single wires

fileLoadList = ["./singleWireDisplaceCLIC/POINT"+str(i)+"LOAD.S2P" for i in range(0,5,1)]
fileLoadListBis = ["./singleWireDisplaceCLIC/POINT"+str(i)+"BISLOAD.S2P" for i in range(4,0,-1)]

fileLoadList = fileLoadListBis+fileLoadList

#### Create a 3D array of S parameters representing varying displacements of wire

dataLoadTemp = []
for fileEn in fileLoadList:
    dataLoadTemp.append(imp.readS21FromVNA(fileEn))

dataTransLoadFact = sp.array(dataLoadTemp)

#### Import measurements from two wire measurements

fileTwoHorz = "./singleWireDisplaceCLIC/transversetwowires/horizontalplane/TWOWIRESIDEAL.S2P"
fileTwoVert = "./singleWireDisplaceCLIC/transversetwowires/verticalplane/TWOWIRESIDEALVERT.S2P"

#### Read array of S parameters

datTwoHorz = sp.array(imp.readS21FromVNA(fileTwoHorz))
datTwoVert = sp.array(imp.readS21FromVNA(fileTwoVert))

#### function impTwoWire returns an array, dimension 0 being frequency and dimension 1 being dipolar
#### beam coupling impedance

impTwoHorz=imp.impTwoWire(datTwoHorz)
impTwoVert=imp.impTwoWire(datTwoVert)

#### Define displacements of wires

wireDisplacements=sp.array([-0.02,-0.015,-0.01,-0.005,0,0.005,0.01,0.015,0.02])

#### Call function fitQuadraticSquaresImp which returns 3 variables, an array of longitudinal experience
#### total transverse impedance fitted using least sqaures algorithm and the regression coefficient for
#### calculating the correctness of the fit.

longitudinalCLICLoad, totTransCLICLoad, errTransLoad = imp.fitQuadraticSquaresImp(dataTransLoadFact, wireDisplacements)

#### Plot longitudinal impedance

pl.plot(longitudinalCLICLoad[:,0]/10**9,longitudinalCLICLoad[:,1]-longitudinalCLICLoad[0,1], label="Constant Term from fitting Load")
pl.plot(dataTransLoadFact[4,:,0]/10**9,imp.logImpFormula(imp.logToLin(dataTransLoadFact[4,:,3]),1,235)-imp.logImpFormula(imp.logToLin(dataTransLoadFact[4,0,3]),1,235), label="Measurement at 0mm displacement Load")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("Z$_{\parallel}$ ($\Omega$)", fontsize=16.0)
pl.legend(loc="upper left")
pl.xlim(0,1.0)
pl.ylim(0,300)
pl.show()
pl.clf()

#### Plot Total transverse impedance

pl.plot(totTransCLICLoad[:,0]/10**9,totTransCLICLoad[:,1]/10**3)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("Z$_{\perp}$$^{Total}$ (k$\Omega$/m)", fontsize=16.0)
pl.show()
pl.clf()

#### Plot transverse impedances (dipolar, quadrupolar and total transverse)

##pl.semilogy()
pl.plot(totTransCLICLoad[:,0]/10**9,totTransCLICLoad[:,1]/10**3)
pl.plot(impTwoHorz[:,0]/10**9,impTwoHorz[:,1]/10**3)
pl.plot(totTransCLICLoad[:,0]/10**9,(totTransCLICLoad[:,1]-impTwoHorz[:,1])/10**3)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("Z$_{\perp}$$^{Dip}$ (k$\Omega$/m)", fontsize=16.0)
pl.show()
pl.clf()

#### Plot dipolar transverse impedance

pl.plot(impTwoVert[:,0]/10**9,impTwoVert[:,1]/10**3)
pl.plot(impTwoHorz[:,0]/10**9,impTwoHorz[:,1]/10**3)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("Z$_{\perp}$$^{Dip}$ (k$\Omega$/m)", fontsize=16.0)
pl.show()
pl.clf()
