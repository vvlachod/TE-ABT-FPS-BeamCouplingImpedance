import csv, time, os, sys
import scipy as sp
import numpy as np
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack
from scipy import signal

####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

######### Define importing, analysis functions #########

c=299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005
lWire = 3.551
lDUT = 2.45
resCopper = 17.24*10.0**-9
resOuter = 1/(3.3*10.0**7)
ZChar = 60*sp.log(dAper/rWire)

def overlapFreqMKI(lenOverlap, harmonic, permitivitty,delta):
    return harmonic*C/(2*permitivitty**0.5*(lenOverlap+delta))

def importDatSimMeas(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
    datStore = []
    for entry in tempDat[1:]:
        temp = map(float, entry.split(','))
        datStore.append(temp)

    return datStore

def analSingReSimMeas(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    reStore = []
    temp = []
##    Z0 = 60*sp.log(1.27*apPipe/r_wire)
    Z0=50.0
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(-2*Z0/lenTot*sp.log(10**(data[i,j]/20))) 
        temp.append(tempLin)
    return temp

def fileImport(tarFile):
    readPart=open(tarFile, 'r+')
    datUnPro = readPart.readlines()
    readPart.close()
    freqDat=[]
    qLoaded=[]
    s21DB=[]
    for row in datUnPro[20:]:
        freqDat.append(float(row.split(",")[0]))
        qLoaded.append(float(row.split(",")[1]))
        s21DB.append(float(row.split(",")[2]))

    return sp.array(freqDat), sp.array(qLoaded), sp.array(s21DB)

def skinDepth(freq, cond):
    return (1/(sp.pi*freq*mu0*cond))**0.5

def resImpGet(tarArr1,tarArr2,tarArr3,attenCableLen):

    freqList, qTotal, transCoeff = tarArr1, tarArr2, tarArr3

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(C*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCable = attenCableLen*freqList*10.0**-9
    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor

    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper-attenCable)/(attenCopper)

    return freqList, zMeas/lDUT

def resImpGetFromFile(targetFile):

    freqList, qTotal, transCoeff = fileImport(targetFile)

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(c*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor


    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper)/attenCopper

    return freqList, zMeas/lDUT

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def extract_dat2013(file_name):
    data = open(file_name, "r+")
    tar = data.readlines()
    temp=[]
    for row in tar[2:]:
        row = map(float, row.split())
        temp.append(row)
    data.close()

    return temp[:-1]

def sParaRead(fileTar, lenDUT, col):
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        phase = row[col+1]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360    

        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        import_dat = [row[0], row[col], sp.radians(phase), -s21Pec]
        data.append(import_dat)
        last = phase

    return data

def readS21FromVNA(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[6:]:
        temp=line.split("\t")
        outputData.append(map(float,temp))
    return outputData

def readS21FromCSV(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[3:]:
        temp=line.split(",")
        outputData.append(map(float,temp))
    return outputData

def impAnalysisSingle(fileTar, lenDUT, r_wire, r_pipe, Zc):
    ###### Analyses a S2P file for measurement of longitudinal impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        phase = row[4]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360


        import_dat = [row[0], -2*Zc*sp.log(linDat), -2*Zc*(sp.radians(phase)+s21Pec)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDUT), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDUT))]
        data.append(import_dat)
        last = phase

    return data

def impAnalysisTwo(fileTar, lenDut, r_wire, r_pipe, d_wire, Zc):
    ###### Analyses a S2P file for measurement of dipole impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter = 0.0
    last = 1e8

    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDut/C
        phase = row[4]-counter
        if phase>last and (phase-last)>180:
            count+=1
            counter+=360
            pgase-=360
        import_dat = [row[0], -2*Zc*sp.log(linDat)*C/(2*sp.pi*row[0]*d_wire**2), -2*Zc*(sp.radians(phase)+s21Pec)*C/(2*sp.pi*row[0]*d_wire**2)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDut)*C/(row[0]*d_wire**2), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDut))*C/(row[0]*d_wire**2)]
        data.append(import_dat)
    return data

def impTransPara(dispWire, measImp, wireErr):
##### Takes an array of longitudinal measurements and returns the transverse impedance
    quadFunc = lambda p, x: p[0] + x * p[1] + x**2 * p[2]
    errFunc = lambda p, x, Z: (Z-quadFunc(p,x))

    totTrans = []
    longComp = []

    for i in range(0,len(measImp[0,:,1])):
        pinit = [1.0, -1.0, 1.0]
        ZerrRe = measImp[:,i,1]*wireErr
        pReal, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,1]), full_output = 0)
        pinit = [1.0, -1.0, 1.0]
        ZerrIm = measImp[:,i,2]*wireErr
        pImag, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,2]), full_output = 0)
        totTrans.append([pReal[2]*C/(2*sp.pi*measImp[0,i,0]), pImag[2]*C/(2*sp.pi*measImp[0,i,0])])
        longComp.append([pReal[0], pImag[0]])

    return longComp, totTrans

def quadImp(totalTrans, dipTrans, axis):
######## Returns the quadrupolar impedance from total transverse and dipolar data
    quadImp = []
    for i in range(0,len(totalTrans)):
        if axis == "horz":
            quadImp.append([totalTrans[i,1]+dipTrans[i,1],totalTrans[i,2]+dipTrans[i,2]])
        elif axis == "vert":
            quadImp.append([totalTrans[i,1]-dipTrans[i,1],totalTrans[i,2]-dipTrans[i,2]])
    return quadImp


def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def peakFitSimulations(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 50
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 50 and y_val[current_max_x]>2:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [0.2, 10.0, 10.0]
        lower_bound = res[0]-50
        upper_bound = res[0]+50
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def peakFitMeasurements(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 0
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 2 and y_val[current_max_x]>3:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [res[1], 4.0, data[res[0],1]]
        lower_bound = res[0]-4
        upper_bound = res[0]+3
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2)

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2
                                                    
def logToLin(data):
    return 10**(data/20)

def linToLog(data):
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

def overLapFreq(overlap, fringeLen, harmonic):
    return harmonic*C/(2*10**0.5*(overlap+fringeLen))

def heatLossBunByBunGauss(imp, bunLen, nBun, nParticles, freqRev):
    prodImpSpec = imp[1,:]*gaussProf(imp[0,:]*2*sp.pi,bunLen)**2
    curImpTime=prodImpSpec
    enLossPerBunch=2*(1.6*10**-19*nParticles*freqRev)**2*(curImpTime.sum())
    pLossBeam = enLossPerBunch*nBun**2
    pLossBeamFreq = 2*(1.6*10**-19*nParticles*freqRev)**2*nBun**2*curImpTime
    print pLossBeam, pLossBeamFreq
    return pLossBeam, pLossBeamFreq, gaussProf(imp[0,:],bunLen)**2

def extract_dat2013_MultipleSweeps(file_name):
    data = open(file_name, "r+")
    tar = data.readlines()
    temp=[]
    bigTemp = []
    count=2
    while count<len(tar):
        try:
            row = map(float, tar[count].split())
            temp.append(row)
            count+=1
        except:
            bigTemp.append(temp)
            temp=[]
            count+=3
    data.close()
    bigTemp.append(temp)
    return bigTemp

def freqRLC(L, C, harmonic):
    return harmonic/(L*C)**0.5/(2*sp.pi)

def heatingValGauss(impArr, beamCur, bunSpac, bunLen):
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac/10**9,impArr[-1,0],impArr[-1,0]*10**9*bunSpac)
    splineFitHeatingImp = SplineFitImp(freqListHeating)
    heatingTotalPart = []
    for i in range(0,len(splineFitHeatingImp)):
        heatingTotalPart.append([freqListHeating[i], abs(2*splineFitHeatingImp[i]*(beamCur**2)*gaussProf(freqListHeating[i]*10.0**9, bunLen))])
    heatingTotalPart=sp.array(heatingTotalPart)
    cumHeating = []
    for i in range(0,len(heatingTotalPart)):
        cumHeating.append([heatingTotalPart[i,0],sum(heatingTotalPart[:i,1])])
    cumHeating=sp.array(cumHeating)
    return heatingTotalPart[:,1].sum(), heatingTotalPart, cumHeating

length = 2.88
f_rev = 43400
nBunches = 288
qPart = 1.6*10.0**-19
nPart=1.1*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=4*0.2/C
testImp = [250*10**6, 10.0, 10000]
bunchCur=nPart*qPart*f_rev

topDirMeas = "E:/PhD/1st_Year_09-10/Data/MKP/28-04-15/MKP-S/"
fileTarMKPCen = topDirMeas+"S21FREQNOGATING.S2P"
s21fileTarMKPCen = sp.array(readS21FromVNA(fileTarMKPCen))
impMKPCen = (logImpFormula(logToLin(s21fileTarMKPCen[:,3]),1,311))-1170

fileTarMKPPlus5mm = topDirMeas+"MKPFREQDXBEAM5MM.S2P"
s21TarMKPPlus5mm = sp.array(readS21FromVNA(fileTarMKPPlus5mm))
impMKPPlus5mm = (logImpFormula(logToLin(s21TarMKPPlus5mm[:,3]),1,311))-1170

fileTarMKPPlus10mm = topDirMeas+"MKPFREQDXBEAM10MM.S2P"
s21TarMKPPlus10mm = sp.array(readS21FromVNA(fileTarMKPPlus10mm))
impMKPPlus10mm = (logImpFormula(logToLin(s21TarMKPPlus10mm[:,3]),1,311))-1170

fileTarMKPPlus15mm = topDirMeas+"MKPFREQDXBEAM15MM.S2P"
s21TarMKPPlus15mm = sp.array(readS21FromVNA(fileTarMKPPlus15mm))
impMKPPlus15mm = (logImpFormula(logToLin(s21TarMKPPlus15mm[:,3]),1,311))-1170

fileTarMKPPlus20mm = topDirMeas+"MKPFREQDXBEAM20MM.S2P"
s21TarMKPPlus20mm = sp.array(readS21FromVNA(fileTarMKPPlus20mm))
impMKPPlus20mm = (logImpFormula(logToLin(s21TarMKPPlus20mm[:,3]),1,311))-1170

fileTarMKPPlus30mm = topDirMeas+"S21FREQXPLUS30MM.S2P"
s21TarMKPPlus30mm = sp.array(readS21FromVNA(fileTarMKPPlus30mm))
impMKPPlus30mm = (logImpFormula(logToLin(s21TarMKPPlus30mm[:,3]),1,311))-1170

fileTarMKPPlus35mm = topDirMeas+"S21FREQXPLUS35MM.S2P"
s21TarMKPPlus35mm = sp.array(readS21FromVNA(fileTarMKPPlus35mm))
impMKPPlus35mm = (logImpFormula(logToLin(s21TarMKPPlus35mm[:,3]),1,311))-1170

fileTarMKPPlus40mm = topDirMeas+"S21FREQXPLUS40MM.S2P"
s21TarMKPPlus40mm = sp.array(readS21FromVNA(fileTarMKPPlus40mm))
impMKPPlus40mm = (logImpFormula(logToLin(s21TarMKPPlus40mm[:,3]),1,311))-1170

fileTarMKPPlus45mm = topDirMeas+"S21FREQXPLUS45MM.S2P"
s21TarMKPPlus45mm = sp.array(readS21FromVNA(fileTarMKPPlus45mm))
impMKPPlus45mm = (logImpFormula(logToLin(s21TarMKPPlus45mm[:,3]),1,311))-1170

fileTarMKPMinus7mm = topDirMeas+"MKPFREQDXBEAM-7MM.S2P"
s21TarMKPMinus7mm = sp.array(readS21FromVNA(fileTarMKPMinus7mm))
impMKPMinus7mm = (logImpFormula(logToLin(s21TarMKPMinus7mm[:,3]),1,311))-1170

fileTarMKPCentreSeri = "E:/PhD/1st_Year_09-10/Data/MKP/07-09-15/mkpSerigraphy/S21FREQMATCHING.S2P"
s21TarMKPCentreSeri = sp.array(readS21FromVNA(fileTarMKPCentreSeri))
impMKPCentreSeri = (logImpFormula(logToLin(s21TarMKPCentreSeri[:,3]),1,311))-1170

fileTarMKPCentreSeriLowFreq = "E:/PhD/1st_Year_09-10/Data/MKP/07-09-15/mkpSerigraphy/S21FREQMATCHINGLOWFREQ.S2P"
s21TarMKPCentreSeriLowFreq = sp.array(readS21FromVNA(fileTarMKPCentreSeriLowFreq))
impMKPCentreSeriLowFreq = (logImpFormula(logToLin(s21TarMKPCentreSeriLowFreq[:,3]),1,311))-1170

fileTarMKPCentreSeriLoad = "E:/PhD/1st_Year_09-10/Data/MKP/15-09-15/mkpLoad/S21FREQMATCHINGLOAD.S2P"
s21TarMKPCentreSeriLoad = sp.array(readS21FromVNA(fileTarMKPCentreSeriLoad))
impMKPCentreSeriLoad = (logImpFormula(logToLin(s21TarMKPCentreSeriLoad[:,3]),1,311))-1200

fileTarMKPCentreSeriLoadLowFreq = "E:/PhD/1st_Year_09-10/Data/MKP/15-09-15/mkpLoad/S21FREQMATCHEDLOADLOW.S2P"
s21TarMKPCentreSeriLoadLowFreq = sp.array(readS21FromVNA(fileTarMKPCentreSeriLoadLowFreq))
impMKPCentreSeriLoadLowFreq = (logImpFormula(logToLin(s21TarMKPCentreSeriLoadLowFreq[:,3]),1,311))-1200


freqListHeating = sp.linspace(40,1500,int(1500/40.0))
print int(1500/40.0)
SplineFitMKPCen = interp.InterpolatedUnivariateSpline(s21fileTarMKPCen[:,0]/10**6, impMKPCen)
splineFitHeatingMKPCen = SplineFitMKPCen(freqListHeating)

SplineFitMKPPlus5mm = interp.InterpolatedUnivariateSpline(s21TarMKPPlus5mm[:,0]/10**6, impMKPPlus5mm)
splineFitHeatingMKPPlus5mm = SplineFitMKPPlus5mm(freqListHeating)

SplineFitMKPPlus10mm = interp.InterpolatedUnivariateSpline(s21TarMKPPlus10mm[:,0]/10**6, impMKPPlus10mm)
splineFitHeatingMKPPlus10mm = SplineFitMKPPlus10mm(freqListHeating)

SplineFitMKPPlus15mm = interp.InterpolatedUnivariateSpline(s21TarMKPPlus15mm[:,0]/10**6, impMKPPlus15mm)
splineFitHeatingMKPPlus15mm = SplineFitMKPPlus15mm(freqListHeating)

SplineFitMKPPlus20mm = interp.InterpolatedUnivariateSpline(s21TarMKPPlus20mm[:,0]/10**6, impMKPPlus20mm)
splineFitHeatingMKPPlus20mm = SplineFitMKPPlus20mm(freqListHeating)

SplineFitMKPPlus30mm = interp.InterpolatedUnivariateSpline(s21TarMKPPlus30mm[:,0]/10**6, impMKPPlus30mm)
splineFitHeatingMKPPlus30mm = SplineFitMKPPlus30mm(freqListHeating)

SplineFitMKPPlus35mm = interp.InterpolatedUnivariateSpline(s21TarMKPPlus35mm[:,0]/10**6, impMKPPlus35mm)
splineFitHeatingMKPPlus35mm = SplineFitMKPPlus35mm(freqListHeating)

SplineFitMKPPlus40mm = interp.InterpolatedUnivariateSpline(s21TarMKPPlus40mm[:,0]/10**6, impMKPPlus40mm)
splineFitHeatingMKPPlus40mm = SplineFitMKPPlus40mm(freqListHeating)

SplineFitMKPPlus45mm = interp.InterpolatedUnivariateSpline(s21TarMKPPlus45mm[:,0]/10**6, impMKPPlus45mm)
splineFitHeatingMKPPlus45mm = SplineFitMKPPlus45mm(freqListHeating)

SplineFitMKPMinus7mm = interp.InterpolatedUnivariateSpline(s21TarMKPMinus7mm[:,0]/10**6, impMKPMinus7mm)
splineFitHeatingMKPMinus7mm = SplineFitMKPPlus20mm(freqListHeating)


heatingTotalMKPCen = 0.0
heatingFreqMKPCen = []
heatingCumFreqMKPCen = []
heatingTotalMKPPlus5mm = 0.0
heatingFreqMKPPlus5mm = []
heatingCumFreqMKPPlus5mm = []
heatingTotalMKPPlus10mm = 0.0
heatingFreqMKPPlus10mm = []
heatingCumFreqMKPPlus10mm = []
heatingTotalMKPPlus15mm = 0.0
heatingFreqMKPPlus15mm = []
heatingCumFreqMKPPlus15mm = []
heatingTotalMKPPlus20mm = 0.0
heatingFreqMKPPlus20mm = []
heatingCumFreqMKPPlus20mm = []
heatingTotalMKPPlus30mm = 0.0
heatingFreqMKPPlus30mm = []
heatingCumFreqMKPPlus30mm = []
heatingTotalMKPPlus35mm = 0.0
heatingFreqMKPPlus35mm = []
heatingCumFreqMKPPlus35mm = []
heatingTotalMKPPlus40mm = 0.0
heatingFreqMKPPlus40mm = []
heatingCumFreqMKPPlus40mm = []
heatingTotalMKPPlus45mm = 0.0
heatingFreqMKPPlus45mm = []
heatingCumFreqMKPPlus45mm = []
##
for i in range(0,len(splineFitHeatingMKPCen)):
    heatingTotalMKPCen+=2*splineFitHeatingMKPCen[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKPCen.append(2*splineFitHeatingMKPCen[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreqMKPCen.append(sp.sum(heatingFreqMKPCen))
    heatingTotalMKPPlus5mm+=2*splineFitHeatingMKPPlus5mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKPPlus5mm.append(2*splineFitHeatingMKPPlus5mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreqMKPPlus5mm.append(sp.sum(heatingFreqMKPPlus5mm))
    heatingTotalMKPPlus10mm+=2*splineFitHeatingMKPPlus10mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKPPlus10mm.append(2*splineFitHeatingMKPPlus10mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreqMKPPlus10mm.append(sp.sum(heatingFreqMKPPlus10mm))
    heatingTotalMKPPlus15mm+=2*splineFitHeatingMKPPlus15mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKPPlus15mm.append(2*splineFitHeatingMKPPlus15mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreqMKPPlus15mm.append(sp.sum(heatingFreqMKPPlus15mm))
    heatingTotalMKPPlus20mm+=2*splineFitHeatingMKPPlus20mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKPPlus20mm.append(2*splineFitHeatingMKPPlus20mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreqMKPPlus20mm.append(sp.sum(heatingFreqMKPPlus20mm))
    heatingTotalMKPPlus30mm+=2*splineFitHeatingMKPPlus30mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKPPlus30mm.append(2*splineFitHeatingMKPPlus30mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreqMKPPlus30mm.append(sp.sum(heatingFreqMKPPlus30mm))
    heatingTotalMKPPlus35mm+=2*splineFitHeatingMKPPlus35mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKPPlus35mm.append(2*splineFitHeatingMKPPlus35mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreqMKPPlus35mm.append(sp.sum(heatingFreqMKPPlus35mm))
    heatingTotalMKPPlus40mm+=2*splineFitHeatingMKPPlus40mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKPPlus40mm.append(2*splineFitHeatingMKPPlus40mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreqMKPPlus40mm.append(sp.sum(heatingFreqMKPPlus40mm))
    heatingTotalMKPPlus45mm+=2*splineFitHeatingMKPPlus45mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKPPlus45mm.append(2*splineFitHeatingMKPPlus45mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreqMKPPlus45mm.append(sp.sum(heatingFreqMKPPlus45mm))
    
print heatingTotalMKPCen
print heatingTotalMKPPlus5mm
print heatingTotalMKPPlus10mm
print heatingTotalMKPPlus15mm
print heatingTotalMKPPlus20mm
print heatingTotalMKPPlus30mm
print heatingTotalMKPPlus35mm
print heatingTotalMKPPlus40mm
print heatingTotalMKPPlus45mm
print bCur
pl.plot(freqListHeating, heatingFreqMKPCen)
pl.plot(freqListHeating, heatingFreqMKPPlus5mm)
pl.plot(freqListHeating, heatingFreqMKPPlus10mm)
pl.plot(freqListHeating, heatingFreqMKPPlus15mm)
pl.plot(freqListHeating, heatingFreqMKPPlus20mm)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("Power Loss (W)", fontsize=16.0)
##pl.show()
pl.clf()
##
##pl.plot(s21fileTarMKPCen[:,0]/10**9, impMKPCen, label="$\Delta$x = 0mm")
##pl.plot(s21TarMKPPlus5mm[:,0]/10**9, impMKPPlus5mm, label="$\Delta$x = 5mm")
##pl.plot(s21TarMKPPlus10mm[:,0]/10**9, impMKPPlus10mm, label="$\Delta$x = 10mm")
##pl.plot(s21TarMKPPlus15mm[:,0]/10**9, impMKPPlus15mm, label="$\Delta$x = 15mm")
##pl.plot(s21TarMKPPlus20mm[:,0]/10**9, impMKPPlus20mm, label="$\Delta$x = 20mm")
##pl.plot(s21TarMKPPlus30mm[:,0]/10**9, impMKPPlus30mm, label="$\Delta$x = 30mm")
##pl.plot(s21TarMKPPlus35mm[:,0]/10**9, impMKPPlus35mm, label="$\Delta$x = 35mm")
##pl.plot(s21TarMKPPlus40mm[:,0]/10**9, impMKPPlus40mm, label="$\Delta$x = 40mm")
##pl.plot(s21TarMKPPlus45mm[:,0]/10**9, impMKPPlus45mm, label="$\Delta$x = 45mm")
##pl.plot(s21TarMKPMinus7mm[:,0]/10**9, impMKPMinus7mm, label="$\Delta$x = -7mm")
pl.plot(s21fileTarMKPCen[:,0]/10**9, impMKPCen, label="MKP No Serigraphy")
##pl.plot(s21TarMKPCentreSeri[:,0]/10**9, impMKPCentreSeri, label="MKP Serigraphy, Open termination")
##pl.plot(s21TarMKPCentreSeriLowFreq[:,0]/10**9, impMKPCentreSeriLowFreq, label="MKP Serigraphy, Open termination low freq")
pl.plot(s21TarMKPCentreSeriLoad[:,0]/10**9, impMKPCentreSeriLoad, label="MKP No Serigraphy, Load")
pl.plot(s21TarMKPCentreSeriLoadLowFreq[:,0]/10**9, impMKPCentreSeriLoadLowFreq, label="MKP No Serigraphy, Load low freq")
pl.legend(loc="upper right")
pl.ylim(-50,300)
pl.xlim(0,0.5)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$\Re{}e(Z_{\parallel})$ ($\Omega$)", fontsize=16.0)
pl.show()
pl.clf()

heatList = sp.array([heatingTotalMKPCen,heatingTotalMKPPlus5mm,heatingTotalMKPPlus10mm,heatingTotalMKPPlus15mm,heatingTotalMKPPlus20mm,heatingTotalMKPPlus30mm,
                     heatingTotalMKPPlus35mm,heatingTotalMKPPlus40mm,heatingTotalMKPPlus45mm])
heatNorm=heatingTotalMKPCen/heatList
lengths = sp.array([0,5,10,15,20,30,35,40,45])

pl.plot(lengths, heatList)
##pl.plot(lengths, heatNorm)
pl.xlabel("Displacement towards GND busbar (mm)", fontsize=16.0)
pl.ylabel("Power Loss (W)", fontsize=16.0)
##pl.ylabel("P$_{0}$/P$_{x_{beam}}$", fontsize=16.0)
pl.show()
pl.clf()
