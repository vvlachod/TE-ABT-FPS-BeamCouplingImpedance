import scipy as sp
import pylab as pl
import time, csv, sys, os
import matplotlib.pyplot as plt
import scipy.integrate as inte

########################## Imported Profile ##################################

start = time.time()

#######   SPS parameters for LHC type beam  #######

C = 299792458.0
circ=6911.0
mu0 = 4*sp.pi*10.0**-7

def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def gauss_prof(dist, sigma, order):
    return sp.e**(-order*(dist/(2*sigma))**2)

def para_prof(dist, bunch_length):
    return 1-(2*dist/bunch_length)**2

def cos_prof(dist, bunch_length):
    return sp.cos(sp.pi/bunch_length*dist)**2

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp


##top_directory = "E:/PhD/1st_Year_09-10/Data/UA9_goniometer/impedances/"
##measure_stash_path = "E:/PhD/1st_Year_09-10/Data/UA9_goniometer/impedances/"
top_directory = "E:/PhD/1st_Year_09-10/Data/UA9_goniometer/eigenmode-results/parked_in/with-bellows-ferrite/"
measure_stash_path = "C:/Users/hugo/PhD/Data/UA9-Goniometer/impedances/"
##top_directory = "/media/CORSAIR/impedances/"

in_data = top_directory+"in-ang-impedance/longitudinal-impedance.csv"
out_data = top_directory+"out-ang-impedance/longitudinal-impedance.csv"




##
########### Frequency domain evaluation ###########


########## Frequency domain evaluation ##################"

eigenmode_store = [] ###### first column - freq, second - Q, Third R, Fourth - Power Loss
eigenmode_transverse_disp_y_Ex_store = []
eigenmode_transverse_disp_y_Ey_store = []
eigenmode_transverse_disp_z_Ex_store = []
eigenmode_transverse_disp_z_Ez_store = []
directory_list = [top_directory+i+"/" for i in os.listdir(top_directory)]
count = 0

for work_directory in directory_list:

    if work_directory == top_directory+"tex_output/":
        pass
##    elif work_directory == top_directory_in+"f470/":
##        pass
    else:
##        print work_directory
        file_input = open(work_directory+"e_field_on_axis.fld", 'r+')
        data = file_input.readlines()
        Ez = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ez.append([temp[0],complex(temp[-6],temp[-5])])

        temp = []
        for i in range(0,len(Ez)):
            temp.append([Ez[i][0],complex((Ez[i][1]).real,(Ez[i][1]).imag)])
        Ez = sp.array(temp)

        potential = 0.0
        potential_alt = 0.0
        gap = (Ez[1][0]-Ez[0][0])
        for i in range(0, len(Ez)-1):
            potential_alt += 0.5*abs(Ez[i+1,1]+Ez[i,1])*gap
        potential = inte.simps(abs(Ez[:,1]), Ez[:,0], gap)
        if count == 0:
            pl.plot(Ez[:,0], Ez[:,1])
            pl.show()
            pl.clf()
            count+=1

        file_input = open(work_directory+"q_1.csv", 'r+')
        data = file_input.readlines()
        Q = []
        for i in data[1:]:
            Q.append(map(float, i.rsplit(',')))

        Q = sp.array(Q)
        ##print Q

        file_input = open(work_directory+"stored_energy_in.csv", 'r+')
        data = file_input.readlines()
        stored_energy = []
        for i in data[1:]:
            stored_energy.append(map(float, i.rsplit(',')))

        ##print stored_energy

        file_input = open(work_directory+"freq.csv", 'r+')
        data = file_input.readlines()
        freq = []
        for i in data[1:]:
            temp = map(float, i.rsplit(','))
            freq.append([temp[0],complex(temp[2],temp[1])])

        freq = sp.array(freq)

        gap = Ez[1,0]-Ez[0,0]
        confluence = []
        for i in range(0,len(Ez)):
            confluence.append(Ez[i,1]*complex(sp.cos(freq[0,1]*2*sp.pi*Ez[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ez[i,0]/C)))

        long_r_over_q = (potential**2/(2*sp.pi*freq[0,1]*stored_energy[0][1]))

        long_r_over_q_alt = abs(inte.simps(confluence, Ez[:,0], gap))**2/(2*2*sp.pi*freq[0,1]*stored_energy[0][1])
##        print long_r_over_q/long_r_over_q_alt

####### Calculation of R/Q for transverse mode displaced in y #########

        file_input = open(work_directory+"e_field_displaced_y.fld", 'r+')
        data = file_input.readlines()
        Ey_disp_y = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ey_disp_y.append([temp[0],complex(temp[-4],temp[-3])])

        temp = []
        for i in range(0,len(Ey_disp_y)):
            temp.append([Ey_disp_y[i][0],complex((Ey_disp_y[i][1]).real,(Ey_disp_y[i][1]).imag)])
        Ey_disp_y = sp.array(temp)

        Ex_disp_y = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ex_disp_y.append([temp[0],complex(temp[-6],temp[-5])])

        temp = []
        for i in range(0,len(Ex_disp_y)):
            temp.append([Ex_disp_y[i][0],complex((Ex_disp_y[i][1]).real,(Ex_disp_y[i][1]).imag)])
        Ex_disp_y = sp.array(temp)

        file_input = open(work_directory+"h_field_displaced_y.fld", 'r+')
        data = file_input.readlines()
        Bz_disp_y = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Bz_disp_y.append([temp[0],mu0*complex(temp[-2],temp[-1])])

        temp = []
        for i in range(0,len(Bz_disp_y)):
            temp.append([Bz_disp_y[i][0],complex((Bz_disp_y[i][1]).real,(Bz_disp_y[i][1]).imag)])
        Bz_disp_y = sp.array(temp)

        confluence_Ey = []
        for i in range(0,len(Ey_disp_y)):
            confluence_Ey.append((Ey_disp_y[i,1]+C*Bz_disp_y[i,1])*complex(sp.cos(freq[0,1]*2*sp.pi*Ez[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ez[i,0]/C)))


        kick_factor_Ey_Bz = abs(inte.simps(confluence_Ey, Ey_disp_y[:,0], gap))**2*(2*sp.pi*freq[0,1])/(4*C*stored_energy[0][1])
        r_over_q_Ey_Bz = 4*kick_factor_Ey_Bz/(2*sp.pi*freq[0,1])

        confluence_Ex_disp = []
        for i in range(0,len(Ex_disp_y)):
            confluence_Ex_disp.append((Ex_disp_y[i,1]-Ez[i,1])*complex(sp.cos(freq[0,1]*2*sp.pi*Ez[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ez[i,0]/C))/disp_y)

        kick_factor_Ex = C*abs(inte.simps(confluence_Ex_disp, Ex_disp_y[:,0], gap))**2/(4*2*sp.pi*freq[0,1]*stored_energy[0][1])
        r_over_q_Ex = 4*kick_factor_Ex/(2*sp.pi*freq[0,1])

##        confluence_Ex_disp = []
##        for i in range(0,len(Ex_disp_y)):
##            confluence_Ex_disp.append((Ex_disp_y[i,1])*complex(sp.cos(freq[0,1]*2*sp.pi*Ez[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ez[i,0]/C)))
##
##        r_over_q_Ex = C**2*abs(inte.simps(confluence_Ex_disp, Ex_disp_y[:,0], gap))**2/(2*(2*sp.pi*freq[0,1])**3*stored_energy[0][1]*disp_y**2)
       
        
##        print r_over_q_Ey_Bz, r_over_q_Ex, r_over_q_Ex/r_over_q_Ey_Bz

####### Calculation of R/Q for transverse mode displaced in z #########

        file_input = open(work_directory+"e_field_displaced_z.fld", 'r+')
        data = file_input.readlines()
        Ez_disp_z = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ez_disp_z.append([temp[0],complex(temp[-2],temp[-1])])

        temp = []
        for i in range(0,len(Ey_disp_y)):
            temp.append([Ez_disp_z[i][0],complex((Ez_disp_z[i][1]).real,(Ez_disp_z[i][1]).imag)])
        Ez_disp_z = sp.array(temp)

        Ex_disp_z = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ex_disp_z.append([temp[0],complex(temp[-6],temp[-5])])

        temp = []
        for i in range(0,len(Ex_disp_z)):
            temp.append([Ex_disp_z[i][0],complex((Ex_disp_z[i][1]).real,(Ex_disp_z[i][1]).imag)])
        Ex_disp_z = sp.array(temp)

        file_input = open(work_directory+"h_field_displaced_z.fld", 'r+')
        data = file_input.readlines()
        By_disp_z = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            By_disp_z.append([temp[0],mu0*complex(temp[-4],temp[-3])])

        temp = []
        for i in range(0,len(By_disp_z)):
            temp.append([By_disp_z[i][0],complex((By_disp_z[i][1]).real,(By_disp_z[i][1]).imag)])
        By_disp_z = sp.array(temp)

        confluence_Ez = []
        for i in range(0,len(Ez_disp_z)):
            confluence_Ez.append((Ez_disp_z[i,1]+C*By_disp_z[i,1])*complex(sp.cos(freq[0,1]*2*sp.pi*Ez[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ez[i,0]/C)))


        kick_factor_Ez_By = abs(inte.simps(confluence_Ez, Ez_disp_z[:,0], gap))**2*(2*sp.pi*freq[0,1])/(4*C*stored_energy[0][1])
        r_over_q_Ez_By = 4*kick_factor_Ez_By/(2*sp.pi*freq[0,1])

        confluence_Ex_disp_z = []
        for i in range(0,len(Ex_disp_z)):
            confluence_Ex_disp_z.append((Ex_disp_z[i,1]-Ez[i,1])*complex(sp.cos(freq[0,1]*2*sp.pi*Ez[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ez[i,0]/C))/disp_y)

        kick_factor_Ex_disp_z = C*abs(inte.simps(confluence_Ex_disp_z, Ex_disp_z[:,0], gap))**2/(4*2*sp.pi*freq[0,1]*stored_energy[0][1])
        r_over_q_Ex_disp_z = 4*kick_factor_Ex_disp_z/(2*sp.pi*freq[0,1])

       
        
##        print r_over_q_Ez_By, r_over_q_Ex_disp_z, r_over_q_Ex_disp_z/r_over_q_Ez_By



    ##    print "Frequency of mode is %f" % freq[0,1]
    ##    print "Q-factor of mode is %f" % Q[0,1]
    ##    print "Transverse kicker factor k_y from dEz/dy1 (V/nC/mm) = %f" % (k_y_from_dEzdy1/10**12)
    ##    print "Transverse R/Q from dEz/dy1 (Ohms/mm) = %f" % (trans_r_over_q_from_dEzdy1/10**3)
    ##    print "Transverse Rs1 = %f (Ohms/mm)" % (trans_r_over_q_from_dEzdy1*Q[0,1]/10**3)
    ##    ##print "Transverse kicker factor k_y from  dEz/dy2 (V/nC/mm) = %f" % (k_y_from_dEzdy2)
    ##    ##print "Transverse R/Q from dEz/dy2 (Ohms/mm) = %f" % (trans_r_over_q_from_dEzdy2)
    ##    ##print "Transverse  Rs2 = %f (Ohms/mm)" % (trans_r_over_q_from_dEzdy2*Q[0,1])
    ##    print "Transverse kicker factor k_y from Ey+cBx (V/nC/mm) = %f" % (k_y_from_Ey_Bx/10**12)
    ##    print "Transverse R/Q from Ey+cBx (Ohms/mm) = %f" % (trans_r_over_q_from_Ey_Bx/10**3)
    ##    print "Transverse Rs = %f (Ohms/mm)" % (trans_r_over_q_from_Ey_Bx*Q[0,1]/10**3)
    ##    print "Longitudinal R/Q = %f" % long_r_over_q
    ##    print "Longitudinal Rs = %f (Ohms)" % (long_r_over_q*Q[0,1])
    ##    print "Longitudinal R/Q alt= %f" % long_r_over_q_alt
    ##    print "Longitudinal Rs alt= %f (Ohms)" % (long_r_over_q_alt*Q[0,1])
    ##    print "Power loss is: %f (Watts)" % ((charge/t_bunch)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2))

        eigenmode_store.append([freq[0,1], Q[0,1], long_r_over_q_alt*Q[0,1], (charge/t_bunch)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2)])
        eigenmode_transverse_disp_y_Ex_store.append([freq[0,1], Q[0,1], r_over_q_Ex*Q[0,1]])
        eigenmode_transverse_disp_y_Ey_store.append([freq[0,1], Q[0,1], r_over_q_Ey_Bz*Q[0,1]])
        eigenmode_transverse_disp_z_Ex_store.append([freq[0,1], Q[0,1], r_over_q_Ex_disp_z*Q[0,1]])
        eigenmode_transverse_disp_z_Ez_store.append([freq[0,1], Q[0,1], r_over_q_Ez_By*Q[0,1]])


freq_list = []
for i in range(0,20000,1):
        freq_list.append(float(i/10.0*10.0**6))

freq_list = list(set(freq_list))
freq_list.sort()

for i in range(0, len(eigenmode_transverse_disp_y_Ex_store)):
    print eigenmode_transverse_disp_y_Ex_store[i]
    print eigenmode_transverse_disp_y_Ey_store[i]

print "sep"
    
for i in range(0, len(eigenmode_transverse_disp_z_Ex_store)):
    print eigenmode_transverse_disp_z_Ex_store[i]
    print eigenmode_transverse_disp_z_Ez_store[i]
    
impedance_profile_trans_disp_y_Ex = []
impedance_profile_trans_disp_y_Ey = []
impedance_profile_trans_disp_z_Ex = []
impedance_profile_trans_disp_z_Ez = []

for i in freq_list:
    total_disp_y_Ex = 0.0
    total_disp_y_Ey = 0.0
    total_disp_z_Ex = 0.0
    total_disp_z_Ez = 0.0
    for entry in eigenmode_transverse_disp_y_Ex_store:
        total_disp_y_Ex += Z_bb(i, entry)
    for entry in eigenmode_transverse_disp_y_Ey_store:
        total_disp_y_Ey += Z_bb(i, entry)
    for entry in eigenmode_transverse_disp_z_Ex_store:
        total_disp_z_Ex += Z_bb(i, entry)
    for entry in eigenmode_transverse_disp_z_Ez_store:
        total_disp_z_Ez += Z_bb(i, entry)
    impedance_profile_trans_disp_y_Ex.append([total_disp_y_Ex.real, total_disp_y_Ex.imag])
    impedance_profile_trans_disp_y_Ey.append([total_disp_y_Ey.real, total_disp_y_Ey.imag])
    impedance_profile_trans_disp_z_Ex.append([total_disp_z_Ex.real, total_disp_z_Ex.imag])
    impedance_profile_trans_disp_z_Ez.append([total_disp_z_Ez.real, total_disp_z_Ez.imag])


impedance_profile_trans_disp_y_Ex = sp.array(impedance_profile_trans_disp_y_Ex)
impedance_profile_trans_disp_y_Ey = sp.array(impedance_profile_trans_disp_y_Ey)
impedance_profile_trans_disp_z_Ex = sp.array(impedance_profile_trans_disp_z_Ex)
impedance_profile_trans_disp_z_Ez = sp.array(impedance_profile_trans_disp_z_Ez)

######## Horizontal Transverse Impedance (z-axis in frequency domain)########

freq_list = sp.array(freq_list)


list_total_time = []
for i in range(0, len(trans_dat_in_horz_quad)):
    temp_real = abs(trans_dat_in_horz_dip[i,1])+abs(trans_dat_in_horz_const[i,1])+abs(trans_dat_in_horz_quad[i,1])
    temp_imag = trans_dat_in_horz_dip[i,2]+trans_dat_in_horz_const[i,2]+trans_dat_in_horz_quad[i,2]
    list_total_time.append([trans_dat_in_horz_quad[i,0], temp_real, temp_imag])

list_total_time = sp.array(list_total_time)
##pl.loglog()
##pl.semilogx()
pl.semilogy()
pl.plot(freq_list/10**9, impedance_profile_trans_disp_z_Ex[:,0], 'b-', label = "Real Impedance Ez")
##pl.plot(freq_list/10**9, impedance_profile_trans_disp_z_Ex[:,1], 'k--', label = "Imaginary Impedance Ez")
pl.plot(freq_list/10**9, impedance_profile_trans_disp_z_Ez[:,0], 'k-', label = "Real Impedance Ez,By")
##pl.plot(freq_list/10**9, impedance_profile_trans_disp_z_Ez[:,1], 'b--', label = "Imaginary Impedance Ez,By")
##pl.plot(trans_dat_in_horz_dip[:,0], abs(trans_dat_in_horz_dip)[:,1], 'r-', label="Parked In - Horizontal Dipolar $\Re{}e(Z)$")
##pl.plot(trans_dat_in_horz_dip[:,0], abs(trans_dat_in_horz_dip[:,2]), 'r-', label="Parked In - Horizontal Dipolar $\Im{}m(Z)$")
##pl.plot(trans_dat_in_horz_quad[:,0], abs(trans_dat_in_horz_quad[:,1]), 'g-', label="Parked In - Horizontal Quadrupolar $\Re{}e(Z)$")
##pl.plot(trans_dat_in_horz_quad[:,0], abs(trans_dat_in_horz_quad[:,2]), 'r-', label="Parked In - Horizontal Quadrupolar $\Im{}m(Z)$")
##pl.plot(trans_dat_in_horz_const[:,0], abs(trans_dat_in_horz_const[:,1]), 'k-', label="Parked In - Horizontal Constant $\Re{}e(Z)$")
##pl.plot(trans_dat_in_horz_const[:,0], abs(trans_dat_in_horz_const[:,2]), 'r-', label="Parked In - Horizontal Constant $\Im{}m(Z)$")
##pl.plot(list_total_time[:,0], list_total_time[:,1], "r-", label="Total $\Re{}e(Z_{\perp})$ Time domain")
##pl.plot(list_total_time[:,0], list_total_time[:,2], "r--", label="Total $\Im{}m(Z_{\perp})$ Time domain")

pl.xlabel("Frequency (GHz)")
pl.ylabel("Impedance ($\Omega/m$)")
pl.legend(loc = "upper left")
pl.grid(linestyle="--", which = "major")
pl.show()
pl.clf()

####### Vertical plane (y in frequency domain) ########


list_total_time_vert = []
for i in range(0, len(trans_dat_in_vert_quad)):
    temp_real = abs(trans_dat_in_vert_quad[i,1])+abs(trans_dat_in_vert_dip[i,1])+abs(trans_dat_in_vert_const[i,1])
##    temp_real = abs(trans_dat_in_vert_dip[i,1])+abs(trans_dat_in_vert_const[i,1])
    temp_imag = trans_dat_in_vert_quad[i,2]+trans_dat_in_vert_dip[i,2]+trans_dat_in_vert_const[i,2]
    list_total_time_vert.append([trans_dat_in_horz_quad[i,0], temp_real, temp_imag])

list_total_time_vert = sp.array(list_total_time_vert)

##print impedance_profile_trans_disp_y_Ex[:,0]
##
##pl.loglog()
##pl.semilogx()
pl.semilogy()
pl.plot(freq_list/10**9, impedance_profile_trans_disp_y_Ex[:,0], 'k-', label = "Real Impedance Ez")
##pl.plot(freq_list/10**9, impedance_profile_trans_disp_y_Ex[:,1], 'r-', label = "Imaginary Impedance Ez")
pl.plot(freq_list/10**9, impedance_profile_trans_disp_y_Ey[:,0], 'b-', label = "Real Impedance Ey,Bx")
##pl.plot(freq_list/10**9, impedance_profile_trans_disp_y_Ey[:,1], 'r-', label = "Imaginary Impedance Ey,Bx")
pl.xlabel("Frequency (GHz)")
pl.ylabel("Impedance ($\Omega/m$)")
pl.grid(linestyle="--", which = "major")
##pl.plot(trans_dat_in_vert_dip[:,0], abs(trans_dat_in_vert_dip[:,1]), 'r-', label="Parked In - Vertical Dipolar $\Re{}e(Z$")
##pl.plot(trans_dat_in_vert_dip[:,0], abs(trans_dat_in_vert_dip[:,2]), 'r-', label="Parked In - Vertical Dipolar $\Im{}m(Z)$")
##pl.plot(trans_dat_in_vert_const[:,0], abs(trans_dat_in_vert_const[:,1]), 'k-', label="Parked In - Vertical Constant $\Re{}e(Z$")
##pl.plot(trans_dat_in_vert_const[:,0], abs(trans_dat_in_vert_const[:,2]), 'r-', label="Parked In - Vertical Constant $\Im{}m(Z)$")
##pl.plot(trans_dat_in_vert_quad[:,0], abs(trans_dat_in_vert_quad[:,1]), 'k-', label="Parked In - Vertical Quadrupolar $\Re{}e(Z)$")
##pl.plot(trans_dat_in_vert_quad[:,0], abs(trans_dat_in_vert_quad[:,2]), 'r-', label="Parked In - Vertical Quadrupolar $\Im{}m(Z)$")
pl.plot(list_total_time[:,0], list_total_time[:,1], "k--", label="Total $\Re{}e(Z_{\perp})$ Time domain")
##pl.plot(list_total_time[:,0], list_total_time[:,2], "r--", label="Total $\Im{}m(Z_{\perp})$ Time domain")

pl.axis([0,1.2, 10**-3, 10**6])
pl.legend(loc = "upper left")
##pl.show()
pl.clf()

