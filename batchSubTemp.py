# Script to run Programmes on lxbatch nodes. Taken from sub-hdtl by K. Li

import re,shutil,stat,sys,os
from numpy import *

class Input:
    def __init__(self):
        self.vname = []
        self.varray = []

#  Beginning of user interface

vname='eDens'; varray = arange(10,20,1); #in units 1e10
istart=1; #Job array index start


#end user interface
def readUserinput():
    print '''\n******************************************************
This version of batchSub.py is designed to launch a variable scan of a parameter for arcPIC 2d assuming
a fixed solution model. Output will be the standard format for arcPIC. Remember to define the working direct
and target directories properly.

Hop to it then!

'''
    print '\n******* Reading input file:\t '+filename
    print '\n******* Scanning parameter:\t '+vname
    imin = min(varray[istart:])
    imax = max(varray[istart:])
    istep = varray[1] - varray[0]

    print '********* from '+str(imin)+' to '+str(imax) + ' in steps of ' +str(istep)+'\n'


#create working and target directory

def dirCreationAndLaunching():
    '''This function cycles through a variable array and launchs a number of different batch jobs dependent
    on the variables chosen. Done due to input file name being fixed for arcPIC'''
    
    global CWD, TRGDIR, WRKDIR, RUNTIMEDIR
    CWD = os.getcwd()
    HOME = os.environ.get('HOME')
    runTime = 'ArcPIC_runTime'
    inputFile = 'input.txt'
    RUNTIMEDIR = HOME+'/arcPIC/arcpic/pic2d/trunk/runTime/src/runTime/'
    WRKDIR = HOME+'/arcPIC/dataBack/'+str(vname)
    TRGDIR = HOME+'/arcPIC/dataBack/'

    if os.path.exists(WRKDIR):
        ans = raw_input('\nWARNING: path already exists! Overwrite? [y or n]\n')
        if ans == ('y' or 'yes'):
            print '\n@@@@@@@@@@ You\'re mine now!'
            shutil.rmtree(WRKDIR)
            os.mkdir(WRKDIR)
            os.chdir(WRKDIR)
        else:
            print 'As you wish. Sad panda in snow'
            sys.exit()

    else:
        os.mkdir(WRKDIR)

    print '\nMade directory:\t '+WRKDIR+'/'
    linesInput = readInputFile(RUNTIMEDIR+inputFile)
    for entry in varray:
        tempWRKDIR = WRKDIR+str(vname)+str(entry)
        os.mkdir(tempWRKDIR)
        os.chdir(tempWRKDIR)
        os.system('cp '+RUNTIMEDIR+runTime+' '+tempWRKDIR+'ArcPIC_runTime')
        
        lsfContents = lsfFileWrite(tempWRKDIR)
        inputContents = inputFileContents(lines, vname, entry)

        tempInput.open('input.txt','w')
        tempInput.write(inputContents)
        tempInput.close()
    
        tempLSF.open('arcPICSub.lsf','w')
        tempLSF.write(lsfContents)
        tempLSF.close()
    
        print '\n******* launch of simulation array succeeded'

        os.system('bsub < arcPICSub.lsf')


# Read base input file

def readInputFile(fileName):
    '''Reads input file sample for modification'''
    fileHead = open(fileName,'r')
    lines = fh.readlines()
    fh.close()

    return lines

#scan variables

def lsfFileWrite(varName, varValue, dirSet):
    '''Generates output for an lsf file'''
    lsf = '''~!/bin/bash

#set environmental variable; (CAUTION: bash accepts no spaces mortal!)
EXECDIR='''+dirSet+'''
WRKDIR='''+dirSet+'''
TRGDIR='''+dirSet+'''

#set sub options
#BSUB -L /bin/bash
##BSUB -o hdtl_out
##BSUB -r hdtl_err
##BSUB -M 640000
#BSUB -u hugo.day@hep.manchester.ac.uk
#BSUB -q 1nw

#copy all files for jo execution
cp ${EXECDIR}/ArcPIC_runTime ./
cp ${EXECDIR}/input.txt ./

#run HEADTAIL
./ArcPIC_runTime > input.txt

#copy back output files
scp ./* ${TRGDIR}/
'''

    return lsf

def inputFileContents(lines, vName, varValue):
    '''Creates contents for an input file for arcPIC'''
    ix = []
    # == Beam intensity
    if vname == "eDens":
        for i in range(len(lines)):
            if lines[i].split()[0] == "density":
                ix.append(i)
                print "0"
                try:
                    ix[0]
                except IndexError:
                    print "\t Warning: Entry\"Density \" not found comrade"
            else:
                print "1"
                n = ix[0]
                
            # change variable 2 & write output
                lines[n] = ('density       : 1e'+str(entry)+'  Particle density\t[particles/cm^3]\n')

    else:
        print "Bad variable name. Covered in bees!"




def main():
    readUserinput()
    dirCreationAndLaunching()

main()
