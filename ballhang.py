import scipy as sp
import pylab as pl
import time, csv, sys, os

def gauss_prof(dist, sigma, order):
    return sp.e**(-order*(dist/(2*sigma))**2)

def para_prof(dist, bunch_length):
    return 1-(2*dist/bunch_length)**2

def cos_prof(dist, bunch_length):
    return sp.cos(sp.pi/bunch_length*dist)**2

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp


########################## Imported Profile ##################################

start = time.time()

C = 299792458.0
circ=26659.0
n_bunches = 2808
possible_bunches = 3578
p_bunch = 1.15*10**11
Q_part = 1.6*10**-19
f_rev = 2*10**7
length = 2.88


top_directory = "E:/PhD/1st_Year_09-10/Data/mki-heating/heating-estimates/"


directory = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/" #Directory of data
without_metallisation = "without-metalisation/longitudinal-impedance.csv"
screen_conductors_15 = "with-15-screen-conductors/longitudinal-impedance.csv"
screen_conductors_24 = "with-24-screen-conductors/longitudinal-impedance.csv"
screen_conductors_15_long_9_short = "15-long-9-short-screen/longitudinal-impedance.csv"
screen_conductors_15_4hv = "15-and-4-hv-plate/longitudinal-impedance.csv"
screen_conductors_15_5hv = "15-and-5-hv-plate/longitudinal-impedance.csv"
screen_conductors_none = "no-conductors/longitudinal-impedance.csv"
no_damping_ferrites = "no-damping-ferrites/longitudinal-impedance.csv"
no_screen = "no-screen/longitudinal-impedance.csv"
alt_screen_1 = "alt-screen-design/longitudinal-impedance.csv"
alt_screen_embedded_cerr = "embedded-in-tube/longitudinal-impedance.csv"
alt_screen_embedded_cerr_sec = "embedded-in-tube/longitudinal-impedance-second.csv"
alt_screen_no_cerr = "embedded-tube-vacuum/longitudinal-impedance.csv"
alt_screen_thick_cerr = "thick_ceramic_5.5mm/longitudinal-impedance.csv"
measurements_24 = "E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/24-strips-measurements.csv"
measurements_15 = "E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/15-strips-measurements.csv"


measure_stash_path = "E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/mki_measurements_mbarnes/"




data_without_metalisation = sp.array(extract_dat(directory+without_metallisation))
data_screen_conductors_15 = sp.array(extract_dat(directory+screen_conductors_15))
data_screen_conductors_24 = sp.array(extract_dat(directory+screen_conductors_24))
data_screen_conductors_15_long_9_short = sp.array(extract_dat(directory+screen_conductors_15_long_9_short))
data_screen_conductors_15_4hv = sp.array(extract_dat(directory+screen_conductors_15_4hv))
data_screen_conductors_15_5hv = sp.array(extract_dat(directory+screen_conductors_15_5hv))
data_screen_conductors_none = sp.array(extract_dat(directory+screen_conductors_none))
data_no_damping_ferrites = sp.array(extract_dat(directory+no_damping_ferrites))
data_no_screen = sp.array(extract_dat(directory+no_screen))
data_alt_screen_1 = sp.array(extract_dat(directory+alt_screen_1))
data_alt_screen_embedded_cerr = sp.array(extract_dat(directory+alt_screen_embedded_cerr))
data_alt_screen_embedded_cerr_sec = sp.array(extract_dat(directory+alt_screen_embedded_cerr_sec))
data_alt_screen_no_cerr = sp.array(extract_dat(directory+alt_screen_no_cerr))
data_alt_screen_thick_cerr = sp.array(extract_dat(directory+alt_screen_thick_cerr))


mark_length = 1.1*10**-9
bunch_length = 1.5*10**-9
time_domain_cos = []
time_domain_para = []
time_domain_gauss = []
data_points = 10000.0
sample_rate = 0.1/(mark_length/data_points)

## For 50ns spacing 22.726

t=pl.r_[-22.726*mark_length:22.726*mark_length:1/sample_rate]
## For 25ns spacing
##t=pl.r_[-11.357*mark_length:11.357*mark_length:1/sample_rate]
    ##print len(t)
omega = sample_rate

for i in t:
    if abs(i)<=bunch_length/2:
        time_domain_cos.append(cos_prof(i, bunch_length))
        time_domain_para.append(para_prof(i, bunch_length))
    else:
        time_domain_cos.append(0)
        time_domain_para.append(0)
    time_domain_gauss.append(gauss_prof(i, bunch_length, 30))
    
time_domain_cos = sp.array(time_domain_cos)
time_domain_para = sp.array(time_domain_para)
time_domain_gauss = sp.array(time_domain_gauss)

pl.plot(t*10**9, time_domain_cos, 'k-', label="cos$^{2}$")
pl.plot(t*10**9, time_domain_para, 'b-', label="parabolic")
pl.plot(t*10**9, time_domain_gauss, 'r-', label="gaussian")
pl.xlabel("Time (ns)", fontsize="16")
pl.ylabel("Amplitude", fontsize="16")
pl.legend(loc="upper right")
pl.axis([-1.5*bunch_length*10**9, 1.5*bunch_length*10**9,0,1])
##pl.show()
pl.clf()

N=len(t)
freq_domain_cos=sp.fft(time_domain_cos)
freq_domain_para=sp.fft(time_domain_para)
freq_domain_gauss=sp.fft(time_domain_gauss)
f = sample_rate*sp.r_[0:(N/2)]/N
n=len(f)


freq_domain_cos = freq_domain_cos[0:n]/freq_domain_cos[0]
freq_domain_para = freq_domain_para[0:n]/freq_domain_para[0]
freq_domain_gauss = freq_domain_gauss[0:n]/freq_domain_gauss[0]

freq_domain_cos_curr_db = 20*sp.log(freq_domain_cos)
freq_domain_para_curr_db = 20*sp.log(freq_domain_para)
freq_domain_gauss_curr_db = 20*sp.log(freq_domain_gauss)

freq_domain_cos_pow_db = 10*sp.log(freq_domain_cos)
freq_domain_para_pow_db = 10*sp.log(freq_domain_para)
freq_domain_gauss_pow_db = 10*sp.log(freq_domain_gauss)

##ax1 = pl.subplot(111)
##pl.plot(f[:n]/10**9, freq_domain_cos_curr_db, 'k-', label="cos$^{2}$")
##pl.plot(f[:n]/10**9, freq_domain_para_curr_db, 'r-', label="parabolic")
##pl.plot(f[:n]/10**9, freq_domain_gauss_curr_db, 'b-', label="gaussian")
##pl.xlabel("Frequency (GHz)", fontsize = "16")
##pl.ylabel("Current Spectrum (dB)", fontsize = "16")
##pl.legend(loc="upper right")
##pl.axis([0,2.5, -100, 0])

ax1 = pl.subplot(111)
pl.plot(f[:n]/10**9, freq_domain_cos_pow_db, 'k-', label="cos$^{2}$")
pl.plot(f[:n]/10**9, freq_domain_para_pow_db, 'r-', label="parabolic")
pl.plot(f[:n]/10**9, freq_domain_gauss_pow_db, 'b-', label="gaussian")
pl.xlabel("Frequency (GHz)", fontsize = "16")
pl.ylabel("Power Spectrum (dB)", fontsize = "16")
pl.legend(loc="upper right")
pl.axis([0,3.0, -60, 0])

ax2 = pl.twinx()
pl.plot(data_screen_conductors_15[:,0], data_screen_conductors_15[:, 1], 'm-', label= "15 Conductive Strips")
pl.plot(data_screen_conductors_24[:,0], abs(data_screen_conductors_24[:, 1]), 'k--', label= "24 Conductive Strips")
pl.plot(data_screen_conductors_15_long_9_short[:,0], data_screen_conductors_15_long_9_short[:, 1], 'r--', label= "15 long, 9 short")
pl.ylabel("Real Longitudinal Impedance ($\Omega$)", fontsize = "16")
pl.legend(loc="upper center")
pl.axis([0,3.0, 0, 80])
pl.show()
pl.clf()
