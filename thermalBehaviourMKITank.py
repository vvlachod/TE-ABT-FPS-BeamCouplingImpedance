import csv, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op

C = 299792458.0
stefBoltzConst = 5.670373*10**-8


def tempInnerCond(tempOuterCond, innerEmis, outerEmis, radiusInnerCond, radiusOuterCond,powIn, thermConv):
    return (powIn*((1/innerEmis)+((1-outerEmis)/(outerEmis)*(radiusInnerCond/radiusOuterCond)))/(stefBoltzConst)+tempOuterCond**4)**0.25


tOuter = 297.0
emisFer = 0.9
radiusInner = 0.028
radiusOuter = 0.25
thermCond = 1.9
powerBeam = 100.0

tempsFerr = []
emisTank = sp.linspace(0.01,1.0,100)
for emis in emisTank:
    tempsFerr.append(tempInnerCond(tOuter, emisFer, emis, radiusInner, radiusOuter, powerBeam,thermCond)-273)

tempsFerr=sp.array(tempsFerr)
pl.plot(emisTank,tempsFerr)
pl.xlabel("Emissivity", fontsize=16.0)
pl.ylabel("T$_{ferrite}$ (C)", fontsize=16.0)
pl.axis([0,0.8,40,100])
pl.show()
pl.clf()
