import csv, os, sys, math
import scipy as sp
import pylab as pl
from scipy import optimize as op

##fitfunc = lambda p, x: p[2] + p[1]*x + p[0]*x**2
fitfunc = lambda p, x: p[2] + p[1]*x + p[0]
errfunc = lambda p, x, y, err: (y-fitfunc(p, x))/err

lenMKI = 2.88
lenWire = 3.2
C = 299792458


def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

imp = "long"
if imp=="long":
    fileNam = "/longitudinal-impedance.csv"
elif imp=="vertdip":
    fileNam = "/vertical-dipolar-impedance.csv"
else:
    print "You're an idiot"

topDir = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/build_up_exercise/"
ferrAsVac = topDir+"simulatingFerrAsVac"+fileNam
ferr = topDir+"ferrite_only"+fileNam
ferrCerr = topDir+"ferrite_and_ceramic"+fileNam
ferrCerrCond = topDir+"ferrite_ceramic_screen_conductors"+fileNam
ferrCerrCondDamp = topDir+"ferrite_ceramic_screen_conductors_damping"+fileNam
ferrCerrCondDampAlt = topDir+"ferrite_ceramic_screen_conductors_damping_alternating"+fileNam

ferrAsVacDat = sp.array(extract_dat(ferrAsVac))
ferrDat = sp.array(extract_dat(ferr))
ferrCerrDat = sp.array(extract_dat(ferrCerr))
ferrCerrCondDat = sp.array(extract_dat(ferrCerrCond))
ferrCerrCondDampDat = sp.array(extract_dat(ferrCerrCondDamp))
ferrCerrCondDampAltDat = sp.array(extract_dat(ferrCerrCondDampAlt))

pl.semilogy()
pl.plot(ferrAsVacDat[:,0], abs(ferrAsVacDat[:,1]), 'm-', label="Ferrite As Vacuum")
pl.plot(ferrDat[:,0], abs(ferrDat[:,1]), 'k-', label="Ferrite Only")
pl.plot(ferrCerrDat[:,0], abs(ferrCerrDat[:,1]), 'r-', label="Ferrite and Ceramic Tube")
pl.plot(ferrCerrCondDat[:,0], abs(ferrCerrCondDat[:,1]), 'b-', label="Ferrite, Ceramic and Screen Conductors")
pl.plot(ferrCerrCondDampDat[:,0], abs(ferrCerrCondDampDat[:,1]), 'g-', label="Ferrite, Ceramic, Screen Conductors and Damping Ferrites")
##pl.plot(ferrCerrCondDampAltDat[:,0], abs(ferrCerrCondDampAltDat[:,1]), 'm-', label="Ferrite, Ceramic, Screen Conductors (Alt) and Damping Ferrites")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$\Re{}e(Z_{\parallel})$ ($\Omega$)", fontsize=16.0)
pl.grid(True)
pl.legend(loc="lower left")
pl.show()
pl.clf()

##pl.semilogy()
pl.plot(ferrDat[:,0], (ferrDat[:,3]), 'k-', label="Ferrite Only")
pl.plot(ferrCerrDat[:,0], (ferrCerrDat[:,3]), 'r-', label="Ferrite and Ceramic Tube")
pl.plot(ferrCerrCondDat[:,0], (ferrCerrCondDat[:,3]), 'b-', label="Ferrite, Ceramic and Screen Conductors")
pl.plot(ferrCerrCondDampDat[:,0], (ferrCerrCondDampDat[:,3]), 'g-', label="Ferrite, Ceramic, Screen Conductors and Damping Ferrites")
##pl.plot(ferrCerrCondDampAltDat[:,0], (ferrCerrCondDampAltDat[:,3]), 'm-', label="Ferrite, Ceramic, Screen Conductors (Alt) and Damping Ferrites")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$\Im{}m(Z_{\parallel})$ ($\Omega$)", fontsize=16.0)
pl.grid(True)
pl.legend(loc="upper left")
##pl.show()
pl.clf()
