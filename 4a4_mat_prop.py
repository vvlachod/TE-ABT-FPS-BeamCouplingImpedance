import scipy as sp
import csv

Z0 = 377.0
e0 = 8.85*10**-12  # Permitivitty of free space
rho = 10**6        # Resistivity of ferrite
muprime = 460.0    # mu prime
tau = 1.0/(20.0*10**6) # relaxation time
er=12.0             # Relative permitivitty

directory = "E:/PhD/1st_Year_09-10/Data/Material Properties/"
# Data directory


def mur(freq):
    return (1 + (muprime/(1+1j*tau*freq)))

def epsr(freq):
    return e0*(er - 1j/(2*sp.pi*freq*e0*rho))

mu_real =[]
mu_imag = []
eps_real=[]
eps_imag=[]

mu_real_file = csv.writer(open(directory+"mu_real.tab", "w+"), delimiter = '\t',
                     lineterminator='\n')
mu_imag_file = csv.writer(open(directory+"mu_imag.tab", "w+"), delimiter = '\t',
                     lineterminator='\n')
mu_loss_file = csv.writer(open(directory+"mu_loss.tab", "w+"), delimiter = '\t',
                     lineterminator='\n')
eps_real_file = csv.writer(open(directory+"eps_real.tab", "w+"), delimiter = '\t',
                     lineterminator='\n')
eps_imag_file = csv.writer(open(directory+"eps_imag.tab", "w+"), delimiter = '\t',
                     lineterminator='\n')
eps_loss_file = csv.writer(open(directory+"eps_loss.tab", "w+"), delimiter = '\t',
                     lineterminator='\n')



freq_list = []
for i in range(0,10,1):
    for j in range(1,100,1):
        freq_list.append(float((j/10.0)*10.0**i))

freq_list = list(set(freq_list))
freq_list.sort()

for i in freq_list:
    mu_real_file.writerow([i, mur(i).real])
    mu_imag_file.writerow([i, mur(i).imag])
    mu_loss_file.writerow([i, -mur(i).imag/mur(i).real])
    eps_real_file.writerow([i, epsr(i).real/e0])
    eps_imag_file.writerow([i, epsr(i).imag/e0])
    eps_loss_file.writerow([i, epsr(i).imag/epsr(i).real])


