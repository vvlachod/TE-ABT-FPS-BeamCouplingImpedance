import csv, time, os, sys
import scipy as sp
import pylab as pl
import numpy as np
import scipy.optimize as op
import scipy.interpolate as interp
from scipy import signal

####### Define constants for calculations ##########

C = 299792458.0
c = 299792458.0
PasToTorr = 101325/760
###### Define the Paschen Voltage. Const A and Const B are both functions
###### of the gas in the breakdown gas. gamma is a property of the metal
###### Known as the secondary ionisation constant

def paschVolt(pres, gap, constA, constB, gamma):
    return constB*pres*gap/sp.log((constA*pres*gap)/sp.log(1/gamma +1))

###### Define the Paschen Voltage. Const A and Const B are both functions
###### of the gas in the breakdown gas. gamma is a property of the metal
###### Known as the secondary ionisation constant. Calculates from input of
###### BD voltage and pd at minima

def paschVoltMins(minVolt, minPD, pres, gap, gamma):
    constA = sp.e**1*sp.log(1/gamma)/minPD
    constB = minVolt*constA/(sp.e**1*sp.log(1/gamma))
    return constB*pres*gap/(sp.log((constA*pres*gap)/sp.log(1/gamma +1)))

###### List Parameters for gases
constAAir, constBAir = 20, 487
constACO2, constBCO2 = 27, 621
constAH2, constBH2 = 7, 173
constAN2, constBN2 = 13, 413
constAHe, constBHe = 4, 45
constAAr, constBAr = 16, 240

gapFixed = 0.003
pdFixed = sp.logspace(-15,1,10**6)

paschCurveAirGam13 = paschVolt(pdFixed/gapFixed, gapFixed, constAAir, constBAir, 1.3)
paschCurveAirGam15 = paschVolt(pdFixed/gapFixed, gapFixed, constAAir, constBAir, 1.5)
##paschCurveAirGam6 = paschVolt(pdFixed/gapFixed, gapFixed, constAAir, constBAir, 1.9)
paschCurveH2Gam13 = paschVolt(pdFixed/gapFixed, gapFixed, constAH2, constBH2, 1.3)
paschCurveN2Gam13 = paschVolt(pdFixed/gapFixed, gapFixed, constAN2, constBN2, 1.3)
paschCurveHeGam13 = paschVolt(pdFixed/gapFixed, gapFixed, constAHe, constBHe, 1.3)
paschCurveArGam13 = paschVolt(pdFixed/gapFixed, gapFixed, constAAr, constBAr, 1.3)

##pl.semilogx()
##pl.semilogy()
pl.plot(pdFixed/PasToTorr, paschCurveAirGam13, label="Air, $\gamma$=1.3")
pl.plot(pdFixed/PasToTorr, paschCurveAirGam15, label="Air, $\gamma$=1.5")
##pl.plot(pdFixed/PasToTorr, paschCurveAirGam6, label="Air, $\gamma$=6")
pl.plot(pdFixed/PasToTorr, paschCurveH2Gam13, label="H$_{2}$, $\gamma$=1.3")
pl.plot(pdFixed/PasToTorr, paschCurveN2Gam13, label="N$_{2}$, $\gamma$=1.3")
pl.plot(pdFixed/PasToTorr, paschCurveHeGam13, label="He, $\gamma$=1.3")
pl.plot(pdFixed/PasToTorr, paschCurveArGam13, label="Ar, $\gamma$=1.3")
pl.xlim(0.0001,0.0150)
pl.ylim(0,2000)
pl.legend(loc="upper right")
pl.xlabel("Pressure.distance (m Torr)", fontsize=16.0)
pl.ylabel("Breakdown Voltage (V)", fontsize=16.0)
pl.show()
pl.clf()
