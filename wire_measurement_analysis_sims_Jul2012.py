import csv, os, sys, math
import scipy as sp
import pylab as pl
from scipy import optimize as op

fitfunc = lambda p, x: p[0] + p[1]*x + p[2]*x**2
errfunc = lambda p, x, y, err: (y-fitfunc(p, x))/err

distance_acc = 0.000001
imp_err = distance_acc/0.003

C = 3.0*10**8
Z0=377.0
lenTot = 0.15
rSep = 0.002
rWire = 0.0005
apDev = 0.005
##print cos_phi_bt
##print cos_phi_at
##print sp.arccos(cos_phi_bt)
##print sp.arccos(cos_phi_at)
length_total = 0.05
length_imp = 0.05
wire_sep = 0.003

def importDat(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
    datStore = []
    for entry in tempDat[1:]:
        temp = map(float, entry.split(','))
        datStore.append(temp)

    return datStore


def analDipRe(data, freqList, lenDUT, lenTot, r_wire, r_sep, apPipe):
    reStore = []
    temp = []
    Z0 = 120*math.acosh(r_sep/(2*r_wire))
    for i in range(0,len(data)):
        temp.append(-2*Z0*C/(2*sp.pi*freqList[i]*r_sep**2)*sp.log(10**(data[i,1]/20))/lenTot) 
    return temp

def analDipIm(data, freqList, lenDUT, lenTot, r_wire, r_sep, apPipe):
    reStore = []
    temp = []
    Z0 = 120*math.acosh(r_sep/(2*r_wire))
    for i in range(0,len(data)):
        temp.append(-2*Z0*C/(2*sp.pi*freqList[i]*r_sep**2)*(sp.radians(data[i,1])+(2*sp.pi*freq_list[i]*lenTot/C))/lenTot) 
    return temp

def analSingRe(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    reStore = []
    temp = []
    Z0 = 60*sp.log(1.27*apPipe/r_wire)
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(-2*Z0*sp.log(10**(-data[i,j]/20))) 
        temp.append(tempLin)
    return temp

def analSingIm(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    reStore = []
    temp = []
    Z0 = 60*sp.log(1.27*apPipe/r_wire)
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(-2*Z0*(sp.radians(data[i,j])+(2*sp.pi*freq_list[i]*lenTot/C))/lenTot) 
        temp.append(tempLin)
    return temp


freq_list = []
for i in range(6,9,1):
    for j in range(1,11,1):
        freq_list.append(float((j)*10.0**i))

freq_list = list(set(freq_list))
freq_list.sort()

directory = "C:/Users/hugo/PhD/Data/frequency_dom_test/project_dimensions_Jun_2012/simulation_data/results_ferr_plates/" #Directory of data

horzSingReList = [directory+"x_scan_db_"+str(i)+".csv" for i in range(3,0,-1)]
horzSingImList = [directory+"x_scan_arg_"+str(i)+".csv" for i in range(3,0,-1)]
horzDipReList = [directory+"x_dip_db_"+str(i)+".csv" for i in range(3,0,-1)]
horzDipImList = [directory+"x_dip_arg_"+str(i)+".csv" for i in range(3,0,-1)]
horzSingReDat = []
horzSingImDat = []
horzDipReDat = []
horzDipImDat = []

for i in range(0,len(horzSingReList)):
    fileTar = horzSingReList[i]
    if i==1:
        temp = importDat(fileTar)
        horzSingReDat+=temp[1:-1]
    else:
        horzSingReDat+=importDat(fileTar)

for i in range(0,len(horzSingImList)):
    fileTar = horzSingImList[i]
    if i==1:
        temp = importDat(fileTar)
        horzSingImDat+=temp[1:-1]
    else:
        horzSingImDat+=importDat(fileTar)

for i in range(0,len(horzDipReList)):
    fileTar = horzDipReList[i]
    if i==1:
        temp = importDat(fileTar)
        horzDipReDat+=temp[1:-1]
    else:
        horzDipReDat+=importDat(fileTar)

for i in range(0,len(horzDipImList)):
    fileTar = horzDipImList[i]
    if i==1:
        temp = importDat(fileTar)
        horzDipImDat+=temp[1:-1]
    else:
        horzDipImDat+=importDat(fileTar)

horzSingReDat = sp.array(horzSingReDat)
print horzSingReDat
horzSingReDat = sp.array(analSingRe(horzSingReDat, freq_list, lenTot, lenTot, rWire, apDev))
horzSingImDat = sp.array(horzSingImDat)
horzSingImDat = sp.array(analSingIm(horzSingImDat, freq_list, lenTot, lenTot, rWire, apDev))
horzDipReDat = sp.array(horzDipReDat)
horzDipReDat = sp.array(analDipRe(horzDipReDat, freq_list, lenTot, lenTot, rWire, rSep, apDev))
for i in range((len(horzDipImDat) - 10),len(horzDipImDat)):
    horzDipImDat[i][1] = float(horzDipImDat[i][1])-180
horzDipImDat = sp.array(horzDipImDat)
horzDipImDat = sp.array(analDipIm(horzDipImDat, freq_list, lenTot, lenTot, rWire, rSep, apDev))



##pl.loglog()
pl.semilogy()
pl.plot(freq_list, abs(horzDipReDat), 'kx')
pl.plot(freq_list, abs(horzDipImDat), 'rx')
##pl.show()
pl.clf()

print horzSingReDat

##pl.loglog()
pl.plot(freq_list, horzSingReDat[:,2], 'k-')
##pl.plot(freq_list[:18], horzSingImDat[:18], 'rx')
pl.axis([0,10**10,-2000,7000])
pl.show()
pl.clf()
