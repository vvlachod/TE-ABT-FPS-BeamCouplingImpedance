import scipy as sp
import pylab as pl
import time, os, sys, csv, time
import scipy.integrate as inte

start = time.time()

C = 299792458.0
circ=26659.0
p_bunch = 1.15*10**11
Q_part = 1.6*10**-19
f_sep = 4*10**7
f_rev = C/circ



def gauss_prof(dist, sigma, order):
    return sp.e**(-order*(dist/sigma)**2)

def para_prof(dist, bunch_length):
    return 1-(2*dist/bunch_length)**2

def cos_prof(dist, bunch_length):
    return sp.cos(sp.pi/bunch_length*dist)**2

if f_sep == 2*10**7:
    n_bunches = 1368
    possible_bunches = 1768
    

elif f_sep == 4*10**7:
    n_bunches = 2808
    possible_bunches = 3528

I_b = p_bunch*Q_part*f_sep
power_tot = I_b**2*n_bunches/possible_bunches/f_sep

spectra_directory = "E:/PhD/1st_Year_09-10/Data/Bunch_Spectra/"
##spectra_directory = "/media/CORSAIR/Bunch_Spectra/"
spectra_list = [spectra_directory+i for i in os.listdir(spectra_directory)]
impedance_dir = "E:/PhD/1st_Year_09-10/Data/LEB_heating/"
##impedance_dir = "/media/CORSAIR/LEB_heating/"
output_dir = impedance_dir+"plots/" 
try:
    os.mkdir(output_dir)
except:
    pass

file_ent = spectra_list[5]

impedance_list = [impedance_dir+i for i in os.listdir(impedance_dir)]
file_list = [i for i in os.listdir(impedance_dir)]
data_file = open(file_ent, 'r+')
data = data_file.readlines()
data_file.close()

######## Measured Spectra #################

k = 0


for file_imp in impedance_list:
    if ".dat" in file_imp:
        print file_imp
        input_file = open(file_imp, 'r+')
        tar = csv.reader(input_file, delimiter=" ")
        imp_data = []
        i=0
        for row in tar:
            if i!=0:
                row = map(float, row)
                imp_data.append(row)
            i+=1
        cumulative = []
        imp_data = sp.array(imp_data)
        total_p_loss=0.0
        start = time.time()
        for i in range(0,len(data)/2):
            j = 30
            while j<len(imp_data):
                if float(data[i])<10.0**6:
                    pass
                if abs(float(data[i])-imp_data[j,0])<1*10**6:
                    p_loss = 2*power_tot*imp_data[j,1]*(float(data[1])-float(data[0]))*10**(float(data[i+32000])/40)
                    total_p_loss+=p_loss
                    cumulative.append([data[i],p_loss])
                    pass
                j+=1
        cumulative = sp.array(cumulative)
        print time.time() - start
        print "Power loss for %s is %f (W)" % (file_imp, total_p_loss)    
        pl.plot(cumulative[:,0], cumulative[:,1])
        pl.xlabel("Frequency (Hz)", fontsize=16)
        pl.ylabel("Power Loss (W)")
        pl.savefig(output_dir+file_imp.strip(impedance_dir)+"_power_loss.png")
        pl.savefig(output_dir+file_imp.strip(impedance_dir)+"_power_loss.eps")
        pl.savefig(output_dir+file_imp.strip(impedance_dir)+"_power_loss.pdf")
        input_file.close()
        pl.clf()
        k+=1


######## Theoretical Spectra #################
##
##mark_length = 1.1*10**-9
##time_domain_parabolic = []
##time_domain_cos = []
##time_domain_gauss = []
##time_domain_gauss_trunc = []
##data_points = 10000.0
##sample_rate = 0.1/(mark_length/data_points)
##bunch_length = 1.2*10**-9
##
##if f_sep == 2*10**7:
##    t=pl.r_[-22.726*mark_length:22.726*mark_length:1/sample_rate]
##    n_bunches = 1768
##
#### For 25ns spacing
##elif f_sep == 4*10**7:
##    t=pl.r_[-11.357*mark_length:11.357*mark_length:1/sample_rate]
##    ##print len(t)
##omega = sample_rate
##
##for i in t:
##    if abs(i)<=bunch_length/2:
##        time_domain_parabolic.append(para_prof(i,bunch_length))           ## Parabolic bunch profile
##        time_domain_cos.append(cos_prof(i,bunch_length))  ## cos^2 profile
##    else:
##        time_domain_parabolic.append(0)
##        time_domain_cos.append(0)
##    time_domain_gauss.append(gauss_prof(i,bunch_length,30))           ## gaussian profile
##
##        
##time_domain_parabolic = sp.array(time_domain_parabolic)
##time_domain_cos = sp.array(time_domain_cos)
##time_domain_gauss = sp.array(time_domain_gauss)
##N=len(t)
##freq_domain_parabolic=sp.fft(time_domain_parabolic)
##freq_domain_cos=sp.fft(time_domain_cos)
##freq_domain_gauss=sp.fft(time_domain_gauss)
##f = sample_rate*sp.r_[0:(N/2)]/N
##n=len(f)
##
##freq_domain_parabolic = freq_domain_parabolic[0:n]/freq_domain_parabolic[0]
##freq_domain_cos = freq_domain_cos[0:n]/freq_domain_cos[0]
##freq_domain_gauss = freq_domain_gauss[0:n]/freq_domain_gauss[0]
##
##
##
##
##for file_imp in impedance_list:
##    if ".dat" in file_imp:
##        heating_para = 0.0
##        heating_cos = 0.0
##        heating_gauss = 0.0
##        print file_imp
##        input_file = open(file_imp, 'r+')
##        tar = csv.reader(input_file, delimiter=" ")
##        imp_data = []
##        i=0
##        for row in tar:
##            if i!=0:
##                row = map(float, row)
##                imp_data.append(row)
##            i+=1
##        cumulative = []
##        imp_data = sp.array(imp_data)
##        total_p_loss=0.0
##        start = time.time()
##        i=0
##        while f[i] < 2*10**9:
##            j = 0
##            while j<len(imp_data):
##                if f[i] == 0:
##                    heating_para += 0
##                    heating_cos+=0
##                    heating_gauss+=0
##                elif abs(f[i] -imp_data[j,0])<1*10**6:
##                    heating_para+= 2*power_tot*imp_data[j,1]*abs(freq_domain_parabolic[i])**2
##                    heating_cos+= 2*power_tot*imp_data[j,1]*abs(freq_domain_cos[i])**2
##                    heating_gauss+= 2*power_tot*imp_data[j,1]*abs(freq_domain_gauss[i])**2
##                    pass
##                j+=1
##            i+=1
##        cumulative = sp.array(cumulative)
##        print time.time() - start
##        print "Power loss for %s is %f, %f, %f (W)" % (file_imp, heating_para, heating_cos, heating_gauss)    
##
