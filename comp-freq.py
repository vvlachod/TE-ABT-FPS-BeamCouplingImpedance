import sys, os, csv,math
import scipy as sp

directory_name = "C:/Users/hday/Documents/PhD/1st_Year_09-10/Data/LHC-MKI-data/copper-tube/displaced-wire/cupipe-0mmhorz-0mmvert/"
file_name = "09MAR2010-S21-FREQDOM-PHASE-CAL-1MHZ2GHZ-COPPERPORT-0MMHORZ-0MMVERT.CSV"
imp_data = csv.reader(open(directory_name+file_name, 'r+'), delimiter=',')
output = csv.writer(open(directory_name+'phase_output.CSV', 'w+'), lineterminator='\n')

toast=[]
for row in imp_data:
    toast.append(row)
    
for i in range(0,len(toast)):
    for j in range(0,len(toast)):
        if toast[i][0] == toast[j][3]:
            output.writerow(toast[i][:3]+toast[j][3:])


