import csv, os, sys
import scipy as sp
import pylab as pl

def quickSort(tar):
    pivot = int(len(tar)/2.0-1)
    tar.pop(pivot)
    less_than = []
    greater_than = []
    for i in tar:
        if i <= tar[pivot]:
            less_than.append(i)
        else:
            greater_than.append(i)
    return less_than+tar[pivot]+greater_than
        

disp = 0.001
length = 2.88
f_rev = 3*10**8/27000.0
directory_wire = "E:/PhD/1st_Year_09-10/Data/wire-meas-cav/cav_wire_results/" #Directory of data
directory_no_wire = "E:/PhD/1st_Year_09-10/Data/wire-meas-cav/cav_no_wire_results/" #Directory of data
directory_wire_disp = "E:/PhD/1st_Year_09-10/Data/wire-meas-cav/cav_wire_disp_results/" #Directory of data

##dip_BL_file = "vertical.csv"
##dip_kroyer_file = "vertical_mine.csv"


list_files = [directory_wire+i for i in os.listdir(directory_wire)]
updated_file_list = []
for i in range(0,len(list_files)):
    if '.csv' in list_files[i]:
        updated_file_list.append(list_files[i])
    else:
        pass



measurement_repository_wire = []
temp=[]
for data_file in updated_file_list:
    input_file = open(data_file, 'r+')
    tar = input_file.readlines()
    for row in tar[1:-1]:
        if len(row)>1:
            row = map(float, row.split(","))
            temp.append(row)
    input_file.close()


##print total/len(temp)
measurement_repository_wire=sp.sort(sp.array(temp),1)

list_files = [directory_wire_disp+i for i in os.listdir(directory_wire_disp)]
updated_file_list = []
for i in range(0,len(list_files)):
    if '.csv' in list_files[i]:
        updated_file_list.append(list_files[i])
    else:
        pass



measurement_repository_wire_disp = []
temp=[]
for data_file in updated_file_list:
    input_file = open(data_file, 'r+')
    tar = input_file.readlines()
    for row in tar[1:-1]:
        if len(row)>1:
            row = map(float, row.split(","))
            temp.append(row)
    input_file.close()


##print total/len(temp)
measurement_repository_wire_disp=sp.sort(sp.array(temp),1)


list_files = [directory_no_wire+i for i in os.listdir(directory_no_wire)]
updated_file_list = []
for i in range(0,len(list_files)):
    if '.csv' in list_files[i]:
        updated_file_list.append(list_files[i])
    else:
        pass



measurement_repository_no_wire = []
temp=[]
for data_file in updated_file_list:
    input_file = open(data_file, 'r+')
    tar = input_file.readlines()
    for row in tar[1:-1]:
        if len(row)>1:
            row = map(float, row.split(","))
            temp.append(row)
    input_file.close()


##print total/len(temp)
measurement_repository_no_wire=sp.sort(sp.array(temp),1)




##pl.semilogx()
pl.plot(measurement_repository_wire[:,1], measurement_repository_wire[:,0], 'k-', label= "With Wire")
pl.plot(measurement_repository_wire_disp[:,1], measurement_repository_wire_disp[:,0], 'b-', label= "With Wire Displaced")
pl.plot(measurement_repository_no_wire[:,1], measurement_repository_no_wire[:,0], 'r-', label= "Transmission no Wire")


pl.grid(linestyle="--", which = "major")
pl.xlabel("Frequency (GHz)",fontsize = 16)                  #Label axes
pl.ylabel("$S_{21}$ (dB)",fontsize = 16)
##pl.title("", fontsize = 16)
pl.legend(loc="lower right")
pl.axis([0,40,-250,1])
##pl.savefig(directory+"test"+".pdf")                  # Save fig for film
pl.show()
pl.clf()   
