import scipy as sp
import pylab as pl
import time, os, sys, csv
import scipy.integrate as inte
import matplotlib.pyplot as plt


###### Define Impedance from resonator ######

def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def gauss_prof(dist, sigma, order):
    return sp.e**(-order*(dist/(2*sigma))**2)

def para_prof(dist, bunch_length):
    return 1-(2*dist/bunch_length)**2

def cos_prof(dist, bunch_length):
    return sp.cos(sp.pi/bunch_length*dist)**2

def extract(fileTar):
    tmpFile = open(fileTar, 'r+')
    dataIn = tmpFile.readlines()
    tmpFile.close()
    count = 0
    store = []
    for row in dataIn:
        if count > 3:
            store.append(map(float, row.rsplit()))
        count+=1

    return sp.array(store)

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

###### Define constants ####
p_bunch = 1.7*10.0**11
n_bunches = 1380
Q_part = 1.6*10.0**-19
f_rev = 2.0*10.0**7
charge = 18.4*10**-9
t_bunch = 50*10**-9
sigma_z = 0.075
C = 299792458.0
circ = 26689
eps0 = 8.854 * 10 **-12
mu0 = 4*sp.pi*10**-7
Z0 = 120*sp.pi
start=time.time()
bound_typ = "PerfE"
x01 = 0.001
x02 = 0.0011
dy = 0.001
disp_y = 0.0005
##top_directory = "E:/PhD/1st_Year_09-10/Data/TCTP/eigenmode_solutions/longitudinal_eigenmodes/"
top_directory = "C:/Users/hugo/PhD/Data/TCTP/eigenmode_solutions/longitudinal_eigenmodes_vacuum/"

eigenmode_store = [] ###### first column - freq, second - Q, Third R, Fourth - Power Loss
eigenmode_store_transverse_Ez = []
eigenmode_store_transverse_EyBx = []

power_loss_tot = 0.0


I_b = p_bunch*Q_part*C/circ*n_bunches
power_tot = I_b**2


#################### Heating Section #########################

########### Calculation from frequency domain ##########

print I_b, power_tot

mark_length = 1.1*10**-9
time_domain_parabolic = []
time_domain_cos = []
time_domain_gauss = []
time_domain_gauss_trunc = []
data_points = 10000.0
sample_rate = 0.1/(mark_length/data_points)
bunch_length = 1.1*10**-9

## For 50ns spacing 22.726
if f_rev == 2*10**7:
    t=pl.r_[-22.726*mark_length:22.726*mark_length:1/sample_rate]
    n_bunches = 144

## For 25ns spacing
elif f_rev == 4*10**7:
    t=pl.r_[-11.357*mark_length:11.357*mark_length:1/sample_rate]
    n_bunch = 144
    ##print len(t)
omega = sample_rate

for i in t:
    if abs(i)<=bunch_length/2:
        time_domain_parabolic.append(para_prof(i,bunch_length))           ## Parabolic bunch profile
        time_domain_cos.append(cos_prof(i,bunch_length))  ## cos^2 profile
    else:
        time_domain_parabolic.append(0)
        time_domain_cos.append(0)
    time_domain_gauss.append(gauss_prof(i,bunch_length,30))           ## gaussian profile

        
time_domain_parabolic = sp.array(time_domain_parabolic)
time_domain_cos = sp.array(time_domain_cos)
time_domain_gauss = sp.array(time_domain_gauss)
N=len(t)
freq_domain_parabolic=sp.fft(time_domain_parabolic)
freq_domain_cos=sp.fft(time_domain_cos)
freq_domain_gauss=sp.fft(time_domain_gauss)
f = sample_rate*sp.r_[0:(N/2)]/N
n=len(f)

freq_domain_parabolic = freq_domain_parabolic[0:n]/freq_domain_parabolic[0]
freq_domain_cos = freq_domain_cos[0:n]/freq_domain_cos[0]
freq_domain_gauss = freq_domain_gauss[0:n]/freq_domain_gauss[0]

###### To dB scale    ########




############ Calculation from time domain #############

dataDir = "E:/PhD/1st_Year_09-10/Data/TCTP/forUse/"
dat8c11 = sp.array(extract_dat(dataDir+"TCTP8C11.csv"))
dat4s60 = sp.array(extract_dat(dataDir+"TCTP4S60.csv"))
datclosed = sp.array(extract_dat(dataDir+"TCTPClosed.csv"))
datopen = sp.array(extract_dat(dataDir+"TCTPOpen.csv"))

pl.plot(dat8c11[:,0], dat8c11[:,1], label="Time Domain, Ferrite, 8C11")
##pl.plot(dat4s60[:,0], dat4s60[:,1], label="Time Domain, 4S60")
pl.plot(datclosed[:,0], datclosed[:,1], label="Time Domain, RF fingers, Phase 1")
pl.plot(datopen[:,0], datopen[:,1], label="Time Domain, Gap, Open")
pl.xlabel("Frequency (GHz)", fontsize="16")
pl.ylabel("$\Re{}e$ ($Z_{\parallel}$) ($\Omega$)", fontsize="16")
pl.legend(loc="upper left")
pl.axis([0,2.5,-10,300])
##pl.show()
pl.clf()

pl.plot(dat8c11[:,0], dat8c11[:,3], label="Time Domain, Ferrite, 8C11")
##pl.plot(dat4s60[:,0], dat4s60[:,3], label="Time Domain, 4S60")
pl.plot(datclosed[:,0], datclosed[:,3], label="Time Domain, RF fingers, Phase 1")
pl.plot(datopen[:,0], datopen[:,3], label="Time Domain, Gap, Open")
pl.xlabel("Frequency (GHz)", fontsize="16")
pl.ylabel("$\Im{}m$ ($Z_{\parallel}$) ($\Omega$)", fontsize="16")
pl.legend(loc="upper left")
pl.axis([0,2.5,-100,300])
pl.show()
pl.clf()

print "Time Taken for code to run = %f (s)" % (time.time()-start)


