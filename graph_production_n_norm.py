import csv
import scipy as sp
import pylab as pl

f_rev = 3*10**8/27000.0
directory = "C:/Users/hday/Documents/PhD/1st_Year_09-10/Data/LHC-TDS-Coll/" #Directory of data
data_tds_long_1 = "TDS-impedance/TDS-x-impedance_x_beam_5_y_beam_0_x_int_0_y_int_0.csv"
data_tds_long_2 = "revision-2/impedances/half-open-horz-dip-x-beam-3-y-beam-0-x-int-0-y-int-0.csv"
file_data_tsutsui = "tungsten_dip_vert.csv"

##dip_BL_file = "vertical.csv"
##dip_kroyer_file = "vertical_mine.csv"


keeper=[]
data_list_1 = []

input_file = open(directory+data_tds_long_1, 'r+')
tar = csv.reader(input_file, delimiter=",")

i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        row[0] = float(row[0])
        row[1] = -float(row[1])/0.005
        row[3] = -float(row[3])/0.005
##        n = 10.0**6*float(row[0])/f_rev
##        row[1] = float(row[1])/n
##        row[3] = float(row[3])/n
        data_list_1.append(row)
    i+=1

input_file.close()
data_sims_1 = sp.array(data_list_1)
##

data_list_1 = []
input_file = open(directory+data_tds_long_2, 'r+')
tar = csv.reader(input_file, delimiter=",")

i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        row[0] = float(row[0])
        row[1] = float(row[1])/0.003
        row[3] = float(row[3])/0.003
##        n = 10.0**6*float(row[0])/f_rev
##        row[1] = float(row[1])/n
##        row[3] = float(row[3])/n
        data_list_1.append(row)
    i+=1

input_file.close()
data_sims_2 = sp.array(data_list_1)
##

data_list_1 = []
##
input_file = open(directory+file_data_tsutsui, 'r+')
tar = csv.reader(input_file)

for row in tar:
    row = map(float, row)
    row[1] = float(row[1])/0.003
    row[2] = float(row[2])/0.003
##    n = float(row[0])/f_rev
##    row[1] = float(row[1])/n
##    row[2] = float(row[2])/n
    row[0] = float(row[0])/(10**6)
    data_list_1.append(row)


input_file.close()
data_tsutsui = sp.array(data_list_1)




##pl.loglog()
pl.plot(data_sims_1[:,0], data_sims_1[:,1], 'k-', markersize=8.0,label = "Real Longitudinal Impedance - Rev 1")                    # plot fitted curve
pl.plot(data_sims_1[:,2], data_sims_1[:,3], 'k:', markersize=8.0,label = "Imaginary Longitudinal Impedance - Rev 1")                    # plot fitted curve
pl.plot(data_sims_2[:,0], data_sims_2[:,1], 'b-', markersize=8.0,label = "Real Londitudinal Impedance - Rev 2")                    # plot fitted curve
pl.plot(data_sims_2[:,2], data_sims_2[:,3], 'b:', markersize=8.0,label = "Imaginary Longitudinal Impedance - Rev 2")                    # plot fitted curve
##pl.plot(data_tsutsui[:,0], data_tsutsui[:,1], 'r-', markersize=8.0,label = "Real Longitudinal Impedance - Tungsten")                    # plot fitted curve
##pl.plot(data_tsutsui[:,0], data_tsutsui[:,2], 'r:', markersize=10.0,label = "Imaginary Longitudinal Impedance - Tungsten")                    # plot fitted curve


pl.grid(linestyle="--", which = "major")
pl.xlabel("Frequency (MHz)",fontsize = 16)                  #Label axes
pl.ylabel("Impedance Z$(\Omega/m)$",fontsize = 16)
##pl.title("", fontsize = 16)
pl.legend(loc="lower left")
pl.axis([1,2000,-10000,10000])
pl.savefig(directory+"TDS_horz_dip_re_wall_second"+".pdf")                  # Save fig for film
pl.show()
pl.clf()   
