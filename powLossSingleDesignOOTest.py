import csv, time, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack
import mpl_toolkits.mplot3d as ToolKit3d
import impedance.impedance as imp

####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

######### Define importing, analysis functions #########

c=299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005
lWire = 3.551
lDUT = 2.45
resCopper = 17.24*10.0**-9
resOuter = 1/(3.3*10.0**7)
ZChar = 60*sp.log(dAper/rWire)

def minValIndex(arrMark, minValue=10.0):
    return sp.array([i for i in range(0,len(arrMark)) if arrMark[i]>minValue]) 

start=time.time()

length = 2.88
f_rev = C/27000.0
nBunches = 2808
qPart = 1.6*10.0**-19
nPart=1.15*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=1.001*10.0**-9
testImp = [250*10**6, 10.0, 10000]
bunchCur=nPart*qPart*f_rev
print bCur, bunchCur

##### Directory definiton #####

##MKI70mmOverlapModMonitors= imp.ImpedanceModel("E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/70mmOverlapPostLS1ModMonitors/", "70mm Overlap Post-LS1")
##MKI70mmOverlapModMonitors.ReadLossFiles()
##MKI70mmOverlapModMonitors.BinnedLosses()
##MKI70mmOverlapModMonitors.CalcGaussPowLoss(bCur, 25.0*10**-9, bLength)
##
##MKI70mmOverlapPostLS2= imp.ImpedanceModel("E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/70mmOverlapWith1mmAirGap/", "70mm Overlap Post-LS2")
##MKI70mmOverlapPostLS2.ReadLossFiles()
##MKI70mmOverlapPostLS2.BinnedLosses()
##MKI70mmOverlapPostLS2.CalcGaussPowLoss(bCur, 25.0*10**-9, bLength)
##
##MKI70mmOverlapFerrCollar = imp.ImpedanceModel("E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/70mmOverlapPostLS1FerriteCollar/", "70mm Overlap Post-LS1, Ferrite Collar")
##MKI70mmOverlapFerrCollar.ReadLossFiles()
##MKI70mmOverlapFerrCollar.BinnedLosses()
##MKI70mmOverlapFerrCollar.CalcGaussPowLoss(bCur, 25.0*10**-9, bLength)
##
##MKIPostLS1 = imp.ImpedanceModel("E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/ferrRing60mmOuterD/", "117mm Overlap Post-LS1")
##MKIPostLS1.ReadLossFiles()
##MKIPostLS1.BinnedLosses()
##MKIPostLS1.CalcGaussPowLoss(bCur, 25.0*10**-9, bLength)

##MKIPostLS2 = imp.ImpedanceModel("E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/airGap1mmBottomSlot5mm/", "117mm Overlap Post-LS2")
##MKIPostLS2.ReadLossFiles()
##MKIPostLS2.BinnedLosses()
##MKIPostLS2.CalcGaussPowLoss(bCur, 25.0*10**-9, bLength)

MKIPostLS1FerriteCollar = imp.ImpedanceModel("E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/FerriteCollar20mm/", "117mm Overlap Post-LS1 Ferrite Collar")
MKIPostLS1FerriteCollar.ReadLossFiles()
MKIPostLS1FerriteCollar.BinnedLosses()
MKIPostLS1FerriteCollar.CalcGaussPowLoss(bCur, 25.0*10**-9, bLength)

MKIPostLS1FerriteCollar8mm = imp.ImpedanceModel("E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/117mmOverlap8mmFerrCollarModMonitors/", "117mm Overlap Post-LS1 Ferrite Collar")
MKIPostLS1FerriteCollar8mm.ReadLossFiles()
MKIPostLS1FerriteCollar8mm.BinnedLosses()
MKIPostLS1FerriteCollar8mm.CalcGaussPowLoss(bCur, 25.0*10**-9, bLength)

meshtime=time.time()-start
print meshtime

##print MKI70mmOverlapModMonitors.powLossTot, sp.sum(MKI70mmOverlapModMonitors.freqPowLoss[:,1]), MKI70mmOverlapModMonitors.cumPowLoss[-1]
##print MKI70mmOverlapPostLS2.powLossTot, sp.sum(MKI70mmOverlapPostLS2.freqPowLoss[:,1]), MKI70mmOverlapPostLS2.cumPowLoss[-1]
##print MKI70mmOverlapFerrCollar.powLossTot, sp.sum(MKI70mmOverlapFerrCollar.freqPowLoss[:,1]), MKI70mmOverlapFerrCollar.cumPowLoss[-1]
##print MKIPostLS1.powLossTot, sp.sum(MKIPostLS1.freqPowLoss[:,1]), MKIPostLS1.cumPowLoss[-1]
##print MKIPostLS2.powLossTot, sp.sum(MKIPostLS2.freqPowLoss[:,1]), MKIPostLS2.cumPowLoss[-1]
print MKIPostLS1FerriteCollar.powLossTot, sp.sum(MKIPostLS1FerriteCollar.freqPowLoss[:,1]), MKIPostLS1FerriteCollar.cumPowLoss[-1]
print MKIPostLS1FerriteCollar8mm.powLossTot, sp.sum(MKIPostLS1FerriteCollar8mm.freqPowLoss[:,1]), MKIPostLS1FerriteCollar8mm.cumPowLoss[-1]

##MKI70mmOverlapModMonitors.PlotBinnedLosses()
##MKI70mmOverlapPostLS2.PlotBinnedLosses()
##MKI70mmOverlapFerrCollar.PlotBinnedLosses()
##MKIPostLS1.PlotBinnedLosses()
##MKIPostLS2.PlotBinnedLosses()
MKIPostLS1FerriteCollar.PlotBinnedLosses()
MKIPostLS1FerriteCollar8mm.PlotBinnedLosses()
##pl.plot(z[0,0],binnedLossesRings60mmDiam, "k-",label="Losses averaged to single components, Post LS1")
pl.xlabel("Displacement from centre of magnet (mm)", fontsize=24.0)
pl.ylabel("Power Loss (W/m)", fontsize=24.0)
##pl.ylabel("Power Loss (a.u)", fontsize=16.0)
pl.legend(loc="upper right")
pl.show()
pl.clf()

##for i in range(0, len(MKI70mmOverlap.normedLossMaps)):
##    pl.plot(MKI70mmOverlap.zMesh, MKI70mmOverlap.normedLossMaps[i])


##for i in range(0, len(MKIPostLS1.normedLossMaps)):
##    pl.plot(MKIPostLS1.zMesh, MKIPostLS1.normedLossMaps[i])
    

##pl.plot(MKI70mmOverlapModMonitors.zMesh, MKI70mmOverlapModMonitors.lossMapProf, label="Weighted Losses Summed 70mm Overlap")
##pl.plot(MKI70mmOverlapFerrCollar.zMesh, MKI70mmOverlapFerrCollar.lossMapProf, label="Weighted Losses Summed 70mm Overlap Ferrite Collar")
##pl.plot(MKIPostLS1.zMesh, MKIPostLS1.lossMapProf, label="Weighted Losses Summed Post-LS1")
pl.plot(MKIPostLS1FerriteCollar.zMesh, MKIPostLS1FerriteCollar.lossMapProf, label="Weighted Losses Summed FerriteCollar")
pl.plot(MKIPostLS1FerriteCollar8mm.zMesh, MKIPostLS1FerriteCollar8mm.lossMapProf, label="Weighted Losses Summed FerriteCollar")
pl.legend()
pl.show()
pl.clf()

##MKI70mmOverlapModMonitors.PlotImpedance()
##MKI70mmOverlapFerrCollar.PlotImpedance()
##MKI70mmOverlapPostLS2.PlotImpedance()
##MKIPostLS1.PlotImpedance()
##MKIPostLS2.PlotImpedance()
MKIPostLS1FerriteCollar.PlotImpedance()
MKIPostLS1FerriteCollar8mm.PlotImpedance()
pl.xlabel("Frequency (GHz)", fontsize=24.0)
pl.ylabel("$Z_{\parallel}$ ($\Omega$)", fontsize=24.0)
##pl.ylabel("Power Loss (a.u)", fontsize=16.0)
pl.xlim([0,2.0])
pl.legend(loc="upper left")
pl.show()
pl.clf()

##MKI70mmOverlapModMonitors.PlotCumBinnedLosses()
##MKI70mmOverlapFerrCollar.PlotCumBinnedLosses()
##MKI70mmOverlapPostLS2.PlotCumBinnedLosses()
##MKIPostLS1.PlotCumBinnedLosses()
##MKIPostLS2.PlotCumBinnedLosses()
MKIPostLS1FerriteCollar.PlotCumBinnedLosses()
MKIPostLS1FerriteCollar8mm.PlotCumBinnedLosses()
pl.xlabel("Displacement from centre of magnet (mm)", fontsize=24.0)
pl.ylabel("Power Loss (W)", fontsize=24.0)
##pl.ylabel("Power Loss (a.u)", fontsize=16.0)
pl.legend(loc="upper left")
pl.show()
pl.clf()
