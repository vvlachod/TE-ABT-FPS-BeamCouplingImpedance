import csv, os, sys, math
import scipy as sp
import pylab as pl
from scipy import optimize as op

fitfunc = lambda p, x: p[2] + p[1]*x + p[0]*x**2
##fitfunc = lambda p, x: p[2] + p[1]*x + p[0]
errfunc = lambda p, x, y, err: (y-fitfunc(p, x))/err

lenMKI = 2.88
lenWire = 3.5356
lenElec = 3.53
C = 299792458

def importDatDip(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
##    print tempDat
    datStore = []
    for i in range(0,len(tempDat)/2):
        if i%2==0:
            temp = [float(tempDat[i].rstrip("\n"))*10.0**6, float(tempDat[i+len(tempDat)/2].rstrip("\n"))]
            datStore.append(temp)
        else:
            pass
    return datStore

def importDatSing(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
##    print tempDat
    datStore = []
    for i in range(0,len(tempDat)/2):
        temp = [float(tempDat[i].rstrip("\n"))*10.0**6, float(tempDat[i+len(tempDat)/2].rstrip("\n"))]
        datStore.append(temp)
    return datStore


def analDipRe(data, lenDUT, r_sep):
    temp = []
    for i in range(0,len(data)):
        temp.append(C/(2*sp.pi*data[i,0]*r_sep**2)*data[i,1]*lenDUT) 
    return temp

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

######### Analysis of the Wire Measurement Results ###########

order=100
listNs = sp.linspace(1,order, order-1)
listFreqs = listNs*C/(2.0*lenWire)


directorySingMKI = "C:/Users/hugo/PhD/Data/mki-data/Central-Port/displaced-wire/" #Directory of data
directorySingCU = "C:/Users/hugo/PhD/Data/mki-data/copper-tube/displaced-wire/" #Directory of data
directoryTwoMKI = "C:/Users/hugo/PhD/Data/mki-data/two-wire-measurements/" #Directory of data
directoryTwoCU = "C:/Users/hugo/PhD/Data/mki-data/copper-tube/two-wire-measurments/" #Directory of data

horzSingReListMKI = [directorySingMKI+str(i)+"mm_x_0mm_y/impedance-results.txt" for i in range(-6,9,3)]
vertSingReListMKI = [directorySingMKI+"0mm_x_"+str(i)+"mm_y/impedance-results.txt" for i in range(-3,9,3)]
horzSingReListCU = [directorySingCU+"cupipe-"+str(i)+"mmhorz_0mmvert/impedance-results.txt" for i in range(-6,9,3)]
vertSingReListCU = [directorySingCU+"cupipe-0mmhorz_"+str(i)+"mmvert/impedance-results.txt" for i in range(-6,9,3)]
horzDipReListMKI = directoryTwoMKI+"dipole-7mm-seperation-horz/impedance-results.txt"
vertDipReListMKI = directoryTwoMKI+"dipole-6mm-seperation-vertical/impedance-results.txt"
horzDipReListCU = directoryTwoCU+"Copper-tube-dipole-7mm-seperation-horz/impedance-results.txt"
vertDipReListCU = directoryTwoCU+"Copper-tube-dipole-7mm-seperation-vertical/impedance-results.txt"

sepWireXMKI = 0.007
sepWireYMKI = 0.006

horzSingReDatMKI = []
vertSingReDatMKI = []

tempHorz = []
for fileEnt in horzSingReListMKI:
    tempHorz.append(importDatSing(fileEnt))
horzSingReDatMKI = sp.array(tempHorz)
tempVert = []
for fileEnt in vertSingReListMKI:
    tempVert.append(importDatSing(fileEnt))
vertSingReDatMKI = sp.array(tempVert)

horzDipReDatMKI = sp.array([sp.array(importDatDip(horzDipReListMKI))[:,0], analDipRe(sp.array(importDatDip(horzDipReListMKI)), lenMKI, sepWireXMKI)])
vertDipReDatMKI = sp.array([sp.array(importDatDip(vertDipReListMKI))[:,0], analDipRe(sp.array(importDatDip(vertDipReListMKI)), lenMKI, sepWireYMKI)])

##time = lenElec/C
timeCu = 11.776*10.0**-9
timeMKI = 11.856*10.0**-9
##print time

##print horzDipReDatMKI[0,:], listFreqs[:len(vertDipReDatMKI[0,:])]

horzDipImDatMKI = sp.array([horzDipReDatMKI[0,:], horzDipReDatMKI[1,:]*sp.tan((-(timeMKI*2*sp.pi*horzDipReDatMKI[0,:])+(timeCu*2*sp.pi*listFreqs[:len(horzDipReDatMKI[0,:])])))])
vertDipImDatMKI = sp.array([vertDipReDatMKI[0,:], vertDipReDatMKI[1,:]*sp.tan((-(timeMKI*2*sp.pi*vertDipReDatMKI[0,:])+(timeCu*2*sp.pi*listFreqs[:len(vertDipReDatMKI[0,:])])))])

print (-(timeMKI*2*sp.pi*horzDipReDatMKI[0,:])+(timeCu*2*sp.pi*listFreqs[:len(horzDipReDatMKI[0,:])]))

imagTemp=[]

for dataList in horzSingReDatMKI[:]:
    imagTemp.append([dataList[:,0], dataList[:,1]*sp.tan((+(timeMKI*2*sp.pi*dataList[:,0])-(timeCu*2*sp.pi*listFreqs[:len(dataList)])))])

horzSingImDatMKI = sp.array(imagTemp)

imagTemp=[]

for dataList in horzSingReDatMKI[:]:
    imagTemp.append([dataList[:,0], dataList[:,1]*sp.tan((+(timeMKI*2*sp.pi*dataList[:,0])-(timeCu*2*sp.pi*listFreqs[:len(dataList)])))])

vertSingImDatMKI = sp.array(imagTemp)

horzLongRe = []
horzLongIm = []
horzTotTransRe = []
horzTotTransIm = []
vertLongRe = []
vertLongIm = []
vertTotTransRe = []
vertTotTransIm = []
horzConstRe = []
horzConstIm = []
vertConstRe = []
vertConstIm = []

for i in range(0,len(horzSingReDatMKI[1])):
    x_data = sp.linspace(-0.006,0.006,5)
    xPlot = sp.linspace(-0.010,0.010,1000)
    transLongRe = horzSingReDatMKI[:,i,1]
    pinit = [0.0, 1.0, 1.0]
    y_err = transLongRe
    out = op.leastsq(errfunc, pinit, args=(x_data,transLongRe,y_err), full_output=1)
    pfinal_real = out[0]
    yPlotRe = sp.polyval(pfinal_real, xPlot)
    covar_real=out[1]
    horzConstRe.append([horzSingReDatMKI[2,i,0], pfinal_real[1]])
    horzLongRe.append([horzSingReDatMKI[2,i,0], pfinal_real[2]])
    horzTotTransRe.append([horzSingReDatMKI[2,i,0], pfinal_real[0]*C/(2*sp.pi*horzSingReDatMKI[2,i,0])])
    pl.plot(x_data*10**3, transLongRe, 'kx', label="Data")
    pl.plot(xPlot*10**3, yPlotRe, 'r-', label="Fit")
    pl.legend(loc="upper center")
    pl.xlabel("Displacement (mm)", fontsize="16.0")
    pl.ylabel("$\Re{}e (Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
##    pl.savefig(savDir+'plot_'+str(i)+"_re.pdf")
##    pl.savefig(savDir+'plot_'+str(i)+"_re.png")
##    pl.savefig(savDir+'plot_'+str(i)+"_re.eps")
    pl.clf()


for i in range(0,len(horzSingImDatMKI[1,1])):
    x_data = sp.linspace(-0.006,0.006,5)
    xPlot = sp.linspace(-0.010,0.010,1000)
    transLongRe = horzSingImDatMKI[:,1,i]
    pinit = [0.0, 1.0, 1.0]
    y_err = transLongRe
    out = op.leastsq(errfunc, pinit, args=(x_data,transLongRe,y_err), full_output=1)
    pfinal_real = out[0]
    yPlotRe = sp.polyval(pfinal_real, xPlot)
    covar_real=out[1]
    horzConstIm.append([horzSingImDatMKI[2,0,i], pfinal_real[1]])
    horzLongIm.append([horzSingImDatMKI[2,0,i], pfinal_real[2]])
    horzTotTransIm.append([horzSingImDatMKI[2,0,i], pfinal_real[0]*C/(2*sp.pi*horzSingImDatMKI[2,0,i])])
    pl.plot(x_data*10**3, transLongRe, 'kx', label="Data")
    pl.plot(xPlot*10**3, yPlotRe, 'r-', label="Fit")
    pl.legend(loc="upper center")
    pl.xlabel("Displacement (mm)", fontsize="16.0")
    pl.ylabel("$\Im{}e (Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
##    pl.savefig(savDir+'plot_'+str(i)+"_im.pdf")
##    pl.savefig(savDir+'plot_'+str(i)+"_im.png")
##    pl.savefig(savDir+'plot_'+str(i)+"_im.eps")
    pl.clf()


for i in range(0,len(vertSingReDatMKI[1])):
    x_data = sp.linspace(-0.003,0.006,4)
    xPlot = sp.linspace(-0.010,0.010,1000)
    transLongRe = vertSingReDatMKI[:,i,1]
    pinit = [0.0, 1.0, 1.0]
    y_err = transLongRe
    out = op.leastsq(errfunc, pinit, args=(x_data,transLongRe,y_err), full_output=1)
    pfinal_real = out[0]
    yPlotRe = sp.polyval(pfinal_real, xPlot)
    covar_real=out[1]
    vertConstRe.append([vertSingReDatMKI[2,i,0], pfinal_real[1]])
    vertLongRe.append([vertSingReDatMKI[2,i,0], pfinal_real[2]])
    vertTotTransRe.append([vertSingReDatMKI[2,i,0], pfinal_real[0]*C/(2*sp.pi*vertSingReDatMKI[2,i,0])])
    pl.plot(x_data*10**3, transLongRe, 'kx', label="Data")
    pl.plot(xPlot*10**3, yPlotRe, 'r-', label="Fit")
    pl.legend(loc="upper center")
    pl.xlabel("Displacement (mm)", fontsize="16.0")
    pl.ylabel("$\Re{}e (Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
##    pl.savefig(savDir+'plot_'+str(i)+"_re.pdf")
##    pl.savefig(savDir+'plot_'+str(i)+"_re.png")
##    pl.savefig(savDir+'plot_'+str(i)+"_re.eps")
    pl.clf()

for i in range(0,len(vertSingImDatMKI[1,1])):
    x_data = sp.linspace(-0.003,0.006,5)
    xPlot = sp.linspace(-0.010,0.010,1000)
    transLongRe = vertSingImDatMKI[:,1,i]
    pinit = [0.0, 1.0, 1.0]
    y_err = transLongRe
    out = op.leastsq(errfunc, pinit, args=(x_data,transLongRe,y_err), full_output=1)
    pfinal_real = out[0]
    yPlotRe = sp.polyval(pfinal_real, xPlot)
    covar_real=out[1]
    vertConstIm.append([vertSingImDatMKI[2,0,i], pfinal_real[1]])
    vertLongIm.append([vertSingImDatMKI[2,0,i], pfinal_real[2]])
    vertTotTransIm.append([vertSingImDatMKI[2,0,i], pfinal_real[0]*C/(2*sp.pi*vertSingImDatMKI[2,0,i])])
    pl.plot(x_data*10**3, transLongRe, 'kx', label="Data")
    pl.plot(xPlot*10**3, yPlotRe, 'r-', label="Fit")
    pl.legend(loc="upper center")
    pl.xlabel("Displacement (mm)", fontsize="16.0")
    pl.ylabel("$\Im{}e (Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
##    pl.savefig(savDir+'plot_'+str(i)+"_im.pdf")
##    pl.savefig(savDir+'plot_'+str(i)+"_im.png")
##    pl.savefig(savDir+'plot_'+str(i)+"_im.eps")
    pl.clf()

horzLongRe=sp.array(horzLongRe)
horzLongIm=sp.array(horzLongIm)
vertLongRe=sp.array(vertLongRe)
vertLongIm=sp.array(vertLongIm)
horzTotTransRe=sp.array(horzTotTransRe)
horzTotTransIm=sp.array(horzTotTransIm)
vertTotTransRe=sp.array(vertTotTransRe)
vertTotTransIm=sp.array(vertTotTransIm)
horzConstRe=sp.array(horzConstRe)
horzConstIm=sp.array(horzConstIm)
vertConstRe=sp.array(vertConstRe)
vertConstIm=sp.array(vertConstIm)


horzReQuadMKI = horzDipReDatMKI[1,:]-horzTotTransRe[:,1]
horzImQuadMKI = horzDipImDatMKI[1,:]-horzTotTransIm[:,1]
vertReQuadMKI = vertDipReDatMKI[1,:]-vertTotTransRe[:,1]
vertImQuadMKI = vertDipImDatMKI[1,:]-vertTotTransIm[:,1]

###########  Simulation Analysis  ############

dirFiles = "C:/Users/hugo/PhD/Data/mki-data/15-screen-conductors-all-impedances/"

longImp = sp.array(extract_dat(dirFiles+"0mmx-0mmy/longitudinal-impedance.csv"))

horzDipList = [dirFiles+str(i)+"mmx-0mmy/horizontal-dipolar-impedance.csv" for i in range(-2,3,1)]
vertDipList = [dirFiles+"0mmx-"+str(i)+"mmy/vertical-dipolar-impedance.csv" for i in range(-2,3,1)]
horzQuadList = [dirFiles+"int"+str(i)+"mmx-int0mmy/horizontal-dipolar-impedance.csv" for i in range(-2,3,1)]
vertQuadList = [dirFiles+"int0mmx-int"+str(i)+"mmy/vertical-dipolar-impedance.csv" for i in range(-2,3,1)]

horzDipList.remove(dirFiles+"0mmx-0mmy/horizontal-dipolar-impedance.csv")
vertDipList.remove(dirFiles+"0mmx--2mmy/vertical-dipolar-impedance.csv")
horzQuadList.remove(dirFiles+"int0mmx-int0mmy/horizontal-dipolar-impedance.csv")
##vertQuadList.remove(dirFiles+"int0mmx-int-2mmy/vertical-dipolar-impedance.csv")


horzDipDat = []
vertDipDat = []
horzQuadDat = []
vertQuadDat = []

for datFile in horzDipList:
    horzDipDat.append(extract_dat(datFile))    
for datFile in vertDipList:
    vertDipDat.append(extract_dat(datFile))    
for datFile in horzQuadList:
    horzQuadDat.append(extract_dat(datFile))    
for datFile in vertQuadList:
    vertQuadDat.append(extract_dat(datFile))    

horzDipDat=sp.array(horzDipDat)
vertDipDat=sp.array(vertDipDat)
horzQuadDat=sp.array(horzQuadDat)
vertQuadDat=sp.array(vertQuadDat)

xDipDisp = sp.array([-0.002,-0.001,0.001,0.002])
yDipDisp = sp.linspace(-0.001,0.002,4)
xQuadDisp = sp.array([-0.002,-0.001,0.001,0.002])
yQuadDisp = sp.linspace(-0.002,0.002,5)

horzDipDip = []
horzDipConst = []
vertDipDip = []
vertDipConst = []
horzQuadQuad = []
horzQuadConst = []
vertQuadQuad = []
vertQuadConst = []

linFitFunc = lambda p, x: p[1]*x + p[0]
errFunc = lambda p, x, y, err: (y-linFitFunc(p, x))/err


for i in range(0, len(horzDipDat[1])):
    realPart = horzDipDat[:,i,1]
    imagPart = horzDipDat[:,i,3]
    pinit = [0.0, 1.0, 1.0]
    out = op.leastsq(errFunc, pinit, args=(xDipDisp,realPart,realPart), full_output=1)
    pfinal_real = out[0]
    pinit = [0.0, 1.0, 1.0]
    out = op.leastsq(errFunc, pinit, args=(xDipDisp,imagPart,imagPart), full_output=1)
    pfinal_imag = out[0]
    horzDipDip.append([horzDipDat[1,i,0], pfinal_real[1], pfinal_imag[1]])        
    horzDipConst.append([horzDipDat[1,i,0], pfinal_real[0], pfinal_imag[0]])

    realPart = vertDipDat[:,i,1]
    imagPart = vertDipDat[:,i,3]
    pinit = [0.0, 1.0, 1.0]
    out = op.leastsq(errFunc, pinit, args=(yDipDisp,realPart,realPart), full_output=1)
    pfinal_real = out[0]
    pinit = [0.0, 1.0, 1.0]
    out = op.leastsq(errFunc, pinit, args=(yDipDisp,imagPart,imagPart), full_output=1)
    pfinal_imag = out[0]
    vertDipDip.append([vertDipDat[1,i,0], pfinal_real[1], pfinal_imag[1]])        
    vertDipConst.append([vertDipDat[1,i,0], pfinal_real[0], pfinal_imag[0]])

    realPart = horzQuadDat[:,i,1]
    imagPart = horzQuadDat[:,i,3]
    pinit = [0.0, 1.0, 1.0]
    out = op.leastsq(errFunc, pinit, args=(xQuadDisp,realPart,realPart), full_output=1)
    pfinal_real = out[0]
    pinit = [0.0, 1.0, 1.0]
    out = op.leastsq(errFunc, pinit, args=(xQuadDisp,imagPart,imagPart), full_output=1)
    pfinal_imag = out[0]
    horzQuadQuad.append([horzQuadDat[1,i,0], pfinal_real[1], pfinal_imag[1]])        
    horzQuadConst.append([horzQuadDat[1,i,0], pfinal_real[0], pfinal_imag[0]])

    realPart = vertQuadDat[:,i,1]
    imagPart = vertQuadDat[:,i,3]
    pinit = [0.0, 1.0, 1.0]
    out = op.leastsq(errFunc, pinit, args=(yQuadDisp,realPart,realPart), full_output=1)
    pfinal_real = out[0]
    pinit = [0.0, 1.0, 1.0]
    out = op.leastsq(errFunc, pinit, args=(yQuadDisp,imagPart,imagPart), full_output=1)
    pfinal_imag = out[0]
    vertQuadQuad.append([vertQuadDat[1,i,0], pfinal_real[1], pfinal_imag[1]])        
    vertQuadConst.append([vertQuadDat[1,i,0], pfinal_real[0], pfinal_imag[0]])

horzDipDip = sp.array(horzDipDip)
horzDipConst = sp.array(horzDipConst)
vertDipDip = sp.array(vertDipDip)
vertDipConst = sp.array(vertDipConst)
horzQuadQuad = sp.array(horzQuadQuad)
horzQuadConst = sp.array(horzQuadConst)
vertQuadQuad = sp.array(vertQuadQuad)
vertQuadConst = sp.array(vertQuadConst)

    
pl.plot(horzLongRe[:,0]/10**9, horzLongRe[:,1]*lenMKI, 'kx', label="$\Re{}e(Z_{\parallel})$ Horizontal Fit")
##pl.plot(horzLongIm[:,0]/10**9, horzLongIm[:,1]*lenMKI, 'rx', label="$\Im{}m(Z_{\parallel})$ Horizontal Fit")
pl.plot(vertLongRe[:,0]/10**9, vertLongRe[:,1]*lenMKI, 'k+', label="$\Re{}e(Z_{\parallel})$ Vertical Fit")
##pl.plot(vertLongIm[:,0]/10**9, vertLongIm[:,1]*lenMKI, 'r+', label="$\Im{}m(Z_{\parallel})$ Vertical Fit")
pl.plot(longImp[:,0], longImp[:,1], 'k-', label="$\Re{}e(Z_{\parallel})$ Simulations")
##pl.plot(longImp[:,0], longImp[:,3], 'r-', label="$\Im{}m(Z_{\parallel})$ Simulations")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\parallel}$ $(\Omega)$", fontsize=16.0)
pl.legend(loc = "upper left")
##pl.axis([0,2,-200,100])
pl.grid(True)
##pl.show()
pl.clf()

pl.plot(horzDipReDatMKI[0,:]/10**9, horzDipReDatMKI[1,:], 'kx', label="$\Re{}e(Z_{\perp, x}^{dipolar})$")
##pl.plot(horzDipImDatMKI[0,:]/10**9, horzDipImDatMKI[1,:], 'rx', label="$\Im{}m(Z_{\perp, x}^{dipolar})$")
pl.plot(horzDipDip[:,0], -horzDipDip[:,1], 'k-', label="$\Re{}e(Z_{\perp, x}^{dipolar})$ Simulated")
##pl.plot(horzDipDip[:,0], horzDipDip[:,2], 'r-', label="$\Im{}m(Z_{\perp, x}^{dipolar})$ Simulated")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\perp}$ $(\Omega/m)$", fontsize=16.0)
pl.legend(loc = "upper left")
pl.grid(True)
##pl.axis([0,2,-200,100])
##pl.show()
pl.clf()

pl.plot(vertDipReDatMKI[0,:]/10**9, vertDipReDatMKI[1,:], 'kx', label="$\Re{}e(Z_{\perp, y}^{dipolar})$")
##pl.plot(vertDipImDatMKI[0,:]/10**9, vertDipImDatMKI[1,:], 'rx', label="$\Im{}m(Z_{\perp, y}^{dipolar})$")
pl.plot(vertDipDip[:,0], -vertDipDip[:,1], 'k-', label="$\Re{}e(Z_{\perp, y}^{dipolar})$ Simulated")
##pl.plot(vertDipDip[:,0], -vertDipDip[:,2], 'r-', label="$\Im{}m(Z_{\perp, y}^{dipolar})$ Simulated")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\perp}$ $(\Omega/m)$", fontsize=16.0)
pl.legend(loc = "upper left")
pl.grid(True)
##pl.axis([0,2,0,250*10.0**3])
##pl.show()
pl.clf()


pl.plot(horzDipReDatMKI[0,:]/10**9, horzReQuadMKI/6, 'kx', label="$\Re{}e(Z_{\perp, x}^{quadrupolar})$")
##pl.plot(horzDipImDatMKI[0,:]/10**9, horzImQuadMKI, 'rx', label="$\Im{}m(Z_{\perp, x}^{quadrupolar})$")
pl.plot(horzQuadQuad[:,0], horzQuadQuad[:,1], 'k-', label="$\Re{}e(Z_{\perp, x}^{quadrupolar})$ Simulated")
##pl.plot(horzQuadQuad[:,0], horzQuadQuad[:,2], 'r-', label="$\Im{}m(Z_{\perp, x}^{quadrupolar})$ Simulated")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\perp}$ $(\Omega/m)$", fontsize=16.0)
pl.legend(loc = "upper left")
pl.grid(True)
##pl.axis([0,2,-200,100])
##pl.show()
pl.clf()

pl.plot(vertDipReDatMKI[0,:]/10**9, vertReQuadMKI, 'kx', label="$\Re{}e(Z_{\perp, y}^{quadrupolar})$")
##pl.plot(vertDipImDatMKI[0,:]/10**9, vertImQuadMKI, 'rx', label="$\Im{}m(Z_{\perp, y}^{quadrupolar})$")
pl.plot(vertQuadQuad[:,0], -vertQuadQuad[:,1], 'k-', label="$\Re{}e(Z_{\perp, y}^{quadrupolar})$ Simulated")
##pl.plot(vertQuadQuad[:,0], vertQuadQuad[:,2], 'r-', label="$\Im{}m(Z_{\perp, y}^{quadrupolar})$ Simulated")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\perp}$ $(\Omega/m)$", fontsize=16.0)
pl.legend(loc = "upper left")
pl.grid(True)
##pl.axis([0,2,-200,100])
##pl.show()
pl.clf()

pl.plot(horzDipConst[:,0], horzDipConst[:,1], 'k-', label="$\Re{}e(Z_{\perp, x}^{Const, Dip})$ Simulated")
##pl.plot(horzDipConst[:,0], horzDipConst[:,2], 'r-', label="$\Im{}m(Z_{\perp, x}^{Const, Dip})$ Simulated")
pl.plot(horzQuadConst[:,0], horzQuadConst[:,1], 'k--', label="$\Re{}e(Z_{\perp, x}^{Const, Quad})$ Simulated")
##pl.plot(horzQuadConst[:,0], horzQuadConst[:,2], 'r--', label="$\Im{}m(Z_{\perp, x}^{Const, Quad})$ Simulated")
pl.plot(horzConstRe[:,0]/10**9, horzConstRe[:,1], 'kx', label="$\Re{}e(Z_{\perp, x}^{Const, Dip})$ Measured")
##pl.plot(horzConstIm[:,0]/10**9, horzConstIm[:,1], 'rx', label="$\Im{}m(Z_{\perp, x}^{Const, Dip})$ Measured")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\perp}$ $(\Omega)$", fontsize=16.0)
pl.legend(loc = "upper left")
pl.grid(True)
##pl.axis([0,2,-200,100])
##pl.show()
pl.clf()

pl.plot(vertDipConst[:,0], vertDipConst[:,1], 'k-', label="$\Re{}e(Z_{\perp}^{Const, Dip})$ Simulated")
##pl.plot(vertDipConst[:,0], vertDipConst[:,2], 'r-', label="$\Im{}m(Z_{\perp, y}^{Const, Dip})$ Simulated")
pl.plot(vertQuadConst[:,0], vertQuadConst[:,1], 'k-.', label="$\Re{}e(Z_{\perp}^{Const, Quad})$ Simulated")
##pl.plot(vertQuadConst[:,0], vertQuadConst[:,2], 'r-', label="$\Im{}m(Z_{\perp, y}^{Const, Quad})$ Simulated")
pl.plot(vertConstRe[:,0]/10**9, -vertConstRe[:,1], 'kx', label="$\Re{}e(Z_{\perp}^{Const})$ Measured")
##pl.plot(vertConstIm[:,0]/10**9, vertConstIm[:,1], 'rx', label="$\Im{}m(Z_{\perp, y}^{Const, Dip})$ Measured")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\perp}$ $(\Omega)$", fontsize=16.0)
pl.legend(loc = "upper left")
pl.grid(True)
##pl.axis([0,2,-200,100])
##pl.show()
pl.clf()

