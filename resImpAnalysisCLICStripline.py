import csv, time, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack
from scipy import signal

####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

######### Define importing, analysis functions #########

c=299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005
lWire = 1.2
lDUT = 1.2
resCopper = 17.24*10.0**-9
resOuter = 1/(3.3*10.0**7)
ZChar = 60*sp.log(dAper/rWire)

def importDatSimMeas(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
    datStore = []
    for entry in tempDat[1:]:
        temp = map(float, entry.split(','))
        datStore.append(temp)

    return datStore

def analSingReSimMeas(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    reStore = []
    temp = []
##    Z0 = 60*sp.log(1.27*apPipe/r_wire)
    Z0=50.0
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(-2*Z0/lenTot*sp.log(10**(data[i,j]/20))) 
        temp.append(tempLin)
    return temp

def fileImport(tarFile):
    readPart=open(tarFile, 'r+')
    datUnPro = readPart.readlines()
    readPart.close()
    freqDat=[]
    qLoaded=[]
    s21DB=[]
    for row in datUnPro[20:]:
        freqDat.append(float(row.split(",")[0]))
        qLoaded.append(float(row.split(",")[1]))
        s21DB.append(float(row.split(",")[2]))

    return sp.array(freqDat), sp.array(qLoaded), sp.array(s21DB)

def skinDepth(freq, cond):
    return (1/(sp.pi*freq*mu0*cond))**0.5

def resImpGet(targetFile):

    freqList, qTotal, transCoeff = fileImport(targetFile)

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(c*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor

    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper)/attenCopper

    return freqList, zMeas/lDUT

def resImpGetArray(tarArr1,tarArr2,tarArr3,attenCableLen):

    freqList, qTotal, transCoeff = tarArr1, tarArr2, tarArr3

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(C*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCable = attenCableLen*freqList*10.0**-9
    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor

    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper-attenCable)/(attenCopper)

    return freqList, zMeas/lDUT

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def extract_dat2013(file_name):
    data = open(file_name, "r+")
    tar = data.readlines()
    temp=[]
    for row in tar[2:]:
        row = map(float, row.split())
        temp.append(row)
    data.close()

    return temp[:-1]

def extract_dat2013_MultipleSweeps(file_name):
    data = open(file_name, "r+")
    tar = data.readlines()
    temp=[]
    bigTemp = []
    count=2
    while count<len(tar):
        try:
            row = map(float, tar[count].split())
            temp.append(row)
            count+=1
        except:
            bigTemp.append(temp)
            temp=[]
            count+=3
    data.close()
    bigTemp.append(temp)
    return bigTemp

def sParaRead(fileTar, lenDUT, col):
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        phase = row[col+1]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360    

        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        import_dat = [row[0], row[col], sp.radians(phase), -s21Pec]
        data.append(import_dat)
        last = phase

    return data


def impAnalysisSingle(fileTar, lenDUT, r_wire, r_pipe, Zc):
    ###### Analyses a S2P file for measurement of longitudinal impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        phase = row[4]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360


        import_dat = [row[0], -2*Zc*sp.log(linDat), -2*Zc*(sp.radians(phase)+s21Pec)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDUT), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDUT))]
        data.append(import_dat)
        last = phase

    return data

def impAnalysisSingleCSVRealLin(fileTar, lenDUT, r_wire, r_pipe, Zc):
    ###### Analyses a S2P file for measurement of longitudinal impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    for row in inputData[5:]:
        row=map(float, row.split(","))
        linDat = row[1]
        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        import_dat = [row[0], -2*Zc*sp.log(linDat)]
        data.append(import_dat)

    return data

def impAnalysisTwo(fileTar, lenDut, r_wire, r_pipe, d_wire, Zc):
    ###### Analyses a S2P file for measurement of dipole impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter = 0.0
    last = 1e8

    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDut/C
        phase = row[4]-counter
        if phase>last and (phase-last)>180:
            count+=1
            counter+=360
            pgase-=360
        import_dat = [row[0], -2*Zc*sp.log(linDat)*C/(2*sp.pi*row[0]*d_wire**2), -2*Zc*(sp.radians(phase)+s21Pec)*C/(2*sp.pi*row[0]*d_wire**2)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDut)*C/(row[0]*d_wire**2), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDut))*C/(row[0]*d_wire**2)]
        data.append(import_dat)
    return data

def impTransPara(dispWire, measImp, wireErr):
##### Takes an array of longitudinal measurements and returns the transverse impedance
    quadFunc = lambda p, x: p[0] + x * p[1] + x**2 * p[2]
    errFunc = lambda p, x, Z: (Z-quadFunc(p,x))

    totTrans = []
    longComp = []

    for i in range(0,len(measImp[0,:,1])):
        pinit = [1.0, -1.0, 1.0]
        ZerrRe = measImp[:,i,1]*wireErr
        pReal, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,1]), full_output = 0)
        pinit = [1.0, -1.0, 1.0]
        ZerrIm = measImp[:,i,2]*wireErr
        pImag, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,2]), full_output = 0)
        totTrans.append([pReal[2]*C/(2*sp.pi*measImp[0,i,0]), pImag[2]*C/(2*sp.pi*measImp[0,i,0])])
        longComp.append([pReal[0], pImag[0]])

    return longComp, totTrans

def quadImp(totalTrans, dipTrans, axis):
######## Returns the quadrupolar impedance from total transverse and dipolar data
    quadImp = []
    for i in range(0,len(totalTrans)):
        if axis == "horz":
            quadImp.append([totalTrans[i,1]+dipTrans[i,1],totalTrans[i,2]+dipTrans[i,2]])
        elif axis == "vert":
            quadImp.append([totalTrans[i,1]-dipTrans[i,1],totalTrans[i,2]-dipTrans[i,2]])
    return quadImp


def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def peakFitSimulations(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 50
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 50 and y_val[current_max_x]>2:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [0.2, 10.0, 10.0]
        lower_bound = res[0]-50
        upper_bound = res[0]+50
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def peakFitMeasurements(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 0
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 2 and y_val[current_max_x]>3:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [res[1], 4.0, data[res[0],1]]
        lower_bound = res[0]-4
        upper_bound = res[0]+3
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2)

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2
                                                    
def logToLin(data):
    return 10**(data/20)

def linToLog(data):
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

def overLapFreq(overlap, fringeLen, harmonic):
    return harmonic*C/(2*10**0.5*(overlap+fringeLen))

def heatingValGauss(impArr, beamCur, bunSpac, bunLen):
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac/10**9,impArr[-1,0],impArr[-1,0]*10**9*bunSpac)
    splineFitHeatingImp = SplineFitImp(freqListHeating)
    heatingTotalPart = []
    for i in range(0,len(splineFitHeatingImp)):
        heatingTotalPart.append([freqListHeating[i], abs(2*splineFitHeatingImp[i]*(beamCur**2)*gaussProf(freqListHeating[i]*10.0**9, bunLen))])
    heatingTotalPart=sp.array(heatingTotalPart)
    return heatingTotalPart[:,1].sum(), heatingTotalPart

def splineFitImpFunc(impArr):
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListTemp = sp.linspace(10**6/10**9,impArr[-1,0],10000)
    splineFitImpFreq=SplineFitImp(freqListTemp)
    temp = []
    for i in range(0,len(freqListTemp)):
        temp.append([freqListTemp[i], splineFitImpFreq[i]])
    return sp.array(temp)

def heatingLossFactorGauss(impSplineFit, bunLength, bunPop):
    convBeamSpecImp = abs(impSplineFit[:,1])*(gaussProf(impSplineFit[:,0]*10**9, bunLength)**2)
    return integrate.simps(fftPack.ifft(convBeamSpecImp), impSplineFit[:,0]*10**9)*2*sp.pi*(1.6*10**-19)**2*bunPop, convBeamSpecImp*2*sp.pi*(1.6*10**-19)**2*bunPop


def resAnalVNARawData(dataArray):
    widths=sp.arange(3,10)
    peakList=signal.find_peaks_cwt(dataArray[:,3],widths)

    fitFunc = lambda p, x: (p[0]/(1+complex(0,1)*p[1]*(x/p[2] - p[2]/x))).real
    errFunc = lambda p, x, y, err: (y-fitFunc(p, x))

    fitNo = 5
    testList=sp.linspace(1,10,10)

    peakDataStash=[]
    for peak in peakList:
        pInit = [logToLin(dataArray[peak,3]),100,dataArray[peak,0]/10**9]
        freqVal = dataArray[peak-fitNo:peak+fitNo,0]/10**9
        yErr = logToLin(dataArray[peak-fitNo:peak+fitNo,3])
        temp = yErr
        out=op.leastsq(errFunc, pInit, args=(freqVal,temp,yErr), full_output=1)
        pFinal = out[0]
    ##    print pFinal
        peakDataStash.append([logToLin(dataArray[peak,3]), pFinal[1], dataArray[peak,0]/10**9])

    peakDataStash=sp.array(peakDataStash)
    check=0
    while check<len(peakDataStash):
        if peakDataStash[check,1]<100:
            peakDataStash=sp.vstack([peakDataStash[:check],peakDataStash[check+1:]])
        else:
            check+=1

    return peakDataStash

length = 2.88
f_rev = C/27000.0
nBunches = 2808
qPart = 1.6*10.0**-19
nPart=1.15*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=1.001*10.0**-9
testImp = [250*10**6, 10.0, 10000]
bunchCur=nPart*qPart*f_rev
print bCur, bunchCur

######Import a selection of wire measurements ########

measurementsStriplineUnloadedFreq, measurementsStriplineUnloadedImp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/CLICStripline/carolina/13_12_13/resonator_noloads"))
measurementsStripline2LoadsFreq, measurementsStripline2LoadsImp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/CLICStripline/carolina/13_12_13/resonator_2loads_feedthrough2"))

measurementsStriplineResFreq, measurementsStriplineResImp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/CLICStripline/carolina/resonant_method_files/resonator"))
measurementsStriplineResMatchedResistorsFreq, measurementsStriplineResMatchedResistorsImp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/CLICStripline/carolina/resonant_method_files/resonator_matchedresistos"))

matchedStriplineMeasDat = sp.array(impAnalysisSingleCSVRealLin("E:/PhD/1st_Year_09-10/Data/CLICStripline/carolina/13_12_13/S12_LONGITUDINALBEAM_200ohmsresistance.CSV", 1.2, 0.0005, 0.02, 270))

targetDirProbeMeas = "E:/PhD/1st_Year_09-10/Data/CLICStripline/carolina/resonant_method_files/"
temp=[]
listOfFiles = ["RESLONG"+str(i)+"-"+str(i+200)+"MHZ.S2P" for i in range(0,400,200)]
for inputFile in listOfFiles:
    tar=open(targetDirProbeMeas+inputFile)
    inputData=tar.readlines()
    tar.close()
    for line in inputData[6:]:
        bit=line.split("\t")
        temp.append(map(float,bit))
              
transmissionDataShort=sp.array(temp)
impConvStriplineShortCables = resAnalVNARawData(transmissionDataShort)
impResultsShortFreq, impResultsShortImp = resImpGetArray(impConvStriplineShortCables[:,2]*10**9,impConvStriplineShortCables[:,1], linToLog(impConvStriplineShortCables[:,0]), 0.0)

pl.semilogy()
pl.plot(measurementsStriplineUnloadedFreq/10**9, measurementsStriplineUnloadedImp, label="Res Unloaded")
pl.plot(measurementsStripline2LoadsFreq/10**9, measurementsStripline2LoadsImp, label="Res 2 Loads")
pl.plot(matchedStriplineMeasDat[:,0]/10**9, matchedStriplineMeasDat[:,1]-matchedStriplineMeasDat[0,1])
##pl.plot(impResultsShortFreq/10**9, impResultsShortImp)
pl.plot(measurementsStriplineResFreq/10**9, measurementsStriplineResImp, label="Res")
pl.plot(measurementsStriplineResMatchedResistorsFreq/10**9, measurementsStriplineResMatchedResistorsImp, label="Res MatchedResistors")
pl.legend(loc="upper left")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$\Re{}e(Z_{\parallel})$ ($\Omega$/m)", fontsize=16.0)
pl.show()
pl.clf()
