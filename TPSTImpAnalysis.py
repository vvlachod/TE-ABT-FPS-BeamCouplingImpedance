import csv, time, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
from scipy import signal as sg

####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

c=299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005
lWire = 3.551
lDUT = 2.45
resCopper = 17.24*10.0**-9
resOuter = 1/(3.3*10.0**7)
ZChar = 60*sp.log(dAper/rWire)

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def eigenmodeGen(freqFile, qFile, rOverQFile):
    inputFreq=open(freqFile, "r+")
    datFreq=inputFreq.readlines()
    inputFreq.close()
    outputFreq=[]
    for line in datFreq[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputFreq.append(float(temp))
    inputQ=open(qFile, "r+")
    datQ=inputQ.readlines()
    inputQ.close()
    outputQ=[]
    for line in datQ[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputQ.append(float(temp))
    inputrOverQ=open(rOverQFile, "r+")
    datrOverQ=inputrOverQ.readlines()
    inputrOverQ.close()
    outputrOverQ=[]
    for line in datrOverQ[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputrOverQ.append(float(temp))
    resDat=[]
    for i in range(0,len(outputFreq)):
        resDat.append([outputFreq[i], outputQ[i], outputrOverQ[i]])

    return resDat

def Z_bb(freq, data):
    return data[2]*data[1]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2*(2**0.5))

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2

def readS21FromVNA(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[6:]:
        temp=line.split("\t")
        outputData.append(map(float,temp))
    return outputData

def logToLin(data):
    return 10**(data/20)

def linToLog(data):
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

def logImpFormulaImg(dataDUT, lenRef, Zc, freqDat):
    adjDat = []
    last=0
    lastCount = 0
    correction = 0
    limitMin=-160
    limitMax=160
    for i in range(0,len(dataDUT)):
        if last < limitMin and dataDUT[i]>limitMax and i-lastCount > 20:
            correction+=360
            adjDat.append(dataDUT[i]-correction)
            last=dataDUT[i]
            lastCount=i
        else:
            adjDat.append(dataDUT[i]-correction)
            last=dataDUT[i]
    adjDat=sp.array(adjDat)
    adjDat = Zc*((adjDat/360)*(2*sp.pi)+(2*sp.pi*freqDat/(C*lenRef)))
##    adjDat = ((adjDat/360)*(2*sp.pi)+(2*sp.pi*freqDat/(C*lenRef)))
    return adjDat

def fileImport(tarFile):
    readPart=open(tarFile, 'r+')
    datUnPro = readPart.readlines()
    readPart.close()
    freqDat=[]
    qLoaded=[]
    s21DB=[]
    for row in datUnPro[20:]:
        freqDat.append(float(row.split(",")[0]))
        qLoaded.append(float(row.split(",")[1]))
        s21DB.append(float(row.split(",")[2]))

    return sp.array(freqDat), sp.array(qLoaded), sp.array(s21DB)

def skinDepth(freq, cond):
    return (1/(sp.pi*freq*mu0*cond))**0.5

def resImpGetFromFile(targetFile):

    freqList, qTotal, transCoeff = fileImport(targetFile)

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(c*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor


    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper)/attenCopper

    return freqList, zMeas/lDUT

fileNoGatingTPSTBeforeMod = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/12-02-15/TPST/S21FREQNOMATCHINGNOGATING.S2P"
fileGatingTPSTBeforeMod = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/12-02-15/TPST/S21FREQNOMATCHINGGATING.S2P"

fileNoGatingTPSTAfterMod = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/03-03-15/TPSTNewDesign/S21FREQNOGATING.S2P"
fileMatchedTPSTAfterMod = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/03-03-15/TPSTNewDesign/S21FREQMATCHINGNOGATING.S2P"


datNoGatingBeforeMod=sp.array(readS21FromVNA(fileNoGatingTPSTBeforeMod))
ImpNoGatingBeforeMod=logImpFormula(logToLin(datNoGatingBeforeMod[:,3]),1,695)

datGatingBeforeMod=sp.array(readS21FromVNA(fileGatingTPSTBeforeMod))
ImpGatingBeforeMod=logImpFormula(logToLin(datGatingBeforeMod[:,3]),1,695)

datNoGatingAfterMod=sp.array(readS21FromVNA(fileNoGatingTPSTAfterMod))
ImpNoGatingAfterMod=logImpFormula(logToLin(datNoGatingAfterMod[:,3]),1,395)

datMatchedAfterMod=sp.array(readS21FromVNA(fileMatchedTPSTAfterMod))
ImpMatchedAfterMod=logImpFormula(logToLin(datMatchedAfterMod[:,3]),1,395)

widths=sp.arange(1,5)
##peaksListNoGrill = sg.find_peaks_cwt(allConnectionsImpNoGrill2GHz-allConnectionsImpNoGrill2GHz[0], widths)
##peaksListGrill = sg.find_peaks_cwt(allConnectionsAddedImpGrill2GHz-allConnectionsAddedImpGrill2GHz[0], widths)
##print len(MatchedMeas2Pump1ZS[:,0]), len(impMatchedMeas2Pump1ZSImg)

freqResImpTPST, impResImpTPST = resImpGetFromFile("E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/12-02-15/TPST/resonator")

##pl.semilogy()
pl.plot(datNoGatingBeforeMod[:,0]/10**9, ImpNoGatingBeforeMod-ImpNoGatingBeforeMod[0], label="Before Mod, No Matching, No Gating")
##pl.plot(datGatingBeforeMod[:,0]/10**9, ImpGatingBeforeMod-ImpGatingBeforeMod[0], label="Before Mod, No Matching, Gating")
##pl.plot(datNoGatingAfterMod[:,0]/10**9, ImpNoGatingAfterMod-ImpNoGatingAfterMod[0], label="After Mod, No Matching")
pl.plot(datMatchedAfterMod[:,0]/10**9, ImpMatchedAfterMod-ImpMatchedAfterMod[0], label="After Mod, Matched")
##pl.plot(freqResImpTPST/10**9, impResImpTPST, "kx")
pl.legend(loc="upper left")
##pl.axis([0,1,0,1400])
pl.xlim(0,2.0)
pl.ylim(-400,2500)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$\Re{}e(Z_{\parallel}$) ($\Omega$)", fontsize=16.0)
pl.show()
pl.clf()
