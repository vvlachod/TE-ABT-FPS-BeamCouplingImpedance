import csv, time, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
from scipy import signal

####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def eigenmodeGen(freqFile, qFile, rOverQFile):
    inputFreq=open(freqFile, "r+")
    datFreq=inputFreq.readlines()
    inputFreq.close()
    outputFreq=[]
    for line in datFreq[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputFreq.append(float(temp))
    inputQ=open(qFile, "r+")
    datQ=inputQ.readlines()
    inputQ.close()
    outputQ=[]
    for line in datQ[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputQ.append(float(temp))
    inputrOverQ=open(rOverQFile, "r+")
    datrOverQ=inputrOverQ.readlines()
    inputrOverQ.close()
    outputrOverQ=[]
    for line in datrOverQ[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputrOverQ.append(float(temp))
    resDat=[]
    for i in range(0,len(outputFreq)):
        resDat.append([outputFreq[i], outputQ[i], outputrOverQ[i]])

    return resDat

def Z_bb(freq, data):
    return data[2]*data[1]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2*(2**0.5))

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2


def readS21FromVNA(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[6:]:
        temp=line.split("\t")
        outputData.append(map(float,temp))
    return outputData

def logToLin(data):
    return 10**(data/20)

def linToLog(data):
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

Zc=307 ### From reflection measurements

dirMeasDat = "E:/PhD/1st_Year_09-10/Data/mkdv/mkdvWithMatching/"

###### Warm squishy test bit. Now for the main show #######

withAttenFile="S21WITHMATCHINGWITHATTENUATOR.S2P"
withAttenLowFreqFile="S21WITHMATCHINGWITHATTENUATOR1-100MHZ.S2P"
withoutAttenFile="S21WITHMATCHINGNOATTENUATOR.S2P"
withAttenDat=sp.array(readS21FromVNA(dirMeasDat+withAttenFile))
withAttenLowFreqDat=sp.array(readS21FromVNA(dirMeasDat+withAttenLowFreqFile))
withoutAttenDat=sp.array(readS21FromVNA(dirMeasDat+withoutAttenFile))

withAttenImp = logImpFormula(logToLin(withAttenDat[:,3]), 1, Zc)
withAttenLowFreqImp = logImpFormula(logToLin(withAttenLowFreqDat[:,3]), 1, Zc)
withoutAttenImp = logImpFormula(logToLin(withoutAttenDat[:,3]), 1, Zc)

pl.plot(withAttenDat[:,0]/10**9, withAttenImp-withAttenImp[0], label="$\Re{}e(Z_{\parallel})$ with Attenuators")
pl.plot(withAttenLowFreqDat[:,0]/10**9, withAttenLowFreqImp-withAttenLowFreqImp[0], label="$\Re{}e(Z_{\parallel})$ with Attenuators")
##pl.plot(withoutAttenDat[:,0]/10**9, withoutAttenImp, label="$\Re{}e(Z_{\parallel})$ without Attenuators")
##pl.plot(testDat[:,0], testDat[:,3])
##pl.xlim(0,0.05)
##pl.ylim(0,200)
##pl.show()
pl.clf()

###### Analysis of displaced data ######

xDisplacements=[38,43,48,53,56]
yDisplacements=[32.5,37.5,42.5,47.5,52.5]
fileName="S21WITHMATCHINGWITHATTENUATOR.S2P"
filePathsX = [dirMeasDat+"displacedX/"+str(i)+"mm/"+fileName for i in xDisplacements]
filePathsY = [dirMeasDat+"displacedY/"+str(i)+"mm/"+fileName for i in yDisplacements]
listOfMeasurementsX = sp.array([])
listOfMeasurementsY = sp.array([])
for entry in filePathsX:
    if len(listOfMeasurementsX)==0:
        listOfMeasurementsX=logImpFormula(logToLin(sp.array(readS21FromVNA(entry))[:,3]),1,Zc)
    else:
        listOfMeasurementsX=sp.vstack((listOfMeasurementsX,logImpFormula(logToLin(sp.array(readS21FromVNA(entry))[:,3]),1,Zc)))
for entry in filePathsY:
    if len(listOfMeasurementsY)==0:
        listOfMeasurementsY=logImpFormula(logToLin(sp.array(readS21FromVNA(entry))[:,3]),1,Zc)
    else:
        listOfMeasurementsY=sp.vstack((listOfMeasurementsY,logImpFormula(logToLin(sp.array(readS21FromVNA(entry))[:,3]),1,Zc)))


##for i in range(0,len(listOfMeasurementsX)):
##    pl.plot(withAttenDat[:,0]/10**9, listOfMeasurementsX[i,:])

for i in range(0,len(listOfMeasurementsY)):
    pl.plot(withAttenDat[:,0]/10**9, listOfMeasurementsY[i,:])
    
##pl.show()
pl.clf()

transConstImpX=[]
transLongImpX=[]
transTotImpX=[]
transConstImpY=[]
transLongImpY=[]
transTotImpY=[]

fitFunc = lambda p, x: (p[0]-x)**2*p[1] + (p[2]-x)*p[3] + p[4]
errFunc = lambda p, x, y, err: (y-fitFunc(p, x))
                                                     
for i in range(0,len(listOfMeasurementsX[0,:])):
    xPlot=sp.linspace(0.035,0.060,1000)
    xFit=sp.array(xDisplacements)*10**-3
    longImpVals = listOfMeasurementsX[:,i]
    pinit = [0.0,1.0,1.0,1.0,1.0]
    yErr = longImpVals
    print longImpVals.shape, xFit.shape
    out=op.leastsq(errFunc, pinit, args=(xFit, longImpVals,yErr), full_output=1)
    pFinalReal = out[0]
    transConstImpX.append(pFinalReal[3])
    transLongImpX.append(pFinalReal[4])
    transTotImpX.append(pFinalReal[1]*C/(2*sp.pi*withAttenDat[i,0]))

transConstImpX=sp.array(transConstImpX)
transTotImpX=sp.array(transTotImpX)
transLongImpX=sp.array(transLongImpX)

pl.plot(withAttenDat[:,0]/10**9, withAttenImp-withAttenImp[0], label="$\Re{}e(Z_{\parallel})$ with Attenuators")
pl.plot(withAttenDat[:,0]/10**9, transLongImpX)
##pl.show()
pl.clf()

##pl.semilogy()
pl.plot(withAttenDat[:,0]/10**9, transConstImpX, 'b-')
pl.plot(withAttenDat[:,0]/10**9, transTotImpX, 'k-')
pl.axis([0,1,-200,200])
##pl.show()
pl.clf()

