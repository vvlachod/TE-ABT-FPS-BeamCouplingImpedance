import scipy as sp
import pylab as pl
import time, csv, sys, os
import matplotlib.pyplot as plt
import scipy.integrate as inte

########################## Imported Profile ##################################

start = time.time()

#######   SPS parameters for LHC type beam  #######

###### Define constants ####
charge = 18.4*10**-9
sigma_z = 0.075
C = 299792458.0
eps0 = 8.854 * 10 **-12
mu0 = 4*sp.pi*10**-7
Z0 = 120*sp.pi
circ=6911.0
t_bunch = 25.0*10**-9
n_bunch = 288
Nb = 1.15*10**11
q_part = 1.6*10**-19

def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def gauss_prof(dist, sigma, order):
    return sp.e**(-order*(dist/(2*sigma))**2)

def para_prof(dist, bunch_length):
    return 1-(2*dist/bunch_length)**2

def cos_prof(dist, bunch_length):
    return sp.cos(sp.pi/bunch_length*dist)**2

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp


########### Import and produce data for frequency domain ###########


top_directory_in = "E:/PhD/1st_Year_09-10/Data/ferr_cav/eigenmode_results/simple_cavity/"
##top_directory_out = "C:/Users/hugo/PhD/Data/UA9-Goniometer/eigenmode-results/parked_out_new/"

power_loss_tot = 0.0
eigenmode_store_ferr = [] ###### first column - freq, second - Q, Third R, Fourth - Power Loss
directory_list = [top_directory_in+i+"/" for i in os.listdir(top_directory_in)]
##print directory_list
count = 0
try:
    os.mkdir(top_directory_in+"tex_output/")
    os.mkdir(top_directory_out+"tex_output/")

except:
    pass


for work_directory in directory_list:

    if work_directory == top_directory_in+"tex_output/":
        pass
    else:
##        print work_directory
        file_input = open(work_directory+"e_field_on_axis.fld", 'r+')
        data = file_input.readlines()
        Ez = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ez.append([temp[2],complex(temp[-2],temp[-1])])

        temp = []
        for i in range(0,len(Ez)):
            temp.append([Ez[i][0],complex((Ez[i][1]).real,(Ez[i][1]).imag)])
        Ez = sp.array(temp)

##        print Ez

        potential = 0.0
        potential_alt = 0.0
##        print work_directory
        gap = (Ez[1][0]-Ez[0][0])
        for i in range(0, len(Ez)-1):
            potential_alt += 0.5*abs(Ez[i+1,1]+Ez[i,1])*gap
        potential = inte.simps(abs(Ez[:,1]), Ez[:,0], gap)
        if count == 0:
            pl.plot(Ez[:,0], Ez[:,1])
##            pl.show()
            pl.clf()
            count+=1

        file_input = open(work_directory+"q_1.csv", 'r+')
        data = file_input.readlines()
        Q = []
        for i in data[1:]:
            Q.append(map(float, i.rsplit(',')))

        Q = sp.array(Q)
        ##print Q

        file_input = open(work_directory+"stored_energy.csv", 'r+')
        data = file_input.readlines()
        stored_energy = []
        for i in data[1:]:
            stored_energy.append(map(float, i.rsplit(',')))

        ##print stored_energy

        file_input = open(work_directory+"freq.csv", 'r+')
        data = file_input.readlines()
        freq = []
        for i in data[1:]:
            temp = map(float, i.rsplit(','))
            freq.append([temp[0],complex(temp[2],temp[1])])

        freq = sp.array(freq)

        gap = Ez[1,0]-Ez[0,0]
        confluence = []
        for i in range(0,len(Ez)):
            confluence.append(Ez[i,1]*complex(sp.cos(freq[0,1]*2*sp.pi*Ez[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ez[i,0]/C)))

        potential = abs(inte.simps(confluence, Ez[:,0], gap))
##        print potential*60*10**-3/(3*10**8)
        print potential, potential*C/(2*sp.pi*freq[0,1])
##        print potential*(2*sp.pi*freq[0,1]*(Ez[-1,0]-Ez[0,0]))/C
        long_r_over_q = (abs(potential)**2/(2*sp.pi*freq[0,1]*stored_energy[0][1]))
        
        long_r_over_q_alt = abs(inte.simps(confluence, Ez[:,0], gap))**2/(2*2*sp.pi*freq[0,1]*stored_energy[0][1])
        print freq[0,1], Q[0,1], long_r_over_q_alt,long_r_over_q_alt*Q[0,1], (Nb*q_part*n_bunch*C/circ)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2) 
        power_loss_tot += (Nb*q_part*n_bunch*C/circ)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2)


######### Plotting comparison time and frequency domain ##########

##for i in eigenmode_store:
##    print i

freq_list = []
for i in range(0,20000,1):
        freq_list.append(float(i/10.0*10.0**6))

freq_list = list(set(freq_list))
freq_list.sort()

impedance_profile_ferr = []
for i in freq_list:
    total = 0.0
    for entry in eigenmode_store_ferr:
        total+=Z_bb(i, entry)
    impedance_profile_ferr.append([total.real, total.imag])
impedance_profile_ferr=sp.array(impedance_profile_ferr)
impedance_profile_no_ferr = []
for i in freq_list:
    total = 0.0
    for entry in eigenmode_store_no_ferr:
        total+=Z_bb(i, entry)
    impedance_profile_no_ferr.append([total.real, total.imag])
impedance_profile_no_ferr=sp.array(impedance_profile_no_ferr)
freq_list = sp.array(freq_list)

pl.semilogy()
pl.plot(freq_list/10**9, abs(impedance_profile_ferr[:,0]), 'k-', label="$\Re{}e(Z)$ Freq Domain, In, Bellows, Ferrite")
##pl.plot(freq_list/10**9, impedance_profile_ferr[:,1], 'r-', label="$\Im{}m(Z)$ Freq Domain, In, Bellows, Ferrite")
pl.plot(freq_list/10**9, abs(impedance_profile_no_ferr[:,0]), 'k--', label="$\Re{}e(Z)$ Freq Domain, In, Bellows, No Ferrite")
##pl.plot(freq_list/10**9, impedance_profile_no_ferr[:,1], 'r-', label="$\Im{}m(Z)$ Freq Domain, In, Bellows, No Ferrite")
##pl.plot(data_in_ua9_long_ferr[:,0], abs(data_in_ua9_long_ferr[:, 1]), 'r-', label= "$\Re{}e(Z)$ Time Domain, In, Bellows, Ferrite")
##pl.plot(data_in_ua9_long_ferr[:,0], data_in_ua9_long_ferr[:, 3], 'r--', label= "$\Im{}m(Z)$ Time Domain, In, Bellows, Ferrite")
##pl.plot(data_in_ua9_long_no_ferr[:,0], abs(data_in_ua9_long_no_ferr[:, 1]), 'r--', label= "$\Re{}e(Z)$ Time Domain, In, Bellows, No Ferrite")
##pl.plot(data_in_ua9_long_no_ferr[:,0], data_in_ua9_long_no_ferr[:, 3], 'r--', label= "$\Im{}m(Z)$ Time Domain, In, Bellows, No Ferrite")
pl.xlabel("Frequency (GHz)", fontsize=16)
pl.ylabel("Impedance ($\Omega$)", fontsize=16)
pl.legend(loc="lower right")
pl.axis([0, 1.2, 10**-4, 10**5])
pl.savefig(top_directory_in+"tex_output/long_imp_real_off_axis.pdf")
pl.savefig(top_directory_in+"tex_output/long_imp_real_off_axis.eps")
##pl.savefig(top_directory_in+"tex_output/long_imp_real_off_axis_no_ferr.pdf")
##pl.savefig(top_directory_in+"tex_output/long_imp_real_off_axis_no_ferr.eps")
##pl.savefig(top_directory_in+"tex_output/long_imp_real_off_axis_ferr.pdf")
##pl.savefig(top_directory_in+"tex_output/long_imp_real_off_axis_ferr.eps")
##pl.show()
pl.clf()
