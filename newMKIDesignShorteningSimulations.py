import csv, time, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack

####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

######### Define importing, analysis functions #########

c=299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005
lWire = 3.551
lDUT = 2.45
resCopper = 17.24*10.0**-9
resOuter = 1/(3.3*10.0**7)
ZChar = 60*sp.log(dAper/rWire)

def importDatSimMeas(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
    datStore = []
    for entry in tempDat[1:]:
        temp = map(float, entry.split(','))
        datStore.append(temp)

    return datStore

def analSingReSimMeas(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    reStore = []
    temp = []
##    Z0 = 60*sp.log(1.27*apPipe/r_wire)
    Z0=50.0
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(-2*Z0/lenTot*sp.log(10**(data[i,j]/20))) 
        temp.append(tempLin)
    return temp

def fileImport(tarFile):
    readPart=open(tarFile, 'r+')
    datUnPro = readPart.readlines()
    readPart.close()
    freqDat=[]
    qLoaded=[]
    s21DB=[]
    for row in datUnPro[20:]:
        freqDat.append(float(row.split(",")[0]))
        qLoaded.append(float(row.split(",")[1]))
        s21DB.append(float(row.split(",")[2]))

    return sp.array(freqDat), sp.array(qLoaded), sp.array(s21DB)

def skinDepth(freq, cond):
    return (1/(sp.pi*freq*mu0*cond))**0.5

def resImpGet(targetFile):

    freqList, qTotal, transCoeff = fileImport(targetFile)

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(c*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor

    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper)/attenCopper

    return freqList, zMeas/lDUT

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def extract_dat2013(file_name):
    data = open(file_name, "r+")
    tar = data.readlines()
    temp=[]
    for row in tar[2:]:
        row = map(float, row.split())
        temp.append(row)
    data.close()

    return temp

def sParaRead(fileTar, lenDUT, col):
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        phase = row[col+1]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360    

        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        import_dat = [row[0], row[col], sp.radians(phase), -s21Pec]
        data.append(import_dat)
        last = phase

    return data


def impAnalysisSingle(fileTar, lenDUT, r_wire, r_pipe, Zc):
    ###### Analyses a S2P file for measurement of longitudinal impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        phase = row[4]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360


        import_dat = [row[0], -2*Zc*sp.log(linDat), -2*Zc*(sp.radians(phase)+s21Pec)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDUT), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDUT))]
        data.append(import_dat)
        last = phase

    return data

def impAnalysisTwo(fileTar, lenDut, r_wire, r_pipe, d_wire, Zc):
    ###### Analyses a S2P file for measurement of dipole impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter = 0.0
    last = 1e8

    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDut/C
        phase = row[4]-counter
        if phase>last and (phase-last)>180:
            count+=1
            counter+=360
            pgase-=360
        import_dat = [row[0], -2*Zc*sp.log(linDat)*C/(2*sp.pi*row[0]*d_wire**2), -2*Zc*(sp.radians(phase)+s21Pec)*C/(2*sp.pi*row[0]*d_wire**2)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDut)*C/(row[0]*d_wire**2), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDut))*C/(row[0]*d_wire**2)]
        data.append(import_dat)
    return data

def impTransPara(dispWire, measImp, wireErr):
##### Takes an array of longitudinal measurements and returns the transverse impedance
    quadFunc = lambda p, x: p[0] + x * p[1] + x**2 * p[2]
    errFunc = lambda p, x, Z: (Z-quadFunc(p,x))

    totTrans = []
    longComp = []

    for i in range(0,len(measImp[0,:,1])):
        pinit = [1.0, -1.0, 1.0]
        ZerrRe = measImp[:,i,1]*wireErr
        pReal, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,1]), full_output = 0)
        pinit = [1.0, -1.0, 1.0]
        ZerrIm = measImp[:,i,2]*wireErr
        pImag, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,2]), full_output = 0)
        totTrans.append([pReal[2]*C/(2*sp.pi*measImp[0,i,0]), pImag[2]*C/(2*sp.pi*measImp[0,i,0])])
        longComp.append([pReal[0], pImag[0]])

    return longComp, totTrans

def quadImp(totalTrans, dipTrans, axis):
######## Returns the quadrupolar impedance from total transverse and dipolar data
    quadImp = []
    for i in range(0,len(totalTrans)):
        if axis == "horz":
            quadImp.append([totalTrans[i,1]+dipTrans[i,1],totalTrans[i,2]+dipTrans[i,2]])
        elif axis == "vert":
            quadImp.append([totalTrans[i,1]-dipTrans[i,1],totalTrans[i,2]-dipTrans[i,2]])
    return quadImp


def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def peakFitSimulations(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 50
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 50 and y_val[current_max_x]>2:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [0.2, 10.0, 10.0]
        lower_bound = res[0]-50
        upper_bound = res[0]+50
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def peakFitMeasurements(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 0
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 2 and y_val[current_max_x]>3:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [res[1], 4.0, data[res[0],1]]
        lower_bound = res[0]-4
        upper_bound = res[0]+3
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2)

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2
                                                    
def logToLin(data):
    return 10**(data/20)

def linToLog(data):
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

def overLapFreq(overlap, fringeLen, harmonic):
    return harmonic*C/(2*10**0.5*(overlap+fringeLen))

def heatingValGauss(impArr, beamCur, bunSpac, bunLen):
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac/10**9,impArr[-1,0],impArr[-1,0]*10**9*bunSpac)
    splineFitHeatingImp = SplineFitImp(freqListHeating)
    heatingTotalPart = []
    for i in range(0,len(splineFitHeatingImp)):
        heatingTotalPart.append([freqListHeating[i], abs(2*splineFitHeatingImp[i]*(beamCur**2)*gaussProf(freqListHeating[i]*10.0**9, bunLen))])
    heatingTotalPart=sp.array(heatingTotalPart)
    return heatingTotalPart[:,1].sum(), heatingTotalPart

def splineFitImpFunc(impArr):
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListTemp = sp.linspace(10**6/10**9,impArr[-1,0],10000)
    splineFitImpFreq=SplineFitImp(freqListTemp)
    temp = []
    for i in range(0,len(freqListTemp)):
        temp.append([freqListTemp[i], splineFitImpFreq[i]])
    return sp.array(temp)


length = 2.88
f_rev = C/27000.0
nBunches = 2808
qPart = 1.6*10.0**-19
nPart=1.15*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=1.001*10.0**-9
testImp = [250*10**6, 10.0, 10000]
bunchCur=nPart*qPart*f_rev
print bCur, bunchCur

directory = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/shortened_end_piece_impedance/"#Directory of data
##listDir = [directory+i for i in os.listdir(directory)]
listDir = []
listDir.append(directory+"100mm-overlap-24-full-length")
listDir.append(directory+"100mm-overlap-24-full-length-AltDesign")
listDir.append(directory+"100mm-overlap-24-full-length-enclosed")
listDir.append(directory+"100mm-overlap-56mm-pipe_alt_20mm")
listDir.append(directory+"100mm-overlap-24-full-length-tapered")
listDir.append(directory+"100mm-overlap-24-full-length-tapered-alternating")
listDir.append(directory+"100mm-overlap-24-full-length-step-out-final")
listDir.append(directory+"109mm_overlap")
##listDir.append(directory+"largeInDiamFerr")
##listDir.append(directory+"largeInDiamFerr")
##listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/with-15-screen-conductors")
##listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/19_conductors_alternating")
##listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/ferriteRings4A4NoYokeLongWakeLength/")
##listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/ferriteRings4B3NoYokeLongWakeLength/")
##listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/ferriteRings4M2NoYokeLongWakeLength/")
##listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/ferriteRingsVacNoYokeLongWakeLength/")
##listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/simulatingFerrAsVac/")
listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/90DegreeRotatedScreen15Conductors/")
fileNam = "/longitudinal-impedance.csv"

plotList=[]

for tarFile in listDir:
    tmp = sp.array(extract_dat(tarFile+fileNam))
    plotList.append([tmp[:,0],tmp[:,1],tmp[:,3]])

plotList = sp.array(plotList)

measurements_15 = sp.array(extract_dat("E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/15-strips-measurements.csv"))
measurements_19 = sp.array(extract_dat("E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/mki_measurements_mbarnes\MKI5_measurement_data-_4_6_2012/CentralTube-impedance-results.csv"))
measurementsMKI061213Freq, measurementsMKI061213Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/06-12-13/mkiTank6-MC10/resonator"))
SplineFitMKI061213 = interp.InterpolatedUnivariateSpline(measurementsMKI061213Freq[:]/10**6, measurementsMKI061213Imp[:])

freqList = sp.linspace(1,2000,2000)

freqListHeating = sp.linspace(40,2000,2000/40)

splineFitMKI061213 = SplineFitMKI061213(freqList)
splineFitHeatingMKI061213 = SplineFitMKI061213(freqListHeating)
heatingFreqMKI061213=[]
heatingTotalCosMKI061213=0.0
heatingTotalGaussMKI061213=0.0

for i in range(0,len(splineFitHeatingMKI061213)):
    heatingTotalGaussMKI061213+=2*splineFitHeatingMKI061213[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosMKI061213+=2*splineFitHeatingMKI061213[i]*(bCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKI061213.append(2*splineFitHeatingMKI061213[i]*(bCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))

print "MKI10-T06-HC13 "+str(heatingTotalCosMKI061213), str(heatingTotalGaussMKI061213)

heatingFreqMKI061213=sp.array(heatingFreqMKI061213)

summedFreqMKI061213= []

for i in range(0,len(heatingFreqMKI061213)):
    summedFreqMKI061213.append(sp.sum(heatingFreqMKI061213[:i]))

summedFreqMKI061213=sp.array(summedFreqMKI061213)

dirMatched = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/10-12-13/"
datFilesForWireMeas = ["S21WITHATTENUATORS0-200MHZ.S2P","S21WITHATTENUATORS200-400MHZ.S2P","S21WITHATTENUATORS400-600MHZ.S2P",
                       "S21WITHATTENUATORS600-800MHZ.S2P", "S21WITHATTENUATORS800-1000MHZ.S2P", "S21WITHATTENUATORS1000-1200MHZ.S2P",
                       "S21WITHATTENUATORS1200-1400MHZ.S2P", "S21WITHATTENUATORS1400-1600MHZ.S2P","S21WITHATTENUATORS1600-1800MHZ.S2P",
                       "S21WITHATTENUATORS1800-2000MHZ.S2P"]

temp=[]
test="E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/10-12-13/S21WITHATTENUATORS0-200MHZ.S2P"

testFile=open(test, "r+")

for inputFile in datFilesForWireMeas:
    tar=open(dirMatched+inputFile,"r+")
    inputData=tar.readlines()
    for line in inputData[6:]:
        bit=line.split("\t")
        temp.append(map(float,bit))

transDataRes=sp.array(temp)
transDataResImp=logImpFormula(logToLin(transDataRes[:,3]), 1, 270)


wake24Meas99Overlap = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/shortened_end_piece_impedance/longWakelength80m99mmOverlap/longitudinal-impedance.csv"
wake24Meas99OverlapDat = sp.array(extract_dat(wake24Meas99Overlap))
wake24meas99OverlapBigGeo = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/shortened_end_piece_impedance/overlap99mmWake80mBiggerGeo/longitudinal-impedance.csv"
wake24Meas99OverlapBigGeoDat = sp.array(extract_dat(wake24meas99OverlapBigGeo))
directory_sims_15 = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/with-15-screen-conductors/longitudinal-impedance.csv"

datSimMeas15Cond  = sp.array(extract_dat(directory_sims_15))

directSimsNewModel = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/"

datSimsBigModelHighMesh=sp.array(extract_dat2013(directSimsNewModel+"NewDesignBigSimulation50mWake/longitudinal-impedance-real.txt"))
datSimsBigModelSmallMesh=sp.array(extract_dat2013(directSimsNewModel+"newDesignLongScreenOnly50mWakeHighMesh/longitudinal-impedance-real.txt"))
datSimsScreenModelSmallMesh=sp.array(extract_dat2013(directSimsNewModel+"NewDesignCutDownModel40mWake/longitudinal-impedance-real.txt"))
datSimsBigModel150mWakeMesh=sp.array(extract_dat2013(directSimsNewModel+"NewDesignBigSimulation150mWake/longitudinal-impedance-real.txt"))
datSimsBigModel150mWakeMeshWake=sp.array(extract_dat2013(directSimsNewModel+"NewDesignBigSimulation150mWake/wakepotential.txt")[:-1])
datSimsBigModel1100mWakeHighMeshDensityShortOverlap=sp.array(extract_dat(directSimsNewModel+"mkiBeamScreenShortenedLongWakeHighDensity/longitudinal-impedance-real.csv"))
datSimsBigModel1100mWakeHighMeshDensityShortOverlapWake=sp.array(extract_dat2013(directSimsNewModel+"mkiBeamScreenShortenedLongWakeHighDensity/wakepotential.txt")[:-1])
datSimsBigModel1100mWakeLowMeshDensityShortOverlap=sp.array(extract_dat(directSimsNewModel+"newDesignShortened100mWakeFullModel/longitudina-impedance-real.csv"))
datSimsBigModel1100mWakeLowMeshDensityShortOverlapWake=sp.array(extract_dat2013(directSimsNewModel+"newDesignShortened100mWakeFullModel/wakepotential.txt")[:-1])
datSimsLargeModelVariousOverlapsDir=directSimsNewModel+"ChangingOverlapLengthLargerModel/longitudinal-impedance-real-first.csv"
datSimsSmallModelVariousOverlapsDir=directSimsNewModel+"ChangingOverlapLength/longitudinal-impedance-real.csv"
datSimsLargeModelExtremeShortOverlapsDir=directSimsNewModel+"longWake50mShortenedFullModel/longitudinal-impedance-real.csv"
powLossSimulated=[]

fileReadLargeModelVariousOverlaps=open(datSimsLargeModelVariousOverlapsDir, "r+")
inputReadLargeModelVariousOverlaps=fileReadLargeModelVariousOverlaps.readlines()
fileReadLargeModelVariousOverlaps.close()

dataLargeModelVariousOverlaps=[]
temp=[]
count=2
while count<len(inputReadLargeModelVariousOverlaps):
    lineTemp=inputReadLargeModelVariousOverlaps[count].split(",")
    if lineTemp[1]=="":
        count+=3
        dataLargeModelVariousOverlaps.append(temp)
        temp=[]
        pass
    else:
        temp.append(map(float, lineTemp[1:3]))
        count+=1

dataLargeModelVariousOverlaps=sp.array(dataLargeModelVariousOverlaps)

fileReadSmallModelVariousOverlaps=open(datSimsSmallModelVariousOverlapsDir, "r+")
inputReadSmallModelVariousOverlaps=fileReadSmallModelVariousOverlaps.readlines()
fileReadSmallModelVariousOverlaps.close()

dataSmallModelVariousOverlaps=[]
temp=[]
count=2
while count<len(inputReadSmallModelVariousOverlaps):
    lineTemp=inputReadSmallModelVariousOverlaps[count].split(",")
    if lineTemp[1]=="":
        count+=3
        dataSmallModelVariousOverlaps.append(temp)
        temp=[]
        pass
    else:
        temp.append(map(float, lineTemp[1:3]))
        count+=1

dataSmallModelVariousOverlaps=sp.array(dataSmallModelVariousOverlaps)

fileReadLargeModelExtremeOverlaps=open(datSimsLargeModelExtremeShortOverlapsDir, "r+")
inputLargeModelExtremeOverlaps=fileReadLargeModelExtremeOverlaps.readlines()
fileReadLargeModelExtremeOverlaps.close()

dataLargeModelExtremeOverlaps=[]
temp=[]
count=2
while count<len(inputLargeModelExtremeOverlaps):
    lineTemp=inputLargeModelExtremeOverlaps[count].split(",")
    if count+1>=len(inputLargeModelExtremeOverlaps):
        dataLargeModelExtremeOverlaps.append(temp[:1000])
        count+=1
        pass
    elif lineTemp[1]=="":
        count+=3
        dataLargeModelExtremeOverlaps.append(temp[:1000])
        temp=[]
        pass
    else:
        temp.append(map(float, lineTemp[1:3]))
        count+=1

dataLargeModelExtremeOverlaps=sp.array(dataLargeModelExtremeOverlaps)

heatingListLargeModel=[]
heatingListPartsLargeModel=[]
for i in range(0,len(dataLargeModelVariousOverlaps)):
    temp = heatingValGauss(dataLargeModelVariousOverlaps[i], bCur, 25*10**-9, bLength)
    heatingListLargeModel.append(temp[0])
    heatingListPartsLargeModel.append(temp[1])


heatingListSmallModel=[]
heatingListPartsSmallModel=[]
for i in range(0,len(dataSmallModelVariousOverlaps)):
    temp = heatingValGauss(dataSmallModelVariousOverlaps[i], bCur, 25*10**-9, bLength)
    heatingListSmallModel.append(temp[0])
    heatingListPartsSmallModel.append(temp[1])

heatingListLargeModelExtreme=[]
heatingListPartsLargeModelExtreme=[]
for i in range(0,len(dataLargeModelExtremeOverlaps)):
    temp = heatingValGauss(dataLargeModelExtremeOverlaps[i], bCur, 25*10**-9, bLength)
    heatingListLargeModelExtreme.append(temp[0])
    heatingListPartsLargeModelExtreme.append(temp[1])

gaussProfPlot = gaussProf(dataSmallModelVariousOverlaps[0,:,0]*10**9, 1.001*10**-9)

fig = pl.figure()
ax1 = fig.add_subplot(111)
##pl.semilogy()
##pl.plot(wake24Meas99OverlapDat[:,0], abs(wake24Meas99OverlapDat[:,1]), label="CST simulation 99mm overlap")
##pl.plot(wake24Meas99OverlapBigGeoDat[:,0], abs(wake24Meas99OverlapBigGeoDat[:,1]), label="CST simulation 99mm overlap, larger geometry")
pl.plot(datSimsBigModelHighMesh[:,0],datSimsBigModelHighMesh[:,1]/2.97, 'g-', label="Simulation Long beam screen $\Delta_{x/y}=0.4mm$ $L_{overlap}=$106mm 50m wake")
pl.plot(datSimsBigModelSmallMesh[:,0],datSimsBigModelSmallMesh[:,1]/2.97, 'r-',label="Simulation Long beam screen $\Delta_{x/y}=0.4mm$ $L_{overlap}=$107mm 50m wake")
pl.plot(datSimsScreenModelSmallMesh[:,0],datSimsScreenModelSmallMesh[:,1]/2.97, 'y-',label="Simulation Capactive Only $\Delta_{x/y}=0.4mm$ $L_{overlap}=$106mm 50m wake")
pl.plot(datSimsBigModel150mWakeMesh[:,0],datSimsBigModel150mWakeMesh[:,1]/2.97, 'm-',label="Simulation Full Model $\Delta_{x/y}=0.1mm$ $L_{overlap}=$106mm 150m wake")
##pl.plot(datSimsBigModel1100mWakeHighMeshDensityShortOverlap[:-1,0], datSimsBigModel1100mWakeHighMeshDensityShortOverlap[:-1,1]/2.97, 'g-', label="Simulation large beam screen $L_{overlap}=$60mm 100m wake")
##pl.plot(datSimsBigModel1100mWakeLowMeshDensityShortOverlap[:-1,0],datSimsBigModel1100mWakeLowMeshDensityShortOverlap[:-1,1]/2.97, 'k-', label="Simulation large beam screen $L_{overlap}=$60mm 100m wake")
##for i in range(0,len(dataLargeModelVariousOverlaps)):
##    pl.plot(dataLargeModelVariousOverlaps[i,:,0], dataLargeModelVariousOverlaps[i,:,1]/2.97, label="$L_{overlap}$="+str(61+17+i*10)+"mm")
##    pl.axvline(overLapFreq((61+17+i*10)/1000.0, -0.004, 1)/10.0**9)
##for i in range(1,len(dataSmallModelVariousOverlaps)):
##    pl.plot(dataSmallModelVariousOverlaps[i,:,0], dataSmallModelVariousOverlaps[i,:,1]/2.97, label="$L_{overlap}$="+str((55+1+i*10))+"mm")
##for i in range(0,len(dataLargeModelExtremeOverlaps)):
##    pl.plot(dataLargeModelExtremeOverlaps[i,:,0], dataLargeModelExtremeOverlaps[i,:,1]/2.97, label="$L_{overlap}$="+str(61-(i+1)*10)+"mm")
##    pl.axvline(overLapFreq((56-(i+1)*10)/1000.0, 0.007, 1)/10.0**9)
##for i in range(1,5,1):
##    pl.axvline(overLapFreq(0.106,1.25*0.007,i)/10**9, color='k')
##    pl.axvline(overLapFreq(0.096,1.25*0.007,i)/10**9, color='b')
##pl.plot(dataSmallModelVariousOverlaps[1,:,0], dataSmallModelVariousOverlaps[1,:,1]/2.97, "g-")
##for entry in freqListHeating:
##    pl.axvline(entry/10**3)
##pl.plot(measurementsMKI061213Freq/10.0**9, measurementsMKI061213Imp, 'kx', markersize=16.0, label="Measured MKI10-T06-HC13 Resonant")
##pl.plot(freqList/10.0**3, splineFitMKI061213, 'k-', label="Interpolation of measurement Measured MKI10-T06-HC13")
##pl.plot(transDataRes[:,0]/10.0**9, transDataResImp/lDUT-880,'b-', label="Measured MKI10-T06-HC13 Classical Trans")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$\Re{}e (Z_{\parallel})$ ($\Omega$/m)", fontsize=16.0)
pl.legend(loc="upper right")
pl.axis([0,2.0,-30,150])
##pl.show()
pl.clf()

fig = pl.figure()
ax1 = fig.add_subplot(111)
for i in range(1,len(dataSmallModelVariousOverlaps)):
    ax1.plot(dataSmallModelVariousOverlaps[i,:,0], dataSmallModelVariousOverlaps[i,:,1]/2.97, label="$L_{overlap}$="+str((55+1+i*10))+"mm")
pl.xlabel("Frequency (GHz)", fontsize=18.0)
pl.ylabel("$\Re{}e (Z_{\parallel})$ ($\Omega$/m)", fontsize=18.0)
pl.legend(loc="upper left")

ax2 = ax1.twinx()
ax2.plot(dataSmallModelVariousOverlaps[0,:,0], linToLog(gaussProfPlot**2), label="1ns Gaussian Profile")
pl.ylabel("Beam Current (dB)", fontsize=18.0)
pl.legend(loc="upper right")
pl.xlim(0,1.2)
pl.show()
pl.clf()

##
for i in range(0,len(heatingListPartsLargeModel)):
    pl.plot(heatingListPartsLargeModel[i][:,0],heatingListPartsLargeModel[i][:,1]/2.97, label=str((61+17+i*10))+"mm")
    print "Heating for large model with overlap of "+str((61+17+i*10))+"mm: "+str(heatingListLargeModel[i]/2.97)
##for i in range(0,len(heatingListPartsSmallModel)):
##    pl.plot(heatingListPartsSmallModel[i][:,0],heatingListPartsSmallModel[i][:,1]/2.97, label="$L_{overlap}$="+str((55+1+i*10))+"mm")
##    print "Heating for small model with overlap of "+str((55+1+i*10))+"mm: "+str(heatingListSmallModel[i]/2.97)
##    print "Heating for small model with overlap of "+str((55+1+i*10))+"mm: "+str(heatingListPartsSmallModel[i][1:,1].sum()/2.97)
for i in range(0,len(heatingListPartsLargeModelExtreme)):
    pl.plot(heatingListPartsLargeModelExtreme[i][:,0],heatingListPartsLargeModelExtreme[i][:,1]/2.97, label=str((61-(i+1)*10))+"mm")
    print "Heating for small model with overlap of "+str((61-(i+1)*10))+"mm: "+str(heatingListLargeModelExtreme[i]/2.97)

pl.xlim(0.0,1.5)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("Power Loss (W/m)", fontsize=16.0)
##pl.show()
pl.clf()


for i in range(0,len(heatingListPartsLargeModel)):
    pl.plot((61+17+i*10),heatingListLargeModel[i]/2.97, "bo")
##for i in range(0,len(heatingListPartsSmallModel)):
##    pl.plot((55+1+i*10),heatingListSmallModel[i]/2.97, "ro")
##    pl.plot((55+1+i*10),heatingListPartsSmallModel[i][1:,1].sum()/2.97, "ro")
for i in range(0,len(heatingListPartsLargeModelExtreme)):
    pl.plot((61-(i+1)*10),heatingListLargeModelExtreme[i]/2.97,"ko")
pl.xlabel("Overlap Length (mm)", fontsize=16.0)
pl.ylabel("Power Loss (W/m)", fontsize=16.0)
##pl.show()
pl.clf()

########## Wakepotential Plotting ###########
pl.semilogx()
pl.plot(datSimsBigModel150mWakeMeshWake[:,0],datSimsBigModel150mWakeMeshWake[:,1]/2.97, 'm-',label="Simulation Full Model $\Delta_{x/y}=0.1mm$ $L_{overlap}=$106mm 150m wake")
pl.plot(datSimsBigModel1100mWakeHighMeshDensityShortOverlapWake[:,0], datSimsBigModel1100mWakeHighMeshDensityShortOverlapWake[:,1], label="Simulation large beam screen $L_{overlap}=$60mm 100m wake")
pl.plot(datSimsBigModel1100mWakeLowMeshDensityShortOverlapWake[:,0], datSimsBigModel1100mWakeLowMeshDensityShortOverlapWake[:,1], label="Simulation large beam screen $L_{overlap}=$60mm 100m wake")

pl.xlabel("Distance (mm)", fontsize=16.0)
pl.ylabel("$W_{\parallel}$", fontsize=16.0)
##pl.show()
pl.clf()

######## Writing Fitted Data to an External File ##########
