import scipy as sp
import pylab as pl
import os, sys

class ClosestDict(dict):
    def get(self, key):
        key=min(self.iterkeys(), key=lambda x:abs(x-key))
        return dict.get(self,key)
    
mu0 = 4*sp.pi*10.0**-7
muCopper = 1.0
muDoubleCopper = 1.0
condCopper = 1.0*10.0**7
rIn = 0.0005
rOut = 0.010
lenCable=0.0

directXToT = ClosestDict({0.0: 1.0, 0.5: 0.9998, 0.6: 0.9997, 0.7:0.9994, 0.8:0.9989,
              0.9:0.9983, 1.0:0.9974, 1.1:0.9962, 1.2:0.9946, 1.3:0.9927,
              1.4:0.9902, 1.5:0.9871, 1.6:0.9834, 1.7:0.9790, 1.8:0.9739,
              1.9:0.9680, 2.0:0.9611, 2.2:0.9448, 2.4:0.9248, 2.6:0.9013,
              2.8:0.8745, 3.0:0.8452, 3.2:0.8140, 3.4:0.7818, 3.6:0.7493,
              3.8:0.7173, 4.0:0.6863, 4.2:0.6568, 4.4:0.6289, 4.6:0.6028,
              4.8:0.5785, 5.0:0.556, 5.2:0.5351, 5.4:0.5157, 5.6:0.4976,
              5.8:0.4809, 6.0:0.4652, 6.2:0.4506, 6.4:0.4368, 6.6:0.4239,
              6.8:0.4117, 7.0:0.4002, 7.2:0.3893, 7.4:0.3790, 7.6:0.3692,
              7.8:0.3599, 8.0:0.3511, 8.2:0.3426, 8.4:0.3346,8.6:0.3269,
              8.8:0.3196, 9.0:0.3126, 9.2:0.3058, 9.4:0.2994, 9.6:0.2932,
              9.8:0.2873, 10.0:0.2816, 10.5:0.2682, 11.0:0.2562, 11.5:0.2452,
              12.0:0.2350, 12.5:0.2257, 13.0:0.2170, 13.5:0.2090, 14.0:0.2016,
              14.5:0.1947, 15.0:0.1882, 16.0:0.1765, 17.0:0.1661, 18.0:0.1589,
              19.0:0.1487, 20.0:0.1413, 21.0:0.1346, 22.0:0.1285, 23.0:0.1229,
              24.0:0.1178, 25.0:0.1131, 26.0:0.1087, 28.0:0.1010, 30.0:0.942,
              32.0:0.0884, 34.0:0.0832, 36.0:0.0785, 38.0:0.0744, 40.0:0.0707,
              42.0:0.0673, 44.0:0.0643, 46.0:0.0615, 48.0:0.0589, 50.0:0.0566,
              60.0:0.0471, 70.0:0.0404, 80.0:0.0354, 90.0:0.0314, 100.0:0.0283
              })


def skinDepth(freq, conductivity, mur, epsr):
    return (1/(conductivity*sp.pi*freq*mur*mu0))**0.5

def inductCoax(freq, rInner, rOuter, cond):
    return 2*10**(-4)*sp.log((2*rOuter+ skinDepth(freq, cond, 1, 1))/(2*rInner+ skinDepth(freq, cond, 1, 1)))

def inductInnerCond(freq, rInner, lenCoax, cond, muPrime, muDouble):
    return 2*lenCoax*(sp.log(2*lenCoax/rInner)-1+(muDouble/4)*directXToT.get(2*sp.pi*rInner*(2*muPrime*mu0*freq/cond)**0.5))

def inductOuterCond(rOuter, lenCoax):
    return 2*lenCoax*(sp.log(2*lenCoax/rOuter)-1)

def inductCoaxLine(freq, rInner, rOuter, cond, muPrime):
##    print sp.log(rOuter/rInner), directXToT.get(2*sp.pi*rInner*(2*muPrime*freq*cond)**0.5)
##    return 2*(sp.log(rOuter/rInner) + 0.25*directXToT.get(2*sp.pi*rInner*(2*mu0*muPrime*freq*cond)**0.5))
    return mu0/(2*sp.pi)*(sp.log(rOuter/rInner) + 0.25*directXToT.get(2*sp.pi*rInner*(2*mu0*muPrime*freq*cond)**0.5))

def fileInductRead(targetFile):
    fileRead=open(targetFile, 'r+')

    listData = fileRead.readlines()
    fileRead.close()
    listCurrents = []
    listStoredEnergyElectric=[]
    listStoredEnergyMagnetic=[]
    for entry in listData:
        if entry.startswith("Total current (integral J ds)  "):
            tidbit=entry.replace("Total current (integral J ds)                    = ","")
            listCurrents.append(float(tidbit.replace(" [A]", "")))
        elif entry.startswith("Stored energy/unit length (integral A.J/2 ds)"):
            tidbit=entry.replace("Stored energy/unit length (integral A.J/2 ds)    = ", "")
            listStoredEnergyElectric.append(float(tidbit.replace(" [J/mm]", ""))*10.0**3)
        elif entry.startswith("Stored energy/unit length (integral B.H/2 ds) "):
            tidbit=entry.replace("Stored energy/unit length (integral B.H/2 ds)    = ", "")
            listStoredEnergyMagnetic.append(float(tidbit.replace(" [J/mm]", ""))*10.0**3)
        else:
            pass

    tempCurrents = []
    tempStoredEnergyElectric=[]
    tempStoredEnergyMagnetic=[]

    for i in range(0,len(listStoredEnergyMagnetic)-1,2):
        tempCurrents.append(listCurrents[i]+listCurrents[i+1])
        tempStoredEnergyElectric.append((listStoredEnergyElectric[i]**2+listStoredEnergyElectric[i+1]**2)**0.5)
        tempStoredEnergyMagnetic.append((listStoredEnergyMagnetic[i]**2+listStoredEnergyMagnetic[i+1]**2)**0.5)


    arrCurrents=sp.array(tempCurrents)
    arrStoredEnergyElectric=sp.array(tempStoredEnergyElectric)
    arrStoredEnergyMagnetic=sp.array(tempStoredEnergyMagnetic)
    listInductances = []
##    print arrStoredEnergyMagnetic

    #Calculate inductances using U=0.5 I^2 L U and I multiplied by 4 for quarter geometry

    geoFact=4
    for i in range(0,len(arrCurrents)):
        listInductances.append([arrStoredEnergyElectric[i]*geoFact*2/(geoFact*arrCurrents[i])**2,arrStoredEnergyMagnetic[i]*geoFact*2/(geoFact*arrCurrents[i])**2])

    arrInductances=sp.array(listInductances)

    return arrInductances

def maxwellDataReadDC(targetFile):
    datChan = open(targetFile, "r+")
    datRead = datChan.readlines()
    datChan.close()
    temp=datRead[-1].split(",")
    return map(float,temp)

def maxwellDataReadAC(targetFile):
    datChan = open(targetFile, "r+")
    datRead = datChan.readlines()
    datChan.close()
    temp=[]
    for entry in datRead[1:]:
        tmp = entry.split(",")
        tmp[-1] = tmp[-1].rstrip("\n")
        temp.append(map(float,tmp))
    return sp.array(temp)

####### Data from Opera2D ########

####### A coaxial line ########

directTar = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/ferriteTorroidHeating/coaxialWireTest/opera2D/"
curInnerInner = fileInductRead(directTar+"innerCurrentInner.lp")
curInnerAll = fileInductRead(directTar+"innerCurrentAll.lp")
curInnerInnerTang = fileInductRead(directTar+"innerCurrentInnerTang.lp")
curInnerAllTang = fileInductRead(directTar+"innerCurrentAllTang.lp")
curOuterOuter = fileInductRead(directTar+"outerCurrentOuter.lp")
curOuterAll = fileInductRead(directTar+"outerCurrentAll.lp")
curOuterInnerAll = fileInductRead(directTar+"innerOuterCurrentBoth.lp")
curOuterInnerInner = fileInductRead(directTar+"innerOuterCurrentInner.lp")
curOuterInnerOuter = fileInductRead(directTar+"innerOuterCurrentOuter.lp")

####### A coaxial lineEnforcedSymmetry ########

directTar = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/ferriteTorroidHeating/coaxialWireTest/opera2D/SimulationSymmetry/"
curInnerInnerSym = fileInductRead(directTar+"InnerCurInner.lp")
curInnerInnerAirSym = fileInductRead(directTar+"InnerCurInnerAir.lp")
curInnerOuterSym = fileInductRead(directTar+"InnerCurOuter.lp")
curInnerOuterAirSym = fileInductRead(directTar+"InnerCurOuterAir.lp")
curInnerAllSym = fileInductRead(directTar+"InnerCurAll.lp")
curOuterInnerSym = fileInductRead(directTar+"OuterCurInner.lp")
curOuterInnerAirSym = fileInductRead(directTar+"OuterCurInnerAir.lp")
curOuterOuterSym = fileInductRead(directTar+"OuterCurOuter.lp")
curOuterOuterAirSym = fileInductRead(directTar+"OuterCurOuterAir.lp")
curOuterAllSym = fileInductRead(directTar+"OuterCurAll.lp")
curInnerOuterInnerSym = fileInductRead(directTar+"InnerOuterCurInner.lp")
curInnerOuterInnerAirSym = fileInductRead(directTar+"InnerOuterCurInnerAir.lp")
curInnerOuterOuterSym = fileInductRead(directTar+"InnerOuterCurOuter.lp")
curInnerOuterOuterAirSym = fileInductRead(directTar+"InnerOuterCurOuterAir.lp")
curInnerOuterAllSym = fileInductRead(directTar+"InnerOuterCurAll.lp")


####### Data From Maxwell DC ##########
""" Data from Maxwell exported in the format
1) Induct All
2) Induct Inner
3) Induct Outer
"""

directTar = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/ferriteTorroidHeating/coaxialWireTest/"
curInnerAutoEnergy = maxwellDataReadDC(directTar+"inductanceFromMaxwellManualAutoEnergyInnerCurOnly.csv")
curOuterAutoEnergy = maxwellDataReadDC(directTar+"inductanceFromMaxwellManualAutoEnergyOuterCurOnly.csv")
curInnerOuterAutoEnergy = maxwellDataReadDC(directTar+"inductanceFromMaxwellManualAutoEnergyInnerOuterCur.csv")
curInnerOuterReversedAutoEnergy = maxwellDataReadDC(directTar+"inductanceFromMaxwellManualAutoEnergyInnerOuterCurReversed.csv")
curInnerManEnergy = maxwellDataReadDC(directTar+"inductanceFromMaxwellManualCalcEnergyInnerCurOnly.csv")
curOuterManEnergy = maxwellDataReadDC(directTar+"inductanceFromMaxwellManualCalcEnergyOuterCurOnly.csv")
curInnerOuterManEnergy = maxwellDataReadDC(directTar+"inductanceFromMaxwellManualManEnergyInnerOuterCur.csv")
curInnerOuterReversedManEnergy = maxwellDataReadDC(directTar+"inductanceFromMaxwellManualManEnergyInnerOuterCurReversed.csv")

####### Data From Maxwell AC ##########
""" Data from Maxwell exported in the format
1) Induct All
2) Induct Inner
3) Induct Outer
"""

directTar = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/ferriteTorroidHeating/coaxialWireTest/"
curInnerAutoEnergyAC = maxwellDataReadAC(directTar+"inductMaxwellACCurrentInnerAuto.csv")
curOuterAutoEnergyAC = maxwellDataReadAC(directTar+"inductMaxwellACCurrentOuterAuto.csv")
curInnerOuterAutoEnergyAC = maxwellDataReadAC(directTar+"inductMaxwellACCurrentInnerOuterAuto.csv")
curInnerOuterReversedAutoEnergyAC = maxwellDataReadAC(directTar+"inductMaxwellACCurrentInnerOuterReversedAuto.csv")
curInnerManEnergyAC = maxwellDataReadAC(directTar+"inductMaxwellACCurrentInnerMan.csv")
curOuterManEnergyAC = maxwellDataReadAC(directTar+"inductMaxwellACCurrentOuterMan.csv")
curInnerOuterManEnergyAC = maxwellDataReadAC(directTar+"inductMaxwellACCurrentInnerOuterMan.csv")
curInnerOuterReversedManEnergyAC = maxwellDataReadAC(directTar+"inductMaxwellACCurrentInnerOuterReversedMan.csv")
curInnerMatrixEnergyAC = maxwellDataReadAC(directTar+"inductMaxwellACCurrentInnerMatrix.csv")
curOuterMatrixEnergyAC = maxwellDataReadAC(directTar+"inductMaxwellACCurrentOuterMatrix.csv")
curInnerOuterMatrixEnergyAC = maxwellDataReadAC(directTar+"inductMaxwellACCurrentInnerOuterMatrix.csv")
curInnerOuterReversedMatrixEnergyAC = maxwellDataReadAC(directTar+"inductMaxwellACCurrentInnerOuterReversedMatrix.csv")


####### Analytical Calculations ########


freqList = []
for i in range(1,10):
    for j in range(1, 10):
        freqList.append(i/10.0*10.0**j)

freqList=list(set(freqList))
freqList.sort()
freqList=sp.array(freqList)

print len(freqList)

freqListDC=0.0

selfInductInner = []
totalInductCoax = []
thing = []
for entry in freqList:
    thing.append([2*sp.pi*rIn*(2*muCopper*mu0*entry*condCopper)**0.5, directXToT.get(2*sp.pi*rIn*(2*muCopper*mu0*entry*condCopper)**0.5)])
    selfInductInner.append(inductInnerCond(entry, rIn, lenCable, condCopper, muCopper, muDoubleCopper))
    totalInductCoax.append(inductCoaxLine(entry, rIn, rOut, condCopper, muCopper))

selfInductOuter = inductOuterCond(rOut, lenCable)

##inductCoax =inductCoax(freqList, rIn, rOut, condCopper)
thing=sp.array(thing)

##print len(curOuterOuter[:,0])
##pl.loglog()
pl.semilogx()
###### Plot Inner Current Only
print curInnerManEnergy[0], inductCoaxLine(100000, 0.0005, 0.005, 5.8*10.0**7, 1)
##pl.plot(1,curInnerManEnergy[0],"bx")
##pl.plot(1,curOuterManEnergy[0],"rx")
##pl.plot(1,curInnerOuterManEnergy[0],"gx")
##pl.plot(1,curInnerOuterReversedManEnergy[0],"kx")
##pl.plot(1,curInnerAutoEnergy[0],"b+")
##pl.plot(1,curOuterAutoEnergy[0],"r+")
##pl.plot(1,curInnerOuterAutoEnergy[0],"g+")
##pl.plot(1,curInnerOuterReversedAutoEnergy[0],"k+")
pl.plot(freqList, curInnerInner[:,1]*10**9, 'r-')
##pl.plot(freqList, curInnerAll[:,1]*10**9, 'k-')
##pl.plot(freqList, curInnerInnerTang[:,1]*10**9, 'b-')
##pl.plot(freqList, curInnerAllTang[:,1]*10**9, 'g-')
##pl.plot(curInnerAutoEnergyAC[:,0]*10**9, 2*curInnerAutoEnergyAC[:,1]*10**9, "rx")
##pl.plot(curInnerManEnergyAC[:,0]*10**9, curInnerManEnergyAC[:,1]*10**9, "kx")
##pl.plot(curInnerManEnergyAC[:,0]*10**9, curInnerManEnergyAC[:,2]*10**9, "rx")
##pl.plot(curInnerManEnergyAC[:,0]*10**9, curInnerManEnergyAC[:,3]*10**9, "bx")
##pl.plot(curInnerMatrixEnergyAC[:,0]*10**9,curInnerMatrixEnergyAC[:,4], "kx")

###### Plot Outer Current Only

##pl.plot(1,curInnerManEnergy[1]*10**9,"bx")
##pl.plot(1,curOuterManEnergy[1]*10**9,"rx")
##pl.plot(1,curInnerOuterManEnergy[1]*10**9,"gx")
##pl.plot(1,curInnerOuterReversedManEnergy[1]*10**9,"kx")
##pl.plot(1,curInnerAutoEnergy[1]*10**9,"b+")
##pl.plot(1,curOuterAutoEnergy[1]*10**9,"r+")
##pl.plot(1,curInnerOuterAutoEnergy[1]*10**9,"g+")
##pl.plot(1,curInnerOuterReversedAutoEnergy[1]*10**9,"k+")
pl.plot(freqList, curOuterOuter[:,1]*10**9, 'r-.')
##pl.plot(freqList, curOuterAll[:,1]*10**9, 'k--')
##pl.plot(curOuterAutoEnergyAC[:,0]*10**9, 2*curOuterAutoEnergyAC[:,2]*10**9, "rx")
##pl.plot(curOuterManEnergyAC[:,0]*10**9, curOuterManEnergyAC[:,3]*10**9, "bo")
##pl.plot(curOuterMatrixEnergyAC[:,0]*10**9,curOuterMatrixEnergyAC[:,3], "kx")

###### Plot InnerandOuter Current

##pl.plot(1,curInnerManEnergy[0]*10**9,"bx")
##pl.plot(1,curOuterManEnergy[1]*10**9,"rx")
##pl.plot(1,curInnerOuterManEnergy[2]*10**9,"gx")
##pl.plot(1,curInnerOuterReversedManEnergy[3]*10**9,"kx")
##pl.plot(1,curInnerAutoEnergy[0]*10**9,"b+")
##pl.plot(1,curOuterAutoEnergy[1]*10**9,"r+")
##pl.plot(1,curInnerOuterAutoEnergy[2]*10**9,"g+")
##pl.plot(1,curInnerOuterReversedAutoEnergy[3]*10**9,"k+")
##pl.plot(freqList, curOuterInnerAll[:,1]*10**9, "m-")
pl.plot(freqList, curOuterInnerInner[:,1]*10**9, "b-")
pl.plot(freqList, curOuterInnerOuter[:,1]*10**9, "g-")
##pl.plot(freqList, curOuterInnerInnerNorm[:,1]*10**9)
##pl.plot(freqList, (curInnerInner[:,1]-curOuterInnerInner[:,1])*10**9, "m--")
##pl.plot(freqList, (curInnerAll[:,1]-curOuterInnerInner[:,1])*10**9, "b--")
##pl.plot(freqList, (curOuterOuter[:,1]-curOuterInnerOuter[:,1])*10**9, "k--")
##pl.plot(freqList, (curOuterAll[:,1]-curOuterInnerOuter[:,1])*10**9, "g--")
##print (curInnerInner[:,1]-curOuterInnerInner[:,1])*10**9/461

##pl.plot(curInnerOuterAutoEnergyAC[:,0]*10**9, curInnerOuterAutoEnergyAC[:,3]*10**9, "rx")
##pl.plot(curInnerOuterManEnergyAC[:,0]*10**9, curInnerOuterManEnergyAC[:,3]*10**9, "bx")
##pl.plot(curInnerOuterMatrixEnergyAC[:,0]*10**9, curInnerOuterMatrixEnergyAC[:,3], "kx")

##pl.plot(curInnerOuterAutoEnergyAC[:,0]*10**9, curInnerOuterAutoEnergyAC[:,1]*10**9, "r-.")
##pl.plot(curInnerOuterAutoEnergyAC[:,0]*10**9, 2*curInnerAutoEnergyAC[:,3]*10**9-2*curInnerOuterAutoEnergyAC[:,1]*10**9, "rx")
##pl.plot(curInnerOuterAutoEnergyAC[:,0]*10**9, 2*curInnerAutoEnergyAC[:,1]*10**9-2*curInnerOuterAutoEnergyAC[:,1]*10**9, "r+")
##pl.plot(curInnerOuterManEnergyAC[:,0]*10**9, curInnerOuterManEnergyAC[:,3]*10**9, "b+")
pl.plot(curInnerOuterManEnergyAC[:,0]*10**9, curInnerOuterManEnergyAC[:,2]*10**9/10, "r+")
##pl.plot(curInnerOuterManEnergyAC[:,0]*10**9, curInnerOuterManEnergyAC[:,1]*10**9, "k+")
##pl.plot(curInnerOuterMatrixEnergyAC[:,0]*10**9, curInnerOuterMatrixEnergyAC[:,3], "kx")

##pl.plot(curInnerOuterReversedAutoEnergyAC[:,0]*10**9, curInnerOuterReversedAutoEnergyAC[:,3]*10**9, "r-")
##pl.plot(curInnerOuterReversedManEnergyAC[:,0]*10**9, curInnerOuterReversedManEnergyAC[:,3]*10**9, "b-")
##pl.plot(curInnerOuterReversedMatrixEnergyAC[:,0]*10**9, curInnerOuterReversedMatrixEnergyAC[:,3], "k-")

##pl.plot(curInnerOuterReversedAutoEnergyAC[:,0]*10**9, curInnerOuterReversedAutoEnergyAC[:,1]*10**9, "r-")
##pl.plot(curInnerOuterReversedManEnergyAC[:,0]*10**9, curInnerOuterReversedManEnergyAC[:,1]*10**9, "b-")
##pl.plot(curInnerOuterReversedMatrixEnergyAC[:,0]*10**9, curInnerOuterReversedMatrixEnergyAC[:,3], "k-")

##pl.plot(freqList, selfInductInner, label="Self Inductance Analytical")
##pl.plot(freqList, totalInductCoax, label="Total Inductance Analytical")
##pl.legend(loc="lower left")
pl.xlabel("Frequency (Hz)")
pl.ylabel("Inductance (nH)")
##pl.ylim(0,2000)
##pl.axis([10**0,10**9,0,500])
##pl.show()
##pl.clf()

##### Test of taking the energy from all sectors

##pl.semilogx()
pl.loglog()
pl.plot(freqList, curInnerInnerSym[:,1]*10**9, "r--")
##pl.plot(freqList, curInnerInnerAirSym[:,1]*10**9, "g--")
pl.plot(freqList, curInnerOuterSym[:,1]*10**9, "m--")
##pl.plot(freqList, curInnerOuterAirSym[:,1]*10**9, "k--")
##pl.plot(freqList, curInnerAllSym[:,1]*10**9, "b--")

pl.plot(freqList, curOuterInnerSym[:,1]*10**9, "r-.")
##pl.plot(freqList, curOuterInnerAirSym[:,1]*10**9, "g-.")
pl.plot(freqList, curOuterOuterSym[:,1]*10**9, "m-")
##pl.plot(freqList, curOuterOuterAirSym[:,1]*10**9, "k-.")
##pl.plot(freqList, curOuterAllSym[:,1]*10**9, "b-.")

pl.plot(freqList, curInnerOuterInnerSym[:,1]*10**9, "r-")
##pl.plot(freqList, curInnerOuterInnerAirSym[:,1]*10**9, "g-")
pl.plot(freqList, curInnerOuterOuterSym[:,1]*10**9, "m-")
##pl.plot(freqList, curInnerOuterOuterAirSym[:,1]*10**9, "k-")
##pl.plot(freqList, curInnerOuterAllSym[:,1]*10**9, "b-")

##pl.legend(loc="lower left")
pl.xlabel("Frequency (Hz)")
pl.ylabel("Inductance (nH)")
##pl.ylim(0,2000)
##pl.axis([10**0,10**9,0,500])
pl.show()
pl.clf()
