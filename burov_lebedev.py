import scipy as sp
import pylab as pl

Z0 = 377
mu0 = 500
beta = 1.0
gamma = 27.7286
a = 0.005   # inner plate
d = 0.015   # outer plate
cond_gr = 7*10**4

def skin(freq,cond):
    return (2/(2*sp.pi*freq*mu0*cond))**0.5

def kappa(freq,cond):
    return (complex(0,-2.0/skin(freq,cond)))**0.5

def tau(freq,cond):
    return kappa(freq,cond)*a*sp.tanh(kappa(freq,cond)*d)

def vert_dip(freq,cond):
    return complex(0,-Z0*sp.pi**2/(2*sp.pi*a**2*beta*gamma**2*12))+complex(0,-sp.pi**2*Z0*beta/(12*2*sp.pi*a**2*(1+tau(freq,cond)/2)))

def horz_dip(freq,cond):
    return complex(0,-Z0*sp.pi**2/(2*sp.pi*a**2*beta*gamma**2*24))+complex(0,-sp.pi**2*Z0*beta/(24*2*sp.pi*a**2*(1+tau(freq,cond)/2)))


freq_list = []
for i in range(0,9,1):
    for j in range(1,100,1):
        freq_list.append(float((j/10.0)*10.0**i))

freq_list = list(set(freq_list))
freq_list.sort()

temp_real = []
temp_imag = []

for i in freq_list:
    temp_real.append(vert_dip(i,cond_gr).real)
    temp_imag.append(vert_dip(i,cond_gr).imag)

pl.loglog()
pl.plot(freq_list,temp_real,'k-')
pl.plot(freq_list,temp_imag,'r-')
pl.xlabel("Frequency (Hz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m^{2}$)", fontsize=16)
pl.show()
pl.clf()
