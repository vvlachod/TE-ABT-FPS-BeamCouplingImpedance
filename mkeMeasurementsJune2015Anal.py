import scipy as sp
import scipy.optimize as op
import pylab as pl
import time, csv, os, sys
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack
from scipy import signal

dirHead="E:/PhD/1st_Year_09-10/Data/CLICStripline/singleWireDisplace/"

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2)

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2

def gaussProfTime(dist, bLength, order):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(dist/sigma)**2/2)

def paraProfTime(dist, bunch_length):
    if bunch_length/2 <= dist:
        return 0
    elif bunch_length/2 >= dist:
        return 1-(2*dist/bunch_length)**2
    else:
        return 0

def cosProfTime(dist, bunch_length):
    if bunch_length/2 <= dist:
        return 0
    if bunch_length/2 >= dist:
##        print dist,bunch_length
        return sp.cos(sp.pi/bunch_length*dist)**2
    else:
        return 0
                                        
def logToLin(data):
    return 10**(data/20)

def linToLog(data):
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

def readS21FromVNA(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[6:]:
        temp=line.split("\t")
        outputData.append(map(float,temp))
    return outputData

def readS21FromCSV(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[3:]:
        temp=line.split(",")
        outputData.append(map(float,temp))
    return outputData

def fitQuadraticSquaresImp(sParaData, dispWire):
    impLongDat=[]
    impTransDat=[]
    quadFunc = lambda p, x: p[0] + x*p[1] + x**2*p[2]
    errFunc = lambda p, x, z: (z-quadFunc(p,x))
    try:
        os.mkdir("E:/PhD/1st_Year_09-10/Data/MKE-K-sept10/MKEJun2015/plots")
    except:
        pass
    for i in range(0,len(sParaData[0])):
        fitDataReal = sp.array(logImpFormula(logToLin(sParaData[:,i,3]),1,255))
        initVal = [-1.0,1.0,1.0]
        pReal, pFunc = op.leastsq(errFunc, initVal, args=(dispWire, fitDataReal), full_output=0)
        impLongDat.append([sParaData[0,i,0], pReal[0]])
        totTrans = (pReal[2]*C)/(2*sp.pi*sParaData[0,i,0])
        impTransDat.append([sParaData[0,i,0], totTrans])
        dispPlot = sp.linspace(dispWire[0]-0.005,dispWire[-1]+0.005,100)
##        pl.plot(dispWire,fitDataReal)
##        pl.plot(dispPlot, quadFunc(pReal,dispPlot))
##        pl.savefig("E:/PhD/1st_Year_09-10/Data/CLICStripline/singleWireDisplace/plots/plotImp"+str(i)+".png")
##        pl.clf()
        
    return sp.array(impLongDat), sp.array(impTransDat)

def impTwoWire(sParaData):
    impData=logImpFormula(logToLin(sParaData[:,3]),1,180)
    normImp=[]
    for i in range(0,len(sParaData)):
        normImp.append([sParaData[i,0],(impData[i]*C)/(2*sp.pi*sParaData[i,0]*0.014**2)-(logImpFormula(logToLin(sParaData[0,3]),1,180)*C)/(2*sp.pi*sParaData[i,0]*0.014**2)])
    return sp.array(normImp)

def heatingValGauss(impArr, beamCur, bunSpac, bunLen):
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac/10**9,impArr[-1,0],impArr[-1,0]*bunSpac)
    splineFitHeatingImp = SplineFitImp(freqListHeating)
    heatingTotalPart = []
    for i in range(0,len(splineFitHeatingImp)):
        heatingTotalPart.append([freqListHeating[i], abs(2*splineFitHeatingImp[i]*(beamCur**2)*gaussProf(freqListHeating[i], bunLen))])
    heatingTotalPart=sp.array(heatingTotalPart)
    cumHeating = []
    for i in range(0,len(heatingTotalPart)):
        cumHeating.append([heatingTotalPart[i,0],sum(heatingTotalPart[:i,1])])
    cumHeating=sp.array(cumHeating)
    return heatingTotalPart[:,1].sum(), heatingTotalPart, cumHeating

def bunchTrainProfTime(bunSpacing, trainSpacing, bunInTrain, trainsInMachine, blankBunSpaces, timeRes):
    spaceCount = ((trainsInMachine*bunInTrain*bunSpacing)+(trainSpacing*(trainsInMachine))+bunSpacing/2+(bunInTrain*trainsInMachine*bLength)+bunSpacing*blankBunSpaces)/timeRes
    print 1/((trainsInMachine*bunInTrain*bunSpacing)+(trainSpacing*(trainsInMachine))+bunSpacing/2+(bunInTrain*trainsInMachine*bLength)+bunSpacing*blankBunSpaces)
    ampListTime = [0 for i in range(0,int(spaceCount))]
##    print spaceCount
    timing = []
    for i in range(0,len(ampListTime)):
        timing.append(i*timeRes)

    bunLimit = int((bLength)/timeRes)

    for i in range(0,trainsInMachine):
        for j in range(0,bunInTrain):
            midPoint = int(((j*bunSpacing)+(i*trainsInMachine*trainSpacing)+(i*bunInTrain*bunSpacing)+bLength/2)/timeRes)
    ##        print midPoint, bunLimit, i
            for k in range(midPoint-bunLimit, midPoint+bunLimit):
                try:
                    ampListTime[k] = gaussProfTime(abs(k-midPoint)*timeRes, bLength, 32)
                except:
    ##                print i, j, j
                    pass

    return timing, ampListTime

def bunchTrainProfFreq(bunSpacing, trainSpacing, bunInTrain, trainsInMachine, blankBunSpaces, freqRes):
    timeSpace = 0.02*10.0**-9
    timing, timeSig = bunchTrainProfTime(bunSpacing, trainSpacing, bunInTrain, trainsInMachine, blankBunSpaces, timeSpace)
    N = len(timing)
    f = 1/timeSpace*sp.r_[0:(N/2)]/N
    n= len(f)
    linFreqSpec = sp.fft(timeSig)
    linFreqSpec = linFreqSpec[0:n]/sp.amax(linFreqSpec)
    return f, abs(linFreqSpec)

C = 299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005

f_rev = 43400
nBunches = 144
qPart = 1.6*10.0**-19
nPart=2.5*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=4*0.2/C
bunchCur=nPart*qPart*f_rev
freqRes = 10.0**5

##### 25ns Spec SPS

bunSpacing25ns = 24.97*10.0**-9
trainSpacing25ns = 8*bunSpacing25ns
bunInTrain25ns = 72
trainsInMachine25ns = 4
blankBunSpaces25ns = 612

##### 50ns Spec SPS

bunSpacing50ns = 2*24.97*10.0**-9
trainSpacing50ns = 224.6*10.0**-9
bunInTrain50ns = 36
trainsInMachine50ns = 4
blankBunSpaces50ns = 306


fileListNoCap = ["E:/PhD/1st_Year_09-10/Data/MKE-K-sept10/MKEJun2015/S21MATCHED"+str(i)+"MM.S2P" for i in [-9,-5,0,5,8]]
fileListNoCapVert = ["E:/PhD/1st_Year_09-10/Data/MKE-K-sept10/mke05062015/verticalplane/S21VERTICAL"+str(i)+"MM.S2P" for i in [-5,0,5]]
fileCapBoxOne = "E:/PhD/1st_Year_09-10/Data/MKE-K-sept10/09-06-15/S21FREQDOM1STCAPBOX.S2P"
fileCapBoxTwo = "E:/PhD/1st_Year_09-10/Data/MKE-K-sept10/10-06-15/S21FREQDOM2NDCAPBOX.S2P"
fileShortCircuit = "E:/PhD/1st_Year_09-10/Data/MKE-K-sept10/11-06-15/S21FREQSHORTED.S2P"

fileOldMeas = "E:/PhD/1st_Year_09-10/Data/MKE-K-sept10/MKE-S3_BeamZ/S21_274R-ATBOTHENDS_LINMAG_8127PSCORRECTEDPHASE_OCINPUT_PROPERTERMINATOROUTPUT_CAPA-BOXES_20KPTS.CSV"
fileOldMeasShort = "E:/PhD/1st_Year_09-10/Data/MKE-K-sept10/MKE-S3_BeamZ/S21_274R-ATBOTHENDS_LINMAG_8127PSCORRECTEDPHASE_OCINPUT_SCOUTPUT_CAPA-BOXES_NO-ENTRYBOXES_20KPTS.CSV"
fileOldMeasOpen = "E:/PhD/1st_Year_09-10/Data/MKE-K-sept10/MKE-S3_BeamZ/S21_274R-ATBOTHENDS_LINMAG_8127PSCORRECTEDPHASE_OCINPUT_OCOUTPUT_20KPTS.CSV"

dataTemp = []
for fileEn in fileListNoCap:
    dataTemp.append(readS21FromVNA(fileEn))

dataTransFact = sp.array(dataTemp)

dataTemp = []
for fileEn in fileListNoCapVert:
    dataTemp.append(readS21FromVNA(fileEn))

dataTransVertFact = sp.array(dataTemp)

datCapBoxOne = readS21FromVNA(fileCapBoxOne)
datCapBoxTwo = readS21FromVNA(fileCapBoxTwo)
datShortCircuit = readS21FromVNA(fileShortCircuit)
datOldMeas = readS21FromCSV(fileOldMeas)
datOldMeasShort = readS21FromCSV(fileOldMeasShort)
datOldMeasOpen = readS21FromCSV(fileOldMeasOpen)
wireDisplacementsHorz=sp.array([-0.009,-0.005,0,0.005,0.008])
wireDisplacementsVert=sp.array([-0.005,0,0.005])

longitudinalMKENoCap, totTransMKENoCap = fitQuadraticSquaresImp(dataTransFact, wireDisplacementsHorz)
longitudinalMKENoCapVert, totTransMKENoCapVert = fitQuadraticSquaresImp(dataTransVertFact, wireDisplacementsVert)
longitudinalMKECapBoxOne = []
longitudinalMKECapBoxTwo = []
longitudinalMKEShortCircuit = []
longitudinalMKEOldMeas = []
longitudinalMKEOldMeasShort = []
longitudinalMKEOldMeasOpen = []

for i in range(0,len(datCapBoxOne)):
    longitudinalMKECapBoxOne.append([datCapBoxOne[i][0], logImpFormula(logToLin(datCapBoxOne[i][3]),1,255)])

longitudinalMKECapBoxOne = sp.array(longitudinalMKECapBoxOne)

for i in range(0,len(datCapBoxTwo)):
    longitudinalMKECapBoxTwo.append([datCapBoxTwo[i][0], logImpFormula(logToLin(datCapBoxTwo[i][3]),1,255)])

longitudinalMKECapBoxTwo = sp.array(longitudinalMKECapBoxTwo)

for i in range(0,len(datShortCircuit)):
    longitudinalMKEShortCircuit.append([datShortCircuit[i][0], logImpFormula(logToLin(datShortCircuit[i][3]),1,255)])

longitudinalMKEShortCircuit = sp.array(longitudinalMKEShortCircuit)

for i in range(0,len(datOldMeas)):
##    longitudinalMKEOldMeas.append([datOldMeas[i][0], logImpFormula(logToLin(datOldMeas[i][1]),1,255)])
    longitudinalMKEOldMeas.append([datOldMeas[i][0], logImpFormula((datOldMeas[i][1]),1,255)])

for i in range(0,len(datOldMeasShort)):
##    longitudinalMKEOldMeas.append([datOldMeas[i][0], logImpFormula(logToLin(datOldMeas[i][1]),1,255)])
    longitudinalMKEOldMeasShort.append([datOldMeasShort[i][0], logImpFormula((datOldMeasShort[i][1]),1,255)])

for i in range(0,len(datOldMeasOpen)):
##    longitudinalMKEOldMeas.append([datOldMeas[i][0], logImpFormula(logToLin(datOldMeas[i][1]),1,255)])
    longitudinalMKEOldMeasOpen.append([datOldMeasOpen[i][0], logImpFormula((datOldMeasOpen[i][1]),1,255)])
    
longitudinalMKEOldMeas = sp.array(longitudinalMKEOldMeas)
longitudinalMKEOldMeasShort = sp.array(longitudinalMKEOldMeasShort)
longitudinalMKEOldMeasOpen = sp.array(longitudinalMKEOldMeasOpen)

for i in range(0,len(dataTransFact)):
    pl.plot(dataTransFact[i,:,0],dataTransFact[i,:,3])
pl.xlabel("Frequency (Hz)", fontsize=16.0)
pl.ylabel("S$_{21}$ (dB)", fontsize=16.0)
##pl.show()
pl.clf()

for i in range(0,len(dataTransFact)):
    pl.plot(dataTransFact[i,:,0],dataTransFact[i,:,3])
##pl.show()
pl.clf()
##print longitudinalMKEOldMeasOpen[1009,0]/10**9, longitudinalMKEOldMeasOpen[1010,0]/10**9
##print longitudinalMKECapBoxOne[79,0]/10**9, longitudinalMKECapBoxOne[80,0]/10**9, longitudinalMKECapBoxOne[81,0]/10**9

mixtureOldLowNewHigh = []
for i in range(0,1009):
    mixtureOldLowNewHigh.append([longitudinalMKEOldMeas[i,0], longitudinalMKEOldMeas[i,1]-longitudinalMKEOldMeas[0,1]])
for i in range(80, len(longitudinalMKECapBoxOne[:,0])):
    mixtureOldLowNewHigh.append([longitudinalMKECapBoxOne[i,0], longitudinalMKECapBoxOne[i,1]-longitudinalMKECapBoxOne[0,1]])

mixtureOldLowNewHigh = sp.array(mixtureOldLowNewHigh)

mixtureOldShortLowNewHigh = []
for i in range(0,1009):
    mixtureOldShortLowNewHigh.append([longitudinalMKEOldMeasShort[i,0], longitudinalMKEOldMeasShort[i,1]-longitudinalMKEOldMeasShort[0,1]])
for i in range(80, len(longitudinalMKECapBoxOne[:,0])):
    mixtureOldShortLowNewHigh.append([longitudinalMKECapBoxOne[i,0], longitudinalMKECapBoxOne[i,1]-longitudinalMKECapBoxOne[0,1]])

mixtureOldShortLowNewHigh = sp.array(mixtureOldShortLowNewHigh)


mixtureOldOpenLowNewHigh = []
for i in range(0,1009):
    mixtureOldOpenLowNewHigh.append([longitudinalMKEOldMeasOpen[i,0], longitudinalMKEOldMeasOpen[i,1]-longitudinalMKEOldMeasOpen[0,1]])
for i in range(80, len(longitudinalMKECapBoxOne[:,0])):
    mixtureOldOpenLowNewHigh.append([longitudinalMKECapBoxOne[i,0], longitudinalMKECapBoxOne[i,1]-longitudinalMKECapBoxOne[0,1]])

mixtureOldOpenLowNewHigh = sp.array(mixtureOldOpenLowNewHigh)

freqSpecFreq25ns, freqSpecAmp25ns = bunchTrainProfFreq(bunSpacing25ns, trainSpacing25ns, bunInTrain25ns, trainsInMachine25ns, blankBunSpaces25ns, freqRes)
freqSpecFreq50ns, freqSpecAmp50ns = bunchTrainProfFreq(bunSpacing50ns, trainSpacing50ns, bunInTrain50ns, trainsInMachine50ns, blankBunSpaces50ns, freqRes)

totLossHybrid, freqLossHybrid, cumLossHybrid = heatingValGauss(mixtureOldLowNewHigh, bCur, 25.0*10.0**-9, bLength)
temp=[]
for i in range(0,len(longitudinalMKECapBoxOne[:,0])):
    temp.append([longitudinalMKECapBoxOne[i,0], longitudinalMKECapBoxOne[i,1]-longitudinalMKECapBoxOne[0,1]])

longitudinalMKECapBoxOne=sp.array(temp)

temp=[]
for i in range(0,len(longitudinalMKEOldMeas[:,0])):
    temp.append([longitudinalMKEOldMeas[i,0], longitudinalMKEOldMeas[i,1]-longitudinalMKEOldMeas[0,1]])

longitudinalMKEOldMeas=sp.array(temp)

temp=[]
for i in range(0,len(longitudinalMKEShortCircuit[:,0])):
    temp.append([longitudinalMKEShortCircuit[i,0], longitudinalMKEShortCircuit[i,1]-longitudinalMKEShortCircuit[0,1]])

longitudinalMKEShortCircuit=sp.array(temp)

totLossOld, freqLossOld, cumLossOld = heatingValGauss(longitudinalMKEOldMeas, bCur, 25*10.0**-9, bLength)
totLossNew, freqLossNew, cumLossNew = heatingValGauss(longitudinalMKECapBoxOne, bCur, 25.0*10.0**-9, bLength)

SplineFitMeas = interp.InterpolatedUnivariateSpline(longitudinalMKECapBoxOne[:,0]/10**6, longitudinalMKECapBoxOne[:,1])
SplineFitMeasOld = interp.InterpolatedUnivariateSpline(longitudinalMKEOldMeas[:,0]/10**6, longitudinalMKEOldMeas[:,1])
SplineFitMeasOldShort = interp.InterpolatedUnivariateSpline(longitudinalMKEOldMeasShort[:,0]/10**6, longitudinalMKEOldMeasShort[:,1])
SplineFitMeasOldOpen = interp.InterpolatedUnivariateSpline(longitudinalMKEOldMeasOpen[:,0]/10**6, longitudinalMKEOldMeasOpen[:,1])
SplineFitMeasShort = interp.InterpolatedUnivariateSpline(longitudinalMKEShortCircuit[:,0]/10**6, longitudinalMKEShortCircuit[:,1])
SplineFitMeasHybrid = interp.InterpolatedUnivariateSpline(mixtureOldLowNewHigh[:,0]/10**6, mixtureOldLowNewHigh[:,1])
SplineFitMeasHybridShort = interp.InterpolatedUnivariateSpline(mixtureOldShortLowNewHigh[:,0]/10**6, mixtureOldShortLowNewHigh[:,1])
SplineFitMeasHybridOpen = interp.InterpolatedUnivariateSpline(mixtureOldOpenLowNewHigh[:,0]/10**6, mixtureOldOpenLowNewHigh[:,1])
fitDat = SplineFitMeas(freqSpecFreq25ns/10**6)
fitDatOld = SplineFitMeasOld(freqSpecFreq25ns/10**6)
fitDatOldShort = SplineFitMeasOldShort(freqSpecFreq25ns/10**6)
fitDatOldOpen = SplineFitMeasOldOpen(freqSpecFreq25ns/10**6)
fitDatShort = SplineFitMeasShort(freqSpecFreq25ns/10**6)
fitDatHybrid = SplineFitMeasHybrid(freqSpecFreq25ns/10**6)
fitDatHybridShort = SplineFitMeasHybridShort(freqSpecFreq25ns/10**6)
fitDatHybridOpen = SplineFitMeasHybridOpen(freqSpecFreq25ns/10**6)
fitDat50ns = SplineFitMeas(freqSpecFreq50ns/10**6)
fitDat50nsOld = SplineFitMeasOld(freqSpecFreq50ns/10**6)
fitDat50nsOldShort= SplineFitMeasOldShort(freqSpecFreq50ns/10**6)
fitDat50nsOldOpen = SplineFitMeasOldOpen(freqSpecFreq50ns/10**6)
fitDat50nsShort = SplineFitMeasShort(freqSpecFreq50ns/10**6)
fitDat50nsHybrid = SplineFitMeasHybrid(freqSpecFreq50ns/10**6)
fitDat50nsHybridShort = SplineFitMeasHybridShort(freqSpecFreq50ns/10**6)
fitDat50nsHybridOpen = SplineFitMeasHybridOpen(freqSpecFreq50ns/10**6)

conv = fitDat*(freqSpecAmp25ns)**2
convOld = fitDatOld*(freqSpecAmp25ns)**2
convOldShort = fitDatOldShort*(freqSpecAmp25ns)**2
convOldOpen = fitDatOldOpen*(freqSpecAmp25ns)**2
convShort = fitDatShort*(freqSpecAmp25ns)**2
convHybrid = fitDatHybrid*(freqSpecAmp25ns)**2
convHybridShort = fitDatHybridShort*(freqSpecAmp25ns)**2
convHybridOpen = fitDatHybridOpen*(freqSpecAmp25ns)**2
##print (freqSpecFreq25ns[1]-freqSpecFreq25ns[0])
##print (2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0]), len(freqSpecFreq25ns)
##print (f_rev*qPart*nPart*bunInTrain25ns*trainsInMachine25ns)**2*sp.sum(conv[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])])
##print (f_rev*qPart*nPart*bunInTrain25ns*trainsInMachine25ns)**2*sp.sum(convOld[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])])
##print (f_rev*qPart*nPart*bunInTrain25ns*trainsInMachine25ns)**2*sp.sum(convOldShort[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])])
##print (f_rev*qPart*nPart*bunInTrain25ns*trainsInMachine25ns)**2*sp.sum(convOldOpen[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])])
##print (f_rev*qPart*nPart*bunInTrain25ns*trainsInMachine25ns)**2*sp.sum(convShort[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])])
##print (f_rev*qPart*nPart*bunInTrain25ns*trainsInMachine25ns)**2*sp.sum(convHybrid[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])])
##print (f_rev*qPart*nPart*bunInTrain25ns*trainsInMachine25ns)**2*sp.sum(convHybridShort[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])])
##print (f_rev*qPart*nPart*bunInTrain25ns*trainsInMachine25ns)**2*sp.sum(convHybridOpen[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])])

conv50ns = fitDat50ns*(freqSpecAmp50ns)**2
conv50nsOld = fitDat50nsOld*(freqSpecAmp50ns)**2
conv50nsOldShort = fitDat50nsOldShort*(freqSpecAmp50ns)**2
conv50nsOldOpen = fitDat50nsOldOpen*(freqSpecAmp50ns)**2
conv50nsShort = fitDat50nsShort*(freqSpecAmp50ns)**2
conv50nsHybrid = fitDat50nsHybrid*(freqSpecAmp50ns)**2
conv50nsHybridShort = fitDat50nsHybridShort*(freqSpecAmp50ns)**2
conv50nsHybridOpen = fitDat50nsHybridOpen*(freqSpecAmp50ns)**2
##print (freqSpecFreq50ns[1]-freqSpecFreq50ns[0])
##print (2.0*10**9)/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0]), len(freqSpecFreq50ns)
##print (f_rev*qPart*nPart*bunInTrain50ns*trainsInMachine50ns)**2*sp.sum(conv50ns[:int(2.0*10**9)/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0])])
##print (f_rev*qPart*nPart*bunInTrain50ns*trainsInMachine50ns)**2*sp.sum(conv50nsOld[:int(2.0*10**9)/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0])])
##print (f_rev*qPart*nPart*bunInTrain50ns*trainsInMachine50ns)**2*sp.sum(conv50nsOldShort[:int(2.0*10**9)/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0])])
##print (f_rev*qPart*nPart*bunInTrain50ns*trainsInMachine50ns)**2*sp.sum(conv50nsOldOpen[:int(2.0*10**9)/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0])])
##print (f_rev*qPart*nPart*bunInTrain50ns*trainsInMachine50ns)**2*sp.sum(conv50nsShort[:int(2.0*10**9)/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0])])
##print (f_rev*qPart*nPart*bunInTrain50ns*trainsInMachine50ns)**2*sp.sum(conv50nsHybrid[:int(2.0*10**9)/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0])])
##print (f_rev*qPart*nPart*bunInTrain50ns*trainsInMachine50ns)**2*sp.sum(conv50nsHybridShort[:int(2.0*10**9)/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0])])
##print (f_rev*qPart*nPart*bunInTrain50ns*trainsInMachine50ns)**2*sp.sum(conv50nsHybridOpen[:int(2.0*10**9)/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0])])

temp = sp.sum(conv[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])])
print "test"
print (f_rev*qPart*nPart*bunInTrain25ns*trainsInMachine25ns)**2*sp.sum(conv[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])])
print temp*(f_rev*qPart*nPart*bunInTrain25ns*trainsInMachine25ns)**2

##pl.loglog()
##pl.semilogy()
##pl.semilogx()
##pl.plot(longitudinalMKENoCap[:,0]/10**9,longitudinalMKENoCap[:,1]-longitudinalMKENoCap[0,1], linewidth=1.5, label="New Serigraphy, Open")
####pl.plot(longitudinalMKENoCapVert[:,0]/10**9,longitudinalMKENoCapVert[:,1]-longitudinalMKENoCapVert[0,1], label="Constant Term from fitting No Load")
pl.plot(longitudinalMKECapBoxOne[:,0]/10**9,longitudinalMKECapBoxOne[:,1], linewidth=1.5, label="New Serigraphy, Terminated")
##pl.plot(longitudinalMKECapBoxTwo[:,0]/10**9,longitudinalMKECapBoxTwo[:,1]-longitudinalMKECapBoxTwo[0,1], label="New Serigraphy, Cap Box 2")
pl.plot(longitudinalMKEShortCircuit[:,0]/10**9,longitudinalMKEShortCircuit[:,1]-longitudinalMKEShortCircuit[0,1], linewidth=1.5, label="New Serigraphy, Short Circuit")
pl.plot(longitudinalMKEOldMeas[:,0]/10**9,longitudinalMKEOldMeas[:,1], linewidth=1.5, label="Old Serigraphy, Old Meas Terminated")
pl.plot(longitudinalMKEOldMeasShort[:,0]/10**9,longitudinalMKEOldMeasShort[:,1], "k-", linewidth=1.5,label="Old Serigraphy, Old Meas Short")
##pl.plot(longitudinalMKEOldMeasOpen[:,0]/10**9,longitudinalMKEOldMeasOpen[:,1], linewidth=1.5, label="Old Serigraphy, Old Meas Open")
##pl.plot(mixtureOldLowNewHigh[:,0]/10**9,mixtureOldLowNewHigh[:,1], label="Hybrid")
pl.xlabel("Frequency (GHz)", fontsize=20.0)
pl.ylabel("Z$_{\parallel}$ ($\Omega$)", fontsize=20.0)
pl.legend(loc="upper left")
pl.xlim(0,0.08)
pl.ylim(0,3000)
##pl.show()
pl.clf()

pl.plot(freqSpecFreq25ns/10**6,fitDat)
##pl.plot(longitudinalMKECapBoxOne[:,0]/10**6, longitudinalMKECapBoxOne[:,1])
##pl.plot(freqSpecFreq25ns[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])]/10**6,conv[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])])
pl.xlim(0,1000)
pl.ylim(0,2000)
##pl.show()
pl.clf()

pl.plot(totTransMKENoCap[:,0]/10**9,totTransMKENoCap[:,1]/10**3)
##pl.plot(totTransMKENoCapVert[:,0]/10**9,totTransMKENoCapVert[:,1]/10**3)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("Z$_{\perp}$$^{Total}$ (k$\Omega$/m)", fontsize=16.0)
##pl.show()
pl.clf()

pl.plot(freqSpecFreq25ns/10**9, freqSpecAmp25ns)
pl.xlim(0,2.0)
##pl.show()
pl.clf()

##pl.plot(freqLossHybrid[:,0]/10**9,freqLossHybrid[:,1], label="Hybrid")
##pl.plot(freqLossOld[:,0]/10**9,freqLossOld[:,1], label="Old")
##pl.plot(freqLossNew[:,0]/10**9,freqLossNew[:,1], label="New")
##pl.plot(freqSpecFreq25ns[:int(2.0*10**9/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0]))]/10**9, (f_rev*qPart*nPart*bunInTrain25ns*trainsInMachine25ns)**2*(conv[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])])))
##pl.plot(freqSpecFreq50ns[:int(2.0*10**9/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0]))]/10**9, (f_rev*qPart*nPart*bunInTrain50ns*trainsInMachine50ns)**2*(conv50ns[:int(2.0*10**9)/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0])]), label="New, Matched")
##pl.plot(freqSpecFreq50ns[:int(2.0*10**9/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0]))]/10**9, (f_rev*qPart*nPart*bunInTrain50ns*trainsInMachine50ns)**2*(conv50nsOld[:int(2.0*10**9)/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0])]), label="Old, Matched")
##pl.plot(freqSpecFreq50ns[:int(2.0*10**9/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0]))]/10**9, (f_rev*qPart*nPart*bunInTrain50ns*trainsInMachine50ns)**2*(conv50nsOldShort[:int(2.0*10**9)/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0])]), label="Old, Short")
##pl.plot(freqSpecFreq50ns[:int(2.0*10**9/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0]))]/10**9, (f_rev*qPart*nPart*bunInTrain50ns*trainsInMachine50ns)**2*(conv50nsOldOpen[:int(2.0*10**9)/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0])]), label="Old, Open")
##pl.plot(freqSpecFreq50ns[:int(2.0*10**9/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0]))]/10**9, (f_rev*qPart*nPart*bunInTrain50ns*trainsInMachine50ns)**2*(conv50nsShort[:int(2.0*10**9)/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0])]), label="New, Short")
##pl.plot(freqSpecFreq50ns[:int(2.0*10**9/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0]))]/10**9, (f_rev*qPart*nPart*bunInTrain50ns*trainsInMachine50ns)**2*(conv50nsHybrid[:int(2.0*10**9)/(freqSpecFreq50ns[1]-freqSpecFreq50ns[0])]), "k-", label="Hybrid, Matched")

pl.plot(freqSpecFreq25ns[:int(2.0*10**9/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0]))]/10**9, (f_rev*qPart*nPart*bunInTrain25ns*trainsInMachine25ns)**2*(conv[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])]), label="New, Matched")
pl.plot(freqSpecFreq25ns[:int(2.0*10**9/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0]))]/10**9, (f_rev*qPart*nPart*bunInTrain25ns*trainsInMachine25ns)**2*(convOld[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])]), label="Old, Matched")
pl.plot(freqSpecFreq25ns[:int(2.0*10**9/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0]))]/10**9, (f_rev*qPart*nPart*bunInTrain25ns*trainsInMachine25ns)**2*(convShort[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])]), label="New, Short")
pl.plot(freqSpecFreq25ns[:int(2.0*10**9/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0]))]/10**9, (f_rev*qPart*nPart*bunInTrain25ns*trainsInMachine25ns)**2*(convHybrid[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])]), "k-", label="Hybrid, Matched")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$P_{loss}$ (W)", fontsize=16.0)
pl.legend()
pl.xlim(0.058,0.062)
pl.ylim(0,35)
pl.show()
pl.clf()

pl.plot(cumLossHybrid[:,0]/10**9,cumLossHybrid[:,1], label="Hybrid")
pl.plot(cumLossOld[:,0]/10**9,cumLossOld[:,1], label="Old")
pl.plot(cumLossNew[:,0]/10**9,cumLossNew[:,1], 'rx',label="New")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$P_{loss}$ (W)", fontsize=16.0)
pl.legend()
pl.show()
pl.clf()
