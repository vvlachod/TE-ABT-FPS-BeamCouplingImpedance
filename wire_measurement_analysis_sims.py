import csv, os, sys
import scipy as sp
import pylab as pl
from scipy import optimize as op

fitfunc = lambda p, x: p[0] + p[1]*x + p[2]*x**2
errfunc = lambda p, x, y, err: (y-fitfunc(p, x))/err

distance_acc = 0.000001
imp_err = distance_acc/0.003

C = 3.0*10**8
Z0=377.0
##print cos_phi_bt
##print cos_phi_at
##print sp.arccos(cos_phi_bt)
##print sp.arccos(cos_phi_at)
length_total = 0.05
length_imp = 0.05
wire_sep = 0.003

scan = "horz"
directory = "E:/PhD/1st_Year_09-10/Data/frequency-dom-test/data/project_new_Aug_2011/" #Directory of data
if scan == "vert":
    data_real = directory+"y_scan_all_real.csv"
    data_imag = directory+"y_scan_all_imag.csv"
    dipole_real = directory+"y_dip_all_real.csv"
    dipole_imag = directory+"y_dip_all_imag.csv"
elif scan == "horz":
    data_real = directory+"x_scan_all_real.csv"
    data_imag = directory+"x_scan_all_imag.csv"
    dipole_real = directory+"x_dip_all_real.csv"
    dipole_imag = directory+"x_dip_all_imag.csv"
r_wire = 0.0003
b_1 = 0.005
r_dip = 0.0003
r_sep = 0.004
r_sep_eff = (r_sep**2-r_dip**2)**0.5
Z_c = 60*sp.log(1.27*b_1/r_wire)

data_list_real = []

##input_file = open(data_real, 'r+')
##linelist = input_file.readlines()
##input_file.close()       
##for row in linelist[1:5]:
##    row=map(float, row.rsplit(','))
##    lin_mag = sp.log(10**(sp.array(row[2:-1])/20))
##    data_list_real.append(list(sp.hstack((sp.array(row[0]*10**9), -2*Z_c*lin_mag/length_imp))))
##
##
##for row in linelist[5:]:
##    row=map(float, row.rsplit(','))
##    lin_mag = sp.log(10**(sp.array(row[2:-1])/20))
##    data_list_real.append(list(sp.hstack((sp.array(row[0]*10**9), -2*Z_c*lin_mag/length_imp))))
##
##
##data_list_real = sp.array(data_list_real)
##
##data_list_imag = []
##
##input_file = open(data_imag, 'r+')
##linelist = input_file.readlines()
##input_file.close()       
##for row in linelist[1:5]:
##    row=map(float, row.split(','))
##    s_21_pec = 2*sp.pi*row[0]*10**9*length_total/C
##    data_list_imag.append(sp.hstack((sp.array(row[0]*10**9), -2*Z_c/length_imp*(sp.radians(row[2:-1])+s_21_pec))))
##
##for row in linelist[5:]:
##    row=map(float, row.split(','))
##    s_21_pec = 2*sp.pi*row[0]*10**9*length_total/C
##    data_list_imag.append(sp.hstack((sp.array(row[0]*10**9), -2*Z_c/length_imp*(sp.radians(row[2:-1])+s_21_pec))))
##
##
##data_list_imag = sp.array(data_list_imag)
data_list_dip = []

##pl.loglog()
##pl.plot(data_list_real[:,0], data_list_real[:,5])
##pl.plot(data_list_imag[:,0], data_list_imag[:,5])
##pl.axis([10**0,10**9,10**-5,10**1])
##pl.show()
##pl.clf()
Z_c = 120*sp.arccosh(r_sep_eff/(2*r_dip))

data_dipole_real=[]
input_file = open(dipole_real, 'r+')
linelist = input_file.readlines()
input_file.close()       
for row in linelist[0:]:
    row=map(float, row.rsplit(','))
    lin_mag = sp.log(10**(sp.array(row[1:])/20))
    data_dipole_real.append(list(sp.hstack((sp.array(row[0]*10**9), -2*Z_c*lin_mag*C/length_imp/(r_sep_eff**2*2*sp.pi*row[0]*10**9)))))

data_dipole_real=sp.array(data_dipole_real)

data_dipole_imag=[]
input_file = open(dipole_imag, 'r+')
linelist = input_file.readlines()
input_file.close()       
for row in linelist[0:]:
    row=map(float, row.split(','))
    s_21_pec = 2*sp.pi*row[0]*10**9*length_total/C
    data_dipole_imag.append(sp.hstack((sp.array(row[0]*10**9), -2*Z_c*(sp.radians(row[1:])+s_21_pec)*C/length_imp/(r_sep_eff**2*2*sp.pi*row[0]*10**9))))

data_dipole_imag = sp.array(data_dipole_imag)

##pl.loglog()
##pl.plot(data_dipole_real[:,0], data_dipole_real[:,1])
##pl.plot(data_dipole_imag[:,0], data_dipole_imag[:,1])
####pl.axis([10**0,10**9,10**0,10**8])
##pl.show()
##pl.clf()

x_data = sp.linspace(-4, 4, 9)
##x_data = sp.array([-0.7,-0.5,0,0.5,0.7])
x_data=x_data*10**-3
coefficients = []

tar_dir_real=(directory+scan+"_real/")
tar_dir_imag=(directory+scan+"_imag/")
try:
    os.mkdir(tar_dir_real)

except:
    pass

try:
    os.mkdir(tar_dir_imag)

except:
    pass

##print data_list_real
##print data_list_imag

##trans_imp = []
##long_imp = []
##
for i in range(0,len(data_list_real)):
    pinit = [1.0, 1.0, 1.0]
    y_err = data_list_real[i,1]*imp_err
    print len(x_data), len(data_list_real[i,1:]), len(y_err)
    out = op.leastsq(errfunc, pinit, args=(x_data,data_list_real[i,1:],y_err), full_output=1)
    pfinal_real = out[0]
    covar_real=out[1]
##    x_plot = sp.linspace(-10,10,1000)*10**-3
##
##    y_test = fitfunc(pfinal_real, x_plot)
##    pl.plot(x_plot, y_test, label= "Fitted Function")
##    pl.plot(x_data, data_list_real[i,1:],'kx', markersize=16,label='Simulated Points')
####    pl.errorbar(x_data,data_list_real[i,1:],yerr=y_err, fmt='ko',label = "Measured Data") 
##    pl.legend(loc='upper left')
##    pl.xlabel('Displacement (mm)', fontsize = 16)
##    pl.ylabel('Longitudinal Impedance (Ohms)', fontsize = 16)
###   pl.show()
##    pl.savefig(tar_dir_real+str(i)+".eps")
##    pl.clf()
##    
##    pinit = [1.0, 1.0, 1.0]
##    y_err = data_list_imag[i,1]*imp_err
##    out = op.leastsq(errfunc, pinit, args=(x_data,data_list_imag[i,1:],y_err), full_output=1)
##    pfinal_imag = out[0]
##    covar_imag=out[1]
##    x_plot = sp.linspace(-10,10,1000)*10**-3
##    y_test = fitfunc(pfinal_imag, x_plot)
##    pl.plot(x_plot, y_test, label= "Fitted Function")
##    pl.plot(x_data,data_list_imag[i,1:],'rx', markersize=16, label='Simulated Points')
####    pl.errorbar(x_data,data_list_imag[i,1:],yerr=y_err, fmt='ko',label = "Measured Data") 
##    pl.legend(loc='upper left')
##    pl.xlabel('Displacement (mm)', fontsize = 16)
##    pl.ylabel('Longitudinal Impedance (Ohms)', fontsize = 16)
###   pl.show()
##    pl.savefig(tar_dir_imag+str(i)+".eps")
##    pl.clf()
##    
##    trans_imp.append([pfinal_real[2]*(C)/(2*sp.pi*data_dipole_real[i,0]),pfinal_imag[2]*C/(2*sp.pi*data_dipole_real[i,0])])
##    long_imp.append([pfinal_real[0],pfinal_imag[0]])
##
##long_imp=sp.array(long_imp)
##trans_imp=sp.array(trans_imp)



dipole_theory_input = directory+"phase_1_moly_60mm_dip_"+scan+".csv"
dipole_theory=[]
input_file = open(dipole_theory_input, 'r+')
linelist = input_file.readlines()
input_file.close()       
for row in linelist:
    dipole_theory.append(map(float, row.split(',')))

dipole_theory=sp.array(dipole_theory)

long_theory_input = directory+"phase_1_moly_60mm_long.csv"
long_theory=[]
input_file = open(long_theory_input, 'r+')
linelist = input_file.readlines()
input_file.close()       
for row in linelist:
    long_theory.append(map(float, row.split(',')))

long_theory=sp.array(long_theory)

quad_theory_input = directory+"phase_1_moly_60mm_quad_"+scan+".csv"
quad_theory=[]
input_file = open(quad_theory_input, 'r+')
linelist = input_file.readlines()
input_file.close()       
for row in linelist:
    quad_theory.append(map(float, row.split(',')))

quad_theory=sp.array(dipole_theory)

##pl.loglog()
##pl.plot(data_list_real[:,0],long_imp[:,0], 'kx',markersize=12,label="Real Impedance Sims")
##pl.plot(data_list_imag[:,0],long_imp[:,1], 'rx',markersize=12,label='Imaginary Impedance Sims')
##pl.plot(long_theory[:,0],long_theory[:,1], 'k-',label="Real Impedance Theory")
##pl.plot(long_theory[:,0],long_theory[:,2], 'r-',label='Imaginary Impedance Theory')
##pl.xlabel('Frequency (Hz)', fontsize = 16)
##pl.ylabel('Longitudinal Impedance ($\Omega/m$)', fontsize = 16)
##pl.legend(loc="upper left")
##pl.figtext(0.2,0.8,'240,000 mesh cells', size=12.0, bbox={'facecolor':'white', 'pad':10})
##pl.axis([10**5,2*10**9,10**-1,10**2])
####pl.show()
##pl.savefig(tar_dir_real+"long-"+scan+".eps")
##pl.clf()
##
##output_file = open(tar_dir_real+"long.csv", 'w+')           #Assign and open output file
##for i in range(0,len(data_list_real)):
##              output_file.write(str(data_dipole_real[i,0])+','+str(long_imp[i,0])+','+str(long_imp[i,1])+'\n')
##
##output_file.close() 

pl.loglog()
pl.plot(data_dipole_real[:,0],data_dipole_real[:,1], 'kx',markersize=12,label="Real Impedance Sims")
pl.plot(data_dipole_imag[:,0],abs(data_dipole_imag[:,1]), 'rx',markersize=12,label='Imaginary Impedance Sims')
pl.plot(dipole_theory[:,0],dipole_theory[:,1], 'k-',label="Real Impedance Theory")
pl.plot(dipole_theory[:,0],dipole_theory[:,2], 'r-',label='Imaginary Impedance Theory')
pl.xlabel('Frequency (Hz)', fontsize = 16)
pl.ylabel('Dipolar Impedance ($\Omega/m^{2}$)', fontsize = 16)
##pl.legend(loc="loqer right")
##pl.figtext(0.2,0.8,'130,000 mesh cells', size=12.0, bbox={'facecolor':'white', 'pad':10})
##pl.show()
pl.axis([10**5,2*10**9,10**3,10**6])
pl.savefig(tar_dir_real+"dipolar-"+scan+".eps")
pl.savefig(tar_dir_real+"dipolar-"+scan+".pdf")
pl.clf()

output_file = open(tar_dir_real+"dipolar-"+scan+".csv", 'w+')           #Assign and open output file
for i in range(0,len(data_list_real)):
              output_file.write(str(data_dipole_real[i,0])+','+str(data_dipole_real[i,1])+','+str(data_dipole_imag[i,1])+'\n')

output_file.close() 

##
##for i in range(0,len(trans_imp)):
##    trans_imp[i,0]= trans_imp[i,0]*C/(data_dipole_real[i,0])
##    trans_imp[i,1]= trans_imp[i,1]*C/(data_dipole_real[i,0])
##
##pl.plot(data_array[0,:,0], trans_imp[:,0],'k-',label="Real Impedance")
##pl.plot(data_array[0,:,0], trans_imp[:,1],'r-', label="Imaginary Impedance")
####pl.axis([0,2*10**9,-10**6,10**6])
##pl.xlabel('Frequency (Hz)', fontsize = 16)
##pl.ylabel('Total Transverse Impedance (Ohms/m)', fontsize = 16)
##pl.legend()
####pl.show()
##pl.savefig(tar_dir_real+"total_transverse_"+scan+".pdf")
##pl.clf()

##output_file = open(tar_dir_real+"total_trans_"+scan+".csv", 'w+')           #Assign and open output file
##for i in range(0,2000):
##              output_file.write(str(data_array[0,i,0])+','+str(trans_imp[i,0])+','+str(trans_imp[i,1])+'\n')
##
##output_file.close() 


##imp_quad = []
##if scan=="horz":
##    for i in range(0,len(trans_imp)):
##        imp_quad.append([-float(trans_imp[i,0])+float(data_dipole_real[i,1]),-float(trans_imp[i,1])+float(data_dipole_imag[i,1])])
##    imp_quad=sp.array(imp_quad)
##
##elif scan=="vert":
##    for i in range(0,len(trans_imp)):
##        imp_quad.append([float(trans_imp[i,0])-float(data_dipole_real[i,1]),float(trans_imp[i,1])-float(data_dipole_imag[i,1])])
##    imp_quad=sp.array(imp_quad)
##    
##
##pl.loglog()
##pl.plot(data_list_real[:,0], imp_quad[:,0]/2.5, 'kx', markersize=12,label="Real Impedance Sims")
##pl.plot(data_list_imag[:,0], imp_quad[:,1]/2.5, 'rx', markersize=12,label="Imaginary Impedance Sims")
##pl.plot(quad_theory[:,0], quad_theory[:,1], 'k-', label="Real Impedance Theory")
##pl.plot(quad_theory[:,0], quad_theory[:,2], 'r-', label="Imagunary Impedance Theory")
##pl.axis([10**3,2*10**9,10**4,10**8])
####pl.show()
##pl.xlabel('Frequency (Hz)', fontsize = 16)
##pl.ylabel('Quadrupolar Impedance ($\Omega/m^{2}$)', fontsize = 16)
##pl.legend(loc="upper right")
##pl.figtext(0.2,0.8,'390,000 mesh cells', size=12.0, bbox={'facecolor':'white', 'pad':10})
####pl.show()
##pl.axis([10**3,2*10**9,10**4,10**8])
##pl.savefig(tar_dir_real+"quad-"+scan+".eps")
##pl.clf()
##
##
##output_file = open(tar_dir_real+"quad_"+scan+".csv", 'w+')           #Assign and open output file
##for i in range(0,len(data_list_real)):
##              output_file.write(str(data_list_real[i,0])+','+str(imp_quad[i,0])+','+str(imp_quad[i,1])+'\n')
##
##output_file.close() 
####
####pl.loglog()
####pl.plot(data_list_real[:,0], trans_imp[:,0],'kx')
####pl.plot(data_list_real[:,0], trans_imp[:,1],'rx')
####pl.show()
####pl.clf()
