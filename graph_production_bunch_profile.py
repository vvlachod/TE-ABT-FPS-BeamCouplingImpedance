import csv, os
import scipy as sp
import pylab as pl
import images2swf
import Image, time

start = time.time()

directory = "E:/PhD/1st_Year_09-10/Software/headtail/data_dump/hdtl-data/" #Directory of data
name = "hdtl-bt-BB-10-SC-100"
list_bt_no_sc = [directory+name+".NumPar."+str(i)+"_bunchds.dat" for i in range(1,301)]


store = "temp_file_test.csv"
plot_dir = directory+"plots/"+name+"/"
try:
    os.mkdir(plot_dir)

except:
    pass
    
file_out = open(directory+store, 'w+')
data_out = csv.writer(file_out, delimiter=',', lineterminator = '\n')

big_bugger =[]
data_list_1 = []
for data_raw in list_bt_no_sc:
    input_file = open(data_raw, 'r+')
    tar = input_file.readlines()
    while '\n' in tar:
        tar.remove('\n')
    for i in range(0,len(tar)):
        tar[i] = tar[i].rstrip('\n')
        tar[i] = tar[i].split()
    sep_parts = []
    sep_parts.append(tar[-128:])
    big_bugger.append(sep_parts)


big_bugger = sp.array(big_bugger)
image_list = []
for i in range(0,300):
    pl.plot(big_bugger[i,0,:,0], big_bugger[i,0,:,1], 'k-')
    pl.grid(linestyle="--", which = "major")
    pl.xlabel("$\sigma_{z}$",fontsize = 16)                  #Label axes
    pl.ylabel("Occupation",fontsize = 16)
    pl.title("Bunch intensity = "+str(i+1)+'E9', fontsize = 16)
    ##pl.legend(loc = "upper left")
    pl.axis([-1.5,1.5,0,200000])
    pl.savefig(plot_dir+str(i)+".png")                  # Save fig for film
    ##pl.show()
    pl.clf()


##os.chdir(plot_dir)    
##os.system("del *.png")
    

print time.time()-start



