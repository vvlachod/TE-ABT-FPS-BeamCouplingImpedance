import scipy as sp
import pylab as pl
import time, csv, sys, os
import matplotlib.pyplot as plt
import scipy.integrate as inte

########################## Imported Profile ##################################

start = time.time()

#######   SPS parameters for LHC type beam  #######

###### Define constants ####
charge = 18.4*10**-9
sigma_z = 0.04
C = 299792458.0
eps0 = 8.854 * 10 **-12
mu0 = 4*sp.pi*10**-7
Z0 = 120*sp.pi
circ=6911.0
t_bunch = 25.0*10**-9
n_bunch = 288
Nb = 1.15*10**11
q_part = 1.6*10**-19
rpw = 0.05
E0 = 2.0**0.5

def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def gauss_prof(dist, sigma, order):
    return sp.e**(-order*(dist/(2*sigma))**2)

def para_prof(dist, bunch_length):
    return 1-(2*dist/bunch_length)**2

def cos_prof(dist, bunch_length):
    return sp.cos(sp.pi/bunch_length*dist)**2

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp


topDir = "E:/PhD/1st_Year_09-10/Data/ferr_cav/eigenmode_results/simple_cavity_ferrite/"
freqFile = topDir+"freq.csv"
qFile = topDir+"q_1.csv"
roverqFile = topDir+"r_over_q.csv"
totSurfLosses = topDir+"surface_losses_hfss.csv"
vacSurfLosses = topDir+"vacuum_surface_losses_hfss.csv"
ferrSurfLosses = topDir+"ferr_surface_losses_hfss.csv"
totVolLosses = topDir+"total_volume_losses_hfss.csv"
vacVolLosses = topDir+"vacuum_volume_losses_hfss.csv"
ferrVolLosses = topDir+"ferr_volume_losses_hfss.csv"
squarePot = topDir+"square_potential_on_axis.csv"

freqStr = open(freqFile, 'r+')
qStr = open(qFile, 'r+')
roverqStr = open(roverqFile, 'r+')
totSurfLossesStr = open(totSurfLosses, 'r+')
vacSurfLossesStr = open(vacSurfLosses, 'r+')
ferrSurfLossesStr = open(ferrSurfLosses, 'r+')
totVolLossesStr = open(totVolLosses, 'r+')
vacVolLossesStr = open(vacVolLosses, 'r+')
ferrVolLossesStr = open(ferrVolLosses, 'r+')
squarePotStr = open(squarePot, 'r+')

freqList = freqStr.readlines()
for entry in freqList[1:]:
    freqStore = map(float, entry.rsplit(','))

freqStore = freqStore[1:12]

qList = qStr.readlines()
for entry in qList[1:2]:
    qStore = map(float, entry.rsplit(','))

qStore = qStore[1:]

roverqList = roverqStr.readlines()
for entry in roverqList[1:2]:
    roverqStore = map(float, entry.rsplit(','))

roverqStore = roverqStore[1:]

totSurfLossesList = totSurfLossesStr.readlines()
for entry in totSurfLossesList[1:2]:
    totSurfLossesStore = map(float, entry.rsplit(','))

totSurfLossesStore = totSurfLossesStore[1:]

vacSurfLossesList = vacSurfLossesStr.readlines()
for entry in vacSurfLossesList[1:2]:
    vacSurfLossesStore = map(float, entry.rsplit(','))

vacSurfLossesStore = vacSurfLossesStore[1:]

ferrSurfLossesList = ferrSurfLossesStr.readlines()
for entry in ferrSurfLossesList[1:2]:
    ferrSurfLossesStore = map(float, entry.rsplit(','))

ferrSurfLossesStore = ferrSurfLossesStore[1:]

totVolLossesList = totVolLossesStr.readlines()
for entry in totVolLossesList[1:2]:
    totVolLossesStore = map(float, entry.rsplit(','))

totVolLossesStore = totVolLossesStore[1:]

vacVolLossesList = vacVolLossesStr.readlines()
for entry in vacVolLossesList[1:2]:
    vacVolLossesStore = map(float, entry.rsplit(','))

vacVolLossesStore = vacVolLossesStore[1:]

ferrVolLossesList = ferrVolLossesStr.readlines()
for entry in ferrVolLossesList[1:2]:
    ferrVolLossesStore = map(float, entry.rsplit(','))

ferrVolLossesStore = ferrVolLossesStore[1:]

squarePotList = squarePotStr.readlines()
for entry in squarePotList[1:2]:
    squarePotStore = map(float, entry.rsplit(','))

squarePotStore = squarePotStore[1:]

ferrMuDoubPrime = sp.linspace(0, 0.2, num=11)

eigenStore = []
for i in range(0,len(freqStore)):
    eigenStore.append([freqStore[i], qStore[i], qStore[i]*roverqStore[i]])

powerStore = []
for entry in eigenStore:
    powerStore.append((Nb*q_part*n_bunch*C/circ)**2*entry[2]*sp.e**(-(2*sp.pi*entry[0]*sigma_z/C)**2))


freqStore = sp.array(freqStore)
qStore= sp.array(qStore)
roverqStore= sp.array(roverqStore)
totSurfLossesStore = sp.array(totSurfLossesStore)
vacSurfLossesStore = sp.array(vacSurfLossesStore)
ferrSurfLossesStore = sp.array(ferrSurfLossesStore)
totVolLossesStore = sp.array(totVolLossesStore)
vacVolLossesStore = sp.array(vacVolLossesStore)
ferrVolLossesStore = sp.array(ferrVolLossesStore)
squarePotStore = sp.array(squarePotStore)

ferrPerLosses = (ferrVolLossesStore)*100/(totSurfLossesStore+totVolLossesStore)
powerStore = sp.array(powerStore)
powerFerrStore = powerStore*ferrPerLosses/100
normPow = squarePotStore/(qStore*roverqStore)
totPow = totSurfLossesStore+totVolLossesStore
##print eigenStore
##print ferrMuDoubPrime
print freqStore
##print qStore
##print roverqStore
print qStore*roverqStore
##print totSurfLossesStore
##print vacSurfLossesStore
##print ferrSurfLossesStore
##print totVolLossesStore
##print vacVolLossesStore
##print ferrVolLossesStore

##print normPow/2
##print totPow

################# Importing Eigenmode Data ####################

########### Import and produce data for frequency domain ###########


top_directory_in = "E:/PhD/1st_Year_09-10/Data/ferr_cav/eigenmode_results/simple_cavity/"
##top_directory_out = "C:/Users/hugo/PhD/Data/UA9-Goniometer/eigenmode-results/parked_out_new/"
eigenmode_store = []
directory_list = [top_directory_in+i+"/" for i in os.listdir(top_directory_in)]
##print directory_list
count = 0
try:
    os.mkdir(top_directory_in+"tex_output/")
    os.mkdir(top_directory_out+"tex_output/")

except:
    pass

for work_directory in directory_list:

    if work_directory == top_directory_in+"tex_output/":
        pass
    elif work_directory == top_directory_in+"test/":
        pass
    elif work_directory == top_directory_in+"test_ferrite/":
        pass
    elif work_directory == top_directory_in+"mode_2/":
        pass
    else:
##        print work_directory
        file_input = open(work_directory+"e_field_on_axis.fld", 'r+')
        data = file_input.readlines()
        Ez = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ez.append([temp[2],complex(temp[-2],temp[-1])])

        temp = []
        for i in range(0,len(Ez)):
            temp.append([Ez[i][0],complex((Ez[i][1]).real,(Ez[i][1]).imag)])
        Ez = sp.array(temp)

##        print Ez

        potential = 0.0
        potential_alt = 0.0
##        print work_directory
        gap = (Ez[1][0]-Ez[0][0])
        for i in range(0, len(Ez)-1):
            potential_alt += 0.5*abs(Ez[i+1,1]+Ez[i,1])*gap
        potential = inte.simps(abs(Ez[:,1]), Ez[:,0], gap)
        if count == 0:
            pl.plot(Ez[:,0], Ez[:,1])
##            pl.show()
            pl.clf()
            count+=1

        file_input = open(work_directory+"q_1.csv", 'r+')
        data = file_input.readlines()
        Q = []
        for i in data[1:]:
            Q.append(map(float, i.rsplit(',')))

        Q = sp.array(Q)
        ##print Q

        file_input = open(work_directory+"stored_energy.csv", 'r+')
        data = file_input.readlines()
        stored_energy = []
        for i in data[1:]:
            stored_energy.append(map(float, i.rsplit(',')))

        ##print stored_energy

        file_input = open(work_directory+"freq.csv", 'r+')
        data = file_input.readlines()
        freq = []
        for i in data[1:]:
            temp = map(float, i.rsplit(','))
            freq.append([temp[0],complex(temp[2],temp[1])])

        freq = sp.array(freq)

        gap = Ez[1,0]-Ez[0,0]
        confluence = []
        for i in range(0,len(Ez)):
            confluence.append(Ez[i,1]*complex(sp.cos(freq[0,1]*2*sp.pi*Ez[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ez[i,0]/C)))

        potential = abs(inte.simps(confluence, Ez[:,0], gap))

        long_r_over_q = (abs(potential)**2/(2*sp.pi*freq[0,1]*stored_energy[0][1]))
    
        long_r_over_q_alt = abs(inte.simps(confluence, Ez[:,0], gap))**2/(2*2*sp.pi*freq[0,1]*stored_energy[0][1])
        other  = abs(inte.simps(confluence, Ez[:,0], gap))**2/(long_r_over_q_alt*Q[0,1]*2)
        print freq[0,1], Q[0,1], long_r_over_q_alt, long_r_over_q_alt*Q[0,1], other
        eigenmode_store.append([freq[0,1], Q[0,1], long_r_over_q_alt*Q[0,1]])


impStore = []

freqListEig = []
for i in range(10000,30001,1):
    freqListEig.append(float(i)*10.0**9/10000.0)
freqListEig = sp.array(freqListEig)

for i in freqListEig:
    total = 0.0
    for entry in eigenmode_store:
        total+=Z_bb(i, entry)
    impStore.append([total.real, total.imag])

impStore = sp.array(impStore)

impStoreFerr = []

for i in freqListEig:
    temp = []
    for entry in eigenStore:
        tot = Z_bb(i, entry)
        temp.append([tot.real, tot.imag])
    impStoreFerr.append(temp)

impStoreFerr = sp.array(impStoreFerr)

###### Plot Longitudinal Impedance

pl.semilogy()
for i in range(0,11):
    pl.plot(freqListEig/10**9, impStoreFerr[:,i,0], label = "$\mu^{''}/\mu^{'}=$"+str(ferrMuDoubPrime[i]))
pl.plot(freqListEig/10**9, impStore[:,0], label="No Ferrite")
pl.legend(loc="upper left")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$\Re{}e(Z_{\parallel})$ ($\Omega$)", fontsize=16.0)
pl.axis([1.6,2.0,10**0,10**5])
pl.show()
pl.clf()

###### Plot Q

pl.plot(ferrMuDoubPrime, qStore ,'kx', markersize=16.0)
pl.xlabel("$\mu^{''}/\mu^{'}$",fontsize=16.0)
pl.ylabel("Q", fontsize=16.0)
##pl.show()
pl.clf()

###### Plot R over Q

pl.plot(ferrMuDoubPrime, roverqStore*qStore, 'bx', markersize=16.0)
pl.xlabel("$\mu^{''}/\mu^{'}$",fontsize=16.0)
pl.ylabel("R/Q ($\Omega$)", fontsize=16.0)
##pl.show()
pl.clf()

###### Plot total surface losses

pl.plot(ferrMuDoubPrime, totSurfLossesStore, 'bx', markersize=16.0)
pl.xlabel("$\mu^{''}/\mu^{'}$",fontsize=16.0)
pl.ylabel("Total Normalised Surface Losses (W)", fontsize=16.0)
##pl.show()
pl.clf()

###### Plot total volume losses

pl.plot(ferrMuDoubPrime, totVolLossesStore, 'bx', markersize=16.0)
pl.xlabel("$\mu^{''}/\mu^{'}$",fontsize=16.0)
pl.ylabel("Total Normalised Volume Losses (W)", fontsize=16.0)
##pl.show()
pl.clf()

###### Plot ferrite percentage losses


pl.plot(qStore, ferrPerLosses, 'k-', markersize=16.0)
##pl.plot(ferrMuDoubPrime, ferrPerLosses, 'bx', markersize=16.0)
##pl.xlabel("$\mu^{''}/\mu^{'}$",fontsize=16.0)
pl.xlabel("Q",fontsize=16.0)
pl.ylabel("Percentage of losses in ferrite", fontsize=16.0)
##pl.axis([0,0.25,0,100])
##pl.show()
pl.clf()

###### Plot ferrite power losses

pl.plot(ferrMuDoubPrime, powerStore,  'bx', markersize=16.0, label="Total Power Loss")
pl.plot(ferrMuDoubPrime, powerFerrStore, 'r+', markersize=16.0, label="Power Loss in Ferrite")
pl.xlabel("$\mu^{''}/\mu^{'}$",fontsize=16.0)
pl.ylabel("Power Loss (W)", fontsize=16.0)
pl.legend(loc="upper right")
##pl.show()
pl.clf()
