from scipy import *
from pylab import *

sample_rate=1000.00
t=r_[0:0.6:1/sample_rate]
N=len(t)
s=sin(2*pi*50*t)+sin(2*pi*70*t+pi/4)
S=fft(s)
f=sample_rate*r_[0:(N/2)]/N
n=len(f)
plot(f,abs(S[0:n])/N)
show()
