import csv
import scipy as sp
import pylab as pl

gamma = 27.7286
v_rf = 6.0*10**5
h = 4620
circ = 6911.0
C = 3.0*10**8
f_rev = C/circ
tune= 0.00324
omega = tune*2*sp.pi*f_rev
e = 1.602176487*10**(-19)
m_pro = 1.672621637 *10**-27
p_0 = gamma*m_pro*C
k_h = 2*sp.pi*h/circ
T_0 = circ/C
alpha_bt = 0.00068
alpha_at = 0.00192
eta_bt = 1/(gamma**2) - alpha_bt
eta_at = 1/(gamma**2) - alpha_at
cos_phi_bt = omega**2*p_0*T_0/k_h/eta_bt/e/v_rf
cos_phi_at = omega**2*p_0*T_0/k_h/eta_at/e/v_rf
mod = 75.48

directory = "C:/Users/hday/Documents/PhD/1st_Year_09-10/Software/headtail/data_dump/comp_wake_non_lin/" #Directory of data
list_bt_sc_wake = [directory+"hdtl-bt-sc-short-wake.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_bt_sc_wake_alt = [directory+"hdtl-bt-sc-short-wake-alt.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_bt_sc_nonlin = [directory+"hdtl-bt-sc-short-nonlin.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_at_sc_wake = [directory+"hdtl-at-sc-short-wake.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_at_sc_wake_alt = [directory+"hdtl-at-sc-short-wake-alt.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_at_sc_nonlin = [directory+"hdtl-at-sc-short-nonlin.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]


value_desired = 9
big_bugger =[]
data_list_1 = []
for data_raw in list_bt_sc_wake:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])
    
bt_sc_wake = sp.array(data_list_1)
data_list_1 = []
for data_raw in list_bt_sc_wake_alt:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])
    
bt_sc_wake_alt = sp.array(data_list_1)



data_list_1 = []
for data_raw in list_bt_sc_nonlin:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])
    
bt_sc_nonlin = sp.array(data_list_1)


data_list_1 = []
for data_raw in list_at_sc_wake:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])
    
at_sc_wake = sp.array(data_list_1)

data_list_1 = []
for data_raw in list_at_sc_wake_alt:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])
    
at_sc_wake_alt = sp.array(data_list_1)

data_list_1 = []
for data_raw in list_at_sc_nonlin:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    print data_raw
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])
    
at_sc_nonlin = sp.array(data_list_1)



x_values = sp.array(range(1,31))

pl.plot(x_values, bt_sc_wake[0,:], 'r-', label = "Below Transition - Syn")
pl.plot(x_values, bt_sc_wake_alt[0,:], 'r-', label = "Below Transition - Wake") # plot fitted curve
pl.plot(x_values, bt_sc_nonlin[0,:],'r:', label = "Below Transition - nonlin")
pl.plot(x_values, at_sc_wake[0,:], 'k-', label = "Above Transition - Syn")                    # plot fitted curve
pl.plot(x_values, at_sc_wake_alt[0,:], 'k-', label = "Above Transition - Wake") 
pl.plot(x_values, at_sc_nonlin[0,:],'k:', label = "Above Transition - nonlin")




pl.grid(linestyle="--", which = "major")
pl.xlabel("Bunch Intensity ($10^{10}$)",fontsize = 16)                  #Label axes
pl.ylabel("RMS Bunch Length $\sigma_{z}$ $(m)$",fontsize = 16)
pl.title("", fontsize = 16)
pl.legend(loc = "upper left")
pl.axis([0,5,0,5])
pl.savefig(directory+"bt-at-rms-bunch-length"+".pdf")                  # Save fig for film
pl.show()
pl.clf()   

##polycoeffs = sp.polyfit(x_values,big_bugger[2,:],1)
##print polycoeffs
##yfit=sp.polyval(polycoeffs,x_values)
##pl.plot(x_values, big_bugger[2,:])
##pl.plot(x_values, yfit)
##pl.show()
##pl.savefig(directory+"moo.pdf")
##pl.clf()
##        

x_val_test = sp.array([i*10**10 for i in range(1,31)])
bt_sc_wake = sp.array([(bt_sc_wake[i]/C) for i in range(0,len(bt_sc_wake))])
polycoeffs = sp.polyfit(x_val_test[:10], bt_sc_wake[:10],1)
x_test = sp.linspace(0,10**11,100)
y_fit = sp.polyval(polycoeffs, x_test)
##pl.plot(x_val_test[:10], bt_no_sc[:10])
##pl.plot(x_test, y_fit)
##pl.show()
Z_l_n = -mod*4*cos_phi_bt*(sp.pi)**2*(v_rf*h)*(f_rev*polycoeffs[1])**2*polycoeffs[0]/(3*e)
print Z_l_n
pl.clf()

x_val_test = sp.array([i*10**10 for i in range(1,31)])
bt_sc_nonlin = sp.array([(bt_sc_nonlin[i]/C) for i in range(0,len(bt_sc_nonlin))])
polycoeffs = sp.polyfit(x_val_test[:10], bt_sc_nonlin[:10],1)
x_test = sp.linspace(0,10**11,100)
y_fit = sp.polyval(polycoeffs, x_test)
##pl.plot(x_val_test[:10], bt_no_sc[:10])
##pl.plot(x_test, y_fit)
##pl.show()
Z_l_n = -mod*4*cos_phi_bt*(sp.pi)**2*(v_rf*h)*(f_rev*polycoeffs[1])**2*polycoeffs[0]/(3*e)
print Z_l_n
pl.clf()

x_val_test = sp.array([i*10**10 for i in range(1,31)])
at_sc_wake = sp.array([(at_sc_wake[i]/C) for i in range(0,len(at_sc_wake))])
polycoeffs = sp.polyfit(x_val_test[:10], at_sc_wake[:10],1)
x_test = sp.linspace(0,10**11,100)
y_fit = sp.polyval(polycoeffs, x_test)
##pl.plot(x_val_test[:10], bt_no_sc[:10])
##pl.plot(x_test, y_fit)
##pl.show()
Z_l_n = -mod*4*cos_phi_at*(sp.pi)**2*(v_rf*h)*(f_rev*polycoeffs[1])**2*polycoeffs[0]/(3*e)
print Z_l_n
pl.clf()

x_val_test = sp.array([i*10**10 for i in range(1,31)])
at_sc_nonlin = sp.array([(at_sc_nonlin[i]/C) for i in range(0,len(at_sc_nonlin))])
polycoeffs = sp.polyfit(x_val_test[:10], at_sc_nonlin[:10],1)
x_test = sp.linspace(0,10**11,100)
y_fit = sp.polyval(polycoeffs, x_test)
##pl.plot(x_val_test[:10], bt_no_sc[:10])
##pl.plot(x_test, y_fit)
##pl.show()
Z_l_n = -mod*4*cos_phi_at*(sp.pi)**2*(v_rf*h)*(f_rev*polycoeffs[1])**2*polycoeffs[0]/(3*e)
print Z_l_n
pl.clf()
