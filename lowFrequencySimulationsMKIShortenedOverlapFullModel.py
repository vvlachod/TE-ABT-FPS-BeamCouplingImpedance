import csv, time, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack

####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

######### Define importing, analysis functions #########

c=299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005
lWire = 3.551
lDUT = 2.45
resCopper = 17.24*10.0**-9
resOuter = 1/(3.3*10.0**7)
ZChar = 60*sp.log(dAper/rWire)

def importDatSimMeas(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
    datStore = []
    for entry in tempDat[1:]:
        temp = map(float, entry.split(','))
        datStore.append(temp)

    return datStore

def analSingReSimMeas(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    reStore = []
    temp = []
##    Z0 = 60*sp.log(1.27*apPipe/r_wire)
    Z0=50.0
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(-2*Z0/lenTot*sp.log(10**(data[i,j]/20))) 
        temp.append(tempLin)
    return temp

def fileImport(tarFile):
    readPart=open(tarFile, 'r+')
    datUnPro = readPart.readlines()
    readPart.close()
    freqDat=[]
    qLoaded=[]
    s21DB=[]
    for row in datUnPro[20:]:
        freqDat.append(float(row.split(",")[0]))
        qLoaded.append(float(row.split(",")[1]))
        s21DB.append(float(row.split(",")[2]))

    return sp.array(freqDat), sp.array(qLoaded), sp.array(s21DB)

def skinDepth(freq, cond):
    return (1/(sp.pi*freq*mu0*cond))**0.5

def resImpGet(targetFile):

    freqList, qTotal, transCoeff = fileImport(targetFile)

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(c*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor

    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper)/attenCopper

    return freqList, zMeas/lDUT

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def extract_dat2013(file_name):
    data = open(file_name, "r+")
    tar = data.readlines()
    temp=[]
    for row in tar[2:]:
        row = map(float, row.split())
        temp.append(row)
    data.close()

    return temp[:-1]

def extract_dat2013_MultipleSweeps(file_name):
    data = open(file_name, "r+")
    tar = data.readlines()
    temp=[]
    bigTemp = []
    count=2
    while count<len(tar):
        try:
            row = map(float, tar[count].split())
            temp.append(row)
            count+=1
        except:
            bigTemp.append(temp)
            temp=[]
            count+=3
    data.close()
    bigTemp.append(temp)
    return bigTemp

def sParaRead(fileTar, lenDUT, col):
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        phase = row[col+1]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360    

        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        import_dat = [row[0], row[col], sp.radians(phase), -s21Pec]
        data.append(import_dat)
        last = phase

    return data


def impAnalysisSingle(fileTar, lenDUT, r_wire, r_pipe, Zc):
    ###### Analyses a S2P file for measurement of longitudinal impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        phase = row[4]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360


        import_dat = [row[0], -2*Zc*sp.log(linDat), -2*Zc*(sp.radians(phase)+s21Pec)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDUT), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDUT))]
        data.append(import_dat)
        last = phase

    return data

def impAnalysisTwo(fileTar, lenDut, r_wire, r_pipe, d_wire, Zc):
    ###### Analyses a S2P file for measurement of dipole impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter = 0.0
    last = 1e8

    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDut/C
        phase = row[4]-counter
        if phase>last and (phase-last)>180:
            count+=1
            counter+=360
            pgase-=360
        import_dat = [row[0], -2*Zc*sp.log(linDat)*C/(2*sp.pi*row[0]*d_wire**2), -2*Zc*(sp.radians(phase)+s21Pec)*C/(2*sp.pi*row[0]*d_wire**2)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDut)*C/(row[0]*d_wire**2), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDut))*C/(row[0]*d_wire**2)]
        data.append(import_dat)
    return data

def impTransPara(dispWire, measImp, wireErr):
##### Takes an array of longitudinal measurements and returns the transverse impedance
    quadFunc = lambda p, x: p[0] + x * p[1] + x**2 * p[2]
    errFunc = lambda p, x, Z: (Z-quadFunc(p,x))

    totTrans = []
    longComp = []

    for i in range(0,len(measImp[0,:,1])):
        pinit = [1.0, -1.0, 1.0]
        ZerrRe = measImp[:,i,1]*wireErr
        pReal, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,1]), full_output = 0)
        pinit = [1.0, -1.0, 1.0]
        ZerrIm = measImp[:,i,2]*wireErr
        pImag, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,2]), full_output = 0)
        totTrans.append([pReal[2]*C/(2*sp.pi*measImp[0,i,0]), pImag[2]*C/(2*sp.pi*measImp[0,i,0])])
        longComp.append([pReal[0], pImag[0]])

    return longComp, totTrans

def quadImp(totalTrans, dipTrans, axis):
######## Returns the quadrupolar impedance from total transverse and dipolar data
    quadImp = []
    for i in range(0,len(totalTrans)):
        if axis == "horz":
            quadImp.append([totalTrans[i,1]+dipTrans[i,1],totalTrans[i,2]+dipTrans[i,2]])
        elif axis == "vert":
            quadImp.append([totalTrans[i,1]-dipTrans[i,1],totalTrans[i,2]-dipTrans[i,2]])
    return quadImp


def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def peakFitSimulations(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 50
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 50 and y_val[current_max_x]>2:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [0.2, 10.0, 10.0]
        lower_bound = res[0]-50
        upper_bound = res[0]+50
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def peakFitMeasurements(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 0
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 2 and y_val[current_max_x]>3:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [res[1], 4.0, data[res[0],1]]
        lower_bound = res[0]-4
        upper_bound = res[0]+3
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2)

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2
                                                    
def logToLin(data):
    return 10**(data/20)

def linToLog(data):
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

def overLapFreq(overlap, fringeLen, harmonic):
    return harmonic*C/(2*10**0.5*(overlap+fringeLen))

def heatingValGauss(impArr, beamCur, bunSpac, bunLen):
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac/10**9,impArr[-1,0],impArr[-1,0]*10**9*bunSpac)
    splineFitHeatingImp = SplineFitImp(freqListHeating)
    heatingTotalPart = []
    for i in range(0,len(splineFitHeatingImp)):
        heatingTotalPart.append([freqListHeating[i], abs(2*splineFitHeatingImp[i]*(beamCur**2)*gaussProf(freqListHeating[i]*10.0**9, bunLen))])
    heatingTotalPart=sp.array(heatingTotalPart)
    cumHeating = []
    for i in range(0,len(heatingTotalPart)):
        cumHeating.append([heatingTotalPart[i,0],sum(heatingTotalPart[:i,1])])
    cumHeating=sp.array(cumHeating)
    return heatingTotalPart[:,1].sum(), heatingTotalPart, cumHeating

def heatingValGaussVariable(impArr, beamCur, bunSpac, bunLen):
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac/10**9,impArr[-1,0],impArr[-1,0]*10**9*bunSpac)
    listFreqVar = []
    for j in range(0,41,1):
        temp = SplineFitImp(freqListHeating+(j/10.0**3))
        tempHold = []
        for i in range(0,len(temp)):
            tempHold.append([freqListHeating[i], temp[i]])
        listFreqVar.append(tempHold)

    listFreqVar = sp.array(listFreqVar)
    heatingTotalPart = []
    for entry in listFreqVar:
        temp = []
        for i in range(0,len(entry)):
            temp.append([entry[i,0], abs(2*entry[i,1]*(beamCur**2)*gaussProf(freqListHeating[i]*10.0**9, bunLen))])
        heatingTotalPart.append(temp)
    heatingTotalPart=sp.array(heatingTotalPart)
    cumHeating = []
    totalHeating = []
    for entry in heatingTotalPart:
        temp=[]
        for i in range(0,len(entry)):
            temp.append([entry[i,0],sum(entry[:i,1])])
        totalHeating.append(sum(entry[:,1]))
        cumHeating.append(temp)
    cumHeating=sp.array(cumHeating)
    totalHeating=sp.array(totalHeating)
    return totalHeating, heatingTotalPart, cumHeating

def splineFitImpFunc(impArr):
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListTemp = sp.linspace(10**6/10**9,impArr[-1,0],10000)
    splineFitImpFreq=SplineFitImp(freqListTemp)
    temp = []
    for i in range(0,len(freqListTemp)):
        temp.append([freqListTemp[i], splineFitImpFreq[i]])
    return sp.array(temp)

def splineFitImpFuncVariable(impArr):
    ### Takes an impedance profile and returns an array of spline fits offset by +/- 40MHz (to examine power loss between beam harmonics)
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListTemp = sp.linspace(10**6/10**9, impArr[0,-1], 10000)
    splineFitImpFreq=SplineFitImp(freqListTemp)
    listFreqVar = []
    for j in range(-40,45,5):
        temp = []
        for i in range(0,len(freqListTemp)):
            temp.append([freqListTemp[i]+(j/10.0**3), splineFitImpFreq[i]])
        listFreqVar.append(temp)
    return sp.array(listFreqVar)

def splineFitImpFuncHeatingVariable(impArr):
    ### Takes an impedance profile and returns an array of spline fits offset by +/- 40MHz (to examine power loss between beam harmonics) and with beam harmonic spread
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac/10**9,impArr[-1,0],impArr[-1,0]*10**9*bunSpac)
    splineFitImpFreq=SplineFitImp(freqListHeating)
    listFreqVar = []
    for j in range(-40,45,5):
        temp = []
        for i in range(0,len(freqListHeating)):
            temp.append([freqListHeating[i]+(j/10.0**3), splineFitImpFreq[i]])
        listFreqVar.append(temp)
    return sp.array(listFreqVar)

def heatingLossFactorGauss(impSplineFit, bunLength, bunPop):
    convBeamSpecImp = abs(impSplineFit[:,1])*(gaussProf(impSplineFit[:,0]*10**9, bunLength)**2)
    return integrate.simps(fftPack.ifft(convBeamSpecImp), impSplineFit[:,0]*10**9)*2*sp.pi*(1.6*10**-19)**2*bunPop, convBeamSpecImp*2*sp.pi*(1.6*10**-19)**2*bunPop, convBeamSpecImp


length = 2.88
f_rev = C/27000.0
nBunches = 2808
qPart = 1.6*10.0**-19
nPart=1.15*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=1.001*10.0**-9
testImp = [250*10**6, 10.0, 10000]
bunchCur=nPart*qPart*f_rev
print bCur, bunchCur

######Import a selection of wire measurements ########

measurementsMKI071113Freq, measurementsMKI071113Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/07-11-13/mki10-mc06/resonator"))
measurementsMKI061213Freq, measurementsMKI061213Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/06-12-13/mkiTank6-MC10/resonator"))
measurementsMKI060214Freq, measurementsMKI060214Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/06-02-14/mki-Tank10-cr2/resonator"))
measurementsMKI110314Freq, measurementsMKI110314Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/11-03-14/mki09-tank2-cr05/resShort"))
measurementsMKI190514Freq, measurementsMKI190514Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/19-05-14/mkit05-cr01/resonator"))
measurementsMKI110714Freq, measurementsMKI110714Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/11-07-14/mkiTank01-CR03/resScript"))

dirMatch140714 = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/14-07-14/mkiTank01-CR03/S21FREQMATCHINGNOGATING.S2P"
tar=open(dirMatch140714,'r+')
inputData=tar.readlines()
tar.close()
temp=[]
for line in inputData[6:]:
    bit=line.split("\t")
    temp.append(map(float,bit))

matched140714 = sp.array(temp)
matched140714Imp = logImpFormula(logToLin(matched140714[:,3]), 1, 160)

dirMatch140714 = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/14-07-14/mkiTank01-CR03/S21FREQMATCHINGLOWFREQ.S2P"
tar=open(dirMatch140714,'r+')
inputData=tar.readlines()
tar.close()
temp=[]
for line in inputData[6:]:
    bit=line.split("\t")
    temp.append(map(float,bit))

matched140714LowFreq = sp.array(temp)
matched140714LowFreqImp = logImpFormula(logToLin(matched140714LowFreq[:,3]), 1, 160)

topDir = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/VaryingOverlaps/"
overlap60mmDat = sp.array(extract_dat2013(topDir+"60mmOverlap/longitudinal-impedance-real.txt"))
overlap70mmDat = sp.array(extract_dat2013(topDir+"70mmOverlap/longitudinal-impedance-real.txt"))
overlap8090mmDat = extract_dat2013_MultipleSweeps(topDir+"80-90mmOverlap/80-90-longitudinal-impedance-real.txt")
overlap80mmDat = sp.array(overlap8090mmDat[0][:-1])
overlap90mmDat = sp.array(overlap8090mmDat[1][:-1])
overlap160mmDat = sp.array(extract_dat2013(topDir+"160mmOverlap/long-impedance-real.txt"))
overlap80110mmDat = extract_dat2013_MultipleSweeps("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/mkiOverlapChanges/80-110-longitudinal-impedance-real.txt")
overlap80mmDatBigger = sp.array(overlap80110mmDat[0][:-1])
overlap90mmDatBigger = sp.array(overlap80110mmDat[1][:-1])
overlap100mmDatBigger = sp.array(overlap80110mmDat[2][:-1])
overlap110mmDatBigger = sp.array(overlap80110mmDat[3][:-1])


overlap107mmStepout3mmDat = sp.array(extract_dat2013("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/stepout3mmAllaround/longitudinal-impedance-real.txt"))

overlap107mmStepout2mm1mmStepDat = sp.array(extract_dat2013("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/step2mmOffset1mmStepAllAlong/longitudinal-impedance-real.txt"))
overlap107mmStepout2mm1mmStep2GHzDat = sp.array(extract_dat2013("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/step2mmOffset1mmStepAllAlong150mWake2GHz/longitudinal-impedance-real-dft.txt"))
overlap107mmStepout2mm1mmStepCera2GHzDat = sp.array(extract_dat2013("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/step2mmOffset1mmStepAllAlongFilledWithCeramic30mWake2GHz/longitudinal-impedance-real.txt"))

overlap107mmStepout3mm1mmDisplace2GHzDat = sp.array(extract_dat2013("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/3mmStepOut1mmOffsetAtBottom30mWake2GHz/longitudinal-impedance-real.txt"))

topDir = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/reBuiltModelNewDesign/"
postls1150mDat = sp.array(extract_dat2013(topDir+"rebuiltModelNewDesign24Cond150mWake/longitudinal-impedance-real.txt"))
postls1100mDat = sp.array(extract_dat2013(topDir+"rebuiltModelNewDesign24Cond100mWake/longitudinal-impedance-real.txt"))
postls1150mLowFreqDat = sp.array(extract_dat2013(topDir+"rebuiltModelNewDesign24Cond150mWakeLowFreq/longitudinal-impedance-real.txt"))
postls1150m3mmStepDat = sp.array(extract_dat2013(topDir+"rebuiltModelNewDesign24Cond3mmAllAround150mWake/longitudinal-impedance-real.txt"))
topDir = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/"
postls130m117mmDat = sp.array(extract_dat2013(topDir+"POSTLS1Overlap117mm/longitudinal-impedance-real.txt"))
postls250m117mmDat = sp.array(extract_dat2013(topDir+"POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/airGap1mmBottomSlot5mmLongWake/longitudinal-impedance-real.txt"))
postls250m70mmDat = sp.array(extract_dat2013(topDir+"POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/70mmOverlapWith1mmAirGap/longitudinal-impedance-real.txt"))
stepOutHLLHC65mmDat = sp.array(extract_dat2013(topDir+"3mmStepOut1mmOffsetAtBottom30mWake2GHzOverlaps/65mmOverlap-longitudinal-impedance-real.txt"))
stepOutHLLHC107mmDat = sp.array(extract_dat2013(topDir+"3mmStepOut1mmOffsetAtBottom30mWake2GHz/longitudinal-impedance-real.txt"))

overlap65mm105mmDat = extract_dat2013_MultipleSweeps("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/3mmStepOut1mmOffsetAtBottom30mWake2GHzOverlaps/65-105mmOverlap-longitudinal-impedance-real.txt")
overlap65mmImp = sp.array(overlap65mm105mmDat[0][:-1])
overlap75mmImp = sp.array(overlap65mm105mmDat[1][:-1])
overlap85mmImp = sp.array(overlap65mm105mmDat[2][:-1])
overlap95mmImp = sp.array(overlap65mm105mmDat[3][:-1])
overlap105mmImp = sp.array(overlap65mm105mmDat[4][:-1])

overlap60mmSpline=splineFitImpFunc(overlap60mmDat)
overlap70mmSpline=splineFitImpFunc(overlap70mmDat)
overlap80mmSpline=splineFitImpFunc(overlap80mmDat)
overlap90mmSpline=splineFitImpFunc(overlap90mmDat)
overlap160mmSpline=splineFitImpFunc(overlap160mmDat)

overlap107mmStepout3mmSpline=splineFitImpFunc(overlap107mmStepout3mmDat)

overlap80mmBiggerSpline=splineFitImpFunc(overlap80mmDatBigger)
overlap90mmBiggerSpline=splineFitImpFunc(overlap90mmDatBigger)
overlap100mmBiggerSpline=splineFitImpFunc(overlap100mmDatBigger)
overlap110mmBiggerSpline=splineFitImpFunc(overlap110mmDatBigger)

overlap107mmStepout2mm1mmStep2GHzSpline=splineFitImpFunc(overlap107mmStepout2mm1mmStep2GHzDat)
overlap107mmStepout2mm1mmStepSpline=splineFitImpFunc(overlap107mmStepout2mm1mmStepDat)

postls1150m3mmStepSpline=splineFitImpFunc(postls1150m3mmStepDat)
postls1150mSpline=splineFitImpFunc(postls1150mDat)
postls1100mSpline=splineFitImpFunc(postls1100mDat)
postls1150mLowFreqSpline=splineFitImpFunc(postls1150mLowFreqDat)

postls1150mSplineVariable=splineFitImpFuncVariable(postls1150mDat)

powLossTotal60mm, powLossFreq60mm, powLossCum60mm = heatingValGauss(overlap60mmDat, bCur, 25.0*10**-9, bLength)
print powLossTotal60mm
powLossTotal70mm, powLossFreq70mm, powLossCum70mm = heatingValGauss(overlap70mmDat, bCur, 25.0*10**-9, bLength)
print powLossTotal70mm
powLossTotal80mm, powLossFreq80mm, powLossCum80mm = heatingValGauss(overlap80mmDat, bCur, 25.0*10**-9, bLength)
print powLossTotal80mm
powLossTotal90mm, powLossFreq90mm, powLossCum90mm = heatingValGauss(overlap90mmDat, bCur, 25.0*10**-9, bLength)
print powLossTotal90mm
powLossTotal160mm, powLossFreq160mm, powLossCum160mm = heatingValGauss(overlap160mmDat, bCur, 25.0*10**-9, bLength)
print powLossTotal160mm
powLossTotal107mmStepout3mm, powLossFreq107mmStepout3mm, powLossCum107mmStepout3mm = heatingValGauss(overlap107mmStepout3mmDat, bCur, 25.0*10**-9, bLength)
print powLossTotal107mmStepout3mm
powLossTotaloverlap107mmStepout2mm1mmStep2GHz, powLossFreqoverlap107mmStepout2mm1mmStep2GHz, powLossCumoverlap107mmStepout2mm1mmStep2GHz = heatingValGauss(overlap107mmStepout2mm1mmStep2GHzDat, bCur, 25.0*10**-9, bLength)
print powLossTotaloverlap107mmStepout2mm1mmStep2GHz
powLossTotaloverlap107mmStepout2mm1mmStep2GHzCera, powLossFreqoverlap107mmStepout2mm1mmStep2GHzCera, powLossCumoverlap107mmStepout2mm1mmStep2GHzCera = heatingValGauss(overlap107mmStepout2mm1mmStepCera2GHzDat, bCur, 25.0*10**-9, bLength)
print powLossTotaloverlap107mmStepout2mm1mmStep2GHzCera
powLossTotaloverlap107mmStepout2mm1mmStep, powLossFreqoverlap107mmStepout2mm1mmStep, powLossCumoverlap107mmStepout2mm1mmStep = heatingValGauss(overlap107mmStepout2mm1mmStepDat, bCur, 25.0*10**-9, bLength)
print powLossTotaloverlap107mmStepout2mm1mmStep
powLossTotal80mmBigger, powLossFreq80mmBigger, powLossCum80mmBigger = heatingValGauss(overlap80mmDatBigger, bCur, 25.0*10**-9, bLength)
print powLossTotal80mmBigger
powLossTotal90mmBigger, powLossFreq90mmBigger, powLossCum90mmBigger = heatingValGauss(overlap90mmDatBigger, bCur, 25.0*10**-9, bLength)
print powLossTotal90mmBigger
powLossTotal100mmBigger, powLossFreq100mmBigger, powLossCum100mmBigger = heatingValGauss(overlap100mmDatBigger, bCur, 25.0*10**-9, bLength)
print powLossTotal100mmBigger
powLossTotal110mmBigger, powLossFreq110mmBigger, powLossCum110mmBigger = heatingValGauss(overlap110mmDatBigger, bCur, 25.0*10**-9, bLength)
print powLossTotal110mmBigger

powLossTotalpostls1150m3mmStep, powLossFreqpostls1150m3mmStep, powLossCumpostls1150m3mmStep = heatingValGauss(postls1150m3mmStepDat, bCur, 25.0*10**-9, bLength)
print powLossTotalpostls1150m3mmStep

powLossTotaloverlap107mmStepout3mm1mmDisplace2GHz, powLossFreqoverlap107mmStepout3mm1mmDisplace2GHz, powLossCumoverlap107mmStepout3mm1mmDisplace2GHz = heatingValGauss(overlap107mmStepout3mm1mmDisplace2GHzDat, bCur, 25.0*10**-9, bLength)
print powLossTotaloverlap107mmStepout3mm1mmDisplace2GHz

powLossTotalpostls130m117mm, powLossFreqpostls130m117mm, powLossCumpostls130m117mm = heatingValGauss(postls130m117mmDat, bCur, 25.0*10**-9, bLength)
print "LS1 Pow Loss: "+str(powLossTotalpostls130m117mm)

powLossTotalpostls250m117mm, powLossFreqpostls250m117mm, powLossCumpostls250m117mm = heatingValGauss(postls250m117mmDat, bCur, 25.0*10**-9, bLength)
print "LS2 Pow Loss: "+str(powLossTotalpostls250m117mm)

powLossTotalpostls250m70mm, powLossFreqpostls250m70mm, powLossCumpostls250m70mm = heatingValGauss(postls250m70mmDat, bCur, 25.0*10**-9, bLength)
print "LS2 Pow Loss: "+str(powLossTotalpostls250m70mm)

powLossTotaloverlap107mmStepout3mm1mmDisplace2GHz, powLossFreqoverlap107mmStepout3mm1mmDisplace2GHz, powLossCumoverlap107mmStepout3mm1mmDisplace2GHz = heatingValGauss(overlap107mmStepout3mm1mmDisplace2GHzDat, bCur, 25.0*10**-9, bLength)
print powLossTotaloverlap107mmStepout3mm1mmDisplace2GHz

powLossTotalpostls1150m, powLossFreqpostls1150m, powLossCumpostls1150m = heatingValGauss(postls1150mDat, bCur, 25.0*10**-9, bLength)
print powLossTotalpostls1150m
powLossTotalpostls1150mLowFreq, powLossFreqpostls1150mLowFreq, powLossCumpostls1150mLowFreq = heatingValGauss(postls1150mLowFreqDat, bCur, 25.0*10**-9, bLength)
print powLossTotalpostls1150mLowFreq

powLossTotalstepOutHLLHC65mm, powLossFreqstepOutHLLHC65mm, powLossCumstepOutHLLHC65mm = heatingValGauss(stepOutHLLHC65mmDat, bCur, 25.0*10**-9, bLength)
print powLossTotalstepOutHLLHC65mm

powLossTotalstepOutHLLHC107mm, powLossFreqstepOutHLLHC107mm, powLossCumstepOutHLLHC107mm = heatingValGauss(stepOutHLLHC107mmDat, bCur, 25.0*10**-9, bLength)
print powLossTotalstepOutHLLHC107mm

powLossTotalpostls1150mVar, powLossFreqpostls1150mVar, powLossCumpostls1150mVar = heatingValGaussVariable(postls1150mDat, bCur, 25.0*10**-9, bLength)
##print powLossCumpostls1150mVar

powLossTotalpostls1117mmVar, powLossFreqpostls1117mmVar, powLossCumpostls1117mmVar = heatingValGaussVariable(postls130m117mmDat, bCur, 25.0*10**-9, bLength)
##print powLossCumpostls1150mVar

powLossTotaloverlap107mmStepout2mm1mmStep2GHzVar, powLossFreqoverlap107mmStepout2mm1mmStep2GHzVar, powLossCumoverlap107mmStepout2mm1mmStep2GHzVar = heatingValGaussVariable(overlap107mmStepout2mm1mmStep2GHzDat, bCur, 25.0*10**-9, bLength)
##print powLossCumoverlap107mmStepout2mm1mmStep2GHzVar

powLossTotaloverlap107mmStepout2mm1mmStepVar, powLossFreqoverlap107mmStepout2mm1mmStepVar, powLossCumoverlap107mmStepout2mm1mmStepVar = heatingValGaussVariable(overlap107mmStepout2mm1mmStepDat, bCur, 25.0*10**-9, bLength)
##print powLossCumoverlap107mmStepout2mm1mmStep2GHzVar

powLossTotaloverlap107mmStepout3mmVar, powLossFreqoverlap107mmStepout3mmVar, powLossCumoverlap107mmStepout3mmVar = heatingValGaussVariable(overlap107mmStepout3mmDat, bCur, 25.0*10**-9, bLength)
##print powLossCumoverlap107mmStepout3mmVar

powLossTotaloverlap107mmStepout3mm1mmDisplace2GHzVar, powLossFreqoverlap107mmStepout3mm1mmDisplace2GHzVar, powLossCumoverlap107mmStepout3mm1mmDisplace2GHzVar = heatingValGaussVariable(overlap107mmStepout3mm1mmDisplace2GHzDat, bCur, 25.0*10**-9, bLength)
##print powLossCumoverlap107mmStepout3mm1mmDisplace2GHzVar

powLossTotal65mm, powLossFreq65mm, powLossCum65mm = heatingValGauss(overlap65mmImp, bCur, 25.0*10**-9, bLength)
print "Pow loss 65mm: "+str(powLossTotal65mm)

powLossTotal75mm, powLossFreq75mm, powLossCum75mm = heatingValGauss(overlap75mmImp, bCur, 25.0*10**-9, bLength)
print "Pow loss 75mm: "+str(powLossTotal75mm)

powLossTotal85mm, powLossFreq85mm, powLossCum85mm = heatingValGauss(overlap85mmImp, bCur, 25.0*10**-9, bLength)
print "Pow loss 85mm: "+str(powLossTotal85mm)

powLossTotal95mm, powLossFreq95mm, powLossCum95mm = heatingValGauss(overlap95mmImp, bCur, 25.0*10**-9, bLength)
print "Pow loss 95mm: "+str(powLossTotal95mm)

powLossTotal105mm, powLossFreq105mm, powLossCum105mm = heatingValGauss(overlap105mmImp, bCur, 25.0*10**-9, bLength)
print "Pow loss 105mm: "+str(powLossTotal105mm)


tempt, wibble, plot = heatingLossFactorGauss(overlap160mmSpline, 1.0*10.0**-9, nPart)
beamProfGauss = gaussProf(overlap160mmSpline[:,0]*10**9, 1.0*10.0**-9)

##print tempt*(nBunches*f_rev)**2
##print nBunches*f_rev*(1.6*10**-19)*nPart
##pl.semilogx()
##pl.semilogy()
##pl.plot(overlap60mmDat[:,0], overlap60mmDat[:,1], "r-", label="L$_{overlap}$=60mm")
##pl.plot(overlap70mmDat[:,0], overlap70mmDat[:,1], "k-", label="L$_{overlap}$=70mm")
##pl.plot(overlap80mmDat[:,0], overlap80mmDat[:,1], "b-", label="L$_{overlap}$=80mm")
##pl.plot(overlap90mmDat[:,0], overlap90mmDat[:,1], "m-", label="L$_{overlap}$=90mm")
##pl.plot(overlap107mmStepout3mmDat[:,0], overlap107mmStepout3mmDat[:,1], 'r-')
##pl.plot(overlap60mmSpline[:,0], overlap60mmSpline[:,1])
##pl.plot(overlap70mmSpline[:,0], overlap70mmSpline[:,1])
##pl.plot(overlap80mmSpline[:,0], overlap80mmSpline[:,1])
##pl.plot(overlap90mmSpline[:,0], overlap90mmSpline[:,1])
##pl.plot(overlap160mmSpline[:,0], overlap160mmSpline[:,1])
##pl.plot(overlap107mmStepout3mmSpline[:,0], overlap107mmStepout3mmSpline[:,1], 'g-')
##pl.plot(postls1150mDat[:,0], postls1150mDat[:,1], 'b-')
##pl.plot(postls1150mLowFreqDat[:,0], postls1150mLowFreqDat[:,1], 'g-')
##pl.plot(postls1150m3mmStepDat[:,0], postls1150m3mmStepDat[:,1])
##pl.plot(overlap80mmDatBigger[:,0], overlap80mmDatBigger[:,1])
##pl.plot(overlap90mmDatBigger[:,0], overlap90mmDatBigger[:,1], "g-", label="L$_{overlap}$=90mm")
##pl.plot(overlap100mmDatBigger[:,0], overlap100mmDatBigger[:,1], "r--", label="L$_{overlap}$=100mm")
##pl.plot(overlap110mmDatBigger[:,0], overlap110mmDatBigger[:,1], "k--", label="L$_{overlap}$=110mm")
##pl.plot(overlap160mmDat[:,0], overlap160mmDat[:,1], "b--", label="L$_{overlap}$=160mm")

##pl.plot(postls1150mDat[:,0], postls1150mDat[:,1]/lDUT, "b-", label="Post LS1 L$_{overlap}$=107mm")
##pl.plot(postls130m117mmDat[:,0], postls130m117mmDat[:,1], "b-", label="Post LS1 L$_{overlap}$=117mm")
pl.plot(postls130m117mmDat[:,0], postls130m117mmDat[:,1], "b-", label="Simulation L$_{overlap}$=117mm")
pl.plot(postls250m117mmDat[:,0], postls250m117mmDat[:,1], "k-", label="Simulation L$_{overlap}$=117mm")
pl.plot(postls250m70mmDat[:,0], postls250m70mmDat[:,1]/lDUT, "k-", label="Simulation L$_{overlap}$=70mm")
pl.plot(overlap107mmStepout2mm1mmStep2GHzDat[:,0], overlap107mmStepout2mm1mmStep2GHzDat[:,1]/lDUT, 'k-', label="1mm Stepout Air L$_{overlap}$=107mm")
##pl.plot(overlap107mmStepout2mm1mmStepDat[:,0], overlap107mmStepout2mm1mmStepDat[:,1], 'r-')
##pl.plot(overlap107mmStepout2mm1mmStepCera2GHzDat[:,0], overlap107mmStepout2mm1mmStepCera2GHzDat[:,1]/lDUT, 'm-', label="1mm Stepout Ceramic L$_{overlap}$=107mm")
##pl.plot(overlap107mmStepout3mmDat[:,0], overlap107mmStepout3mmDat[:,1]/lDUT, 'r-', label="Square L$_{overlap}$=107mm")
##pl.plot(overlap107mmStepout3mm1mmDisplace2GHzDat[:,0], overlap107mmStepout3mm1mmDisplace2GHzDat[:,1]/lDUT, "g-", label="1mm Stepout offset L$_{overlap}$=107mm")
##pl.plot(stepOutHLLHC107mmDat[:,0], stepOutHLLHC107mmDat[:,1], "g-")
##
##pl.plot(stepOutHLLHC65mmDat[:,0], stepOutHLLHC65mmDat[:,1])
##
pl.plot(overlap65mmImp[:,0], overlap65mmImp[:,1]/2.45)
pl.plot(overlap75mmImp[:,0], overlap75mmImp[:,1]/2.45)
pl.plot(overlap85mmImp[:,0], overlap85mmImp[:,1]/2.45)
pl.plot(overlap95mmImp[:,0], overlap95mmImp[:,1]/2.45)
pl.plot(overlap105mmImp[:,0], overlap105mmImp[:,1]/2.45)

##pl.plot(measurementsMKI071113Freq/10**9, measurementsMKI071113Imp, "rx", markersize=16.0, label="Resonator Measurements")
##pl.plot(measurementsMKI061213Freq/10**9, measurementsMKI061213Imp, "y--")
##pl.plot(measurementsMKI060214Freq/10**9, measurementsMKI060214Imp, "g--")
##pl.plot(measurementsMKI110314Freq/10**9, measurementsMKI110314Imp, "k--")
##pl.plot(measurementsMKI190514Freq/10**9, measurementsMKI190514Imp, "b--")
##pl.plot(measurementsMKI110714Freq/10**9, measurementsMKI110714Imp, "m--")
##pl.plot(matched140714LowFreq/10**9, matched140714LowFreqImp-matched140714LowFreqImp[0])
##pl.plot(matched140714[:,0]/10**9, matched140714Imp-400, "k-", label="Classical Coaxial Measurements")
##for i in range(0,int(1/0.04)):
##    pl.axvline(i*0.04)
##pl.plot(overlap160mmSpline[:,0], plot)

##for plot in postls1150mSplineVariable:
##    pl.plot(plot[:,0], plot[:,1])
pl.legend(loc="upper right")
pl.xlabel("Frequency (GHz)", fontsize="16.0")
pl.ylabel("$\Re{}e(Z_{\parallel})$ ($\Omega$/m)", fontsize="16.0")
pl.xlim(0,1.0)
pl.ylim(-10,150)
pl.show()
pl.clf()

##pl.plot(overlap160mmSpline[:,0], tempt[1])
##for i in range(0,int(1/0.04)):
##    pl.axvline(i*0.04)
##for i in range(0,20):
##    pl.axvline((i*C)/(4*(2.8))/10**9)
##pl.plot(powLossFreq60mm[:,0], powLossFreq60mm[:,1], "r-", label="L$_{overlap}$=60mm")
##pl.plot(powLossFreq70mm[:,0], powLossFreq70mm[:,1], "b-", label="L$_{overlap}$=70mm")
##pl.plot(powLossFreq80mm[:,0], powLossFreq80mm[:,1], "k-", label="L$_{overlap}$=80mm")
##pl.plot(powLossFreq90mm[:,0], powLossFreq90mm[:,1], "g-", label="L$_{overlap}$=90mm")
##pl.plot(powLossFreq100mmBigger[:,0], powLossFreq100mmBigger[:,1], "m-", label="L$_{overlap}$=100mm")
##pl.plot(powLossFreq110mmBigger[:,0], powLossFreq110mmBigger[:,1], "k--", label="L$_{overlap}$=110mm")
##pl.plot(powLossFreq160mm[:,0], powLossFreq160mm[:,1], "r--", label="L$_{overlap}$=160mm")

##pl.plot(powLossFreq107mmStepout3mm[:,0], powLossFreq107mmStepout3mm[:,1], "m-", label="L$_{overlap}$=107mm, stepout3mm")
##pl.plot(powLossFreqoverlap107mmStepout2mm1mmStep2GHz[:,0], powLossFreqoverlap107mmStepout2mm1mmStep2GHz[:,1], "k-", label="L$_{overlap}$=107mm, 1mmstep")
##pl.plot(powLossFreqoverlap107mmStepout2mm1mmStep[:,0], powLossFreqoverlap107mmStepout2mm1mmStep[:,1], "b-", label="L$_{overlap}$=107mm, 1mmstep")


pl.plot(powLossCum65mm[:,0], powLossFreq65mm[:,1], "r-", label="L$_{overlap}$=65mm")
pl.plot(powLossCum75mm[:,0], powLossFreq75mm[:,1], "b-", label="L$_{overlap}$=75mm")
pl.plot(powLossCum85mm[:,0], powLossFreq85mm[:,1], "k-", label="L$_{overlap}$=85mm")
pl.plot(powLossCum95mm[:,0], powLossFreq95mm[:,1], "g-", label="L$_{overlap}$=95mm")
pl.plot(powLossCum105mm[:,0], powLossFreq105mm[:,1], "g-", label="L$_{overlap}$=105mm")

##pl.plot(powLossFreqoverlap107mmStepout2mm1mmStep2GHz[:,0], powLossFreqoverlap107mmStepout2mm1mmStep2GHz[:,1], "k-", label="1mm Stepout Air L$_{overlap}$=107mm")
##pl.plot(powLossFreqoverlap107mmStepout2mm1mmStep2GHzCera[:,0], powLossFreqoverlap107mmStepout2mm1mmStep2GHzCera[:,1], "m-", label="1mm Stepout Ceramic L$_{overlap}$=107mm")
##pl.plot(powLossFreqpostls1150m3mmStep[:,0], powLossFreqpostls1150m3mmStep[:,1], 'r-', label="Square L$_{overlap}$=107mm")
##pl.plot(powLossFreqoverlap107mmStepout3mm1mmDisplace2GHz[:,0], powLossFreqoverlap107mmStepout3mm1mmDisplace2GHz[:,1], "b-", label="1mm Stepout offset L$_{overlap}$=107mm")
##pl.plot(powLossFreqpostls1150m[:,0], powLossFreqpostls1150m[:,1], 'g-', label="Post LS1 L$_{overlap}$=107mm")

pl.plot(powLossFreqpostls130m117mm[:,0], powLossFreqpostls130m117mm[:,1], "c-", label="Post LS1 L$_{overlap}$=117mm")
pl.plot(powLossFreqpostls250m117mm[:,0], powLossFreqpostls250m117mm[:,1], "m-", label="Post LS2 L$_{overlap}$=117mm")
pl.plot(powLossFreqpostls250m70mm[:,0], powLossFreqpostls250m70mm[:,1], "m-", label="Post LS2 L$_{overlap}$=70mm")
##pl.plot(powLossFreqstepOutHLLHC107mm[:,0], powLossFreqstepOutHLLHC107mm[:,1], "g-")
##pl.plot(powLossFreqstepOutHLLHC65mm[:,0], powLossFreqstepOutHLLHC65mm[:,1])

##pl.xlim(0,1.2)
pl.legend(loc="upper right")
pl.xlabel("Frequency (GHz)", fontsize="16.0")
pl.ylabel("Power Loss (W)", fontsize="16.0")
pl.show()
pl.clf()

pl.plot(powLossCum65mm[:,0], powLossCum65mm[:,1]/2.45, "r-", label="L$_{overlap}$=65mm")
pl.plot(powLossCum75mm[:,0], powLossCum75mm[:,1]/2.45, "b-", label="L$_{overlap}$=75mm")
pl.plot(powLossCum85mm[:,0], powLossCum85mm[:,1]/2.45, "k-", label="L$_{overlap}$=85mm")
pl.plot(powLossCum95mm[:,0], powLossCum95mm[:,1]/2.45, "g-", label="L$_{overlap}$=95mm")
pl.plot(powLossCum105mm[:,0], powLossCum105mm[:,1]/2.45, "g-", label="L$_{overlap}$=105mm")
##pl.plot(powLossCum60mm[:,0], powLossCum60mm[:,1], "r-", label="L$_{overlap}$=60mm")
##pl.plot(powLossCum70mm[:,0], powLossCum70mm[:,1], "b-", label="L$_{overlap}$=70mm")
##pl.plot(powLossCum80mm[:,0], powLossCum80mm[:,1], "k-", label="L$_{overlap}$=80mm")
##pl.plot(powLossCum90mm[:,0], powLossCum90mm[:,1], "g-", label="L$_{overlap}$=90mm")
##pl.plot(powLossCum100mmBigger[:,0], powLossCum100mmBigger[:,1], "m-", label="L$_{overlap}$=100mm")
##pl.plot(powLossCum110mmBigger[:,0], powLossCum110mmBigger[:,1], "k--", label="L$_{overlap}$=110mm")
##pl.plot(powLossCum160mm[:,0], powLossCum160mm[:,1], "r--", label="L$_{overlap}$=160mm")
##pl.plot(powLossCum107mmStepout3mm[:,0], powLossCum107mmStepout3mm[:,1], "m-", label="L$_{overlap}$=107mm, stepout3mm")
##pl.plot(powLossCumoverlap107mmStepout2mm1mmStep2GHz[:,0],powLossCumoverlap107mmStepout2mm1mmStep2GHz[:,1], "k-", label="L$_{overlap}$=107mm, 1mmstep")
##pl.plot(powLossCumoverlap107mmStepout2mm1mmStep[:,0], powLossCumoverlap107mmStepout2mm1mmStep[:,1], "b-", label="L$_{overlap}$=107mm, 1mmstep")
##pl.plot(powLossCumoverlap107mmStepout3mm1mmDisplace2GHz[:,0], powLossCumoverlap107mmStepout3mm1mmDisplace2GHz[:,1], "b--", label="L$_{overlap}$=107mm, 3mmstep, 1mm offset")
##
##pl.plot(powLossCumpostls1150m3mmStep[:,0], powLossCumpostls1150m3mmStep[:,1], 'g-', label="L$_{overlap}$=105mm, 3mm step")
##pl.plot(powLossCumpostls1150m[:,0], powLossCumpostls1150m[:,1], 'r-', label="L$_{overlap}$=105mm, Post-LS1")
##pl.plot(powLossCumpostls1150mLowFreq[:,0], powLossCumpostls1150mLowFreq[:,1], 'g--', label="L$_{overlap}$=105mm, Post-LS1")



##pl.plot(powLossCumpostls1150m[:,0], powLossCumpostls1150m[:,1], 'c-', label="Post LS1 L$_{overlap}$=107mm")
##pl.plot(powLossCumoverlap107mmStepout2mm1mmStep2GHz[:,0], powLossCumoverlap107mmStepout2mm1mmStep2GHz[:,1], "k-", label="1mm Stepout Air L$_{overlap}$=107mm")
##pl.plot(powLossCumoverlap107mmStepout2mm1mmStep2GHzCera[:,0], powLossCumoverlap107mmStepout2mm1mmStep2GHzCera[:,1], "m-", label="1mm Stepout Ceramic L$_{overlap}$=107mm")
##pl.plot(powLossCumpostls1150m3mmStep[:,0], powLossCumpostls1150m3mmStep[:,1], 'r-', label="Square L$_{overlap}$=107mm")
##pl.plot(powLossCumoverlap107mmStepout3mm1mmDisplace2GHz[:,0], powLossCumoverlap107mmStepout3mm1mmDisplace2GHz[:,1], "b-", label="1mm Stepout offset L$_{overlap}$=107mm")

pl.plot(powLossCumpostls130m117mm[:,0], powLossCumpostls130m117mm[:,1], "c-", label="Post LS1 L$_{overlap}$=117mm")
pl.plot(powLossCumpostls250m117mm[:,0], powLossCumpostls250m117mm[:,1], "m-", label="Post LS1 L$_{overlap}$=117mm")
pl.plot(powLossCumpostls250m70mm[:,0], powLossCumpostls250m70mm[:,1], "m-", label="Post LS1 L$_{overlap}$=70mm")
##pl.plot(powLossCumstepOutHLLHC107mm[:,0], powLossCumstepOutHLLHC107mm[:,1], "g-")
##pl.plot(powLossCumstepOutHLLHC65mm[:,0], powLossCumstepOutHLLHC65mm[:,1])

##pl.xlim(0,1.2)
pl.legend(loc="upper left")
pl.xlabel("Frequency (GHz)", fontsize="16.0")
pl.ylabel("Cumultive Power Loss (W)", fontsize="16.0")
pl.show()
pl.clf()

##print powLossCumpostls130m117mm[:,0]
##print powLossCumpostls130m117mm[:,1]

ax1 = pl.subplot(111)
pl.plot(overlap60mmDat[:,0], overlap60mmDat[:,1])
pl.plot(overlap70mmDat[:,0], overlap70mmDat[:,1])
pl.plot(overlap80mmDat[:,0], overlap80mmDat[:,1])
pl.plot(overlap90mmDat[:,0], overlap90mmDat[:,1])
pl.plot(overlap160mmDat[:,0], overlap160mmDat[:,1])
##pl.plot(overlap60mmSpline[:,0], overlap60mmSpline[:,1])
##pl.plot(overlap70mmSpline[:,0], overlap70mmSpline[:,1])
##pl.plot(overlap80mmSpline[:,0], overlap80mmSpline[:,1])
##pl.plot(overlap90mmSpline[:,0], overlap90mmSpline[:,1])
##pl.plot(overlap160mmSpline[:,0], overlap160mmSpline[:,1])
pl.xlabel("Frequency (GHz)", fontsize="16.0")
pl.ylabel("$\Re{}e(Z_{\parallel})$ ($\Omega$)", fontsize="16.0")
pl.ylim(0,500)
ax2 = ax1.twinx()
pl.plot(powLossFreq60mm[:,0], powLossFreq60mm[:,1], "r-")
pl.plot(powLossFreq70mm[:,0], powLossFreq70mm[:,1], "b-")
pl.plot(powLossFreq80mm[:,0], powLossFreq80mm[:,1], "k-")
pl.plot(powLossFreq90mm[:,0], powLossFreq90mm[:,1], "g-")
pl.plot(powLossFreq160mm[:,0], powLossFreq160mm[:,1], "m-")
for i in range(1,30):
    pl.axvline(i*0.04)
pl.ylabel("Power Loss (W)", fontsize="16.0")
##pl.show()
pl.clf()

ax1 = pl.subplot(111)
pl.plot(overlap60mmDat[:,0], overlap60mmDat[:,1])
pl.plot(overlap70mmDat[:,0], overlap70mmDat[:,1])
pl.plot(overlap80mmDat[:,0], overlap80mmDat[:,1])
pl.plot(overlap90mmDat[:,0], overlap90mmDat[:,1])
pl.plot(overlap160mmDat[:,0], overlap160mmDat[:,1])
##pl.plot(overlap60mmSpline[:,0], overlap60mmSpline[:,1])
##pl.plot(overlap70mmSpline[:,0], overlap70mmSpline[:,1])
##pl.plot(overlap80mmSpline[:,0], overlap80mmSpline[:,1])
##pl.plot(overlap90mmSpline[:,0], overlap90mmSpline[:,1])
##pl.plot(overlap160mmSpline[:,0], overlap160mmSpline[:,1])
pl.xlabel("Frequency (GHz)", fontsize="16.0")
pl.ylabel("$\Re{}e(Z_{\parallel})$ ($\Omega$)", fontsize="16.0")
pl.ylim(0,500)
ax2 = ax1.twinx()
pl.plot(powLossCum60mm[:,0], powLossCum60mm[:,1], "r--", label="L$_{overlap}$=60mm")
pl.plot(powLossCum70mm[:,0], powLossCum70mm[:,1], "b--", label="L$_{overlap}$=70mm")
pl.plot(powLossCum80mm[:,0], powLossCum80mm[:,1], "k--", label="L$_{overlap}$=80mm")
pl.plot(powLossCum90mm[:,0], powLossCum90mm[:,1], "g--", label="L$_{overlap}$=90mm")
pl.plot(powLossCum160mm[:,0], powLossCum160mm[:,1], "m--", label="L$_{overlap}$=160mm")
##pl.plot(powLossCum107mmStepout3mm[:,0], powLossCum107mmStepout3mm[:,1], "m-", label="L$_{overlap}$=107mm, stepout3mm")
##pl.plot(powLossCumoverlap107mmStepout2mm1mmStep2GHz[:,0],powLossCumoverlap107mmStepout2mm1mmStep2GHz[:,1], "k-", label="L$_{overlap}$=107mm, 1mmstep")
pl.plot(powLossCumoverlap107mmStepout2mm1mmStep[:,0], powLossCumoverlap107mmStepout2mm1mmStep[:,1], "b-", label="L$_{overlap}$=107mm, 1mmstep")

pl.plot(powLossCumpostls1150m3mmStep[:,0], powLossCumpostls1150m3mmStep[:,1], 'g-', label="L$_{overlap}$=105mm, 3mm step")
pl.plot(powLossCumpostls1150m[:,0], powLossCumpostls1150m[:,1], 'r-', label="L$_{overlap}$=105mm, Post-LS1")
pl.plot(powLossCumpostls1150mLowFreq[:,0], powLossCumpostls1150mLowFreq[:,1], 'g--', label="L$_{overlap}$=105mm, Post-LS1")
pl.xlim(0,1.5)
pl.legend(loc="upper left")
##pl.show()
pl.clf()

count=0
##for entry in powLossCumpostls1150mVar:
##    pl.plot(entry[:,0], entry[:,1], label=str(count*1)+"MHz Shift")
##    count+=1
##count=0
##for entry in powLossCumoverlap107mmStepout2mm1mmStep2GHzVar:
##    pl.plot(entry[:,0], entry[:,1], label=str(count*1)+"MHz Shift")
##    count+=1
##count=0
##for entry in powLossCumoverlap107mmStepout2mm1mmStepVar:
##    pl.plot(entry[:,0], entry[:,1], label=str(count*1)+"MHz Shift")
##    count+=1
for entry in  powLossCumoverlap107mmStepout3mmVar:
    pl.plot(entry[:,0], entry[:,1], label=str(count*1)+"MHz Shift")
    count+=1
##pl.legend(loc="upper left")
##pl.show()
pl.clf()

##print powLossCumoverlap107mmStepout3mmVar
print len(powLossTotaloverlap107mmStepout3mmVar)

freqShift = sp.array(range(0,41,1))
##print powLossTotalpostls1150mVar 
pl.plot(freqShift, powLossTotalpostls1150mVar, label="Post LS1")
pl.plot(freqShift, powLossTotaloverlap107mmStepout2mm1mmStep2GHzVar, label="2mm Stepout, 1mm whole length")
##pl.plot(freqShift, powLossTotaloverlap107mmStepout2mm1mmStepVar)
pl.plot(freqShift,powLossCumpostls1117mmVar[:,-1,1], label="PostLS1 117mm")
pl.plot(freqShift,powLossCumoverlap107mmStepout3mmVar[:,-1,1], label="3mm stepout centred")
pl.plot(freqShift,powLossCumoverlap107mmStepout3mm1mmDisplace2GHzVar[:,-1,1], label="3mm stepout centred, 1mm offset")
pl.xlabel("Frequency Shift (MHz)", fontsize=16.0)
pl.ylabel("Power Loss (W)", fontsize=16.0)
pl.legend(loc="upper left")
##pl.show()
pl.clf()


lenArr = sp.array([60,70,80,90,100,110,160])
powArr = sp.array([powLossTotal60mm,powLossTotal70mm,powLossTotal80mm,powLossTotal90mm,powLossTotal100mmBigger,powLossTotal110mmBigger,powLossTotal160mm])


pl.plot(lenArr, powArr, "k-")
pl.xlabel("$L_{overlap}$ (mm)", fontsize="16.0")
pl.ylabel("Power Loss (W)", fontsize="16.0")
##pl.show()
pl.clf()
