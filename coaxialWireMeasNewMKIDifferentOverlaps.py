import csv, time, os, sys
import scipy as sp
import numpy as np
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack
from scipy import signal

####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

######### Define importing, analysis functions #########

c=299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005
lWire = 3.551
lDUT = 2.45
resCopper = 17.24*10.0**-9
resOuter = 1/(3.3*10.0**7)
ZChar = 60*sp.log(dAper/rWire)

def overlapFreqMKI(lenOverlap, harmonic, permitivitty,delta):
    return harmonic*C/(2*permitivitty**0.5*(lenOverlap+delta))

def importDatSimMeas(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
    datStore = []
    for entry in tempDat[1:]:
        temp = map(float, entry.split(','))
        datStore.append(temp)

    return datStore

def analSingReSimMeas(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    reStore = []
    temp = []
##    Z0 = 60*sp.log(1.27*apPipe/r_wire)
    Z0=50.0
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(-2*Z0/lenTot*sp.log(10**(data[i,j]/20))) 
        temp.append(tempLin)
    return temp

def fileImport(tarFile):
    readPart=open(tarFile, 'r+')
    datUnPro = readPart.readlines()
    readPart.close()
    freqDat=[]
    qLoaded=[]
    s21DB=[]
    for row in datUnPro[20:]:
        freqDat.append(float(row.split(",")[0]))
        qLoaded.append(float(row.split(",")[1]))
        s21DB.append(float(row.split(",")[2]))

    return sp.array(freqDat), sp.array(qLoaded), sp.array(s21DB)

def skinDepth(freq, cond):
    return (1/(sp.pi*freq*mu0*cond))**0.5

def resImpGet(tarArr1,tarArr2,tarArr3,attenCableLen):

    freqList, qTotal, transCoeff = tarArr1, tarArr2, tarArr3

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(C*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCable = attenCableLen*freqList*10.0**-9
    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor

    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper-attenCable)/(attenCopper)

    return freqList, zMeas/lDUT

def resImpGetFromFile(targetFile):

    freqList, qTotal, transCoeff = fileImport(targetFile)

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(c*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor


    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper)/attenCopper

    return freqList, zMeas/lDUT

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def extract_dat2013(file_name):
    data = open(file_name, "r+")
    tar = data.readlines()
    temp=[]
    for row in tar[2:]:
        row = map(float, row.split())
        temp.append(row)
    data.close()

    return temp[:-1]

def sParaRead(fileTar, lenDUT, col):
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        phase = row[col+1]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360    

        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        import_dat = [row[0], row[col], sp.radians(phase), -s21Pec]
        data.append(import_dat)
        last = phase

    return data

def readS21FromVNA(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[6:]:
        temp=line.split("\t")
        outputData.append(map(float,temp))
    return outputData

def readS21FromCSV(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[3:]:
        temp=line.split(",")
        outputData.append(map(float,temp))
    return outputData

def impAnalysisSingle(fileTar, lenDUT, r_wire, r_pipe, Zc):
    ###### Analyses a S2P file for measurement of longitudinal impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        phase = row[4]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360


        import_dat = [row[0], -2*Zc*sp.log(linDat), -2*Zc*(sp.radians(phase)+s21Pec)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDUT), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDUT))]
        data.append(import_dat)
        last = phase

    return data

def impAnalysisTwo(fileTar, lenDut, r_wire, r_pipe, d_wire, Zc):
    ###### Analyses a S2P file for measurement of dipole impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter = 0.0
    last = 1e8

    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDut/C
        phase = row[4]-counter
        if phase>last and (phase-last)>180:
            count+=1
            counter+=360
            pgase-=360
        import_dat = [row[0], -2*Zc*sp.log(linDat)*C/(2*sp.pi*row[0]*d_wire**2), -2*Zc*(sp.radians(phase)+s21Pec)*C/(2*sp.pi*row[0]*d_wire**2)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDut)*C/(row[0]*d_wire**2), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDut))*C/(row[0]*d_wire**2)]
        data.append(import_dat)
    return data

def impTransPara(dispWire, measImp, wireErr):
##### Takes an array of longitudinal measurements and returns the transverse impedance
    quadFunc = lambda p, x: p[0] + x * p[1] + x**2 * p[2]
    errFunc = lambda p, x, Z: (Z-quadFunc(p,x))

    totTrans = []
    longComp = []

    for i in range(0,len(measImp[0,:,1])):
        pinit = [1.0, -1.0, 1.0]
        ZerrRe = measImp[:,i,1]*wireErr
        pReal, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,1]), full_output = 0)
        pinit = [1.0, -1.0, 1.0]
        ZerrIm = measImp[:,i,2]*wireErr
        pImag, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,2]), full_output = 0)
        totTrans.append([pReal[2]*C/(2*sp.pi*measImp[0,i,0]), pImag[2]*C/(2*sp.pi*measImp[0,i,0])])
        longComp.append([pReal[0], pImag[0]])

    return longComp, totTrans

def quadImp(totalTrans, dipTrans, axis):
######## Returns the quadrupolar impedance from total transverse and dipolar data
    quadImp = []
    for i in range(0,len(totalTrans)):
        if axis == "horz":
            quadImp.append([totalTrans[i,1]+dipTrans[i,1],totalTrans[i,2]+dipTrans[i,2]])
        elif axis == "vert":
            quadImp.append([totalTrans[i,1]-dipTrans[i,1],totalTrans[i,2]-dipTrans[i,2]])
    return quadImp


def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def peakFitSimulations(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 50
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 50 and y_val[current_max_x]>2:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [0.2, 10.0, 10.0]
        lower_bound = res[0]-50
        upper_bound = res[0]+50
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def peakFitMeasurements(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 0
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 2 and y_val[current_max_x]>10:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [res[1], 4.0, data[res[0],1]]
        lower_bound = res[0]-4
        upper_bound = res[0]+3
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2)

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2
                                                    
def logToLin(data):
    return 10**(data/20)

def linToLog(data):
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

def overLapFreq(overlap, fringeLen, harmonic):
    return harmonic*C/(2*10**0.5*(overlap+fringeLen))

def heatLossBunByBunGauss(imp, bunLen, nBun, nParticles, freqRev):
    prodImpSpec = imp[1,:]*gaussProf(imp[0,:]*2*sp.pi,bunLen)**2
    curImpTime=prodImpSpec
    enLossPerBunch=2*(1.6*10**-19*nParticles*freqRev)**2*(curImpTime.sum())
    pLossBeam = enLossPerBunch*nBun**2
    pLossBeamFreq = 2*(1.6*10**-19*nParticles*freqRev)**2*nBun**2*curImpTime
    print pLossBeam, pLossBeamFreq
    return pLossBeam, pLossBeamFreq, gaussProf(imp[0,:],bunLen)**2

def extract_dat2013_MultipleSweeps(file_name):
    data = open(file_name, "r+")
    tar = data.readlines()
    temp=[]
    bigTemp = []
    count=2
    while count<len(tar):
        try:
            row = map(float, tar[count].split())
            temp.append(row)
            count+=1
        except:
            bigTemp.append(temp)
            temp=[]
            count+=3
    data.close()
    bigTemp.append(temp)
    return bigTemp

def freqRLC(L, C, harmonic):
    return harmonic/(L*C)**0.5/(2*sp.pi)

def heatingValGauss(impArr, beamCur, bunSpac, bunLen):
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac/10**9,impArr[-1,0],impArr[-1,0]*10**9*bunSpac)
    splineFitHeatingImp = SplineFitImp(freqListHeating)
    heatingTotalPart = []
    for i in range(0,len(splineFitHeatingImp)):
        heatingTotalPart.append([freqListHeating[i], abs(2*splineFitHeatingImp[i]*(beamCur**2)*gaussProf(freqListHeating[i]*10.0**9, bunLen))])
    heatingTotalPart=sp.array(heatingTotalPart)
    cumHeating = []
    for i in range(0,len(heatingTotalPart)):
        cumHeating.append([heatingTotalPart[i,0],sum(heatingTotalPart[:i,1])])
    cumHeating=sp.array(cumHeating)
    return heatingTotalPart[:,1].sum(), heatingTotalPart, cumHeating

def impFromFreq(transMisDat, Zch):
    impStore=[]
    for i in range(0,len(transMisDat[:,0])):
        impStore.append([transMisDat[i,0], logImpFormula(logToLin(transMisDat[i,1]),1, Zch)])
    return sp.array(impStore)

def findPeakFreqImp(impData):
    test=(impData[:,1]-impData[-1,1])
    widths=sp.arange(5,30)
    peakList=signal.find_peaks_cwt(test,widths, min_snr=1.5)
    ##print peakList
    peakStore=[]
    for entry in peakList:
        peakStore.append([impData[entry,0], impData[entry,1]])
    peakStore=sp.array(peakStore)
    return peakStore

def findPeakFreqImpFitted(freqData, impData):
    test=(impData[:]-impData[-1])
    widths=sp.arange(1,10)
    peakList=signal.find_peaks_cwt(test,widths, min_snr=4.0)
    ##print peakList
    peakStore=[]
    for entry in peakList:
        peakStore.append([freqData[entry], impData[entry]])
    peakStore=sp.array(peakStore)
    return peakStore

def singArrayFromMany(*arrInput):
    storeSmall = []
    storeBig = []
    for i in range(0,len(arrInput[0])):
        for j in range(0,len(list(arrInput))):
            storeSmall.append(arrInput[j][i])
        storeBig.append(storeSmall)
        storeSmall=[]
    return sp.array(storeBig)

def bunchTrainProfTime(bunSpacing, trainSpacing, bunInTrain, trainsInMachine, blankBunSpaces, timeRes):
    spaceCount = ((trainsInMachine*bunInTrain*bunSpacing)+(trainSpacing*(trainsInMachine))+bunSpacing/2+(bunInTrain*trainsInMachine*bLength)+bunSpacing*blankBunSpaces)/timeRes
    print 1/((trainsInMachine*bunInTrain*bunSpacing)+(trainSpacing*(trainsInMachine))+bunSpacing/2+(bunInTrain*trainsInMachine*bLength)+bunSpacing*blankBunSpaces)
    ampListTime = [0 for i in range(0,int(spaceCount))]
##    print spaceCount
    timing = []
    for i in range(0,len(ampListTime)):
        timing.append(i*timeRes)

    bunLimit = int((bLength)/timeRes)

    for i in range(0,trainsInMachine):
        for j in range(0,bunInTrain):
            midPoint = int(((j*bunSpacing)+(i*trainsInMachine*trainSpacing)+(i*bunInTrain*bunSpacing)+bLength/2)/timeRes)
    ##        print midPoint, bunLimit, i
            for k in range(midPoint-bunLimit, midPoint+bunLimit):
                try:
                    ampListTime[k] = gaussProfTime(abs(k-midPoint)*timeRes, bLength, 20)
                except:
    ##                print i, j, j
                    pass

    return timing, ampListTime

def bunchTrainProfFreq(bunSpacing, trainSpacing, bunInTrain, trainsInMachine, blankBunSpaces, freqRes):
    timeSpace = 0.02*10.0**-9
    timing, timeSig = bunchTrainProfTime(bunSpacing, trainSpacing, bunInTrain, trainsInMachine, blankBunSpaces, timeSpace)
    N = len(timing)
    f = 1/timeSpace*sp.r_[0:(N/2)]/N
    n= len(f)
    linFreqSpec = sp.fft(timeSig)
    linFreqSpec = linFreqSpec[0:n]/sp.amax(linFreqSpec)
    return f, abs(linFreqSpec)

length = 2.88
f_rev = C/27000.0
nBunches = 2808
qPart = 1.6*10.0**-19
nPart=1.15*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=1.001*10.0**-9
testImp = [250*10**6, 10.0, 10000]
bunchCur=nPart*qPart*f_rev

##### 25ns Spec LHC

bunSpacing25nsLHC = 24.97*10.0**-9
trainSpacing25nsLHC = 8*bunSpacing25nsLHC
bunInTrain25nsLHC = 288
trainsInMachine25nsLHC = 10
blankBunSpaces25nsLHC = 610

##### 50ns Spec LHC

bunSpacing50nsLHC = 2*24.97*10.0**-9
trainSpacing50nsLHC = 224.6*10.0**-9
bunInTrain50nsLHC = 144
trainsInMachine50nsLHC = 9
blankBunSpaces50nsLHC = 306

##### 25ns Spec FCC

bunSpacing25nsFCC = 24.97*10.0**-9
trainSpacing25nsFCC = 8*bunSpacing25nsFCC
bunInTrain25nsFCC = 132
trainsInMachine25nsFCC = 90
blankBunSpaces25nsFCC = 610

overlap59mm99mmDat = extract_dat2013_MultipleSweeps("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/HLLHCProposal/59-99mmOverLap-Full-HLLHCProposal-real-40m-wake.txt")
overlap59mmImp = sp.array(overlap59mm99mmDat[0][:-1])
overlap69mmImp = sp.array(overlap59mm99mmDat[1][:-1])
overlap79mmImp = sp.array(overlap59mm99mmDat[2][:-1])
overlap89mmImp = sp.array(overlap59mm99mmDat[3][:-1])
overlap99mmImp = sp.array(overlap59mm99mmDat[4][:-1])

powLossTotal59mm, powLossFreq59mm, powLossCum59mm = heatingValGauss(overlap59mmImp, bCur, 25.0*10**-9, bLength)
powLossTotal69mm, powLossFreq69mm, powLossCum59mm = heatingValGauss(overlap69mmImp, bCur, 25.0*10**-9, bLength)
powLossTotal79mm, powLossFreq79mm, powLossCum59mm = heatingValGauss(overlap79mmImp, bCur, 25.0*10**-9, bLength)
powLossTotal89mm, powLossFreq89mm, powLossCum59mm = heatingValGauss(overlap89mmImp, bCur, 25.0*10**-9, bLength)
powLossTotal99mm, powLossFreq99mm, powLossCum59mm = heatingValGauss(overlap99mmImp, bCur, 25.0*10**-9, bLength)


overlap65mm105mmDat = extract_dat2013_MultipleSweeps("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/3mmStepOut1mmOffsetAtBottom30mWake2GHzOverlaps/65-105mmOverlap-longitudinal-impedance-real.txt")
overlap65mmImp = sp.array(overlap65mm105mmDat[0][:-1])
overlap75mmImp = sp.array(overlap65mm105mmDat[1][:-1])
overlap85mmImp = sp.array(overlap65mm105mmDat[2][:-1])
overlap95mmImp = sp.array(overlap65mm105mmDat[3][:-1])
overlap105mmImp = sp.array(overlap65mm105mmDat[4][:-1])

topDir = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/"
postls130m117mmDat = sp.array(extract_dat2013(topDir+"POSTLS1Overlap117mm/longitudinal-impedance-real.txt"))
Mesh54milDat = sp.array(extract_dat2013(topDir+"HLLHCProposal/80mmHigherMeshDens/longitudinal-impedance-real-dft.txt"))

topDir = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/"
postls130m117mmNormDat = sp.array(extract_dat2013(topDir+"powLossTo2GHz/longitudinal-impedance-real.txt"))
postls130m117mmGNDAirDat = sp.array(extract_dat2013(topDir+"powLossTo2GHzGNDAir/longitudinal-impedance-real.txt"))
postls130m117mmCapAirDat = sp.array(extract_dat2013(topDir+"powLossTo2GHzCapAir/longitudinal-impedance-real.txt"))
postls130m117mmCapAir60mDat = sp.array(extract_dat2013(topDir+"powLossTo2GHzCapAir/longitudinal-impedance-real-60m.txt"))

Freq117mmOverlap, Imp117mmOverlap = sp.array(resImpGetFromFile("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/11-03-14/mki09-tank2-cr05/resShort"))

Freq129mmOverlap, Imp129mmOverlap = sp.array(resImpGetFromFile("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/129.4mm/resonator4"))
Freq119mmOverlap, Imp119mmOverlap = sp.array(resImpGetFromFile("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/119.4mm/resonator"))
Freq99mmOverlap, Imp99mmOverlap = sp.array(resImpGetFromFile("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/99.4mm/resonator"))
Freq89mmOverlap, Imp89mmOverlap = sp.array(resImpGetFromFile("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/89.4mm/resonator"))
Freq79mmOverlap, Imp79mmOverlap = sp.array(resImpGetFromFile("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/79.4mm/Resonator"))
Freq69mmOverlap, Imp69mmOverlap = sp.array(resImpGetFromFile("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/69.4mm/resonator"))
Freq59mmOverlap, Imp59mmOverlap = sp.array(resImpGetFromFile("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/59.4mm/resonator"))

fileTar129mmGating = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/129.4mm/S21GATING.S2P"
s21129mmGating = sp.array(readS21FromVNA(fileTar129mmGating))

fileTar119mmGating = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/119.4mm/S21FREQGATING.S2P"
s21119mmGating = sp.array(readS21FromVNA(fileTar119mmGating))
fileTar119mmGatingCSV = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/119.4mm/S21FREQGATING.CSV"
s21119mmGatingCSV = sp.array(readS21FromCSV(fileTar119mmGatingCSV))

fileTar99mmGating = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/99.4mm/S21FREQGATING.S2P"
s2199mmGating = sp.array(readS21FromVNA(fileTar99mmGating))
fileTar99mmGatingCSV = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/99.4mm/S21FREQGATING.CSV"
s2199mmGatingCSV = sp.array(readS21FromCSV(fileTar99mmGatingCSV))

fileTar89mmGating = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/89.4mm/S21FREQGATING.S2P"
s2189mmGating = sp.array(readS21FromVNA(fileTar89mmGating))
fileTar89mmGatingCSV = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/89.4mm/S21FREQGATING.CSV"
s2189mmGatingCSV = sp.array(readS21FromCSV(fileTar89mmGatingCSV))

fileTime89mmCSV = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/89.4mm/S21TIMEIMP.CSV"
s21Time89mmCSV = sp.array(readS21FromCSV(fileTime89mmCSV))

fileTar79mmGatingCSV = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/79.4mm/PORTS1+2-IMPULSE-S11.CSV"
s2179mmGatingCSV = sp.array(readS21FromCSV(fileTar79mmGatingCSV))

fileTar69mmGatingCSV = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/69.4mm/S21FREQTIMEGATING.CSV"
s2169mmGatingCSV = sp.array(readS21FromCSV(fileTar69mmGatingCSV))

fileTar59mmGatingCSV = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/59.4mm/S21FREQGATING.CSV"
s2159mmGatingCSV = sp.array(readS21FromCSV(fileTar59mmGatingCSV))

freqListHeating = sp.linspace(40,2000,2000/40)
freqListSpread = sp.linspace(1,2000,2000)
SplineFit59mm = interp.InterpolatedUnivariateSpline(Freq59mmOverlap/10**6, Imp59mmOverlap)
splineFitHeating59mm = SplineFit59mm(freqListHeating)
splineFit59mm = SplineFit59mm(freqListSpread)
SplineFit69mm = interp.InterpolatedUnivariateSpline(Freq69mmOverlap/10**6, Imp69mmOverlap)
splineFitHeating69mm = SplineFit69mm(freqListHeating)
splineFit69mm = SplineFit69mm(freqListSpread)
SplineFit79mm = interp.InterpolatedUnivariateSpline(Freq79mmOverlap/10**6, Imp79mmOverlap)
splineFitHeating79mm = SplineFit79mm(freqListHeating)
splineFit79mm = SplineFit79mm(freqListSpread)
SplineFit89mm = interp.InterpolatedUnivariateSpline(Freq89mmOverlap/10**6, Imp89mmOverlap)
splineFitHeating89mm = SplineFit89mm(freqListHeating)
splineFit89mm = SplineFit89mm(freqListSpread)
SplineFit99mm = interp.InterpolatedUnivariateSpline(Freq99mmOverlap/10**6, Imp99mmOverlap)
splineFitHeating99mm = SplineFit99mm(freqListHeating)
splineFit99mm = SplineFit99mm(freqListSpread)
SplineFit119mm = interp.InterpolatedUnivariateSpline(Freq119mmOverlap/10**6, Imp119mmOverlap)
splineFitHeating119mm = SplineFit119mm(freqListHeating)
splineFit119mm = SplineFit119mm(freqListSpread)
SplineFit117mm = interp.InterpolatedUnivariateSpline(Freq117mmOverlap/10**6, Imp117mmOverlap)
splineFitHeating117mm = SplineFit117mm(freqListHeating)
splineFit117mm = SplineFit117mm(freqListSpread)
SplineFit117mmSim = interp.InterpolatedUnivariateSpline(postls130m117mmDat[:,0]*10**3, postls130m117mmDat[:,1])
splineFitHeating117mmSim = SplineFit117mmSim(freqListHeating)

heatingTotal59mm = 0.0
heatingFreq59mm = []
heatingCumFreq59mm = []
heatingTotal69mm = 0.0
heatingFreq69mm = []
heatingCumFreq69mm = []
heatingTotal79mm = 0.0
heatingFreq79mm = []
heatingCumFreq79mm = []
heatingTotal89mm = 0.0
heatingFreq89mm = []
heatingCumFreq89mm = []
heatingTotal99mm = 0.0
heatingFreq99mm = []
heatingCumFreq99mm = []
heatingTotal119mm = 0.0
heatingFreq119mm = []
heatingCumFreq119mm = []
heatingTotal117mm = 0.0
heatingFreq117mm = []
heatingCumFreq117mm = []
heatingTotal117mmSim = 0.0
heatingFreq117mmSim = []
heatingCumFreq117mmSim = []

for i in range(0,len(splineFitHeating99mm)):
    heatingTotal59mm+=2*splineFitHeating59mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreq59mm.append(2*splineFitHeating59mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreq59mm.append(sp.sum(heatingFreq59mm))
    heatingTotal69mm+=2*splineFitHeating69mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreq69mm.append(2*splineFitHeating69mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreq69mm.append(sp.sum(heatingFreq69mm))
    heatingTotal79mm+=2*splineFitHeating79mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreq79mm.append(2*splineFitHeating79mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreq79mm.append(sp.sum(heatingFreq79mm))
    heatingTotal89mm+=2*splineFitHeating89mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreq89mm.append(2*splineFitHeating89mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreq89mm.append(sp.sum(heatingFreq89mm))
    heatingTotal99mm+=2*splineFitHeating99mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreq99mm.append(2*splineFitHeating99mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreq99mm.append(sp.sum(heatingFreq99mm))
    heatingTotal119mm+=2*splineFitHeating119mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreq119mm.append(2*splineFitHeating119mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreq119mm.append(sp.sum(heatingFreq119mm))
    heatingTotal117mm+=2*splineFitHeating117mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreq117mm.append(2*splineFitHeating117mm[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreq117mm.append(sp.sum(heatingFreq117mm))
    heatingTotal117mmSim+=2*splineFitHeating117mmSim[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreq117mmSim.append(2*splineFitHeating117mmSim[i]*(bCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength))
    heatingCumFreq117mmSim.append(sp.sum(heatingFreq117mmSim))

print heatingTotal59mm    
print heatingTotal69mm
print heatingTotal79mm
print heatingTotal89mm
print heatingTotal99mm
print heatingTotal119mm
print heatingTotal117mm
print heatingTotal117mmSim

pl.plot(freqListHeating, heatingFreq59mm, label="L$_{Overlap}$=59mm")
pl.plot(freqListHeating, heatingFreq69mm, label="L$_{Overlap}$=69mm")
pl.plot(freqListHeating, heatingFreq79mm, label="L$_{Overlap}$=79mm")
pl.plot(freqListHeating, heatingFreq89mm, label="L$_{Overlap}$=89mm")
pl.plot(freqListHeating, heatingFreq99mm, label="L$_{Overlap}$=99mm")
pl.plot(freqListHeating, heatingFreq117mm, label="L$_{Overlap}$=117mm")
pl.plot(freqListHeating, heatingFreq117mmSim, label="L$_{Overlap}$=117mm Sim")
pl.plot(freqListHeating, heatingFreq119mm, label="L$_{Overlap}$=119mm")
pl.legend(loc="upper left")
##pl.show()
pl.clf()

pl.plot(freqListHeating, heatingCumFreq59mm, label="L$_{Overlap}$=59mm")
pl.plot(freqListHeating, heatingCumFreq69mm, label="L$_{Overlap}$=69mm")
pl.plot(freqListHeating, heatingCumFreq79mm, label="L$_{Overlap}$=79mm")
pl.plot(freqListHeating, heatingCumFreq89mm, label="L$_{Overlap}$=89mm")
pl.plot(freqListHeating, heatingCumFreq99mm, label="L$_{Overlap}$=99mm")
pl.plot(freqListHeating, heatingCumFreq117mm, label="L$_{Overlap}$=117mm")
pl.plot(freqListHeating, heatingCumFreq117mmSim, label="L$_{Overlap}$=117mm Sim")
pl.plot(freqListHeating, heatingCumFreq119mm, label="L$_{Overlap}$=119mm")
pl.legend(loc="upper left")
pl.xlabel("Frequency (MHz)", fontsize=16.0)
pl.ylabel("Power Loss (W/m)", fontsize=16.0)
pl.show()
pl.clf()

##freqRes = 10.0**5
##print (trainsInMachine25nsLHC*bunInTrain25nsLHC*bunSpacing25nsLHC)+(trainSpacing25nsLHC*(trainsInMachine25nsLHC))+bunSpacing25nsLHC/2+(bunInTrain25nsLHC*trainsInMachine25nsLHC*bLength)+bunSpacing25nsLHC*blankBunSpaces25nsLHC
##print 1/((trainsInMachine25nsLHC*bunInTrain25nsLHC*bunSpacing25nsLHC)+(trainSpacing25nsLHC*(trainsInMachine25nsLHC))+bunSpacing25nsLHC/2+(bunInTrain25nsLHC*trainsInMachine25nsLHC*bLength)+bunSpacing25nsLHC*blankBunSpaces25nsLHC)
##
##freqSpecFreq25ns, freqSpecAmp25ns = bunchTrainProfFreq(bunSpacing25nsLHC, trainSpacing25nsLHC, bunInTrain25nsLHC, trainsInMachine25nsLHC, blankBunSpaces25nsLHC, freqRes)
##fitDat = SplineFit117mm(freqSpecFreq25ns/10**6)
##
##conv = fitDat*(freqSpecAmp25ns)**2
##print (trainsInMachine25nsLHC*bunInTrain25nsLHC*bunSpacing25nsLHC)+(trainSpacing25nsLHC*(trainsInMachine25nsLHC))+bunSpacing25nsLHC/2+(bunInTrain25nsLHC*trainsInMachine25nsLHC*bLength)+bunSpacing25nsLHC*blankBunSpaces25nsLHC
##print 1/((trainsInMachine25nsLHC*bunInTrain25nsLHC*bunSpacing25nsLHC)+(trainSpacing25nsLHC*(trainsInMachine25nsLHC))+bunSpacing25nsLHC/2+(bunInTrain25nsLHC*trainsInMachine25nsLHC*bLength)+bunSpacing25nsLHC*blankBunSpaces25nsLHC)
##print (freqSpecFreq25ns[1]-freqSpecFreq25ns[0])
##print (2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0]), len(freqSpecFreq25ns)
##print (f_rev*qPart*nPart*bunInTrain25nsLHC*trainsInMachine25nsLHC)**2/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])*integrate.simps(conv[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])],freqSpecFreq25ns[:int(2.0*10**9)/(freqSpecFreq25ns[1]-freqSpecFreq25ns[0])])
##print (f_rev*qPart*nPart*bunInTrain25nsLHC*trainsInMachine25nsLHC)**2*sp.sum(conv[:184076])


#### Peak Finding  ######
impCSV119mm = impFromFreq(s21119mmGatingCSV, 230)
peaks119mm = findPeakFreqImp(impCSV119mm)
impCSV99mm = impFromFreq(s2199mmGatingCSV, 230)
peaks99mm = findPeakFreqImp(impCSV99mm)
impCSV89mm = impFromFreq(s2189mmGatingCSV, 230)
peaks89mm = findPeakFreqImp(impCSV89mm)
impCSV79mm = impFromFreq(s2179mmGatingCSV, 230)
peaks79mm = findPeakFreqImp(impCSV79mm)
impCSV69mm = impFromFreq(s2169mmGatingCSV, 230)
peaks69mm = findPeakFreqImp(impCSV69mm)
impCSV59mm = impFromFreq(s2159mmGatingCSV, 230)
peaks59mm = findPeakFreqImp(impCSV59mm)


peaks99mmSpread = findPeakFreqImpFitted(freqListSpread, splineFit99mm)
peaks89mmSpread = findPeakFreqImpFitted(freqListSpread, splineFit89mm)
peaks79mmSpread = findPeakFreqImpFitted(freqListSpread, splineFit79mm)
peaks69mmSpread = findPeakFreqImpFitted(freqListSpread, splineFit69mm)
peaks59mmSpread = findPeakFreqImpFitted(freqListSpread, splineFit59mm)

dat99mmSing = singArrayFromMany(Freq99mmOverlap, Imp99mmOverlap)
peakFitRes99mm, freqPeak = peakFitMeasurements(dat99mmSing)
peakFitRes99mm=sp.array(peakFitRes99mm)
errBars99mm = peakFitRes99mm[:,0]/peakFitRes99mm[:,1]

dat89mmSing = singArrayFromMany(Freq89mmOverlap, Imp89mmOverlap)
peakFitRes89mm, freqPeak = peakFitMeasurements(dat89mmSing)
peakFitRes89mm=sp.array(peakFitRes89mm)
errBars89mm = peakFitRes89mm[:,0]/peakFitRes89mm[:,1]

dat79mmSing = singArrayFromMany(Freq79mmOverlap, Imp79mmOverlap)
peakFitRes79mm, freqPeak = peakFitMeasurements(dat79mmSing)
peakFitRes79mm=sp.array(peakFitRes79mm)
errBars79mm = peakFitRes79mm[:,0]/peakFitRes79mm[:,1]

dat69mmSing = singArrayFromMany(Freq69mmOverlap, Imp69mmOverlap)
peakFitRes69mm, freqPeak = peakFitMeasurements(dat69mmSing)
peakFitRes69mm=sp.array(peakFitRes69mm)
errBars69mm = peakFitRes69mm[:,0]/peakFitRes69mm[:,1]

dat59mmSing = singArrayFromMany(Freq59mmOverlap, Imp59mmOverlap)
peakFitRes59mm, freqPeak = peakFitMeasurements(dat59mmSing)
peakFitRes59mm=sp.array(peakFitRes59mm)
errBars59mm = peakFitRes59mm[:,0]/peakFitRes59mm[:,1]

pl.plot(peakFitRes99mm[:,0]/10**9,peakFitRes99mm[:,1],"bo")
pl.plot(peakFitRes89mm[:,0]/10**9,peakFitRes89mm[:,1],"ro")
pl.plot(peakFitRes79mm[:,0]/10**9,peakFitRes79mm[:,1],"ko")
##pl.show()
pl.clf()

##pl.semilogx()
##print peakDataStashLong
##impResultsFreqLong, impResultsImpLong=resImpGet(peakDataStashLong[:,2]*10**9,peakDataStashLong[:,1], linToLog(peakDataStashLong[:,0]), 0.000)
##pl.plot(impResultsFreqLong/10**9, impResultsImpLong/2.97, 'rx', label="Manual Measurement of Q-factors from VNA, MKI+6m cable")
##pl.plot(overlap65mmImp[:,0], overlap65mmImp[:,1]/2.97)
##pl.plot(overlap75mmImp[:,0], overlap75mmImp[:,1]/2.97)
##pl.plot(overlap85mmImp[:,0], overlap85mmImp[:,1]/2.97)
##pl.plot(overlap95mmImp[:,0], overlap95mmImp[:,1]/2.97)
##pl.plot(overlap105mmImp[:,0], overlap105mmImp[:,1]/2.97)
##pl.plot(overlap59mmImp[:,0], overlap59mmImp[:,1]/2.97, "m--")
##pl.plot(overlap69mmImp[:,0], overlap69mmImp[:,1]/2.97, "g--")
##pl.plot(overlap79mmImp[:,0], overlap79mmImp[:,1]/2.97, "k--")
##pl.plot(overlap89mmImp[:,0], overlap89mmImp[:,1]/2.97, "b--")
##pl.plot(overlap99mmImp[:,0], overlap99mmImp[:,1]/2.97, "r--")
##pl.plot(Mesh54milDat[:,0], Mesh54milDat[:,1])
pl.plot(postls130m117mmDat[:,0], postls130m117mmDat[:,1]/2.97, "k-", label="Post LS1 L$_{overlap}$=117mm")
pl.plot(postls130m117mmGNDAirDat[:,0], postls130m117mmGNDAirDat[:,1]/2.97, "b-", label="Post LS1 L$_{overlap}$=117mm, GND Air")
pl.plot(postls130m117mmCapAirDat[:,0], postls130m117mmCapAirDat[:,1]/2.97, "r-", label="Post LS1 L$_{overlap}$=117mm, Cap Air")
##pl.plot(postls130m117mmCapAir60mDat[:,0], postls130m117mmCapAir60mDat[:,1]/2.97, "g-", label="Post LS1 L$_{overlap}$=117mm, Cap Air 60m")
##pl.plot(Freq119mmOverlap/10**9, Imp119mmOverlap, label="L$_{overlap}$=119mm")
##pl.plot(impCSV119mm[:,0]/10**9, impCSV119mm[:,1]/2.45, label="L$_{overlap}$=119mm Not-Resonant")
##pl.plot(Freq117mmOverlap/10**9, Imp117mmOverlap, label="Post-LS1 L$_{overlap}$=117mm")
##pl.plot(Freq99mmOverlap/10**9, Imp99mmOverlap, label="L$_{overlap}$=99mm")
##pl.plot(impCSV99mm[:,0]/10**9, impCSV99mm[:,1]/2.45, label="L$_{overlap}$=99mm Not-Resonant")
##pl.plot(Freq89mmOverlap/10**9, Imp89mmOverlap, label="L$_{overlap}$=89mm")
##pl.plot(impCSV89mm[:,0]/10**9, impCSV89mm[:,1]/2.45, label="L$_{overlap}$=89mm Not-Resonant")
##pl.plot(Freq79mmOverlap/10**9, Imp79mmOverlap, label="L$_{overlap}$=79mm")
##pl.plot(impCSV79mm[:,0]/10**9, impCSV79mm[:,1]/2.45, label="L$_{overlap}$=79mm Not-Resonant")

##pl.plot(impCSV119mm[:,0]/10**9, impCSV119mm[:,1]/2.45, "r-", label="L$_{overlap}$=119mm Gating")
##pl.plot(Freq99mmOverlap/10**9, Imp99mmOverlap, "rx", markersize=16.0, label="L$_{overlap}$=99mm")
##pl.plot(impCSV99mm[:,0]/10**9, impCSV99mm[:,1]/2.45, "r-", label="L$_{overlap}$=99mm Gating")
##pl.plot(Freq89mmOverlap/10**9, Imp89mmOverlap, "bx", markersize=16.0, label="L$_{overlap}$=89mm")
##pl.plot(impCSV89mm[:,0]/10**9, impCSV89mm[:,1]/2.45, "b-", label="L$_{overlap}$=89mm Gating")
##pl.plot(Freq79mmOverlap/10**9, Imp79mmOverlap, "kx", markersize=16.0, label="L$_{overlap}$=79mm")
##pl.plot(impCSV79mm[:,0]/10**9, impCSV79mm[:,1]/2.45, "k-", label="L$_{overlap}$=79mm Not-Resonant")
##pl.plot(Freq69mmOverlap/10**9, Imp69mmOverlap, "gx", markersize=16.0, label="L$_{overlap}$=69mm")
##pl.plot(impCSV69mm[:,0]/10**9, impCSV69mm[:,1]/2.45, "g-", label="L$_{overlap}$=69mm Gating")
##pl.plot(Freq59mmOverlap/10**9, Imp59mmOverlap, "mx", markersize=16.0, label="L$_{overlap}$=59mm")
##pl.plot(impCSV59mm[:,0]/10**9, impCSV59mm[:,1]/2.45, "m-", label="L$_{overlap}$=59mm Gating")

pl.plot(impCSV119mm[:,0]/10**9, impCSV119mm[:,1]/2.45, "r-", label="L$_{overlap}$=119mm Gating")
pl.plot(peaks119mm[:,0]/10**9, peaks119mm[:,1]/2.45, "rx", markersize=16.0, label="L$_{overlap}$=99mm")
pl.plot(impCSV99mm[:,0]/10**9, impCSV99mm[:,1]/2.45, "b-", label="L$_{overlap}$=99mm Gating")
pl.plot(peaks99mm[:,0]/10**9, peaks99mm[:,1]/2.45, "bx", markersize=16.0, label="L$_{overlap}$=99mm")
pl.plot(impCSV89mm[:,0]/10**9, impCSV89mm[:,1]/2.45, "k-", label="L$_{overlap}$=89mm Gating")
pl.plot(peaks89mm[:,0]/10**9, peaks89mm[:,1]/2.45, "kx", markersize=16.0, label="L$_{overlap}$=89mm")
pl.plot(impCSV79mm[:,0]/10**9, impCSV79mm[:,1]/2.45, "k-", label="L$_{overlap}$=79mm Not-Resonant")
pl.plot(peaks79mm[:,0]/10**9, peaks79mm[:,1]/2.45, "yx", markersize=16.0, label="L$_{overlap}$=79mm")
pl.plot(impCSV69mm[:,0]/10**9, impCSV69mm[:,1]/2.45, "g-", label="L$_{overlap}$=69mm Gating")
pl.plot(peaks69mm[:,0]/10**9, peaks69mm[:,1]/2.45, "gx", markersize=16.0, label="L$_{overlap}$=69mm")
pl.plot(impCSV59mm[:,0]/10**9, impCSV59mm[:,1]/2.45, "m-", label="L$_{overlap}$=59mm Gating")
pl.plot(peaks59mm[:,0]/10**9, peaks59mm[:,1]/2.45, "mx", markersize=16.0, label="L$_{overlap}$=59mm")
##pl.plot(s2159mmGatingCSV[:,0]/10**9, (logImpFormula(logToLin(s2159mmGatingCSV[:,1]),1,230)-logImpFormula(logToLin(s2159mmGatingCSV[:,1]),1,230)[-1])/2.45, "m-", label="L$_{overlap}$=59mm Gating")


pl.legend(loc="upper right")
##for x in range(1,5):
##    print freqRLC(0.5*10**-9,148.0*10**-12,x)/10**9
##for x in range(1,5):
##    pl.axvline(overlapFreqMKI(0.06,x,10,0.006)/10**9)
##pl.ylim(0,150)
##pl.xlim(0,2)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$\Re{}e(Z_{\parallel})$ ($\Omega$/m)", fontsize=16.0)
pl.show()
pl.clf()


pl.plot(Freq99mmOverlap/10**9, Imp99mmOverlap, label="L$_{overlap}$=99mm")
pl.plot(freqListSpread/10**3, splineFit99mm)
pl.plot(peaks99mmSpread[:,0]/10**3,peaks99mmSpread[:,1], "bx", markersize=16.0)
pl.legend(loc="upper right")
##for x in range(1,5):
##    print freqRLC(0.5*10**-9,148.0*10**-12,x)/10**9
##for x in range(1,5):
##    pl.axvline(overlapFreqMKI(0.06,x,10,0.006)/10**9)
##pl.ylim(0,150)
##pl.xlim(0,2)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$\Re{}e(Z_{\parallel})$ ($\Omega$/m)", fontsize=16.0)
##pl.show()
pl.clf()

##pl.plot(s2199mmGating[:,0], s2199mmGating[:,3], label="L$_{overlap}$=99mm")
pl.plot(s2199mmGatingCSV[:,0], s2199mmGatingCSV[:,1], label="L$_{overlap}$=99mm")
##pl.plot(s21119mmGating[:,0], s21119mmGating[:,3], label="L$_{overlap}$=119mm")
pl.plot(s21119mmGatingCSV[:,0], s21119mmGatingCSV[:,1], label="L$_{overlap}$=119mm")
##pl.plot(s21129mmGating[:,0], s21129mmGating[:,3], label="L$_{overlap}$=129mm")
pl.legend(loc="lower left")
##pl.show()
pl.clf()

pl.plot(s21Time89mmCSV[:,0], s21Time89mmCSV[:,1])
##pl.show()
pl.clf()

s21Freq89mmCSV=fftPack.fftshift(fftPack.fft(s21Time89mmCSV[:,1]))
##print s21Freq89mmCSV
pl.plot(s21Freq89mmCSV)
##pl.show()
pl.clf()

comps21119mmNotGated = []
for i in range(0,len(s21119mmGating[:,3])):
    comps21119mmNotGated.append(complex(s21119mmGating[i,3]*sp.cos(s21119mmGating[i,4]),s21119mmGating[i,3]*sp.sin(s21119mmGating[i,4])))
comps21119mmNotGated  = sp.array(comps21119mmNotGated)

pl.plot(comps21119mmNotGated)
##pl.plot(comps21119mmNotGated.imag())
##pl.show()
pl.clf()

ifftSample = fftPack.ifft(comps21119mmNotGated)
ifftSamplePure=np.fft.ifft(logToLin(s21119mmGating[:,3]))
##pl.plot(ifftSample)
pl.plot(ifftSamplePure)
##pl.show()
pl.clf()


lenOverlap = [59,69,79,89,99,119]
powLoss=[heatingTotal59mm,heatingTotal69mm,heatingTotal79mm,heatingTotal89mm,heatingTotal99mm,heatingTotal119mm]

lenOverlapSims = [59,69,79,89,99]
powLossSims=[powLossTotal59mm/lDUT,powLossTotal69mm/lDUT,powLossTotal79mm/lDUT,powLossTotal89mm/lDUT,powLossTotal99mm/lDUT]

pl.plot(lenOverlap, powLoss, "k+", markersize=16.0)
pl.plot(lenOverlap, powLoss, "k-")
##pl.plot(lenOverlapSims, powLossSims, "r+", markersize=16.0)
##pl.plot(lenOverlapSims, powLossSims, "r-")
##pl.plot(117,heatingTotal117mmSim, "ro")
pl.xlabel("L$_{overlap}$ (mm)", fontsize=20.0)
pl.ylabel("Total Power Loss (W/m)", fontsize=20.0)
pl.show()
pl.clf()

print len(peaks99mmSpread), len(peaks89mmSpread), len(peaks79mmSpread), len(peaks69mmSpread), len(peaks59mmSpread)

qFactorAvg = 20.0
xErr99mm = peaks99mmSpread[:,0]/10.0**3/qFactorAvg/2

##pl.plot(peaks99mmSpread[:,0], peaks99mmSpread[:,1], "rx", markersize=16.0)
pl.errorbar(peaks99mmSpread[:,0]/10.0**3, peaks99mmSpread[:,1], xerr=xErr99mm)
##pl.plot(peaks89mmSpread[:,0], peaks89mmSpread[:,1], "bx", markersize=16.0)
##pl.plot(peaks79mmSpread[:,0], peaks79mmSpread[:,1], "kx", markersize=16.0)
##pl.plot(peaks69mmSpread[:,0], peaks69mmSpread[:,1], "mx", markersize=16.0)
##pl.plot(peaks59mmSpread[:,0], peaks59mmSpread[:,1], "gx", markersize=16.0)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
##pl.show()
pl.clf()

lengthSpaff=[]
for i in range(0,len(peakFitRes99mm[:,0])):
    lengthSpaff.append(99)
lengthSpaff=sp.array(lengthSpaff)

lengthSpaff2=[]
for i in range(0,len(peakFitRes89mm[:,0])):
    lengthSpaff2.append(89)
lengthSpaff2=sp.array(lengthSpaff2)

lengthSpaff3=[]
for i in range(0,len(peakFitRes79mm[:,0])):
    lengthSpaff3.append(79)
lengthSpaff3=sp.array(lengthSpaff3)

lengthSpaff4=[]
for i in range(0,len(peakFitRes69mm[:,0])):
    lengthSpaff4.append(69)
lengthSpaff4=sp.array(lengthSpaff4)

lengthSpaff5=[]
for i in range(0,len(peakFitRes59mm[:,0])):
    lengthSpaff5.append(59)
lengthSpaff5=sp.array(lengthSpaff5)

peak1 = sp.array([peaks119mm[0,0]/10**9,peaks99mm[0,0]/10**9,peaks89mm[0,0]/10**9,peaks69mm[0,0]/10**9,peaks59mm[0,0]/10**9])
peak2 = sp.array([peaks119mm[1,0]/10**9,peaks99mm[1,0]/10**9,peaks89mm[1,0]/10**9,peaks69mm[1,0]/10**9,peaks59mm[2,0]/10**9])
peak3 = sp.array([peaks119mm[2,0]/10**9,peaks99mm[2,0]/10**9,peaks89mm[2,0]/10**9,peaks69mm[2,0]/10**9,peaks59mm[3,0]/10**9])

freqRange = sp.linspace(1,2*10**9, 2000)
gaussProfPlot = gaussProf(freqRange, bLength)

errPeaks2 = peak2/qFactorAvg
errPeaks3 = peak3/qFactorAvg
lengths = sp.array([119,99,89,69,59])
freqListResonances = sp.linspace(40*10**6,2*10**9,((2*10**9)/(40*10**6)))

fig=pl.figure()
ax1 = fig.add_subplot(1,1,1)
##pl.plot(peak1, lengths)
pl.errorbar(peak2, lengths, xerr=errPeaks2, fmt="-o")
pl.errorbar(peak3, lengths, xerr=errPeaks3, fmt="-o")
##pl.errorbar(peakFitRes99mm[:,0]/10**9, lengthSpaff,xerr=errBars99mm/10**9, fmt="o")
##pl.errorbar(peakFitRes89mm[:,0]/10**9, lengthSpaff2,xerr=errBars89mm/10**9, fmt="o")
##pl.errorbar(peakFitRes79mm[:,0]/10**9, lengthSpaff3,xerr=errBars79mm/10**9, fmt="o")
##pl.errorbar(peakFitRes69mm[:,0]/10**9, lengthSpaff4,xerr=errBars69mm/10**9, fmt="o")
##pl.errorbar(peakFitRes59mm[:,0]/10**9, lengthSpaff5,xerr=errBars59mm/10**9, fmt="o")
##for i in range(1,50,1):
##    pl.axvline(i*0.04)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$L_{overla^p}$ (mm)", fontsize=16.0)
ax2 = ax1.twinx()
pl.plot(freqRange/10**9, linToLog(gaussProfPlot))
pl.ylabel("Beam Power Spectrum, normalised", fontsize=16.0)
pl.show()
pl.clf()
