import scipy as sp
import pylab as pl
import csv,time
from PIL import Image
import images2swf

start = time.time()
directory = "C:/Users/hday/Documents/PhD/1st_Year_09-10/Software/materials/" #Directory of data
data_file = "with_plane_eps.csv"      # File containing data


input_file = open(directory+data_file, "r+")

data_raw = csv.reader(input_file)       #Assign and open input file

y_data= []    # Create blank for data
wire_t = 5*10**-4                  # Radius of wire
seperation = 1.6*10**-2            # Seperation of plates

for row in data_raw:
    for i in range(0,len(row)):
        row[i] = float(row[i])
    y_data.append(row)   # Read in data

input_file.close()                              # close input file

y_array = sp.array(y_data)

##x_data = sp.linspace(-9,9,7)                    # Create x data
x_data_fit = sp.linspace(0,2000,2000)
coefficients = []          # Blank for coeffs
image_list=[]                                   # frames for image
long_plot = sp.random.random((1,1))             # array for long data



poly_coeffs_epsprime = sp.polyfit(y_array[:,0], y_array[:,1], 40)  # coeffs for one data set
poly_coeffs_epsdoubleprime = sp.polyfit(y_array[:,0], y_array[:,2], 40)  # coeffs for one data set
print poly_coeffs_epsprime, poly_coeffs_epsdoubleprime                               # Verify its not garbage
y_fit_epsprime = sp.polyval(poly_coeffs_epsprime, x_data_fit)     # fitted line for plot
y_fit_epsdoubleprime = sp.polyval(poly_coeffs_epsdoubleprime, x_data_fit)
pl.plot(y_array[:,0], y_array[:,1], 'k.')                # Plot raw_data
pl.plot(x_data_fit, y_fit_epsprime, 'r-',markersize=25)                    # plot fitted curve
pl.xlabel("Displacement (mm)",fontsize = 16)                  #Label axes
pl.ylabel("Impedance (Ohms/m)",fontsize = 16)
pl.show()
pl.savefig(directory+str(i)+"1.png")                  # Save fig for film
pl.clf()                                        # Clear figure
pl.plot(y_array[:,0], y_array[:,2], 'k.')                # Plot raw_data
pl.plot(x_data_fit, y_fit_epsdoubleprime, 'r-',markersize=25)                    # plot fitted curve
pl.xlabel("Displacement (mm)",fontsize = 16)                  #Label axes
pl.ylabel("Impedance (Ohms/m)",fontsize = 16)
pl.show()
pl.savefig(directory+str(i)+"2.png")                  # Save fig for film
pl.clf()                                        # Clear figure

##    coefficients = sp.vstack((coefficients, poly_coeffs)) #Store coeffs
##    if i%10==0:                                       # Here to prevent memory overflow
##        image_list.append(Image.open(directory+str(i)+".png"))
##    long_plot = sp.vstack((long_plot, y_data[i][3]))
##
##pl.plot(long_plot[i], 'k.')                # Plot raw_data
##pl.xlabel("Frequency (MHz)")                  #Label axes
##pl.ylabel("Impedance (Ohms)")
##pl.savefig(directory+"longitudinal_imp.png")                  # Save fig for film
##pl.clf()
##
##output_file = open(directory+"output_imag.csv", 'w+')           #Assign and open output file
##
##data_out = csv.writer(output_file, delimiter=',', lineterminator = '\n')
##data_out.writerows(coefficients)
##
##images2swf.writeSwf(directory+"movie_imag.swf", image_list,3)
##output_file.close() 

print time.time() - start

