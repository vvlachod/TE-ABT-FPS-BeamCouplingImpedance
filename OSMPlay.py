import numpy as np
import dask.array as da
from dask.dot import dot_graph
import pandas as pd
import dask.dataframe as dd
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap

##def

nrows = sum(1 for _ in open('E:/POICsv/POIWorld_2015.10.11/POIWorld.csv'))
print nrows

a=np.random.randn(1000)
b = a*4
b_min = b.min()
print b_min

a2 = da.from_array(a, chunks=200)
b2 = a2*4
b2_min = b2.min()
print b2_min
print b2_min.compute()

data = pd.read_csv('E:/POICsv/POIWorld_2015.10.11/POIWorld.csv', nrows = 5)
data.columns

columns = ["name", "amenity", "Longitude", "Latitude"]
data = dd.read_csv('E:/POICsv/POIWorld_2015.10.11/POIWorld.csv', usecols=columns)
with_name = data[data.name.notnull()]
with_amenity = data[data.amenity.notnull()]

is_starbucks = with_name.name.str.contains("[Ss]tarbucks")
is_dunkin = with_name.name.str.contains('[Dd]unkin')

starbucks = with_name[is_starbucks]
dunkin = with_name[is_dunkin]

print dd.compute(starbucks.name.count(), dunkin.name.count())
