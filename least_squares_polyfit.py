import scipy as sp
import pylab as pl
import csv,time
from PIL import Image
import images2swf

start = time.time()
directory = "C:/Users/hday/Documents/PhD/1st_Year_09-10/Data/frequency-dom-test/data/power-dis/maxwell-data/geometry_2/" #Directory of data
data_file = "y_scan_real.csv"      # File containing data


input_file = open(directory+data_file, "r+")

data_raw = csv.reader(input_file)       #Assign and open input file

y_data= []    # Create blank for data
wire_t = 5*10**-4                  # Radius of wire
seperation = 1.6*10**-2            # Seperation of plates

for row in data_raw:
    for i in range(0,len(row)):
        row[i] = float(row[i])
    y_data.append(row)  # Read in data

input_file.close()                              # close input file
y_data = sp.array(y_data)

x_data = sp.array([-3,-2,-1,1,2,3])                    # Create x data
x_data_fit = sp.linspace(-3,3,100)
coefficients = []          # Blank for coeffs
image_list=[]                                   # frames for image
long_plot = sp.random.random((1,1))             # array for long data



for i in range(0,len(y_data)):
    poly_coeffs = sp.polyfit(x_data, y_data[i], 2)  # coeffs for one data set
    #print poly_coeffs                               # Verify its not garbage
    y_fit = sp.polyval(poly_coeffs, x_data_fit)     # fitted line for plot

    pl.plot(x_data, y_data[i], 'k.')                # Plot raw_data
    pl.plot(x_data_fit, y_fit, 'r-',markersize=25)                    # plot fitted curve
    pl.xlabel("Displacement (mm)",fontsize = 16)                  #Label axes
    pl.ylabel("Impedance (Ohms/m)",fontsize = 16)
    pl.savefig(directory+str(i)+".png")                  # Save fig for film
    pl.clf()                                        # Clear figure
    coefficients.append(poly_coeffs) #Store coeffs
    if i%50==0:                                       # Here to prevent memory overflow
        image_list.append(Image.open(directory+str(i)+".png"))
    long_plot = sp.vstack((long_plot, y_data[i][3]))

pl.plot(long_plot[i], 'k.')                # Plot raw_data
pl.xlabel("Displacement (mm)")                  #Label axes
pl.ylabel("Impedance ($\Omega/m$)")
pl.savefig(directory+"longitudinal_imp.png")                  # Save fig for film
pl.clf()

output_file = open(directory+"output_real_vert.csv", 'w+')           #Assign and open output file

data_out = csv.writer(output_file, delimiter=',', lineterminator = '\n')
data_out.writerows(coefficients)

images2swf.writeSwf(directory+"movie_real_vert.swf", image_list,3)
output_file.close() 

print time.time() - start

