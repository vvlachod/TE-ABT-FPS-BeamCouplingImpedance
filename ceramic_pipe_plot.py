import scipy as sp
import pylab as pl
import time, os, sys, csv, time
import scipy.integrate as inte

parentDir = "E:/PhD/1st_Year_09-10/Data/mki-heating/ceramic-tube/"

file_50mm_overlap = parentDir+"50mm-overlap/longitudinal-impedance.csv"
file_50mm_overlap_sec = parentDir+"50mm-overlap/longitudinal-impedance-sec.csv"
file_60mm_overlap = parentDir+"60mm-overlap/longitudinal-impedance.csv"
file_70mm_overlap = parentDir+"70mm-overlap/longitudinal-impedance.csv"
file_80mm_overlap = parentDir+"80mm-overlap/longitudinal-impedance.csv"
file_90mm_overlap = parentDir+"90mm-overlap/longitudinal-impedance.csv"
file_ferr_50mm_overlap = parentDir+"ceramic-with-ferrite-50mm-overlap/longitudinal-impedance.csv"
file_ferr_70mm_overlap = parentDir+"ceramic-with-ferrite-70mm-overlap/longitudinal-impedance.csv"
file_ferr_80mm_overlap = parentDir+"ceramic-with-ferrite-80mm-overlap/longitudinal-impedance.csv"
file_em_ferr_80mm_overlap = parentDir+"ceramic-embedded-ferrite-80mm-overlap/longitudinal-impedance.csv"
file_tube2ghz = parentDir+"measurements/CopperTube_NoCeramic-2GHz-impedance-results.csv"
file_tube_cerr = parentDir+"measurements/Resonator-cu-pipe-ceramic-insert-impedance-results.csv"

input_file = open(file_50mm_overlap, 'r+')
tar = csv.reader(input_file, delimiter=",")
imp_data = []
i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        imp_data.append(row)
    i+=1
cumulative = []
dat50mmOver = sp.array(imp_data)

input_file = open(file_50mm_overlap_sec, 'r+')
tar = csv.reader(input_file, delimiter=",")
imp_data = []
i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        imp_data.append(row)
    i+=1
cumulative = []
dat50mmOverSec = sp.array(imp_data)

input_file = open(file_60mm_overlap, 'r+')
tar = csv.reader(input_file, delimiter=",")
imp_data = []
i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        imp_data.append(row)
    i+=1
cumulative = []
dat60mmOver = sp.array(imp_data)

input_file = open(file_70mm_overlap, 'r+')
tar = csv.reader(input_file, delimiter=",")
imp_data = []
i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        imp_data.append(row)
    i+=1
cumulative = []
dat70mmOver = sp.array(imp_data)

input_file = open(file_80mm_overlap, 'r+')
tar = csv.reader(input_file, delimiter=",")
imp_data = []
i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        imp_data.append(row)
    i+=1
cumulative = []
dat80mmOver = sp.array(imp_data)

input_file = open(file_90mm_overlap, 'r+')
tar = csv.reader(input_file, delimiter=",")
imp_data = []
i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        imp_data.append(row)
    i+=1
cumulative = []
dat90mmOver = sp.array(imp_data)

input_file = open(file_ferr_50mm_overlap, 'r+')
tar = csv.reader(input_file, delimiter=",")
imp_data = []
i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        imp_data.append(row)
    i+=1
cumulative = []
datFerr50mmOver = sp.array(imp_data)

input_file = open(file_ferr_70mm_overlap, 'r+')
tar = csv.reader(input_file, delimiter=",")
imp_data = []
i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        imp_data.append(row)
    i+=1
cumulative = []
datFerr70mmOver = sp.array(imp_data)

input_file = open(file_ferr_80mm_overlap, 'r+')
tar = csv.reader(input_file, delimiter=",")
imp_data = []
i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        imp_data.append(row)
    i+=1
cumulative = []
datFerr80mmOver = sp.array(imp_data)

input_file = open(file_em_ferr_80mm_overlap, 'r+')
tar = csv.reader(input_file, delimiter=",")
imp_data = []
i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        imp_data.append(row)
    i+=1
cumulative = []
datEmFerr80mmOver = sp.array(imp_data)

input_file = open(file_tube2ghz, 'r+')
tar = csv.reader(input_file, delimiter=",")
imp_data = []
i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        imp_data.append(row)
    i+=1
cumulative = []
dattube2ghz = sp.array(imp_data)

input_file = open(file_tube_cerr, 'r+')
tar = csv.reader(input_file, delimiter=",")
imp_data = []
i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        imp_data.append(row)
    i+=1
cumulative = []
dattube3ghz = sp.array(imp_data)

##pl.semilogx()
##pl.plot(dat50mmOver[:,0] ,dat50mmOver[:,1], 'k-', label = "Strip Length = 950mm")
##pl.plot(dat50mmOverSec[:,0] ,dat50mmOverSec[:,1], 'r-', label = "Strip Length = 950mm, Open")
##pl.plot(dat60mmOver[:,0] ,dat60mmOver[:,1], 'r-', label = "Strip Length = 940mm")
##pl.plot(dat70mmOver[:,0] ,dat70mmOver[:,1], 'b-', label = "Strip Length = 930mm")
##pl.plot(dat80mmOver[:,0] ,dat80mmOver[:,1], 'm-', label = "Strip Length = 920mm")
##pl.plot(dat90mmOver[:,0] ,dat90mmOver[:,1], 'g-', label = "Strip Length = 910mm")
##pl.plot(datFerr50mmOver[:,0] ,datFerr50mmOver[:,1], 'b-', label = "Strip Length = 950mm, Ferr")
##pl.plot(datFerr70mmOver[:,0] ,datFerr70mmOver[:,1], 'r-', label = "Strip Length = 930mm, Ferr")
##pl.plot(datFerr80mmOver[:,0] ,datFerr80mmOver[:,1], 'b-', label = "Strip Length = 920mm, Ferr")
##pl.plot(datEmFerr80mmOver[:,0] ,datEmFerr80mmOver[:,1], 'g-', label = "Strip Length = 920mm, Ferr, Embed")
pl.plot(dattube2ghz[:,0]/10**3 ,dattube2ghz[:,1], 'g-', label = "Measurements - Cu Pipe, 2GHz")
pl.plot(dattube3ghz[:,0]/10**3 ,dattube3ghz[:,1], 'b-', label = "Measurements - Cu Pipe, Ceramic Tube")
pl.grid(linestyle="--", which = "major")
pl.xlabel("Frequency (GHz)", fontsize="16")
pl.ylabel("Real Longitudinal Impedance ($\Omega/m$)", fontsize="16")
pl.legend(loc="upper left")
##pl.axis([0.5, 1,0,10])
pl.show()
pl.clf()
