import csv
import scipy as sp
import pylab as pl

directory = "C:/Users/hday/Documents/PhD/1st_Year_09-10/Software/headtail/data_dump/comp_wake_meth/" #Directory of data
list_bt_no_sc = [directory+"hdtl-bt-no-sc.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,301)]
list_bt_sc = [directory+"hdtl-bt-sc.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,301)]
list_bt_sc_no_bb =  [directory+"hdtl-bt-sc-no-BB.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,301)]
list_at_no_sc = [directory+"hdtl-at-no-sc.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,301)]
list_at_sc = [directory+"hdtl-at-sc.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,301)]
list_at_sc_no_bb =  [directory+"hdtl-at-sc-no-BB.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,301)]


value_desired = 9
big_bugger =[]
data_list_1 = []
for data_raw in list_bt_no_sc:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(float(append_tar[9])*float(append_tar[10]))
    
bt_no_sc=sp.array(data_list_1)



data_list_1 = []
for data_raw in list_bt_sc:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(float(append_tar[9])*float(append_tar[10]))
    
bt_sc=sp.array(data_list_1)
    



data_list_1 = []
for data_raw in list_bt_sc_no_bb:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(float(append_tar[9])*float(append_tar[10]))


bt_sc_no_bb=sp.array(data_list_1)


data_list_1 = []
for data_raw in list_at_no_sc:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(float(append_tar[9])*float(append_tar[10]))
    
at_no_sc=sp.array(data_list_1)



data_list_1 = []
for data_raw in list_at_sc:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(float(append_tar[9])*float(append_tar[10]))
    
at_sc=sp.array(data_list_1)
    



data_list_1 = []
for data_raw in list_at_sc_no_bb:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(float(append_tar[9])*float(append_tar[10]))


at_sc_no_bb=sp.array(data_list_1)



big_bugger = sp.array(big_bugger)

x_values = sp.array(range(1,31))
temp = range(1,301)
for i in range(1, len(temp)):
    temp[i] = float(temp[i])/10
    
x_val_new = sp.array(temp)
pl.plot(x_val_new , bt_no_sc[:], 'k-', label = "Below Transition - NoSC/BB")                    # plot fitted curve
pl.plot(x_val_new , bt_sc[:],'k:', label = "Below Transition - SC/BB")
pl.plot(x_val_new, bt_sc_no_bb[:],'k-.', label = "Below Transition - SC/noBB")
pl.plot(x_val_new , at_no_sc[:], 'r-', label = "Above Transition - NoSC/BB")                    # plot fitted curve
pl.plot(x_val_new , at_sc[:],'r:', label = "Above Transition - SC/BB")
pl.plot(x_val_new, at_sc_no_bb[:],'r-.', label = "Above Transition - SC/noBB")



pl.grid(linestyle="--", which = "major")
pl.xlabel("Bunch Intensity ($10^{10}$)",fontsize = 16)                  #Label axes
pl.ylabel("RMS Longitudinal Emittance $(keVs)$",fontsize = 16)
pl.title("", fontsize = 16)
pl.legend(loc = "upper left")
##pl.axis([0,30,0.0001,0.0005])
pl.savefig(directory+"emittance-comp-alt"+".pdf")                  # Save fig for film
pl.show()
pl.clf()   
