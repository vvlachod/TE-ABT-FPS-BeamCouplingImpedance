import csv, time, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack
import scipy.signal as signal
sys.path.append("../")
import impedance.impedance as imp

####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

######### Define importing, analysis functions #########

c=299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005
lWire = 3.551
lDUT = 2.45
resCopper = 17.24*10.0**-9
resOuter = 1/(3.3*10.0**7)
ZChar = 60*sp.log(dAper/rWire)

def importDatSimMeas(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
    datStore = []
    for entry in tempDat[1:]:
        temp = map(float, entry.split(','))
        datStore.append(temp)

    return datStore

def analSingReSimMeas(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    reStore = []
    temp = []
##    Z0 = 60*sp.log(1.27*apPipe/r_wire)
    Z0=50.0
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(-2*Z0/lenTot*sp.log(10**(data[i,j]/20)))
        temp.append(tempLin)
    return temp

def fileImport(tarFile):
    readPart=open(tarFile, 'r+')
    datUnPro = readPart.readlines()
    readPart.close()
    freqDat=[]
    qLoaded=[]
    s21DB=[]
    for row in datUnPro[20:]:
        freqDat.append(float(row.split(",")[0]))
        qLoaded.append(float(row.split(",")[1]))
        s21DB.append(float(row.split(",")[2]))

    return sp.array(freqDat), sp.array(qLoaded), sp.array(s21DB)

def skinDepth(freq, cond):
    return (1/(sp.pi*freq*mu0*cond))**0.5

def resImpGet(targetFile):

    freqList, qTotal, transCoeff = fileImport(targetFile)

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(c*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor

    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper)/attenCopper

    return freqList, zMeas/lDUT

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def extract_dat2013(file_name):
    data = open(file_name, "r+")
    tar = data.readlines()
    temp=[]
    for row in tar[2:]:
        row = map(float, row.split())
        temp.append(row)
    data.close()

    return temp[:-1]

def sParaRead(fileTar, lenDUT, col):
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        phase = row[col+1]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360

        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        import_dat = [row[0], row[col], sp.radians(phase), -s21Pec]
        data.append(import_dat)
        last = phase

    return data


def impAnalysisSingle(fileTar, lenDUT, r_wire, r_pipe, Zc):
    ###### Analyses a S2P file for measurement of longitudinal impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        phase = row[4]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360


        import_dat = [row[0], -2*Zc*sp.log(linDat), -2*Zc*(sp.radians(phase)+s21Pec)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDUT), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDUT))]
        data.append(import_dat)
        last = phase

    return data

def readS21FromVNA(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[6:]:
        temp=line.split("\t")
        outputData.append(map(float,temp))
    return outputData

def readS21FromCSV(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[3:]:
        temp=line.split(",")
        outputData.append(map(float,temp))
    return outputData

def findPeakFreqImp(impData):
    test=(impData[:,1]-impData[-1,1])
    widths=sp.arange(5,30)
    peakList=signal.find_peaks_cwt(test,widths, min_snr=1.5)
    ##print peakList
    peakStore=[]
    for entry in peakList:
        peakStore.append([impData[entry,0], impData[entry,1]])
    peakStore=sp.array(peakStore)
    return peakStore

def findPeakFreqImpFitted(freqData, impData):
    test=(impData[:]-impData[-1])
    widths=sp.arange(1,10)
    peakList=signal.find_peaks_cwt(test,widths, min_snr=4.0)
    ##print peakList
    peakStore=[]
    for entry in peakList:
        peakStore.append([freqData[entry], impData[entry]])
    peakStore=sp.array(peakStore)
    return peakStore

def singArrayFromMany(*arrInput):
    storeSmall = []
    storeBig = []
    for i in range(0,len(arrInput[0])):
        for j in range(0,len(list(arrInput))):
            storeSmall.append(arrInput[j][i])
        storeBig.append(storeSmall)
        storeSmall=[]
    return sp.array(storeBig)

def impFromFreq(transMisDat, Zch):
    impStore=[]
    for i in range(0,len(transMisDat[:,0])):
        impStore.append([transMisDat[i,0], logImpFormula(logToLin(transMisDat[i,1]),1, Zch)])
    return sp.array(impStore)

def impAnalysisTwo(fileTar, lenDut, r_wire, r_pipe, d_wire, Zc):
    ###### Analyses a S2P file for measurement of dipole impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter = 0.0
    last = 1e8

    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDut/C
        phase = row[4]-counter
        if phase>last and (phase-last)>180:
            count+=1
            counter+=360
            pgase-=360
        import_dat = [row[0], -2*Zc*sp.log(linDat)*C/(2*sp.pi*row[0]*d_wire**2), -2*Zc*(sp.radians(phase)+s21Pec)*C/(2*sp.pi*row[0]*d_wire**2)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDut)*C/(row[0]*d_wire**2), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDut))*C/(row[0]*d_wire**2)]
        data.append(import_dat)
    return data

def impTransPara(dispWire, measImp, wireErr):
##### Takes an array of longitudinal measurements and returns the transverse impedance
    quadFunc = lambda p, x: p[0] + x * p[1] + x**2 * p[2]
    errFunc = lambda p, x, Z: (Z-quadFunc(p,x))

    totTrans = []
    longComp = []

    for i in range(0,len(measImp[0,:,1])):
        pinit = [1.0, -1.0, 1.0]
        ZerrRe = measImp[:,i,1]*wireErr
        pReal, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,1]), full_output = 0)
        pinit = [1.0, -1.0, 1.0]
        ZerrIm = measImp[:,i,2]*wireErr
        pImag, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,2]), full_output = 0)
        totTrans.append([pReal[2]*C/(2*sp.pi*measImp[0,i,0]), pImag[2]*C/(2*sp.pi*measImp[0,i,0])])
        longComp.append([pReal[0], pImag[0]])

    return longComp, totTrans

def quadImp(totalTrans, dipTrans, axis):
######## Returns the quadrupolar impedance from total transverse and dipolar data
    quadImp = []
    for i in range(0,len(totalTrans)):
        if axis == "horz":
            quadImp.append([totalTrans[i,1]+dipTrans[i,1],totalTrans[i,2]+dipTrans[i,2]])
        elif axis == "vert":
            quadImp.append([totalTrans[i,1]-dipTrans[i,1],totalTrans[i,2]-dipTrans[i,2]])
    return quadImp


def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def peakFitSimulations(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []

    lower_bound = 50
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 50 and y_val[current_max_x]>2:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [0.2, 10.0, 10.0]
        lower_bound = res[0]-50
        upper_bound = res[0]+50
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def peakFitMeasurements(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []

    lower_bound = 0
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 2 and y_val[current_max_x]>3:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [res[1], 4.0, data[res[0],1]]
        lower_bound = res[0]-4
        upper_bound = res[0]+3
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2)

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2

def logToLin(data):
    return 10**(data/20)

def linToLog(data):
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

def overLapFreq(overlap, fringeLen, harmonic):
    return harmonic*C/(2*10**0.5*(overlap+fringeLen))

def heatLossBunByBunGauss(imp, bunLen, nBun, nParticles, freqRev):
    prodImpSpec = imp[1,:]*gaussProf(imp[0,:]*2*sp.pi,bunLen)**2
    curImpTime=prodImpSpec
    enLossPerBunch=2*(1.6*10**-19*nParticles*freqRev)**2*(curImpTime.sum())
    pLossBeam = enLossPerBunch*nBun**2
    pLossBeamFreq = 2*(1.6*10**-19*nParticles*freqRev)**2*nBun**2*curImpTime
    print pLossBeam, pLossBeamFreq
    return pLossBeam, pLossBeamFreq, gaussProf(imp[0,:],bunLen)**2

def extract_dat2013_MultipleSweeps(file_name):
    data = open(file_name, "r+")
    tar = data.readlines()
    temp=[]
    bigTemp = []
    count=2
    while count<len(tar):
        try:
            row = map(float, tar[count].split())
            temp.append(row)
            count+=1
        except:
            bigTemp.append(temp)
            temp=[]
            count+=3
    data.close()
    bigTemp.append(temp)
    return bigTemp

length = 2.88
f_rev = C/27000.0
nBunches = 2808
qPart = 1.6*10.0**-19
nPart=1.15*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=1.001*10.0**-9
testImp = [250*10**6, 10.0, 10000]
##bunchCur=nPart*qPart*f_rev
bunchCur=bCur
nBunches=1
print bCur, bunchCur*(nBunches)**2

directory = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/shortened_end_piece_impedance/"#Directory of data
##listDir = [directory+i for i in os.listdir(directory)]
listDir = []
listDir.append(directory+"100mm-overlap-24-full-length")
listDir.append(directory+"100mm-overlap-24-full-length-AltDesign")
listDir.append(directory+"100mm-overlap-24-full-length-enclosed")
listDir.append(directory+"100mm-overlap-56mm-pipe_alt_20mm")
listDir.append(directory+"100mm-overlap-24-full-length-tapered")
listDir.append(directory+"100mm-overlap-24-full-length-tapered-alternating")
listDir.append(directory+"100mm-overlap-24-full-length-step-out-final")
listDir.append(directory+"109mm_overlap")
##listDir.append(directory+"largeInDiamFerr")
##listDir.append(directory+"largeInDiamFerr")
##listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/with-15-screen-conductors")
##listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/19_conductors_alternating")
##listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/ferriteRings4A4NoYokeLongWakeLength/")
##listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/ferriteRings4B3NoYokeLongWakeLength/")
##listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/ferriteRings4M2NoYokeLongWakeLength/")
##listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/ferriteRingsVacNoYokeLongWakeLength/")
##listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/simulatingFerrAsVac/")
listDir.append("E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/90DegreeRotatedScreen15Conductors/")
fileNam = "/longitudinal-impedance.csv"

plotList=[]

for tarFile in listDir:
    tmp = sp.array(extract_dat(tarFile+fileNam))
    plotList.append([tmp[:,0],tmp[:,1],tmp[:,3]])

plotList = sp.array(plotList)

measurements_15 = sp.array(extract_dat("E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/15-strips-measurements.csv"))
measurements_19 = sp.array(extract_dat("E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/mki_measurements_mbarnes\MKI5_measurement_data-_4_6_2012/CentralTube-impedance-results.csv"))
measurementsStepOut = sp.array(extract_dat("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/31-05-13/tankNo13/resonator-impedance-results.csv"))
measurementsStepOutTank2 = sp.array(extract_dat("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/MKIkicker/28-06-2013/resonator/res_tom.txt-impedance-results.csv"))
measurementsStepOutTank2LowCoup = sp.array(extract_dat("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/MKIkicker/28-06-2013/resonator/res_tom_low_coupl.txt-impedance-results.csv"))
measurementsOldMKI8dFreq, measurementsOldMKI8dImp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/23-07-13/mki8d-old/resonator"))
measurementsTank7Old2ABeforeBakeoutFreq, measurementsTank7Old2ABeforeBakeoutImp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/Central-Port/displaced-wire/0mm-Horiz_0mm-Vert/02mar2010-resonator-t10"))
measurementsTank7Old2AAfterBakeoutFreq, measurementsTank7Old2AAfterBakeoutImp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/Central-Port/28-05-2010-post-bake-out/resonator"))
measurementsTank7Old2AFreq, measurementsTank7Old2AImp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/30-07-13/resonator"))
measurementsMKI08T11C09Freq, measurementsMKI08T11C09Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/30-07-13/mkitank11/resonator"))
measurementsMKI08T11C092Freq, measurementsMKI08T11C092Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/31-07-13/resonatorafterfingerchange"))
measurementsNewBroken = sp.array(extract_dat("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/31-07-13/resonatorafterfingerchange-impedance-results.csv"))
measurementsNewBroken2Freq, measurementsNewBroken2Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/02-08-13/resonator"))
measurementsMKI02T08C07Freq, measurementsMKI02T08C07Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/09-09-13/MKI08-C07-0262/resonator"))
measurementsMKI071113Freq, measurementsMKI071113Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/07-11-13/mki10-mc06/resonator"))
measurementsMKI061213Freq, measurementsMKI061213Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/06-12-13/mkiTank6-MC10/resonator"))
measurementsMKI060214Freq, measurementsMKI060214Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/06-02-14/mki-Tank10-cr2/resonator"))
measurementsMKI110314Freq, measurementsMKI110314Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/11-03-14/mki09-tank2-cr05/resShort"))
measurementsMKI190514Freq, measurementsMKI190514Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/19-05-14/mkit05-cr01/resonator"))
measurementsMKI110714Freq, measurementsMKI110714Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/11-07-14/mkiTank01-CR03/resScript"))
measurementsMKI231014Freq, measurementsMKI231014Imp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/23-10-14/mkitank3_cr9/resonator"))
measurementsMKI12117mmFreq, measurementsMKI12117mmImp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/21-10-15/mkiTransverseVertRes/0mmres"))
measurementsMKI1257mmFreq, measurementsMKI1257mmImp = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/18-11-15/mkiResVert/y0mm"))



resList, peakData = peakFitMeasurements(measurementsStepOut)
dat3, dat4 = peakFitSimulations(plotList[-1])

print measurementsStepOut[:,0], measurementsStepOut[:,1]

SplineFit15Cond = interp.InterpolatedUnivariateSpline(measurements_15[:,0], measurements_15[:,1])
SplineFit19Cond = interp.InterpolatedUnivariateSpline(measurements_19[:,0], measurements_19[:,1])
SplineFitTank1 = interp.InterpolatedUnivariateSpline(measurementsStepOut[:,0], measurementsStepOut[:,1])
SplineFitTank2 = interp.InterpolatedUnivariateSpline(measurementsStepOutTank2[:,0], measurementsStepOutTank2[:,1])
SplineFitTank2LowCoup = interp.InterpolatedUnivariateSpline(measurementsStepOutTank2LowCoup[:,0], measurementsStepOutTank2LowCoup[:,1])
SplineFitOld8d = interp.InterpolatedUnivariateSpline(measurementsOldMKI8dFreq[:]/10**6, measurementsOldMKI8dImp[:])
SplineFitTank7Old2A = interp.InterpolatedUnivariateSpline(measurementsTank7Old2AFreq[:]/10**6, measurementsTank7Old2AImp[:])
SplineFitTank7Old2ABeforeBakeout = interp.InterpolatedUnivariateSpline(measurementsTank7Old2ABeforeBakeoutFreq[:]/10**6, measurementsTank7Old2ABeforeBakeoutImp[:])
SplineFitTank7Old2AAfterBakeout = interp.InterpolatedUnivariateSpline(measurementsTank7Old2AAfterBakeoutFreq[:]/10**6, measurementsTank7Old2AAfterBakeoutImp[:])
SplineFitMKI08T11C09 = interp.InterpolatedUnivariateSpline(measurementsMKI08T11C09Freq[:]/10**6, measurementsMKI08T11C09Imp[:])
SplineFitMKI02T08C07 = interp.InterpolatedUnivariateSpline(measurementsMKI02T08C07Freq[:]/10**6, measurementsMKI02T08C07Imp[:])
SplineFitMKI071113 = interp.InterpolatedUnivariateSpline(measurementsMKI071113Freq[:]/10**6, measurementsMKI071113Imp[:])
SplineFitMKI061213 = interp.InterpolatedUnivariateSpline(measurementsMKI061213Freq[:]/10**6, measurementsMKI061213Imp[:])
SplineFitMKI060214 = interp.InterpolatedUnivariateSpline(measurementsMKI060214Freq[:]/10**6, measurementsMKI060214Imp[:])
SplineFitMKI110314 = interp.InterpolatedUnivariateSpline(measurementsMKI110314Freq[:]/10**6, measurementsMKI110314Imp[:])
SplineFitMKI190514 = interp.InterpolatedUnivariateSpline(measurementsMKI190514Freq[:]/10**6, measurementsMKI190514Imp[:])
SplineFitMKI110714 = interp.InterpolatedUnivariateSpline(measurementsMKI110714Freq[:]/10**6, measurementsMKI110714Imp[:])
SplineFitMKI231014 = interp.InterpolatedUnivariateSpline(measurementsMKI231014Freq[:]/10**6, measurementsMKI231014Imp[:])
SplineFitMKI12117mm = interp.InterpolatedUnivariateSpline(measurementsMKI12117mmFreq[:]/10**6, measurementsMKI12117mmImp[:])
SplineFitMKI1257mm = interp.InterpolatedUnivariateSpline(measurementsMKI1257mmFreq[:]/10**6, measurementsMKI1257mmImp[:])


freqList = sp.linspace(1,2000,2000)
freqListOld8d = sp.linspace(1,1500,1500)
specGauss = gaussProf(freqList*10.0**6, bLength)

splineFit15Cond = SplineFit15Cond(freqList)
splineFit19Cond = SplineFit19Cond(freqList)
splineFitDataTank1 = SplineFitTank1(freqList)
splineFitDataTank2 = SplineFitTank2(freqList)
splineFitDataTank2LowCoup = SplineFitTank2LowCoup(freqList)
splineFitOld8d = SplineFitOld8d(freqListOld8d)
splineFitTank7Old2A = SplineFitTank7Old2A(freqList)
splineFitTank7Old2ABeforeBakeout = SplineFitTank7Old2ABeforeBakeout(freqList)
splineFitTank7Old2AAfterBakeout = SplineFitTank7Old2AAfterBakeout(freqList)
splineFitMKI08T11C09 = SplineFitMKI08T11C09(freqList)
splineFitMKI02T08C07 = SplineFitMKI02T08C07(freqList)
splineFitMKI071113 = SplineFitMKI071113(freqList)
splineFitMKI061213 = SplineFitMKI061213(freqList)
splineFitMKI060214 = SplineFitMKI060214(freqList)
splineFitMKI110314 = SplineFitMKI110314(freqList)
splineFitMKI190514 = SplineFitMKI190514(freqList)
splineFitMKI110714 = SplineFitMKI110714(freqList)
splineFitMKI231014 = SplineFitMKI231014(freqList)
splineFitMKI12117mm = SplineFitMKI12117mm(freqList)
splineFitMKI1257mm = SplineFitMKI1257mm(freqList)

print "Bloop"
inteOverlap =integrate.simps(splineFitMKI231014*(gaussProf(freqList*10.0**6, bLength))**2, freqList*10.0**6)
##print inteOverlap
print inteOverlap*(1.6*(10.0**-19))**2
print inteOverlap*(1.6*(10.0**-19)*nPart)**2
print inteOverlap*(1.6*(10.0**-19)*nPart*2808)**2
print inteOverlap*(1.6*(10.0**-19)*nPart)**2*2808
print inteOverlap*(1.6*(10.0**-19)*nPart)**2*2808*(f_rev)/sp.pi
print integrate.simps(splineFitMKI231014*(gaussProf(freqList*10.0**6, bLength))**2*(1.6*(10.0**-19)*nPart)**2*2808*(f_rev)/sp.pi, freqList*10.0**6)
total = inteOverlap*(1.6*(10.0**-19)*nPart*2808)**2*(f_rev)/sp.pi
print total
##print measurements_15[:,0], measurements_15[:,1]

pl.plot(freqList, splineFitMKI231014*(gaussProf(freqList*10.0**6, bLength))**2*(1.6*(10.0**-19)*nPart)**2*nBunches*(f_rev)/sp.pi)
##pl.show()
pl.clf()

freqListHeating = sp.linspace(40,2000,2000/40)

freqListHeatingOld8d = sp.linspace(20,1500,1500/20)

splitFitHeating15Cond = SplineFit15Cond(freqListHeating)
splitFitHeating19Cond = SplineFit19Cond(freqListHeating)
splitFitHeatingTank1 = SplineFitTank1(freqListHeating)
splitFitHeatingTank2 = SplineFitTank2(freqListHeating)
splitFitHeatingTank2LowCoup = SplineFitTank2LowCoup(freqListHeating)
##splitFitHeatingOld8d = SplineFitOld8d(freqListHeatingOld8d)
splitFitHeatingTank7Old2A = SplineFitTank7Old2A(freqListHeating)
splitFitHeatingTank7Old2ABeforeBakeout = SplineFitTank7Old2ABeforeBakeout(freqListHeating)
splitFitHeatingTank7Old2AAfterBakeout = SplineFitTank7Old2AAfterBakeout(freqListHeating)
splitFitHeatingMKI08T11C09 = SplineFitMKI08T11C09(freqListHeating)
splitFitHeatingMKI02T08C07 = SplineFitMKI02T08C07(freqListHeating)
splitFitHeatingMKI071113 = SplineFitMKI071113(freqListHeating)
splitFitHeatingMKI061213 = SplineFitMKI061213(freqListHeating)
splitFitHeatingMKI060214 = SplineFitMKI060214(freqListHeating)
splitFitHeatingMKI110314 = SplineFitMKI110314(freqListHeating)
splitFitHeatingMKI190514 = SplineFitMKI190514(freqListHeating)
splitFitHeatingMKI110714 = SplineFitMKI110714(freqListHeating)
splitFitHeatingMKI231014 = SplineFitMKI231014(freqListHeating)
splitFitHeatingMKI12117mm = SplineFitMKI12117mm(freqListHeating)
splitFitHeatingMKI1257mm = SplineFitMKI1257mm(freqListHeating)

heatingTotalCos15Cond = 0.0
heatingTotalGauss15Cond = 0.0
heatingFreq15Cond = []
heatingTotalCos19Cond = 0.0
heatingTotalGauss19Cond = 0.0
heatingFreq19Cond = []
heatingTotalCosTank1 = 0.0
heatingTotalGaussTank1 = 0.0
heatingFreqTank1=[]
heatingTotalCosTank2 = 0.0
heatingTotalGaussTank2 = 0.0
heatingFreqTank2=[]
heatingTotalCosTank2LowCoup = 0.0
heatingTotalGaussTank2LowCoup = 0.0
heatingFreqOld8d=[]
heatingTotalCosOld8d=0.0
heatingTotalGaussOld8d=0.0
heatingFreqTank7Old2A=[]
heatingTotalCosTank7Old2A=0.0
heatingTotalGaussTank7Old2A=0.0
heatingFreqTank7Old2ABeforeBakeout=[]
heatingTotalCosTank7Old2ABeforeBakeout=0.0
heatingTotalGaussTank7Old2ABeforeBakeout=0.0
heatingFreqTank7Old2AAfterBakeout=[]
heatingTotalCosTank7Old2AAfterBakeout=0.0
heatingTotalGaussTank7Old2AAfterBakeout=0.0
heatingFreqMKI08T11C09=[]
heatingTotalCosMKI08T11C09=0.0
heatingTotalGaussMKI08T11C09=0.0
heatingFreqMKI02T08C07=[]
heatingTotalCosMKI02T08C07=0.0
heatingTotalGaussMKI02T08C07=0.0
heatingFreqMKI071113=[]
heatingTotalCosMKI071113=0.0
heatingTotalGaussMKI071113=0.0
heatingFreqMKI061213=[]
heatingTotalCosMKI061213=0.0
heatingTotalGaussMKI061213=0.0
heatingFreqMKI060214=[]
heatingTotalCosMKI060214=0.0
heatingTotalGaussMKI060214=0.0
heatingFreqMKI110314=[]
heatingTotalCosMKI110314=0.0
heatingTotalGaussMKI110314=0.0
heatingFreqMKI190514=[]
heatingTotalCosMKI190514=0.0
heatingTotalGaussMKI190514=0.0
heatingFreqMKI110714=[]
heatingTotalCosMKI110714=0.0
heatingTotalGaussMKI110714=0.0
heatingFreqMKI231014=[]
heatingTotalCosMKI231014=0.0
heatingTotalGaussMKI231014=0.0
heatingFreqMKI12117mm=[]
heatingTotalCosMKI12117mm=0.0
heatingTotalGaussMKI12117mm=0.0
heatingFreqMKI1257mm=[]
heatingTotalCosMKI1257mm=0.0
heatingTotalGaussMKI1257mm=0.0

for i in range(0,len(splitFitHeatingTank1)):
    heatingTotalGauss15Cond+=2*splitFitHeating15Cond[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCos15Cond+=2*splitFitHeating15Cond[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreq15Cond.append(2*splitFitHeating15Cond[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGauss19Cond+=2*splitFitHeating19Cond[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCos19Cond+=2*splitFitHeating19Cond[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreq19Cond.append(2*splitFitHeating19Cond[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussTank1+=2*splitFitHeatingTank1[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosTank1+=2*splitFitHeatingTank1[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqTank1.append(2*splitFitHeatingTank1[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussTank2+=2*splitFitHeatingTank2[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosTank2+=2*splitFitHeatingTank2[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqTank2.append(2*splitFitHeatingTank2[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussTank2LowCoup+=2*splitFitHeatingTank2LowCoup[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosTank2LowCoup+=2*splitFitHeatingTank2LowCoup[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalGaussTank7Old2A+=2*splitFitHeatingTank7Old2A[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosTank7Old2A+=2*splitFitHeatingTank7Old2A[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqTank7Old2A.append(2*splitFitHeatingTank7Old2A[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussTank7Old2ABeforeBakeout+=2*splitFitHeatingTank7Old2ABeforeBakeout[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosTank7Old2ABeforeBakeout+=2*splitFitHeatingTank7Old2ABeforeBakeout[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqTank7Old2ABeforeBakeout.append(2*splitFitHeatingTank7Old2ABeforeBakeout[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussTank7Old2AAfterBakeout+=2*splitFitHeatingTank7Old2AAfterBakeout[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosTank7Old2AAfterBakeout+=2*splitFitHeatingTank7Old2AAfterBakeout[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqTank7Old2AAfterBakeout.append(2*splitFitHeatingTank7Old2AAfterBakeout[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussMKI08T11C09+=2*splitFitHeatingMKI08T11C09[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosMKI08T11C09+=2*splitFitHeatingMKI08T11C09[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKI08T11C09.append(2*splitFitHeatingMKI08T11C09[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussMKI02T08C07+=2*splitFitHeatingMKI02T08C07[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosMKI02T08C07+=2*splitFitHeatingMKI02T08C07[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKI02T08C07.append(2*splitFitHeatingMKI02T08C07[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussMKI071113+=2*splitFitHeatingMKI071113[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosMKI071113+=2*splitFitHeatingMKI071113[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKI071113.append(2*splitFitHeatingMKI071113[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussMKI061213+=2*splitFitHeatingMKI061213[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosMKI061213+=2*splitFitHeatingMKI061213[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKI061213.append(2*splitFitHeatingMKI061213[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussMKI060214+=2*splitFitHeatingMKI060214[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosMKI060214+=2*splitFitHeatingMKI060214[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKI060214.append(2*splitFitHeatingMKI060214[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussMKI110314+=2*splitFitHeatingMKI110314[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosMKI110314+=2*splitFitHeatingMKI110314[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKI110314.append(2*splitFitHeatingMKI110314[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussMKI190514+=2*splitFitHeatingMKI190514[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosMKI190514+=2*splitFitHeatingMKI190514[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKI190514.append(2*splitFitHeatingMKI190514[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussMKI110714+=2*splitFitHeatingMKI110714[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosMKI110714+=2*splitFitHeatingMKI110714[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKI110714.append(2*splitFitHeatingMKI110714[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussMKI231014+=2*splitFitHeatingMKI231014[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosMKI231014+=2*splitFitHeatingMKI231014[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKI231014.append(2*splitFitHeatingMKI231014[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussMKI12117mm+=2*splitFitHeatingMKI12117mm[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosMKI12117mm+=2*splitFitHeatingMKI12117mm[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKI12117mm.append(2*splitFitHeatingMKI12117mm[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
    heatingTotalGaussMKI1257mm+=2*splitFitHeatingMKI1257mm[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
    heatingTotalCosMKI1257mm+=2*splitFitHeatingMKI1257mm[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)
    heatingFreqMKI1257mm.append(2*splitFitHeatingMKI1257mm[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))

##for i in range(0,len(splitFitHeatingOld8d)):
##    heatingFreqOld8d.append(2*splitFitHeatingOld8d[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength))
##    heatingTotalGaussOld8d+=2*splitFitHeatingOld8d[i]*nBunches*(bunchCur**2)*gaussProf(freqListHeating[i]*10.0**6, bLength)
##    heatingTotalCosOld8d+=2*splitFitHeatingOld8d[i]*nBunches*(bunchCur**2)*cosProf(freqListHeating[i]*10.0**6, bLength)

pLossTestMKI071113, pLossTestFreqMKI071113, impCurSpecMKI071113= heatLossBunByBunGauss(sp.array([freqList*10.0**6, splineFitMKI071113]), 1.0*10**-9, 2808, 1.15*10.0**11, f_rev)

print "15 Conductors "+str(heatingTotalCos15Cond), str(heatingTotalGauss15Cond)
print "19 Conductors "+str(heatingTotalCos19Cond), str(heatingTotalGauss19Cond)
print "MKI11-T13-MC03 "+str(heatingTotalCosTank1), str(heatingTotalGaussTank1)
print "MKI12-T12-MC01 "+str(heatingTotalCosTank2), str(heatingTotalGaussTank2)
##print heatingTotalCosTank2LowCoup, heatingTotalGaussTank2LowCoup
##print heatingTotalCosOld8d, heatingTotalGaussOld8d
print "MKI08-T11-MC09 "+str(heatingTotalCosMKI08T11C09), str(heatingTotalGaussMKI08T11C09)
print heatingTotalCosTank7Old2ABeforeBakeout, heatingTotalGaussTank7Old2ABeforeBakeout
##print heatingTotalCosTank7Old2AAfterBakeout, heatingTotalGaussTank7Old2AAfterBakeout
##print heatingTotalCosTank7Old2A, heatingTotalGaussTank7Old2A
print "MKI07-T08-MC08 "+str(heatingTotalCosMKI02T08C07), str(heatingTotalGaussMKI02T08C07)
print "MKI06-T07-HC12 "+str(heatingTotalCosMKI071113), str(heatingTotalGaussMKI071113)
print pLossTestMKI071113
print "MKI10-T06-HC13 "+str(heatingTotalCosMKI061213), str(heatingTotalGaussMKI061213)
print "MKI02-T10-HC14 "+str(heatingTotalCosMKI060214), str(heatingTotalGaussMKI060214)
print "MKI05-T02-HC15 "+str(heatingTotalCosMKI110314), str(heatingTotalGaussMKI110314)
print "MKI09-T05-HC16 "+str(heatingTotalCosMKI190514), str(heatingTotalGaussMKI190514)
print "MKI03-T01-HC17 "+str(heatingTotalCosMKI110714), str(heatingTotalGaussMKI110714)
print "MKI09-T03-HC18 "+str(heatingTotalCosMKI231014), str(heatingTotalGaussMKI231014)
print "MKI 117mm "+str(heatingTotalCosMKI12117mm), str(heatingTotalGaussMKI12117mm)
print "MKI 57mm "+str(heatingTotalCosMKI1257mm), str(heatingTotalGaussMKI1257mm)

##pl.semilogy()
##pl.plot(measurementsMKI071113Freq, measurementsMKI071113Imp/100)
##pl.plot(freqList*10**6, pLossTestFreqMKI071113)
pl.plot(freqList*10**6, (1.6*10**-19*1.15*10**11*(C/27000))**2*2808*splineFitMKI071113*impCurSpecMKI071113**2)
##pl.show()
pl.clf()

heatingFreqTank1=sp.array(heatingFreqTank1)
heatingFreqTank2=sp.array(heatingFreqTank2)
heatingFreqOld8d=sp.array(heatingFreqOld8d)
heatingFreqTank7Old2A=sp.array(heatingFreqTank7Old2A)
heatingFreqMKI08T11C09=sp.array(heatingFreqMKI08T11C09)
heatingFreqMKI02T08C07=sp.array(heatingFreqMKI02T08C07)
heatingFreqMKI071113=sp.array(heatingFreqMKI071113)
heatingFreqMKI061213=sp.array(heatingFreqMKI061213)
heatingFreqMKI060214=sp.array(heatingFreqMKI060214)
heatingFreqMKI110314=sp.array(heatingFreqMKI110314)
heatingFreqMKI190514=sp.array(heatingFreqMKI190514)
heatingFreqMKI110714=sp.array(heatingFreqMKI110714)
heatingFreqMKI231014=sp.array(heatingFreqMKI231014)
heatingFreqMKI12117mm=sp.array(heatingFreqMKI12117mm)
heatingFreqMKI1257mm=sp.array(heatingFreqMKI1257mm)

summedFreqTank1 = []
summedFreqTank2 = []
summedFreqOld8d = []
summedFreqTank7Old2A= []
summedFreqMKI08T11C09= []
summedFreqMKI02T08C07= []
summedFreqMKI071113= []
summedFreqMKI061213= []
summedFreqMKI060214= []
summedFreqMKI110314= []
summedFreqMKI190514= []
summedFreqMKI110714= []
summedFreqMKI231014= []
summedFreqMKI12117mm= []
summedFreqMKI1257mm= []

for i in range(0,len(heatingFreqTank1)):
    summedFreqTank1.append(sp.sum(heatingFreqTank1[:i]))
    summedFreqTank2.append(sp.sum(heatingFreqTank2[:i]))
    summedFreqOld8d.append(sp.sum(heatingFreqOld8d[:i]))
    summedFreqTank7Old2A.append(sp.sum(heatingFreqTank7Old2A[:i]))
    summedFreqMKI08T11C09.append(sp.sum(heatingFreqMKI08T11C09[:i]))
    summedFreqMKI02T08C07.append(sp.sum(heatingFreqMKI02T08C07[:i]))
    summedFreqMKI071113.append(sp.sum(heatingFreqMKI071113[:i]))
    summedFreqMKI061213.append(sp.sum(heatingFreqMKI061213[:i]))
    summedFreqMKI060214.append(sp.sum(heatingFreqMKI060214[:i]))
    summedFreqMKI110314.append(sp.sum(heatingFreqMKI110314[:i]))
    summedFreqMKI190514.append(sp.sum(heatingFreqMKI190514[:i]))
    summedFreqMKI110714.append(sp.sum(heatingFreqMKI110714[:i]))
    summedFreqMKI231014.append(sp.sum(heatingFreqMKI231014[:i]))
    summedFreqMKI12117mm.append(sp.sum(heatingFreqMKI12117mm[:i]))
    summedFreqMKI1257mm.append(sp.sum(heatingFreqMKI1257mm[:i]))

summedFreqTank1=sp.array(summedFreqTank1)
summedFreqTank2=sp.array(summedFreqTank2)
summedFreqOld8=sp.array(summedFreqOld8d)
summedFreqTank7Old2A=sp.array(summedFreqTank7Old2A)
summedFreqMKI08T11C09=sp.array(summedFreqMKI08T11C09)
summedFreqMKI02T08C07=sp.array(summedFreqMKI02T08C07)
summedFreqMKI071113=sp.array(summedFreqMKI071113)
summedFreqMKI061213=sp.array(summedFreqMKI061213)
summedFreqMKI060214=sp.array(summedFreqMKI060214)
summedFreqMKI110314=sp.array(summedFreqMKI110314)
summedFreqMKI190514=sp.array(summedFreqMKI190514)
summedFreqMKI110714=sp.array(summedFreqMKI110714)
summedFreqMKI231014=sp.array(summedFreqMKI231014)

lowFreqNewRealFile = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/MKIkicker/28-06-2013/resistors/S12_FREQ_GATING.CSV"
inputNew = open(lowFreqNewRealFile, 'r+')
dataLowFreqNew = inputNew.readlines()
inputNew.close()

datList = []

for row in dataLowFreqNew[3:]:
    temp=map(float, row.split(','))
    datList.append([temp[0], -2*377.0*sp.log(10.0**(temp[1]/20))])

datList=sp.array(datList)

dirMatched = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/10-12-13/"
datFilesForWireMeas = ["S21WITHATTENUATORS0-200MHZ.S2P","S21WITHATTENUATORS200-400MHZ.S2P","S21WITHATTENUATORS400-600MHZ.S2P",
                       "S21WITHATTENUATORS600-800MHZ.S2P", "S21WITHATTENUATORS800-1000MHZ.S2P", "S21WITHATTENUATORS1000-1200MHZ.S2P",
                       "S21WITHATTENUATORS1200-1400MHZ.S2P", "S21WITHATTENUATORS1400-1600MHZ.S2P","S21WITHATTENUATORS1600-1800MHZ.S2P",
                       "S21WITHATTENUATORS1800-2000MHZ.S2P"]

temp=[]


for inputFile in datFilesForWireMeas:
    tar=open(dirMatched+inputFile,"r+")
    inputData=tar.readlines()
    for line in inputData[6:]:
        bit=line.split("\t")
        temp.append(map(float,bit))

transDataRes=sp.array(temp)
transDataResImp=logImpFormula(logToLin(transDataRes[:,3]), 1, 270)

dirMatch140714 = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/14-07-14/mkiTank01-CR03/S21FREQMATCHINGNOGATING.S2P"
tar=open(dirMatch140714,'r+')
inputData=tar.readlines()
tar.close()
temp=[]
for line in inputData[6:]:
    bit=line.split("\t")
    temp.append(map(float,bit))

matched140714 = sp.array(temp)
matched140714Imp = logImpFormula(logToLin(matched140714[:,3]), 1, 160)

dirMatch140714 = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/14-07-14/mkiTank01-CR03/S21FREQMATCHINGLOWFREQ.S2P"
tar=open(dirMatch140714,'r+')
inputData=tar.readlines()
tar.close()
temp=[]
for line in inputData[6:]:
    bit=line.split("\t")
    temp.append(map(float,bit))

matched140714LowFreq = sp.array(temp)
matched140714LowFreqImp = logImpFormula(logToLin(matched140714LowFreq[:,3]), 1, 160)

inputMeasNoMatchingGating = open("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/19-05-14/mkit05-cr01/S21NOMATCHINGGATINGLOGMAG.CSV", "r+")
dataMeasNoMatchingGating = inputMeasNoMatchingGating.readlines()
inputMeasNoMatchingGating.close()
temp=[]
for line in dataMeasNoMatchingGating[3:]:
    bit=line.split(",")
    temp.append(map(float,bit))
inputMeasNoMatchingGatingRes=sp.array(temp)
inputMeasNoMatchingGatingResImp=logImpFormula(logToLin(inputMeasNoMatchingGatingRes[:,1]), 1, 270)

dirMatch231014 = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/24-10-14/S21FREQATTEN.S2P"
tar=open(dirMatch231014,'r+')
inputData=tar.readlines()
tar.close()
temp=[]
for line in inputData[6:]:
    bit=line.split("\t")
    temp.append(map(float,bit))

matched231014 = sp.array(temp)
matched231014Imp = logImpFormula(logToLin(matched231014[:,3]), 1, 160)

dirMatch117mm = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/27-10-15/mkiVertTrans/Y0MM.S2P"
tar=open(dirMatch117mm,'r+')
inputData=tar.readlines()
tar.close()
temp=[]
for line in inputData[6:]:
    bit=line.split("\t")
    temp.append(map(float,bit))

matched117mm = sp.array(temp)
matched117mmImp = logImpFormula(logToLin(matched117mm[:,3]), 1, 160)

dirMatch57mm = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/24-11-15/vertNoAtten/Y0MM.S2P"
tar=open(dirMatch57mm,'r+')
inputData=tar.readlines()
tar.close()
temp=[]
for line in inputData[6:]:
    bit=line.split("\t")
    temp.append(map(float,bit))

matched57mm = sp.array(temp)
matched57mmImp = logImpFormula(logToLin(matched57mm[:,3]), 1, 160)

freqListSimMeas = sp.linspace(0.001*10.0**9, 10**9, 1000)

tarFileSimMeas24Cond = "E:/PhD/1st_Year_09-10/Data/mkiSimulatedWireMeas/MKIScreenDesignNewPostLS1/106mmOverlapFinalDesigndB.csv"
datSimMeas24Cond = importDatSimMeas(tarFileSimMeas24Cond)
analDatSimMeas24Cond = 0.25*sp.array(analSingReSimMeas(sp.array(datSimMeas24Cond), freqListSimMeas, 0.25, 0.25, 0.0005, 0.024))
tarFileSimMeas24Cond99 = "E:/PhD/1st_Year_09-10/Data/mkiSimulatedWireMeas/MKIScreenDesignNewPostLS1/99mmOverlapFinalDesigndB.csv"
datSimMeas24Cond99 = importDatSimMeas(tarFileSimMeas24Cond)
analDatSimMeas24Cond99 = 0.25*sp.array(analSingReSimMeas(sp.array(datSimMeas24Cond99), freqListSimMeas, 0.25, 0.25, 0.0005, 0.024))
tarFileSimMeas24Cond99Displaced = "E:/PhD/1st_Year_09-10/Data/mkiSimulatedWireMeas/MKIScreenDesignNewPostLS1/99mmOverlapFinalDesignDisplacedWiredB.csv"
datSimMeas24Cond99Displaced = importDatSimMeas(tarFileSimMeas24Cond99Displaced)
analDatSimMeas24Cond99Displaced = 0.25*sp.array(analSingReSimMeas(sp.array(datSimMeas24Cond99Displaced), freqListSimMeas, 0.25, 0.25, 0.0005, 0.024))
tarFileSimMeas24Cond99Displacedy3mm = "E:/PhD/1st_Year_09-10/Data/mkiSimulatedWireMeas/MKIScreenDesignNewPostLS1/99mmOverlapFinalDesignDisplacedWire3mmydB.csv"
datSimMeas24Cond99Displacedy3mm = importDatSimMeas(tarFileSimMeas24Cond99Displacedy3mm)
analDatSimMeas24Cond99Displacedy3mm = 0.25*sp.array(analSingReSimMeas(sp.array(datSimMeas24Cond99Displacedy3mm), freqListSimMeas, 0.25, 0.25, 0.0005, 0.024))
tarFileSimMeas24Cond99Displacedx3mm = "E:/PhD/1st_Year_09-10/Data/mkiSimulatedWireMeas/MKIScreenDesignNewPostLS1/99mmOverlapFinalDesignDisplacedWire3mmxdB.csv"
datSimMeas24Cond99Displacedx3mm = importDatSimMeas(tarFileSimMeas24Cond99Displacedx3mm)
analDatSimMeas24Cond99Displacedx3mm = 0.25*sp.array(analSingReSimMeas(sp.array(datSimMeas24Cond99Displacedx3mm), freqListSimMeas, 0.25, 0.25, 0.0005, 0.024))
tarFileSimMeas24Cond99Displacedx3mmy3mm = "E:/PhD/1st_Year_09-10/Data/mkiSimulatedWireMeas/MKIScreenDesignNewPostLS1/99mmOverlapFinalDesignDisplacedWire3mmx3mmydB.csv"
datSimMeas24Cond99Displacedx3mmy3mm  = importDatSimMeas(tarFileSimMeas24Cond99Displacedx3mmy3mm )
analDatSimMeas24Cond99Displacedx3mmy3mm  = 0.25*sp.array(analSingReSimMeas(sp.array(datSimMeas24Cond99Displacedx3mmy3mm ), freqListSimMeas, 0.25, 0.25, 0.0005, 0.024))
tarFileSimMeas24Cond99Displacedx3mmy3mmLongerTube = "E:/PhD/1st_Year_09-10/Data/mkiSimulatedWireMeas/MKIScreenDesignNewPostLS1/99mmOverlapFinalDesignDisplacedWire3mmx3mmyLongerStructuredB.csv"
datSimMeas24Cond99Displacedx3mmy3mmLongerTube  = importDatSimMeas(tarFileSimMeas24Cond99Displacedx3mmy3mmLongerTube )
analDatSimMeas24Cond99Displacedx3mmy3mmLongerTube  = 0.25*sp.array(analSingReSimMeas(sp.array(datSimMeas24Cond99Displacedx3mmy3mmLongerTube ), freqListSimMeas, 0.25, 0.25, 0.0005, 0.024))
tarFileSimMeas24Cond99Displacedx3mmy3mmLongerWiderTube = "E:/PhD/1st_Year_09-10/Data/mkiSimulatedWireMeas/MKIScreenDesignNewPostLS1/99mmOverlapFinalDesignDisplacedWire3mmx3mmyLongerStructureWideneddB.csv"
datSimMeas24Cond99Displacedx3mmy3mmLongerWiderTube  = importDatSimMeas(tarFileSimMeas24Cond99Displacedx3mmy3mmLongerWiderTube )
analDatSimMeas24Cond99Displacedx3mmy3mmLongerWiderTube  = 0.25*sp.array(analSingReSimMeas(sp.array(datSimMeas24Cond99Displacedx3mmy3mmLongerWiderTube ), freqListSimMeas, 0.25, 0.25, 0.0005, 0.024))
tarFileSimMeas24Cond99Displacedx3mmy3mmLongerWiderTubeMore = "E:/PhD/1st_Year_09-10/Data/mkiSimulatedWireMeas/MKIScreenDesignNewPostLS1/99mmOverlapFinalDesignDisplacedWire3mmx3mmyLongerStructureWideneddB.csv"
datSimMeas24Cond99Displacedx3mmy3mmLongerWiderTubeMore  = importDatSimMeas(tarFileSimMeas24Cond99Displacedx3mmy3mmLongerWiderTubeMore )
analDatSimMeas24Cond99Displacedx3mmy3mmLongerWiderTubeMore  = 0.25*sp.array(analSingReSimMeas(sp.array(datSimMeas24Cond99Displacedx3mmy3mmLongerWiderTubeMore ), freqListSimMeas, 0.25, 0.25, 0.0005, 0.024))
wake24Meas99Overlap = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/shortened_end_piece_impedance/longWakelength80m99mmOverlap/longitudinal-impedance.csv"
wake24Meas99OverlapDat = sp.array(extract_dat(wake24Meas99Overlap))
wake24meas99OverlapBigGeo = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/shortened_end_piece_impedance/overlap99mmWake80mBiggerGeo/longitudinal-impedance.csv"
wake24Meas99OverlapBigGeoDat = sp.array(extract_dat(wake24meas99OverlapBigGeo))
directory_sims_15 = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/with-15-screen-conductors/longitudinal-impedance.csv"
datSimMeas15Cond  = sp.array(extract_dat(directory_sims_15))
topDir = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/"
postls130m117mmDat = sp.array(extract_dat2013(topDir+"POSTLS1Overlap117mm/longitudinal-impedance-real.txt"))
postls130m117mm100mDat = sp.array(extract_dat2013(topDir+"POSTLS1Overlap117mm100mWake/longitudinal-impedance-real.txt"))
hllhc80mmDat = sp.array(extract_dat2013(topDir+"HLLHCProposal/80mmHigherMeshDens/longitudinal-impedance-real.txt"))

print len(freqListSimMeas), len(analDatSimMeas24Cond)


directSimsNewModel = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/"

datSimsBigModelHighMesh=sp.array(extract_dat2013(directSimsNewModel+"NewDesignBigSimulation50mWake/longitudinal-impedance-real.txt"))
datSimsBigModelSmallMesh=sp.array(extract_dat2013(directSimsNewModel+"newDesignLongScreenOnly50mWakeHighMesh/longitudinal-impedance-real.txt"))
datSimsScreenModelSmallMesh=sp.array(extract_dat2013(directSimsNewModel+"NewDesignCutDownModel40mWake/longitudinal-impedance-real.txt"))
datSimsBigModel150mWakeMesh=sp.array(extract_dat2013(directSimsNewModel+"NewDesignBigSimulation150mWake/longitudinal-impedance-real.txt"))
datSimsBigModel150mWakeHighMeshDensity=sp.array(extract_dat(directSimsNewModel+"mkiBeamScreenShortenedLongWakeHighDensity/longitudinal-impedance-real.csv"))
datSimsBigModel1100mWakeHighMeshDensityShortOverlap=sp.array(extract_dat(directSimsNewModel+"mkiBeamScreenShortenedLongWakeHighDensity/longitudinal-impedance-real.csv"))
datSimsBigModel1100mWakeLowMeshDensityShortOverlap=sp.array(extract_dat(directSimsNewModel+"newDesignShortened100mWakeFullModel/longitudina-impedance-real.csv"))

Freq129mmOverlap, Imp129mmOverlap = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/129.4mm/resonator4"))
Freq119mmOverlap, Imp119mmOverlap = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/119.4mm/resonator"))
Freq99mmOverlap, Imp99mmOverlap = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/99.4mm/resonator"))
Freq89mmOverlap, Imp89mmOverlap = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/89.4mm/resonator"))
Freq79mmOverlap, Imp79mmOverlap = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/79.4mm/Resonator"))
Freq69mmOverlap, Imp69mmOverlap = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/69.4mm/resonator"))
Freq59mmOverlap, Imp59mmOverlap = sp.array(resImpGet("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/59.4mm/resonator"))

fileTar129mmGating = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/129.4mm/S21GATING.S2P"
s21129mmGating = sp.array(readS21FromVNA(fileTar129mmGating))

fileTar119mmGating = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/119.4mm/S21FREQGATING.S2P"
s21119mmGating = sp.array(readS21FromVNA(fileTar119mmGating))
fileTar119mmGatingCSV = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/119.4mm/S21FREQGATING.CSV"
s21119mmGatingCSV = sp.array(readS21FromCSV(fileTar119mmGatingCSV))

fileTar99mmGating = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/99.4mm/S21FREQGATING.S2P"
s2199mmGating = sp.array(readS21FromVNA(fileTar99mmGating))
fileTar99mmGatingCSV = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/99.4mm/S21FREQGATING.CSV"
s2199mmGatingCSV = sp.array(readS21FromCSV(fileTar99mmGatingCSV))

fileTar89mmGating = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/89.4mm/S21FREQGATING.S2P"
s2189mmGating = sp.array(readS21FromVNA(fileTar89mmGating))
fileTar89mmGatingCSV = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/89.4mm/S21FREQGATING.CSV"
s2189mmGatingCSV = sp.array(readS21FromCSV(fileTar89mmGatingCSV))

fileTime89mmCSV = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/89.4mm/S21TIMEIMP.CSV"
s21Time89mmCSV = sp.array(readS21FromCSV(fileTime89mmCSV))

fileTar79mmGatingCSV = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/79.4mm/PORTS1+2-IMPULSE-S11.CSV"
s2179mmGatingCSV = sp.array(readS21FromCSV(fileTar79mmGatingCSV))

fileTar69mmGatingCSV = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/69.4mm/S21FREQTIMEGATING.CSV"
s2169mmGatingCSV = sp.array(readS21FromCSV(fileTar69mmGatingCSV))

fileTar59mmGatingCSV = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/09-02-15/mkiSuperSpeschul/59.4mm/S21FREQGATING.CSV"
s2159mmGatingCSV = sp.array(readS21FromCSV(fileTar59mmGatingCSV))

impCSV119mm = impFromFreq(s21119mmGatingCSV, 230)
peaks119mm = findPeakFreqImp(impCSV119mm)
impCSV99mm = impFromFreq(s2199mmGatingCSV, 230)
peaks99mm = findPeakFreqImp(impCSV99mm)
impCSV89mm = impFromFreq(s2189mmGatingCSV, 230)
peaks89mm = findPeakFreqImp(impCSV89mm)
impCSV79mm = impFromFreq(s2179mmGatingCSV, 230)
peaks79mm = findPeakFreqImp(impCSV79mm)
impCSV69mm = impFromFreq(s2169mmGatingCSV, 230)
peaks69mm = findPeakFreqImp(impCSV69mm)
impCSV59mm = impFromFreq(s2159mmGatingCSV, 230)
peaks59mm = findPeakFreqImp(impCSV59mm)

overlap59mm99mmDat = extract_dat2013_MultipleSweeps("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/VaryingLengthMKI/HLLHCProposal/59-99mmOverLap-Full-HLLHCProposal-real-40m-wake.txt")
overlap59mmImp = sp.array(overlap59mm99mmDat[0][:-1])
overlap69mmImp = sp.array(overlap59mm99mmDat[1][:-1])
overlap79mmImp = sp.array(overlap59mm99mmDat[2][:-1])
overlap89mmImp = sp.array(overlap59mm99mmDat[3][:-1])
overlap99mmImp = sp.array(overlap59mm99mmDat[4][:-1])

print datSimsBigModel150mWakeHighMeshDensity[0]

powLossSimulated=[]

for i in range(39,len(datSimsBigModelSmallMesh[:,1]),40):
    powLossSimulated.append(2*datSimsBigModelSmallMesh[i,1]*(bCur**2)*cosProf(datSimsBigModelSmallMesh[i,0]*10.0**9, bLength))

powLossSimulatedTotal=0.0
powLossSimulated=sp.array(powLossSimulated)

for i in range(0,len(powLossSimulated)):
    powLossSimulatedTotal+=powLossSimulated[i]

print powLossSimulatedTotal
##pl.semilogy()
##pl.semilogx()

##pl.plot(measurements_15[:,0]/10.0**9, measurements_15[:,1], 'k-',markersize=16.0, label="15 Screen Conductors")
##pl.plot(freqList/10.0**3, splineFit15Cond, 'b-', label="Interpolation of measurement Measured 15 Conductors")
##pl.plot(datSimMeas15Cond[:,0], datSimMeas15Cond[:,1]/2.88, 'b-')
##pl.plot(measurements_19[:,0]/10.0**3, measurements_19[:,1], 'kx',markersize=16.0,label="19 Screen Conductors (New after TS3 MKI8D) - Measured")
##pl.plot(freqList/10.0**3, splineFit19Cond, 'm-', label="Interpolation of measurement Measured 19 Conductors")
##pl.plot(freqList/10.0**3, splineFit19Cond, 'k-')
##pl.plot(plotList[-6,0], plotList[6,1]/lDUT, label="New Screen Design Simulated")
##pl.plot(measurementsStepOut[:,0]/10.0**3, measurementsStepOut[:,1], 'rx', markersize=16.0, label="Measured MKI11-T13-MC03")
##pl.plot(measurementsStepOut[:,0]/10.0**3, measurementsStepOut[:,1], 'rx', markersize=16.0, label="New Design (24 Conds) - Measured")
##pl.plot(freqList/10.0**3, splineFitDataTank1, 'k-',label="Interpolation of measurement Measured MKI11-T13-MC03")
##pl.plot(freqList/10.0**3, splineFitDataTank1, 'k-')
##pl.plot(measurementsStepOutTank2[:,0]/10.0**3, measurementsStepOutTank2[:,1], 'cx' ,markersize=16.0, label="Measured MKI12-T12-MC01")
##pl.plot(freqList/10.0**3, splineFitDataTank2, 'r-', label="Interpolation of measurement Measured MKI12-T12-MC01")
##pl.plot(freqList/10.0**3, splineFitDataTank2, 'c-')
##pl.plot(measurementsMKI08T11C09Freq/10.0**9, measurementsMKI08T11C09Imp, 'gx' ,markersize=16.0, label="Measured MKI08-T11-MC09")
##pl.plot(measurementsMKI08T11C092Freq/10.0**9, measurementsMKI08T11C092Imp, 'k-' ,markersize=16.0, label="Measured MKI08-T11-MC09")
##pl.plot(measurementsNewBroken2Freq/10.0**9, measurementsNewBroken2Imp, 'b+' ,markersize=16.0, label="Measured Redo MKI08-T11-MC09")
##pl.plot(measurementsNewBroken[:,0]/10.0**3, measurementsNewBroken[:,1], 'k+' ,markersize=16.0, label="Measured Second MKI08-T11-MC09")
##pl.plot(freqList/10.0**3, splineFitMKI08T11C09, 'g-', label="Interpolation of measurement Measured MKI08-T11-MC09")
##pl.plot(freqList/10.0**3, splineFitMKI08T11C09, 'g-')
##pl.plot(measurementsMKI02T08C07Freq/10.0**9, measurementsMKI02T08C07Imp, 'bx', markersize=16.0, label="Measured MKI07-T08-MC08")
##pl.plot(freqList/10.0**3, splineFitMKI02T08C07, 'b-', label="Interpolation of measurement Measured MKI07-T08-MC08")
##pl.plot(freqList/10.0**3, splineFitMKI02T08C07, 'b-')
##pl.plot(measurementsMKI071113Freq/10.0**9, measurementsMKI071113Imp, 'mx', markersize=16.0, label="Measured MKI06-T07-HC12")
##pl.plot(freqList/10.0**3, splineFitMKI071113, 'm-', label="Interpolation of measurement Measured MKI06-T07-HC12")
##pl.plot(freqList/10.0**3, splineFitMKI071113, 'm-')
##pl.plot(measurementsMKI071113Freq/10.0**9, measurementsMKI061213Imp, 'rx', markersize=16.0, label="Measured MKI10-T06-HC13")
##pl.plot(freqList/10.0**3, splineFitMKI061213, 'r-', label="Interpolation of measurement Measured MKI10-T06-HC13")
##pl.plot(freqList/10.0**3, splineFitMKI061213, 'r-')
##pl.plot(measurementsMKI071113Freq/10.0**9, measurementsMKI061213Imp, 'kx', markersize=16.0, label="24 Screen Conductors - Measured")
##pl.plot(freqList/10.0**3, splineFitMKI061213, 'k-', label="Interpolation of measurement 24 screen conductors")
##pl.plot(transDataRes[:,0]/10.0**9, transDataResImp/lDUT-880,'g-', label="Measured MKI10-T06-HC13 Classical Trans")
##pl.plot(measurementsMKI060214Freq/10.0**9, measurementsMKI060214Imp, 'b+', markersize=16.0, label="Measured MKI02-T10-HC14")
##pl.plot(freqList/10.0**3, splineFitMKI060214, 'b--', label="Interpolation of measurement Measured MKI02-T10-HC14")
##pl.plot(freqList/10.0**3, splineFitMKI060214, 'b--')
##pl.plot(measurementsMKI110314Freq/10.0**9, measurementsMKI110314Imp, 'g+', markersize=16.0, label="Measured MKI05-T02-HC15")
##pl.plot(freqList/10.0**3, splineFitMKI110314, 'g--', label="Interpolation of measurement Measured MKI05-T02-HC15")
##pl.plot(freqList/10.0**3, splineFitMKI110314, 'g--')
##pl.plot(measurementsMKI190514Freq/10.0**9, measurementsMKI190514Imp, 'm+', markersize=16.0, label="Measured MKI01-T05-HC16")
##pl.plot(freqList/10.0**3, splineFitMKI190514, 'm--', label="Interpolation of measurement Measured MKI01-T05-HC16")
##pl.plot(freqList/10.0**3, splineFitMKI190514, 'm--')
##pl.plot(measurementsMKI110714Freq/10.0**9, measurementsMKI110714Imp, 'k+', markersize=16.0, label="Measured MKI03-T01-HC17")
##pl.plot(freqList/10.0**3, splineFitMKI110714, 'k--', label="Interpolation of measurement Measured MKI03-T01-HC17")
##pl.plot(freqList/10.0**3, splineFitMKI110714, 'k--')
##pl.plot(matched140714[:,0]/10**9, matched140714Imp-390, "k-")
##pl.plot(matched140714LowFreq[:,0]/10**9, matched140714LowFreqImp-matched140714LowFreqImp[0], "m-")
##pl.plot(measurementsMKI231014Freq/10.0**9, measurementsMKI231014Imp, "r+", markersize=16.0, label="Measured MKI09-T03-HC18")
##pl.plot(freqList/10.0**3, splineFitMKI231014, 'r--', label="Interpolation of measurement Measured MKI09-T03-HC18")
##pl.plot(freqList/10.0**3, splineFitMKI231014, 'r--')
##pl.plot(measurementsMKI12117mmFreq/10.0**9, measurementsMKI12117mmImp, "r+", markersize=16.0, label="Measured 12 117mm")
pl.plot(freqList/10.0**3, splineFitMKI12117mm, 'r--', label="Interpolation of measurement Measured 12 117mm")
pl.plot(freqList/10.0**3, splineFitMKI12117mm, 'r--')
pl.plot(matched117mm[:,0]/10**9, signal.detrend(matched117mmImp-600)/lDUT, "r-")
pl.plot(measurementsMKI1257mmFreq/10.0**9, measurementsMKI1257mmImp, "b+", markersize=16.0, label="Measured 12 57mm")
pl.plot(freqList/10.0**3, splineFitMKI1257mm, 'b--', label="Interpolation of measurement Measured 12 57mm")
pl.plot(freqList/10.0**3, splineFitMKI1257mm, 'b--')
pl.plot(matched57mm[:,0]/10**9, signal.detrend(matched57mmImp-640)/lDUT, "b-")
##pl.plot(matched231014[:,0]/10**9, matched231014Imp-1140, "r-")
##pl.plot(inputMeasNoMatchingGatingRes[:,0]/10**9,inputMeasNoMatchingGatingResImp-inputMeasNoMatchingGatingResImp[-1], "k-", label="Measured MKI10-T06-HC13 Time Domain Gating")
##pl.plot(measurementsTank7Old2ABeforeBakeoutFreq/10.0**9, measurementsTank7Old2ABeforeBakeoutImp[:], 'g-' ,markersize=16.0, label="Measured Tank7Old2A Before bakeout")
##pl.plot(measurementsTank7Old2AAfterBakeoutFreq/10.0**9, measurementsTank7Old2AAfterBakeoutImp[:], 'b-' ,markersize=16.0, label="Measured Tank7Old2A After one bakeout")
##pl.plot(measurementsTank7Old2AFreq/10.0**9, measurementsTank7Old2AImp[:], 'k-' ,markersize=16.0, label="Measured Tank7Old2A After two bakeouts")
##pl.plot(measurementsTank7Old2AFreq/10.0**9, measurementsTank7Old2AImp[:], 'gx' ,markersize=16.0, label="Measured Tank7Old2A")
##pl.plot(freqList/10.0**3, splineFitTank7Old2A, 'g-', label="Interpolation of measurement Measured Tank7Old2A")
##pl.plot(measurementsStepOutTank2LowCoup[:,0]/10.0**3, measurementsStepOutTank2LowCoup[:,1], 'bx',label="Measured MKI2LowCoup")
##pl.plot(freqList/10.0**3, splineFitDataTank2LowCoup, label="Interpolation of measurement Measured MKI2LowCoupe.")
##pl.plot(measurementsOldMKI8dFreq[:]/10.0**9, measurementsOldMKI8dImp[:], 'gx',markersize=16.0,label="Old MKI8D Pre-TS3, twisted 15 conds")
##pl.plot(freqListOld8d/10.0**3, splineFitOld8d, 'k-',label="Interpolation of measurement Old MKI-8D")
##pl.plot(freqListOld8d/10.0**3, splineFitOld8d, 'g-')
##pl.plot(plotList[-1,0], plotList[-1,1], label="Simulated 90$^{\circ}$ rotation of screens 15 Screen Conductors)"
##pl.plot(datList[:,0]/10.0**9, datList[:,1])
##pl.plot(datSimsBigModel1100mWakeHighMeshDensityShortOverlap[:-1,0], datSimsBigModel1100mWakeHighMeshDensityShortOverlap[:-1,1]/2.97, 'r-', label="Simulation large beam screen $L_{overlap}=$60mm 100m wake")
##pl.plot(datSimsBigModel1100mWakeLowMeshDensityShortOverlap[:-1,0],datSimsBigModel1100mWakeLowMeshDensityShortOverlap[:-1,1]/2.97, 'k-', label="Simulation large beam screen $L_{overlap}=$60mm 100m wake")
##pl.plot(freqListSimMeas/10.0**9, analDatSimMeas24Cond, label="Simulated Measurements")
##pl.plot(freqListSimMeas/10.0**9, analDatSimMeas24Cond99, label="Simulated Measurements 99mm overlap")
##pl.plot(freqListSimMeas/10.0**9, analDatSimMeas24Cond99Displaced, label="Simulated Measurements 99mm overlap displaced y=0.5mm")
##pl.plot(freqListSimMeas/10.0**9, analDatSimMeas24Cond99Displacedy3mm, label="Simulated Measurements 99mm overlap displaced y=3mm")
##pl.plot(freqListSimMeas/10.0**9, analDatSimMeas24Cond99Displacedx3mm, label="Simulated Measurements 99mm overlap displaced x=3mm")
##pl.plot(freqListSimMeas/10.0**9, analDatSimMeas24Cond99Displacedx3mmy3mm, label="Simulated Measurements 99mm overlap displaced x=3mm, y=3mm")
##pl.plot(freqListSimMeas/10.0**9, analDatSimMeas24Cond99Displacedx3mmy3mmLongerTube, label="Simulated Measurements 99mm overlap Longer displaced x=3mm, y=3mm")
##pl.plot(freqListSimMeas/10.0**9, analDatSimMeas24Cond99Displacedx3mmy3mmLongerWiderTube, label="Simulated Measurements 99mm overlap Longer Wider displaced x=3mm, y=3mm")
##pl.plot(freqListSimMeas/10.0**9, analDatSimMeas24Cond99Displacedx3mmy3mmLongerWiderTubeMore, label="Simulated Measurements 99mm overlap Longer Wider displaced x=3mm, y=3mm")
##pl.plot(wake24Meas99OverlapDat[:,0], abs(wake24Meas99OverlapDat[:,1]), label="CST simulation 99mm overlap")
##pl.plot(wake24Meas99OverlapBigGeoDat[:,0], abs(wake24Meas99OverlapBigGeoDat[:,1]), label="CST simulation 99mm overlap, larger geometry")
##pl.plot(datSimsBigModelHighMesh[:,0],datSimsBigModelHighMesh[:,1]/2.97, label="Simulation large beam screen ")
##pl.plot(datSimsBigModelSmallMesh[:,0],datSimsBigModelSmallMesh[:,1]/2.97, 'r-',label="Simulation large beam screen $L_{overlap}=$107mm 50m wake")
##pl.plot(datSimsScreenModelSmallMesh[:,0],datSimsScreenModelSmallMesh[:,1]/2.97, 'g-',label="Simulation capactive coupling only $L_{overlap}=$106mm 50m wake")
##pl.plot(datSimsBigModel150mWakeMesh[:,0],datSimsBigModel150mWakeMesh[:,1]/2.97, 'm-',label="Simulation large beam screen $L_{overlap}=$106mm 50m wake")
##pl.plot(datSimsBigModel150mWakeHighMeshDensity[:-1,0], datSimsBigModel150mWakeHighMeshDensity[:-1,1]/2.97, 'b-', label="Simulation large beam screen $L_{overlap}=$106mm 150m wake")
##pl.plot(postls130m117mmDat[:,0], postls130m117mmDat[:,1]/lDUT, 'b-')
pl.plot(postls130m117mm100mDat[:,0], postls130m117mm100mDat[:,1]/lDUT, 'k-', label="Post-LS1 MKI Simulation")
##pl.plot(hllhc80mmDat[:,0], hllhc80mmDat[:,1]/lDUT, 'g-', label="80mm Simulation")

##pl.plot(impCSV119mm[:,0]/10**9, impCSV119mm[:,1]/2.45, "r-", label="L$_{overlap}$=119mm Gating")
##pl.plot(Freq99mmOverlap/10**9, Imp99mmOverlap, "rx", markersize=16.0, label="L$_{overlap}$=99mm")
##pl.plot(impCSV99mm[:,0]/10**9, impCSV99mm[:,1]/2.45, "r-", label="L$_{overlap}$=99mm Gating")
##pl.plot(Freq89mmOverlap/10**9, Imp89mmOverlap, "bx", markersize=16.0, label="L$_{overlap}$=89mm")
##pl.plot(impCSV89mm[:,0]/10**9, impCSV89mm[:,1]/2.45, "b-", label="L$_{overlap}$=89mm Gating")
##pl.plot(Freq79mmOverlap/10**9, Imp79mmOverlap, "kx", markersize=16.0, label="L$_{overlap}$=79mm")
##pl.plot(impCSV79mm[:,0]/10**9, impCSV79mm[:,1]/2.45, "k-", label="L$_{overlap}$=79mm Not-Resonant")
pl.plot(Freq69mmOverlap/10**9, Imp69mmOverlap/lDUT, "gx", markersize=16.0, label="L$_{overlap}$=69mm")
##pl.plot(impCSV69mm[:,0]/10**9, impCSV69mm[:,1]/2.45, "g-", label="L$_{overlap}$=69mm Gating")
##pl.plot(Freq59mmOverlap/10**9, Imp59mmOverlap, "mx", markersize=16.0, label="L$_{overlap}$=59mm")
##pl.plot(impCSV59mm[:,0]/10**9, impCSV59mm[:,1]/2.45, "m-", label="L$_{overlap}$=59mm Gating")

pl.plot(overlap69mmImp[:,0], overlap69mmImp[:,1]/lDUT, "m-")

##for i in range(1,5,1):
##    pl.axvline(overLapFreq(0.106,1.25*0.007,i)/10**9, color='k')
##    pl.axvline(overLapFreq(0.096,1.25*0.007,i)/10**9, color='b')

##for entry in freqListHeating:
##    pl.axvline(entry/10**3)
pl.xlabel("Frequency (GHz)", fontsize=18.0)
pl.ylabel("$\Re{}e (Z_{\parallel})$ ($\Omega$/m)", fontsize=18.0)
pl.legend(loc="upper left")
##pl.axis([0,2.0,0,60])
##pl.xlim(0,2.0)
pl.show()
pl.clf()

print len(sp.linspace(0,1.2,1200/40)), len(powLossSimulated)
pl.plot(freqListHeating/10**3, heatingFreqTank1, label="Measured MKI11-T13-MC03")
pl.plot(freqListHeating/10**3, heatingFreqTank2, label="Measured MKI12-T12-MC01")
pl.plot(freqListHeating/10**3, heatingFreqMKI08T11C09, label="Measured MKI08-T11-MC09")
pl.plot(freqListHeating/10**3, heatingFreqMKI02T08C07, label="Measured MKI02-T08-MC07")
pl.plot(freqListHeating/10**3, heatingFreqMKI071113, label="Measured MKI071113")
pl.plot(freqListHeating/10**3, heatingFreqMKI190514, label="Measured MKI071113")
pl.plot(freqListHeating/10**3, heatingFreqMKI12117mm, label="Measured 117mm")
pl.plot(freqListHeating/10**3, heatingFreqMKI1257mm, label="Measured 57mm")
##pl.plot(sp.linspace(0,1.2,1200/40),powLossSimulated)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("Power Loss (W)", fontsize=16.0)
pl.legend(loc="lower right")
##pl.axis([0,2,0,6])
##pl.show()
pl.clf()

pl.plot(freqListHeating/10**3, summedFreqTank1, label="Measured MKI11-T13-MC03")
pl.plot(freqListHeating/10**3, summedFreqTank2, label="Measured MKI12-T12-MC01")
pl.plot(freqListHeating/10**3, summedFreqMKI08T11C09, label="Measured MKI08-T11-MC09")
pl.plot(freqListHeating/10**3, summedFreqMKI02T08C07, label="Measured MKI02-T08-MC07")
pl.plot(freqListHeating/10**3, summedFreqMKI071113, label="Measured MKI071113")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("Power Loss (W)", fontsize=16.0)
pl.legend(loc="upper left")
##pl.axis([0,2,0,50])
##pl.show()
pl.clf()

######## Plot Current Spectrum With Impedance #########

ax1=pl.subplot(111)
pl.plot(measurementsMKI071113Freq/10.0**9, measurementsMKI071113Imp, 'mx', markersize=16.0, label="Measured MKI06-T07-HC12")
pl.plot(freqList/10.0**3, splineFitMKI071113, 'm-', label="Interpolation of measurement Measured MKI06-T07-HC12")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$\Re{}e (Z_{\parallel})$ ($\Omega$/m)", fontsize=16.0)
pl.ylim(0,60)
pl.legend()
ax2 = ax1.twinx()
pl.plot(freqList/10**3,linToLog(specGauss), label="Gaussian Profile")
pl.legend()
pl.ylabel("Beam Current Spectra (dB)", fontsize=16.0)
##pl.show()
pl.clf()


######## Writing Fitted Data to an External File ##########

fileOutputOld8D=open("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/23-07-13/mki8d-old/InterpolatedResDataOld8d.csv", "w+")
fileOutputMKI10MC06=open("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/07-11-13/mki10-mc06/InterpolatedResData.csv", "w+")
fileOutputMKI11MC03=open("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/07-11-13/mki10-mc06/InterpolatedResDataHighest.csv", "w+")

for i in range(0,len(splineFitOld8d)):
    fileOutputOld8D.write(str(freqListOld8d[i])+" ,"+str(splineFitOld8d[i])+"\n")

for i in range(0,len(splineFitMKI071113)):
    fileOutputMKI10MC06.write(str(freqList[i])+" ,"+str(splineFitMKI071113[i])+"\n")

for i in range(0,len(splineFitMKI071113)):
    fileOutputMKI11MC03.write(str(freqList[i])+" ,"+str(splineFitDataTank1[i])+"\n")

fileOutputOld8D.close()
fileOutputMKI10MC06.close()
fileOutputMKI11MC03.close()




######## FFT and gating of matched resistor measurements #########
fftMKIRes = fftPack.ifft(transDataRes[:,3])
fftMKIRes = fftPack.ifftshift(fftMKIRes[1:-2])
##pl.plot(transDataRes[:,3])
pl.plot(fftMKIRes[1:-2])
##pl.show()
pl.clf()
