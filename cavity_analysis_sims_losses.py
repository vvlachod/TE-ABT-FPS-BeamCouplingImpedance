import scipy as sp
import pylab as pl
import time, os, sys
import scipy.integrate as inte

###### Define Impedance from resonator ######

def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

###### Define constants ####
charge = 18.4*10**-9
t_bunch = 50*10**-9
sigma_z = 0.075
C = 299792458.0
circ = 26679.0
eps0 = 8.854 * 10 **-12
mu0 = 4*sp.pi*10**-7
Z0 = 120*sp.pi
start=time.time()
bound_typ = "PerfE"
x01 = 0.001
x02 = 0.0011
dy = 0.001
disp_y = 0.0005
power_loss_tot = 0.0
top_directory = "E:/PhD/1st_Year_09-10/Data/TCTP/eigenmode_solutions/longitudinal_eigenmodes/"

eigenmode_store = [] ###### first column - freq, second - Q, Third R, Fourth - Power Loss
eigenmode_store_transverse_Ez = []
eigenmode_store_transverse_EyBx = []
directory_list = [top_directory+i+"/" for i in os.listdir(top_directory)]

count = 0
for work_directory in directory_list:

    if work_directory == top_directory+"tex_output/":
        pass
    elif work_directory == top_directory+"f193/":
        pass
    else:
        for pos in os.listdir(work_directory):
            if "PerfE" in pos:
                bound_typ = "PerfE"
            elif "perfE" in pos:
                bound_typ = "perfE"
            elif "PerfH" in pos:
                bound_typ = "PerfH"
            elif "perfH" in pos:
                bound_typ = "perfH"
            else:
                pass
##        print work_directory
        file_input = open(work_directory+"E_field_"+bound_typ+".fld", 'r+')
        data = file_input.readlines()
        Ez = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ez.append([temp[2],complex(temp[-2],temp[-1])])

        ##Ez = sp.array(Ez)

        ##print Ez
        if bound_typ == "PerfE" or bound_typ == "perfE":
            temp = []
            for i in range(1,len(Ez)):
                temp.append([-Ez[-i][0], complex((Ez[-i][1]).real,(Ez[-i][1]).imag)])
            for i in range(0,len(Ez)):
                temp.append([Ez[i][0],complex((Ez[i][1]).real,(Ez[i][1]).imag)])
            Ez = sp.array(temp)
        elif bound_typ == "PerfH" or "perfH":
            temp = []
            for i in range(1,len(Ez)):
                temp.append([-Ez[-i][0], complex(-(Ez[-i][1]).real,-(Ez[-i][1]).imag)])
            for i in range(0,len(Ez)):
                temp.append([Ez[i][0], complex((Ez[i][1]).real,(Ez[i][1]).imag)])
            Ez = sp.array(temp)

        ##pl.plot(Ez[:,0], Ez[:,1].real)
        ##pl.plot(Ez[:,0], Ez[:,1].imag)
        ##pl.show()
        ##pl.clf()

        gap = (Ez[1][0]-Ez[0][0])

        file_input = open(work_directory+"E_field_"+bound_typ+".fld", 'r+')
        data = file_input.readlines()
        Ey = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ey.append([temp[2],complex(temp[-4],temp[-3])])

        ##Ey = sp.array(Ey)
        ##print Ey

        if bound_typ == "PerfE" or bound_typ == "perfE":
            temp = []
            for i in range(1,len(Ey)):
                temp.append([-Ey[-i][0], complex(-(Ey[-i][1]).real,-(Ey[-i][1]).imag)])
            for i in range(0,len(Ey)):
                temp.append([Ey[i][0],complex((Ey[i][1]).real,(Ey[i][1]).imag)])
            Ey = sp.array(temp)
        elif bound_typ == "PerfH" or bound_typ == "perfH":
            temp = []
            for i in range(1,len(Ey)):
                temp.append([-Ey[-i][0], complex((Ey[-i][1]).real,(Ey[-i][1]).imag)])
            for i in range(0,len(Ey)):
                temp.append([Ey[i][0], complex((Ey[i][1]).real,(Ey[i][1]).imag)])
            Ey = sp.array(temp)

        try:
            file_input = open(work_directory+"E_field_"+bound_typ+"_displaced.fld", 'r+')
        except:
            file_input = open(work_directory+"E_field_displaced_"+bound_typ+".fld", 'r+')
            
        data = file_input.readlines()
        Ezdisp1 = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ezdisp1.append([temp[2],complex(temp[-2],temp[-1])])

        ##Ey = sp.array(Ey)
        ##print Ezdisp

        if bound_typ == "PerfE" or bound_typ == "perfE":
            temp = []
            for i in range(1,len(Ezdisp1)):
                temp.append([-Ezdisp1[-i][0], complex(-(Ezdisp1[-i][1]).real,-(Ezdisp1[-i][1]).imag)])
            for i in range(0,len(Ezdisp1)):
                temp.append([Ezdisp1[i][0],complex((Ezdisp1[i][1]).real,(Ezdisp1[i][1]).imag)])
            Ezdisp1 = sp.array(temp)
        elif bound_typ == "PerfH" or bound_typ == "perfH":
            temp = []
            for i in range(1,len(Ezdisp1)):
                temp.append([-Ezdisp1[-i][0], complex((Ezdisp1[-i][1]).real,(Ezdisp1[-i][1]).imag)])
            for i in range(0,len(Ezdisp1)):
                temp.append([Ezdisp1[i][0], complex((Ezdisp1[i][1]).real,(Ezdisp1[i][1]).imag)])
            Ezdisp1 = sp.array(temp)

        try:
            file_input = open(work_directory+"H_field_"+bound_typ+"_displaced.fld", 'r+')
        except:
            pass
        try:
            file_input = open(work_directory+"H_field_displaced_"+bound_typ+".fld", 'r+')
        except:
            pass
        try:
            file_input = open(work_directory+"H_field_"+bound_typ+".fld", 'r+')
        except:
            pass
                    
            
        data = file_input.readlines()
        Bx = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Bx.append([temp[2],mu0*complex(temp[-6],temp[-5])])

        ##Bx = sp.array(Bx)
        ##print Bx

        if bound_typ == "PerfE" or bound_typ == "perfE":
            temp = []
            for i in range(1,len(Bx)):
                temp.append([-Bx[-i][0], complex((Bx[-i][1]).real,(Bx[-i][1]).imag)])
            for i in range(0,len(Bx)):
                temp.append([Bx[i][0],complex((Bx[i][1]).real,(Bx[i][1]).imag)])
            Bx = sp.array(temp)
        elif bound_typ == "PerfH" or bound_typ == "perfH":
            temp = []
            for i in range(1,len(Bx)):
                temp.append([-Bx[-i][0], complex(-(Bx[-i][1]).real,-(Bx[-i][1]).imag)])
            for i in range(0,len(Bx)):
                temp.append([Bx[i][0], complex((Bx[i][1]).real,(Bx[i][1]).imag)])
            Bx = sp.array(temp)

        file_input = open(work_directory+"q_1.csv", 'r+')
        data = file_input.readlines()
        Q = []
        for i in data[1:]:
            Q.append(map(float, i.rsplit(',')))

        Q = sp.array(Q)
        ##print Q

        file_input = open(work_directory+"stored_energy.csv", 'r+')
        data = file_input.readlines()
        stored_energy = []
        for i in data[1:]:
            stored_energy.append(map(float, i.rsplit(',')))

        ##print stored_energy

        file_input = open(work_directory+"freq.csv", 'r+')
        data = file_input.readlines()
        freq = []
        for i in data[1:]:
##            print i
            temp = map(float, i.rsplit(','))
            freq.append([temp[0],complex(temp[2],temp[1])])

        freq = sp.array(freq)

        if count == 68:
            pl.plot(Ez[:,0],Ez[:,1])
            pl.show()
            pl.clf()
        count+=1

        gap = Ez[1,0]-Ez[0,0]
        confluence = []
        for i in range(0,len(Ez)):
            confluence.append(Ez[i,1]*complex(sp.cos(freq[0,1]*2*sp.pi*Ez[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ez[i,0]/C)))


        long_r_over_q_alt = abs(inte.simps(confluence, Ez[:,0], gap))*(Ez[-1,0]-Ez[0,0])/C

        gap = Ezdisp1[1,0]-Ezdisp1[0,0]
        confluence = []
        for i in range(0,len(Ezdisp1)):
            confluence.append((Ezdisp1[i,1]-Ez[i,1])*complex(sp.cos(freq[0,1]*2*sp.pi*Ezdisp1[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ezdisp1[i,0]/C))/disp_y)

        k_y_from_dEzdy1 = C*abs(inte.simps(confluence, Ezdisp1[:,0], gap))**2/(4*2*sp.pi*freq[0,1]*stored_energy[0][1])
        trans_r_over_q_from_dEzdy1 = 4*k_y_from_dEzdy1/(2*sp.pi*freq[0,1])

        gap = Ey[1,0]-Ey[0,0]
        confluence = []
        for i in range(0,len(Ey)):
            confluence.append(complex(Ey[i,1]+C*Bx[i,1])*complex(sp.cos(freq[0,1]*2*sp.pi*Ey[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ey[i,0]/C)))

        k_y_from_Ey_Bx = abs(inte.simps(confluence, Ey[:,0], gap))**2*(2*sp.pi*freq[0,1])/(4*C*stored_energy[0][1])
        trans_r_over_q_from_Ey_Bx = 4*k_y_from_Ey_Bx/(2*sp.pi*freq[0,1])
##        print trans_r_over_q_from_Ey_Bx/trans_r_over_q_from_dEzdy1, long_r_over_q_alt
        
    ##    print "Frequency of mode is %f" % freq[0,1]
    ##    print "Q-factor of mode is %f" % Q[0,1]
    ##    print "Transverse kicker factor k_y from dEz/dy1 (V/nC/mm) = %f" % (k_y_from_dEzdy1/10**12)
    ##    print "Transverse R/Q from dEz/dy1 (Ohms/mm) = %f" % (trans_r_over_q_from_dEzdy1/10**3)
    ##    print "Transverse Rs1 = %f (Ohms/mm)" % (trans_r_over_q_from_dEzdy1*Q[0,1]/10**3)
    ##    ##print "Transverse kicker factor k_y from  dEz/dy2 (V/nC/mm) = %f" % (k_y_from_dEzdy2)
    ##    ##print "Transverse R/Q from dEz/dy2 (Ohms/mm) = %f" % (trans_r_over_q_from_dEzdy2)
    ##    ##print "Transverse  Rs2 = %f (Ohms/mm)" % (trans_r_over_q_from_dEzdy2*Q[0,1])
    ##    print "Transverse kicker factor k_y from Ey+cBx (V/nC/mm) = %f" % (k_y_from_Ey_Bx/10**12)
    ##    print "Transverse R/Q from Ey+cBx (Ohms/mm) = %f" % (trans_r_over_q_from_Ey_Bx/10**3)
    ##    print "Transverse Rs = %f (Ohms/mm)" % (trans_r_over_q_from_Ey_Bx*Q[0,1]/10**3)
    ##    print "Longitudinal R/Q = %f" % long_r_over_q
    ##    print "Longitudinal Rs = %f (Ohms)" % (long_r_over_q*Q[0,1])
    ##    print "Longitudinal R/Q alt= %f" % long_r_over_q_alt
    ##    print "Longitudinal Rs alt= %f (Ohms)" % (long_r_over_q_alt*Q[0,1])
    ##    print "Power loss is: %f (Watts)" % ((charge/t_bunch)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2))
        print freq[0,1], Q[0,1], long_r_over_q_alt, count
        eigenmode_store.append([freq[0,1], Q[0,1], long_r_over_q_alt*Q[0,1], (charge/t_bunch)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2)])
        eigenmode_store_transverse_Ez.append([freq[0,1], Q[0,1], trans_r_over_q_from_dEzdy1*Q[0,1]])
        eigenmode_store_transverse_EyBx.append([freq[0,1], Q[0,1], trans_r_over_q_from_Ey_Bx*Q[0,1]])
        power_loss_tot += ((charge/t_bunch)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2)).real



