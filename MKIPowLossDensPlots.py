import csv, time, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack
import mpl_toolkits.mplot3d as ToolKit3d
import impedance.impedance as imp
import impedance.powlossprof as prof

####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

######### Define importing, analysis functions #########

c=299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005
lWire = 3.551
lDUT = 2.45
resCopper = 17.24*10.0**-9
resOuter = 1/(3.3*10.0**7)
ZChar = 60*sp.log(dAper/rWire)

def minValIndex(arrMark, minValue=10.0):
    return sp.array([i for i in range(0,len(arrMark)) if arrMark[i]>minValue]) 

start=time.time()

length = 2.88
f_rev = C/27000.0
nBunches = 2808
qPart = 1.6*10.0**-19
nPart=1.15*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=1.001*10.0**-9
testImp = [250*10**6, 10.0, 10000]
bunchCur=nPart*qPart*f_rev
print bCur, bunchCur

directory = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/"
lossTest = directory+"f0.4233DLoss.txt"
lossTestf0838 = directory+"f0.8383DLoss.txt"
lossTestf1239 = directory+"f1.2393DLoss.txt"
lossTestf1652 = directory+"f1.6523DLoss.txt"
lossTestf1977 = directory+"f1.9773DLoss.txt"
datTest=sp.array(imp.extract_dat2013LossMap(lossTest))
(x,y,z), fieldsPlot, lens = imp.meshGridCSTFieldMap(datTest)
lossSumOnZ = imp.cumSumLoss3DInZ(fieldsPlot, lens[2])
lossOnZ = imp.sumLoss3DInZ(fieldsPlot, lens[2])
lossSliceZ = imp.cumSumLoss3DInZDiff(fieldsPlot, "X")

fieldsPlot=None
lens = None
(x,y,z)=(None,None,None)

datTestf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x,y,z), fieldsPlotf0838, lensf0838 = imp.meshGridCSTFieldMap(datTestf0838)

lossSumOnZf0838 = imp.cumSumLoss3DInZ(fieldsPlotf0838, lensf0838[2])
lossOnZf0838 = imp.sumLoss3DInZ(fieldsPlotf0838, lensf0838[2])
lossSliceZf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotf0838, "X")

fieldsPlotf0838=None
lensf0838 = None
(x,y,z)=(None,None,None)


datTestf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x,y,z), fieldsPlotf1239, lensf1239 = imp.meshGridCSTFieldMap(datTestf1239)

lossSumOnZf1239 = imp.cumSumLoss3DInZ(fieldsPlotf1239, lensf1239[2])
lossOnZf1239 = imp.sumLoss3DInZ(fieldsPlotf1239, lensf1239[2])
lossSliceZf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotf1239, "X")

fieldsPlotf1239=None
lensf1239 = None
(x,y,z)=(None,None,None)

datTestf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x,y,z), fieldsPlotf1652, lensf1652 = imp.meshGridCSTFieldMap(datTestf1652)
lossSumOnZf1652 = imp.cumSumLoss3DInZ(fieldsPlotf1652, lensf1652[2])
lossOnZf1652 = imp.sumLoss3DInZ(fieldsPlotf1652, lensf1652[2])
lossSliceZf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotf1652, "X")

fieldsPlotf1652=None
lensf1652 = None
(x,y,z)=(None,None,None)

datTestf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x,y,z), fieldsPlotf1977, lensf1977 = imp.meshGridCSTFieldMap(datTestf1977)
lossSumOnZf1977 = imp.cumSumLoss3DInZ(fieldsPlotf1977, lensf1977[2])
lossOnZf1977 = imp.sumLoss3DInZ(fieldsPlotf1977, lensf1977[2])
lossSliceZf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotf1977, "X")

fieldsPlotf1977=None
lensf1977 = None

directorySidePlate = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/lossWithSidePlateGround/"
lossTestf0423 = directorySidePlate+"f0423.txt"
lossTestf0838 = directorySidePlate+"f0838.txt"
lossTestf1239 = directorySidePlate+"f1239.txt"
lossTestf1652 = directorySidePlate+"f1652.txt"
lossTestf1977 = directorySidePlate+"f1977.txt"

datTestSidePlatef0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x,y,z), fieldsPlotSidePlatef0423, lensSidePlatef0423 = imp.meshGridCSTFieldMap(datTestSidePlatef0423)
lossSumOnZSidePlatef0423 = imp.cumSumLoss3DInZ(fieldsPlotSidePlatef0423, lensSidePlatef0423[2])
lossOnZSidePlatef0423 = imp.sumLoss3DInZ(fieldsPlotSidePlatef0423, lensSidePlatef0423[2])
lossSliceZSidePlatef0423 = imp.cumSumLoss3DInZDiff(fieldsPlotSidePlatef0423, "X")

fieldsPlotSidePlatef0423=None
lensSidePlatef0423 = None

datTestSidePlatef0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x,y,z), fieldsPlotSidePlatef0838, lensSidePlatef0838 = imp.meshGridCSTFieldMap(datTestSidePlatef0838)
lossSumOnZSidePlatef0838 = imp.cumSumLoss3DInZ(fieldsPlotSidePlatef0838, lensSidePlatef0838[2])
lossOnZSidePlatef0838 = imp.sumLoss3DInZ(fieldsPlotSidePlatef0838, lensSidePlatef0838[2])
lossSliceZSidePlatef0838 = imp.cumSumLoss3DInZDiff(fieldsPlotSidePlatef0838, "X")

fieldsPlotSidePlatef0838=None
lensSidePlatef0838 = None
(x,y,z)=(None,None,None)

datTestSidePlatef1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x,y,z), fieldsPlotSidePlatef1239, lensSidePlatef1239 = imp.meshGridCSTFieldMap(datTestSidePlatef1239)
lossSumOnZSidePlatef1239 = imp.cumSumLoss3DInZ(fieldsPlotSidePlatef1239, lensSidePlatef1239[2])
lossOnZSidePlatef1239 = imp.sumLoss3DInZ(fieldsPlotSidePlatef1239, lensSidePlatef1239[2])
lossSliceZSidePlatef1239 = imp.cumSumLoss3DInZDiff(fieldsPlotSidePlatef1239, "X")

fieldsPlotSidePlatef1239=None
lensSidePlatef1239 = None
(x,y,z)=(None,None,None)

datTestSidePlatef1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x,y,z), fieldsPlotSidePlatef1652, lensSidePlatef1652 = imp.meshGridCSTFieldMap(datTestSidePlatef1652)
lossSumOnZSidePlatef1652 = imp.cumSumLoss3DInZ(fieldsPlotSidePlatef1652, lensSidePlatef1652[2])
lossOnZSidePlatef1652 = imp.sumLoss3DInZ(fieldsPlotSidePlatef1652, lensSidePlatef1652[2])
lossSliceZSidePlatef1652 = imp.cumSumLoss3DInZDiff(fieldsPlotSidePlatef1652, "X")

fieldsPlotSidePlatef1652=None
lensSidePlatef1652 = None
(x,y,z)=(None,None,None)

datTestSidePlatef1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x,y,z), fieldsPlotSidePlatef1977, lensSidePlatef1977 = imp.meshGridCSTFieldMap(datTestSidePlatef1977)
lossSumOnZSidePlatef1977 = imp.cumSumLoss3DInZ(fieldsPlotSidePlatef1977, lensSidePlatef1977[2])
lossOnZSidePlatef1977 = imp.sumLoss3DInZ(fieldsPlotSidePlatef1977, lensSidePlatef1977[2])
lossSliceZSidePlatef1977 = imp.cumSumLoss3DInZDiff(fieldsPlotSidePlatef1977, "X")

fieldsPlotSidePlatef1977=None
lensSidePlatef1977 = None


directoryCapEndAir = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/lossAirEndRings/"

lossTestf0423 = directoryCapEndAir+"f0423.txt"
lossTestf0838 = directoryCapEndAir+"f0838.txt"
lossTestf1239 = directoryCapEndAir+"f1239.txt"
lossTestf1652 = directoryCapEndAir+"f1652.txt"
lossTestf1977 = directoryCapEndAir+"f1977.txt"

datTestCapEndAirf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x,y,z), fieldsPlotCapEndAirf0423, lensCapEndAirf0423 = imp.meshGridCSTFieldMap(datTestCapEndAirf0423)
lossSumOnZCapEndAirf0423 = imp.cumSumLoss3DInZ(fieldsPlotCapEndAirf0423, lensCapEndAirf0423[2])
lossOnZCapEndAirf0423 = imp.sumLoss3DInZ(fieldsPlotCapEndAirf0423, lensCapEndAirf0423[2])
lossSliceZCapEndAirf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotCapEndAirf0423, "X")

fieldsPlotCapEndAirf0423=None
lensCapEndAirf0423 = None

datTestCapEndAirf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x,y,z), fieldsPlotCapEndAirf0838, lensCapEndAirf0838 = imp.meshGridCSTFieldMap(datTestCapEndAirf0838)
lossSumOnZCapEndAirf0838 = imp.cumSumLoss3DInZ(fieldsPlotCapEndAirf0838, lensCapEndAirf0838[2])
lossOnZCapEndAirf0838 = imp.sumLoss3DInZ(fieldsPlotCapEndAirf0838, lensCapEndAirf0838[2])
lossSliceZCapEndAirf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotCapEndAirf0838, "X")

fieldsPlotCapEndAirf0838=None
lensCapEndAirf0838 = None
(x,y,z)=(None,None,None)

datTestCapEndAirf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x,y,z), fieldsPlotCapEndAirf1239, lensCapEndAirf1239 = imp.meshGridCSTFieldMap(datTestCapEndAirf1239)
lossSumOnZCapEndAirf1239 = imp.cumSumLoss3DInZ(fieldsPlotCapEndAirf1239, lensCapEndAirf1239[2])
lossOnZCapEndAirf1239 = imp.sumLoss3DInZ(fieldsPlotCapEndAirf1239, lensCapEndAirf1239[2])
lossSliceZCapEndAirf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotCapEndAirf1239, "X")

fieldsPlotCapEndAirf1239=None
lensCapEndAirf1239 = None
(x,y,z)=(None,None,None)

datTestCapEndAirf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x,y,z), fieldsPlotCapEndAirf1652, lensCapEndAirf1652 = imp.meshGridCSTFieldMap(datTestCapEndAirf1652)
lossSumOnZCapEndAirf1652 = imp.cumSumLoss3DInZ(fieldsPlotCapEndAirf1652, lensCapEndAirf1652[2])
lossOnZCapEndAirf1652 = imp.sumLoss3DInZ(fieldsPlotCapEndAirf1652, lensCapEndAirf1652[2])
lossSliceZCapEndAirf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotCapEndAirf1652, "X")

fieldsPlotCapEndAirf1652=None
lensSCapEndAirf1652 = None
(x,y,z)=(None,None,None)

datTestCapEndAirf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x,y,z), fieldsPlotCapEndAirf1977, lensCapEndAirf1977 = imp.meshGridCSTFieldMap(datTestCapEndAirf1977)
lossSumOnZCapEndAirf1977 = imp.cumSumLoss3DInZ(fieldsPlotCapEndAirf1977, lensCapEndAirf1977[2])
lossOnZCapEndAirf1977 = imp.sumLoss3DInZ(fieldsPlotCapEndAirf1977, lensCapEndAirf1977[2])
lossSliceZCapEndAirf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotCapEndAirf1977, "X")

fieldsPlotCapEndAirf1977=None
lensCapEndAirf1977 = None

##directoryRingsAir50m = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/lossAirEndRings/50mWake/"
##
##lossTestf0423 = directoryRingsAir50m+"f0423.txt"
##lossTestf0838 = directoryRingsAir50m+"f0838.txt"
##lossTestf1239 = directoryRingsAir50m+"f1239.txt"
##lossTestf1652 = directoryRingsAir50m+"f1652.txt"
##lossTestf1977 = directoryRingsAir50m+"f1977.txt"
##
##datTestRingsAir50mf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
####print len(datTestSidePlatef0423)
##(x,y,z), fieldsPlotRingsAir50mf0423, lensRingsAir50mf0423 = imp.meshGridCSTFieldMap(datTestRingsAir50mf0423)
##lossSumOnZRingsAir50mf0423 = imp.cumSumLoss3DInZ(fieldsPlotRingsAir50mf0423, lensRingsAir50mf0423[2])
##lossOnZRingsAir50mf0423 = imp.sumLoss3DInZ(fieldsPlotRingsAir50mf0423, lensRingsAir50mf0423[2])
##lossSliceZRingsAir50mf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotRingsAir50mf0423, "X")
##
##fieldsPlotRingsAir50mf0423=None
##lensRingsAir50mf0423 = None
##
##datTestRingsAir50mf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
##(x,y,z), fieldsPlotRingsAir50mf0838, lensRingsAir50mf0838 = imp.meshGridCSTFieldMap(datTestRingsAir50mf0838)
##lossSumOnZRingsAir50mf0838 = imp.cumSumLoss3DInZ(fieldsPlotRingsAir50mf0838, lensRingsAir50mf0838[2])
##lossOnZRingsAir50mf0838 = imp.sumLoss3DInZ(fieldsPlotRingsAir50mf0838, lensRingsAir50mf0838[2])
##lossSliceZRingsAir50mf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotRingsAir50mf0838, "X")
##
##fieldsPlotRingsAir50mf0838=None
##lensRingsAir50mf0838 = None
##(x,y,z)=(None,None,None)
##
##datTestRingsAir50mf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
##(x,y,z), fieldsPlotRingsAir50mf1239, lensRingsAir50mf1239 = imp.meshGridCSTFieldMap(datTestRingsAir50mf1239)
##lossSumOnZRingsAir50mf1239 = imp.cumSumLoss3DInZ(fieldsPlotRingsAir50mf1239, lensRingsAir50mf1239[2])
##lossOnZRingsAir50mf1239 = imp.sumLoss3DInZ(fieldsPlotRingsAir50mf1239, lensRingsAir50mf1239[2])
##lossSliceZRingsAir50mf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotRingsAir50mf1239, "X")
##
##fieldsPlotRingsAir50mf1239=None
##lensRingsAir50mf1239 = None
##(x,y,z)=(None,None,None)
##
##datTestRingsAir50mf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
##(x,y,z), fieldsPlotRingsAir50mf1652, lensRingsAir50mf1652 = imp.meshGridCSTFieldMap(datTestRingsAir50mf1652)
##lossSumOnZRingsAir50mf1652 = imp.cumSumLoss3DInZ(fieldsPlotRingsAir50mf1652, lensRingsAir50mf1652[2])
##lossOnZRingsAir50mf1652 = imp.sumLoss3DInZ(fieldsPlotRingsAir50mf1652, lensRingsAir50mf1652[2])
##lossSliceZRingsAir50mf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotRingsAir50mf1652, "X")
##
##fieldsPlotRingsAir50mf1652=None
##lensSRingsAir50mf1652 = None
##(x,y,z)=(None,None,None)
##
##datTestRingsAir50mf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
##(x,y,z), fieldsPlotRingsAir50mf1977, lensRingsAir50mf1977 = imp.meshGridCSTFieldMap(datTestRingsAir50mf1977)
##lossSumOnZRingsAir50mf1977 = imp.cumSumLoss3DInZ(fieldsPlotRingsAir50mf1977, lensRingsAir50mf1977[2])
##lossOnZRingsAir50mf1977 = imp.sumLoss3DInZ(fieldsPlotRingsAir50mf1977, lensRingsAir50mf1977[2])
##lossSliceZRingsAir50mf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotRingsAir50mf1977, "X")
##
##fieldsPlotRingsAir50mf1977=None
##lensRingsAir50mf1977 = None

directoryRings50mmDiam = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/ferrRings50mmD/"

lossTestf0423 = directoryRings50mmDiam+"f0423.txt"
lossTestf0838 = directoryRings50mmDiam+"f0838.txt"
lossTestf1239 = directoryRings50mmDiam+"f1239.txt"
lossTestf1652 = directoryRings50mmDiam+"f1652.txt"
lossTestf1977 = directoryRings50mmDiam+"f1977.txt"

datTestRings50mmDiamf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x,y,z), fieldsPlotRings50mmDiamf0423, lensRings50mmDiamf0423 = imp.meshGridCSTFieldMap(datTestRings50mmDiamf0423)
lossSumOnZRings50mmDiamf0423 = imp.cumSumLoss3DInZ(fieldsPlotRings50mmDiamf0423, lensRings50mmDiamf0423[2])
lossOnZRings50mmDiamf0423 = imp.sumLoss3DInZ(fieldsPlotRings50mmDiamf0423, lensRings50mmDiamf0423[2])
lossSliceZRings50mmDiamf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotRings50mmDiamf0423, "X")

fieldsPlotRings50mmDiamf0423=None
lensRings50mmDiamf0423 = None

datTestRings50mmDiamf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x,y,z), fieldsPlotRings50mmDiamf0838, lensRings50mmDiamf0838 = imp.meshGridCSTFieldMap(datTestRings50mmDiamf0838)
lossSumOnZRings50mmDiamf0838 = imp.cumSumLoss3DInZ(fieldsPlotRings50mmDiamf0838, lensRings50mmDiamf0838[2])
lossOnZRings50mmDiamf0838 = imp.sumLoss3DInZ(fieldsPlotRings50mmDiamf0838, lensRings50mmDiamf0838[2])
lossSliceZRings50mmDiamf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotRings50mmDiamf0838, "X")

fieldsPlotRings50mmDiamf0838=None
lensRings50mmDiamf0838 = None
(x,y,z)=(None,None,None)

datTestRings50mmDiamf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x,y,z), fieldsPlotRings50mmDiamf1239, lensRings50mmDiamf1239 = imp.meshGridCSTFieldMap(datTestRings50mmDiamf1239)
lossSumOnZRings50mmDiamf1239 = imp.cumSumLoss3DInZ(fieldsPlotRings50mmDiamf1239, lensRings50mmDiamf1239[2])
lossOnZRings50mmDiamf1239 = imp.sumLoss3DInZ(fieldsPlotRings50mmDiamf1239, lensRings50mmDiamf1239[2])
lossSliceZRings50mmDiamf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotRings50mmDiamf1239, "X")

fieldsPlotRings50mmDiamf1239=None
lensRings50mmDiamf1239 = None
(x,y,z)=(None,None,None)

datTestRings50mmDiamf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x,y,z), fieldsPlotRings50mmDiamf1652, lensRings50mmDiamf1652 = imp.meshGridCSTFieldMap(datTestRings50mmDiamf1652)
lossSumOnZRings50mmDiamf1652 = imp.cumSumLoss3DInZ(fieldsPlotRings50mmDiamf1652, lensRings50mmDiamf1652[2])
lossOnZRings50mmDiamf1652 = imp.sumLoss3DInZ(fieldsPlotRings50mmDiamf1652, lensRings50mmDiamf1652[2])
lossSliceZRings50mmDiamf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotRings50mmDiamf1652, "X")

fieldsPlotRings50mmDiamf1652=None
lensSRings50mmDiamf1652 = None
(x,y,z)=(None,None,None)

datTestRings50mmDiamf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x,y,z), fieldsPlotRings50mmDiamf1977, lensRings50mmDiamf1977 = imp.meshGridCSTFieldMap(datTestRings50mmDiamf1977)
lossSumOnZRings50mmDiamf1977 = imp.cumSumLoss3DInZ(fieldsPlotRings50mmDiamf1977, lensRings50mmDiamf1977[2])
lossOnZRings50mmDiamf1977 = imp.sumLoss3DInZ(fieldsPlotRings50mmDiamf1977, lensRings50mmDiamf1977[2])
lossSliceZRings50mmDiamf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotRings50mmDiamf1977, "X")

fieldsPlotRings50mmDiamf1977=None
lensRings50mmDiamf1977 = None


directoryRings60mmDiam = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/ferrRing60mmOuterD/"

lossTestf0423 = directoryRings60mmDiam+"f0423.txt"
lossTestf0838 = directoryRings60mmDiam+"f0838.txt"
lossTestf1239 = directoryRings60mmDiam+"f1239.txt"
lossTestf1652 = directoryRings60mmDiam+"f1652.txt"
lossTestf1977 = directoryRings60mmDiam+"f1977.txt"

datTestRings60mmDiamf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x,y,z), fieldsPlotRings60mmDiamf0423, lensRings60mmDiamf0423 = imp.meshGridCSTFieldMap(datTestRings60mmDiamf0423)
lossSumOnZRings60mmDiamf0423 = imp.cumSumLoss3DInZ(fieldsPlotRings60mmDiamf0423, lensRings60mmDiamf0423[2])
lossOnZRings60mmDiamf0423 = imp.sumLoss3DInZ(fieldsPlotRings60mmDiamf0423, lensRings60mmDiamf0423[2])
lossSliceZRings60mmDiamf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotRings60mmDiamf0423, "X")

fieldsPlotRings60mmDiamf0423=None
lensRings60mmDiamf0423 = None

datTestRings60mmDiamf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x,y,z), fieldsPlotRings60mmDiamf0838, lensRings60mmDiamf0838 = imp.meshGridCSTFieldMap(datTestRings60mmDiamf0838)
lossSumOnZRings60mmDiamf0838 = imp.cumSumLoss3DInZ(fieldsPlotRings60mmDiamf0838, lensRings60mmDiamf0838[2])
lossOnZRings60mmDiamf0838 = imp.sumLoss3DInZ(fieldsPlotRings60mmDiamf0838, lensRings60mmDiamf0838[2])
lossSliceZRings60mmDiamf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotRings60mmDiamf0838, "X")

fieldsPlotRings60mmDiamf0838=None
lensRings60mmDiamf0838 = None
(x,y,z)=(None,None,None)

datTestRings60mmDiamf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x,y,z), fieldsPlotRings60mmDiamf1239, lensRings60mmDiamf1239 = imp.meshGridCSTFieldMap(datTestRings60mmDiamf1239)
lossSumOnZRings60mmDiamf1239 = imp.cumSumLoss3DInZ(fieldsPlotRings60mmDiamf1239, lensRings60mmDiamf1239[2])
lossOnZRings60mmDiamf1239 = imp.sumLoss3DInZ(fieldsPlotRings60mmDiamf1239, lensRings60mmDiamf1239[2])
lossSliceZRings60mmDiamf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotRings60mmDiamf1239, "X")

fieldsPlotRings60mmDiamf1239=None
lensRings60mmDiamf1239 = None
(x,y,z)=(None,None,None)

datTestRings60mmDiamf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x,y,z), fieldsPlotRings60mmDiamf1652, lensRings60mmDiamf1652 = imp.meshGridCSTFieldMap(datTestRings60mmDiamf1652)
lossSumOnZRings60mmDiamf1652 = imp.cumSumLoss3DInZ(fieldsPlotRings60mmDiamf1652, lensRings60mmDiamf1652[2])
lossOnZRings60mmDiamf1652 = imp.sumLoss3DInZ(fieldsPlotRings60mmDiamf1652, lensRings60mmDiamf1652[2])
lossSliceZRings60mmDiamf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotRings60mmDiamf1652, "X")

fieldsPlotRings60mmDiamf1652=None
lensSRings60mmDiamf1652 = None
(x,y,z)=(None,None,None)

datTestRings60mmDiamf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x,y,z), fieldsPlotRings60mmDiamf1977, lensRings60mmDiamf1977 = imp.meshGridCSTFieldMap(datTestRings60mmDiamf1977)
lossSumOnZRings60mmDiamf1977 = imp.cumSumLoss3DInZ(fieldsPlotRings60mmDiamf1977, lensRings60mmDiamf1977[2])
lossOnZRings60mmDiamf1977 = imp.sumLoss3DInZ(fieldsPlotRings60mmDiamf1977, lensRings60mmDiamf1977[2])
lossSliceZRings60mmDiamf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotRings60mmDiamf1977, "X")

fieldsPlotRings60mmDiamf1977=None
lensRings60mmDiamf1977 = None

directoryRings70mmDiam = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/ferrRing70mmOuterD/"

lossTestf0423 = directoryRings70mmDiam+"f0423.txt"
lossTestf0838 = directoryRings70mmDiam+"f0838.txt"
lossTestf1239 = directoryRings70mmDiam+"f1239.txt"
lossTestf1652 = directoryRings70mmDiam+"f1652.txt"
lossTestf1977 = directoryRings70mmDiam+"f1977.txt"

datTestRings70mmDiamf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x,y,z), fieldsPlotRings70mmDiamf0423, lensRings70mmDiamf0423 = imp.meshGridCSTFieldMap(datTestRings70mmDiamf0423)
lossSumOnZRings70mmDiamf0423 = imp.cumSumLoss3DInZ(fieldsPlotRings70mmDiamf0423, lensRings70mmDiamf0423[2])
lossOnZRings70mmDiamf0423 = imp.sumLoss3DInZ(fieldsPlotRings70mmDiamf0423, lensRings70mmDiamf0423[2])
lossSliceZRings70mmDiamf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotRings70mmDiamf0423, "X")

fieldsPlotRings70mmDiamf0423=None
lensRings70mmDiamf0423 = None

datTestRings70mmDiamf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x,y,z), fieldsPlotRings70mmDiamf0838, lensRings70mmDiamf0838 = imp.meshGridCSTFieldMap(datTestRings70mmDiamf0838)
lossSumOnZRings70mmDiamf0838 = imp.cumSumLoss3DInZ(fieldsPlotRings70mmDiamf0838, lensRings70mmDiamf0838[2])
lossOnZRings70mmDiamf0838 = imp.sumLoss3DInZ(fieldsPlotRings70mmDiamf0838, lensRings70mmDiamf0838[2])
lossSliceZRings70mmDiamf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotRings70mmDiamf0838, "X")

fieldsPlotRings70mmDiamf0838=None
lensRings70mmDiamf0838 = None
(x,y,z)=(None,None,None)

datTestRings70mmDiamf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x,y,z), fieldsPlotRings70mmDiamf1239, lensRings70mmDiamf1239 = imp.meshGridCSTFieldMap(datTestRings70mmDiamf1239)
lossSumOnZRings70mmDiamf1239 = imp.cumSumLoss3DInZ(fieldsPlotRings70mmDiamf1239, lensRings70mmDiamf1239[2])
lossOnZRings70mmDiamf1239 = imp.sumLoss3DInZ(fieldsPlotRings70mmDiamf1239, lensRings70mmDiamf1239[2])
lossSliceZRings70mmDiamf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotRings70mmDiamf1239, "X")

fieldsPlotRings70mmDiamf1239=None
lensRings70mmDiamf1239 = None
(x,y,z)=(None,None,None)

datTestRings70mmDiamf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x,y,z), fieldsPlotRings70mmDiamf1652, lensRings70mmDiamf1652 = imp.meshGridCSTFieldMap(datTestRings70mmDiamf1652)
lossSumOnZRings70mmDiamf1652 = imp.cumSumLoss3DInZ(fieldsPlotRings70mmDiamf1652, lensRings70mmDiamf1652[2])
lossOnZRings70mmDiamf1652 = imp.sumLoss3DInZ(fieldsPlotRings70mmDiamf1652, lensRings70mmDiamf1652[2])
lossSliceZRings70mmDiamf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotRings70mmDiamf1652, "X")

fieldsPlotRings70mmDiamf1652=None
lensSRings70mmDiamf1652 = None
(x,y,z)=(None,None,None)

datTestRings70mmDiamf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x,y,z), fieldsPlotRings70mmDiamf1977, lensRings70mmDiamf1977 = imp.meshGridCSTFieldMap(datTestRings70mmDiamf1977)
lossSumOnZRings70mmDiamf1977 = imp.cumSumLoss3DInZ(fieldsPlotRings70mmDiamf1977, lensRings70mmDiamf1977[2])
lossOnZRings70mmDiamf1977 = imp.sumLoss3DInZ(fieldsPlotRings70mmDiamf1977, lensRings70mmDiamf1977[2])
lossSliceZRings70mmDiamf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotRings70mmDiamf1977, "X")

fieldsPlotRings70mmDiamf1977=None
lensRings70mmDiamf1977 = None


directoryRings90mmDiam = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/ferrRing90mmOuterD/"

lossTestf0423 = directoryRings90mmDiam+"f0423.txt"
lossTestf0838 = directoryRings90mmDiam+"f0838.txt"
lossTestf1239 = directoryRings90mmDiam+"f1239.txt"
lossTestf1652 = directoryRings90mmDiam+"f1652.txt"
lossTestf1977 = directoryRings90mmDiam+"f1977.txt"

datTestRings90mmDiamf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x,y,z), fieldsPlotRings90mmDiamf0423, lensRings90mmDiamf0423 = imp.meshGridCSTFieldMap(datTestRings90mmDiamf0423)
lossSumOnZRings90mmDiamf0423 = imp.cumSumLoss3DInZ(fieldsPlotRings90mmDiamf0423, lensRings90mmDiamf0423[2])
lossOnZRings90mmDiamf0423 = imp.sumLoss3DInZ(fieldsPlotRings90mmDiamf0423, lensRings90mmDiamf0423[2])
lossSliceZRings90mmDiamf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotRings90mmDiamf0423, "X")

fieldsPlotRings90mmDiamf0423=None
lensRings90mmDiamf0423 = None

datTestRings90mmDiamf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x,y,z), fieldsPlotRings90mmDiamf0838, lensRings90mmDiamf0838 = imp.meshGridCSTFieldMap(datTestRings90mmDiamf0838)
lossSumOnZRings90mmDiamf0838 = imp.cumSumLoss3DInZ(fieldsPlotRings90mmDiamf0838, lensRings90mmDiamf0838[2])
lossOnZRings90mmDiamf0838 = imp.sumLoss3DInZ(fieldsPlotRings90mmDiamf0838, lensRings90mmDiamf0838[2])
lossSliceZRings90mmDiamf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotRings90mmDiamf0838, "X")

fieldsPlotRings90mmDiamf0838=None
lensRings90mmDiamf0838 = None
(x,y,z)=(None,None,None)

datTestRings90mmDiamf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x,y,z), fieldsPlotRings90mmDiamf1239, lensRings90mmDiamf1239 = imp.meshGridCSTFieldMap(datTestRings90mmDiamf1239)
lossSumOnZRings90mmDiamf1239 = imp.cumSumLoss3DInZ(fieldsPlotRings90mmDiamf1239, lensRings90mmDiamf1239[2])
lossOnZRings90mmDiamf1239 = imp.sumLoss3DInZ(fieldsPlotRings90mmDiamf1239, lensRings90mmDiamf1239[2])
lossSliceZRings90mmDiamf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotRings90mmDiamf1239, "X")

fieldsPlotRings90mmDiamf1239=None
lensRings90mmDiamf1239 = None
(x,y,z)=(None,None,None)

datTestRings90mmDiamf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x,y,z), fieldsPlotRings90mmDiamf1652, lensRings90mmDiamf1652 = imp.meshGridCSTFieldMap(datTestRings90mmDiamf1652)
lossSumOnZRings90mmDiamf1652 = imp.cumSumLoss3DInZ(fieldsPlotRings90mmDiamf1652, lensRings90mmDiamf1652[2])
lossOnZRings90mmDiamf1652 = imp.sumLoss3DInZ(fieldsPlotRings90mmDiamf1652, lensRings90mmDiamf1652[2])
lossSliceZRings90mmDiamf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotRings90mmDiamf1652, "X")

fieldsPlotRings90mmDiamf1652=None
lensSRings90mmDiamf1652 = None
(x,y,z)=(None,None,None)

datTestRings90mmDiamf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x90mm,y90mm,z90mm), fieldsPlotRings90mmDiamf1977, lensRings90mmDiamf1977 = imp.meshGridCSTFieldMap(datTestRings90mmDiamf1977)
lossSumOnZRings90mmDiamf1977 = imp.cumSumLoss3DInZ(fieldsPlotRings90mmDiamf1977, lensRings90mmDiamf1977[2])
lossOnZRings90mmDiamf1977 = imp.sumLoss3DInZ(fieldsPlotRings90mmDiamf1977, lensRings90mmDiamf1977[2])
lossSliceZRings90mmDiamf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotRings90mmDiamf1977, "X")

fieldsPlotRings90mmDiamf1977=None
lensRings90mmDiamf1977 = None
(x90mm, y90mm) = (None,None)

directoryRings100mmDiam = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/ferrRing100mmOuterD/"

lossTestf0423 = directoryRings100mmDiam+"f0423.txt"
lossTestf0838 = directoryRings100mmDiam+"f0838.txt"
lossTestf1239 = directoryRings100mmDiam+"f1239.txt"
lossTestf1652 = directoryRings100mmDiam+"f1652.txt"
lossTestf1977 = directoryRings100mmDiam+"f1977.txt"

datTestRings100mmDiamf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x,y,z), fieldsPlotRings100mmDiamf0423, lensRings100mmDiamf0423 = imp.meshGridCSTFieldMap(datTestRings100mmDiamf0423)
lossSumOnZRings100mmDiamf0423 = imp.cumSumLoss3DInZ(fieldsPlotRings100mmDiamf0423, lensRings100mmDiamf0423[2])
lossOnZRings100mmDiamf0423 = imp.sumLoss3DInZ(fieldsPlotRings100mmDiamf0423, lensRings100mmDiamf0423[2])
lossSliceZRings100mmDiamf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotRings100mmDiamf0423, "X")

fieldsPlotRings100mmDiamf0423=None
lensRings100mmDiamf0423 = None

datTestRings100mmDiamf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x,y,z), fieldsPlotRings100mmDiamf0838, lensRings100mmDiamf0838 = imp.meshGridCSTFieldMap(datTestRings100mmDiamf0838)
lossSumOnZRings100mmDiamf0838 = imp.cumSumLoss3DInZ(fieldsPlotRings100mmDiamf0838, lensRings100mmDiamf0838[2])
lossOnZRings100mmDiamf0838 = imp.sumLoss3DInZ(fieldsPlotRings100mmDiamf0838, lensRings100mmDiamf0838[2])
lossSliceZRings100mmDiamf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotRings100mmDiamf0838, "X")

fieldsPlotRings100mmDiamf0838=None
lensRings100mmDiamf0838 = None
(x,y,z)=(None,None,None)

datTestRings100mmDiamf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x,y,z), fieldsPlotRings100mmDiamf1239, lensRings100mmDiamf1239 = imp.meshGridCSTFieldMap(datTestRings100mmDiamf1239)
lossSumOnZRings100mmDiamf1239 = imp.cumSumLoss3DInZ(fieldsPlotRings100mmDiamf1239, lensRings100mmDiamf1239[2])
lossOnZRings100mmDiamf1239 = imp.sumLoss3DInZ(fieldsPlotRings100mmDiamf1239, lensRings100mmDiamf1239[2])
lossSliceZRings100mmDiamf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotRings100mmDiamf1239, "X")

fieldsPlotRings100mmDiamf1239=None
lensRings100mmDiamf1239 = None
(x,y,z)=(None,None,None)

datTestRings100mmDiamf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x,y,z), fieldsPlotRings100mmDiamf1652, lensRings100mmDiamf1652 = imp.meshGridCSTFieldMap(datTestRings100mmDiamf1652)
lossSumOnZRings100mmDiamf1652 = imp.cumSumLoss3DInZ(fieldsPlotRings100mmDiamf1652, lensRings100mmDiamf1652[2])
lossOnZRings100mmDiamf1652 = imp.sumLoss3DInZ(fieldsPlotRings100mmDiamf1652, lensRings100mmDiamf1652[2])
lossSliceZRings100mmDiamf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotRings100mmDiamf1652, "X")

fieldsPlotRings100mmDiamf1652=None
lensSRings100mmDiamf1652 = None
(x,y,z)=(None,None,None)

datTestRings100mmDiamf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x,y,z), fieldsPlotRings100mmDiamf1977, lensRings100mmDiamf1977 = imp.meshGridCSTFieldMap(datTestRings100mmDiamf1977)
lossSumOnZRings100mmDiamf1977 = imp.cumSumLoss3DInZ(fieldsPlotRings100mmDiamf1977, lensRings100mmDiamf1977[2])
lossOnZRings100mmDiamf1977 = imp.sumLoss3DInZ(fieldsPlotRings100mmDiamf1977, lensRings100mmDiamf1977[2])
lossSliceZRings100mmDiamf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotRings100mmDiamf1977, "X")

fieldsPlotRings100mmDiamf1977=None
lensRings100mmDiamf1977 = None

directoryFerriteCollar = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/ferriteCollarCapEnd/"

lossTestf0423 = directoryFerriteCollar+"f0423.txt"
lossTestf0838 = directoryFerriteCollar+"f0838.txt"
lossTestf1239 = directoryFerriteCollar+"f1239.txt"
lossTestf1652 = directoryFerriteCollar+"f1652.txt"
lossTestf1977 = directoryFerriteCollar+"f1977.txt"

datTestFerriteCollarf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x,y,z), fieldsPlotFerriteCollarf0423, lensFerriteCollarf0423 = imp.meshGridCSTFieldMap(datTestFerriteCollarf0423)
lossSumOnZFerriteCollarf0423 = imp.cumSumLoss3DInZ(fieldsPlotFerriteCollarf0423, lensFerriteCollarf0423[2])
lossOnZFerriteCollarf0423 = imp.sumLoss3DInZ(fieldsPlotFerriteCollarf0423, lensFerriteCollarf0423[2])
lossSliceZFerriteCollarf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotFerriteCollarf0423, "X")

fieldsPlotFerriteCollarf0423=None
lensFerriteCollarf0423 = None

datTestFerriteCollarf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x,y,z), fieldsPlotFerriteCollarf0838, lensFerriteCollarf0838 = imp.meshGridCSTFieldMap(datTestFerriteCollarf0838)
lossSumOnZFerriteCollarf0838 = imp.cumSumLoss3DInZ(fieldsPlotFerriteCollarf0838, lensFerriteCollarf0838[2])
lossOnZFerriteCollarf0838 = imp.sumLoss3DInZ(fieldsPlotFerriteCollarf0838, lensFerriteCollarf0838[2])
lossSliceZFerriteCollarf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotFerriteCollarf0838, "X")

fieldsPlotFerriteCollarf0838=None
lensFerriteCollarf0838 = None
(x,y,z)=(None,None,None)

datTestFerriteCollarf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x,y,z), fieldsPlotFerriteCollarf1239, lensFerriteCollarf1239 = imp.meshGridCSTFieldMap(datTestFerriteCollarf1239)
lossSumOnZFerriteCollarf1239 = imp.cumSumLoss3DInZ(fieldsPlotFerriteCollarf1239, lensFerriteCollarf1239[2])
lossOnZFerriteCollarf1239 = imp.sumLoss3DInZ(fieldsPlotFerriteCollarf1239, lensFerriteCollarf1239[2])
lossSliceZFerriteCollarf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotFerriteCollarf1239, "X")

fieldsPlotFerriteCollarf1239=None
lensFerriteCollarf1239 = None
(x,y,z)=(None,None,None)

datTestFerriteCollarf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x,y,z), fieldsPlotFerriteCollarf1652, lensFerriteCollarf1652 = imp.meshGridCSTFieldMap(datTestFerriteCollarf1652)
lossSumOnZFerriteCollarf1652 = imp.cumSumLoss3DInZ(fieldsPlotFerriteCollarf1652, lensFerriteCollarf1652[2])
lossOnZFerriteCollarf1652 = imp.sumLoss3DInZ(fieldsPlotFerriteCollarf1652, lensFerriteCollarf1652[2])
lossSliceZFerriteCollarf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotFerriteCollarf1652, "X")

fieldsPlotFerriteCollarf1652=None
lensFerriteCollarf1652 = None
(x,y,z)=(None,None,None)

datTestFerriteCollarf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x,y,z), fieldsPlotFerriteCollarf1977, lensFerriteCollarf1977 = imp.meshGridCSTFieldMap(datTestFerriteCollarf1977)
lossSumOnZFerriteCollarf1977 = imp.cumSumLoss3DInZ(fieldsPlotFerriteCollarf1977, lensFerriteCollarf1977[2])
lossOnZFerriteCollarf1977 = imp.sumLoss3DInZ(fieldsPlotFerriteCollarf1977, lensFerriteCollarf1977[2])
lossSliceZFerriteCollarf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotFerriteCollarf1977, "X")

fieldsPlotFerriteCollarf1977=None
lensFerriteCollarf1977 = None

directoryFerriteCollar20mm = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/ferriteCollar20mm/"

lossTestf0423 = directoryFerriteCollar20mm+"f0423.txt"
lossTestf0838 = directoryFerriteCollar20mm+"f0838.txt"
lossTestf1239 = directoryFerriteCollar20mm+"f1239.txt"
lossTestf1652 = directoryFerriteCollar20mm+"f1652.txt"
lossTestf1977 = directoryFerriteCollar20mm+"f1977.txt"

datTestFerriteCollar20mmf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x90mm,y90mm,z90mm), fieldsPlotFerriteCollar20mmf0423, lensFerriteCollar20mmf0423 = imp.meshGridCSTFieldMap(datTestFerriteCollar20mmf0423)
lossSumOnZFerriteCollar20mmf0423 = imp.cumSumLoss3DInZ(fieldsPlotFerriteCollar20mmf0423, lensFerriteCollar20mmf0423[2])
lossOnZFerriteCollar20mmf0423 = imp.sumLoss3DInZ(fieldsPlotFerriteCollar20mmf0423, lensFerriteCollar20mmf0423[2])
lossSliceZFerriteCollar20mmf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotFerriteCollar20mmf0423, "X")

fieldsPlotFerriteCollar20mmf0423=None
lensFerriteCollar20mmf0423 = None

datTestFerriteCollar20mmf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x90mm,y90mm,z90mm), fieldsPlotFerriteCollar20mmf0838, lensFerriteCollar20mmf0838 = imp.meshGridCSTFieldMap(datTestFerriteCollar20mmf0838)
lossSumOnZFerriteCollar20mmf0838 = imp.cumSumLoss3DInZ(fieldsPlotFerriteCollar20mmf0838, lensFerriteCollar20mmf0838[2])
lossOnZFerriteCollar20mmf0838 = imp.sumLoss3DInZ(fieldsPlotFerriteCollar20mmf0838, lensFerriteCollar20mmf0838[2])
lossSliceZFerriteCollar20mmf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotFerriteCollar20mmf0838, "X")

fieldsPlotFerriteCollar20mmf0838=None
lensFerriteCollar20mmf0838 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestFerriteCollar20mmf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x90mm,y90mm,z90mm), fieldsPlotFerriteCollar20mmf1239, lensFerriteCollar20mmf1239 = imp.meshGridCSTFieldMap(datTestFerriteCollar20mmf1239)
lossSumOnZFerriteCollar20mmf1239 = imp.cumSumLoss3DInZ(fieldsPlotFerriteCollar20mmf1239, lensFerriteCollar20mmf1239[2])
lossOnZFerriteCollar20mmf1239 = imp.sumLoss3DInZ(fieldsPlotFerriteCollar20mmf1239, lensFerriteCollar20mmf1239[2])
lossSliceZFerriteCollar20mmf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotFerriteCollar20mmf1239, "X")

fieldsPlotFerriteCollar20mmf1239=None
lensFerriteCollar20mmf1239 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestFerriteCollar20mmf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x90mm,y90mm,z90mm), fieldsPlotFerriteCollar20mmf1652, lensFerriteCollar20mmf1652 = imp.meshGridCSTFieldMap(datTestFerriteCollar20mmf1652)
lossSumOnZFerriteCollar20mmf1652 = imp.cumSumLoss3DInZ(fieldsPlotFerriteCollar20mmf1652, lensFerriteCollar20mmf1652[2])
lossOnZFerriteCollar20mmf1652 = imp.sumLoss3DInZ(fieldsPlotFerriteCollar20mmf1652, lensFerriteCollar20mmf1652[2])
lossSliceZFerriteCollar20mmf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotFerriteCollar20mmf1652, "X")

fieldsPlotFerriteCollar20mmf1652=None
lensFerriteCollar20mmf1652 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestFerriteCollar20mmf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x90mm,y90mm,z90mm), fieldsPlotFerriteCollar20mmf1977, lensFerriteCollar20mmf1977 = imp.meshGridCSTFieldMap(datTestFerriteCollar20mmf1977)
lossSumOnZFerriteCollar20mmf1977 = imp.cumSumLoss3DInZ(fieldsPlotFerriteCollar20mmf1977, lensFerriteCollar20mmf1977[2])
lossOnZFerriteCollar20mmf1977 = imp.sumLoss3DInZ(fieldsPlotFerriteCollar20mmf1977, lensFerriteCollar20mmf1977[2])
lossSliceZFerriteCollar20mmf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotFerriteCollar20mmf1977, "X")

fieldsPlotFerriteCollar20mmf1977=None
lensFerriteCollar20mmf1977 = None


directoryAirGap = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/airGap1mmBottom/"

lossTestf0423 = directoryAirGap+"f0423.txt"
lossTestf0838 = directoryAirGap+"f0838.txt"
lossTestf1239 = directoryAirGap+"f1239.txt"
lossTestf1652 = directoryAirGap+"f1652.txt"
lossTestf1977 = directoryAirGap+"f1977.txt"

datTestAirGapf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x90mm,y90mm,z90mm), fieldsPlotAirGapf0423, lensAirGapf0423 = imp.meshGridCSTFieldMap(datTestAirGapf0423)
lossSumOnZAirGapf0423 = imp.cumSumLoss3DInZ(fieldsPlotAirGapf0423, lensAirGapf0423[2])
lossOnZAirGapf0423 = imp.sumLoss3DInZ(fieldsPlotAirGapf0423, lensAirGapf0423[2])
lossSliceZAirGapf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapf0423, "X")

fieldsPlotAirGapf0423=None
lensAirGapf0423 = None

datTestAirGapf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x90mm,y90mm,z90mm), fieldsPlotAirGapf0838, lensAirGapf0838 = imp.meshGridCSTFieldMap(datTestAirGapf0838)
lossSumOnZAirGapf0838 = imp.cumSumLoss3DInZ(fieldsPlotAirGapf0838, lensAirGapf0838[2])
lossOnZAirGapf0838 = imp.sumLoss3DInZ(fieldsPlotAirGapf0838, lensAirGapf0838[2])
lossSliceZAirGapf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapf0838, "X")

fieldsPlotAirGapf0838=None
lensAirGapf0838 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x90mm,y90mm,z90mm), fieldsPlotAirGapf1239, lensAirGapf1239 = imp.meshGridCSTFieldMap(datTestAirGapf1239)
lossSumOnZAirGapf1239 = imp.cumSumLoss3DInZ(fieldsPlotAirGapf1239, lensAirGapf1239[2])
lossOnZAirGapf1239 = imp.sumLoss3DInZ(fieldsPlotAirGapf1239, lensAirGapf1239[2])
lossSliceZAirGapf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapf1239, "X")

fieldsPlotAirGapf1239=None
lensAirGapf1239 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x90mm,y90mm,z90mm), fieldsPlotAirGapf1652, lensAirGapf1652 = imp.meshGridCSTFieldMap(datTestAirGapf1652)
lossSumOnZAirGapf1652 = imp.cumSumLoss3DInZ(fieldsPlotAirGapf1652, lensAirGapf1652[2])
lossOnZAirGapf1652 = imp.sumLoss3DInZ(fieldsPlotAirGapf1652, lensAirGapf1652[2])
lossSliceZAirGapf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapf1652, "X")

fieldsPlotAirGapf1652=None
lensAirGapf1652 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x90mm,y90mm,z90mm), fieldsPlotAirGapf1977, lensAirGapf1977 = imp.meshGridCSTFieldMap(datTestAirGapf1977)
lossSumOnZAirGapf1977 = imp.cumSumLoss3DInZ(fieldsPlotAirGapf1977, lensAirGapf1977[2])
lossOnZAirGapf1977 = imp.sumLoss3DInZ(fieldsPlotAirGapf1977, lensAirGapf1977[2])
lossSliceZAirGapf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapf1977, "X")

fieldsPlotAirGapf1977=None
lensAirGapf1977 = None


directoryAirGapSlot5mm = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/airGap1mmBottomSlot5mm/"

lossTestf0423 = directoryAirGapSlot5mm+"f0423.txt"
lossTestf0838 = directoryAirGapSlot5mm+"f0838.txt"
lossTestf1239 = directoryAirGapSlot5mm+"f1239.txt"
lossTestf1652 = directoryAirGapSlot5mm+"f1652.txt"
lossTestf1977 = directoryAirGapSlot5mm+"f1977.txt"

datTestAirGapSlot5mmf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlot5mmf0423, lensAirGapSlot5mmf0423 = imp.meshGridCSTFieldMap(datTestAirGapSlot5mmf0423)
lossSumOnZAirGapSlot5mmf0423 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlot5mmf0423, lensAirGapSlot5mmf0423[2])
lossOnZAirGapSlot5mmf0423 = imp.sumLoss3DInZ(fieldsPlotAirGapSlot5mmf0423, lensAirGapSlot5mmf0423[2])
lossSliceZAirGapSlot5mmf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlot5mmf0423, "X")

fieldsPlotAirGapSlot5mmf0423=None
lensAirGapSlot5mmf0423 = None

datTestAirGapSlot5mmf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlot5mmf0838, lensAirGapSlot5mmf0838 = imp.meshGridCSTFieldMap(datTestAirGapSlot5mmf0838)
lossSumOnZAirGapSlot5mmf0838 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlot5mmf0838, lensAirGapSlot5mmf0838[2])
lossOnZAirGapSlot5mmf0838 = imp.sumLoss3DInZ(fieldsPlotAirGapSlot5mmf0838, lensAirGapSlot5mmf0838[2])
lossSliceZAirGapSlot5mmf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlot5mmf0838, "X")

fieldsPlotAirGapSlot5mmf0838=None
lensAirGapSlot5mmf0838 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlot5mmf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlot5mmf1239, lensAirGapSlot5mmf1239 = imp.meshGridCSTFieldMap(datTestAirGapSlot5mmf1239)
lossSumOnZAirGapSlot5mmf1239 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlot5mmf1239, lensAirGapSlot5mmf1239[2])
lossOnZAirGapSlot5mmf1239 = imp.sumLoss3DInZ(fieldsPlotAirGapSlot5mmf1239, lensAirGapSlot5mmf1239[2])
lossSliceZAirGapSlot5mmf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlot5mmf1239, "X")

fieldsPlotAirGapSlot5mmf1239=None
lensAirGapSlot5mmf1239 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlot5mmf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlot5mmf1652, lensAirGapSlot5mmf1652 = imp.meshGridCSTFieldMap(datTestAirGapSlot5mmf1652)
lossSumOnZAirGapSlot5mmf1652 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlot5mmf1652, lensAirGapSlot5mmf1652[2])
lossOnZAirGapSlot5mmf1652 = imp.sumLoss3DInZ(fieldsPlotAirGapSlot5mmf1652, lensAirGapSlot5mmf1652[2])
lossSliceZAirGapSlot5mmf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlot5mmf1652, "X")

fieldsPlotAirGapSlot5mmf1652=None
lensAirGapSlot5mmf1652 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlot5mmf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlot5mmf1977, lensAirGapSlot5mmf1977 = imp.meshGridCSTFieldMap(datTestAirGapSlot5mmf1977)
lossSumOnZAirGapSlot5mmf1977 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlot5mmf1977, lensAirGapSlot5mmf1977[2])
lossOnZAirGapSlot5mmf1977 = imp.sumLoss3DInZ(fieldsPlotAirGapSlot5mmf1977, lensAirGapSlot5mmf1977[2])
lossSliceZAirGapSlot5mmf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlot5mmf1977, "X")

fieldsPlotAirGapSlot5mmf1977=None
lensAirGapSlot5mmf1977 = None


##directoryAirGapSlot5mmLongWake = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/airGap1mmBottomSlot5mmLongWake/"
directoryAirGapSlot5mmLongWake = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/70mmOverlapWith1mmAirGap/"

lossTestf0423 = directoryAirGapSlot5mmLongWake+"f04385.txt"
lossTestf0838 = directoryAirGapSlot5mmLongWake+"f0860.txt"
lossTestf1239 = directoryAirGapSlot5mmLongWake+"f1264.txt"
lossTestf1652 = directoryAirGapSlot5mmLongWake+"f1661.txt"
lossTestf1977 = directoryAirGapSlot5mmLongWake+"f2029.txt"

datTestAirGapSlot5mmLongWakef0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlot5mmLongWakef0423, lensAirGapSlot5mmLongWakef0423 = imp.meshGridCSTFieldMap(datTestAirGapSlot5mmLongWakef0423)
lossSumOnZAirGapSlot5mmLongWakef0423 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlot5mmLongWakef0423, lensAirGapSlot5mmLongWakef0423[2])
lossOnZAirGapSlot5mmLongWakef0423 = imp.sumLoss3DInZ(fieldsPlotAirGapSlot5mmLongWakef0423, lensAirGapSlot5mmLongWakef0423[2])
lossSliceZAirGapSlot5mmLongWakef0423 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlot5mmLongWakef0423, "X")

fieldsPlotAirGapSlot5mmLongWakef0423=None
lensAirGapSlot5mmLongWakef0423 = None

datTestAirGapSlot5mmLongWakef0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlot5mmLongWakef0838, lensAirGapSlot5mmLongWakef0838 = imp.meshGridCSTFieldMap(datTestAirGapSlot5mmLongWakef0838)
lossSumOnZAirGapSlot5mmLongWakef0838 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlot5mmLongWakef0838, lensAirGapSlot5mmLongWakef0838[2])
lossOnZAirGapSlot5mmLongWakef0838 = imp.sumLoss3DInZ(fieldsPlotAirGapSlot5mmLongWakef0838, lensAirGapSlot5mmLongWakef0838[2])
lossSliceZAirGapSlot5mmLongWakef0838 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlot5mmLongWakef0838, "X")

fieldsPlotAirGapSlot5mmLongWakef0838=None
lensAirGapSlot5mmLongWakef0838 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlot5mmLongWakef1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlot5mmLongWakef1239, lensAirGapSlot5mmLongWakef1239 = imp.meshGridCSTFieldMap(datTestAirGapSlot5mmLongWakef1239)
lossSumOnZAirGapSlot5mmLongWakef1239 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlot5mmLongWakef1239, lensAirGapSlot5mmLongWakef1239[2])
lossOnZAirGapSlot5mmLongWakef1239 = imp.sumLoss3DInZ(fieldsPlotAirGapSlot5mmLongWakef1239, lensAirGapSlot5mmLongWakef1239[2])
lossSliceZAirGapSlot5mmLongWakef1239 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlot5mmLongWakef1239, "X")

fieldsPlotAirGapSlot5mmLongWakef1239=None
lensAirGapSlot5mmLongWakef1239 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlot5mmLongWakef1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlot5mmLongWakef1652, lensAirGapSlot5mmLongWakef1652 = imp.meshGridCSTFieldMap(datTestAirGapSlot5mmLongWakef1652)
lossSumOnZAirGapSlot5mmLongWakef1652 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlot5mmLongWakef1652, lensAirGapSlot5mmLongWakef1652[2])
lossOnZAirGapSlot5mmLongWakef1652 = imp.sumLoss3DInZ(fieldsPlotAirGapSlot5mmLongWakef1652, lensAirGapSlot5mmLongWakef1652[2])
lossSliceZAirGapSlot5mmLongWakef1652 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlot5mmLongWakef1652, "X")

fieldsPlotAirGapSlot5mmLongWakef1652=None
lensAirGapSlot5mmLongWakef1652 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlot5mmLongWakef1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlot5mmLongWakef1977, lensAirGapSlot5mmLongWakef1977 = imp.meshGridCSTFieldMap(datTestAirGapSlot5mmLongWakef1977)
lossSumOnZAirGapSlot5mmLongWakef1977 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlot5mmLongWakef1977, lensAirGapSlot5mmLongWakef1977[2])
lossOnZAirGapSlot5mmLongWakef1977 = imp.sumLoss3DInZ(fieldsPlotAirGapSlot5mmLongWakef1977, lensAirGapSlot5mmLongWakef1977[2])
lossSliceZAirGapSlot5mmLongWakef1977 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlot5mmLongWakef1977, "X")

fieldsPlotAirGapSlot5mmLongWakef1977=None
lensAirGapSlot5mmLongWakef1977 = None

directoryAirGapSlotTop5mm = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/airGap1mmBottomSlotOnTop5mm/"

lossTestf0423 = directoryAirGapSlotTop5mm+"f0423.txt"
lossTestf0838 = directoryAirGapSlotTop5mm+"f0838.txt"
lossTestf1239 = directoryAirGapSlotTop5mm+"f1239.txt"
lossTestf1652 = directoryAirGapSlotTop5mm+"f1652.txt"
lossTestf1977 = directoryAirGapSlotTop5mm+"f1977.txt"

datTestAirGapSlotTop5mmf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop5mmf0423, lensAirGapSlotTop5mmf0423 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop5mmf0423)
lossSumOnZAirGapSlotTop5mmf0423 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop5mmf0423, lensAirGapSlotTop5mmf0423[2])
lossOnZAirGapSlotTop5mmf0423 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop5mmf0423, lensAirGapSlotTop5mmf0423[2])
lossSliceZAirGapSlotTop5mmf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop5mmf0423, "X")

fieldsPlotAirGapSlotTop5mmf0423=None
lensAirGapSlotTop5mmf0423 = None

datTestAirGapSlotTop5mmf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop5mmf0838, lensAirGapSlotTop5mmf0838 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop5mmf0838)
lossSumOnZAirGapSlotTop5mmf0838 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop5mmf0838, lensAirGapSlotTop5mmf0838[2])
lossOnZAirGapSlotTop5mmf0838 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop5mmf0838, lensAirGapSlotTop5mmf0838[2])
lossSliceZAirGapSlotTop5mmf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop5mmf0838, "X")

fieldsPlotAirGapSlotTop5mmf0838=None
lensAirGapSlotTop5mmf0838 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlotTop5mmf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop5mmf1239, lensAirGapSlotTop5mmf1239 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop5mmf1239)
lossSumOnZAirGapSlotTop5mmf1239 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop5mmf1239, lensAirGapSlotTop5mmf1239[2])
lossOnZAirGapSlotTop5mmf1239 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop5mmf1239, lensAirGapSlotTop5mmf1239[2])
lossSliceZAirGapSlotTop5mmf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop5mmf1239, "X")

fieldsPlotAirGapSlotTop5mmf1239=None
lensAirGapSlotTop5mmf1239 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlotTop5mmf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop5mmf1652, lensAirGapSlotTop5mmf1652 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop5mmf1652)
lossSumOnZAirGapSlotTop5mmf1652 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop5mmf1652, lensAirGapSlotTop5mmf1652[2])
lossOnZAirGapSlotTop5mmf1652 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop5mmf1652, lensAirGapSlotTop5mmf1652[2])
lossSliceZAirGapSlotTop5mmf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop5mmf1652, "X")

fieldsPlotAirGapSlotTop5mmf1652=None
lensAirGapSlotTop5mmf1652 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlotTop5mmf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop5mmf1977, lensAirGapSlotTop5mmf1977 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop5mmf1977)
lossSumOnZAirGapSlotTop5mmf1977 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop5mmf1977, lensAirGapSlotTop5mmf1977[2])
lossOnZAirGapSlotTop5mmf1977 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop5mmf1977, lensAirGapSlotTop5mmf1977[2])
lossSliceZAirGapSlotTop5mmf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop5mmf1977, "X")

fieldsPlotAirGapSlotTop5mmf1977=None
lensAirGapSlotTop5mmf1977 = None


directoryAirGapSlotTop10mm = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz\powLossDens/airGap1mmBottomSlotOnTop10mm/"

lossTestf0423 = directoryAirGapSlotTop10mm+"f04385.txt"
lossTestf0838 = directoryAirGapSlotTop10mm+"f0860.txt"
lossTestf1239 = directoryAirGapSlotTop10mm+"f1264.txt"
lossTestf1652 = directoryAirGapSlotTop10mm+"f1661.txt"
lossTestf1977 = directoryAirGapSlotTop10mm+"f2029.txt"

datTestAirGapSlotTop10mmf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop10mmf0423, lensAirGapSlotTop10mmf0423 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop10mmf0423)
lossSumOnZAirGapSlotTop10mmf0423 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop10mmf0423, lensAirGapSlotTop10mmf0423[2])
lossOnZAirGapSlotTop10mmf0423 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop10mmf0423, lensAirGapSlotTop10mmf0423[2])
lossSliceZAirGapSlotTop10mmf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop10mmf0423, "X")

fieldsPlotAirGapSlotTop10mmf0423=None
lensAirGapSlotTop10mmf0423 = None

datTestAirGapSlotTop10mmf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop10mmf0838, lensAirGapSlotTop10mmf0838 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop10mmf0838)
lossSumOnZAirGapSlotTop10mmf0838 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop10mmf0838, lensAirGapSlotTop10mmf0838[2])
lossOnZAirGapSlotTop10mmf0838 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop10mmf0838, lensAirGapSlotTop10mmf0838[2])
lossSliceZAirGapSlotTop10mmf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop10mmf0838, "X")

fieldsPlotAirGapSlotTop10mmf0838=None
lensAirGapSlotTop10mmf0838 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlotTop10mmf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop10mmf1239, lensAirGapSlotTop10mmf1239 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop10mmf1239)
lossSumOnZAirGapSlotTop10mmf1239 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop10mmf1239, lensAirGapSlotTop10mmf1239[2])
lossOnZAirGapSlotTop10mmf1239 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop10mmf1239, lensAirGapSlotTop10mmf1239[2])
lossSliceZAirGapSlotTop10mmf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop10mmf1239, "X")

fieldsPlotAirGapSlotTop10mmf1239=None
lensAirGapSlotTop10mmf1239 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlotTop10mmf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop10mmf1652, lensAirGapSlotTop10mmf1652 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop10mmf1652)
lossSumOnZAirGapSlotTop10mmf1652 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop10mmf1652, lensAirGapSlotTop10mmf1652[2])
lossOnZAirGapSlotTop10mmf1652 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop10mmf1652, lensAirGapSlotTop10mmf1652[2])
lossSliceZAirGapSlotTop10mmf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop10mmf1652, "X")

fieldsPlotAirGapSlotTop10mmf1652=None
lensAirGapSlotTop10mmf1652 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlotTop10mmf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop10mmf1977, lensAirGapSlotTop10mmf1977 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop10mmf1977)
lossSumOnZAirGapSlotTop10mmf1977 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop10mmf1977, lensAirGapSlotTop10mmf1977[2])
lossOnZAirGapSlotTop10mmf1977 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop10mmf1977, lensAirGapSlotTop10mmf1977[2])
lossSliceZAirGapSlotTop10mmf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop10mmf1977, "X")

fieldsPlotAirGapSlotTop10mmf1977=None
lensAirGapSlotTop10mmf1977 = None

directoryAirGapSlotTop20mm = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz\powLossDens/airGap1mmBottomSlot20mmTop/"

lossTestf0423 = directoryAirGapSlotTop20mm+"f04385.txt"
lossTestf0838 = directoryAirGapSlotTop20mm+"f0860.txt"
lossTestf1239 = directoryAirGapSlotTop20mm+"f1264.txt"
lossTestf1652 = directoryAirGapSlotTop20mm+"f1661.txt"
lossTestf1977 = directoryAirGapSlotTop20mm+"f2029.txt"

datTestAirGapSlotTop20mmf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop20mmf0423, lensAirGapSlotTop20mmf0423 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop20mmf0423)
lossSumOnZAirGapSlotTop20mmf0423 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop20mmf0423, lensAirGapSlotTop20mmf0423[2])
lossOnZAirGapSlotTop20mmf0423 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop20mmf0423, lensAirGapSlotTop20mmf0423[2])
lossSliceZAirGapSlotTop20mmf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop20mmf0423, "X")

fieldsPlotAirGapSlotTop20mmf0423=None
lensAirGapSlotTop20mmf0423 = None

datTestAirGapSlotTop20mmf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop20mmf0838, lensAirGapSlotTop20mmf0838 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop20mmf0838)
lossSumOnZAirGapSlotTop20mmf0838 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop20mmf0838, lensAirGapSlotTop20mmf0838[2])
lossOnZAirGapSlotTop20mmf0838 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop20mmf0838, lensAirGapSlotTop20mmf0838[2])
lossSliceZAirGapSlotTop20mmf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop20mmf0838, "X")

fieldsPlotAirGapSlotTop20mmf0838=None
lensAirGapSlotTop20mmf0838 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlotTop20mmf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop20mmf1239, lensAirGapSlotTop20mmf1239 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop20mmf1239)
lossSumOnZAirGapSlotTop20mmf1239 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop20mmf1239, lensAirGapSlotTop20mmf1239[2])
lossOnZAirGapSlotTop20mmf1239 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop20mmf1239, lensAirGapSlotTop20mmf1239[2])
lossSliceZAirGapSlotTop20mmf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop20mmf1239, "X")

fieldsPlotAirGapSlotTop20mmf1239=None
lensAirGapSlotTop20mmf1239 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlotTop20mmf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop20mmf1652, lensAirGapSlotTop20mmf1652 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop20mmf1652)
lossSumOnZAirGapSlotTop20mmf1652 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop20mmf1652, lensAirGapSlotTop20mmf1652[2])
lossOnZAirGapSlotTop20mmf1652 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop20mmf1652, lensAirGapSlotTop20mmf1652[2])
lossSliceZAirGapSlotTop20mmf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop20mmf1652, "X")

fieldsPlotAirGapSlotTop20mmf1652=None
lensAirGapSlotTop20mmf1652 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlotTop20mmf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop20mmf1977, lensAirGapSlotTop20mmf1977 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop20mmf1977)
lossSumOnZAirGapSlotTop20mmf1977 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop20mmf1977, lensAirGapSlotTop20mmf1977[2])
lossOnZAirGapSlotTop20mmf1977 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop20mmf1977, lensAirGapSlotTop20mmf1977[2])
lossSliceZAirGapSlotTop20mmf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop20mmf1977, "X")

fieldsPlotAirGapSlotTop20mmf1977=None
lensAirGapSlotTop20mmf1977 = None


directoryAirGapSlotTop40mm = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/airGap1mmBottomSlot40mmTop/"

lossTestf0423 = directoryAirGapSlotTop40mm+"f0423.txt"
lossTestf0838 = directoryAirGapSlotTop40mm+"f0838.txt"
lossTestf1239 = directoryAirGapSlotTop40mm+"f1239.txt"
lossTestf1652 = directoryAirGapSlotTop40mm+"f1652.txt"
lossTestf1977 = directoryAirGapSlotTop40mm+"f1997.txt"

datTestAirGapSlotTop40mmf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop40mmf0423, lensAirGapSlotTop40mmf0423 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop40mmf0423)
lossSumOnZAirGapSlotTop40mmf0423 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop40mmf0423, lensAirGapSlotTop40mmf0423[2])
lossOnZAirGapSlotTop40mmf0423 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop40mmf0423, lensAirGapSlotTop40mmf0423[2])
lossSliceZAirGapSlotTop40mmf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop40mmf0423, "X")

fieldsPlotAirGapSlotTop40mmf0423=None
lensAirGapSlotTop40mmf0423 = None

datTestAirGapSlotTop40mmf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop40mmf0838, lensAirGapSlotTop40mmf0838 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop40mmf0838)
lossSumOnZAirGapSlotTop40mmf0838 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop40mmf0838, lensAirGapSlotTop40mmf0838[2])
lossOnZAirGapSlotTop40mmf0838 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop40mmf0838, lensAirGapSlotTop40mmf0838[2])
lossSliceZAirGapSlotTop40mmf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop40mmf0838, "X")

fieldsPlotAirGapSlotTop40mmf0838=None
lensAirGapSlotTop40mmf0838 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlotTop40mmf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop40mmf1239, lensAirGapSlotTop40mmf1239 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop40mmf1239)
lossSumOnZAirGapSlotTop40mmf1239 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop40mmf1239, lensAirGapSlotTop40mmf1239[2])
lossOnZAirGapSlotTop40mmf1239 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop40mmf1239, lensAirGapSlotTop40mmf1239[2])
lossSliceZAirGapSlotTop40mmf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop40mmf1239, "X")

fieldsPlotAirGapSlotTop40mmf1239=None
lensAirGapSlotTop40mmf1239 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlotTop40mmf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop40mmf1652, lensAirGapSlotTop40mmf1652 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop40mmf1652)
lossSumOnZAirGapSlotTop40mmf1652 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop40mmf1652, lensAirGapSlotTop40mmf1652[2])
lossOnZAirGapSlotTop40mmf1652 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop40mmf1652, lensAirGapSlotTop40mmf1652[2])
lossSliceZAirGapSlotTop40mmf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop40mmf1652, "X")

fieldsPlotAirGapSlotTop40mmf1652=None
lensAirGapSlotTop40mmf1652 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlotTop40mmf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop40mmf1977, lensAirGapSlotTop40mmf1977 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop40mmf1977)
lossSumOnZAirGapSlotTop40mmf1977 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop40mmf1977, lensAirGapSlotTop40mmf1977[2])
lossOnZAirGapSlotTop40mmf1977 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop40mmf1977, lensAirGapSlotTop40mmf1977[2])
lossSliceZAirGapSlotTop40mmf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop40mmf1977, "X")

fieldsPlotAirGapSlotTop40mmf1977=None
lensAirGapSlotTop40mmf1977 = None

directoryAirGapSlotTop5mmCollar = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz\powLossDens/airGap1mmBottomSlot5mmTopFerriteCollar/"

lossTestf0423 = directoryAirGapSlotTop5mmCollar+"f04385.txt"
lossTestf0838 = directoryAirGapSlotTop5mmCollar+"f0860.txt"
lossTestf1239 = directoryAirGapSlotTop5mmCollar+"f1264.txt"
lossTestf1652 = directoryAirGapSlotTop5mmCollar+"f1661.txt"
lossTestf1977 = directoryAirGapSlotTop5mmCollar+"f2029.txt"

datTestAirGapSlotTop5mmCollarf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop5mmCollarf0423, lensAirGapSlotTop5mmCollarf0423 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop5mmCollarf0423)
lossSumOnZAirGapSlotTop5mmCollarf0423 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop5mmCollarf0423, lensAirGapSlotTop5mmCollarf0423[2])
lossOnZAirGapSlotTop5mmCollarf0423 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop5mmCollarf0423, lensAirGapSlotTop5mmCollarf0423[2])
lossSliceZAirGapSlotTop5mmCollarf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop5mmCollarf0423, "X")

fieldsPlotAirGapSlotTop5mmCollarf0423=None
lensAirGapSlotTop5mmCollarf0423 = None

datTestAirGapSlotTop5mmCollarf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop5mmCollarf0838, lensAirGapSlotTop5mmCollarf0838 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop5mmCollarf0838)
lossSumOnZAirGapSlotTop5mmCollarf0838 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop5mmCollarf0838, lensAirGapSlotTop5mmCollarf0838[2])
lossOnZAirGapSlotTop5mmCollarf0838 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop5mmCollarf0838, lensAirGapSlotTop5mmCollarf0838[2])
lossSliceZAirGapSlotTop5mmCollarf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop5mmCollarf0838, "X")

fieldsPlotAirGapSlotTop5mmCollarf0838=None
lensAirGapSlotTop5mmCollarf0838 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlotTop5mmCollarf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop5mmCollarf1239, lensAirGapSlotTop5mmCollarf1239 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop5mmCollarf1239)
lossSumOnZAirGapSlotTop5mmCollarf1239 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop5mmCollarf1239, lensAirGapSlotTop5mmCollarf1239[2])
lossOnZAirGapSlotTop5mmCollarf1239 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop5mmCollarf1239, lensAirGapSlotTop5mmCollarf1239[2])
lossSliceZAirGapSlotTop5mmCollarf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop5mmCollarf1239, "X")

fieldsPlotAirGapSlotTop5mmCollarf1239=None
lensAirGapSlotTop5mmCollarf1239 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlotTop5mmCollarf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop5mmCollarf1652, lensAirGapSlotTop5mmCollarf1652 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop5mmCollarf1652)
lossSumOnZAirGapSlotTop5mmCollarf1652 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop5mmCollarf1652, lensAirGapSlotTop5mmCollarf1652[2])
lossOnZAirGapSlotTop5mmCollarf1652 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop5mmCollarf1652, lensAirGapSlotTop5mmCollarf1652[2])
lossSliceZAirGapSlotTop5mmCollarf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop5mmCollarf1652, "X")

fieldsPlotAirGapSlotTop5mmCollarf1652=None
lensAirGapSlotTop5mmCollarf1652 = None
(x90mm,y90mm,z90mm)=(None,None,None)

datTestAirGapSlotTop5mmCollarf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x90mm,y90mm,z90mm), fieldsPlotAirGapSlotTop5mmCollarf1977, lensAirGapSlotTop5mmCollarf1977 = imp.meshGridCSTFieldMap(datTestAirGapSlotTop5mmCollarf1977)
lossSumOnZAirGapSlotTop5mmCollarf1977 = imp.cumSumLoss3DInZ(fieldsPlotAirGapSlotTop5mmCollarf1977, lensAirGapSlotTop5mmCollarf1977[2])
lossOnZAirGapSlotTop5mmCollarf1977 = imp.sumLoss3DInZ(fieldsPlotAirGapSlotTop5mmCollarf1977, lensAirGapSlotTop5mmCollarf1977[2])
lossSliceZAirGapSlotTop5mmCollarf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotAirGapSlotTop5mmCollarf1977, "X")

fieldsPlotAirGapSlotTop5mmCollarf1977=None
lensAirGapSlotTop5mmCollarf1977 = None


meshtime=time.time()-start
print meshtime


##print sp.amax(fieldsPlot)
##print fieldsPlot[:,54,63]
##print z
##print len(x), len(y), len(z)
##print len(fieldsPlot[:,:,:])
##print len(fieldsPlot[0][0][:])
##datSumTest=cumSumLoss1D(datTest)



tmp = []
bins = 44
offset = 0
test = len(lossSumOnZSidePlatef0423)/bins
tmpZ=[]
for i in range(0,bins):
    for j in range(0,test):
        tmpZ.append(offset+test*i)

##print tmpZ

for i in range(0,len(tmpZ)):
##    print tmpZ[i], len(lossSumOnZ)
    if tmpZ[i]<len(lossSumOnZSidePlatef0423)-1:
        tmp.append(lossSumOnZSidePlatef0423[tmpZ[i]]/lossSumOnZSidePlatef0423[-1])
    else:
        tmp.append(lossSumOnZSidePlatef0423[-1]/lossSumOnZSidePlatef0423[-1])
tmp=sp.array(tmp)

##print tmp

tmpNorm = []
tmpNormZ=[]
for i in range(0,bins):
    for j in range(0,test):
        tmpNormZ.append(test*i)



##print test, len(tmpNormZ)
for i in range(0,len(tmpNormZ)):
    if i+test<len(tmpNormZ)-1:
##        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
##        print tmpZ[i+test]
        tmpNorm.append(sp.ma.average(lossOnZ[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNorm.append(sp.ma.average(lossOnZ[tmpZ[i]:-1]))
tmpNorm=sp.array(tmpNorm)



tmpNormSidePlate = []
tmpNormZSidePlate=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZSidePlate.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZSidePlate)):
    if i+test<len(tmpNormZSidePlate)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormSidePlate.append(sp.ma.average(lossOnZSidePlatef0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormSidePlate.append(sp.ma.average(lossOnZSidePlatef0423[tmpZ[i]:-1]))
tmpNormSidePlate=sp.array(tmpNormSidePlate)

tmpNormCapEndAir = []
tmpNormZCapEndAir=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZCapEndAir.append(test*i)



##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZCapEndAir)):
    if i+test<len(tmpNormZCapEndAir)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormCapEndAir.append(sp.ma.average(lossOnZCapEndAirf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormCapEndAir.append(sp.ma.average(lossOnZCapEndAirf0423[tmpZ[i]:-1]))
tmpNormCapEndAir=sp.array(tmpNormCapEndAir)



##tmpNormRingsAir50m = []
##tmpNormZRingsAir50m=[]
##for i in range(0,bins):
##   for j in range(0,test):
##        tmpNormZRingsAir50m.append(test*i)
##
##
####print test, len(tmpNormZSidePlate)
##for i in range(0,len(tmpNormZRingsAir50m)):
##    if i+test<len(tmpNormZRingsAir50m)-1:
######        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
######        print tmpZ[i+test]
##        tmpNormRingsAir50m.append(sp.ma.average(lossOnZRingsAir50mf0423[tmpZ[i]:tmpZ[i+test]]))
##    else:
##        tmpNormRingsAir50m.append(sp.ma.average(lossOnZRingsAir50mf0423[tmpZ[i]:-1]))
##tmpNormRingsAir50m=sp.array(tmpNormRingsAir50m)


tmpNormRings50mmDiam = []
tmpNormZRings50mmDiam=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZRings50mmDiam.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZRings50mmDiam)):
    if i+test<len(tmpNormZRings50mmDiam)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormRings50mmDiam.append(sp.ma.average(lossOnZRings50mmDiamf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormRings50mmDiam.append(sp.ma.average(lossOnZRings50mmDiamf0423[tmpZ[i]:-1]))
tmpNormRings50mmDiam=sp.array(tmpNormRings50mmDiam)


tmpNormRings60mmDiam = []
tmpNormZRings60mmDiam=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZRings60mmDiam.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZRings60mmDiam)):
    if i+test<len(tmpNormZRings60mmDiam)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormRings60mmDiam.append(sp.ma.average(lossOnZRings60mmDiamf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormRings60mmDiam.append(sp.ma.average(lossOnZRings60mmDiamf0423[tmpZ[i]:-1]))
tmpNormRings60mmDiam=sp.array(tmpNormRings60mmDiam)


tmpNormRings70mmDiam = []
tmpNormZRings70mmDiam=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZRings70mmDiam.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZRings70mmDiam)):
    if i+test<len(tmpNormZRings70mmDiam)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormRings70mmDiam.append(sp.ma.average(lossOnZRings70mmDiamf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormRings70mmDiam.append(sp.ma.average(lossOnZRings70mmDiamf0423[tmpZ[i]:-1]))
tmpNormRings70mmDiam=sp.array(tmpNormRings70mmDiam)


tmpNormRings90mmDiam = []
tmpNormZRings90mmDiam=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZRings90mmDiam.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZRings90mmDiam)):
    if i+test<len(tmpNormZRings90mmDiam)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormRings90mmDiam.append(sp.ma.average(lossOnZRings90mmDiamf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormRings90mmDiam.append(sp.ma.average(lossOnZRings90mmDiamf0423[tmpZ[i]:-1]))
tmpNormRings90mmDiam=sp.array(tmpNormRings90mmDiam)

tmpNormRings100mmDiam = []
tmpNormZRings100mmDiam=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZRings100mmDiam.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZRings100mmDiam)):
    if i+test<len(tmpNormZRings100mmDiam)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormRings100mmDiam.append(sp.ma.average(lossOnZRings100mmDiamf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormRings100mmDiam.append(sp.ma.average(lossOnZRings100mmDiamf0423[tmpZ[i]:-1]))
tmpNormRings100mmDiam=sp.array(tmpNormRings100mmDiam)


tmpNormFerriteCollar = []
tmpNormZFerriteCollar=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZFerriteCollar.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZFerriteCollar)):
    if i+test<len(tmpNormZFerriteCollar)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormFerriteCollar.append(sp.ma.average(lossOnZFerriteCollarf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormFerriteCollar.append(sp.ma.average(lossOnZFerriteCollarf0423[tmpZ[i]:-1]))
tmpNormFerriteCollar=sp.array(tmpNormFerriteCollar)


tmpNormFerriteCollar20mm = []
tmpNormZFerriteCollar20mm=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZFerriteCollar20mm.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZFerriteCollar20mm)):
    if i+test<len(tmpNormZFerriteCollar20mm)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormFerriteCollar20mm.append(sp.ma.average(lossOnZFerriteCollar20mmf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormFerriteCollar20mm.append(sp.ma.average(lossOnZFerriteCollar20mmf0423[tmpZ[i]:-1]))
tmpNormFerriteCollar20mm=sp.array(tmpNormFerriteCollar20mm)


tmpNormAirGap = []
tmpNormZAirGap=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZAirGap.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZAirGap)):
    if i+test<len(tmpNormZAirGap)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormAirGap.append(sp.ma.average(lossOnZAirGapf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormAirGap.append(sp.ma.average(lossOnZAirGapf0423[tmpZ[i]:-1]))
tmpNormAirGap=sp.array(tmpNormAirGap)


tmpNormAirGapSlot5mm = []
tmpNormZAirGapSlot5mm=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZAirGapSlot5mm.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZAirGapSlot5mm)):
    if i+test<len(tmpNormZAirGapSlot5mm)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormAirGapSlot5mm.append(sp.ma.average(lossOnZAirGapSlot5mmf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormAirGapSlot5mm.append(sp.ma.average(lossOnZAirGapSlot5mmf0423[tmpZ[i]:-1]))
tmpNormAirGapSlot5mm=sp.array(tmpNormAirGapSlot5mm)


tmpNormAirGapSlot5mmLongWake = []
tmpNormZAirGapSlot5mmLongWake=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZAirGapSlot5mmLongWake.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZAirGapSlot5mmLongWake)):
    if i+test<len(tmpNormZAirGapSlot5mmLongWake)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormAirGapSlot5mmLongWake.append(sp.ma.average(lossOnZAirGapSlot5mmLongWakef0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormAirGapSlot5mmLongWake.append(sp.ma.average(lossOnZAirGapSlot5mmLongWakef0423[tmpZ[i]:-1]))
tmpNormAirGapSlot5mmLongWake=sp.array(tmpNormAirGapSlot5mmLongWake)


tmpNormAirGapSlotTop5mm = []
tmpNormZAirGapSlotTop5mm=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZAirGapSlotTop5mm.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZAirGapSlotTop5mm)):
    if i+test<len(tmpNormZAirGapSlotTop5mm)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormAirGapSlotTop5mm.append(sp.ma.average(lossOnZAirGapSlotTop5mmf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormAirGapSlotTop5mm.append(sp.ma.average(lossOnZAirGapSlotTop5mmf0423[tmpZ[i]:-1]))
tmpNormAirGapSlotTop5mm=sp.array(tmpNormAirGapSlotTop5mm)


tmpNormAirGapSlotTop10mm = []
tmpNormZAirGapSlotTop10mm=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZAirGapSlotTop10mm.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZAirGapSlotTop10mm)):
    if i+test<len(tmpNormZAirGapSlotTop10mm)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormAirGapSlotTop10mm.append(sp.ma.average(lossOnZAirGapSlotTop10mmf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormAirGapSlotTop10mm.append(sp.ma.average(lossOnZAirGapSlotTop10mmf0423[tmpZ[i]:-1]))
tmpNormAirGapSlotTop10mm=sp.array(tmpNormAirGapSlotTop10mm)


tmpNormAirGapSlotTop20mm = []
tmpNormZAirGapSlotTop20mm=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZAirGapSlotTop20mm.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZAirGapSlotTop20mm)):
    if i+test<len(tmpNormZAirGapSlotTop20mm)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormAirGapSlotTop20mm.append(sp.ma.average(lossOnZAirGapSlotTop20mmf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormAirGapSlotTop20mm.append(sp.ma.average(lossOnZAirGapSlotTop20mmf0423[tmpZ[i]:-1]))
tmpNormAirGapSlotTop20mm=sp.array(tmpNormAirGapSlotTop20mm)


tmpNormAirGapSlotTop40mm = []
tmpNormZAirGapSlotTop40mm=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZAirGapSlotTop40mm.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZAirGapSlotTop40mm)):
    if i+test<len(tmpNormZAirGapSlotTop40mm)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormAirGapSlotTop40mm.append(sp.ma.average(lossOnZAirGapSlotTop40mmf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormAirGapSlotTop40mm.append(sp.ma.average(lossOnZAirGapSlotTop40mmf0423[tmpZ[i]:-1]))
tmpNormAirGapSlotTop40mm=sp.array(tmpNormAirGapSlotTop40mm)

tmpNormAirGapSlotTop5mmCollar = []
tmpNormZAirGapSlotTop5mmCollar=[]
for i in range(0,bins):
   for j in range(0,test):
        tmpNormZAirGapSlotTop5mmCollar.append(test*i)


##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZAirGapSlotTop5mmCollar)):
    if i+test<len(tmpNormZAirGapSlotTop5mmCollar)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormAirGapSlotTop5mmCollar.append(sp.ma.average(lossOnZAirGapSlotTop5mmCollarf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormAirGapSlotTop5mmCollar.append(sp.ma.average(lossOnZAirGapSlotTop5mmCollarf0423[tmpZ[i]:-1]))
tmpNormAirGapSlotTop5mmCollar=sp.array(tmpNormAirGapSlotTop5mmCollar)


###Losses No Side Plates ###

normf0423 = (lossOnZ/lossSumOnZ[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normf0838 = (lossOnZf0838/lossSumOnZf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normf1239 = (lossOnZf1239/lossSumOnZf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normf1652 = (lossOnZf1652/lossSumOnZf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normf1977 = (lossOnZf1977/lossSumOnZf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanVal = []
for i in range(0,len(normf0423)):
    meanVal.append(sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanVal = sp.array(meanVal)
meanSum = []
for i in range(0,len(meanVal)):
    meanSum.append(sp.sum(meanVal[:i]))
meanSum=sp.array(meanSum)/meanSum[-1]

powLossMKIAvg = 52

listZeros = sp.nonzero(lossOnZ)
##print listZeros[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLosses = list(sp.zeros(len(lossOnZ)))
for i in range(0,len(listZeros[0])-1):
##    print listZeros[0][currentUpperBound]
    while listZeros[0][currentUpperBound]==(listZeros[0][currentUpperBound+1]-1) and (currentUpperBound<len(listZeros[0])-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLosses[listZeros[0][i]]=sp.mean(meanVal[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]])
    if listZeros[0][i]==listZeros[0][currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
binnedLosses=sp.array(binnedLosses)*powLossMKIAvg*2.45/((z[0,0,1]-z[0,0,0])/1000)
        
##print z[0,0]
lossCalc = tmpNorm/lossSumOnZ[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
lossConst = lossOnZ/lossSumOnZ[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)

### Losses No Side Plates ###

###Losses for Side Plates ###

normSidePlatef0423 = (lossOnZSidePlatef0423/lossSumOnZSidePlatef0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normSidePlatef0838 = (lossOnZSidePlatef0838/lossSumOnZSidePlatef0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normSidePlatef1239 = (lossOnZSidePlatef1239/lossSumOnZSidePlatef1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normSidePlatef1652 = (lossOnZSidePlatef1652/lossSumOnZSidePlatef1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normSidePlatef1977 = (lossOnZSidePlatef1977/lossSumOnZSidePlatef1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValSidePlate = []
for i in range(0,len(normSidePlatef0423)):
    meanValSidePlate.append(sp.mean(normSidePlatef0423[i]+normSidePlatef0838[i]+normSidePlatef1239[i]+normSidePlatef1652[i]+normSidePlatef1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValSidePlate = sp.array(meanValSidePlate)
meanSumSidePlate = []
for i in range(0,len(meanValSidePlate)):
    meanSumSidePlate.append(sp.sum(meanValSidePlate[:i]))
meanSumSidePlate=sp.array(meanSumSidePlate)/meanSumSidePlate[-1]

##powLossMKIAvg = 52

listZerosSidePlate = sp.nonzero(lossOnZ)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesSidePlate = list(sp.zeros(len(lossOnZSidePlatef0423)))
for i in range(0,len(listZerosSidePlate[0])-1):
##    print listZeros[0][currentUpperBound]
    while listZerosSidePlate[0][currentUpperBound]==(listZerosSidePlate[0][currentUpperBound+1]-1) and (currentUpperBound<len(listZerosSidePlate[0])-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesSidePlate[listZerosSidePlate[0][i]]=sp.mean(meanValSidePlate[listZerosSidePlate[0][currentLowerBound]:listZerosSidePlate[0][currentUpperBound]])
    if listZerosSidePlate[0][i]==listZerosSidePlate[0][currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
binnedLossesSidePlate=sp.array(binnedLossesSidePlate)*powLossMKIAvg*2.45/((z[0,0,1]-z[0,0,0])/1000)
        
##print z[0,0]
lossCalcSidePlate = tmpNormSidePlate/lossSumOnZSidePlatef0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
lossConstSidePlate = lossOnZSidePlatef0423/lossSumOnZSidePlatef0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)

### End losses side Plates ###

###Losses Capacitive End Ferr Air ###

normCapEndAirf0423 = (lossOnZCapEndAirf0423/lossSumOnZCapEndAirf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normCapEndAirf0838 = (lossOnZCapEndAirf0838/lossSumOnZCapEndAirf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normCapEndAirf1239 = (lossOnZCapEndAirf1239/lossSumOnZCapEndAirf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normCapEndAirf1652 = (lossOnZCapEndAirf1652/lossSumOnZCapEndAirf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normCapEndAirf1977 = (lossOnZCapEndAirf1977/lossSumOnZCapEndAirf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValCapEndAir = []
for i in range(0,len(normCapEndAirf0423)):
    meanValCapEndAir.append(sp.mean(normCapEndAirf0423[i]+normCapEndAirf0838[i]+normCapEndAirf1239[i]+normCapEndAirf1652[i]+normCapEndAirf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValCapEndAir = sp.array(meanValCapEndAir)
meanSumCapEndAir = []
for i in range(0,len(meanValCapEndAir)):
    meanSumCapEndAir.append(sp.sum(meanValCapEndAir[:i]))
meanSumCapEndAir=sp.array(meanSumCapEndAir)/meanSumCapEndAir[-1]

##powLossMKIAvg = 52

listZerosCapEndAir = sp.nonzero(lossOnZ)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesCapEndAir = list(sp.zeros(len(lossOnZCapEndAirf0423)))
for i in range(0,len(listZerosCapEndAir[0])-1):
##    print listZeros[0][currentUpperBound]
    while listZerosCapEndAir[0][currentUpperBound]==(listZerosCapEndAir[0][currentUpperBound+1]-1) and (currentUpperBound<len(listZerosCapEndAir[0])-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesCapEndAir[listZerosCapEndAir[0][i]]=sp.mean(meanValCapEndAir[listZerosCapEndAir[0][currentLowerBound]:listZerosCapEndAir[0][currentUpperBound]])
    if listZerosCapEndAir[0][i]==listZerosCapEndAir[0][currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
binnedLossesCapEndAir=sp.array(binnedLossesCapEndAir)*powLossMKIAvg*2.45/((z[0,0,1]-z[0,0,0])/1000)
        
##print z[0,0]
lossCalcCapEndAir = tmpNormCapEndAir/lossSumOnZCapEndAirf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
lossConstCapEndAir = lossOnZCapEndAirf0423/lossSumOnZCapEndAirf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)

### End Capacitive End Ferr Air ###


###Losses Ferrite Rings as Air 50m Wake ###

##normRingsAir50mf0423 = (lossOnZRingsAir50mf0423/lossSumOnZRingsAir50mf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
##normRingsAir50mf0838 = (lossOnZRingsAir50mf0838/lossSumOnZRingsAir50mf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
##normRingsAir50mf1239 = (lossOnZRingsAir50mf1239/lossSumOnZRingsAir50mf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
##normRingsAir50mf1652 = (lossOnZRingsAir50mf1652/lossSumOnZRingsAir50mf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
##normRingsAir50mf1977 = (lossOnZRingsAir50mf1977/lossSumOnZRingsAir50mf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2
##
##meanValRingsAir50m = []
##for i in range(0,len(normRingsAir50mf0423)):
##    meanValRingsAir50m.append(sp.mean(normRingsAir50mf0423[i]+normRingsAir50mf0838[i]+normRingsAir50mf1239[i]+normRingsAir50mf1652[i]+normRingsAir50mf1977[i]))
####    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
##meanValRingsAir50m = sp.array(meanValRingsAir50m)
##meanSumRingsAir50m = []
##for i in range(0,len(meanValRingsAir50m)):
##    meanSumRingsAir50m.append(sp.sum(meanValRingsAir50m[:i]))
##meanSumRingsAir50m=sp.array(meanSumRingsAir50m)/meanSumRingsAir50m[-1]
##
##powLossMKIAvg = 52
##
##listZerosRingsAir50m = sp.nonzero(lossOnZ)
####print listZerosSidePlate[0][1]
##currentLowerBound = 0
##currentUpperBound = 1
##binnedLossesRingsAir50m = list(sp.zeros(len(lossOnZRingsAir50mf0423)))
##for i in range(0,len(listZerosRingsAir50m[0])-1):
####    print listZeros[0][currentUpperBound]
##    while listZerosRingsAir50m[0][currentUpperBound]==(listZerosRingsAir50m[0][currentUpperBound+1]-1) and (currentUpperBound<len(listZerosRingsAir50m[0])-2):
##        currentUpperBound+=1
####    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
##    binnedLossesRingsAir50m[listZerosRingsAir50m[0][i]]=sp.mean(meanValRingsAir50m[listZerosRingsAir50m[0][currentLowerBound]:listZerosRingsAir50m[0][currentUpperBound]])
##    if listZerosRingsAir50m[0][i]==listZerosRingsAir50m[0][currentUpperBound]:
##        currentLowerBound=currentUpperBound+1
##        currentUpperBound=currentLowerBound+1
##    
####binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
##binnedLossesRingsAir50m=sp.array(binnedLossesRingsAir50m)*powLossMKIAvg*2.45/((z[0,0,1]-z[0,0,0])/1000)
####binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
##        
####print z[0,0]
##lossCalcRingsAir50m = tmpNormRingsAir50m/lossSumOnZRingsAir50mf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
##lossConstRingsAir50m = lossOnZRingsAir50mf0423/lossSumOnZRingsAir50mf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)

### End Ferrite Rings as Air 50m Wake ###

###Losses Ferrite Rings 50mm Rad ###

normRings50mmDiamf0423 = (lossOnZRings50mmDiamf0423/lossSumOnZRings50mmDiamf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normRings50mmDiamf0838 = (lossOnZRings50mmDiamf0838/lossSumOnZRings50mmDiamf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normRings50mmDiamf1239 = (lossOnZRings50mmDiamf1239/lossSumOnZRings50mmDiamf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normRings50mmDiamf1652 = (lossOnZRings50mmDiamf1652/lossSumOnZRings50mmDiamf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normRings50mmDiamf1977 = (lossOnZRings50mmDiamf1977/lossSumOnZRings50mmDiamf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValRings50mmDiam = []
for i in range(0,len(normRings50mmDiamf0423)):
    meanValRings50mmDiam.append(sp.mean(normRings50mmDiamf0423[i]+normRings50mmDiamf0838[i]+normRings50mmDiamf1239[i]+normRings50mmDiamf1652[i]+normRings50mmDiamf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValRings50mmDiam = sp.array(meanValRings50mmDiam)
meanSumRings50mmDiam = []
for i in range(0,len(meanValRings50mmDiam)):
    meanSumRings50mmDiam.append(sp.sum(meanValRings50mmDiam[:i]))
meanSumRings50mmDiam=sp.array(meanSumRings50mmDiam)/meanSumRings50mmDiam[-1]

##powLossMKIAvg = 52

listZerosRings50mmDiam = sp.nonzero(lossOnZ)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesRings50mmDiam = list(sp.zeros(len(lossOnZRings50mmDiamf0423)))
for i in range(0,len(listZerosRings50mmDiam[0])-1):
##    print listZeros[0][currentUpperBound]
    while listZerosRings50mmDiam[0][currentUpperBound]==(listZerosRings50mmDiam[0][currentUpperBound+1]-1) and (currentUpperBound<len(listZerosRings50mmDiam[0])-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesRings50mmDiam[listZerosRings50mmDiam[0][i]]=sp.mean(meanValRings50mmDiam[listZerosRings50mmDiam[0][currentLowerBound]:listZerosRings50mmDiam[0][currentUpperBound]])
    if listZerosRings50mmDiam[0][i]==listZerosRings50mmDiam[0][currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesRings50mmDiam=sp.array(binnedLossesRings50mmDiam)*powLossMKIAvg*2.45/((z[0,0,1]-z[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcRings50mmDiam = tmpNormRings50mmDiam/lossSumOnZRings50mmDiamf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
lossConstRings50mmDiam = lossOnZRings50mmDiamf0423/lossSumOnZRings50mmDiamf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)

### End Ferrite Rings 50mm Rad ###

###Losses Ferrite Rings 60mm Rad ###

normRings60mmDiamf0423 = (lossOnZRings60mmDiamf0423/lossSumOnZRings60mmDiamf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normRings60mmDiamf0838 = (lossOnZRings60mmDiamf0838/lossSumOnZRings60mmDiamf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normRings60mmDiamf1239 = (lossOnZRings60mmDiamf1239/lossSumOnZRings60mmDiamf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normRings60mmDiamf1652 = (lossOnZRings60mmDiamf1652/lossSumOnZRings60mmDiamf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normRings60mmDiamf1977 = (lossOnZRings60mmDiamf1977/lossSumOnZRings60mmDiamf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValRings60mmDiam = []
for i in range(0,len(normRings60mmDiamf0423)):
    meanValRings60mmDiam.append(sp.mean(normRings60mmDiamf0423[i]+normRings60mmDiamf0838[i]+normRings60mmDiamf1239[i]+normRings60mmDiamf1652[i]+normRings60mmDiamf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValRings60mmDiam = sp.array(meanValRings60mmDiam)
meanSumRings60mmDiam = []
for i in range(0,len(meanValRings60mmDiam)):
    meanSumRings60mmDiam.append(sp.sum(meanValRings60mmDiam[:i]))
meanSumRings60mmDiam=sp.array(meanSumRings60mmDiam)/meanSumRings60mmDiam[-1]

##powLossMKIAvg = 52

listZerosRings60mmDiam = sp.nonzero(lossOnZ)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesRings60mmDiam = list(sp.zeros(len(lossOnZRings60mmDiamf0423)))
for i in range(0,len(listZerosRings60mmDiam[0])-1):
##    print listZeros[0][currentUpperBound]
    while listZerosRings60mmDiam[0][currentUpperBound]==(listZerosRings60mmDiam[0][currentUpperBound+1]-1) and (currentUpperBound<len(listZerosRings60mmDiam[0])-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesRings60mmDiam[listZerosRings60mmDiam[0][i]]=sp.mean(meanValRings60mmDiam[listZerosRings60mmDiam[0][currentLowerBound]:listZerosRings60mmDiam[0][currentUpperBound]])
    if listZerosRings60mmDiam[0][i]==listZerosRings60mmDiam[0][currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesRings60mmDiam=sp.array(binnedLossesRings60mmDiam)*powLossMKIAvg*2.45/((z[0,0,1]-z[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcRings60mmDiam = tmpNormRings60mmDiam/lossSumOnZRings60mmDiamf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
lossConstRings60mmDiam = lossOnZRings60mmDiamf0423/lossSumOnZRings60mmDiamf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)

### End Ferrite Rings 60mm Rad ###

###Losses Ferrite Rings 70mm Rad ###

normRings70mmDiamf0423 = (lossOnZRings70mmDiamf0423/lossSumOnZRings70mmDiamf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normRings70mmDiamf0838 = (lossOnZRings70mmDiamf0838/lossSumOnZRings70mmDiamf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normRings70mmDiamf1239 = (lossOnZRings70mmDiamf1239/lossSumOnZRings70mmDiamf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normRings70mmDiamf1652 = (lossOnZRings70mmDiamf1652/lossSumOnZRings70mmDiamf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normRings70mmDiamf1977 = (lossOnZRings70mmDiamf1977/lossSumOnZRings70mmDiamf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValRings70mmDiam = []
for i in range(0,len(normRings70mmDiamf0423)):
    meanValRings70mmDiam.append(sp.mean(normRings70mmDiamf0423[i]+normRings70mmDiamf0838[i]+normRings70mmDiamf1239[i]+normRings70mmDiamf1652[i]+normRings70mmDiamf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValRings70mmDiam = sp.array(meanValRings70mmDiam)
meanSumRings70mmDiam = []
for i in range(0,len(meanValRings70mmDiam)):
    meanSumRings70mmDiam.append(sp.sum(meanValRings70mmDiam[:i]))
meanSumRings70mmDiam=sp.array(meanSumRings70mmDiam)/meanSumRings70mmDiam[-1]

##powLossMKIAvg = 52

listZerosRings70mmDiam = sp.nonzero(lossOnZ)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesRings70mmDiam = list(sp.zeros(len(lossOnZRings70mmDiamf0423)))
for i in range(0,len(listZerosRings70mmDiam[0])-1):
##    print listZeros[0][currentUpperBound]
    while listZerosRings70mmDiam[0][currentUpperBound]==(listZerosRings70mmDiam[0][currentUpperBound+1]-1) and (currentUpperBound<len(listZerosRings70mmDiam[0])-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesRings70mmDiam[listZerosRings70mmDiam[0][i]]=sp.mean(meanValRings70mmDiam[listZerosRings70mmDiam[0][currentLowerBound]:listZerosRings70mmDiam[0][currentUpperBound]])
    if listZerosRings70mmDiam[0][i]==listZerosRings70mmDiam[0][currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesRings70mmDiam=sp.array(binnedLossesRings70mmDiam)*powLossMKIAvg*2.45/((z[0,0,1]-z[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcRings70mmDiam = tmpNormRings70mmDiam/lossSumOnZRings70mmDiamf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
lossConstRings70mmDiam = lossOnZRings70mmDiamf0423/lossSumOnZRings70mmDiamf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)

### End Ferrite Rings 70mm Rad ###


###Losses Ferrite Rings 90mm Rad ###

normRings90mmDiamf0423 = (lossOnZRings90mmDiamf0423/lossSumOnZRings90mmDiamf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normRings90mmDiamf0838 = (lossOnZRings90mmDiamf0838/lossSumOnZRings90mmDiamf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normRings90mmDiamf1239 = (lossOnZRings90mmDiamf1239/lossSumOnZRings90mmDiamf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normRings90mmDiamf1652 = (lossOnZRings90mmDiamf1652/lossSumOnZRings90mmDiamf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normRings90mmDiamf1977 = (lossOnZRings90mmDiamf1977/lossSumOnZRings90mmDiamf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValRings90mmDiam = []
for i in range(0,len(normRings90mmDiamf0423)):
    meanValRings90mmDiam.append(sp.mean(normRings90mmDiamf0423[i]+normRings90mmDiamf0838[i]+normRings90mmDiamf1239[i]+normRings90mmDiamf1652[i]+normRings90mmDiamf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValRings90mmDiam = sp.array(meanValRings90mmDiam)
meanSumRings90mmDiam = []
for i in range(0,len(meanValRings90mmDiam)):
    meanSumRings90mmDiam.append(sp.sum(meanValRings90mmDiam[:i]))
meanSumRings90mmDiam=sp.array(meanSumRings90mmDiam)/meanSumRings90mmDiam[-1]

##powLossMKIAvg = 52

listZerosRings90mmDiam = minValIndex(lossOnZRings90mmDiamf0423,10.0**3)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesRings90mmDiam = list(sp.zeros(len(lossOnZRings90mmDiamf0423)))
for i in range(0,len(listZerosRings90mmDiam)-1):
##    print listZeros[0][currentUpperBound]
    while listZerosRings90mmDiam[currentUpperBound]==(listZerosRings90mmDiam[currentUpperBound+1]-1) and (currentUpperBound<len(listZerosRings90mmDiam)-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesRings90mmDiam[listZerosRings90mmDiam[i]]=sp.mean(meanValRings90mmDiam[listZerosRings90mmDiam[currentLowerBound]:listZerosRings90mmDiam[currentUpperBound]])
    if listZerosRings90mmDiam[i]==listZerosRings90mmDiam[currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesRings90mmDiam=sp.array(binnedLossesRings90mmDiam)*powLossMKIAvg*2.45/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcRings70mmDiam = tmpNormRings70mmDiam/lossSumOnZRings70mmDiamf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
lossConstRings70mmDiam = lossOnZRings70mmDiamf0423/lossSumOnZRings70mmDiamf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)

### End Ferrite Rings 90mm Rad ###

###Losses Ferrite Rings 100mm Rad ###

normRings100mmDiamf0423 = (lossOnZRings100mmDiamf0423/lossSumOnZRings70mmDiamf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normRings100mmDiamf0838 = (lossOnZRings100mmDiamf0838/lossSumOnZRings70mmDiamf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normRings100mmDiamf1239 = (lossOnZRings100mmDiamf1239/lossSumOnZRings70mmDiamf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normRings100mmDiamf1652 = (lossOnZRings100mmDiamf1652/lossSumOnZRings70mmDiamf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normRings100mmDiamf1977 = (lossOnZRings100mmDiamf1977/lossSumOnZRings70mmDiamf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValRings100mmDiam = []
for i in range(0,len(normRings100mmDiamf0423)):
    meanValRings100mmDiam.append(sp.mean(normRings100mmDiamf0423[i]+normRings100mmDiamf0838[i]+normRings100mmDiamf1239[i]+normRings100mmDiamf1652[i]+normRings100mmDiamf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValRings100mmDiam = sp.array(meanValRings100mmDiam)
meanSumRings100mmDiam = []
for i in range(0,len(meanValRings100mmDiam)):
    meanSumRings100mmDiam.append(sp.sum(meanValRings100mmDiam[:i]))
meanSumRings100mmDiam=sp.array(meanSumRings100mmDiam)/meanSumRings100mmDiam[-1]

##powLossMKIAvg = 52

listZerosRings100mmDiam = sp.nonzero(lossOnZ)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesRings100mmDiam = list(sp.zeros(len(lossOnZRings100mmDiamf0423)))
for i in range(0,len(listZerosRings100mmDiam[0])-1):
##    print listZeros[0][currentUpperBound]
    while listZerosRings100mmDiam[0][currentUpperBound]==(listZerosRings100mmDiam[0][currentUpperBound+1]-1) and (currentUpperBound<len(listZerosRings100mmDiam[0])-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesRings100mmDiam[listZerosRings100mmDiam[0][i]]=sp.mean(meanValRings100mmDiam[listZerosRings100mmDiam[0][currentLowerBound]:listZerosRings100mmDiam[0][currentUpperBound]])
    if listZerosRings100mmDiam[0][i]==listZerosRings100mmDiam[0][currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesRings100mmDiam=sp.array(binnedLossesRings100mmDiam)*powLossMKIAvg*2.45/((z[0,0,1]-z[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcRings100mmDiam = tmpNormRings100mmDiam/lossSumOnZRings100mmDiamf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
lossConstRings100mmDiam = lossOnZRings100mmDiamf0423/lossSumOnZRings100mmDiamf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)

### End Ferrite Rings 100mm Rad ###

###Losses Ferrite Collar ###

normFerriteCollarf0423 = (lossOnZFerriteCollarf0423/lossSumOnZFerriteCollarf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normFerriteCollarf0838 = (lossOnZFerriteCollarf0838/lossSumOnZFerriteCollarf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normFerriteCollarf1239 = (lossOnZFerriteCollarf1239/lossSumOnZFerriteCollarf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normFerriteCollarf1652 = (lossOnZFerriteCollarf1652/lossSumOnZFerriteCollarf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normFerriteCollarf1977 = (lossOnZFerriteCollarf1977/lossSumOnZFerriteCollarf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValFerriteCollar = []
for i in range(0,len(normFerriteCollarf0423)):
    meanValFerriteCollar.append(sp.mean(normFerriteCollarf0423[i]+normFerriteCollarf0838[i]+normFerriteCollarf1239[i]+normFerriteCollarf1652[i]+normFerriteCollarf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValFerriteCollar = sp.array(meanValFerriteCollar)
meanSumFerriteCollar = []
for i in range(0,len(meanValFerriteCollar)):
    meanSumFerriteCollar.append(sp.sum(meanValFerriteCollar[:i]))
meanSumFerriteCollar=sp.array(meanSumFerriteCollar)/meanSumFerriteCollar[-1]

powLossMKIAvg = 52

listZerosFerriteCollar = minValIndex(lossOnZFerriteCollarf0423,10.0**5)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesFerriteCollar = list(sp.zeros(len(lossOnZFerriteCollarf0423)))
for i in range(0,len(listZerosFerriteCollar)-1):
##    print listZeros[0][currentUpperBound]
    while listZerosFerriteCollar[currentUpperBound]==(listZerosFerriteCollar[currentUpperBound+1]-1) and (currentUpperBound<len(listZerosFerriteCollar)-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesFerriteCollar[listZerosFerriteCollar[i]]=sp.mean(meanValFerriteCollar[listZerosFerriteCollar[currentLowerBound]:listZerosFerriteCollar[currentUpperBound]])
    if listZerosFerriteCollar[i]==listZerosFerriteCollar[currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesFerriteCollar=sp.array(binnedLossesFerriteCollar)*powLossMKIAvg*2.45/((z[0,0,1]-z[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcFerriteCollar = tmpNormFerriteCollar/lossSumOnZFerriteCollarf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
lossConstFerriteCollar = lossOnZFerriteCollarf0423/lossSumOnZFerriteCollarf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)

### End Ferrite Collar ###


###Losses Ferrite Collar 20mm###

normFerriteCollar20mmf0423 = (lossOnZFerriteCollar20mmf0423/lossSumOnZFerriteCollar20mmf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normFerriteCollar20mmf0838 = (lossOnZFerriteCollar20mmf0838/lossSumOnZFerriteCollar20mmf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normFerriteCollar20mmf1239 = (lossOnZFerriteCollar20mmf1239/lossSumOnZFerriteCollar20mmf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normFerriteCollar20mmf1652 = (lossOnZFerriteCollar20mmf1652/lossSumOnZFerriteCollar20mmf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normFerriteCollar20mmf1977 = (lossOnZFerriteCollar20mmf1977/lossSumOnZFerriteCollar20mmf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValFerriteCollar20mm = []
for i in range(0,len(normFerriteCollar20mmf0423)):
    meanValFerriteCollar20mm.append(sp.mean(normFerriteCollar20mmf0423[i]+normFerriteCollar20mmf0838[i]+normFerriteCollar20mmf1239[i]+normFerriteCollar20mmf1652[i]+normFerriteCollar20mmf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValFerriteCollar20mm = sp.array(meanValFerriteCollar20mm)
meanSumFerriteCollar20mm = []
for i in range(0,len(meanValFerriteCollar20mm)):
    meanSumFerriteCollar20mm.append(sp.sum(meanValFerriteCollar20mm[:i]))
meanSumFerriteColla20mm=sp.array(meanSumFerriteCollar20mm)/meanSumFerriteCollar20mm[-1]

##powLossMKIAvg = 52

listZerosFerriteCollar20mm = minValIndex(lossOnZFerriteCollar20mmf0423,10.0**3)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesFerriteCollar20mm = list(sp.zeros(len(lossOnZFerriteCollar20mmf0423)))
for i in range(0,len(listZerosFerriteCollar20mm)-1):
##    print listZeros[0][currentUpperBound]
    while listZerosFerriteCollar20mm[currentUpperBound]==(listZerosFerriteCollar20mm[currentUpperBound+1]-1) and (currentUpperBound<len(listZerosFerriteCollar20mm)-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesFerriteCollar20mm[listZerosFerriteCollar20mm[i]]=sp.mean(meanValFerriteCollar20mm[listZerosFerriteCollar20mm[currentLowerBound]:listZerosFerriteCollar20mm[currentUpperBound]])
    if listZerosFerriteCollar20mm[i]==listZerosFerriteCollar20mm[currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesFerriteCollar20mm=sp.array(binnedLossesFerriteCollar20mm)*powLossMKIAvg*2.45/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcFerriteCollar20mm = tmpNormFerriteCollar20mm/lossSumOnZFerriteCollar20mmf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
lossConstFerriteCollar20mm = lossOnZFerriteCollar20mmf0423/lossSumOnZFerriteCollar20mmf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)

### End Ferrite Collar 20mm###

### Losses Air Gap###

normAirGapf0423 = (lossOnZAirGapf0423/lossSumOnZAirGapf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normAirGapf0838 = (lossOnZAirGapf0838/lossSumOnZAirGapf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normAirGapf1239 = (lossOnZAirGapf1239/lossSumOnZAirGapf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normAirGapf1652 = (lossOnZAirGapf1652/lossSumOnZAirGapf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normAirGapf1977 = (lossOnZAirGapf1977/lossSumOnZAirGapf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValAirGap = []
for i in range(0,len(normAirGapf0423)):
    meanValAirGap.append(sp.mean(normAirGapf0423[i]+normAirGapf0838[i]+normAirGapf1239[i]+normAirGapf1652[i]+normAirGapf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValAirGap = sp.array(meanValAirGap)
meanSumAirGap = []
for i in range(0,len(meanValAirGap)):
    meanSumAirGap.append(sp.sum(meanValAirGap[:i]))
meanSumAirGap=sp.array(meanSumAirGap)/meanSumAirGap[-1]

##powLossMKIAvg = 52

listZerosAirGap = minValIndex(lossOnZAirGapf0423,10.0**3)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesAirGap = list(sp.zeros(len(lossOnZAirGapf0423)))
for i in range(0,len(listZerosAirGap)-1):
##    print listZeros[0][currentUpperBound]
    while listZerosAirGap[currentUpperBound]==(listZerosAirGap[currentUpperBound+1]-1) and (currentUpperBound<len(listZerosAirGap)-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesAirGap[listZerosAirGap[i]]=sp.mean(meanValAirGap[listZerosAirGap[currentLowerBound]:listZerosAirGap[currentUpperBound]])
    if listZerosAirGap[i]==listZerosAirGap[currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesAirGap=sp.array(binnedLossesAirGap)*powLossMKIAvg*2.45/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcAirGap = tmpNormAirGap/lossSumOnZAirGapf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
lossConstAirGap = lossOnZAirGapf0423/lossSumOnZAirGapf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)

### End Air Gap ###

### Losses Air Gap+Slot 5mm ###

normAirGapSlot5mmf0423 = (lossOnZAirGapSlot5mmf0423/lossSumOnZAirGapSlot5mmf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normAirGapSlot5mmf0838 = (lossOnZAirGapSlot5mmf0838/lossSumOnZAirGapSlot5mmf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normAirGapSlot5mmf1239 = (lossOnZAirGapSlot5mmf1239/lossSumOnZAirGapSlot5mmf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normAirGapSlot5mmf1652 = (lossOnZAirGapSlot5mmf1652/lossSumOnZAirGapSlot5mmf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normAirGapSlot5mmf1977 = (lossOnZAirGapSlot5mmf1977/lossSumOnZAirGapSlot5mmf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValAirGapSlot5mm = []
for i in range(0,len(normAirGapSlot5mmf0423)):
    meanValAirGapSlot5mm.append(sp.mean(normAirGapSlot5mmf0423[i]+normAirGapSlot5mmf0838[i]+normAirGapSlot5mmf1239[i]+normAirGapSlot5mmf1652[i]+normAirGapSlot5mmf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValAirGapSlot5mm = sp.array(meanValAirGapSlot5mm)
meanSumAirGapSlot5mm= []
for i in range(0,len(meanValAirGapSlot5mm)):
    meanSumAirGapSlot5mm.append(sp.sum(meanValAirGapSlot5mm[:i]))
meanSumAirGapSlot5mm=sp.array(meanSumAirGapSlot5mm)/meanSumAirGapSlot5mm[-1]

##powLossMKIAvg = 52

listZerosAirGapSlot5mm = minValIndex(lossOnZAirGapSlot5mmf0423,10.0**3)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesAirGapSlot5mm = list(sp.zeros(len(lossOnZAirGapSlot5mmf0423)))
for i in range(0,len(listZerosAirGapSlot5mm)-1):
##    print listZeros[0][currentUpperBound]
    while listZerosAirGapSlot5mm[currentUpperBound]==(listZerosAirGapSlot5mm[currentUpperBound+1]-1) and (currentUpperBound<len(listZerosAirGapSlot5mm)-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesAirGapSlot5mm[listZerosAirGapSlot5mm[i]]=sp.mean(meanValAirGapSlot5mm[listZerosAirGapSlot5mm[currentLowerBound]:listZerosAirGapSlot5mm[currentUpperBound]])
    if listZerosAirGapSlot5mm[i]==listZerosAirGapSlot5mm[currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesAirGapSlot5mm=sp.array(binnedLossesAirGapSlot5mm)*powLossMKIAvg*2.45/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcAirGapSlot5mm = tmpNormAirGapSlot5mm/lossSumOnZAirGapSlot5mmf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
lossConstAirGapSlot5mm = lossOnZAirGapSlot5mmf0423/lossSumOnZAirGapSlot5mmf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)

### End Air Gap+Slot 5mm ###

### Losses Air Gap+Slot 5mm Long Wake ###

normAirGapSlot5mmLongWakef0423 = (lossOnZAirGapSlot5mmLongWakef0423/lossSumOnZAirGapSlot5mmLongWakef0423[-1])*prof.gaussProf(0.4365*10**9, bLength)**2
normAirGapSlot5mmLongWakef0838 = (lossOnZAirGapSlot5mmLongWakef0838/lossSumOnZAirGapSlot5mmLongWakef0838[-1])*prof.gaussProf(0.86*10**9, bLength)**2
normAirGapSlot5mmLongWakef1239 = (lossOnZAirGapSlot5mmLongWakef1239/lossSumOnZAirGapSlot5mmLongWakef1239[-1])*prof.gaussProf(1.264*10**9, bLength)**2
normAirGapSlot5mmLongWakef1652 = (lossOnZAirGapSlot5mmLongWakef1652/lossSumOnZAirGapSlot5mmLongWakef1652[-1])*prof.gaussProf(1.661*10**9, bLength)**2
normAirGapSlot5mmLongWakef1977 = (lossOnZAirGapSlot5mmLongWakef1977/lossSumOnZAirGapSlot5mmLongWakef1977[-1])*prof.gaussProf(2.029*10**9, bLength)**2

meanValAirGapSlot5mmLongWake = []
for i in range(0,len(normAirGapSlot5mmLongWakef0423)):
    meanValAirGapSlot5mmLongWake.append(sp.mean(normAirGapSlot5mmLongWakef0423[i]+normAirGapSlot5mmLongWakef0838[i]+normAirGapSlot5mmLongWakef1239[i]+normAirGapSlot5mmLongWakef1652[i]+normAirGapSlot5mmLongWakef1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValAirGapSlot5mmLongWake = sp.array(meanValAirGapSlot5mmLongWake)
meanSumAirGapSlot5mmLongWake= []
for i in range(0,len(meanValAirGapSlot5mmLongWake)):
    meanSumAirGapSlot5mmLongWake.append(sp.sum(meanValAirGapSlot5mmLongWake[:i]))
meanSumAirGapSlot5mmLongWake=sp.array(meanSumAirGapSlot5mmLongWake)/meanSumAirGapSlot5mmLongWake[-1]

##powLossMKIAvg = 52

listZerosAirGapSlot5mmLongWake = minValIndex(lossOnZAirGapSlot5mmLongWakef0423,10.0**3)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesAirGapSlot5mmLongWake = list(sp.zeros(len(lossOnZAirGapSlot5mmLongWakef0423)))
for i in range(0,len(listZerosAirGapSlot5mmLongWake)-1):
##    print listZeros[0][currentUpperBound]
    while listZerosAirGapSlot5mmLongWake[currentUpperBound]==(listZerosAirGapSlot5mmLongWake[currentUpperBound+1]-1) and (currentUpperBound<len(listZerosAirGapSlot5mmLongWake)-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesAirGapSlot5mmLongWake[listZerosAirGapSlot5mmLongWake[i]]=sp.mean(meanValAirGapSlot5mmLongWake[listZerosAirGapSlot5mmLongWake[currentLowerBound]:listZerosAirGapSlot5mmLongWake[currentUpperBound]])
    if listZerosAirGapSlot5mmLongWake[i]==listZerosAirGapSlot5mmLongWake[currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesAirGapSlot5mmLongWake=sp.array(binnedLossesAirGapSlot5mmLongWake)*powLossMKIAvg*2.45/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcAirGapSlot5mmLongWake = tmpNormAirGapSlot5mmLongWake/lossSumOnZAirGapSlot5mmLongWakef0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
lossConstAirGapSlot5mmLongWake = lossOnZAirGapSlot5mmLongWakef0423/lossSumOnZAirGapSlot5mmLongWakef0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)

### End Air Gap+Slot 5mm Long Wake ###

### Losses Air Gap+SlotTop 5mm ###

normAirGapSlotTop5mmf0423 = (lossOnZAirGapSlotTop5mmf0423/lossSumOnZAirGapSlotTop5mmf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normAirGapSlotTop5mmf0838 = (lossOnZAirGapSlotTop5mmf0838/lossSumOnZAirGapSlotTop5mmf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normAirGapSlotTop5mmf1239 = (lossOnZAirGapSlotTop5mmf1239/lossSumOnZAirGapSlotTop5mmf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normAirGapSlotTop5mmf1652 = (lossOnZAirGapSlotTop5mmf1652/lossSumOnZAirGapSlotTop5mmf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normAirGapSlotTop5mmf1977 = (lossOnZAirGapSlotTop5mmf1977/lossSumOnZAirGapSlotTop5mmf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValAirGapSlotTop5mm = []
for i in range(0,len(normAirGapSlotTop5mmf0423)):
    meanValAirGapSlotTop5mm.append(sp.mean(normAirGapSlotTop5mmf0423[i]+normAirGapSlotTop5mmf0838[i]+normAirGapSlotTop5mmf1239[i]+normAirGapSlotTop5mmf1652[i]+normAirGapSlotTop5mmf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValAirGapSlotTop5mm = sp.array(meanValAirGapSlotTop5mm)
meanSumAirGapSlotTop5mm= []
for i in range(0,len(meanValAirGapSlotTop5mm)):
    meanSumAirGapSlotTop5mm.append(sp.sum(meanValAirGapSlotTop5mm[:i]))
meanSumAirGapSlotTop5mm=sp.array(meanSumAirGapSlotTop5mm)/meanSumAirGapSlotTop5mm[-1]

##powLossMKIAvg = 52

listZerosAirGapSlotTop5mm = minValIndex(lossOnZAirGapSlotTop5mmf0423,10.0**3)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesAirGapSlotTop5mm = list(sp.zeros(len(lossOnZAirGapSlotTop5mmf0423)))
for i in range(0,len(listZerosAirGapSlotTop5mm)-1):
##    print listZeros[0][currentUpperBound]
    while listZerosAirGapSlotTop5mm[currentUpperBound]==(listZerosAirGapSlotTop5mm[currentUpperBound+1]-1) and (currentUpperBound<len(listZerosAirGapSlotTop5mm)-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesAirGapSlotTop5mm[listZerosAirGapSlotTop5mm[i]]=sp.mean(meanValAirGapSlotTop5mm[listZerosAirGapSlotTop5mm[currentLowerBound]:listZerosAirGapSlotTop5mm[currentUpperBound]])
    if listZerosAirGapSlotTop5mm[i]==listZerosAirGapSlotTop5mm[currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesAirGapSlotTop5mm=sp.array(binnedLossesAirGapSlotTop5mm)*powLossMKIAvg*2.45/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcAirGapSlotTop5mm = tmpNormAirGapSlotTop5mm/lossSumOnZAirGapSlotTop5mmf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
lossConstAirGapSlotTop5mm = lossOnZAirGapSlotTop5mmf0423/lossSumOnZAirGapSlotTop5mmf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)

### End Air Gap+SlotTop 5mm ###


### Losses Air Gap+SlotTop 10mm ###

normAirGapSlotTop10mmf0423 = (lossOnZAirGapSlotTop10mmf0423/lossSumOnZAirGapSlotTop10mmf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normAirGapSlotTop10mmf0838 = (lossOnZAirGapSlotTop10mmf0838/lossSumOnZAirGapSlotTop10mmf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normAirGapSlotTop10mmf1239 = (lossOnZAirGapSlotTop10mmf1239/lossSumOnZAirGapSlotTop10mmf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normAirGapSlotTop10mmf1652 = (lossOnZAirGapSlotTop10mmf1652/lossSumOnZAirGapSlotTop10mmf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normAirGapSlotTop10mmf1977 = (lossOnZAirGapSlotTop10mmf1977/lossSumOnZAirGapSlotTop10mmf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValAirGapSlotTop10mm = []
for i in range(0,len(normAirGapSlotTop10mmf0423)):
    meanValAirGapSlotTop10mm.append(sp.mean(normAirGapSlotTop10mmf0423[i]+normAirGapSlotTop10mmf0838[i]+normAirGapSlotTop10mmf1239[i]+normAirGapSlotTop10mmf1652[i]+normAirGapSlotTop10mmf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValAirGapSlotTop10mm = sp.array(meanValAirGapSlotTop10mm)
meanSumAirGapSlotTop10mm= []
for i in range(0,len(meanValAirGapSlotTop10mm)):
    meanSumAirGapSlotTop10mm.append(sp.sum(meanValAirGapSlotTop10mm[:i]))
meanSumAirGapSlotTop10mm=sp.array(meanSumAirGapSlotTop10mm)/meanSumAirGapSlotTop10mm[-1]

##powLossMKIAvg = 52

listZerosAirGapSlotTop10mm = minValIndex(lossOnZAirGapSlotTop10mmf0423,10.0**3)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesAirGapSlotTop10mm = list(sp.zeros(len(lossOnZAirGapSlotTop10mmf0423)))
for i in range(0,len(listZerosAirGapSlotTop10mm)-1):
##    print listZeros[0][currentUpperBound]
    while listZerosAirGapSlotTop10mm[currentUpperBound]==(listZerosAirGapSlotTop10mm[currentUpperBound+1]-1) and (currentUpperBound<len(listZerosAirGapSlotTop10mm)-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesAirGapSlotTop10mm[listZerosAirGapSlotTop10mm[i]]=sp.mean(meanValAirGapSlotTop10mm[listZerosAirGapSlotTop10mm[currentLowerBound]:listZerosAirGapSlotTop10mm[currentUpperBound]])
    if listZerosAirGapSlotTop10mm[i]==listZerosAirGapSlotTop10mm[currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesAirGapSlotTop10mm=sp.array(binnedLossesAirGapSlotTop10mm)*powLossMKIAvg*2.45/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcAirGapSlotTop10mm = tmpNormAirGapSlotTop10mm/lossSumOnZAirGapSlotTop10mmf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
lossConstAirGapSlotTop10mm = lossOnZAirGapSlotTop10mmf0423/lossSumOnZAirGapSlotTop10mmf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)

### End Air Gap+SlotTop 10mm ###


### Losses Air Gap+SlotTop 20mm ###

normAirGapSlotTop20mmf0423 = (lossOnZAirGapSlotTop20mmf0423/lossSumOnZAirGapSlotTop20mmf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normAirGapSlotTop20mmf0838 = (lossOnZAirGapSlotTop20mmf0838/lossSumOnZAirGapSlotTop20mmf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normAirGapSlotTop20mmf1239 = (lossOnZAirGapSlotTop20mmf1239/lossSumOnZAirGapSlotTop20mmf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normAirGapSlotTop20mmf1652 = (lossOnZAirGapSlotTop20mmf1652/lossSumOnZAirGapSlotTop20mmf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normAirGapSlotTop20mmf1977 = (lossOnZAirGapSlotTop20mmf1977/lossSumOnZAirGapSlotTop20mmf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValAirGapSlotTop20mm = []
for i in range(0,len(normAirGapSlotTop20mmf0423)):
    meanValAirGapSlotTop20mm.append(sp.mean(normAirGapSlotTop20mmf0423[i]+normAirGapSlotTop20mmf0838[i]+normAirGapSlotTop20mmf1239[i]+normAirGapSlotTop20mmf1652[i]+normAirGapSlotTop20mmf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValAirGapSlotTop20mm = sp.array(meanValAirGapSlotTop20mm)
meanSumAirGapSlotTop20mm= []
for i in range(0,len(meanValAirGapSlotTop20mm)):
    meanSumAirGapSlotTop20mm.append(sp.sum(meanValAirGapSlotTop20mm[:i]))
meanSumAirGapSlotTop20mm=sp.array(meanSumAirGapSlotTop20mm)/meanSumAirGapSlotTop20mm[-1]

##powLossMKIAvg = 52

listZerosAirGapSlotTop20mm = minValIndex(lossOnZAirGapSlotTop20mmf0423,10.0**3)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesAirGapSlotTop20mm = list(sp.zeros(len(lossOnZAirGapSlotTop20mmf0423)))
for i in range(0,len(listZerosAirGapSlotTop20mm)-1):
##    print listZeros[0][currentUpperBound]
    while listZerosAirGapSlotTop20mm[currentUpperBound]==(listZerosAirGapSlotTop20mm[currentUpperBound+1]-1) and (currentUpperBound<len(listZerosAirGapSlotTop20mm)-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesAirGapSlotTop20mm[listZerosAirGapSlotTop20mm[i]]=sp.mean(meanValAirGapSlotTop20mm[listZerosAirGapSlotTop20mm[currentLowerBound]:listZerosAirGapSlotTop20mm[currentUpperBound]])
    if listZerosAirGapSlotTop20mm[i]==listZerosAirGapSlotTop20mm[currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesAirGapSlotTop20mm=sp.array(binnedLossesAirGapSlotTop20mm)*powLossMKIAvg*2.45/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcAirGapSlotTop20mm = tmpNormAirGapSlotTop20mm/lossSumOnZAirGapSlotTop20mmf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
lossConstAirGapSlotTop20mm = lossOnZAirGapSlotTop20mmf0423/lossSumOnZAirGapSlotTop20mmf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)

### End Air Gap+SlotTop 20mm ###


### Losses Air Gap+SlotTop 40mm ###

normAirGapSlotTop40mmf0423 = (lossOnZAirGapSlotTop40mmf0423/lossSumOnZAirGapSlotTop40mmf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normAirGapSlotTop40mmf0838 = (lossOnZAirGapSlotTop40mmf0838/lossSumOnZAirGapSlotTop40mmf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normAirGapSlotTop40mmf1239 = (lossOnZAirGapSlotTop40mmf1239/lossSumOnZAirGapSlotTop40mmf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normAirGapSlotTop40mmf1652 = (lossOnZAirGapSlotTop40mmf1652/lossSumOnZAirGapSlotTop40mmf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normAirGapSlotTop40mmf1977 = (lossOnZAirGapSlotTop40mmf1977/lossSumOnZAirGapSlotTop40mmf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValAirGapSlotTop40mm = []
for i in range(0,len(normAirGapSlotTop40mmf0423)):
    meanValAirGapSlotTop40mm.append(sp.mean(normAirGapSlotTop40mmf0423[i]+normAirGapSlotTop40mmf0838[i]+normAirGapSlotTop40mmf1239[i]+normAirGapSlotTop40mmf1652[i]+normAirGapSlotTop40mmf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValAirGapSlotTop40mm = sp.array(meanValAirGapSlotTop40mm)
meanSumAirGapSlotTop40mm= []
for i in range(0,len(meanValAirGapSlotTop40mm)):
    meanSumAirGapSlotTop40mm.append(sp.sum(meanValAirGapSlotTop40mm[:i]))
meanSumAirGapSlotTop40mm=sp.array(meanSumAirGapSlotTop40mm)/meanSumAirGapSlotTop40mm[-1]

##powLossMKIAvg = 52

listZerosAirGapSlotTop40mm = minValIndex(lossOnZAirGapSlotTop40mmf0423,10.0**3)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesAirGapSlotTop40mm = list(sp.zeros(len(lossOnZAirGapSlotTop40mmf0423)))
for i in range(0,len(listZerosAirGapSlotTop40mm)-1):
##    print listZeros[0][currentUpperBound]
    while listZerosAirGapSlotTop40mm[currentUpperBound]==(listZerosAirGapSlotTop40mm[currentUpperBound+1]-1) and (currentUpperBound<len(listZerosAirGapSlotTop40mm)-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesAirGapSlotTop40mm[listZerosAirGapSlotTop40mm[i]]=sp.mean(meanValAirGapSlotTop40mm[listZerosAirGapSlotTop40mm[currentLowerBound]:listZerosAirGapSlotTop40mm[currentUpperBound]])
    if listZerosAirGapSlotTop40mm[i]==listZerosAirGapSlotTop40mm[currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesAirGapSlotTop40mm=sp.array(binnedLossesAirGapSlotTop40mm)*powLossMKIAvg*2.45/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcAirGapSlotTop40mm = tmpNormAirGapSlotTop40mm/lossSumOnZAirGapSlotTop40mmf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
lossConstAirGapSlotTop40mm = lossOnZAirGapSlotTop40mmf0423/lossSumOnZAirGapSlotTop40mmf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)

### End Air Gap+SlotTop 40mm ###


### Losses Air Gap+SlotTop 5mm+FerriteCollar ###

normAirGapSlotTop5mmCollarf0423 = (lossOnZAirGapSlotTop5mmCollarf0423/lossSumOnZAirGapSlotTop5mmCollarf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normAirGapSlotTop5mmCollarf0838 = (lossOnZAirGapSlotTop5mmCollarf0838/lossSumOnZAirGapSlotTop5mmCollarf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normAirGapSlotTop5mmCollarf1239 = (lossOnZAirGapSlotTop5mmCollarf1239/lossSumOnZAirGapSlotTop5mmCollarf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normAirGapSlotTop5mmCollarf1652 = (lossOnZAirGapSlotTop5mmCollarf1652/lossSumOnZAirGapSlotTop5mmCollarf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normAirGapSlotTop5mmCollarf1977 = (lossOnZAirGapSlotTop5mmCollarf1977/lossSumOnZAirGapSlotTop5mmCollarf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValAirGapSlotTop5mmCollar = []
for i in range(0,len(normAirGapSlotTop5mmCollarf0423)):
    meanValAirGapSlotTop5mmCollar.append(sp.mean(normAirGapSlotTop5mmCollarf0423[i]+normAirGapSlotTop5mmCollarf0838[i]+normAirGapSlotTop5mmCollarf1239[i]+normAirGapSlotTop5mmCollarf1652[i]+normAirGapSlotTop5mmCollarf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValAirGapSlotTop5mmCollar = sp.array(meanValAirGapSlotTop5mmCollar)
meanSumAirGapSlotTop5mmCollar= []
for i in range(0,len(meanValAirGapSlotTop5mmCollar)):
    meanSumAirGapSlotTop5mmCollar.append(sp.sum(meanValAirGapSlotTop5mmCollar[:i]))
meanSumAirGapSlotTop5mmCollar=sp.array(meanSumAirGapSlotTop5mmCollar)/meanSumAirGapSlotTop5mmCollar[-1]

##powLossMKIAvg = 52

listZerosAirGapSlotTop5mmCollar = minValIndex(lossOnZAirGapSlotTop5mmCollarf0423,10.0**3)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesAirGapSlotTop5mmCollar = list(sp.zeros(len(lossOnZAirGapSlotTop5mmCollarf0423)))
for i in range(0,len(listZerosAirGapSlotTop5mmCollar)-1):
##    print listZeros[0][currentUpperBound]
    while listZerosAirGapSlotTop5mmCollar[currentUpperBound]==(listZerosAirGapSlotTop5mmCollar[currentUpperBound+1]-1) and (currentUpperBound<len(listZerosAirGapSlotTop5mmCollar)-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesAirGapSlotTop5mmCollar[listZerosAirGapSlotTop5mmCollar[i]]=sp.mean(meanValAirGapSlotTop5mmCollar[listZerosAirGapSlotTop5mmCollar[currentLowerBound]:listZerosAirGapSlotTop5mmCollar[currentUpperBound]])
    if listZerosAirGapSlotTop5mmCollar[i]==listZerosAirGapSlotTop5mmCollar[currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesAirGapSlotTop5mmCollar=sp.array(binnedLossesAirGapSlotTop5mmCollar)*powLossMKIAvg*2.45/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcAirGapSlotTop5mmCollar = tmpNormAirGapSlotTop5mmCollar/lossSumOnZAirGapSlotTop5mmCollarf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)
lossConstAirGapSlotTop5mmCollar = lossOnZAirGapSlotTop5mmCollarf0423/lossSumOnZAirGapSlotTop5mmCollarf0423[-1]*powLossMKIAvg/((z90mm[0,0,1]-z90mm[0,0,0])/1000)

### End Air Gap+SlotTop 5mm+FerriteCollar ###

print sp.mean(binnedLosses)
print sp.mean(binnedLossesSidePlate)
print sp.mean(binnedLossesCapEndAir)
##print sp.mean(binnedLossesRingsAir)
##print sp.mean(binnedLossesRingsAir30m)
##print sp.mean(binnedLossesRingsAir50m)
print sp.mean(binnedLossesRings50mmDiam)
print sp.mean(binnedLossesRings60mmDiam)
print sp.mean(binnedLossesRings70mmDiam)
print sp.mean(binnedLossesRings100mmDiam)
##print sp.mean(binnedLossesFerriteCollar)
print sp.mean(binnedLossesFerriteCollar20mm)
print sp.mean(binnedLossesAirGap)
print sp.mean(binnedLossesAirGapSlot5mm)
print sp.mean(binnedLossesAirGapSlotTop5mm)
print sp.mean(binnedLossesAirGapSlotTop10mm)
print sp.mean(binnedLossesAirGapSlotTop20mm)
print sp.mean(binnedLossesAirGapSlotTop40mm)
print sp.mean(binnedLossesAirGapSlotTop5mmCollar)
              
print sp.sum(meanValSidePlate*powLossMKIAvg*2.45)
print sp.sum(meanValCapEndAir*powLossMKIAvg*2.45)
##print sp.sum(meanValRingsAir*powLossMKIAvg*2.45)
##print sp.sum(meanValRingsAir30m*powLossMKIAvg*2.45)
print sp.sum(meanValRings50mmDiam*powLossMKIAvg*2.45)
print sp.sum(meanValRings60mmDiam*powLossMKIAvg*2.45)
print sp.sum(meanValRings70mmDiam*powLossMKIAvg*2.45)
print sp.sum(meanValRings100mmDiam*powLossMKIAvg*2.45)
##print sp.sum(meanValFerriteCollar*powLossMKIAvg*2.45)
print sp.sum(meanValFerriteCollar20mm*powLossMKIAvg*2.45)
print sp.sum(meanValAirGap*powLossMKIAvg*2.45)
print sp.sum(meanValAirGapSlot5mm*powLossMKIAvg*2.45)
print sp.sum(meanValAirGapSlotTop5mm*powLossMKIAvg*2.45)
print sp.sum(meanValAirGapSlotTop10mm*powLossMKIAvg*2.45)
print sp.sum(meanValAirGapSlotTop20mm*powLossMKIAvg*2.45)
print sp.sum(meanValAirGapSlotTop40mm*powLossMKIAvg*2.45)
print sp.sum(meanValAirGapSlotTop5mmCollar*powLossMKIAvg*2.45)

##pl.semilogy()
##pl.plot(fieldsPlot[:,27,44])
##print datSumTest
##pl.plot(datSumTest[:,0], datSumTest[:,1])
##pl.plot(datTest[:,2], datTest[:,3])
##for i in range(0,lens[0]):
##    for j in range(0,lens[1]):
##        pl.plot(fieldsPlot[:,i,j])
##pl.plot(z[:,0],lossSumOnZ/lossSumOnZ[-1])
##pl.plot(z[0,0], lossSumOnZ/lossSumOnZ[-1],"k-",label="f=0.438GHz, no side plates")
##pl.plot(z[0,0], lossSumOnZf0838/lossSumOnZf0838[-1],"r-",label="f=0.838GHz, no side plates")
##pl.plot(z[0,0], lossSumOnZf1239/lossSumOnZf1239[-1],"b-",label="f=1.239GHz, no side plates")
##pl.plot(z[0,0], lossSumOnZf1652/lossSumOnZf1652[-1],"m-",label="f=1.652GHz, no side plates")
##pl.plot(z[0,0], lossSumOnZf1977/lossSumOnZf1977[-1],"y-",label="f=1.977GHz, no side plates")
##pl.plot(z[0,0], meanSum,"g-",label="Mean Value, no side plates")
##pl.plot(z[0,0], lossSumOnZSidePlatef0423/lossSumOnZSidePlatef0423[-1],"k-",label="f=0.438GHz, side plates")
##pl.plot(z[0,0], lossSumOnZSidePlatef0838/lossSumOnZSidePlatef0838[-1],"r-",label="f=0.838GHz, side plates")
##pl.plot(z[0,0], lossSumOnZSidePlatef1239/lossSumOnZSidePlatef1239[-1],"b-",label="f=1.239GHz, side plates")
##pl.plot(z[0,0], lossSumOnZSidePlatef1652/lossSumOnZSidePlatef1652[-1],"g-",label="f=1.652GHz, side plates")
##pl.plot(z[0,0], lossSumOnZSidePlatef1977/lossSumOnZSidePlatef1977[-1],"m-",label="f=1.977GHz, side plates")
##pl.plot(z[0,0], meanSumSidePlate,"b-",label="Mean Value, side plates")
##pl.plot(z[0,0],binnedLossesSidePlate, "r-",label="Losses averaged to single components, side plates")
##pl.plot(z[0,0],binnedLossesCapEndAir, "m-",label="Losses averaged to single components, Cap End Air")
##pl.plot(z[0,0],binnedLossesRingsAir, "g-",label="Losses averaged to single components, Ferrite Rings Air")
##pl.plot(z[0,0],binnedLossesRingsAir30m, "g-",label="Losses averaged to single components, Ferrite Rings Air 30m Wake")
##pl.plot(z[0,0],binnedLossesRingsAir50m, "r-",label="Losses averaged to single components, Ferrite Rings Air 50m Wake")
##pl.plot(z[0,0],binnedLossesRings50mmDiam, "k-",label="Losses averaged to single components, Ferrite Rings 50mm Rad")
##pl.plot(z[0,0],binnedLossesRings60mmDiam, "m-",label="Losses averaged to single components, Ferrite Rings 60mm Rad")
pl.plot(z[0,0],binnedLossesRings60mmDiam, "k-",label="Losses averaged to single components, Post LS1")
##pl.plot(z[0,0],binnedLossesRings70mmDiam, "y-",label="Losses averaged to single components, Ferrite Rings 70mm Rad")
##pl.plot(z90mm[0,0],binnedLossesRings90mmDiam, "b--",label="Losses averaged to single components, Ferrite Rings 90mm Rad")
##pl.plot(z[0,0],binnedLossesRings100mmDiam, "r--",label="Losses averaged to single components, Ferrite Rings 100mm Rad")
##pl.plot(z[0,0],binnedLossesFerriteCollar, "k--",label="Losses averaged to single components, Ferrite Collar")
##pl.plot(z90mm[0,0],binnedLossesFerriteCollar20mm, "g--",label="Losses averaged to single components, Ferrite Collar 20mm")
##pl.plot(z90mm[0,0],binnedLossesAirGap*1.84, "b-",label="Losses averaged to single components, Air Gap")
##pl.plot(z90mm[0,0],binnedLossesAirGapSlot5mm*1.84, "g-",label="Losses averaged to single components, Air Gap+70mm Overlap")
##pl.plot(z90mm[0,0],binnedLossesAirGapSlotTop5mm*1.84, "r-",label="Losses averaged to single components, Air Gap+SlotTop5mm")
##pl.plot(z90mm[0,0],binnedLossesAirGapSlot5mmLongWake*1.84, "r-",label="Losses averaged to single components, Air Gap+Slot5mm Long Wake")
##pl.plot(z90mm[0,0],binnedLossesAirGapSlotTop10mm*1.84, "b-",label="Losses averaged to single components, Air Gap+SlotTop10mm")
##pl.plot(z90mm[0,0],binnedLossesAirGapSlotTop20mm*1.84, "b-",label="Losses averaged to single components, Air Gap+SlotTop20mm")
##pl.plot(z90mm[0,0],binnedLossesAirGapSlotTop40mm*1.84, "k--",label="Losses averaged to single components, Air Gap+SlotTop40mm")
##pl.plot(z90mm[0,0],binnedLossesAirGapSlotTop5mmCollar*1.84, "m-",label="Losses averaged to single components, Air Gap+SlotTop5mm+Collar")
pl.xlabel("Displacement from centre of magnet (mm)", fontsize=24.0)
pl.ylabel("Power Loss (W/m)", fontsize=24.0)
##pl.ylabel("Power Loss (a.u)", fontsize=16.0)
pl.legend(loc="upper right")
pl.show()
pl.clf()

##pl.plot(z[0,0],meanValSidePlate*powLossMKIAvg*2.45, "r-",label="Losses averaged to single components, side plates")
##pl.plot(z[0,0],meanValSideFerr*powLossMKIAvg*2.45, "b-",label="Losses averaged to single components, side ferrites")
##pl.plot(z[0,0],meanValCapEndAir*powLossMKIAvg*2.45, "m-",label="Losses averaged to single components, Cap End Air")
##pl.plot(z[0,0],meanValRingsAir*powLossMKIAvg*2.45, "k--",label="Losses averaged to single components, Ferrite Rings Air")
pl.plot(z[0,0],meanSumRings60mmDiam, "b-", label="Post-LS1")
pl.plot(z90mm[0,0],meanSumFerriteCollar20mm/meanSumFerriteCollar20mm[-1], "m-", label="Post-LS1 Ferrite Collar")
pl.plot(z90mm[0,0],meanSumAirGap, "k-", label="Proposed HL-LHC 117mm")
pl.plot(z90mm[0,0],meanSumAirGapSlot5mm, "g-", label="Proposed HL-LHC+70mm Overlap")
##pl.plot(z90mm[0,0],meanSumAirGapSlotTop5mm, "r-", label="Proposed HL-LHC 117mm 5mm Slot")
##pl.plot(z90mm[0,0],meanSumAirGapSlotTop10mm, "r--", label="Proposed HL-LHC 117mm 10mm Slot")
pl.plot(z90mm[0,0],meanSumAirGapSlotTop20mm, "b--", label="Proposed HL-LHC 117mm 20mm Slot")
pl.plot(z90mm[0,0],meanSumAirGapSlotTop40mm, "g--", label="Proposed HL-LHC 117mm 40mm Slot")
pl.plot(z90mm[0,0],meanSumAirGapSlotTop5mmCollar, "k--", label="Proposed HL-LHC 117mm Ferrite Collar")
##pl.plot(z[0,0], lossOnZ)
##pl.plot(z[0,0], meanVal,"g-",label="Mean Value")
##pl.plot(z[0,0], lossConst, "r-", label="Losses")
##print len(z[0,:]), len(lossSumOnZ)
##pl.plot(tmp)
##pl.plot(tmpNorm/lossSumOnZ[-1])
##pl.plot(sliceZPlot)
##print histoZ
##pl.hist()
##pl.plot(lossSliceZ)
##print len(z[0,0,:-1]), len(lossCalc)
##pl.plot(z[0,0,:len(tmpNormZ)], lossCalc)
##pl.plot(z[0,0]+offset*10, lossConst)
##pl.axhline(52,label="Average 52W/m")
pl.xlabel("Displacement from centre of magnet (mm)", fontsize=24.0)
pl.ylabel("Power Loss Integrated Along Length (a.u.)", fontsize=24.0)
##pl.ylabel("Power Loss (a.u)", fontsize=16.0)
pl.legend(loc="lower right")
pl.show()
pl.clf()

##pl.plot(z[0,0],lossOnZFerriteCollarf0423)
pl.plot(z90mm[0,0],lossOnZFerriteCollar20mmf0423)
pl.plot(z90mm[0,0],lossOnZFerriteCollar20mmf0838)
pl.plot(z90mm[0,0],lossOnZFerriteCollar20mmf1239)
pl.plot(z90mm[0,0],lossOnZFerriteCollar20mmf1652)
pl.plot(z90mm[0,0],lossOnZFerriteCollar20mmf1977)
##pl.plot(z90mm[0,0],lossOnZAirGapf0423)
pl.plot(z90mm[0,0],lossOnZAirGapSlot5mmf0423)
pl.plot(z90mm[0,0],lossOnZAirGapSlot5mmf0838)
pl.plot(z90mm[0,0],lossOnZAirGapSlot5mmf1239)
pl.plot(z90mm[0,0],lossOnZAirGapSlot5mmf1652)
pl.plot(z90mm[0,0],lossOnZAirGapSlot5mmf1977)
pl.plot(z90mm[0,0],lossOnZAirGapSlotTop5mmf0423)
pl.plot(z90mm[0,0],lossOnZAirGapSlotTop5mmf0838)
pl.plot(z90mm[0,0],lossOnZAirGapSlotTop5mmf1239)
pl.plot(z90mm[0,0],lossOnZAirGapSlotTop5mmf1652)
pl.plot(z90mm[0,0],lossOnZAirGapSlotTop5mmf1977)
##pl.show()
pl.clf()

##for i in range(0,len(fieldsPlot[:,0,0])):
##    pl.subplot(2,1,1)
    ##cmapPlot = pl.pcolormesh(lossSliceZ)
##    cmapPlot = pl.pcolormesh(fieldsPlot[i,:,:])
    ##cmapPlot = pl.pcolormesh(lossSliceZ[lens[2]/2])
##    pl.colorbar()
    ##pl.show()
##    pl.savefig(directory+"figs/"+str(i)+".png")
##    pl.clf()

##fig=pl.figure(1)
##fig.clf()
##ax=ToolKit3d.Axes3D(fig)
##ax.scatter(mesh[0],mesh[1],mesh[2], c=fieldsPlot)
##pl.draw()
##pl.show()
##pl.clf()

stop=time.time()-start
print stop
