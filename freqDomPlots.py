import csv, os, sys
import scipy as sp
import pylab as pl
import scipy.special as spec

def waterBagFreq(freq, length):
    return -(2*sp.pi)**0.5*spec.kn(freq*length/(2*(-(freq*length)**-0.5)),1)/freq

bunLen = 1.0*10.0**-9

freq_list = []
for i in range(3,10,1):
    for j in range(1,100,1):
        freq_list.append(float((j/10.0)*10.0**i))

freq_list = list(set(freq_list))
freq_list.sort()

freqProf = []
for entry in freq_list:
    freqProf.append(waterBagFreq(entry,bunLen))

freqProf=sp.array(freqProf)
freqProfdB = 20*sp.log10(freqProf)
freq_list=sp.array(freq_list)

##pl.semilogy()
##pl.plot(freq_list, freqProf)
pl.plot(freq_list/10**9, freqProfdB)
pl.grid(linestyle="--", which = "major")
pl.xlabel("Frequency (GHz)", fontsize = 16)                  #Label axes
pl.ylabel("S $(dB)$", fontsize = 16)
##pl.ylabel("$\Im{}m (Z_{\parallel})$ $(\Omega)$",fontsize = 16)
##pl.title("", fontsize = 16)
pl.legend(loc="upper left")
##pl.axis([0,10**6,0,10**3])
##pl.savefig(directory+"test"+".pdf")                  # Save fig for film
pl.show()
pl.clf()   
