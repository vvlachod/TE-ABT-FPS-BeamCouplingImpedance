import csv, os, sys, time
import scipy as sp
import pylab as pl

########################## Imported Profile ##################################

start = time.time()

C = 299792458.0
circ=26659.0
n_bunches = 1404
p_bunch = 1.15*10**11
Q_part = 1.6*10**-19
f_rev = 2*10**7



I_b = p_bunch*Q_part*f_rev
power_tot = I_b**2#*n_bunches

print I_b, power_tot

top_directory = "E:/PhD/1st_Year_09-10/Data/mki-heating/heating-estimates/"


directory_prof = "E:/PhD/1st_Year_09-10/Data/mki-heating/longitudinal-profile/"
directory_imp = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/new-plots/"
measurements_24 = "E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/24-strips-measurements.csv"
directory_sims_24 = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/impedances-288m-long/all-conductors/"
directory_sims_15 = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/impedances-288m-long/15-conductors/"
directory_sims_none = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/impedances-288m-long/no-conductors/"

sim_impedance = "longitudinal-impedance.csv"

impedance_values = []

data=open(directory_imp+"longitudinal.csv", 'r+')
linelist = data.readlines()
data.close()
for row in linelist[:-3]:
    row=map(float, row.rsplit(','))
    impedance_values.append([row[0]/1000, row[1]])

impedance_values=sp.array(impedance_values)

poly_coeffs_imp = sp.polyfit(impedance_values[:,0],impedance_values[:,1],30)
x_test = sp.linspace(0,2,1000)
imp_fit = sp.polyval(poly_coeffs_imp,x_test)

impedance_values_24 = []

data=open(measurements_24, 'r+')
linelist = data.readlines()
data.close()
for row in linelist[:-3]:
    row=map(float, row.rsplit(','))
    impedance_values_24.append([row[0]/1000, row[1]])

impedance_values_24=sp.array(impedance_values_24)

poly_coeffs_imp_24 = sp.polyfit(impedance_values_24[:,0],impedance_values_24[:,1],10)
x_test = sp.linspace(0,1.8,1000)
imp_fit_24 = sp.polyval(poly_coeffs_imp,x_test)

##pl.plot(impedance_values_24[:,0],impedance_values_24[:,1],'r+')
##pl.plot(x_test,imp_fit_24,'b-')
##pl.axis([0,2,0,50])
##pl.show()
##pl.clf()

impedance_values_24 = []

data=open(directory_sims_24+sim_impedance, 'r+')
linelist = data.readlines()
data.close()
for row in linelist[:-3]:
    row=map(float, row.rsplit(','))
    impedance_values_24.append([row[0]/1000, row[1]])

impedance_values_24=sp.array(impedance_values_24)


impedance_values_15 = []

data=open(directory_sims_15+sim_impedance, 'r+')
linelist = data.readlines()
data.close()
for row in linelist[:-3]:
    row=map(float, row.rsplit(','))
    impedance_values_15.append([row[0]/1000, row[1]])

impedance_values_15=sp.array(impedance_values_15)


impedance_values_none = []

data=open(directory_sims_none+sim_impedance, 'r+')
linelist = data.readlines()
data.close()
for row in linelist[:-3]:
    row=map(float, row.rsplit(','))
    impedance_values_none.append([row[0]/1000, row[1]])

impedance_values_none=sp.array(impedance_values_none)

##pl.plot(impedance_values_none[:,0],impedance_values_none[:,1])
##pl.plot(impedance_values_15[:,0],impedance_values_15[:,1])
##pl.plot(impedance_values_24[:,0],impedance_values_24[:,1])
##pl.show()


prof_values = []
data=open(directory_prof+"long_bunch_power_spectrum",'r+')
linelist=data.readlines()
data.close()
for row in linelist[2:]:
    row=map(float,row.rsplit(','))
    prof_values.append(row)

prof_values=sp.array(prof_values)

x_val = prof_values[:,0]
y_val = prof_values[:,1]
lin_y = 10**(y_val/20)/(10**(y_val[3]/20))
lin_fit_imp = sp.polyfit(x_val,lin_y,80)

poly_coeffs_prof = sp.polyfit(x_val,y_val,70)
x_test_prof = sp.linspace(0,3,3000)
prof_fit = sp.polyval(poly_coeffs_prof, x_test_prof)
lin_prof_fit = sp.polyval(lin_fit_imp,x_test_prof)


##pl.plot(prof_values[:,0],lin_y[:],'r+')
##pl.plot(x_test_prof,lin_prof_fit,'b-')
##pl.show()
##pl.clf()


convolution_imp = []
for i in range(0,1800000000,f_rev):
    j = float(i)/10**9
    convolution_imp.append([j,power_tot*2*sp.polyval(poly_coeffs_imp,j)*(sp.polyval(lin_fit_imp,j))*3.55/2.7])


total_imp=0.0
running_total = []
for i in range(0,len(convolution_imp)):
    total_imp+=convolution_imp[i][1]
    running_total.append(total_imp)
##    print total_imp


##convolution_imp_sims_24 = []
##
##for i in range(0,1800000000,f_rev):
##    j = float(i)/10**9
##    convolution_imp_sims_24.append([j,power_tot*2*sp.polyval(poly_coeffs_imp,j)*(sp.polyval(lin_fit_imp,j))*3.55/2.7])
##
##
##total_imp_24=0.0
##running_total_24 = []
##for i in range(0,len(convolution_imp_sims_24)):
##    total_imp_24+=convolution_imp_sims_24[i][1]
##    running_total_24.append(total_imp)
##
##
##convolution_imp_sims_15 = []
##
##for i in range(0,1800000000,f_rev):
##    j = float(i)/10**9
##    convolution_imp_sim_15.append([j,power_tot*2*sp.polyval(poly_coeffs_imp,j)*(sp.polyval(lin_fit_imp,j))*3.55/2.7])
##
##
##total_imp_15=0.0
##running_total_15 = []
##for i in range(0,len(convolution_imp_sims_15)):
##    total_imp_15+=convolution_imp_sims_15[i][1]
##    running_total_15.append(total_imp)
##
##
##convolution_imp_sims_none = []
##
##for i in range(0,1800000000,f_rev):
##    j = float(i)/10**9
##    convolution_imp_sims_none.append([j,power_tot*2*sp.polyval(poly_coeffs_imp,j)*(sp.polyval(lin_fit_imp,j))*3.55/2.7])
##
##
##total_imp_none=0.0
##running_total_none = []
##for i in range(0,len(convolution_imp_sims_none)):
##    total_imp_none+=convolution_imp_sims_none[i][1]
##    running_total_none.append(total_imp)



########################## Cos^2 profile #############################


t_0 = 0.6*10**-9
data_points = 1000.0
sample_rate = 0.1/(t_0/data_points)

t=pl.r_[-40.0*t_0:40.0*t_0:1/sample_rate]
##print len(t)
omega = sample_rate
s = []
for i in t:
    if abs(i)<=t_0:
        s.append(sp.cos(2*sp.pi*0.25/t_0*i)**2)
    else:
        s.append(0)
s = sp.array(s)
N=len(t)
S=sp.fft(s)
f = sample_rate*sp.r_[0:(N/2)]/N
n=len(f)

##pl.plot(t,s)
##pl.show()
##pl.clf()
##print f
f=f/10**9
curr = abs(S[0:n])/abs(S[0])
S=(abs(S[0:n]))**2/(abs(S[0]))**2

##
##pl.plot(f,(abs(S[0:n]))**2/(abs(S[0]))**2)
##pl.plot(f,S[0:n],label='Cos$^{2}$ fit')
##pl.plot(f,curr)
######pl.plot(x_test_prof, lin_prof_fit, label='Linear Measured')
##pl.axis([0,3,0,1])
##pl.xlabel('Frequency (GHz)')
##pl.ylabel('Magnitude')
##pl.show()
##pl.clf()

##pl.plot(f/10**9,S[0:n])
##pl.axis([0,3,0,2])
##pl.show()
##pl.clf()


total=0.0
convolution_cos = []
running_total_cos = []

print f[1]-f[0]
for i in range(0,len(f)-1):
##    j = (float(f[i])+((float(f[1])-float(f[0]))/2))/10**9
    j = float(f[i])#/10**9

    if j<1.8: #and (j*1000)%40 == 0.0:
##        print j
        convolution_cos.append([j,power_tot*2*sp.polyval(poly_coeffs_imp,j)*S[i]])
        total+=power_tot*2*sp.polyval(poly_coeffs_imp,j)*S[i]*3.55/2.7
        running_total_cos.append(total)
##        print total
##for i in range(0,len(convolution_cos)):
##    total+=convolution_cos[i][1]

convolution_imp=sp.array(convolution_imp)
convolution_cos=sp.array(convolution_cos)

##pl.plot(convolution_imp[:,0], convolution_imp[:,1], label = 'Measured Spectrum Power Loss')
##pl.plot(convolution_cos[:,0], convolution_cos[:,1], label = 'Cos$^{2}$ Spectrum Power Loss')
##pl.legend()
##pl.xlabel('Frequency (GHz)')
##pl.ylabel('Power Loss (W/m)')
##pl.show()
##pl.clf()


##pl.plot(convolution_imp[:,0], running_total, label = 'Measured Spectrum Power Loss')
##pl.plot(convolution_cos[:,0], running_total_cos, 'k-',label = 'Cos$^{2}$ Spectrum Power Loss')
##pl.legend(loc='upper left')
##pl.xlabel('Frequency (GHz)')
##pl.ylabel('Integrated Power Loss (W/m)')
##pl.axis([0,2,0,120])
##pl.show()
##pl.clf()

##print running_total, running_total_cos
print total_imp*0.8, total

######### Measured Spectra from Phillipe and Themis ##########

f_rev = C/circ
I_b = p_bunch*Q_part*f_rev#*n_bunches
power_tot = I_b**2

power_tot = Q_part**2*sp.pi*p_bunch**2*n_bunches*f_rev

print I_b, power_tot



spectra_directory = "E:/PhD/1st_Year_09-10/Data/Bunch_Spectra/"
spectra_list = [spectra_directory+i for i in os.listdir(spectra_directory)]

count = 1
for file_ent in spectra_list:
    data_file = open(file_ent, 'r+')
    data = data_file.readlines()
    convolution_imp = []
    data_file.close()
    for i in range(0,len(data)/2):
        if i == 0:
            convolution_imp.append([float(data[i]),0.0])
        elif float(data[i]) < 1.75*10**9:
            convolution_imp.append([float(data[i]),power_tot*2*(float(data[1])-float(data[0]))*10**(float(data[i+32000])/40)*sp.polyval(poly_coeffs_imp,float(data[i])/10**9)])
##            convolution_imp.append([float(data[i]),power_tot*2*10**(float(data[i+32000])/20)*sp.polyval(poly_coeffs_imp,float(data[i])/10**9)])

        else:
            pass

    convolution_imp=sp.array(convolution_imp)
    total_imp=0
    running_total = []
    for i in range(0,len(convolution_imp)):
        total_imp += float(convolution_imp[i,1])
        running_total.append(total_imp)


    ax1 = pl.subplot(111)
    pl.plot(data[:32000],data[32000:], 'b-')
    pl.xlabel('Frequency (Hz)')
    pl.ylabel("S(f) (dB)")
    if "BeforeRamp" in file_ent:
        text = "Before Ramp $P_{loss}$ = %f W" % total_imp
    elif "Ramp" in file_ent:
        text = "Ramp $P_{loss}$ = %f W" % total_imp
    elif "FlatTop" in file_ent:
        text = "Flat Top $P_{loss}$ = %f W" % total_imp
    elif "Squeeze" in file_ent:
        text = "Squeeze $P_{loss}$ = %f W" % total_imp
    elif "Adjust" in file_ent:
        text = "Adjust $P_{loss}$ = %f W" % total_imp
    elif "StableBeams" in file_ent:
        text = "Stable Beams $P_{loss}$ = %f W" % total_imp

    pl.axis([0, 3.5*10**9, -60, 10])
    pl.figtext(0.55,0.6, text, size=12.0, bbox={'facecolor':'white', 'pad':10})
    ax2 = pl.twinx()
    

    pl.plot(x_test*10**9,imp_fit,'k-')
    pl.plot(impedance_values[:,0]*10**9,impedance_values[:,1],'r+')
    pl.ylabel("Impedance ($\Omega$)")
    pl.legend(loc = "upper right")
    pl.axis([0, 3.5*10**9, -15, 60])
    idea =file_ent.replace(spectra_directory, '')
    idea = idea.rstrip('.csv')
    pl.savefig(top_directory+"tex_output/"+idea+'.png')
    pl.savefig(top_directory+"tex_output/"+idea+'.eps')
    pl.savefig(top_directory+"tex_output/"+idea+'.pdf')
    pl.clf()
    count+=1
    
    pl.plot(convolution_imp[:,0], running_total, 'k-', label = "Cumulative Power Loss")
    pl.xlabel('Frequency (Hz)')
    pl.ylabel('Cumulative Power Loss (W)')
    pl.savefig(top_directory+"tex_output/"+idea+'_cumulative_power_loss.png')
    pl.savefig(top_directory+"tex_output/"+idea+'_cumulative_power_loss.eps')
    pl.savefig(top_directory+"tex_output/"+idea+'_cumulative_power_loss.pdf')
    pl.figtext(0.55,0.6, text, size=12.0, bbox={'facecolor':'white', 'pad':10})
    pl.clf()


    pl.plot(convolution_imp[:,0], convolution_imp[:,1], 'k-', label = "Cumulative Power Loss")
    pl.xlabel('Frequency (Hz)')
    pl.ylabel('Power Loss (W)')
    pl.savefig(top_directory+"tex_output/"+idea+'_frequency_power_loss.png')
    pl.savefig(top_directory+"tex_output/"+idea+'_frequency_power_loss.eps')
    pl.savefig(top_directory+"tex_output/"+idea+'_frequency_power_loss.pdf')
    pl.figtext(0.55,0.6, text, size=12.0, bbox={'facecolor':'white', 'pad':10})
    pl.clf()

    lin_approx = []
    for i in range(32000, len(data)):
        lin_approx.append(10**(float(data[i])/40))
    pl.plot(data[:32000],lin_approx, 'b-', label="Meas. Phillipe, Themis")
    pl.plot(x_test_prof*10**9,lin_prof_fit,'k-', label="Meas. R. Steinhagen")
    pl.xlabel('Frequency (Hz)')
    pl.ylabel("S(f)")
    pl.legend(loc="upper right")
    pl.savefig(top_directory+"tex_output/"+idea+'_spectra_comp.png')
    pl.clf()

print "Time Taken for code to run = %f (s)" % (time.time()-start)



