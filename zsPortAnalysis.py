import csv, time, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
from scipy import signal as sg

####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def eigenmodeGen(freqFile, qFile, rOverQFile):
    inputFreq=open(freqFile, "r+")
    datFreq=inputFreq.readlines()
    inputFreq.close()
    outputFreq=[]
    for line in datFreq[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputFreq.append(float(temp))
    inputQ=open(qFile, "r+")
    datQ=inputQ.readlines()
    inputQ.close()
    outputQ=[]
    for line in datQ[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputQ.append(float(temp))
    inputrOverQ=open(rOverQFile, "r+")
    datrOverQ=inputrOverQ.readlines()
    inputrOverQ.close()
    outputrOverQ=[]
    for line in datrOverQ[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputrOverQ.append(float(temp))
    resDat=[]
    for i in range(0,len(outputFreq)):
        resDat.append([outputFreq[i], outputQ[i], outputrOverQ[i]])

    return resDat

def Z_bb(freq, data):
    return data[2]*data[1]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2*(2**0.5))

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2


def readS21FromVNA(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[6:]:
        temp=line.split("\t")
        outputData.append(map(float,temp))
    return outputData

def logToLin(data):
    return 10**(data/20)

def linToLog(data):
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

def logImpFormulaImg(dataDUT, lenRef, Zc, freqDat):
    adjDat = []
    last=0
    lastCount = 0
    correction = 0
    limitMin=-160
    limitMax=160
    for i in range(0,len(dataDUT)):
        if last < limitMin and dataDUT[i]>limitMax and i-lastCount > 20:
            correction+=360
            adjDat.append(dataDUT[i]-correction)
            last=dataDUT[i]
            lastCount=i
        else:
            adjDat.append(dataDUT[i]-correction)
            last=dataDUT[i]
    adjDat=sp.array(adjDat)
    adjDat = Zc*((adjDat/360)*(2*sp.pi)+(2*sp.pi*freqDat/(C*lenRef)))
##    adjDat = ((adjDat/360)*(2*sp.pi)+(2*sp.pi*freqDat/(C*lenRef)))
    return adjDat

targetDir = "E:/PhD/1st_Year_09-10/Data/MKIRFFingers/impedanceSimulations/"
impNoRFFingers = sp.array(extract_dat(targetDir+"longitudinalImpedanceCavityNoFingers.csv"))
impConnecteRFFingers10m = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mmAttached.csv"))
impConnecteRFFingers30m = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mmAttached30mWake.csv"))
imp1BrokenRFFinger5deg = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mm1Detached30mWake.csv"))
imp2BrokenRFFinger5deg = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mm2Detached10mWake.csv"))
imp3BrokenRFFinger5deg = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mm3Detached10mWake.csv"))
imp1BrokenRFFinger1deg = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mm1Detached1Degree10mWake.csv"))
imp2BrokenRFFinger1deg = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mm2Detached1Degree10mWake.csv"))
imp3BrokenRFFinger1deg = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mm3Detached1Degree10mWake.csv"))
imp1BrokenRFFinger1degSpring = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mm1Detached1Degree10mWakeLumpedCircuit.csv"))

resDirectory = "E:/PhD/1st_Year_09-10/Data/MKIRFFingers/eigenmode/"
res1Fing1Deg=eigenmodeGen(resDirectory+"1FingersSeperated1DegreeHighMeshDensity/frequencies.txt",
                          resDirectory+"1FingersSeperated1DegreeHighMeshDensity/q_1.txt",
                          resDirectory+"1FingersSeperated1DegreeHighMeshDensity/rOverQ.txt")
res3Fing1Deg=eigenmodeGen(resDirectory+"3FingersSeperated1Degree/frequencies.txt",
                          resDirectory+"3FingersSeperated1Degree/q_1.txt",
                          resDirectory+"3FingersSeperated1Degree/rOverQ.txt")
res1Fing5Deg=eigenmodeGen(resDirectory+"1FingersSeperated5DegreeHighMeshDensity/frequencies.txt",
                          resDirectory+"1FingersSeperated5DegreeHighMeshDensity/q_1.txt",
                          resDirectory+"1FingersSeperated5DegreeHighMeshDensity/rOverQ.txt")
res3Fing1Deg=eigenmodeGen(resDirectory+"3FingersSeperated1Degree/frequencies.txt",
                          resDirectory+"3FingersSeperated1Degree/q_1.txt",
                          resDirectory+"3FingersSeperated1Degree/rOverQ.txt")

##freqRangeRes[sp.linspace(10**6,2*10**9,2000):


targetDirProbeMeas = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/15-11-13/"
s11OriginalInput = sp.array(readS21FromVNA(targetDirProbeMeas+"S11FREQORIGINALPOS.S2P"))
s11OriginalInputAnode = sp.array(readS21FromVNA(targetDirProbeMeas+"S11FREQORIGINALPOSANODE.S2P"))
s11OriginalInputAnodeOpen = sp.array(readS21FromVNA(targetDirProbeMeas+"S11FREQORIGINALPOSANODEOPEN.S2P"))
s11OriginalInputAnodeOtherOpen = sp.array(readS21FromVNA(targetDirProbeMeas+"S11FREQORIGINALPOSANODEOTHEROPEN.S2P"))

####### Import Simulated Measurements ########

targetDirSimMeas = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/SimulationResultsBenoit/"
simMeasS1115ghz=sp.array(extract_dat(targetDirSimMeas+"S11.csv"))
simMeasS2115ghz=sp.array(extract_dat(targetDirSimMeas+"S21.csv"))
simMeasS111ghz=sp.array(extract_dat(targetDirSimMeas+"simulatedWire0-1GHzS11.csv"))
simMeasS211ghz=sp.array(extract_dat(targetDirSimMeas+"simulatedWire0-1GHzS21.csv"))
simMeasS112ghz=sp.array(extract_dat(targetDirSimMeas+"simulatedWire1-2GHzS11.csv"))
simMeasS212ghz=sp.array(extract_dat(targetDirSimMeas+"simulatedWire1-2GHzS21.csv"))
simMeasS1105ghz=sp.array(extract_dat(targetDirSimMeas+"simulatedWire05GHzS11.csv"))
simMeasS2105ghz=sp.array(extract_dat(targetDirSimMeas+"simulatedWire05GHzS21.csv"))
simImpedanceRe=sp.array(extract_dat(targetDirSimMeas+"ReZ.csv"))
simImpedanceIm=sp.array(extract_dat(targetDirSimMeas+"ImZ.csv"))



####### Plotting Measurements ########



pl.plot(s11OriginalInput[:,0]/10**9, s11OriginalInput[:,1], 'k-', label="Ion Trap matched anode")
pl.plot(s11OriginalInputAnodeOpen[:,0]/10**9, s11OriginalInputAnodeOpen[:,1], 'b-', label="Ion Trap unmatched anode")
pl.plot(s11OriginalInputAnode[:,0]/10**9, s11OriginalInputAnode[:,1], 'r-', label="Anode matched ion trap")
pl.plot(s11OriginalInputAnodeOtherOpen[:,0]/10**9, s11OriginalInputAnodeOtherOpen[:,1], 'g-', label="Anode unmatched ion trap")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("LogMag $S_{11}$ (dB)", fontsize=16.0)
pl.legend(loc="upper left")
##pl.show()
pl.clf()

pl.plot(s11OriginalInput[:,0]/10**9, s11OriginalInput[:,2], 'k--', label="Ion Trap matched anode")
pl.plot(s11OriginalInputAnodeOpen[:,0]/10**9, s11OriginalInputAnodeOpen[:,2], 'b--', label="Ion Trap unmatched anode")
pl.plot(s11OriginalInputAnode[:,0]/10**9, s11OriginalInputAnode[:,2], 'r--', label="Anode matched ion trap")
pl.plot(s11OriginalInputAnodeOtherOpen[:,0]/10**9, s11OriginalInputAnodeOtherOpen[:,2], 'g--', label="Anode unmatched ion trap")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("Phase $S_{11}$ ", fontsize=16.0)
pl.legend(loc="upper left")
##pl.show()
pl.clf()

ax1=pl.subplot(111)
##pl.plot(s11OriginalInput[:,0]/10**9, s11OriginalInput[:,1], 'k-', label="Ion Trap matched anode")
##pl.plot(s11OriginalInputAnodeOpen[:,0]/10**9, s11OriginalInputAnodeOpen[:,1], 'b-', label="Ion Trap unmatched anode")
pl.plot(s11OriginalInputAnode[:,0]/10**9, s11OriginalInputAnode[:,1], 'r-', label="Anode matched ion trap")
pl.plot(s11OriginalInputAnodeOtherOpen[:,0]/10**9, s11OriginalInputAnodeOtherOpen[:,1], 'g-', label="Anode unmatched ion trap")
pl.ylabel("LogMag $S_{11}$ (dB)", fontsize=16.0)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
ax2=pl.twinx()

##pl.plot(s11OriginalInput[:,0]/10**9, s11OriginalInput[:,2], 'k--', label="Ion Trap matched anode")
##pl.plot(s11OriginalInputAnodeOpen[:,0]/10**9, s11OriginalInputAnodeOpen[:,2], 'b--', label="Ion Trap unmatched anode")
pl.plot(s11OriginalInputAnode[:,0]/10**9, s11OriginalInputAnode[:,2], 'r--', label="Anode matched ion trap")
pl.plot(s11OriginalInputAnodeOtherOpen[:,0]/10**9, s11OriginalInputAnodeOtherOpen[:,2], 'g--', label="Anode unmatched ion trap")
pl.ylabel("Phase $S_{11}$ ", fontsize=16.0)
pl.legend(loc="upper left")
##pl.show()
pl.clf()

fileMatchedMeasAllConnected = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/septazsMatchingResistors/allconnectedToCircuits/S2120DBATTENUATORS.S2P"
fileMatchedMeasAllOpen = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/septazsMatchingResistors/allOpen/S2120DBATTENUATORS.S2P"
fileMatchedMeasAnodeToIonTrapBox = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/septazsMatchingResistors/anodeToIonTrapBox/S2120DBATTENUATORS.S2P"
fileMatchedMeasIonTrapAndAnodeShorted = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/septazsMatchingResistors/ionTrapandAnodeShorted/S2120DBATTENUATORS.S2P"
fileMatchedMeasIonTrapAnodeMatched = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/septazsMatchingResistors/ionTrapAnodeMatched/S2120DBATTENUATORS.S2P"
fileMatchedMeasWithGrill = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/zsWithGrating/S21WITHATTENUATORS.S2P"
fileMatchedMeasWithGrill2GHz = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/17-12-13/ZSS21WITHATTENUATORS.S2P"
fileMatchedMeasWithNoGrill2GHz = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/17-12-13/S21WITHATTENTUATORSWITHOUTGRILL.S2P"
fileUnmatchedMeasNewPipe2GHz = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/09-09-15/ysSepta/S21FREQNOMATCHING.S2P"
fileMatchedMeasNewPipe2GHz = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/15-09-15/ysNew/S21FREQMATCHING.S2P"

####### Files for 2 pumping modules and 1 ZS ########

fileMatchedMeas2Pump1ZS = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/07-07-14/ZSWithPumpingModules/S21WIRETWOMODULESCOURSEFREQRANGE.S2P"
fileMatchedMeas2Pump1ZSNoGrill = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/15-07-14/zsNoGrillWire/S21FREQNOGATING.S2P"
fileListHighFreqRes = ["E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/07-07-14/ZSWithPumpingModules/hiFreqRes/"+str(i)+"-"+str(i+200)+"MHZ.S2P" for i in range(0,2000,200)]
temp=[]
for fileName in fileListHighFreqRes:
    tar=open(fileName)
    inputData=tar.readlines()
    tar.close()
    for line in inputData[6:]:
        bit=line.split("\t")
        temp.append(map(float,bit))
highResData=sp.array(temp)

####### Conversions to presentable ########

datAllConnectionsAdded=sp.array(readS21FromVNA(fileMatchedMeasAllConnected))
allConnectionsAddedImp=logImpFormula(logToLin(datAllConnectionsAdded[:,3]),1,310)
datOpen =sp.array(readS21FromVNA(fileMatchedMeasAllOpen))
allOpenImp=logImpFormula(logToLin(datOpen[:,3]),1,310)
allOpenImpImg=logImpFormulaImg(datOpen[:,4], 0.275, 310, datAllConnectionsAdded[:,0])
datAnodeToIonTrapBox=sp.array(readS21FromVNA(fileMatchedMeasAnodeToIonTrapBox))
allAnodeToIonTrapBoxImp=logImpFormula(logToLin(datAnodeToIonTrapBox[:,3]),1,310)
allAnodeToIonTrapBoxImpImg=logImpFormulaImg(datAnodeToIonTrapBox[:,4], 0.275, 310, datAllConnectionsAdded[:,0])
datIonTrapAndAnodeShorted=sp.array(readS21FromVNA(fileMatchedMeasIonTrapAndAnodeShorted))
allIonTrapAndAnodeShortedImp=logImpFormula(logToLin(datIonTrapAndAnodeShorted[:,3]),1,310)
allIonTrapAndAnodeShortedImpImg=logImpFormulaImg(datIonTrapAndAnodeShorted[:,4], 0.275, 310, datAllConnectionsAdded[:,0])
datIonTrapAnodeMatched=sp.array(readS21FromVNA(fileMatchedMeasIonTrapAnodeMatched))
allIonTrapAnodeMatchedImp=logImpFormula(logToLin(datIonTrapAnodeMatched[:,3]),1,310)
allIonTrapAnodeMatchedImpImg = logImpFormulaImg(datIonTrapAnodeMatched[:,4], 0.275, 310, datAllConnectionsAdded[:,0])
allConnectionsAddedGrill=sp.array(readS21FromVNA(fileMatchedMeasWithGrill))
allConnectionsAddedImpGrill=logImpFormula(logToLin(allConnectionsAddedGrill[:,3]),1,310)
allConnectionsAddedImpGrillImg = logImpFormulaImg(allConnectionsAddedGrill[:,4], 0.275, 310, allConnectionsAddedGrill[:,0])
allConnectionsAddedGrill2GHz=sp.array(readS21FromVNA(fileMatchedMeasWithGrill2GHz))
allConnectionsAddedImpGrill2GHz=logImpFormula(logToLin(allConnectionsAddedGrill2GHz[:,3]),1,310)
allConnectionsAddedGrill2GHzImg = logImpFormulaImg(allConnectionsAddedGrill2GHz[:,4], 0.275, 310, allConnectionsAddedGrill2GHz[:,0])
allConnectionsNoGrill2GHz=sp.array(readS21FromVNA(fileMatchedMeasWithNoGrill2GHz))
allConnectionsImpNoGrill2GHz=logImpFormula(logToLin(allConnectionsNoGrill2GHz[:,3]),1,310)
allConnectionsNoGrill2GHzImg = logImpFormulaImg(allConnectionsNoGrill2GHz[:,4], 0.275, 310, allConnectionsNoGrill2GHz[:,0])

MatchedMeas2Pump1ZS = sp.array(readS21FromVNA(fileMatchedMeas2Pump1ZS))
ImpMatchedMeas2Pump1ZS = logImpFormula(logToLin(MatchedMeas2Pump1ZS[:,3]),1,310)
impMatchedMeas2Pump1ZSImg = logImpFormulaImg(MatchedMeas2Pump1ZS[:,4], 0.215, 310, MatchedMeas2Pump1ZS[:,0])
MatchedMeas2Pump1ZSNoGrill = sp.array(readS21FromVNA(fileMatchedMeas2Pump1ZSNoGrill))
ImpMatchedMeas2Pump1ZSNoGrill = logImpFormula(logToLin(MatchedMeas2Pump1ZSNoGrill[:,3]),1,310)
ImpMatchedMeas2Pump1ZSNoGrillImg = logImpFormulaImg(MatchedMeas2Pump1ZSNoGrill[:,4], 0.215, 310, MatchedMeas2Pump1ZSNoGrill[:,0])
MatchedMeas2Pump1ZSHighRes = highResData
ImpMatchedMeas2Pump1ZSHighRes = logImpFormula(logToLin(MatchedMeas2Pump1ZSHighRes[:,3]),1,310)
ImpMatchedMeas2Pump1ZSHighResImg = logImpFormulaImg(MatchedMeas2Pump1ZSHighRes[:,4], 0.215, 310, MatchedMeas2Pump1ZSHighRes[:,0])

datUnmatchedMeasNewPipe2GHz=sp.array(readS21FromVNA(fileUnmatchedMeasNewPipe2GHz))
datMatchedMeasNewPipe2GHz=sp.array(readS21FromVNA(fileMatchedMeasNewPipe2GHz))
ImpUnmatchedNew = logImpFormula(logToLin(datUnmatchedMeasNewPipe2GHz[:,3]),1,310)
ImpMatchedNew = logImpFormula(logToLin(datMatchedMeasNewPipe2GHz[:,3]),1,310)

widths=sp.arange(1,5)
peaksListNoGrill = sg.find_peaks_cwt(allConnectionsImpNoGrill2GHz-allConnectionsImpNoGrill2GHz[0], widths)
peaksListGrill = sg.find_peaks_cwt(allConnectionsAddedImpGrill2GHz-allConnectionsAddedImpGrill2GHz[0], widths)
print len(MatchedMeas2Pump1ZS[:,0]), len(impMatchedMeas2Pump1ZSImg)


##for peak in peaksListNoGrill:
##    pl.plot(allConnectionsNoGrill2GHz[peak,0]/10**9, allConnectionsImpNoGrill2GHz[peak]-allConnectionsImpNoGrill2GHz[0],"ro")
##    print allConnectionsNoGrill2GHz[peak,0]/10**9, allConnectionsImpNoGrill2GHz[peak]-allConnectionsImpNoGrill2GHz[0]

##for peak in peaksListGrill:
##    pl.plot(allConnectionsNoGrill2GHz[peak,0]/10**9, allConnectionsAddedImpGrill2GHz[peak]-allConnectionsAddedImpGrill2GHz[0],"bo")
##    print allConnectionsNoGrill2GHz[peak,0]/10**9, allConnectionsAddedImpGrill2GHz[peak]-allConnectionsAddedImpGrill2GHz[0]


##pl.semilogy()
##pl.plot(datAllConnectionsAdded[:,0]/10**9, allConnectionsAddedImp-allConnectionsAddedImp[0], label="Measurements without RF insert all connected as operational")
##pl.plot(datOpen[:,0]/10**9, allOpenImp-allOpenImp[0], label="Measurements without RF insert all Open")
##pl.plot(datAnodeToIonTrapBox[:,0]/10**9, allAnodeToIonTrapBoxImp-allAnodeToIonTrapBoxImp[0], 'g-',label="Measurements without ZS without Grill, Old Design")
##pl.plot(datIonTrapAndAnodeShorted[:,0]/10**9, allIonTrapAndAnodeShortedImp-allIonTrapAndAnodeShortedImp[0], label="Measurements without RF insert Ion Trap/Anode grounded")
##pl.plot(datIonTrapAnodeMatched[:,0]/10**9, allIonTrapAnodeMatchedImp-allIonTrapAnodeMatchedImp[0], label="Measurements without RF insert Ion Trap/Anode matched load")
##pl.plot(allConnectionsAddedGrill[:,0]/10**9, allConnectionsAddedImpGrill-allConnectionsAddedImpGrill[0], label="Measurements of ZS with Grill, Old Design")
##pl.plot(allConnectionsNoGrill2GHz[:,0]/10**9, allConnectionsAddedImpGrill2GHz-allConnectionsAddedImpGrill2GHz[0], 'r-',label="Measurements with RF insert")
##pl.plot(allConnectionsNoGrill2GHz[:,0]/10**9, allConnectionsImpNoGrill2GHz-allConnectionsImpNoGrill2GHz[0], 'g-', label="Measurements without RF insert")
##pl.plot(MatchedMeas2Pump1ZS[:,0]/10**9, ImpMatchedMeas2Pump1ZS-ImpMatchedMeas2Pump1ZS[0], "k-", label="Measurement with ZS+2Pump, Old Design")
##pl.plot(MatchedMeas2Pump1ZS[:,0]/10**9, impMatchedMeas2Pump1ZSImg, "k-")
pl.plot(MatchedMeas2Pump1ZSHighRes[:,0]/10**9, ImpMatchedMeas2Pump1ZSHighRes-ImpMatchedMeas2Pump1ZSHighRes[0], "k-", label="Measurements ZS and 2 Pumping Ports Insert")
pl.plot(MatchedMeas2Pump1ZSNoGrill[:,0]/10**9, ImpMatchedMeas2Pump1ZSNoGrill-ImpMatchedMeas2Pump1ZSNoGrill[0], "b-", label="Measurements ZS and 2 Pumping Ports No Insert")
##pl.plot(MatchedMeas2Pump1ZSNoGrill[:,0]/10**9, ImpMatchedMeas2Pump1ZSNoGrillImg, "b-")
##pl.plot(simMeasS2115ghz[:,0], logImpFormula(logToLin(simMeasS2115ghz[:,1]),1,50), 'k-', label="Simulated Wire Measurements to 1.5GHz")
##pl.plot(simMeasS211ghz[:,0], logImpFormula(logToLin(simMeasS211ghz[:,1]),1,50), 'r-', label="Simulated Wire Measurements to 1GHz")
##pl.plot(simMeasS212ghz[:,0], logImpFormula(logToLin(simMeasS212ghz[:,1]),1,50), 'b-',label="Simulated Wire Measurements to 2GHz")
##pl.plot(simMeasS2105ghz[:,0], logImpFormula(logToLin(simMeasS2105ghz[:,1]),1,50), 'b-',label="Simulated Wire Measurements to 0.5GHz")
##pl.plot(simImpedanceRe[:,0], simImpedanceRe[:,1], label="Simulated Beam Impedance")
##pl.plot(datUnmatchedMeasNewPipe2GHz[:,0]/10**9, ImpUnmatchedNew, "r-")
pl.plot(datMatchedMeasNewPipe2GHz[:,0]/10**9, ImpMatchedNew-ImpMatchedNew[0], "r-", label="Measurements New Pipe, New Design")
pl.legend(loc="upper left")
##pl.axis([0,1,0,1400])
pl.xlim(0,1.5)
pl.ylim(-200,5000)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$\Re{}e(Z_{\parallel}$) ($\Omega$)", fontsize=16.0)
pl.show()
pl.clf()

##pl.plot(allConnectionsAddedGrill[:,0]/10**9, allConnectionsAddedImpGrillImg, label="Measurements with Grill")
##pl.plot(datOpen[:,0]/10**9, allOpenImpImg, label="Measurements without RF insert all Open")
##pl.plot(datAnodeToIonTrapBox[:,0]/10**9, allAnodeToIonTrapBoxImpImg, label="Measurements without Grill Anode to Ion Trap")
##pl.plot(datIonTrapAndAnodeShorted[:,0]/10**9, allIonTrapAndAnodeShortedImpImg, label="Measurements without RF insert Ion Trap/Anode grounded")
##pl.plot(datIonTrapAnodeMatched[:,0]/10**9, allIonTrapAnodeMatchedImpImg, label="Measurements without RF insert Ion Trap/Anode matched load")
pl.plot(allConnectionsNoGrill2GHz[:,0]/10**9, allConnectionsAddedGrill2GHzImg, 'r-',label="Measurements with RF insert")
pl.plot(allConnectionsNoGrill2GHz[:,0]/10**9, allConnectionsNoGrill2GHzImg, label="Measurements without RF insert")
##pl.plot(MatchedMeas2Pump1ZS[:,0]/10**9, MatchedMeas2Pump1ZSImg, "r-")
pl.plot(MatchedMeas2Pump1ZSHighRes[:,0]/10**9, ImpMatchedMeas2Pump1ZSHighResImg, "k-", label="Measurements ZS and 2 Pumping Ports Insert")
pl.plot(MatchedMeas2Pump1ZSNoGrill[:,0]/10**9, ImpMatchedMeas2Pump1ZSNoGrillImg, "b-", label="Measurements ZS and 2 Pumping Ports No Insert")
pl.legend(loc="lower left")
##pl.axis([0,1,0,1400])
pl.xlim(0.0,1.0)
pl.ylim(-600,300)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$\Im{}m(Z_{\parallel}$) ($\Omega$)", fontsize=16.0)
##pl.show()
pl.clf()

pl.plot(allConnectionsAddedGrill[:,0]/10**9, allConnectionsAddedImpGrillImg/(allConnectionsAddedGrill[:,0]/(C/6970)), label="Measurements with Grill")
##pl.plot(datOpen[:,0]/10**9, allOpenImpImg/(allConnectionsAddedGrill[:,0]/(C/6970)), label="Measurements without RF insert all Open")
##pl.plot(datAnodeToIonTrapBox[:,0]/10**9, allAnodeToIonTrapBoxImpImg/(allConnectionsAddedGrill[:,0]/(C/6970)), label="Measurements without Grill Anode to Ion Trap")
##pl.plot(datIonTrapAndAnodeShorted[:,0]/10**9, allIonTrapAndAnodeShortedImpImg/(allConnectionsAddedGrill[:,0]/(C/6970)), label="Measurements without RF insert Ion Trap/Anode grounded")
##pl.plot(datIonTrapAnodeMatched[:,0]/10**9, allIonTrapAnodeMatchedImpImg/(allConnectionsAddedGrill[:,0]/(C/6970)), label="Measurements without RF insert Ion Trap/Anode matched load")
##pl.plot(allConnectionsNoGrill2GHz[:,0]/10**9, allConnectionsAddedGrill2GHzImg/(allConnectionsAddedGrill[:,0]/(C/6970)), 'r-',label="Measurements with RF insert")
pl.plot(allConnectionsNoGrill2GHz[:,0]/10**9, allConnectionsNoGrill2GHzImg/(allConnectionsAddedGrill[:,0]/(C/6970)), label="Measurements without RF insert")
##pl.plot(MatchedMeas2Pump1ZS[:,0]/10**9, MatchedMeas2Pump1ZSImg/(allConnectionsAddedGrill[:,0]/(C/6970)), "r-")
pl.plot(MatchedMeas2Pump1ZSHighRes[:,0]/10**9, ImpMatchedMeas2Pump1ZSHighResImg/(MatchedMeas2Pump1ZSHighRes[:,0]/(C/6970)), "k-", label="Measurements ZS and 2 Pumping Ports Insert")
pl.plot(MatchedMeas2Pump1ZSNoGrill[:,0]/10**9, ImpMatchedMeas2Pump1ZSNoGrillImg/(allConnectionsAddedGrill[:,0]/(C/6970)), "b-", label="Measurements ZS and 2 Pumping Ports No Insert")
pl.legend(loc="upper right")
##pl.axis([0,1,0,1400])
pl.xlim(0.0,0.2)
##pl.ylim(-600,300)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$\Im{}m(Z_{\parallel}$) ($\Omega$)", fontsize=16.0)
##pl.show()
pl.clf()


ax1=pl.subplot(111)
pl.plot(MatchedMeas2Pump1ZS[:,0]/10**9, impMatchedMeas2Pump1ZSImg, "k-")
pl.plot(MatchedMeas2Pump1ZSNoGrill[:,0]/10**9, ImpMatchedMeas2Pump1ZSNoGrillImg, "b-")
ax2=pl.twinx()
pl.plot(MatchedMeas2Pump1ZS[:,0]/10**9, MatchedMeas2Pump1ZS[:,4], "r-")
pl.plot(MatchedMeas2Pump1ZSNoGrill[:,0]/10**9, MatchedMeas2Pump1ZSNoGrill[:,4], "g-", label="Measurements ZS and 2 Pumping Ports No Insert")
##pl.show()
pl.clf()

######## Probe Measurements #########

probeSeptaAllConnectedDepth10cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/zsSeptawithProbe/10CMPROBEOUT.S2P"
probeSeptaAllConnectedDepth15cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/zsSeptawithProbe/15CMPROBEOUT.S2P"
probeSeptaAllConnectedDepth20cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/zsSeptawithProbe/20CMPROBEOUT.S2P"
probeSeptaAllConnectedDepth25cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/zsSeptawithProbe/25CMPROBEOUT.S2P"
probeSeptaAllConnectedDepth30cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/zsSeptawithProbe/30CMPROBEOUT.S2P"
probeSeptaAllConnectedDepth36cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/zsSeptawithProbe/36CMPROBEOUT.S2P"


probeSeptaWithGrillBrute= "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/16-12-13/zsProbeWithGrill/TRANSMISSIONBOTHPORTS.S2P"
probeSeptaWithGrillMidDepth= "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/16-12-13/zsProbeWithGrill/TRANSMISSIONMIDDEPTH.S2P"
probeSeptaWithGrillModeratelyShallow= "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/16-12-13/zsProbeWithGrill/TRANSMISSIONMODERATELYSHALLOW.S2P"
probeSeptaWithGrillQuiteDeep= "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/16-12-13/zsProbeWithGrill/TRANSMISSIONQUITEDEEP.S2P"
probeSeptaWithGrillRightIn= "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/16-12-13/zsProbeWithGrill/TRANSMISSIONRIGHTIN.S2P"
probeSeptaWithGrillVeryShallow= "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/16-12-13/zsProbeWithGrill/TRANSMISSIONVERYSHALLOW.S2P"

datSeptaProbe10cm = sp.array(readS21FromVNA(probeSeptaAllConnectedDepth10cm))
datSeptaProbe15cm = sp.array(readS21FromVNA(probeSeptaAllConnectedDepth15cm))
datSeptaProbe20cm = sp.array(readS21FromVNA(probeSeptaAllConnectedDepth20cm))
datSeptaProbe25cm = sp.array(readS21FromVNA(probeSeptaAllConnectedDepth25cm))
datSeptaProbe30cm = sp.array(readS21FromVNA(probeSeptaAllConnectedDepth30cm))
datSeptaProbe36cm = sp.array(readS21FromVNA(probeSeptaAllConnectedDepth36cm))
datSeptaProbeGrillBrute = sp.array(readS21FromVNA(probeSeptaWithGrillBrute))
datSeptaProbeGrillMidDepth = sp.array(readS21FromVNA(probeSeptaWithGrillMidDepth))
datSeptaProbeGrillModeratelyShallow = sp.array(readS21FromVNA(probeSeptaWithGrillModeratelyShallow))
datSeptaProbeGrillQuiteDeep = sp.array(readS21FromVNA(probeSeptaWithGrillQuiteDeep))
datSeptaProbeGrillRightIn = sp.array(readS21FromVNA(probeSeptaWithGrillRightIn))
datSeptaProbeGrillVeryShallow = sp.array(readS21FromVNA(probeSeptaWithGrillVeryShallow))

probeS22SeptaTwoPumpDepth0cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S220MMPROTRUDING.S2P"
probeS22SeptaTwoPumpDepth5cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S225CMPROTRUDING.S2P"
probeS22SeptaTwoPumpDepth10cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S2210CMPROTRUDING.S2P"
probeS22SeptaTwoPumpDepth15cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S2215CMPROTRUDING.S2P"
probeS22SeptaTwoPumpDepth20cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S2220CMPROTRUDING.S2P"
probeS22SeptaTwoPumpDepth25cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S2225CMPROTRUDING.S2P"
probeS22SeptaTwoPumpDepth30cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S2230CMPROTRUDING.S2P"
probeS22SeptaTwoPumpDepth35cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S2235CMPROTRUDING.S2P"
probeS11SeptaTwoPumpDepth0cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S110MMPROTRUDING.S2P"
probeS11SeptaTwoPumpDepth5cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S115CMPROTRUDING.S2P"
probeS11SeptaTwoPumpDepth10cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S1110CMPROTRUDING.S2P"
probeS11SeptaTwoPumpDepth15cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S1115CMPROTRUDING.S2P"
probeS11SeptaTwoPumpDepth20cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S1120CMPROTRUDING.S2P"
probeS11SeptaTwoPumpDepth25cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S1125CMPROTRUDING.S2P"
probeS11SeptaTwoPumpDepth30cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S1130CMPROTRUDING.S2P"
probeS11SeptaTwoPumpDepth35cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/11-07-14/zsGrillWithProbes/S1135CMPROTRUDING.S2P"

probeS22SeptaTwoPumpNoGrillDepth0cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/14-07-14/zsNoGrillProbes/S22ZSTWOPUMPS0CMDEPTH.S2P"
probeS22SeptaTwoPumpNoGrillDepth5cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/14-07-14/zsNoGrillProbes/S22ZSTWOPUMPS5CMDEPTH.S2P"
probeS22SeptaTwoPumpNoGrillDepth10cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/14-07-14/zsNoGrillProbes/S22ZSTWOPUMPS10CMDEPTH.S2P"
probeS22SeptaTwoPumpNoGrillDepth15cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/14-07-14/zsNoGrillProbes/S22ZSTWOPUMPS15CMDEPTH.S2P"
probeS22SeptaTwoPumpNoGrillDepth20cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/14-07-14/zsNoGrillProbes/S22ZSTWOPUMPS20CMDEPTH.S2P"
probeS22SeptaTwoPumpNoGrillDepth25cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/14-07-14/zsNoGrillProbes/S22ZSTWOPUMPS25CMDEPTH.S2P"
probeS22SeptaTwoPumpNoGrillDepth30cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/14-07-14/zsNoGrillProbes/S22ZSTWOPUMPS30CMDEPTH.S2P"
probeS11SeptaTwoPumpNoGrillDepth0cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/14-07-14/zsNoGrillProbes/S11ZSTWOPUMPS0CMDEPTH.S2P"
probeS11SeptaTwoPumpNoGrillDepth5cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/14-07-14/zsNoGrillProbes/S11ZSTWOPUMPS5CMDEPTH.S2P"
probeS11SeptaTwoPumpNoGrillDepth10cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/14-07-14/zsNoGrillProbes/S11ZSTWOPUMPS10CMDEPTH.S2P"
probeS11SeptaTwoPumpNoGrillDepth15cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/14-07-14/zsNoGrillProbes/S11ZSTWOPUMPS15CMDEPTH.S2P"
probeS11SeptaTwoPumpNoGrillDepth20cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/14-07-14/zsNoGrillProbes/S11ZSTWOPUMPS20CMDEPTH.S2P"
probeS11SeptaTwoPumpNoGrillDepth25cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/14-07-14/zsNoGrillProbes/S11ZSTWOPUMPS25CMDEPTH.S2P"
probeS11SeptaTwoPumpNoGrillDepth30cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/14-07-14/zsNoGrillProbes/S11ZSTWOPUMPS30CMDEPTH.S2P"
probeS11SeptaTwoPumpNoGrillDepth35cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/14-07-14/zsNoGrillProbes/S11ZSTWOPUMPS35CMDEPTH.S2P"


datS22SeptaTwoPumpProbe0cm = sp.array(readS21FromVNA(probeS22SeptaTwoPumpDepth0cm))
datS22SeptaTwoPumpProbe5cm = sp.array(readS21FromVNA(probeS22SeptaTwoPumpDepth5cm))
datS22SeptaTwoPumpProbe10cm = sp.array(readS21FromVNA(probeS22SeptaTwoPumpDepth10cm))
datS22SeptaTwoPumpProbe15cm = sp.array(readS21FromVNA(probeS22SeptaTwoPumpDepth15cm))
datS22SeptaTwoPumpProbe20cm = sp.array(readS21FromVNA(probeS22SeptaTwoPumpDepth20cm))
datS22SeptaTwoPumpProbe25cm = sp.array(readS21FromVNA(probeS22SeptaTwoPumpDepth25cm))
datS22SeptaTwoPumpProbe30cm = sp.array(readS21FromVNA(probeS22SeptaTwoPumpDepth30cm))
datS22SeptaTwoPumpProbe35cm = sp.array(readS21FromVNA(probeS22SeptaTwoPumpDepth35cm))

datS11SeptaTwoPumpProbe0cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpDepth0cm))
datS11SeptaTwoPumpProbe5cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpDepth5cm))
datS11SeptaTwoPumpProbe10cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpDepth10cm))
datS11SeptaTwoPumpProbe15cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpDepth15cm))
datS11SeptaTwoPumpProbe20cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpDepth20cm))
datS11SeptaTwoPumpProbe25cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpDepth25cm))
datS11SeptaTwoPumpProbe30cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpDepth30cm))
datS11SeptaTwoPumpProbe35cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpDepth35cm))

datS22SeptaTwoPumpNoGrillProbe0cm = sp.array(readS21FromVNA(probeS22SeptaTwoPumpNoGrillDepth0cm))
datS22SeptaTwoPumpNoGrillProbe5cm = sp.array(readS21FromVNA(probeS22SeptaTwoPumpNoGrillDepth5cm))
datS22SeptaTwoPumpNoGrillProbe10cm = sp.array(readS21FromVNA(probeS22SeptaTwoPumpNoGrillDepth10cm))
datS22SeptaTwoPumpNoGrillProbe15cm = sp.array(readS21FromVNA(probeS22SeptaTwoPumpNoGrillDepth15cm))
datS22SeptaTwoPumpNoGrillProbe20cm = sp.array(readS21FromVNA(probeS22SeptaTwoPumpNoGrillDepth20cm))
datS22SeptaTwoPumpNoGrillProbe25cm = sp.array(readS21FromVNA(probeS22SeptaTwoPumpNoGrillDepth25cm))
datS22SeptaTwoPumpNoGrillProbe30cm = sp.array(readS21FromVNA(probeS22SeptaTwoPumpNoGrillDepth30cm))

datS11SeptaTwoPumpNoGrillProbe0cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpNoGrillDepth0cm))
datS11SeptaTwoPumpNoGrillProbe5cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpNoGrillDepth5cm))
datS11SeptaTwoPumpNoGrillProbe10cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpNoGrillDepth10cm))
datS11SeptaTwoPumpNoGrillProbe15cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpNoGrillDepth15cm))
datS11SeptaTwoPumpNoGrillProbe20cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpNoGrillDepth20cm))
datS11SeptaTwoPumpNoGrillProbe25cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpNoGrillDepth25cm))
datS11SeptaTwoPumpNoGrillProbe30cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpNoGrillDepth30cm))
datS11SeptaTwoPumpNoGrillProbe35cm = sp.array(readS21FromVNA(probeS11SeptaTwoPumpNoGrillDepth35cm))


depArray=sp.array([0.1,0.15,0.2,0.25,0.3,0.36])
freqArray=C/(4*depArray)

pl.plot(datSeptaProbe10cm[:,0]/10**9, datSeptaProbe10cm[:,1], label="Depth=10cm")
pl.plot(datSeptaProbe15cm[:,0]/10**9, datSeptaProbe15cm[:,1], label="Depth=15cm")
pl.plot(datSeptaProbe20cm[:,0]/10**9, datSeptaProbe20cm[:,1], label="Depth=20cm")
pl.plot(datSeptaProbe25cm[:,0]/10**9, datSeptaProbe25cm[:,1], label="Depth=25cm")
pl.plot(datSeptaProbe30cm[:,0]/10**9, datSeptaProbe30cm[:,1], label="Depth=30cm")
pl.plot(datSeptaProbe36cm[:,0]/10**9, datSeptaProbe36cm[:,1], label="Depth=36cm")
pl.plot(datSeptaProbeGrillVeryShallow[:,0]/10**9, datSeptaProbeGrillVeryShallow[:,1], "y--", label="Grill Depth=very shallow")
pl.plot(datSeptaProbeGrillModeratelyShallow[:,0]/10**9, datSeptaProbeGrillModeratelyShallow[:,1], "k--", label="Grill Depth=kind of shallow")
pl.plot(datSeptaProbeGrillBrute[:,0]/10**9, datSeptaProbeGrillBrute[:,1], "b--",label="Grill Depth=some")
pl.plot(datSeptaProbeGrillMidDepth[:,0]/10**9, datSeptaProbeGrillMidDepth[:,1], "r--", label="Grill Depth=mid")
pl.plot(datSeptaProbeGrillQuiteDeep[:,0]/10**9, datSeptaProbeGrillQuiteDeep[:,1], "g--", label="Grill Depth=Quite deep")
pl.plot(datSeptaProbeGrillRightIn[:,0]/10**9, datSeptaProbeGrillRightIn[:,1], "m--", label="Grill Depth=Right in")



##for entry in freqArray:
##    pl.axvline(entry/10**9)

pl.legend(loc="lower left")
##pl.show()
pl.clf()


####### Comparison Probe/Wire all connections #######

ax1=pl.subplot(111)

##pl.plot(datSeptaProbe10cm[:,0]/10**9, datSeptaProbe10cm[:,1], label="Depth=10cm")
##pl.plot(datSeptaProbe15cm[:,0]/10**9, datSeptaProbe15cm[:,1], label="Depth=15cm")
##pl.plot(datSeptaProbe20cm[:,0]/10**9, datSeptaProbe20cm[:,1], label="Depth=20cm")
##pl.plot(datSeptaProbe25cm[:,0]/10**9, datSeptaProbe25cm[:,1], label="Depth=25cm")
##pl.plot(datSeptaProbe30cm[:,0]/10**9, datSeptaProbe30cm[:,1], label="Depth=30cm")
##pl.plot(datSeptaProbe36cm[:,0]/10**9, datSeptaProbe36cm[:,1], label="Depth=36cm")
pl.plot(datSeptaProbeGrillVeryShallow[:,0]/10**9, datSeptaProbeGrillVeryShallow[:,1], label="Grill Depth=very shallow")
pl.plot(datSeptaProbeGrillModeratelyShallow[:,0]/10**9, datSeptaProbeGrillModeratelyShallow[:,1], label="Grill Depth=kind of shallow")
pl.plot(datSeptaProbeGrillBrute[:,0]/10**9, datSeptaProbeGrillBrute[:,1], label="Grill Depth=some")
pl.plot(datSeptaProbeGrillMidDepth[:,0]/10**9, datSeptaProbeGrillMidDepth[:,1], label="Grill Depth=mid")
pl.plot(datSeptaProbeGrillQuiteDeep[:,0]/10**9, datSeptaProbeGrillQuiteDeep[:,1], label="Grill Depth=Quite deep")
pl.plot(datSeptaProbeGrillRightIn[:,0]/10**9, datSeptaProbeGrillRightIn[:,1], label="Grill Depth=Right in")
pl.legend(loc="upper left")
pl.ylim(-1,0)
pl.ylabel("S$_{11}$ (dB)", fontsize=16.0)
pl.xlabel("Frequency (GHz)", fontsize=16.0)


ax2=pl.twinx()
##pl.plot(datAllConnectionsAdded[:,0]/10**9, allConnectionsAddedImp-allConnectionsAddedImp[0], "k-",label="Measurements without Grill")
##pl.plot(allConnectionsNoGrill2GHz[:,0]/10**9, allConnectionsImpNoGrill2GHz-allConnectionsImpNoGrill2GHz[0], label="Measurements without Grill 2GHz")
##pl.plot(allConnectionsAddedGrill[:,0]/10**9, allConnectionsAddedImpGrill-allConnectionsAddedImpGrill[0], label="Measurements with Grill")
pl.plot(allConnectionsNoGrill2GHz[:,0]/10**9, allConnectionsAddedImpGrill2GHz-allConnectionsAddedImpGrill2GHz[0], label="Measurements with Grill 2GHz")
##pl.legend(loc="lower left")
pl.ylabel("$\Re{}e(Z_{\parallel}$ ($\Omega$)", fontsize=16.0)
pl.xlim(0.1,0.15)
pl.ylim(0,400)
##pl.show()
pl.clf()

pl.plot(datSeptaProbe10cm[:,0]/10**9, datSeptaProbe10cm[:,7], label="Depth=10cm")
pl.plot(datSeptaProbe15cm[:,0]/10**9, datSeptaProbe15cm[:,7], label="Depth=15cm")
pl.plot(datSeptaProbe20cm[:,0]/10**9, datSeptaProbe20cm[:,7], label="Depth=20cm")
pl.plot(datSeptaProbe25cm[:,0]/10**9, datSeptaProbe25cm[:,7], label="Depth=25cm")
pl.plot(datSeptaProbe30cm[:,0]/10**9, datSeptaProbe30cm[:,7], label="Depth=30cm")
pl.plot(datSeptaProbe36cm[:,0]/10**9, datSeptaProbe36cm[:,7], label="Depth=36cm")
##pl.plot(datSeptaProbeGrillBrute[:,0]/10**9, datSeptaProbeGrillBrute[:,1], label="Grill Depth=some")
##pl.plot(datSeptaProbeGrillMidDepth[:,0]/10**9, datSeptaProbeGrillMidDepth[:,1], label="Grill Depth=mid")
##pl.plot(datSeptaProbeGrillModeratelyShallow[:,0]/10**9, datSeptaProbeGrillModeratelyShallow[:,1], label="Grill Depth=kind of shallow")
##pl.plot(datSeptaProbeGrillQuiteDeep[:,0]/10**9, datSeptaProbeGrillQuiteDeep[:,1], label="Grill Depth=Quite deep")
##pl.plot(datSeptaProbeGrillRightIn[:,0]/10**9, datSeptaProbeGrillRightIn[:,1], label="Grill Depth=Right in")
##pl.plot(datSeptaProbeGrillVeryShallow[:,0]/10**9, datSeptaProbeGrillVeryShallow[:,1], label="Grill Depth=very shallow")
##pl.plot(datS22SeptaTwoPumpProbe0cm[:,0]/10**9, datS22SeptaTwoPumpProbe0cm[:,7], label="Depth=0cm")
##pl.plot(datS22SeptaTwoPumpProbe5cm[:,0]/10**9, datS22SeptaTwoPumpProbe5cm[:,7], label="Depth=5cm")
##pl.plot(datS22SeptaTwoPumpProbe10cm[:,0]/10**9, datS22SeptaTwoPumpProbe10cm[:,7], label="Depth=10cm")
##pl.plot(datS22SeptaTwoPumpProbe15cm[:,0]/10**9, datS22SeptaTwoPumpProbe15cm[:,7], label="Depth=15cm")
##pl.plot(datS22SeptaTwoPumpProbe20cm[:,0]/10**9, datS22SeptaTwoPumpProbe20cm[:,7], label="Depth=20cm")
##pl.plot(datS22SeptaTwoPumpProbe25cm[:,0]/10**9, datS22SeptaTwoPumpProbe25cm[:,7], label="Depth=25cm")
##pl.plot(datS22SeptaTwoPumpProbe30cm[:,0]/10**9, datS22SeptaTwoPumpProbe30cm[:,7], label="Depth=30cm")
##pl.plot(datS22SeptaTwoPumpProbe35cm[:,0]/10**9, datS22SeptaTwoPumpProbe35cm[:,7], label="Depth=35cm")
##pl.plot(datS11SeptaTwoPumpProbe0cm[:,0]/10**9, datS11SeptaTwoPumpProbe0cm[:,1], label="Depth=0cm")
##pl.plot(datS11SeptaTwoPumpProbe5cm[:,0]/10**9, datS11SeptaTwoPumpProbe5cm[:,1], label="Depth=5cm")
##pl.plot(datS11SeptaTwoPumpProbe10cm[:,0]/10**9, datS11SeptaTwoPumpProbe10cm[:,1], label="Depth=10cm")
##pl.plot(datS11SeptaTwoPumpProbe15cm[:,0]/10**9, datS11SeptaTwoPumpProbe15cm[:,1], label="Depth=15cm")
##pl.plot(datS11SeptaTwoPumpProbe20cm[:,0]/10**9, datS11SeptaTwoPumpProbe20cm[:,1], label="Depth=20cm")
##pl.plot(datS11SeptaTwoPumpProbe25cm[:,0]/10**9, datS11SeptaTwoPumpProbe25cm[:,1], label="Depth=25cm")
##pl.plot(datS11SeptaTwoPumpProbe30cm[:,0]/10**9, datS11SeptaTwoPumpProbe30cm[:,1], label="Depth=30cm")
##pl.plot(datS11SeptaTwoPumpProbe35cm[:,0]/10**9, datS11SeptaTwoPumpProbe35cm[:,1], label="Depth=35cm")
##pl.plot(datS22SeptaTwoPumpNoGrillProbe0cm[:,0]/10**9, datS22SeptaTwoPumpNoGrillProbe0cm[:,7], label="Depth=0cm")
##pl.plot(datS22SeptaTwoPumpNoGrillProbe5cm[:,0]/10**9, datS22SeptaTwoPumpNoGrillProbe5cm[:,7], label="Depth=5cm")
##pl.plot(datS22SeptaTwoPumpNoGrillProbe10cm[:,0]/10**9, datS22SeptaTwoPumpNoGrillProbe10cm[:,7], label="Depth=10cm")
##pl.plot(datS22SeptaTwoPumpNoGrillProbe15cm[:,0]/10**9, datS22SeptaTwoPumpNoGrillProbe15cm[:,7], label="Depth=15cm")
##pl.plot(datS22SeptaTwoPumpNoGrillProbe20cm[:,0]/10**9, datS22SeptaTwoPumpNoGrillProbe20cm[:,7], label="Depth=20cm")
##pl.plot(datS22SeptaTwoPumpNoGrillProbe25cm[:,0]/10**9, datS22SeptaTwoPumpNoGrillProbe25cm[:,7], label="Depth=25cm")
##pl.plot(datS22SeptaTwoPumpNoGrillProbe30cm[:,0]/10**9, datS22SeptaTwoPumpNoGrillProbe30cm[:,7], label="Depth=30cm")
##pl.plot(datS11SeptaTwoPumpNoGrillProbe0cm[:,0]/10**9, datS11SeptaTwoPumpNoGrillProbe0cm[:,1], label="Depth=0cm")
##pl.plot(datS11SeptaTwoPumpNoGrillProbe5cm[:,0]/10**9, datS11SeptaTwoPumpNoGrillProbe5cm[:,1], label="Depth=5cm")
##pl.plot(datS11SeptaTwoPumpNoGrillProbe10cm[:,0]/10**9, datS11SeptaTwoPumpNoGrillProbe10cm[:,1], label="Depth=10cm")
##pl.plot(datS11SeptaTwoPumpNoGrillProbe15cm[:,0]/10**9, datS11SeptaTwoPumpNoGrillProbe15cm[:,1], label="Depth=15cm")
##pl.plot(datS11SeptaTwoPumpNoGrillProbe20cm[:,0]/10**9, datS11SeptaTwoPumpNoGrillProbe20cm[:,1], label="Depth=20cm")
##pl.plot(datS11SeptaTwoPumpNoGrillProbe25cm[:,0]/10**9, datS11SeptaTwoPumpNoGrillProbe25cm[:,1], label="Depth=25cm")
##pl.plot(datS11SeptaTwoPumpNoGrillProbe30cm[:,0]/10**9, datS11SeptaTwoPumpNoGrillProbe30cm[:,1], label="Depth=30cm")
##pl.plot(datS11SeptaTwoPumpNoGrillProbe35cm[:,0]/10**9, datS11SeptaTwoPumpNoGrillProbe35cm[:,1], label="Depth=35cm")
pl.legend(loc="upper left")
pl.ylabel("S$_{22}$ (dB)", fontsize=16.0)
pl.xlabel("Frequency (GHz)", fontsize=16.0)

ax2=pl.twinx()
pl.plot(datAllConnectionsAdded[:,0]/10**9, allConnectionsAddedImp-allConnectionsAddedImp[0], "k-",label="Measurements without Grill")
##pl.plot(allConnectionsNoGrill2GHz[:,0]/10**9, allConnectionsImpNoGrill2GHz-allConnectionsImpNoGrill2GHz[0], label="Measurements without Grill 2GHz")
##pl.plot(allConnectionsAddedGrill[:,0]/10**9, allConnectionsAddedImpGrill-allConnectionsAddedImpGrill[0], label="Measurements with Grill")
##pl.plot(allConnectionsNoGrill2GHz[:,0]/10**9, allConnectionsAddedImpGrill2GHz-allConnectionsAddedImpGrill2GHz[0], label="Measurements with Grill 2GHz")
##pl.plot(MatchedMeas2Pump1ZS[:,0]/10**9, ImpMatchedMeas2Pump1ZS-ImpMatchedMeas2Pump1ZS[0], "r-")
##pl.plot(MatchedMeas2Pump1ZSHighRes[:,0]/10**9, ImpMatchedMeas2Pump1ZSHighRes-ImpMatchedMeas2Pump1ZSHighRes[0], "k-")
##pl.plot(MatchedMeas2Pump1ZSNoGrill[:,0]/10**9, ImpMatchedMeas2Pump1ZSNoGrill-ImpMatchedMeas2Pump1ZSNoGrill[0], "b-", label="Measurements ZS and 2 Pumping Ports No Insert")
pl.legend(loc="lower left")
pl.ylabel("$\Re{}e(Z_{\parallel}$ ($\Omega$)", fontsize=16.0)
pl.xlim(0,1.0)
##pl.show()
pl.clf()

######## Pumping Module Wire Measurements #########

dirMeasPumpingModule = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/pumpingModuleWithWire/"

pumpModuleNoAtten=sp.array(readS21FromVNA(dirMeasPumpingModule+"WIREWITHOUTATTENUATORS.S2P"))
pumpModuleNoAttenData=logImpFormula(logToLin(pumpModuleNoAtten[:,3]),1,310)
pumpModuleAtten=sp.array(readS21FromVNA(dirMeasPumpingModule+"WIREWITHATTENUATORS.S2P"))
pumpModuleAttenData=logImpFormula(logToLin(pumpModuleAtten[:,3]),1,310)


probePumpingModule10cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/pumpingModuleWithProbe/10CMPROBEDEPTH.S2P"
probePumpingModule75cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/pumpingModuleWithProbe/7.5CMPROBEDEPTH.S2P"
probePumpingModule5cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/pumpingModuleWithProbe/5CMPROBEDEPTH.S2P"
probePumpingModuleWithoutDamping75cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/pumpingModuleWithProbe/7.5CMPROBEDEPTHWITHOUTDAMPING.S2P"
probePumpingModuleWithoutDamping10cm = "E:/PhD/1st_Year_09-10/Data/SeptaZSMeasurements/pumpingModuleWithProbe/10CMPROBEDEPTHWITHOUTDAMPING.S2P"

datprobePumpingModule10cm = sp.array(readS21FromVNA(probePumpingModule10cm))
datprobePumpingModule75cm = sp.array(readS21FromVNA(probePumpingModule75cm))
datprobePumpingModule5cm = sp.array(readS21FromVNA(probePumpingModule5cm))
datprobePumpingModuleNoDamping10cm = sp.array(readS21FromVNA(probePumpingModuleWithoutDamping75cm))
datprobePumpingModuleNoDamping75cm = sp.array(readS21FromVNA(probePumpingModuleWithoutDamping10cm))

ax1=pl.subplot(111)

##pl.plot(pumpModuleNoAtten[:,0]/10**9, pumpModuleNoAttenData)
##pl.plot(pumpModuleAtten[:,0]/10**9, pumpModuleAttenData)
pl.plot(pumpModuleNoAtten[:,0]/10**9, pumpModuleNoAtten[:,3], label="Measurements without Attenuators")
pl.plot(pumpModuleAtten[:,0]/10**9, pumpModuleAtten[:,3]+20, label="Measurements with Attenuators")
##pl.ylabel("$\Re{}e(Z_{\parallel}$ ($\Omega$)")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("S$_{21}$ (dB)", fontsize=16.0)
pl.legend(loc="lower left")

ax2=pl.twinx()
pl.plot(datprobePumpingModule10cm[:,0]/10**9,datprobePumpingModule10cm[:,1], "r--")
pl.plot(datprobePumpingModule75cm[:,0]/10**9,datprobePumpingModule75cm[:,1], "b--")
pl.plot(datprobePumpingModule5cm[:,0]/10**9,datprobePumpingModule5cm[:,1], "k--")
pl.plot(datprobePumpingModuleNoDamping10cm[:,0]/10**9,datprobePumpingModuleNoDamping10cm[:,1], "m--")
pl.plot(datprobePumpingModuleNoDamping75cm[:,0]/10**9,datprobePumpingModuleNoDamping75cm[:,1], "g--")
##pl.show()
pl.clf()
