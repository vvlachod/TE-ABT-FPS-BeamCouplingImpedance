import scipy as sp
import pylab as pl
import time, os, sys
import scipy.integrate as inte

spectra_directory = "E:/PhD/1st_Year_09-10/Data/Bunch_Spectra/"
spectra_list = [spectra_directory+i for i in os.listdir(spectra_directory)]

try:
    os.mkdir(spectra_directory+"tex_output/")
except:
    pass

for file_ent in spectra_list:
    data_file = open(file_ent, 'r+')
    data = data_file.readlines()
    data_file.close()

    ax1 = pl.subplot(111)
    pl.plot(data[:32000],data[32000:], 'b-')
    pl.xlabel('Frequency (Hz)')
    pl.ylabel("S(f) (dB)")
    if "BeforeRamp" in file_ent:
        text = "Before Ramp"
    elif "Ramp" in file_ent:
        text = "Ramp" 
    elif "FlatTop" in file_ent:
        text = "Flat Top"
    elif "Squeeze" in file_ent:
        text = "Squeeze"
    elif "Adjust" in file_ent:
        text = "Adjust"
    elif "StableBeams" in file_ent:
        text = "Stable Beams"


    for i in range(int(20.04*10**6),int(3.5*10**9),int(20.04*10**6)):
        if i==2.0024*10**9:
            pl.axvline(i,-60,10, color='r', label="Spectral line")
        else:
            pl.axvline(i,-60,10, color='r')
    pl.figtext(0.55,0.6, text, size=12.0, bbox={'facecolor':'white', 'pad':10})
    idea =file_ent.replace(spectra_directory, '')
    idea = idea.rstrip('.csv')
    pl.axis([0.8*10**9, 1.2*10**9, -60, 10])
    pl.savefig(spectra_directory+"tex_output/"+idea+'_1.0ghz.png')
    pl.savefig(spectra_directory+"tex_output/"+idea+'_1.0ghz.eps')
    pl.savefig(spectra_directory+"tex_output/"+idea+'_1.0ghz.pdf')
    pl.clf()
    
    lin_approx = []
    for i in range(32000, len(data)):
        lin_approx.append(10**(float(data[i])/40))
    pl.plot(data[:32000],lin_approx, 'b-', label="Meas. Phillipe, Themis")
    for i in range(int(2.4*10**6),int(3.5*10**9),int(20*10**6)):
        if i==2.0024*10**9:
            pl.axvline(i,-60,10, color='r', label="Spectral line")
        else:
            pl.axvline(i,-60,10, color='r')
    pl.xlabel('Frequency (Hz)')
    pl.ylabel("S(f)")
    pl.legend(loc="upper right")
    pl.axis([0.8*10**9, 1.2*10**9, 0, 1])
    pl.savefig(spectra_directory+"tex_output/"+idea+'_1.0ghz_spectra_comp.png')
    pl.clf()
