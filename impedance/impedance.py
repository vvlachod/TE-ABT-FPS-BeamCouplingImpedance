### Impedance Related Functions ###

""" Includes functions for VNA analysis, CST import """

import csv, time, os, sys
import scipy as sp
import numpy as np
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack
import mpl_toolkits.mplot3d as ToolKit3d

C = 299792458.0
Z0 = 377
c=299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
lDUT = 2.45
dAper = 0.045
rWire = 0.0005
lWire = 3.551
resCopper = 17.24*10.0**-9
resOuter = 1/(3.3*10.0**7)
ZChar = 60*sp.log(dAper/rWire)

def gaussProf(freq, bLength):
    """ Returns gaussian profile at frequency freq for a bunch of length bLength
    """
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2)

def cosProf(freq, bunLength):
    """ Returns cos^2 profile at frequency freq for a bunch of length bLength
    """
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2

def heatingValGauss(impArr, beamCur, bunSpac, bunLen):
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac/10**9,impArr[-1,0],impArr[-1,0]*10**9*bunSpac)
    splineFitHeatingImp = SplineFitImp(freqListHeating)
    heatingTotalPart = []
    for i in range(0,len(splineFitHeatingImp)):
        heatingTotalPart.append([freqListHeating[i], abs(2*splineFitHeatingImp[i]*(beamCur**2)*gaussProf(freqListHeating[i]*10.0**9, bunLen))])
    heatingTotalPart=sp.array(heatingTotalPart)
    cumHeating = []
    for i in range(0,len(heatingTotalPart)):
        cumHeating.append([heatingTotalPart[i,0],sum(heatingTotalPart[:i,1])])
    cumHeating=sp.array(cumHeating)
    return heatingTotalPart[:,1].sum(), heatingTotalPart, cumHeating

def heatingValGaussS2P(impArr, beamCur, bunSpac, bunLen):
    ### Expects frequency in Hz ###
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac,impArr[-1,0],impArr[-1,0]*bunSpac)
    splineFitHeatingImp = SplineFitImp(freqListHeating)
    heatingTotalPart = []
    for i in range(0,len(splineFitHeatingImp)):
        heatingTotalPart.append([freqListHeating[i], abs(2*splineFitHeatingImp[i]*(beamCur**2)*gaussProf(freqListHeating[i], bunLen))])
    heatingTotalPart=sp.array(heatingTotalPart)
    cumHeating = []
    for i in range(0,len(heatingTotalPart)):
        cumHeating.append([heatingTotalPart[i,0],sum(heatingTotalPart[:i,1])])
    cumHeating=sp.array(cumHeating)
    return heatingTotalPart[:,1].sum(), heatingTotalPart, cumHeating

def heatingValGaussRes(impArr, beamCur, bunSpac, bunLen):
    ### Expects frequency in Hz ###
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac,impArr[-1,0],impArr[-1,0]*bunSpac)
    splineFitHeatingImp = SplineFitImp(freqListHeating)
    heatingTotalPart = []
    for i in range(0,len(splineFitHeatingImp)):
        heatingTotalPart.append([freqListHeating[i], abs(2*splineFitHeatingImp[i]*(beamCur**2)*gaussProf(freqListHeating[i], bunLen))])
    heatingTotalPart=sp.array(heatingTotalPart)
    cumHeating = []
    for i in range(0,len(heatingTotalPart)):
        cumHeating.append([heatingTotalPart[i,0],sum(heatingTotalPart[:i,1])])
    cumHeating=sp.array(cumHeating)
    return heatingTotalPart[:,1].sum(), heatingTotalPart, cumHeating

def impTwoWire(sParaData):
    impData=logImpFormula(logToLin(sParaData[:,3]),1,180)
    normImp=[]
    for i in range(0,len(sParaData)):
        normImp.append([sParaData[i,0],(impData[i]*C)/(2*sp.pi*sParaData[i,0]*0.014**2)-(logImpFormula(logToLin(sParaData[0,3]),1,180)*C)/(2*sp.pi*sParaData[i,0]*0.014**2)])
    return sp.array(normImp)

def importDatSimMeas(file_tar):
    """ Function Imports simulated wire measurements from a
    comma-separated variable file, typically from HFSS or Maxwell """
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
    datStore = []
    for entry in tempDat[1:]:
        temp = map(float, entry.split(','))
        datStore.append(temp)

    return datStore

def analSingReSimMeas(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    """ Function returns real component of longitudinal impedance
    from simulated wire measurement """
    reStore = []
    temp = []
##    Z0 = 60*sp.log(1.27*apPipe/r_wire)
    Z0=50.0
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(-2*Z0/lenTot*sp.log(10**(data[i,j]/20))) 
        temp.append(tempLin)
    return temp

def concatArrRes(freqArr, impArr):
    temp = [[freqArr[i], impArr[i]] for i in range(0,len(freqArr))]
    return sp.array(temp)

def readS21FromVNA(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[6:]:
        temp=line.split("\t")
        outputData.append(map(float,temp))
    return outputData

def readS21FromCSV(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[3:]:
        temp=line.split(",")
        outputData.append(map(float,temp))
    return outputData

def fileImport(tarFile):
    """ Function imports measured parameters from resonant wire method macro
    on network analyser in file tarFile """
    readPart=open(tarFile, 'r+')
    datUnPro = readPart.readlines()
    readPart.close()
    freqDat=[]
    qLoaded=[]
    s21DB=[]
    for row in datUnPro[20:]:
        freqDat.append(float(row.split(",")[0]))
        qLoaded.append(float(row.split(",")[1]))
        s21DB.append(float(row.split(",")[2]))

    return sp.array(freqDat), sp.array(qLoaded), sp.array(s21DB)

def skinDepth(freq, cond):
    """ Returns skin depth for a material of conductivity cond at frequency freq
    """
    return (1/(sp.pi*freq*mu0*cond))**0.5

def resImpGet(targetFile):
    """ Function calculates real component of longitudinal impedance
    from resonant coaxial wire measurements using VNA macro, saved in file targetFile
    """
    freqList, qTotal, transCoeff = fileImport(targetFile)

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(c*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor

    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper)/attenCopper

    return freqList, zMeas/lDUT

def resImpGetFromFile(targetFile):

    freqList, qTotal, transCoeff = fileImport(targetFile)

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(c*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor


    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper)/attenCopper

    return freqList, zMeas/lDUT

def resImpGetFromFileTrans(targetFile):

    freqList, qTotal, transCoeff = fileImport(targetFile)

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(c*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor


    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper)/attenCopper

    return list(freqList), list(zMeas/lDUT)

def extract_dat(file_name):
    """ Imports data from a CST file exported file_name, pre-2013
    """
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def extract_dat2013(file_name):
    """ Imports data from a CST file exported file_name, post-2013
    """
    data = open(file_name, "r+")
    tar = data.readlines()
    temp=[]
    for row in tar[2:]:
        row = map(float, row.split())
        temp.append(row)
    data.close()

    return temp

def extract_dat2013_MultipleSweeps(file_name):
    data = open(file_name, "r+")
    tar = data.readlines()
    temp=[]
    bigTemp = []
    count=2
    while count<len(tar):
        try:
            row = map(float, tar[count].split())
            temp.append(row)
            count+=1
        except:
            bigTemp.append(temp)
            temp=[]
            count+=3
    data.close()
    bigTemp.append(temp)
    return bigTemp

def extract_dat2013LossMap(file_name):
    """ Imports data from a CST field map file exported file_name, post-2013
    """
    data = open(file_name, "r+")
    data.readline()
    data.readline()
    temp=[]
    for row in iter(data):
        row = map(float, row.split())
        temp.append(row)
    data.close()

    return temp

def splineFitImpFunc(impArr):
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListTemp = sp.linspace(10**6/10**9,impArr[-1,0],10000)
    splineFitImpFreq=SplineFitImp(freqListTemp)
    temp = []
    for i in range(0,len(freqListTemp)):
        temp.append([freqListTemp[i], splineFitImpFreq[i]])
    return sp.array(temp)

def splineFitImpFuncVariable(impArr):
    ### Takes an impedance profile and returns an array of spline fits offset by +/- 40MHz (to examine power loss between beam harmonics)
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListTemp = sp.linspace(10**6/10**9, impArr[0,-1], 10000)
    splineFitImpFreq=SplineFitImp(freqListTemp)
    listFreqVar = []
    for j in range(-40,45,5):
        temp = []
        for i in range(0,len(freqListTemp)):
            temp.append([freqListTemp[i]+(j/10.0**3), splineFitImpFreq[i]])
        listFreqVar.append(temp)
    return sp.array(listFreqVar)

def splineFitImpFuncHeatingVariable(impArr):
    ### Takes an impedance profile and returns an array of spline fits offset by +/- 40MHz (to examine power loss between beam harmonics) and with beam harmonic spread
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac/10**9,impArr[-1,0],impArr[-1,0]*10**9*bunSpac)
    splineFitImpFreq=SplineFitImp(freqListHeating)
    listFreqVar = []
    for j in range(-40,45,5):
        temp = []
        for i in range(0,len(freqListHeating)):
            temp.append([freqListHeating[i]+(j/10.0**3), splineFitImpFreq[i]])
        listFreqVar.append(temp)
    return sp.array(listFreqVar)

def meshGridCSTFieldMap(fieldArr):
    """ Generates a 3D cartesian mesh and field points for an imported loss map
    imported using extract_dat2013LossMap. Also outputs mesh length
    """
    xmin = sp.amin(fieldArr[:,0])
    xmax = sp.amax(fieldArr[:,0])
    xlen = len(set(fieldArr[:,0]))
    ymin = sp.amin(fieldArr[:,1])
    ymax = sp.amax(fieldArr[:,1])
    ylen = len(set(fieldArr[:,1]))
    zmin = sp.amin(fieldArr[:,2])
    zmax = sp.amax(fieldArr[:,2])
    zlen = len(set(fieldArr[:,2]))
    
##    print xmin, xmax, ymin, ymax, zmin, zmax

    (X,Y,Z) = sp.mgrid[xmin:xmax:xlen*1j, ymin:ymax:ylen*1j, zmin:zmax:zlen*1j]
    datStore = []
##    print fieldArr[253031,3]
##    print fieldArr[253032,3]
##    print fieldArr[253033,3]
##    print fieldArr[253034,3]
##    print fieldArr[253035,3]
##    print fieldArr[253036,3]
##    print fieldArr[253037,3]
##    print fieldArr[253038,3]
##    print fieldArr[253039,3]
##    print fieldArr[253040,3]
    for zCount in range(0,zlen):
        zTemp=[]
        for yCount in range(0,ylen):
            yTemp=[]
            for xCount in range(0,xlen):
##                print (zCount*(ylen*xlen)+yCount*ylen+xCount)
                yTemp.append(fieldArr[(zCount*(ylen*xlen)+yCount*xlen+xCount)-1,3])
##                if zCount*(ylen*xlen)+yCount*ylen+xCount > 253036:
##                    print zCount*(ylen*xlen)+yCount*ylen+xCount
##                print fieldArr[(zCount*zlen+yCount*ylen+xCount),:], zCount, zCount*zlen+yCount*ylen+xCount
##                print xlen, ylen, zlen, xCount, yCount, zCount, (zCount*(ylen+xlen)+yCount*ylen+xCount), fieldArr[(zCount*(ylen+xlen)+yCount*ylen+xCount),3]
            zTemp.append(yTemp)
        datStore.append(zTemp)
##        print zCount*(ylen*xlen)+yCount*ylen+xCount, zCount
    datPro = sp.array(datStore)

    return (X,Y,Z), datPro, [xlen, ylen, zlen]

def sParaRead(fileTar, lenDUT, col):
    """ returns S21 from an S2P file, with phase adjusted to not be
    rectified between -180/+180 degrees
    """
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        phase = row[col+1]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360    

        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        import_dat = [row[0], row[col], sp.radians(phase), -s21Pec]
        data.append(import_dat)
        last = phase

    return data


def impAnalysisSingle(fileTar, lenDUT, r_wire, r_pipe, Zc):
    """ Analyses a S2P file from measurement using single wire
    to give longitudinal impedance
    """
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        phase = row[4]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360


        import_dat = [row[0], -2*Zc*sp.log(linDat), -2*Zc*(sp.radians(phase)+s21Pec)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDUT), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDUT))]
        data.append(import_dat)
        last = phase

    return data

def impAnalysisTwo(fileTar, lenDut, r_wire, r_pipe, d_wire, Zc):
    """ Analyses a S2P file from measurement using two wires
    to give dipolar impedance
    """
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter = 0.0
    last = 1e8

    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDut/C
        phase = row[4]-counter
        if phase>last and (phase-last)>180:
            count+=1
            counter+=360
            pgase-=360
        import_dat = [row[0], -2*Zc*sp.log(linDat)*C/(2*sp.pi*row[0]*d_wire**2), -2*Zc*(sp.radians(phase)+s21Pec)*C/(2*sp.pi*row[0]*d_wire**2)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDut)*C/(row[0]*d_wire**2), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDut))*C/(row[0]*d_wire**2)]
        data.append(import_dat)
    return data

def impTransPara(dispWire, measImp, wireErr):
    """ Takes an array of single wire measurements measImp and associated wire displacements dispWire
    and calculates the total transverse impedance and longitudinal impedance from this
    """
    quadFunc = lambda p, x: p[0] + x * p[1] + x**2 * p[2]
    errFunc = lambda p, x, Z: (Z-quadFunc(p,x))

    totTrans = []
    longComp = []

    for i in range(0,len(measImp[0,:,1])):
        pinit = [1.0, -1.0, 1.0]
        ZerrRe = measImp[:,i,1]*wireErr
        pReal, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,1]), full_output = 0)
        pinit = [1.0, -1.0, 1.0]
        ZerrIm = measImp[:,i,2]*wireErr
        pImag, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,2]), full_output = 0)
        totTrans.append([pReal[2]*C/(2*sp.pi*measImp[0,i,0]), pImag[2]*C/(2*sp.pi*measImp[0,i,0])])
        longComp.append([pReal[0], pImag[0]])

    return longComp, totTrans

def impTransParaRes(dispWire, measImp, wireErr):
##### Takes an array of longitudinal measurements and returns the transverse impedance
    quadFunc = lambda p, x: p[0] + x * p[1] + x**2 * p[2]
    errFunc = lambda p, x, Z: (Z-quadFunc(p,x))

    totTrans = []
    longComp = []
    for i in range(0,len(measImp)):
        pinit = [1.0, -1.0, 1.0]
        ZerrRe = measImp[i,1:]*wireErr
        pReal, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[i,1:]), full_output = 0)
        totTrans.append(pReal[2]*C/(2*sp.pi*measImp[i,0]))
        longComp.append(pReal[0])

    return longComp, totTrans

def quadImp(totalTrans, dipTrans, axis):
    """ Takes the total transverse impedance and dipolar impedance and calculates the quadrupolar impedance,
    assuming top/bottom, left/right symmetry. Horizontal and Vertical orientation needed to use right formula
    """
    quadImp = []
    for i in range(0,len(totalTrans)):
        if axis == "horz":
            quadImp.append([totalTrans[i,1]+dipTrans[i,1],totalTrans[i,2]+dipTrans[i,2]])
        elif axis == "vert":
            quadImp.append([totalTrans[i,1]-dipTrans[i,1],totalTrans[i,2]-dipTrans[i,2]])
    return quadImp

def logToLin(data):
    """ converts log (power) data to lin data
    """
    return 10**(data/20)

def linToLog(data):
    """ converts lin data to log (power) data
    """
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    """ Takes lin S21 data and calculates longitudinal impedance using lumped impedance model
    """
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    """ Takes lin S21 data and calculates longitudinal impedance using the distributed impedance model
    """
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

def cumSumLoss1D(lossArr):
    """ Takes loss array and calculates total loss in one plane
    """
    sumArr = []
    for i in range(0,len(lossArr)):
        sumArr.append([lossArr[i,0],sp.sum(lossArr[:i,1])])
    return sp.array(sumArr)

def cumSumLoss3DInZ(lossArr, lengthZ):
    """ Takes lin S21 data and calculates longitudinal impedance using the distributed impedance model
    """
    sumArr= []
    for i in range(0,len(lossArr[:,0,0])):
##        print len(lossArr[:,0,0]), i
        sumArr.append(sp.sum(lossArr[:i,:,:]))
    return sp.array(sumArr)

def sumLoss3DInZ(lossArr, lengthZ):
    """ Takes loss array and returns 
    """
    sumArr= []
    for i in range(0,len(lossArr[:,0,0])):
##        print len(lossArr[:,0,0]), i
        sumArr.append(sp.sum(lossArr[i,:,:]))
    return sp.array(sumArr)

def cumSumLoss3DInZDiff(lossArr, axis):
    """ Takes 3D loss array and returns 1D array of sum of loss in a plane
    X - 1D plane perpendicular to XY cross section
    Y - 1D plane perpendicular to YZ cross section
    """
    sumArr= []
##    print axis
    if axis == "x" or axis == "X":
        print "X"
        for i in range(0,len(lossArr[0,0,:])):
            temp=[]
            for j in range(0,len(lossArr[:,0,0])):
                temp.append(sp.sum(lossArr[:j,:,i]))
            sumArr.append(temp)
    elif axis == "y" or axis == "Y":
        print "Y"
        for i in range(0,len(lossArr[0,:,0])):
            temp=[]
            for j in range(0,len(lossArr[:,0,0])):
                temp.append(sp.sum(lossArr[:j,i,:]))
            sumArr.append(temp)
    return sp.array(sumArr)


def imgImpFromSPara(inputData, lenDUT, Zc):
    """ Takes an S2P file and returns the imaginary impedance
    """
    data=[]
    for row in inputData:
        tmpDat = []
        count = 0
        counter=0.0
        last = 1e8
        freq_last = 0.0
        for line in row:
            s21Pec = 2*sp.pi*line[0]*lenDUT/C
            phase = line[4]-counter
            if phase > last and (phase-last)>180 and abs(freq_last-line[0])>0.3e8:
                count+=1
                counter+=360
                phase-=360
                freq_last = line[0]
            elif phase>last and (phase-last)>180 and abs(freq_last-line[0])<0.3e8:
                phase+=360
        
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDUT), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDUT))]
            tmpDat.append(-2*Zc*(sp.radians(phase)+s21Pec))
            last = phase
        data.append(tmpDat)
    return sp.array(data)


def fitQuadraticSquaresImp(sParaData, dispWire):
    """ Takes an array of S paramters from S2P files sParaData and wire displacements dispWire
    and returns longitudinal (real, imag), total transverse (real,imag) and accuracy of fit using a
    least squares quadratic fit
    """
    impLongDat=[]
    impTransDat=[]
    errors=[]
    quadFunc = lambda p, x: p[0] + x*p[1] + x**2*p[2]
    errFunc = lambda p, x, z: (z-quadFunc(p,x))
    impDatImag = imgImpFromSPara(sParaData, 3.45, 305)
    try:
        os.mkdir("E:/PhD/1st_Year_09-10/Data/CLICStripline/singleWireDisplace/plots")
    except:
        pass
    for i in range(0,len(sParaData[0])):
        fitDataReal = sp.array(logImpFormula(logToLin(sParaData[:,i,3]),1,305))
        initVal = [1.0,-0.6,0.7]
        out = op.leastsq(errFunc, initVal, args=(dispWire, fitDataReal), full_output=1)
        pReal, pFunc = out[0], out[1]
        out = op.leastsq(errFunc, initVal, args=(dispWire, impDatImag[:,i]), full_output = 1)
        pImag, pother = out[0], out[1]
        impLongDat.append([sParaData[0,i,0], pReal[0], pImag[0]])
##        print C/(2*sp.pi*sParaData[0,i,0])
        totTrans = [(pReal[2]*C)/(2*sp.pi*sParaData[0,i,0]), (pImag[2]*C)/(2*sp.pi*sParaData[0,i,0])]
##        print totTrans
        impTransDat.append([sParaData[0,i,0], totTrans[0], totTrans[1]])
        errors.append([pFunc, pother])
##        dispPlot = sp.linspace(dispWire[0]-0.005,dispWire[-1]+0.005,100)
##        pl.plot(dispWire,fitDataReal)
##        pl.plot(dispPlot, quadFunc(pReal,dispPlot))
##        pl.savefig("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/transMeasPostLS1/27-10-15/mkiHorzTrans/plotImp"+str(i)+".png")
        pl.clf()
        
    return sp.array(impLongDat), sp.array(impTransDat), sp.array(errors)

def errorPlottables(errJacobian, impData):
    """Takes a list of error Jacobians and impedance data as input. Outputs scaled values for statistical output
    """
##    print errJacobian
    errReal, errImag = errJacobian[:,0,2,2], errJacobian[:,1,2,2]
    errReal = sp.sqrt(errReal)*impData[:,1]/10.0**-6*(C/(2*sp.pi*impData[:,0]))
    errImag = sp.sqrt(errImag)*impData[:,2]/10.0**-6*(C/(2*sp.pi*impData[:,0]))
    return errReal, errImag

def errorfill(x, y, yerr, color=None, alpha_fill=0.2, ax=None, label=None):
    """ Plots a solid line and block filled colour error bars
    """
    ax = ax if ax is not None else pl.gca()
    if color is None:
        color = ax._get_lines.color_cycle.next()
    if np.isscalar(yerr) or len(yerr) == len(y):
        ymin = y - yerr
        ymax = y + yerr
    elif len(yerr) == 2:
        ymin, ymax = yerr
    ax.plot(x, y, color=color, label=label)
    ax.fill_between(x, ymax, ymin, color=color, alpha=alpha_fill)

def readAnalDat(fileDir):
    inputDat = open(fileDir, "r+")
    datUn = inputDat.readlines()
    inputDat.close()
    temp=[]
    for row in datUn:
        row = map(float, row.split("\t"))
        temp.append(row)

    return temp

def charImpSquare(wRad, AperRad):
    """ Returns characteristic impedance of a coaxial line of central conductor of radius wRad
    and two parallel plates of separation AperRad
    """    
    return 60*sp.log(1.27*AperRad/wRad)

def charImpTwoWire(wRad, wSep):
    """ Returns characteristic impedance of a coaxial line of two central conductors of radius wRad
    and separated by wSep
    """    
    return 120*sp.acosh(wSep/(2*wRad))

def hfssImport(fileTarget,rangeLim,startNo=1):
    """ Imports HFSS output in range startNo to rangeLim
    """        
    fileLink = open(fileTarget, "r+")
    fileDat = fileLink.readlines()
    fileLink.close()
    datTemp = []
    for line in fileDat[startNo:rangeLim]:
        temp = (line.split(","))
        temp[-1]=temp[-1].rstrip("\n")
        dat = map(float,temp)
        datTemp.append(dat)
    return datTemp

def hfssImagImp(freq, argMeas, lenDUT, Zc):
    """ calculates imaginary impedance from hfssImport
    """        
    data=[]
    count = 0
    counter=0.0
    last = -120
    for i in range(0,len(freq)):
        tmpDat = []
        freq_last = 10**7
        s21Pec = 2*sp.pi*freq[i]*10**9*lenDUT/C
        phase = argMeas[i]-counter
        if (phase-last)>180 and (abs(freq[i]*10**9-freq_last)>0.1*10.0**8):
            count+=1
            counter+=360
            phase-=360
            freq_last = freq[i]*10**9
            pass
        else:
            pass
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDUT), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDUT))]
        tmpDat.append(-2*Zc*(sp.radians(phase)+s21Pec))
        last = phase
##        print tmpDat
        data.append(tmpDat)
    return sp.array(data)

def CSTtoFuncComp(impList):
    """ Converts CST formatted output (from extract_dat2013) to format compatible with some other
    functions based around S2P input
    """
    temp = []
    for i in range(0,len(impList)-1):
        temp.append([impList[i][0],impList[i][1]])
    return sp.array(temp)

def bunchTrainProfTime(bunSpacing, trainSpacing, bunInTrain, trainsInMachine, blankBunSpaces, timeRes, bLength=1.0*10.0**-9):
    spaceCount = ((trainsInMachine*bunInTrain*bunSpacing)+(trainSpacing*(trainsInMachine))+bunSpacing/2+(bunInTrain*trainsInMachine*bLength)+bunSpacing*blankBunSpaces)/timeRes
    print 1/((trainsInMachine*bunInTrain*bunSpacing)+(trainSpacing*(trainsInMachine))+bunSpacing/2+(bunInTrain*trainsInMachine*bLength)+bunSpacing*blankBunSpaces)
    ampListTime = [0 for i in range(0,int(spaceCount))]
##    print spaceCount
    timing = []
    for i in range(0,len(ampListTime)):
        timing.append(i*timeRes)

    bunLimit = int((bLength)/timeRes)

    for i in range(0,trainsInMachine):
        for j in range(0,bunInTrain):
            midPoint = int(((j*bunSpacing)+(i*trainsInMachine*trainSpacing)+(i*bunInTrain*bunSpacing)+bLength/2)/timeRes)
    ##        print midPoint, bunLimit, i
            for k in range(midPoint-bunLimit, midPoint+bunLimit):
                try:
                    ampListTime[k] = gaussProfTime(abs(k-midPoint)*timeRes, bLength, 32)
                except:
    ##                print i, j, j
                    pass

    return timing, ampListTime

def bunchTrainProfFreq(bunSpacing, trainSpacing, bunInTrain, trainsInMachine, blankBunSpaces, freqRes, bLength=1.0*10.0**-9):
    timeSpace = 0.02*10.0**-9
    timing, timeSig = bunchTrainProfTime(bunSpacing, trainSpacing, bunInTrain, trainsInMachine, blankBunSpaces, timeSpace)
    N = len(timing)
    f = 1/timeSpace*sp.r_[0:(N/2)]/N
    n= len(f)
    linFreqSpec = sp.fft(timeSig)
    linFreqSpec = linFreqSpec[0:n]/sp.amax(linFreqSpec)
    return f, abs(linFreqSpec)

def minValIndex(arrMark, minValue=10.0):
    return sp.array([i for i in range(0,len(arrMark)) if arrMark[i]>minValue]) 

class ImpedanceModel:
    def __init__(self, tarDirect, modelName):
        self.modelName=modelName
        self.sourceDirect = tarDirect
        self.listDirContents = os.listdir(self.sourceDirect)
        self.lossMaps = []
        self.freqMaps = []
        try:
            self.longImpedance = CSTtoFuncComp(extract_dat2013(self.sourceDirect+"longitudinal-impedance-real.txt"))
        except:
            print "No relevant impedance file"

    def ReadLossFiles(self):
        for contentFile in self.listDirContents:
            if "f" in contentFile and "dft" not in contentFile:
                print contentFile
                freqStore = contentFile.rstrip(".txt")
                freqStore = freqStore.lstrip("f")
                if len(freqStore)==4:
                    self.freqMaps.append(float(freqStore)/1000)
                elif len(freqStore)==5:
                    self.freqMaps.append(float(freqStore)/10000)
                temp = sp.array(extract_dat2013LossMap(self.sourceDirect+contentFile))
                (x,y,z), fieldsPlot, lens = meshGridCSTFieldMap(temp)
                self.xMesh = x[0,0]
                self.yMesh = y[0,0]
                self.zMesh = z[0,0]
                self.lossMaps.append(fieldsPlot)
                self.xMeshLen = lens[0]
                self.yMeshLen = lens[1]
                self.zMeshLen = lens[2]

    def LossInZAxis(self):
        self.lossInZ = []
        for lossMap in self.lossMaps:
            self.lossInZ.append(sumLoss3DInZ(lossMap, self.zMeshLen))
        self.lossInZ=sp.array(self.lossInZ)

    def FindZeros(self, limit):
        self.zeroIndices = minValIndex(self.lossInZ[0], limit)

    def NormedLosses(self):
        self.normedLossMaps = []
        for lossMap in self.lossInZ:
            self.normedLossMaps.append(lossMap/sp.sum(lossMap))
        self.normedLossMaps = sp.array(self.normedLossMaps)

    def WeightedLosses(self):
        self.lossMapProf = []
        for i in range(0,len(self.normedLossMaps[0])):
            temp = 0
            for j in range(0,len(self.normedLossMaps)):
                temp+=self.normedLossMaps[j, i]*gaussProf(self.freqMaps[j]*10.0**9, 1.0*10.0**-9)**2
            self.lossMapProf.append(temp)
        self.lossMapProf=sp.array(self.lossMapProf)

    def CalcGaussPowLoss(self, bCur, bunSpacing, bunLength):
        self.powLossTot, self.freqPowLoss = heatingValGauss(self.longImpedance, bCur, bunSpacing, bunLength)
        self.cumPowLoss = []
        for i in range(0,len(self.freqPowLoss)):
            self.cumPowLoss.append([self.freqPowLoss[i,0], sp.sum(self.freqPowLoss[0:i,1])])
        self.cumPowLoss=sp.array(self.cumPowLoss)

    def BinnedLosses(self):
        self.LossInZAxis()
        self.NormedLosses()
        self.WeightedLosses()
        self.FindZeros(10**4)
        currentLowBound = 0
        currentHighBound = 1
        self.binnedLossMap = list(sp.zeros(len(self.lossMapProf)))
        for i in range(0,len(self.zeroIndices)-1):
            while self.zeroIndices[currentHighBound]==(self.zeroIndices[currentHighBound+1]-1) and (currentHighBound <len(self.zeroIndices)-2):
                currentHighBound+=1
            self.binnedLossMap[self.zeroIndices[i]]=sp.mean(self.lossMapProf[self.zeroIndices[currentLowBound]:self.zeroIndices[currentHighBound]])
            if self.zeroIndices[i]==self.zeroIndices[currentHighBound]:
                currentLowBound=currentHighBound+1
                currentHighBound=currentLowBound+1
        self.binnedLossMap = sp.array(self.binnedLossMap/sp.sum(self.binnedLossMap))/((self.zMesh[1]-self.zMesh[0])/1000)

    def PlotCumBinnedLosses(self):
        self.cumBinnedLoss = []
        for i in range(0, len(self.binnedLossMap)):
            self.cumBinnedLoss.append(sp.sum(self.binnedLossMap[:i]))
        self.cumBinnedLoss=sp.array(self.cumBinnedLoss/self.cumBinnedLoss[-1])
        pl.plot(self.zMesh, self.cumBinnedLoss*self.powLossTot, label=self.modelName)

    def PlotBinnedLosses(self):
        pl.plot(self.zMesh, self.binnedLossMap*self.powLossTot, label=self.modelName)

    def PlotImpedance(self):
        pl.plot(self.longImpedance[:,0], self.longImpedance[:,1], label=self.modelName)
                                
    def PlotPowLoss(self):
        pl.plot(self.freqPowLoss[:,0], self.freqPowLoss[:,1], label=self.modelName)
                                
    def PlotCumPowLossFreq(self):
        pl.plot(self.cumPowLoss[:,0], self.cumPowLoss[:,1], label=self.modelName)

##class ParticleBeam:
##    def __init__(self, bunchLength, bunchSpacing, bunPerTrain, trainPerBeam):

