""" Package for impedance evaluation.
Contains functions for import, evaluation of files from CST, HFSS.
Import and analysis of data from VNA with wire measurements
Field analysis
Beam spectra
Power loss calculations
"""
import csv, time, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack
import mpl_toolkits.mplot3d as ToolKit3d
