import csv
import scipy as sp
import pylab as pl

directory = "C:/Users/hday/Documents/PhD/1st_Year_09-10/Software/headtail/data_dump/comp_wake_non_lin/" #Directory of data
list_bt_sc_wake = [directory+"hdtl-bt-sc-short-wake.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_bt_sc_nonlin = [directory+"hdtl-bt-sc-short-nonlin.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_at_sc_wake = [directory+"hdtl-at-sc-short-wake.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_at_sc_nonlin = [directory+"hdtl-at-sc-short-nonlin.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]



value_desired = 10
big_bugger =[]
data_list_1 = []
for data_raw in list_bt_sc_wake:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    append_tar = []
    for row in linelist:
        temp = map(float, row.split())
        append_tar.append([temp[0],temp[value_desired]])
    data_list_1.append(append_tar)
    
bt_sc_wake = sp.array(data_list_1)



data_list_1 = []
for data_raw in list_bt_sc_nonlin:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    append_tar = []
    for row in linelist:
        temp = map(float, row.split())
        append_tar.append([temp[0],temp[value_desired]])
    data_list_1.append(append_tar)
    
bt_sc_nonlin = sp.array(data_list_1)
    



data_list_1 = []
for data_raw in list_at_sc_wake:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    append_tar = []
    for row in linelist:
        temp = map(float, row.split())
        append_tar.append([temp[0],temp[value_desired]])
    data_list_1.append(append_tar)


at_sc_wake=sp.array(data_list_1)


##
##data_list_1 = []
##for data_raw in list_at_sc_nonlin:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    append_tar = []
##    for row in linelist:
##        temp = map(float, row.split())
##        append_tar.append([temp[0],temp[value_desired]])
##    data_list_1.append(append_tar)

    
at_sc_nonlin = sp.array(data_list_1)



x_values = sp.array(range(1,31))


pl.plot(bt_sc_wake[14,:,0], bt_sc_wake[14,:,1], 'k-', label = "Below Transition - wake")                    # plot fitted curve
pl.plot(bt_sc_nonlin[14,:,0], bt_sc_nonlin[14,:,1],'b:', label = "Below Transition - nonlin")
pl.plot(at_sc_wake[14,:,0], at_sc_wake[14,:,1], 'r-', label = "Above Transition - wake")                    # plot fitted curve
##pl.plot(at_sc_nonlin[14,:,0], at_sc_nonlin[14,:,1],'r:', label = "Above Transition - nonlin")



pl.grid(linestyle="--", which = "major")
pl.xlabel("Time (ms)",fontsize = 16)                  #Label axes
pl.ylabel("Momentum Spread $\delta{}p/p$",fontsize = 16)
pl.title("", fontsize = 16)
##pl.legend(loc = "upper left")
##pl.axis([0,30,-0.5,1.5])
pl.savefig(directory+"time_evol_mom_spread"+".pdf")                  # Save fig for film
pl.show()
pl.clf()   
