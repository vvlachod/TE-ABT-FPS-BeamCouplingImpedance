import pylab as pl
import scipy as sp

C = 299792458.0
t_0 = 0.6*10**-9
data_points = 5000.0
sample_rate = 0.1/(t_0/data_points)
broadband_impedance = 1
n_bunches = 1368
p_bunch = 1.15*10**11
Q_part = 1.6*10**-19
f_rev = 2*10**7

I_b = p_bunch*Q_part*f_rev
power_tot = I_b**2#*n_bunches

circumference = 26659
spaces = int(circumference/(C/f_rev))

t=pl.r_[-40*t_0:40*t_0:1/sample_rate]
##print len(t)
omega = sample_rate
s = []
for i in t:
    if abs(i)<=t_0:
        s.append(sp.cos(2*sp.pi*0.25/t_0*i)**2)
    else:
        s.append(0)
s = sp.array(s)
N=len(t)
S=sp.fft(s)
f = sample_rate*sp.r_[0:(N/2)]/N
n=len(f)

##pl.plot(t,s)
##pl.axis([-1*10**-9, 1*10**-9, 0, 1.2])
##pl.xlabel("Time (s)", size="16")
##pl.ylabel("Current/Peak Current", size="16")
##pl.show()
##pl.clf()
##print f
f=f/10**9
curr = abs(S[0:n])/abs(S[0])
S=(abs(S[0:n]))**2/(abs(S[0]))**2

##
power_spec_db = 20*sp.log(abs(S[0:n])/abs(S[0])-10)
print power_spec_db
pl.semilogy()
##pl.plot(f,power_spec_db, label = "Power Spectrum")
pl.plot(f,S[0:n],label='Cos$^{2}$ fit')
##pl.plot(f,curr)
pl.axis([0,3,10**-20,1])
pl.xlabel('Frequency (GHz)')
pl.ylabel('Magnitude')
pl.legend(loc = "center right")
pl.show()
pl.clf()

##pl.plot(f/10**9,S[0:n])
##pl.axis([0,3,0,2])
##pl.show()
##pl.clf()


total=0.0
convolution_cos = []
running_total_cos = []

print f[1]-f[0]
for i in range(0,len(f)-1):
##    j = (float(f[i])+((float(f[1])-float(f[0]))/2))/10**9
    j = float(f[i])#/10**9

    if j<1.8: #and (j*1000)%40 == 0.0:
##        print j
        convolution_cos.append([j,power_tot*2*broadband_impedance*S[i]])
        total+=power_tot*2*broadband_impedance*S[i]*3.55/2.7
        running_total_cos.append(total)
##        print total
##for i in range(0,len(convolution_cos)):
##    total+=convolution_cos[i][1]

convolution_cos=sp.array(convolution_cos)

##pl.plot(convolution_imp[:,0], convolution_imp[:,1], label = 'Measured Spectrum Power Loss')
##pl.plot(convolution_cos[:,0], convolution_cos[:,1], label = 'Cos$^{2}$ Spectrum Power Loss')
##pl.legend()
##pl.xlabel('Frequency (GHz)')
##pl.ylabel('Power Loss (W/m)')
##pl.show()
##pl.clf()


##pl.plot(convolution_imp[:,0], running_total, label = 'Measured Spectrum Power Loss')
##pl.plot(convolution_cos[:,0], running_total_cos, 'k-',label = 'Cos$^{2}$ Spectrum Power Loss')
##pl.legend(loc='upper left')
##pl.xlabel('Frequency (GHz)')
##pl.ylabel('Integrated Power Loss (W/m)')
##pl.axis([0,2,0,120])
##pl.show()
##pl.clf()

##print running_total, running_total_cos
print  total, total*(float(n_bunches)/spaces)
