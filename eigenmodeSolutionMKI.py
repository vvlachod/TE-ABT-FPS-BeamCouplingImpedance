import scipy as sp
import pylab as pl
import time, csv, sys, os
import matplotlib.pyplot as plt
import scipy.integrate as inte

########################## Imported Profile ##################################

start = time.time()

#######   SPS parameters for LHC type beam  #######

C = 299792458.0
circ=6911.0

def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def gauss_prof(dist, sigma, order):
    return sp.e**(-order*(dist/(2*sigma))**2)

def para_prof(dist, bunch_length):
    return 1-(2*dist/bunch_length)**2

def cos_prof(dist, bunch_length):
    return sp.cos(sp.pi/bunch_length*dist)**2

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = data.readlines()
    data.close()
    temp=[]
    for row in tar[1:]:
        row = map(float, row.split(","))
        temp.append(row[1])
    return temp

top_directory = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/hfssModel/testModes/"
dirList = [top_directory+direct for direct in os.listdir(top_directory)]

modeProperties = []

for entry in dirList:
    freq = extract_dat(entry+"/freq.csv")
    q = extract_dat(entry+"/q.csv")
    roverqLong = extract_dat(entry+"/roverqLong.csv")
    roverqTransYEz = extract_dat(entry+"/roverqYEz.csv")
    roverqTransYEy = extract_dat(entry+"/roverqYEy.csv")
    roverqTransXEz = extract_dat(entry+"/roverqXEz.csv")
    roverqTransXEx = extract_dat(entry+"/roverqXEx.csv")
    modeProperties.append([[freq[0],q[0],roverqLong[0]],[freq[0],q[0],roverqTransYEz[0]],[freq[0],q[0],roverqTransYEy[0]],[freq[0],q[0],roverqTransXEz[0]],[freq[0],q[0],roverqTransXEx[0]]])
    
print modeProperties

freqList = sp.linspace(10**6,2*10**9,2000)

impLongCollective=[]
impTranYEzCollective=[]
impTranYEyCollective=[]
impTranXEzCollective=[]
impTranXExCollective=[]
for i in range(0,len(freqList)):
    impTemp=0.0
    for entry in modeProperties:
        impTemp+=Z_bb(freqList[i], entry[0])
    impLongCollective.append(impTemp)
    impTemp=0.0
    for entry in modeProperties:
        impTemp+=Z_bb(freqList[i], entry[1])
    impTranYEzCollective.append(impTemp)
    impTemp=0.0
    for entry in modeProperties:
        impTemp+=Z_bb(freqList[i], entry[2])
    impTranYEyCollective.append(impTemp)
    impTemp=0.0
    for entry in modeProperties:
        impTemp+=Z_bb(freqList[i], entry[3])
    impTranXEzCollective.append(impTemp)
    impTemp=0.0
    for entry in modeProperties:
        impTemp+=Z_bb(freqList[i], entry[4])
    impTranXExCollective.append(impTemp)

pl.semilogy()
pl.plot(freqList, impLongCollective)
pl.show()
pl.clf()

pl.semilogy()
pl.plot(freqList, impTranYEzCollective)
pl.show()
pl.clf()

pl.semilogy()
pl.plot(freqList, impTranYEyCollective)
pl.show()
pl.clf()

pl.semilogy()
pl.plot(freqList, impTranXEzCollective)
pl.show()
pl.clf()

pl.semilogy()
pl.plot(freqList, impTranXExCollective)
pl.show()
pl.clf()


print time.time()-start
