import csv, os, sys, time, datetime
import scipy as sp
import pylab as pl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.patches as mpatches
import scipy.optimize as op

def peak_finder(xValues, yValues):
    listPeaks=[]
    pastHighestY = 0.0
    pastHighestX = 0.0
    i=0
    while i <len(yValues):
        if yValues[i]>pastHighestY:
            pastHighestY=yValues[i]
            pastHighestX=i
            i+=1
        elif yValues[i]<pastHighestY:
            listPeaks.append([pastHighestX, xValues[pastHighestX], yValues[pastHighestX]])
            pastHighestY=0.0
            while yValues[i+1]<yValues[i]:
                i+=1
            pastHighestX=i
        else:
            i+=1

    return listPeaks

def extractTimberDataMKI(fileTar):
    fileHead = open(fileTar, "r+")
    dataAll = fileHead.readlines()
    fileHead.close()

    segmentedData = []
    temp=[]
    for entry in dataAll:
        if entry.isspace():
            segmentedData.append(temp)
            temp=[]
        else:
            temp.append(entry)
    segmentedData.append(temp)
    segmentedData.pop(0)
    dataArray = []
    
    count=0
    for dataBlock in segmentedData:
        temp=[]
        count+=1
        for entry in dataBlock[1:-1]:
            stringTemp=entry.split(',')
            if stringTemp[1]=="\n":
                pass
            else:
                delimiter=stringTemp[0][-4:]
                temp.append([mdates.strpdate2num("%Y-%m-%d %H:%M:%S")(stringTemp[0].replace(delimiter,"")), float(stringTemp[1])])
        dataArray.append(temp)

    return dataArray

def stacker(dataList, targetNo):
    dataReturn=[]
    for entry in dataList:
        for i in range(0,len(entry[targetNo])):
            if len(entry[targetNo])==0:
                pass
            else:
                dataReturn.append(entry[targetNo][i])
    return dataReturn


####### TS1 2012 MKI IP8 #######
targetDirect = "E:/TimberData/MKITempAndPressure/21-04-2012 to 28-04-2012 Beam 2/"
####### TS1 2012 MKI IP2 #######
##targetDirect = "E:/TimberData/MKITempAndPressure/21-04-2012 to 28-04-2012 Beam 1/"
####### TS2 2012 MKI IP8 #######
##targetDirect = "E:/TimberData/MKITempAndPressure/16-06-2012 to 01-07-2012 Beam 2/"
####### TS2 2012 MKI IP2 #######
##targetDirect = "E:/TimberData/MKITempAndPressure/16-06-2012 to 01-07-2012 Beam 1/"
####### TS3 2012 MKI IP8 #######
##targetDirect = "E:/TimberData/MKITempAndPressure/15-09-2012 to 23-09-2012/"
####### TS3 2012 MKI IP2 #######
##targetDirect = "E:/TimberData/MKITempAndPressure/15-09-2012 to 23-09-2012 beam 1/"
listFiles = [targetDirect+str(result) for result in os.listdir(targetDirect)]

dataStore = []
for fileTar in listFiles:
    dataStore.append(extractTimberDataMKI(fileTar))
                    



mki8aPress = sp.array(stacker(dataStore,0))
mki8aTempMagUp = sp.array(stacker(dataStore,1))
mki8aTempMagDown = sp.array(stacker(dataStore,2))
mki8aTempTubeUp = sp.array(stacker(dataStore,3))
mki8aTempTubeDown = sp.array(stacker(dataStore,4))
mki8bPress = sp.array(stacker(dataStore,5))
mki8bTempMagUp = sp.array(stacker(dataStore,6))
mki8bTempMagDown = sp.array(stacker(dataStore,7))
mki8bTempTubeUp = sp.array(stacker(dataStore,8))
mki8bTempTubeDown = sp.array(stacker(dataStore,9))
mki8cPress = sp.array(stacker(dataStore,10))
mki8cTempMagUp = sp.array(stacker(dataStore,11))
mki8cTempMagDown = sp.array(stacker(dataStore,12))
mki8cTempTubeUp = sp.array(stacker(dataStore,13))
mki8cTempTubeDown = sp.array(stacker(dataStore,14))
mki8dPress = sp.array(stacker(dataStore,15))
mki8dTempMagUp = sp.array(stacker(dataStore,16))
mki8dTempMagDown = sp.array(stacker(dataStore,17))
mki8dTempTubeUp = sp.array(stacker(dataStore,18))
mki8dTempTubeDown = sp.array(stacker(dataStore,19))


ax1=pl.subplot(111)

pl.semilogy()
pl.plot_date(mki8aPress[:,0], mki8aPress[:,1], 'k--', label="MKI8a")
pl.plot_date(mki8bPress[:,0], mki8bPress[:,1], 'b--', label="MKI8b")
pl.plot_date(mki8cPress[:,0], mki8cPress[:,1], 'g--', label="MKI8c")
pl.plot_date(mki8dPress[:,0], mki8dPress[:,1], 'r--', label="MKI8d")
pl.ylabel("Vacuum Pressure (mbar)", fontsize=16.0)
pl.legend()
ax2 = pl.twinx()

##pl.plot(mdates.num2date(mki8aTempMagUp[:,0]), mki8aTempMagUp[:,1])
##pl.plot(mdates.num2date(mki8aTempMagDown[:,0]), mki8aTempMagDown[:,1])
##pl.plot(mdates.num2date(mki8aTempTubeUp[:,0]), mki8aTempTubeUp[:,1])
##pl.plot(mdates.num2date(mki8aTempTubeDown[:,0]), mki8aTempTubeDown[:,1])
##pl.plot(mdates.num2date(mki8bTempMagUp[:,0]), mki8bTempMagUp[:,1])
##pl.plot(mdates.num2date(mki8bTempMagDown[:,0]), mki8bTempMagDown[:,1])
##pl.plot(mdates.num2date(mki8bTempTubeUp[:,0]), mki8bTempTubeUp[:,1])
##pl.plot(mdates.num2date(mki8bTempTubeDown[:,0]), mki8bTempTubeDown[:,1])
##pl.plot(mdates.num2date(mki8cTempMagUp[:,0]), mki8cTempMagUp[:,1])
##pl.plot(mdates.num2date(mki8cTempMagDown[:,0]), mki8cTempMagDown[:,1])
##pl.plot(mdates.num2date(mki8cTempTubeUp[:,0]), mki8cTempTubeUp[:,1])
##pl.plot(mdates.num2date(mki8cTempTubeDown[:,0]), mki8cTempTubeDown[:,1])
##pl.plot(mdates.num2date(mki8dTempMagUp[:,0]), mki8dTempMagUp[:,1])
##pl.plot(mdates.num2date(mki8dTempMagDown[:,0]), mki8dTempMagDown[:,1])
##pl.plot(mdates.num2date(mki8dTempTubeUp[:,0]), mki8dTempTubeUp[:,1])
##pl.plot(mdates.num2date(mki8dTempTubeDown[:,0]), mki8dTempTubeDown[:,1])
pl.ylabel("Temperature ($^{\circ}$C)")
##pl.show()
pl.clf()

pl.semilogy()
pl.plot_date(mki8aPress[:,0], mki8aPress[:,1], 'k--', label="MKI8a")
pl.plot_date(mki8bPress[:,0], mki8bPress[:,1], 'b--', label="MKI8b")
pl.plot_date(mki8cPress[:,0], mki8cPress[:,1], 'g--', label="MKI8c")
pl.plot_date(mki8dPress[:,0], mki8dPress[:,1], 'r--', label="MKI8d")
pl.ylabel("Vacuum Pressure (mbar)", fontsize=16.0)
pl.ylim((10**-11,2*10**-10))
pl.legend()
pl.show()
pl.clf()
