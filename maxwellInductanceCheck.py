import csv, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op

mu0 = 4*sp.pi*10.0**-7
muCopper = 1.0
muDoubleCopper = 1.0
condCopper = 1.0*10.0**7
rIn = 0.0005
rOut = 0.010
lenCable=0.0

class ClosestDict(dict):
    def get(self, key):
        key=min(self.iterkeys(), key=lambda x:abs(x-key))
        return dict.get(self,key)

directXToT = ClosestDict({0.0: 1.0, 0.5: 0.9998, 0.6: 0.9997, 0.7:0.9994, 0.8:0.9989,
              0.9:0.9983, 1.0:0.9974, 1.1:0.9962, 1.2:0.9946, 1.3:0.9927,
              1.4:0.9902, 1.5:0.9871, 1.6:0.9834, 1.7:0.9790, 1.8:0.9739,
              1.9:0.9680, 2.0:0.9611, 2.2:0.9448, 2.4:0.9248, 2.6:0.9013,
              2.8:0.8745, 3.0:0.8452, 3.2:0.8140, 3.4:0.7818, 3.6:0.7493,
              3.8:0.7173, 4.0:0.6863, 4.2:0.6568, 4.4:0.6289, 4.6:0.6028,
              4.8:0.5785, 5.0:0.556, 5.2:0.5351, 5.4:0.5157, 5.6:0.4976,
              5.8:0.4809, 6.0:0.4652, 6.2:0.4506, 6.4:0.4368, 6.6:0.4239,
              6.8:0.4117, 7.0:0.4002, 7.2:0.3893, 7.4:0.3790, 7.6:0.3692,
              7.8:0.3599, 8.0:0.3511, 8.2:0.3426, 8.4:0.3346,8.6:0.3269,
              8.8:0.3196, 9.0:0.3126, 9.2:0.3058, 9.4:0.2994, 9.6:0.2932,
              9.8:0.2873, 10.0:0.2816, 10.5:0.2682, 11.0:0.2562, 11.5:0.2452,
              12.0:0.2350, 12.5:0.2257, 13.0:0.2170, 13.5:0.2090, 14.0:0.2016,
              14.5:0.1947, 15.0:0.1882, 16.0:0.1765, 17.0:0.1661, 18.0:0.1589,
              19.0:0.1487, 20.0:0.1413, 21.0:0.1346, 22.0:0.1285, 23.0:0.1229,
              24.0:0.1178, 25.0:0.1131, 26.0:0.1087, 28.0:0.1010, 30.0:0.942,
              32.0:0.0884, 34.0:0.0832, 36.0:0.0785, 38.0:0.0744, 40.0:0.0707,
              42.0:0.0673, 44.0:0.0643, 46.0:0.0615, 48.0:0.0589, 50.0:0.0566,
              60.0:0.0471, 70.0:0.0404, 80.0:0.0354, 90.0:0.0314, 100.0:0.0283
              })

def fileInductRead(targetFile):
    fileRead=open(targetFile, 'r+')

    listData = fileRead.readlines()
    fileRead.close()
    listCurrents = []
    listStoredEnergyElectric=[]
    listStoredEnergyMagnetic=[]
    for entry in listData:
        if entry.startswith("Total current (integral J ds)  "):
            tidbit=entry.replace("Total current (integral J ds)                    = ","")
            listCurrents.append(float(tidbit.replace(" [A]", "")))
        elif entry.startswith("Stored energy/unit length (integral A.J/2 ds)"):
            tidbit=entry.replace("Stored energy/unit length (integral A.J/2 ds)    = ", "")
            listStoredEnergyElectric.append(float(tidbit.replace(" [J/mm]", ""))*10.0**3)
        elif entry.startswith("Stored energy/unit length (integral B.H/2 ds) "):
            tidbit=entry.replace("Stored energy/unit length (integral B.H/2 ds)    = ", "")
            listStoredEnergyMagnetic.append(float(tidbit.replace(" [J/mm]", ""))*10.0**3)
        else:
            pass

    tempCurrents = []
    tempStoredEnergyElectric=[]
    tempStoredEnergyMagnetic=[]

    for i in range(0,len(listStoredEnergyMagnetic)-1,2):
        tempCurrents.append(listCurrents[i]+listCurrents[i+1])
        tempStoredEnergyElectric.append((listStoredEnergyElectric[i]**2+listStoredEnergyElectric[i+1]**2)**0.5)
        tempStoredEnergyMagnetic.append((listStoredEnergyMagnetic[i]**2+listStoredEnergyMagnetic[i+1]**2)**0.5)


    arrCurrents=sp.array(tempCurrents)
    arrStoredEnergyElectric=sp.array(tempStoredEnergyElectric)
    arrStoredEnergyMagnetic=sp.array(tempStoredEnergyMagnetic)
    listInductances = []
##    print arrStoredEnergyMagnetic

    #Calculate inductances using U=0.5 I^2 L U and I multiplied by 4 for quarter geometry

    geoFact=4
    for i in range(0,len(arrCurrents)):
        listInductances.append([arrStoredEnergyElectric[i]*geoFact*2/(geoFact*arrCurrents[i])**2,arrStoredEnergyMagnetic[i]*geoFact*2/(geoFact*arrCurrents[i])**2])

    arrInductances=sp.array(listInductances)

    return arrInductances

def maxwellDataReadAC(targetFile):
    datChan = open(targetFile, "r+")
    datRead = datChan.readlines()
    datChan.close()
    temp=[]
    for entry in datRead[1:]:
        tmp = entry.split(",")
        tmp[-1] = tmp[-1].rstrip("\n")
        temp.append(map(float,tmp))
    return sp.array(temp)

def maxwellMatrixRead(targetFile):
    datChan = open(targetFile, "r+")
    datRead = datChan.readlines()
    datChan.close()
    temp=[]
    tidbit=[]
    countList = 0
    while countList< len(datRead):
        if datRead[countList].endswith("Hz\n"):
            tidbit.append(datRead[countList].replace("Hz\n",""))
            countList+=1
        elif datRead[countList].startswith("	R,L"):
            countList+=2
            tidbit.append(str(datRead[countList].replace("		Current1	","")))
            tidbit.append((datRead[countList+1].replace("		Current2	","")))
            temp.append(tidbit)
##            print tidbit
            tidbit=[]
        else:
            countList+=1
            pass
    inductStore = []
    for i in range(0,len(temp)):
        stringRaw = temp[i]
##        print stringRaw
##        print stringRaw[0]
        freq = float(stringRaw[0].strip("Adaptive Freq : "))
        coup11, coup12 = stringRaw[1].split("\t")
##        print coup11, coup12
        L11 = coup11.split(",")[1]
        L12 = coup12.split(",")[1]
        L11=float(L11.rstrip("\n"))
        L12=float(L12.rstrip("\n"))
##        print L11, L12
        coup21, coup22 = stringRaw[2].split("\t")
        L21 = coup21.split(",")[1]
        L22 = coup22.split(",")[1]
        L21=float(L21.rstrip("\n"))
        L22=float(L22.rstrip("\n"))
        inductAdd = [freq, L11, L12, L21, L22]
##        print inductAdd
        inductStore.append(inductAdd)

    return sp.array(inductStore)

### Data from matrix calculation in Maxwell

tarDirMat = "E:/PhD/1st_Year_09-10/Data/maxwellTestCoax/"
matDataInnerCond = maxwellMatrixRead(tarDirMat+"coaxACTest_Maxwell2DDesign1InnerCondCurrent.txt")
matDataOuterCond = maxwellMatrixRead(tarDirMat+"coaxACTest_Maxwell2DDesign1OuterCondCurrent.txt")
matDataInnerOuterCond = maxwellMatrixRead(tarDirMat+"coaxACTest_Maxwell2DDesign1InnerOuterCondCurrent.txt")
matDataInnerOuterCondHighMesh = maxwellMatrixRead(tarDirMat+"coaxACTest_Maxwell2DDesign1InnerOuterCondCurrentHighMesh.txt")

### Data from field calculation in Maxwell

calcDataInnerOnlyCurInner = maxwellDataReadAC(tarDirMat+"inCondInductInnerconCur.csv")
calcDataOuterOnlyCurInner = maxwellDataReadAC(tarDirMat+"outCondInductInnerconCur.csv")
calcDataTotalCurInner = maxwellDataReadAC(tarDirMat+"totCondInductInnerconCur.csv")
calcDataInnerAirCurInner = maxwellDataReadAC(tarDirMat+"inAirGapInductInnerconCur.csv")
calcDataOuterAirCurInner = maxwellDataReadAC(tarDirMat+"outAirGapInductInnerconCur.csv")

calcDataInnerOnlyCurOuter = maxwellDataReadAC(tarDirMat+"inCondInductOuterconCur.csv")
calcDataOuterOnlyCurOuter = maxwellDataReadAC(tarDirMat+"outCondInductOuterconCur.csv")
calcDataTotalCurOuter = maxwellDataReadAC(tarDirMat+"totCondInductOuterconCur.csv")
calcDataInnerAirCurOuter = maxwellDataReadAC(tarDirMat+"inAirGapInductOuterconCur.csv")
calcDataOuterAirCurOuter = maxwellDataReadAC(tarDirMat+"outAirGapInductOuterconCur.csv")

calcDataInnerOnlyCurInnerOuter = maxwellDataReadAC(tarDirMat+"inCondInductInnerOuterconCur.csv")
calcDataOuterOnlyCurInnerOuter = maxwellDataReadAC(tarDirMat+"outCondInductInnerOuterconCur.csv")
calcDataTotalCurInnerOuter = maxwellDataReadAC(tarDirMat+"totCondInductInnerOuterconCur.csv")
calcDataInnerAirCurInnerOuter = maxwellDataReadAC(tarDirMat+"inAirGapInductInnerOuterconCur.csv")
calcDataOuterAirCurInnerOuter = maxwellDataReadAC(tarDirMat+"outAirGapInductInnerOuterconCur.csv")

calcDataInnerOnlyCurInnerOuterHighMesh = maxwellDataReadAC(tarDirMat+"inCondInductInnerOuterconCurHighMesh.csv")
calcDataOuterOnlyCurInnerOuterHighMesh = maxwellDataReadAC(tarDirMat+"outCondInductInnerOuterconCurHighMesh.csv")
calcDataTotalCurInnerOuterHighMesh = maxwellDataReadAC(tarDirMat+"totCondInductInnerOuterconCurHighMesh.csv")
calcDataInnerAirCurInnerOuterHighMesh = maxwellDataReadAC(tarDirMat+"inAirGapInductInnerOuterconCurHighMesh.csv")
calcDataOuterAirCurInnerOuterHighMesh = maxwellDataReadAC(tarDirMat+"outAirGapInductInnerOuterconCurHighMesh.csv")

### Data from calculation in Opera

tarDirOperaLogFiles = "E:/PhD/1st_Year_09-10/Data/maxwellTestCoax/operaLogFiles/"
inductInnerOnlyCurInner = fileInductRead(tarDirOperaLogFiles+"innerConductorCurrentInnerGeo.lp")
inductOuterOnlyCurInner = fileInductRead(tarDirOperaLogFiles+"innerConductorCurrentOuterGeo.lp")
inductAllCurInner = fileInductRead(tarDirOperaLogFiles+"innerConductorCurrentAllGeo.lp")

inductInnerOnlyCurOuter = fileInductRead(tarDirOperaLogFiles+"outerConductorCurrentInnerGeo.lp")
inductOuterOnlyCurOuter = fileInductRead(tarDirOperaLogFiles+"outerConductorCurrentOuterGeo.lp")
inductAllCurOuter = fileInductRead(tarDirOperaLogFiles+"outerConductorCurrentAllGeo.lp")

inductInnerOnlyCurInnerOuter = fileInductRead(tarDirOperaLogFiles+"innerOuterConductorCurrentInnerGeo.lp")
inductOuterOnlyCurInnerOuter = fileInductRead(tarDirOperaLogFiles+"innerOuterConductorCurrentOuterGeo.lp")
inductAllCurInnerOuter = fileInductRead(tarDirOperaLogFiles+"innerOuterConductorCurrentAllGeo.lp")

inductInnerOnlyCurInnerSym = fileInductRead(tarDirOperaLogFiles+"innerConductorCurrentInnerGeoSym.lp")
inductOuterOnlyCurInnerSym = fileInductRead(tarDirOperaLogFiles+"innerConductorCurrentOuterGeoSym.lp")
inductAllCurInnerSym = fileInductRead(tarDirOperaLogFiles+"innerConductorCurrentAllGeoSym.lp")

inductInnerOnlyCurOuterSym = fileInductRead(tarDirOperaLogFiles+"outerConductorCurrentInnerGeoSym.lp")
inductOuterOnlyCurOuterSym = fileInductRead(tarDirOperaLogFiles+"outerConductorCurrentOuterGeoSym.lp")
inductAllCurOuterSym = fileInductRead(tarDirOperaLogFiles+"outerConductorCurrentAllGeoSym.lp")

inductInnerOnlyCurInnerOuterSym = fileInductRead(tarDirOperaLogFiles+"innerOuterConductorCurrentInnerGeoSym.lp")
inductOuterOnlyCurInnerOuterSym = fileInductRead(tarDirOperaLogFiles+"innerOuterConductorCurrentOuterGeoSym.lp")
inductAllCurInnerOuterSym = fileInductRead(tarDirOperaLogFiles+"innerOuterConductorCurrentAllGeoSym.lp")

inductInnerOnlyCurInnerSymNorm = fileInductRead(tarDirOperaLogFiles+"innerConductorCurrentInnerGeoSymFieldNorm.lp")
inductOuterOnlyCurInnerSymNorm = fileInductRead(tarDirOperaLogFiles+"innerConductorCurrentOuterGeoSymFieldNorm.lp")
inductAllCurInnerSymNorm = fileInductRead(tarDirOperaLogFiles+"innerConductorCurrentAllGeoSymFieldNorm.lp")

inductAllCurInnerSymTan = fileInductRead(tarDirOperaLogFiles+"innerConductorCurrentAllGeoSymFieldTan.lp")

inductAllCurInnerNorm = fileInductRead(tarDirOperaLogFiles+"innerConductorCurrentAllGeoNorm.lp")

inductAllCurInnerTan = fileInductRead(tarDirOperaLogFiles+"innerConductorCurrentAllGeoTan.lp")

freqListOpera = []
for i in range(-1,9):
    for j in range(1,10):
        freqListOpera.append(j*10.0**i)
freqListOpera=sp.array(freqListOpera)
print freqListOpera

pl.semilogx()
##pl.loglog()
pl.plot(matDataInnerCond[:,0], matDataInnerCond[:,1]*10**6)
##pl.plot(matDataInnerCond[:,0], matDataInnerCond[:,2]*10**6)
##pl.plot(matDataInnerCond[:,0], matDataInnerCond[:,3]*10**6)
##pl.plot(matDataInnerCond[:,0], matDataInnerCond[:,4]*10**6)
##pl.plot(matDataOuterCond[:,0], matDataOuterCond[:,1]*10**6)
##pl.plot(matDataOuterCond[:,0], matDataOuterCond[:,2]*10**6)
##pl.plot(matDataOuterCond[:,0], matDataOuterCond[:,3]*10**6)
##pl.plot(matDataOuterCond[:,0], matDataOuterCond[:,4]*10**6)
##pl.plot(matDataInnerOuterCond[:,0], matDataInnerOuterCond[:,1]*10**6)
##pl.plot(matDataInnerOuterCond[:,0], matDataInnerOuterCond[:,2]*10**6)
##pl.plot(matDataInnerOuterCond[:,0], matDataInnerOuterCond[:,3]*10**6)
##pl.plot(matDataInnerOuterCond[:,0], matDataInnerOuterCond[:,4]*10**6)
##pl.plot(matDataInnerOuterCondHighMesh[:,0], matDataInnerOuterCondHighMesh[:,1]*10**6)
##pl.plot(matDataInnerOuterCondHighMesh[:,0], matDataInnerOuterCondHighMesh[:,2]*10**6)
##pl.plot(matDataInnerOuterCondHighMesh[:,0], matDataInnerOuterCondHighMesh[:,3]*10**6)
##pl.plot(matDataInnerOuterCondHighMesh[:,0], matDataInnerOuterCondHighMesh[:,4]*10**6)

pl.plot(calcDataInnerOnlyCurInner[:,0]*10**6,calcDataInnerOnlyCurInner[:,1]*10**9)
##pl.plot(calcDataOuterOnlyCurInner[:,0]*10**6,calcDataOuterOnlyCurInner[:,1]*10**9, label="Outer Only")
##pl.plot(calcDataTotalCurInner[:,0]*10**6,calcDataTotalCurInner[:,1]*10**9, label="Total Maxwell")
##pl.plot(calcDataInnerAirCurInner[:,0]*10**6,calcDataInnerAirCurInner[:,1]*10**9)
##pl.plot(calcDataOuterAirCurInner[:,0]*10**6,calcDataOuterAirCurInner[:,1]*10**9)

##totalThing = (calcDataInnerOnlyCurInner[:,1]+2*calcDataOuterOnlyCurInner[:,1]+calcDataInnerAirCurInner[:,1]+calcDataOuterAirCurInner[:,1])*10.0**9
##totalThing = (calcDataInnerOnlyCurInner[:,1]+calcDataInnerAirCurInner[:,1]+calcDataOuterAirCurInner[:,1])*10.0**9
##pl.plot(calcDataOuterAirCurInner[:,0]*10**6,totalThing, label="Total Summed")

##pl.plot(calcDataInnerOnlyCurOuter[:,0]*10**6,calcDataInnerOnlyCurOuter[:,1]*10**9)
##pl.plot(calcDataOuterOnlyCurOuter[:,0]*10**6,calcDataOuterOnlyCurOuter[:,1]*10**9)
##pl.plot(calcDataTotalCurOuter[:,0]*10**6,calcDataTotalCurOuter[:,1]*10**9)
##pl.plot(calcDataInnerAirCurOuter[:,0]*10**6,calcDataInnerAirCurOuter[:,1]*10**9)
##pl.plot(calcDataOuterAirCurOuter[:,0]*10**6,calcDataOuterAirCurOuter[:,1]*10**9)

##pl.plot(calcDataInnerOnlyCurInnerOuter[:,0]*10**6,calcDataInnerOnlyCurInnerOuter[:,1]*10**9)
##pl.plot(calcDataOuterOnlyCurInnerOuter[:,0]*10**6,calcDataOuterOnlyCurInnerOuter[:,1]*10**9)
##pl.plot(calcDataTotalCurInnerOuter[:,0]*10**6,calcDataTotalCurInnerOuter[:,1]*10**9)
##pl.plot(calcDataInnerAirCurInnerOuter[:,0]*10**6,calcDataInnerAirCurInnerOuter[:,1]*10**9)
##pl.plot(calcDataOuterAirCurInnerOuter[:,0]*10**6,calcDataOuterAirCurInnerOuter[:,1]*10**9)

##pl.plot(calcDataInnerOnlyCurInnerOuterHighMesh[:,0]*10**6,calcDataInnerOnlyCurInnerOuterHighMesh[:,1]*10**9)
##pl.plot(calcDataOuterOnlyCurInnerOuterHighMesh[:,0]*10**6,calcDataOuterOnlyCurInnerOuterHighMesh[:,1]*10**9)
##pl.plot(calcDataTotalCurInnerOuterHighMesh[:,0]*10**6,calcDataTotalCurInnerOuterHighMesh[:,1]*10**9)
##pl.plot(calcDataInnerAirCurInnerOuterHighMesh[:,0]*10**6,calcDataInnerAirCurInnerOuterHighMesh[:,1]*10**9)
##pl.plot(calcDataOuterAirCurInnerOuterHighMesh[:,0]*10**6,calcDataOuterAirCurInnerOuterHighMesh[:,1]*10**9)


pl.plot(freqListOpera, inductInnerOnlyCurInner[:,1]*10**9)
##pl.plot(freqListOpera, inductOuterOnlyCurInner[:,1]*10**9)
pl.plot(freqListOpera, inductAllCurInner[:,1]*10**9)

##pl.plot(freqListOpera, inductInnerOnlyCurOuter[:,1]*10**9)
##pl.plot(freqListOpera, inductOuterOnlyCurOuter[:,1]*10**9)
##pl.plot(freqListOpera, inductAllCurOuter[:,1]*10**9)

##pl.plot(freqListOpera, inductInnerOnlyCurInnerOuter[:,0]*10**9)
##pl.plot(freqListOpera, inductOuterOnlyCurInnerOuter[:,0]*10**9)
##pl.plot(freqListOpera, inductAllCurInnerOuter[:,1]*10**9)

##pl.plot(freqListOpera, inductInnerOnlyCurInnerSym[:,1]*10**9)
##pl.plot(freqListOpera, inductOuterOnlyCurInnerSym[:,1]*10**9)
##pl.plot(freqListOpera, inductAllCurInnerSym[:,1]*10**9)

##pl.plot(freqListOpera, inductInnerOnlyCurOuterSym[:,1]*10**9)
##pl.plot(freqListOpera, inductOuterOnlyCurOuterSym[:,1]*10**9)
##pl.plot(freqListOpera, inductAllCurOuterSym[:,1]*10**9)

##pl.plot(freqListOpera, inductInnerOnlyCurInnerOuterSym[:,0]*10**9)
##pl.plot(freqListOpera, inductOuterOnlyCurInnerOuterSym[:,0]*10**9)
##pl.plot(freqListOpera, inductAllCurInnerOuterSym[:,1]*10**9)

##pl.plot(freqListOpera, inductInnerOnlyCurInnerSymNorm[:,1]*10**9)
##pl.plot(freqListOpera, inductOuterOnlyCurInnerSymNorm[:,1]*10**9)
##pl.plot(freqListOpera, inductAllCurInnerSymNorm[:,1]*10**9)

##pl.plot(freqListOpera, inductAllCurInnerSymTan[:,1]*10**9)

##pl.plot(freqListOpera, inductAllCurInnerNorm[:,1]*10**9)

##pl.plot(freqListOpera, inductAllCurInnerTan[:,1]*10**9)

##pl.ylim(200,300)
##pl.ylim(10**-9,10**-6)
pl.legend(loc="upper left")
pl.show()
pl.clf()
