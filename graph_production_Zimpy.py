import csv, time
import scipy as sp
import pylab as pl
import numpy as np

start = time.time()

gamma = 27.7286
v_rf = 6.0*10**5
h = 4620
circ = 6911.0
C = 3.0*10**8
f_rev = C/circ
tune= 0.00324
omega = tune*2*sp.pi*f_rev
e = 1.602176487*10**-19
m_pro = 1.672621637 *10**-27
p_0 = gamma*m_pro*C
k_h = 2*sp.pi*h/circ
T_0 = circ/C
alpha_bt = 0.00068
alpha_at = 0.00192
eta_bt = 1/(gamma**2) - alpha_bt
eta_at = 1/(gamma**2) - alpha_at
cos_phi_bt = sp.cos(0)        #### Non-accelerating bucket with maximum momentum acceptance below transition
cos_phi_at = sp.cos(sp.pi)       #### Non-accelerating bucket with maximum momentum acceptance above transition


def Z_l_n(x_terms, bunch_lengths, x_min, x_max, trans):
    time_lengths = sp.array([(2**0.5*bunch_lengths[i]/C) for i in range(0,len(bunch_lengths))])
    fit_coeffs = sp.polyfit(x_terms[x_min:x_max], time_lengths[x_min:x_max],1)
    if trans == "bt":
        return -4*1.0*(sp.pi)**2*(v_rf*h)*(2*sp.pi*f_rev*fit_coeffs[1])**2*fit_coeffs[0]/(3*e)
    elif trans == "at":
        return -4*(-1.0)*(sp.pi)**2*(v_rf*h)*(2*sp.pi*f_rev*fit_coeffs[1])**2*fit_coeffs[0]/(3*e)
    else:
        return "Incorrect input"

directory = "E:/PhD/1st_Year_09-10/Software/headtail/data_dump/hdtl-data/" #Directory of data

list_bt_BB_0_SC_1 = [directory+"hdtl-bt-BB-0-SC-1.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_bt_BB_0_SC_5 = [directory+"hdtl-bt-BB-0-SC-5.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_bt_BB_0_SC_10 = [directory+"hdtl-bt-BB-0-SC-10.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_bt_BB_0_SC_20 = [directory+"hdtl-bt-BB-0-SC-20.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_bt_BB_1_SC_0 = [directory+"hdtl-bt-BB-1-SC-0.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_bt_BB_5_SC_0 = [directory+"hdtl-bt-BB-5-SC-0.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_bt_BB_10_SC_0 = [directory+"hdtl-bt-BB-10-SC-0.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_bt_BB_20_SC_0 = [directory+"hdtl-bt-BB-20-SC-0.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_bt_BB_10_SC_1 = [directory+"hdtl-bt-BB-10-SC-1.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_bt_BB_10_SC_5 = [directory+"hdtl-bt-BB-10-SC-5.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_bt_BB_10_SC_10 = [directory+"hdtl-bt-BB-10-SC-10.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_bt_BB_10_SC_20 = [directory+"hdtl-bt-BB-10-SC-20.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_bt_BB_10_SC_100 = [directory+"hdtl-bt-BB-10-SC-100.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]




list_at_BB_0_SC_1 = [directory+"hdtl-at-BB-0-SC-1.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_BB_0_SC_5 = [directory+"hdtl-at-BB-0-SC-5.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_BB_0_SC_10 = [directory+"hdtl-at-BB-0-SC-10.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_BB_0_SC_20 = [directory+"hdtl-at-BB-0-SC-20.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_BB_1_SC_0 = [directory+"hdtl-at-BB-1-SC-0.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_BB_5_SC_0 = [directory+"hdtl-at-BB-5-SC-0.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_BB_10_SC_0 = [directory+"hdtl-at-BB-10-SC-0.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_BB_20_SC_0 = [directory+"hdtl-at-BB-20-SC-0.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_BB_10_SC_1 = [directory+"hdtl-at-BB-10-SC-1.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_BB_10_SC_5 = [directory+"hdtl-at-BB-10-SC-5.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_BB_10_SC_10 = [directory+"hdtl-at-BB-10-SC-10.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_BB_10_SC_20 = [directory+"hdtl-at-BB-10-SC-20.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]
list_at_BB_10_SC_100 = [directory+"hdtl-at-BB-10-SC-100.NumPar."+str(i)+"_prt.dat" for i in range(1,301)]


value_desired = 9

##data_list_1 = []
##for data_raw in list_bt_BB_0_SC_1:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_BB_0_SC_1=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_bt_BB_0_SC_5:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_BB_0_SC_5=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_bt_BB_0_SC_10:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_BB_0_SC_10=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_bt_BB_0_SC_20:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_BB_0_SC_20=sp.array(data_list_1)

##data_list_1 = []
##for data_raw in list_bt_BB_1_SC_0:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_BB_1_SC_0=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_bt_BB_5_SC_0:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_BB_5_SC_0=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_bt_BB_10_SC_0:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_BB_10_SC_0=sp.array(data_list_1)
####
##data_list_1 = []
##for data_raw in list_bt_BB_20_SC_0:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_BB_20_SC_0=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_bt_BB_10_SC_1:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_BB_10_SC_1=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_bt_BB_10_SC_5:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_BB_10_SC_5=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_bt_BB_10_SC_10:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_BB_10_SC_10=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_bt_BB_10_SC_20:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_BB_10_SC_20=sp.array(data_list_1)
##
##
##data_list_1 = []
##for data_raw in list_bt_BB_10_SC_100:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##bt_BB_10_SC_100=sp.array(data_list_1)



data_list_1 = []
for data_raw in list_at_BB_0_SC_1:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    if value_desired == 9 or 10 or 13 or 16:
        data_list_1.append(append_tar[value_desired])
    elif value_desired == 20:
        data_list_1.append(append_tar[9]*append_tar[10]) 
    
at_BB_0_SC_1=sp.array(data_list_1)

data_list_1 = []
for data_raw in list_at_BB_0_SC_5:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    if value_desired == 9 or 10 or 13 or 16:
        data_list_1.append(append_tar[value_desired])
    elif value_desired == 20:
        data_list_1.append(append_tar[9]*append_tar[10]) 
    
at_BB_0_SC_5=sp.array(data_list_1)

data_list_1 = []
for data_raw in list_at_BB_0_SC_10:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    if value_desired == 9 or 10 or 13 or 16:
        data_list_1.append(append_tar[value_desired])
    elif value_desired == 20:
        data_list_1.append(append_tar[9]*append_tar[10]) 
    
at_BB_0_SC_10=sp.array(data_list_1)

data_list_1 = []
for data_raw in list_at_BB_0_SC_20:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    if value_desired == 9 or 10 or 13 or 16:
        data_list_1.append(append_tar[value_desired])
    elif value_desired == 20:
        data_list_1.append(append_tar[9]*append_tar[10]) 
    
at_BB_0_SC_20=sp.array(data_list_1)


##data_list_1 = []
##for data_raw in list_at_BB_1_SC_0:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##at_BB_1_SC_0=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_at_BB_5_SC_0:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##at_BB_5_SC_0=sp.array(data_list_1)

##data_list_1 = []
##for data_raw in list_at_BB_10_SC_0:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##at_BB_10_SC_0=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_at_BB_20_SC_0:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##at_BB_20_SC_0=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_at_BB_10_SC_1:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##at_BB_10_SC_1=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_at_BB_10_SC_5:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##at_BB_10_SC_5=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_at_BB_10_SC_10:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##at_BB_10_SC_10=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_at_BB_10_SC_20:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##at_BB_10_SC_20=sp.array(data_list_1)
##
##data_list_1 = []
##for data_raw in list_at_BB_10_SC_100:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    row=linelist[-1]
##    append_tar = map(float, row.split())
##    if value_desired == 9 or 10 or 13 or 16:
##        data_list_1.append(append_tar[value_desired])
##    elif value_desired == 20:
##        data_list_1.append(append_tar[9]*append_tar[10]) 
##    
##at_BB_10_SC_100=sp.array(data_list_1)
##

x_val = sp.array([(i)*10**9 for i in range(1,301)])
x_val_plot = sp.array([(i) for i in range(1,301)])

##Z_l_n_bt_BB_0_SC_1 = Z_l_n(x_val, bt_BB_0_SC_1, 0, 50, "bt")
##Z_l_n_bt_BB_0_SC_5 = Z_l_n(x_val, bt_BB_0_SC_5, 0, 50, "bt")
##Z_l_n_bt_BB_0_SC_10 = Z_l_n(x_val, bt_BB_0_SC_10, 0, 50, "bt")
##Z_l_n_bt_BB_0_SC_20 = Z_l_n(x_val, bt_BB_0_SC_20, 0, 5, "bt")
##Z_l_n_bt_BB_1_SC_0 = Z_l_n(x_val, bt_BB_1_SC_0, 150, 300, "bt")
##Z_l_n_bt_BB_5_SC_0 = Z_l_n(x_val, bt_BB_5_SC_0, 70, 100, "bt")
##Z_l_n_bt_BB_10_SC_0 = Z_l_n(x_val, bt_BB_10_SC_0, 50, 70, "bt")
##Z_l_n_bt_BB_20_SC_0 = Z_l_n(x_val, bt_BB_20_SC_0, 15, 30, "bt")
##Z_l_n_bt_BB_10_SC_1 = Z_l_n(x_val, bt_BB_10_SC_1, 0, 50, "bt")
##Z_l_n_bt_BB_10_SC_5 = Z_l_n(x_val, bt_BB_10_SC_5, 0, 50, "bt")
##Z_l_n_bt_BB_10_SC_10 = Z_l_n(x_val, bt_BB_10_SC_10, 0, 50, "bt")
##Z_l_n_bt_BB_10_SC_20 = Z_l_n(x_val, bt_BB_10_SC_20, 0, 50, "bt")
##Z_l_n_bt_BB_10_SC_100 = Z_l_n(x_val, bt_BB_10_SC_100, 0, 50, "bt")

Z_l_n_at_BB_0_SC_1 = Z_l_n(x_val, at_BB_0_SC_1, 20, 70, "at")
Z_l_n_at_BB_0_SC_5 = Z_l_n(x_val, at_BB_0_SC_5, 0, 50, "at")
Z_l_n_at_BB_0_SC_10 = Z_l_n(x_val, at_BB_0_SC_10, 0, 40, "at")
Z_l_n_at_BB_0_SC_20 = Z_l_n(x_val, at_BB_0_SC_20, 0, 20, "at")
##Z_l_n_at_BB_1_SC_0 = Z_l_n(x_val, at_BB_1_SC_0, 100, 120, "at")
##Z_l_n_at_BB_5_SC_0 = Z_l_n(x_val, at_BB_5_SC_0, 100, 120, "at")
##Z_l_n_at_BB_10_SC_0 = Z_l_n(x_val, at_BB_10_SC_0, 0, 80, "at")
##Z_l_n_at_BB_20_SC_0 = Z_l_n(x_val, at_BB_20_SC_0, 0, 60, "at")
##Z_l_n_at_BB_10_SC_1 = Z_l_n(x_val, at_BB_10_SC_1, 0, 80, "at")
##Z_l_n_at_BB_10_SC_5 = Z_l_n(x_val, at_BB_10_SC_5, 0, 50, "at")
##Z_l_n_at_BB_10_SC_10 = Z_l_n(x_val, at_BB_10_SC_10, 0, 40, "at")
##Z_l_n_at_BB_10_SC_20 = Z_l_n(x_val, at_BB_10_SC_20, 0, 20, "at")
##Z_l_n_at_BB_10_SC_100 = Z_l_n(x_val, at_BB_10_SC_100, 0, 40, "at")

##print "Z/n for BT, BB = 0 Ohms, SC = 1 = %f Ohms" % Z_l_n_bt_BB_0_SC_1
##print "Z/n for BT, BB = 0 Ohms, SC = 5 = %f Ohms" % Z_l_n_bt_BB_0_SC_5
##print "Z/n for BT, BB = 0 Ohms, SC = 10 = %f Ohms" % Z_l_n_bt_BB_0_SC_10
##print "Z/n for BT, BB = 0 Ohms, SC = 20 = %f Ohms" % Z_l_n_bt_BB_0_SC_20
##print "Z/n for BT, BB = 1 Ohms, SC = 0 = %f Ohms" % Z_l_n_bt_BB_1_SC_0 
##print "Z/n for BT, BB = 5 Ohms, SC = 0 = %f Ohms" % Z_l_n_bt_BB_5_SC_0 
##print "Z/n for BT, BB = 10 Ohms, SC = 0 = %f Ohms" % Z_l_n_bt_BB_10_SC_0 
##print "Z/n for BT, BB = 20 Ohms, SC = 0 = %f Ohms" % Z_l_n_bt_BB_20_SC_0
##print "Z/n for BT, BB = 10 Ohms, SC = 1 = %f Ohms" % Z_l_n_bt_BB_10_SC_1
##print "Z/n for BT, BB = 10 Ohms, SC = 5 = %f Ohms" % Z_l_n_bt_BB_10_SC_5
##print "Z/n for BT, BB = 10 Ohms, SC = 10 = %f Ohms" % Z_l_n_bt_BB_10_SC_10
##print "Z/n for BT, BB = 10 Ohms, SC = 20 = %f Ohms" % Z_l_n_bt_BB_10_SC_20
##print "Z/n for BT, BB = 10 Ohms, SC = 100 = %f Ohms" % Z_l_n_bt_BB_10_SC_100

print "Z/n for AT, BB = 0 Ohms, SC = 1 = %f Ohms" % Z_l_n_at_BB_0_SC_1
print "Z/n for AT, BB = 0 Ohms, SC = 5 = %f Ohms" % Z_l_n_at_BB_0_SC_5
print "Z/n for AT, BB = 0 Ohms, SC = 10 = %f Ohms" % Z_l_n_at_BB_0_SC_10
print "Z/n for AT, BB = 0 Ohms, SC = 20 = %f Ohms" % Z_l_n_at_BB_0_SC_20
##print "Z/n for AT, BB = 1 Ohms, SC = 0 = %f Ohms" % Z_l_n_at_BB_1_SC_0 
##print "Z/n for AT, BB = 5 Ohms, SC = 0 = %f Ohms" % Z_l_n_at_BB_5_SC_0 
##print "Z/n for AT, BB = 10 Ohms, SC = 0 = %f Ohms" % Z_l_n_at_BB_10_SC_0 
##print "Z/n for AT, BB = 20 Ohms, SC = 0 = %f Ohms" % Z_l_n_at_BB_20_SC_0
##print "Z/n for AT, BB = 10 Ohms, SC = 1 = %f Ohms" % Z_l_n_at_BB_10_SC_1
##print "Z/n for AT, BB = 10 Ohms, SC = 5 = %f Ohms" % Z_l_n_at_BB_10_SC_5
##print "Z/n for AT, BB = 10 Ohms, SC = 10 = %f Ohms" % Z_l_n_at_BB_10_SC_10
##print "Z/n for AT, BB = 10 Ohms, SC = 20 = %f Ohms" % Z_l_n_at_BB_10_SC_20
##print "Z/n for AT, BB = 10 Ohms, SC = 100 = %f Ohms" % Z_l_n_at_BB_10_SC_100

pl.grid(linestyle="--", which = "major")
##pl.plot(x_val_plot, bt_BB_0_SC_1, 'k-', label="bt BB 0 SC 1")
##pl.plot(x_val_plot, bt_BB_0_SC_5, 'r-', label="bt BB 0 SC 5")
##pl.plot(x_val_plot, bt_BB_0_SC_10, 'b-', label="bt BB 0 SC 10")
##pl.plot(x_val_plot, bt_BB_0_SC_20, 'g-', label="bt BB 0 SC 20")
##pl.plot(x_val_plot, bt_BB_1_SC_0, 'k-', label="bt BB 1 SC 0")
##pl.plot(x_val_plot, bt_BB_5_SC_0, 'r-', label="bt BB 5 SC 0")
##pl.plot(x_val_plot, bt_BB_10_SC_0, 'b-', label="bt BB 10 SC 0")
##pl.plot(x_val_plot, bt_BB_20_SC_0, 'g-', label="bt BB 20 SC 0")
##pl.plot(x_val_plot, bt_BB_10_SC_1, 'k-', label="bt BB 10 SC 1")
##pl.plot(x_val_plot, bt_BB_10_SC_5, 'r-', label="bt BB 10 SC 5")
##pl.plot(x_val_plot, bt_BB_10_SC_10, 'b-', label="bt BB 10 SC 10")
##pl.plot(x_val_plot, bt_BB_10_SC_20, 'g-', label="bt BB 10 SC 20")
##pl.plot(x_val_plot, bt_BB_10_SC_100, 'y-', label="bt BB 10 SC 100")


pl.plot(x_val_plot, at_BB_0_SC_1, 'k--', label="at BB 0 SC 1")
pl.plot(x_val_plot, at_BB_0_SC_5, 'r--', label="at BB 0 SC 5")
pl.plot(x_val_plot, at_BB_0_SC_10, 'b--', label="at BB 0 SC 10")
pl.plot(x_val_plot, at_BB_0_SC_20, 'g--', label="at BB 0 SC 20")
##pl.plot(x_val_plot, at_BB_1_SC_0, 'k--', label="at BB 1_SC 0")
##pl.plot(x_val_plot, at_BB_5_SC_0, 'r--', label="at BB 5_SC 0")
##pl.plot(x_val_plot, at_BB_10_SC_0, 'b-', label="at BB 10 SC 0")
##pl.plot(x_val_plot, at_BB_20_SC_0, 'g--', label="at BB 20 SC 0")
##pl.plot(x_val_plot, at_BB_10_SC_1, 'k-', label="at BB 10 SC 1")
##pl.plot(x_val_plot, at_BB_10_SC_5, 'r-', label="at BB 10 SC 5")
##pl.plot(x_val_plot, at_BB_10_SC_10, 'm-', label="at BB 10 SC 10")
##pl.plot(x_val_plot, at_BB_10_SC_20, 'g-', label="at BB 10 SC 20")
##pl.plot(x_val_plot, at_BB_10_SC_100, 'y-', label="at BB 10 SC 100")

##pl.axis([0,150,0.2,0.23])
pl.xlabel("Bunch Population $N_{b} $($10^{9}$)",fontsize = 16)
if value_desired == 9:
    pl.ylabel("Bunch length $\sigma_{z}$ (m)",fontsize = 16)
if value_desired == 10:
    pl.ylabel("Momentum Spread $\delta p/p$",fontsize = 16)
if value_desired == 16:
    pl.ylabel("Longitudinal emittance (eVs)",fontsize = 16)
pl.legend(loc="upper left")
pl.show()
pl.clf()

print time.time() - start



