import csv, os, sys
import scipy as sp
import pylab as pl
from scipy import optimize as op

fitfunc = lambda p, x: p[0] + p[1]*x + p[2]*x**2
errfunc = lambda p, x, y, err: (y-fitfunc(p, x))/err

distance_acc = 0.0005
imp_err = distance_acc/0.003

C = 3.0*10**8
##Z0=377.0
Z0=240.0
##print cos_phi_bt
##print cos_phi_at
##print sp.arccos(cos_phi_bt)
##print sp.arccos(cos_phi_at)
length = 2.2
act_wire_sep = 0.009
wire_thick = 0.0005
wire_sep = (act_wire_sep**2-wire_thick**2)**0.5
Z0 = 120*sp.arccosh(wire_sep/wire_thick)


directory = "E:/PhD/1st_Year_09-10/Data/Two-wire-measurements-mke-seriagraphed/2-wire-horz/transmission/" #Directory of data
list_data_files = [directory+"x_"+str(i)+"/LOGMAG_X_"+str(i)+".S2P" for i in range(0,30,3)]
dipole_data = directory+"TRANSMISSION-TWO-WIRE-HORIZ.S2P"


##data_list = []
##for data_raw in list_data_files:
##    data=[]
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    counter=0.0
##    count=0
##    last=1e8
##    freq_last = 0.0                     
##    for row in linelist[5:]:
##        row=map(float, row.split())
##        lin_mag = 10**(row[3]/20)
##        s_21_pec = 2*sp.pi*row[0]*length/C
##        phase = row[4]-counter
##        if phase>last and (phase-last)>180 and abs(freq_last-row[0])>1e8:
##            count+=1
##            counter+=360
##            phase-=360
##            freq_last = row[0]
##        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<1e8:
##            phase+=360
##            
##        import_dat = [row[0], -2*Z0*sp.log(lin_mag), -2*Z0*(sp.radians(phase)+s_21_pec)]
##        data.append(import_dat)
##        last=phase
##    data_list.append(data)
##    
##data_array=sp.array(data_list)
data_list_dip = []


input_file = open(dipole_data, 'r+')
linelist = input_file.readlines()
input_file.close()
counter=0.0
count=0
last=1e8

for row in linelist[5:]:
    row=map(float, row.split())
    lin_mag = 10**(row[3]/20)
    s_21_pec = 2*sp.pi*row[0]*length/C
    phase = row[4]-counter
    if phase>last and (phase-last)>180:
        count+=1
        counter+=360
        phase-=360
##    import_dat = [row[0], -2*Z0*sp.log(lin_mag)*C/(2*sp.pi*row[0]*wire_sep**2), -2*Z0*(sp.radians(phase)+s_21_pec)*C/(2*sp.pi*row[0]*wire_sep**2)]
    import_dat = [row[0], -2*Z0*sp.log(lin_mag)/wire_sep**2, -2*Z0*(sp.radians(phase)+s_21_pec)/wire_sep**2]
    data_list_dip.append(import_dat)
    last=phase

dip_data=sp.array(data_list_dip)
impedance_map = []

x_data = sp.linspace(0, 27, 10)
coefficients = []

tar_dir_real=(directory+"horz_real/")
tar_dir_imag=(directory+"horz_imag/")
try:
    os.mkdir(tar_dir_real)

except:
    pass

try:
    os.mkdir(tar_dir_imag)

except:
    pass


##trans_imp = []
##long_imp = []
##
##for i in range(0,2000):
##    pinit = [1.0, 1.0, 1.0]
##    y_err = data_array[:,i,1]*imp_err
##    out = op.leastsq(errfunc, pinit, args=(x_data,data_array[:,i,1],y_err), full_output=1)
##    pfinal_real = out[0]
##    covar_real=out[1]
##    x_plot = sp.linspace(-3,30,1000)
##    y_test = fitfunc(pfinal_real, x_plot)
##    pl.plot(x_plot, y_test, label= "Fitted Function")
##    pl.errorbar(x_data,data_array[:,i,1],yerr=y_err, fmt='ko',label = "Measured Data") 
##    pl.legend()
##    pl.xlabel('Displacement (mm)', fontsize = 16)
##    pl.ylabel('Longitudinal Impedance (Ohms)', fontsize = 16)
###   pl.show()
##    pl.savefig(tar_dir_real+str(i)+".png")
##    pl.clf()
##    
##    pinit = [1.0, 1.0, 1.0]
##    y_err = data_array[:,i,2]*imp_err
##    out = op.leastsq(errfunc, pinit, args=(x_data,data_array[:,i,2],y_err), full_output=1)
##    pfinal_imag = out[0]
##    covar_imag=out[1]
##    x_plot = sp.linspace(-3,30,1000)
##    y_test = fitfunc(pfinal_imag, x_plot)
##    pl.plot(x_plot, y_test, label= "Fitted Function")
##    pl.errorbar(x_data,data_array[:,i,2],yerr=y_err, fmt='ko',label = "Measured Data") 
##    pl.legend()
##    pl.xlabel('Displacement (mm)', fontsize = 16)
##    pl.ylabel('Longitudinal Impedance (Ohms)', fontsize = 16)
###   pl.show()
##    pl.savefig(tar_dir_imag+str(i)+".png")
##    pl.clf()
##    
##    trans_imp.append([pfinal_real[2]*C*10**6/((i+1)*10**6),pfinal_imag[2]*C*10**6/((i+1)*10**6)])
##    long_imp.append([pfinal_real[0],pfinal_imag[0]])
##
##long_imp=sp.array(long_imp)
##trans_imp=sp.array(trans_imp)
pl.semilogy()
pl.plot(dip_data[:,0],dip_data[:,1], 'k-',label="Real Impedance")
pl.plot(dip_data[:,0],dip_data[:,2], 'r-',label='Imaginary Impedance')
pl.xlabel('Frequency (Hz)', fontsize = 16)
pl.ylabel('Dipolar Impedance (Ohms/m)', fontsize = 16)
pl.legend()
##pl.show()
##pl.axis([0,2*10**9, 10**5, 10**9])
pl.savefig(tar_dir_real+"dipolar_horz.pdf")
pl.savefig(tar_dir_real+"dipolar_horz.eps")
pl.clf()

output_file = open(tar_dir_real+"dipolar_horz.csv", 'w+')           #Assign and open output file
for i in range(0,len(dip_data)):
              output_file.write(str(dip_data[i,0])+','+str(dip_data[i,1])+','+str(dip_data[i,2])+'\n')

output_file.close() 
##
##pl.plot(data_array[0,:,0],long_imp[:,0], 'k-',label="Real Impedance")
##pl.plot(data_array[0,:,0],long_imp[:,1], 'r-',label='Imaginary Impedance')
##pl.xlabel('Frequency (Hz)', fontsize = 16)
##pl.ylabel('Longitudinal Impedance (Ohms)', fontsize = 16)
##pl.legend()
####pl.show()
##pl.savefig(tar_dir_real+"longitudinal.pdf")
##pl.clf()
##
##output_file = open(tar_dir_real+"longitudinal.csv", 'w+')           #Assign and open output file
##for i in range(0,len(long_imp)):
##              output_file.write(str(data_array[0,i,0])+','+str(long_imp[i,0])+','+str(long_imp[i,1])+'\n')
##
##output_file.close() 
##
####pl.plot(dip_data[:,0],dip_data[:,1], 'k-')
####pl.axis([0,2*10**9,0,10**7])
####pl.show()
####pl.clf()
##
##for i in range(0,len(trans_imp)):
##    trans_imp[i,0]= trans_imp[i,0]*C/(data_array[0,i,0])
##    trans_imp[i,1]= trans_imp[i,1]*C/(data_array[0,i,0])
##
##pl.plot(data_array[0,:,0], trans_imp[:,0],'k-',label="Real Impedance")
##pl.plot(data_array[0,:,0], trans_imp[:,1],'r-', label="Imaginary Impedance")
####pl.axis([0,2*10**9,-10**6,10**6])
##pl.xlabel('Frequency (Hz)', fontsize = 16)
##pl.ylabel('Total Transverse Impedance (Ohms/m)', fontsize = 16)
##pl.legend()
####pl.show()
##pl.savefig(tar_dir_real+"total_transverse_vert.pdf")
##pl.clf()
##
##output_file = open(tar_dir_real+"total_trans_vert.csv", 'w+')           #Assign and open output file
##for i in range(0,2000):
##              output_file.write(str(data_array[0,i,0])+','+str(trans_imp[i,0])+','+str(trans_imp[i,1])+'\n')
##
##output_file.close() 
##
##imp_quad = []
##for i in range(0,len(trans_imp)):
##    imp_quad.append([trans_imp[i,0]-dip_data[i,0],trans_imp[i,1]-dip_data[i,1]])
##imp_quad=sp.array(imp_quad)
##
##pl.plot(data_array[0,:,0], imp_quad[:,0], 'k-', label="Real Impedance")
##pl.plot(data_array[0,:,0], imp_quad[:,1], 'r-', label="Imaginary Impedance")
####pl.axis([0,2*10**9,-10**6,10**6])
####pl.show()
##pl.legend()
##pl.xlabel('Frequency (Hz)', fontsize = 16)
##pl.ylabel('Quadrupolar Impedance (Ohms/m)', fontsize = 16)
##pl.savefig(tar_dir_real+"quad_horz.pdf")
##pl.clf()
##
##output_file = open(tar_dir_real+"quad_vert.csv", 'w+')           #Assign and open output file
##for i in range(0,2000):
##              output_file.write(str(data_array[0,i,0])+','+str(imp_quad[i,0])+','+str(imp_quad[i,1])+'\n')
##
##output_file.close() 
